<?php

namespace App\Tests\Entity;

use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Override;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserEntityTest extends KernelTestCase
{
    private EntityManager $entityManager;

    #[Override]
    protected function setUp(): void
    {
        $this->entityManager = static::getContainer()->get('doctrine')->getManager();
    }

    public function testSaveOneUser(): void
    {
        /** @var EntityRepository $userRepo */
        $userRepo = $this->entityManager->getRepository(User::class);

        $initialCount = $userRepo->count([]);

        $newUser = new User();
        $newUser->setUsername('new')
            ->setPassword('testtesttest')
            ->setEmail('new@refae.fr')
            ->setNotifyUserRegistration(true)
            ->setUserDefinedPassword(false)
        ;
        $this->entityManager->persist($newUser);
        $this->entityManager->flush();

        $this->assertEquals($initialCount + 1, $userRepo->count([]));
    }
}
