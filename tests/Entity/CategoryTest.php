<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\Tenant;
use App\Repository\CategoryRepository;
use App\Repository\TenantRepository;
use Doctrine\ORM\EntityManager;
use Override;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CategoryTest extends KernelTestCase
{
    private EntityManager $entityManager;

    #[Override]
    protected function setUp(): void
    {
        $this->entityManager = static::getContainer()->get('doctrine')->getManager();
    }

    public function testGetOutcome(): void
    {
        /** @var TenantRepository $userRepo */
        $tenantRepository = $this->entityManager->getRepository(Tenant::class);
        /** @var Tenant $tenant */
        $tenant = $tenantRepository->findOneBy([]);

        $category1 = new Category();
        $category1->setTenant($tenant);
        $category1->setDescription('test_category_test');
        $category1->setName(uniqid());
        $this->entityManager->persist($category1);

        $category2 = new Category();
        $category2->setTenant($tenant);
        $category2->setDescription('test_category_test');
        $category2->setName(uniqid());
        $category2->setParent($category1);
        $this->entityManager->persist($category2);

        $category3 = new Category();
        $category3->setTenant($tenant);
        $category3->setDescription('test_category_test');
        $category3->setName(uniqid());
        $category3->setParent($category2);
        $this->entityManager->persist($category3);

        $category4 = new Category();
        $category4->setTenant($tenant);
        $category4->setDescription('test_category_test');
        $category4->setName(uniqid());
        $category4->setParent($category1);
        $this->entityManager->persist($category4);
        $this->entityManager->flush();
        $category5 = new Category();
        $category5->setTenant($tenant);
        $category5->setDescription('test_category_test');
        $category5->setName(uniqid());
        $this->entityManager->persist($category5);

        $this->entityManager->flush();

        $category6 = new Category();
        $category6->setTenant($tenant);
        $category6->setDescription('test_category_test');
        $category6->setName(uniqid());
        $category6->setParent($category1);
        $this->entityManager->persist($category6);

        $this->entityManager->flush();

        $lftRghtMap = $this->renderLftRghtMap();
        $expected = <<<EOT
1 / 10
2 / 5
3 / 4
6 / 7
1 / 2
8 / 9

EOT;
        $this->assertEquals($expected, $lftRghtMap);

        $category7 = new Category();
        $category7->setTenant($tenant);
        $category7->setDescription('test_category_test');
        $category7->setName(uniqid());
        $this->entityManager->persist($category7);

        $category8 = new Category();
        $category8->setTenant($tenant);
        $category8->setDescription('test_category_test');
        $category8->setName(uniqid());
        $category8->setParent($category7);
        $this->entityManager->persist($category8);

        $this->entityManager->flush();

        $lftRghtMap = $this->renderLftRghtMap();
        $expected = <<<EOT
1 / 10
2 / 5
3 / 4
6 / 7
1 / 2
8 / 9
1 / 4
2 / 3

EOT;
        $this->assertEquals($expected, $lftRghtMap);

        $category9 = new Category();
        $category9->setTenant($tenant);
        $category9->setDescription('test_category_test');
        $category9->setName(uniqid());
        $category9->setParent($category1);
        $this->entityManager->persist($category1);
        $this->entityManager->persist($category9);

        $category10 = new Category();
        $category10->setTenant($tenant);
        $category10->setDescription('test_category_test');
        $category10->setName(uniqid());
        $category10->setParent($category9);
        $this->entityManager->persist($category10);

        $this->entityManager->flush();

        $lftRghtMap = $this->renderLftRghtMap();
        $expected = <<<EOT
1 / 14
2 / 5
3 / 4
6 / 7
1 / 2
8 / 9
1 / 4
2 / 3
10 / 13
11 / 12

EOT;
        $this->assertEquals($expected, $lftRghtMap);
    }

    private function renderLftRghtMap(): string
    {
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = $this->entityManager->getRepository(Category::class);
        $results = $categoryRepository->findBy(['description' => 'test_category_test'], ['id' => 'ASC']);
        $lftRghtMap = '';
        foreach ($results as $result) {
            $lft = $result->getLft();
            $rght = $result->getRght();
            $lftRghtMap .= "$lft / $rght\n";
        }
        return $lftRghtMap;
    }
}
