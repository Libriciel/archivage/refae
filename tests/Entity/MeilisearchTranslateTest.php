<?php

namespace App\Tests\Entity;

use App\Entity\Attribute\ResultSet;
use App\Entity\MeilisearchIndexedInterface;
use App\Service\AttributeExtractor;
use Generator;
use ReflectionAttribute;
use ReflectionClass;
use ReflectionMethod;
use ReflectionProperty;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Serializer\Annotation\Groups;

class MeilisearchTranslateTest extends KernelTestCase
{
    /**
     * @dataProvider provideCases
     */
    public function testEntityClass(string $className)
    {
        $extractor = new AttributeExtractor($className);

        $groups = $extractor->getAnyHaving(Groups::class);
        $withMeilisearch = array_filter(
            $groups,
            function (ReflectionProperty|ReflectionMethod $reflection) {

                /** @var ReflectionAttribute|null $groupAttribute */
                $groupAttribute = $reflection->getAttributes(Groups::class)[0]?->getArguments();
                return in_array('meilisearch', $groupAttribute[0]);
            }
        );
        unset($withMeilisearch['id']);

        $resultSet = $extractor->getAnyHaving(ResultSet::class);
        $diff = array_diff(
            array_keys($withMeilisearch),
            array_keys($resultSet)
        );
        $this->assertEmpty(
            $diff,
            sprintf(
                "L'entité %s n'a pas d'attribut ResultSet pour les éléments Meilisearch : %s",
                $className,
                implode(', ', $diff)
            )
        );
    }

    public function provideCases(): Generator
    {
        $finder = new Finder();
        $finder->files()->in(__DIR__ . '/../../src/Entity')->name('*.php');
        foreach ($finder as $file) {
            $className = 'App\\Entity\\' . $file->getFilenameWithoutExtension();
            if (
                class_exists($className)
                && (new ReflectionClass($className))->implementsInterface(MeilisearchIndexedInterface::class)
            ) {
                yield sprintf("%s implements MeilisearchIndexedInterface", $file->getFilenameWithoutExtension())
                    => [$className];
            }
        }
    }
}
