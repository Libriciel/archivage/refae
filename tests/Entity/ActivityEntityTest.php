<?php

namespace App\Tests\Entity;

use App\Entity\Activity;
use App\Repository\ActivityRepository;
use Doctrine\ORM\EntityManager;
use Override;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ActivityEntityTest extends KernelTestCase
{
    private EntityManager $entityManager;

    #[Override]
    protected function setUp(): void
    {
        $this->entityManager = static::getContainer()->get('doctrine')->getManager();
    }

    public function testGetOutcome(): void
    {
        /** @var ActivityRepository $userRepo */
        $activityRepository = $this->entityManager->getRepository(Activity::class);
        /** @var Activity $activity */
        $activity = $activityRepository->findOneBy(['action' => 'add'], ['id' => 'ASC']);
        $this->assertEquals(
            "{author:test_user1} a {action:ajouté} " .
            "{subject:le service test_tenant1} le {date:03/02/2023} à {time:15h45}",
            $activity->getOutcome()
        );
    }
}
