<?php

namespace App\Tests\Entity;

use App\Entity\Tenant;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TenantEntityTest extends KernelTestCase
{
    public function testBaseurlConstraint(): void
    {
        $tenant = new Tenant();
        $tenant->setName('public')
            ->setBaseurl('public');

        $validator = static::getContainer()->get('validator');
        $errors = (string)$validator->validate($tenant);
        $this->assertStringContainsString('Le baseurl ne peut pas prendre cette valeur', $errors);
    }
}
