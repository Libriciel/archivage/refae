<?php

namespace App\Tests\Entity;

use App\Entity\AccessUser;
use App\Repository\RoleRepository;
use App\Repository\TenantRepository;
use App\Repository\UserRepository;
use App\Service\Permission;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RoleUserEntityTest extends KernelTestCase
{
    public function testUniqueTenantUserConstraint(): void
    {
        $user = static::getContainer()->get(UserRepository::class)
            ->findOneByUsername('test_user1');
        $tenant = static::getContainer()->get(TenantRepository::class)
            ->findOneByBaseurl('test_tenant1');
        $role = static::getContainer()->get(RoleRepository::class)
            ->findOneByName(Permission::ROLE_READER);

        $accessUser = new AccessUser();
        $accessUser->setUser($user)
            ->setTenant($tenant)
            ->setRole($role);

        $validator = static::getContainer()->get('validator');
        $errors = (string)$validator->validate($accessUser);
        $this->assertStringContainsString('Cet utilisateur a déjà un rôle sur ce tenant', $errors);
    }
}
