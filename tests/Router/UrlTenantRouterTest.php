<?php

namespace App\Tests\Router;

use Override;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UrlTenantRouterTest extends KernelTestCase
{
    private ?object $router = null;

    #[Override]
    protected function setUp(): void
    {
        $container = self::getContainer();
        $this->router = $container->get('router');
    }

    public function testGenerate()
    {
        $this->assertEquals(
            sprintf('/%s/home', 'foo'),
            $this->router->generate('app_home', ['tenant_url' => 'foo'])
        );
    }

    public function test404OnNonexistentTenant()
    {
        try {
            $this->router->match('/not_a_tenant/home');
        } catch (NotFoundHttpException) {
            $this->assertTrue(true);
            return;
        }
        $this->fail('router->match("/not_a_tenant/home") did not throw an Exception');
    }
}
