<?php

namespace App\Tests\Service;

use App\Entity\Authority;
use App\Entity\VersionableEntityInterface;
use App\Repository\AuthorityRepository;
use App\Service\NewVersion;
use Doctrine\ORM\EntityManager;
use Exception;
use Override;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class NewVersionTest extends KernelTestCase
{
    private NewVersion $newVersionService;
    private EntityManager $entityManager;

    #[Override]
    protected function setUp(): void
    {
        $this->entityManager = static::getContainer()->get('doctrine')->getManager();
        $this->newVersionService = static::getContainer()->get(NewVersion::class);
    }

    public function testFromEntity()
    {
        /** @var AuthorityRepository $repository */
        $repository = static::getContainer()->get(AuthorityRepository::class);
        /** @var Authority $baseEntity */
        $baseEntity = $repository
            ->findOneBy(['identifier' => 'authority1'], ['version' => 'DESC']);
        /** @var VersionableEntityInterface $newEntity */
        $newEntity = $this->newVersionService->fromEntity($baseEntity, 'test');
        $this->entityManager->persist($newEntity);
        try {
            $this->entityManager->flush();
        } catch (Exception $e) {
            $this->fail($e->getMessage());
        }

        $this->assertEquals($baseEntity->getVersion() + 1, $newEntity->getVersion());
    }
}
