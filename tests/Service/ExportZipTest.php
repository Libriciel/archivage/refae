<?php

namespace App\Tests\Service;

use App\Entity\Profile;
use App\Repository\ProfileRepository;
use App\Service\ExportZip;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ExportZipTest extends KernelTestCase
{
    public function testWithNoResult()
    {
        $em = self::getContainer()->get('doctrine')->getManager();
        /** @var ProfileRepository $repo */
        $repo = $em->getRepository(Profile::class);
        $query = $repo->createQueryBuilder('p')
            ->where('1 = 2');

        $exportZip = new ExportZip();
        $this->assertEquals([], $exportZip->getJsonData(Profile::class, $query));
    }
}
