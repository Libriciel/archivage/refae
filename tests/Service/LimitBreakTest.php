<?php

namespace App\Tests\Service;

use App\Service\LimitBreak;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class LimitBreakTest extends KernelTestCase
{
    public function testFormatBytes()
    {
        $expected = [
            276 => "276 o",
            664062 => "648.5 Ko",
            630204176 => "601.01 Mo",
            1077564049472 => "1003.56 Go",
            163829032538624 => "149 To",
            1053852992804696064 => "936.01 Po",
        ];
        $result = [];
        foreach (array_keys($expected) as $size) {
            $result[$size] = LimitBreak::formatBytes($size);
        }
        $this->assertEquals($expected, $result);
    }
}
