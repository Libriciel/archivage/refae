<?php

namespace App\Tests\Service;

use App\ResultSet\CategoryResultSet;
use App\Service\ExportCsv;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class ExportCsvTest extends KernelTestCase
{
    public function testGenerateCsvFromResultSet()
    {
        $request = new Request(attributes: ['tenant_url' => 'test_tenant1']);
        self::getContainer()->get(RequestStack::class)->push($request);
        $categoryResultSet = self::getContainer()->get(CategoryResultSet::class);

        /** @var ExportCsv $exportCsv */
        $exportCsv = self::getContainer()->get(ExportCsv::class);
        $reponse = $exportCsv->fromResultSet($categoryResultSet);
        $this->assertStringContainsString("Accueil touristique", $reponse->getContent());
        $this->assertStringContainsString(
            "Pôle administratif, Finances et Moyens Généraux",
            $reponse->getContent()
        );
    }
}
