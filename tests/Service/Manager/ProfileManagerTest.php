<?php

namespace App\Tests\Service\Manager;

use App\Entity\VersionableEntityInterface;
use App\Repository\ActivityRepository;
use App\Repository\ProfileRepository;
use App\Repository\TenantRepository;
use App\Service\Manager\ProfileManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProfileManagerTest extends KernelTestCase
{
    public function testNewVersion()
    {
        /** @var ProfileRepository $profileRepository */
        $tenantRepository = static::getContainer()->get(TenantRepository::class);
        $tenant = $tenantRepository->findOneBy(['name' => 'test_tenant1']);
        /** @var ProfileManager $profileManager */
        $profileManager = static::getContainer()->get(ProfileManager::class);

        $profile = $profileManager->newProfile($tenant)
            ->setName('foo')
            ->setIdentifier('bar')
            ->setDescription('baz')
            ->setPublic(true)
            ->setState(VersionableEntityInterface::S_PUBLISHED)
        ;

        $newVersion = $profileManager->newVersion($profile, 'new is always better');

        $this->assertEquals($profile->getName(), $newVersion->getName());
        $this->assertEquals($profile->getIdentifier(), $newVersion->getIdentifier());
        $this->assertEquals($profile->getDescription(), $newVersion->getDescription());
        $this->assertEquals($profile->isPublic(), $newVersion->isPublic());
        $this->assertEquals($profile->getVersion() + 1, $newVersion->getVersion());
        $this->assertEquals(VersionableEntityInterface::S_EDITING, $newVersion->getState());

        /** @var ActivityRepository $activityRepository */
        $activityRepository = static::getContainer()->get(ActivityRepository::class);
        $activity = $activityRepository->findOneBy([
            'action' => 'new-version',
            'profile' => $newVersion,
        ]);
        $this->assertNotNull($activity);
    }
}
