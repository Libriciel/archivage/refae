<?php

namespace App\Tests\Service\Manager;

use App\Entity\Term;
use App\Entity\VersionableEntityInterface;
use App\Entity\Vocabulary;
use App\Repository\ActivityRepository;
use App\Repository\TenantRepository;
use App\Repository\VocabularyRepository;
use App\Service\Manager\VocabularyManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class VocabularyManagerTest extends KernelTestCase
{
    public function testNewVersion()
    {
        /** @var VocabularyRepository $vocabularyRepository */
        $tenantRepository = static::getContainer()->get(TenantRepository::class);
        $tenant = $tenantRepository->findOneBy(['name' => 'test_tenant1']);
        /** @var VocabularyManager $profileManager */
        $vocabularyManager = static::getContainer()->get(VocabularyManager::class);

        $vocabulary = new Vocabulary();
        $vocabulary
            ->setName('foo')
            ->setIdentifier('bar')
            ->setDescription('baz')
            ->setTenant($tenant)
            ->setPublic(true)
            ->setState(VersionableEntityInterface::S_PUBLISHED)
        ;

        $term = new Term($vocabulary);
        $term
            ->setName('name')
            ->setIdentifier('identifier')
            ->setDirectoryName('directoryName');

        $vocabulary->addTerm($term);

        $newVersion = $vocabularyManager->newVersion($vocabulary, 'new is always better');

        $this->assertEquals($vocabulary->getName(), $newVersion->getName());
        $this->assertEquals($vocabulary->getIdentifier(), $newVersion->getIdentifier());
        $this->assertEquals($vocabulary->getDescription(), $newVersion->getDescription());
        $this->assertEquals($vocabulary->isPublic(), $newVersion->isPublic());
        $this->assertEquals($vocabulary->getVersion() + 1, $newVersion->getVersion());
        $this->assertEquals($vocabulary->getTermsCount(), $newVersion->getTermsCount());
        $this->assertEquals(VersionableEntityInterface::S_EDITING, $newVersion->getState());

        /** @var ActivityRepository $activityRepository */
        $activityRepository = static::getContainer()->get(ActivityRepository::class);
        $activity = $activityRepository->findOneBy([
            'action' => 'new-version',
            'vocabulary' => $newVersion,
        ]);
        $this->assertNotNull($activity);
    }
}
