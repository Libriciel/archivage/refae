<?php

namespace App\Tests\Service\Manager;

use App\Service\Manager\FileManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class FileManagerTest extends KernelTestCase
{
    public function testSetDataFromPath()
    {
        $file = FileManager::setDataFromPath(__DIR__ . '/../../resources/example.txt');
        $this->assertEquals('example.txt', $file->getName());
        $this->assertEquals(hash('sha256', 'this is a file'), $file->getHash());

        $file = FileManager::setDataFromPath(__DIR__ . '/../../resources/example.yml', $file);
        $this->assertEquals('example.yml', $file->getName());
    }
}
