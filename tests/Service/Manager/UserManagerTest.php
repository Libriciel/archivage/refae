<?php

namespace App\Tests\Service\Manager;

use App\Entity\Tenant;
use App\Repository\AccessUserRepository;
use App\Repository\UserRepository;
use App\Service\Manager\UserManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserManagerTest extends WebTestCase
{
    public function testCanDeleteUser()
    {
        $accessUserRepository = static::getContainer()->get(AccessUserRepository::class);
        $accessUserRepository->createQueryBuilder('au')
            ->delete()
            ->getQuery()
            ->execute()
        ;
        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneByUsername('test_user2');
        $user->getTenants()->map(fn(Tenant $t) => $user->removeTenant($t));

        /** @var UserManager $userManager */
        $userManager = static::getContainer()->get(UserManager::class);
        $this->assertTrue($userManager->deleteUser($user));
    }

    public function testCantDeleteMyself()
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneByUsername('test_user1');
        $client->loginUser($user);

        /** @var UserManager $userManager */
        $userManager = static::getContainer()->get(UserManager::class);
        $this->assertFalse($userManager->deleteUser($user));
    }
}
