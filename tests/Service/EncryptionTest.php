<?php

namespace App\Tests\Service;

use App\Service\Encryption;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class EncryptionTest extends KernelTestCase
{
    public function testEncryptDecrypt()
    {
        $encryption = new Encryption();

        $initialText = "foo bar";
        $encryptedText = $encryption->encrypt($initialText);
        $decryptedText = $encryption->decrypt($encryptedText);
        $this->assertEquals($initialText, $decryptedText);
        $this->assertNotEquals($initialText, $encryptedText);
    }
}
