<?php

namespace App\Tests\Service;

use App\Repository\PermissionPrivilegeRepository;
use App\Repository\UserRepository;
use App\Service\Permission;
use Doctrine\ORM\EntityManagerInterface;
use Override;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PermissionTest extends KernelTestCase
{
    private readonly EntityManagerInterface $entityManager;

    #[Override]
    protected function setUp(): void
    {
        $this->entityManager = self::getContainer()->get('doctrine')->getManager();
    }

    public function testUserCanAccess()
    {
        /** @var Permission $permission */
        $permission = self::getContainer()->get(Permission::class);

        /** @var UserRepository $userRepository */
        $userRepository = self::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneByUsername('test_user1');

        $this->assertTrue($permission->userCanAccess($user, null, 'Tenant', 'delete'));

        $permissionPrivilegeRepository = self::getContainer()->get(PermissionPrivilegeRepository::class);
        $perm = $permissionPrivilegeRepository->findOneBy([
            'permissionClassname' => 'Tenant',
            'action' => 'delete',
        ]);
        $this->entityManager->remove($perm);
        $this->entityManager->flush();

        $this->assertTrue($permission->userCanAccess($user, null, 'Tenant', 'delete'));
    }
}
