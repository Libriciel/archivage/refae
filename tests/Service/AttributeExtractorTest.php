<?php

/** @noinspection PhpUnusedPrivateFieldInspection */
// phpcs:ignoreFile

namespace App\Tests\Service;

use App\Service\AttributeExtractor;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Attribute;

#[Attribute]
class TestAttribute
{
    public function __construct(public ?string $arg1 = null, public ?string $arg2 = null)
    {
    }
}

class TestClass
{
    #[TestAttribute('arg1')]
    private $prop1;

    #[TestAttribute('arg2', 'arg3')]
    private $prop2;

    #[TestAttribute]
    private $prop3;

    #[TestAttribute('arg4')]
    public function getTest()
    {
    }
}

class AttributeExtractorTest extends KernelTestCase
{
    public function testGetPropertiesForAttribute()
    {
        $result = AttributeExtractor::getPropertiesForAttribute((new TestClass()), TestAttribute::class);
        $expected = ['prop1' => ['arg1'], 'prop2' => ['arg2', 'arg3'], 'prop3' => [], 'getTest' => ['arg4']];
        $this->assertEquals($expected, $result);
    }
}
