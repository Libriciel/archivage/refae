<?php

namespace App\Tests\Controller\Public;

use App\Entity\ManagementRule;
use App\Repository\ManagementRuleRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class ManagementRuleControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/public/management-rule');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', "Règles de gestion");
    }

    public function testView(): void
    {
        $client = static::createClient();

        $em = self::getContainer()->get('doctrine')->getManager();
        /** @var ManagementRuleRepository $repo */
        $repo = $em->getRepository(ManagementRule::class);

        $managementRule = $repo->findOneBy(['public' => true]);
        $client->request(Request::METHOD_GET, sprintf('/public/management-rule/view/%s', $managementRule->getId()));
        $this->assertResponseIsSuccessful();

        $managementRule->setPublic(false);
        $em->persist($managementRule);
        $em->flush();

        $client->request(Request::METHOD_GET, sprintf('/public/management-rule/view/%s', $managementRule->getId()));
        $this->assertResponseRedirects('/public/login');
    }
}
