<?php

namespace App\Tests\Controller\Public;

use App\Entity\DocumentRule;
use App\Repository\DocumentRuleRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class DocumentRuleControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/public/document-rule');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', "Règles documentaires");
    }

    public function testView(): void
    {
        $client = static::createClient();

        $em = self::getContainer()->get('doctrine')->getManager();
        /** @var DocumentRuleRepository $repo */
        $repo = $em->getRepository(DocumentRule::class);

        $documentRule = $repo->findOneBy(['public' => true]);
        $client->request(Request::METHOD_GET, sprintf('/public/document-rule/view/%s', $documentRule->getId()));
        $this->assertResponseIsSuccessful();

        $documentRule->setPublic(false);
        $em->persist($documentRule);
        $em->flush();

        $client->request(Request::METHOD_GET, sprintf('/public/document-rule/view/%s', $documentRule->getId()));
        $this->assertResponseRedirects('/public/login');
    }
}
