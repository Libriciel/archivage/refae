<?php

namespace App\Tests\Controller\Public;

use App\Entity\Agreement;
use App\Repository\AgreementRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class AgreementControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/public/agreement');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', "Accords de versement");
    }

    public function testView(): void
    {
        $client = static::createClient();

        $em = self::getContainer()->get('doctrine')->getManager();
        /** @var AgreementRepository $repo */
        $repo = $em->getRepository(Agreement::class);

        $agreement = $repo->findOneBy(['public' => true]);
        $client->request(Request::METHOD_GET, sprintf('/public/agreement/view/%s', $agreement->getId()));
        $this->assertResponseIsSuccessful();

        $agreement->setPublic(false);
        $em->persist($agreement);
        $em->flush();

        $client->request(Request::METHOD_GET, sprintf('/public/agreement/view/%s', $agreement->getId()));
        $this->assertResponseRedirects('/public/login');
    }
}
