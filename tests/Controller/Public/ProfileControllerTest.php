<?php

namespace App\Tests\Controller\Public;

use App\Entity\Profile;
use App\Repository\ProfileRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class ProfileControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/public/profile');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', "Profils d'archives");
    }

    public function testView(): void
    {
        $client = static::createClient();

        $em = self::getContainer()->get('doctrine')->getManager();
        /** @var ProfileRepository $repo */
        $repo = $em->getRepository(Profile::class);

        $profile = $repo->findOneBy(['public' => true]);
        $client->request(Request::METHOD_GET, sprintf('/public/profile/view/%s', $profile->getId()));
        $this->assertResponseIsSuccessful();

        $profile->setPublic(false);
        $em->persist($profile);
        $em->flush();

        $client->request(Request::METHOD_GET, sprintf('/public/profile/view/%s', $profile->getId()));
        $this->assertResponseRedirects('/public/login');
    }
}
