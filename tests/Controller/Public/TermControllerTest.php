<?php

namespace App\Tests\Controller\Public;

use App\Entity\Vocabulary;
use App\Repository\VocabularyRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class TermControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();

        $em = self::getContainer()->get('doctrine')->getManager();
        /** @var VocabularyRepository $repo */
        $repo = $em->getRepository(Vocabulary::class);

        $vocabulary = $repo->findOneBy(['public' => true]);
        $client->request(Request::METHOD_GET, sprintf('/public/vocabulary/%s/term', $vocabulary->getId()));

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains(
            'h1',
            sprintf("Termes liés au vocabulaire contrôlé %s (%s)", $vocabulary->getName(), $vocabulary->getIdentifier())
        );
    }
}
