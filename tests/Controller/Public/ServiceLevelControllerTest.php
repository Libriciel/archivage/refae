<?php

namespace App\Tests\Controller\Public;

use App\Entity\ServiceLevel;
use App\Repository\ServiceLevelRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class ServiceLevelControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/public/service-level');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', "Niveaux de service");
    }

    public function testView(): void
    {
        $client = static::createClient();

        $em = self::getContainer()->get('doctrine')->getManager();
        /** @var ServiceLevelRepository $repo */
        $repo = $em->getRepository(ServiceLevel::class);

        $serviceLevel = $repo->findOneBy(['public' => true]);
        $client->request(Request::METHOD_GET, sprintf('/public/service-level/view/%s', $serviceLevel->getId()));
        $this->assertResponseIsSuccessful();

        $serviceLevel->setPublic(false);
        $em->persist($serviceLevel);
        $em->flush();

        $client->request(Request::METHOD_GET, sprintf('/public/service-level/view/%s', $serviceLevel->getId()));
        $this->assertResponseRedirects('/public/login');
    }
}
