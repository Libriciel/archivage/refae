<?php

namespace App\Tests\Controller\Public;

use App\Entity\Vocabulary;
use App\Repository\VocabularyRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class VocabularyControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/public/vocabulary');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', "Vocabulaires contrôlés");
    }

    public function testView(): void
    {
        $client = static::createClient();

        $em = self::getContainer()->get('doctrine')->getManager();
        /** @var VocabularyRepository $repo */
        $repo = $em->getRepository(Vocabulary::class);

        $vocabulary = $repo->findOneBy(['public' => true]);
        $client->request(Request::METHOD_GET, sprintf('/public/vocabulary/view/%s', $vocabulary->getId()));
        $this->assertResponseIsSuccessful();

        $vocabulary->setPublic(false);
        $em->persist($vocabulary);
        $em->flush();

        $client->request(Request::METHOD_GET, sprintf('/public/vocabulary/view/%s', $vocabulary->getId()));
        $this->assertResponseRedirects('/public/login');
    }
}
