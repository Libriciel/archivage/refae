<?php

namespace App\Tests\Controller\Public;

use App\Entity\Authority;
use App\Repository\AuthorityRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class AuthorityControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/public/authority');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', "Notices d'autorité");
    }

    public function testView(): void
    {
        $client = static::createClient();

        $em = self::getContainer()->get('doctrine')->getManager();
        /** @var AuthorityRepository $repo */
        $repo = $em->getRepository(Authority::class);

        $authority = $repo->findOneBy(['public' => true]);
        $client->request(Request::METHOD_GET, sprintf('/public/authority/view/%s', $authority->getId()));
        $this->assertResponseIsSuccessful();

        $authority->setPublic(false);
        $em->persist($authority);
        $em->flush();

        $client->request(Request::METHOD_GET, sprintf('/public/authority/view/%s', $authority->getId()));
        $this->assertResponseRedirects('/public/login');
    }
}
