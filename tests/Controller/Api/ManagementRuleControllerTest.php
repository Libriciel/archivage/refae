<?php

namespace App\Tests\Controller\Api;

use App\Tests\Controller\ClientTrait;
use Override;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ManagementRuleControllerTest extends WebTestCase
{
    use ClientTrait;

    #[Override]
    protected function setUp(): void
    {
        static::createClient(); // doit être appelé avant getManager()
        $this->entityManager = static::getContainer()->get('doctrine')->getManager();
    }

    public function testGetCollection(): void
    {
        $this->apiGet('api_management_rule_published_list');
        $this->assertResponseIsSuccessful();

        $content = self::getClient()->getResponse()->getContent();
        $json = json_decode((string) $content, true);

        $this->assertArrayHasKey('hydra:member', $json);
        $this->assertNotEmpty($json['hydra:member']);
    }
}
