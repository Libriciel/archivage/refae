<?php

namespace App\Tests\Controller\Api;

use App\Tests\Controller\ClientTrait;
use Override;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class WhoamiControllerTest extends WebTestCase
{
    use ClientTrait;

    #[Override]
    protected function setUp(): void
    {
        static::createClient(); // doit être appelé avant getManager()
        $this->entityManager = static::getContainer()->get('doctrine')->getManager();
    }

    public function testWhoami(): void
    {
        $this->apiGet('api_whoami');
        $this->assertResponseIsSuccessful();

        $content = self::getClient()->getResponse()->getContent();
        $json = json_decode((string) $content, true);

        $this->assertArrayHasKey('username', $json);

        $this->assertNotEmpty($json['username']);
    }
}
