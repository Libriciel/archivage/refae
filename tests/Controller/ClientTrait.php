<?php

namespace App\Tests\Controller;

use App\Entity\AccessUser;
use App\Entity\ApiToken;
use App\Entity\Tenant;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Router\UrlTenantRouter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\TestBrowserToken;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockFileSessionStorage;
use Symfony\Component\Routing\RouterInterface;

/**
 * @mixin WebTestCase
 */
trait ClientTrait
{
    protected ?EntityManagerInterface $entityManager;
    protected string $defaultUsername = 'test_user1';
    protected string $defaultTenanBaseurl = 'test_tenant1';

    /**
     * @var KernelBrowser[]
     */
    protected static array $loggedClient;
    protected ?User $loggedUser;
    protected ?Tenant $loggedTenant;
    protected Session $session;

    protected function generateUrl(string $route, array $params = [])
    {
        /** @var UrlTenantRouter $router */
        $router = self::getContainer()->get(RouterInterface::class);
        return $router->generate($route, $params);
    }

    protected function createLoggedClient(string $username, ?string $tenant_url = null)
    {
        $client = static::$booted ? static::getClient() : static::createClient();

        $this->setLoggedUser($username, $tenant_url);

        $token = new TestBrowserToken($this->loggedUser->getRoles(), $this->loggedUser, 'main');
        $token->setAttributes([]);

        static::getContainer()->get('security.untracked_token_storage')->setToken($token);

        $sessionSavePath = static::getContainer()->getParameter('session.save_path');
        $sessionStorage = new MockFileSessionStorage($sessionSavePath);

        $this->session = new Session($sessionStorage);
        $this->session->start();

        $sessionCookie = new Cookie(
            $this->session->getName(),
            $this->session->getId(),
            null,
            null,
            'localhost',
        );
        $client->getCookieJar()->set($sessionCookie);

        $tenantSessionId = sprintf('tenant.%s', $tenant_url ?: 'public');
        $this->session->set(
            $tenantSessionId,
            [
                'baseurl' => $tenant_url,
                'username' => $username,
                'connection' => AccessUser::LOGIN_TYPE_REFAE,
            ]
        );
        $this->session->set('_security_main', serialize($token));
        $this->session->save();

        $client->followRedirects();
        static::getClient($client);
        return $client;
    }

    protected function getLoggedClient(string $username, ?string $tenantUrl = null): KernelBrowser
    {
        $key = $username . '/' . $tenantUrl;
        if (!isset(self::$loggedClient[$key])) {
            $client = $this->createLoggedClient($username, $tenantUrl);
            self::$loggedClient[$key] = $client;
        }
        static::getClient(self::$loggedClient[$key]);
        return self::$loggedClient[$key];
    }

    protected function getDefaultLoggedClient(): KernelBrowser
    {
        return $this->getLoggedClient($this->defaultUsername, $this->defaultTenanBaseurl);
    }

    protected function getDefaultAdminLoggedClient(): KernelBrowser
    {
        return $this->getLoggedClient($this->defaultUsername);
    }

    protected function httpGet(string $route, array $routeParams = [], bool $logged = true): Crawler
    {
        $client = $logged
            ? $this->getDefaultLoggedClient()
            : (static::$booted ? static::getClient() : static::createClient());
        $url = $this->generateUrl($route, $routeParams);
        return $client->request(Request::METHOD_GET, $url);
    }

    protected function getLoggedUser(): User
    {
        if (!isset($this->loggedUser)) {
            $this->setLoggedUser($this->defaultUsername, $this->defaultTenanBaseurl);
        }
        return $this->loggedUser;
    }

    protected function setLoggedUser(?string $username = null, ?string $tenant_url = null): void
    {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $this->loggedTenant = null;
        if (!$tenant_url) {
            $this->loggedUser = $userRepository->findOneByUsername($username);
            return;
        }
        $this->loggedUser = $userRepository->findOneByUsernameTenant($username, $tenant_url);
        foreach ($this->loggedUser->getTenants() as $tenant) {
            if ($tenant->getBaseurl() === $tenant_url) {
                $this->loggedUser->setActiveTenant($tenant);
                $this->loggedTenant = $tenant;
                break;
            }
        }
    }

    protected function renewEntity(object &$entity): null|object
    {
        $this->entityManager = static::getContainer()->get('doctrine')->getManager();
        $repository = $this->entityManager->getRepository($entity::class);
        $entity = $entity->getId()
            ? $repository
                ->createQueryBuilder('entity')
                ->where('entity.id = :id')
                ->setParameter('id', $entity->getId())
                ->getQuery()
                ->getOneOrNullResult()
            : null
        ;
        if ($entity) {
            $this->entityManager->refresh($entity);
        }
        return $entity;
    }

    protected function assertEntityDeleted(object &$entity): void
    {
        $entity = $this->renewEntity($entity);
        $this->assertNull(
            $entity ? $entity::class . ':' . $entity->getId() : null,
            'Assert entity is deleted'
        );
    }

    protected function exportCrawler(Crawler $crawler): void
    {
        $exportPath = dirname(__DIR__, 2) . '/public/build/test-debug-export.html';
        $crawler->getNode(0)->ownerDocument->saveHTMLFile($exportPath);
        $url = 'https://' . $_ENV['HTTP_HOST'] . '/build/' . basename($exportPath);
        $this->assertTrue(false, 'exportCrawler: ' . $url);
    }

    protected function apiGet(
        string $route,
        array $routeParams = [],
        string $username = 'WebserviceRequester'
    ): Crawler {
        $routeParams += ['tenant_url' => 'tenant1'];
        $this->setLoggedUser($username, $routeParams['tenant_url']);

        $client = static::$booted ? static::getClient() : static::createClient();
        $url = $this->generateUrl($route, $routeParams);

        $apiToken = new ApiToken();
        $apiToken->setTenant($this->loggedTenant);
        $apiToken->setUser($this->loggedUser);

        $this->entityManager->persist($apiToken);
        $this->entityManager->flush();

        $server = ['HTTP_AUTHORIZATION' => sprintf('Bearer %s', $apiToken->getToken())];

        return $client->request(Request::METHOD_GET, $url, server: $server);
    }
}
