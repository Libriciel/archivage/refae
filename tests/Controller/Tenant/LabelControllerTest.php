<?php

namespace App\Tests\Controller\Tenant;

use App\Entity\Label;
use App\Repository\LabelRepository;
use App\Repository\TenantRepository;
use App\Tests\Controller\ClientTrait;
use Doctrine\ORM\EntityManager;
use Override;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LabelControllerTest extends WebTestCase
{
    use ClientTrait;

    #[Override]
    protected function setUp(): void
    {
        static::createClient(); // doit être appelé avant getManager()
        $this->entityManager = static::getContainer()->get('doctrine')->getManager();
    }

    public function testIndex()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request('GET', '/test_tenant1/label');
        $this->assertResponseIsSuccessful();
    }

    public function testView()
    {
        $client = $this->getDefaultLoggedClient();

        /** @var LabelRepository $labelRepository */
        $labelRepository = self::getContainer()->get(LabelRepository::class);
        $label = $labelRepository->findOneBy(['name' => 'test']);

        $client->request('GET', '/test_tenant1/label/view/' . $label->getId());
        $this->assertResponseIsSuccessful();
    }

    public function testAdd()
    {
        $client = $this->getDefaultLoggedClient();

        /** @var LabelRepository $labelRepository */
        $labelRepository = self::getContainer()->get(LabelRepository::class);

        $crawler = $client->request('GET', '/test_tenant1/label/add');
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton('submit');
        $form = $buttonCrawlerNode->form();

        $data = [
            'name' => 'newLabel',
            'color' => '#813d9c',
        ];
        $form['label'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $label = $labelRepository->findOneBy($data);
        $this->assertNotNull($label);
    }

    public function testEdit()
    {
        $client = $this->getDefaultLoggedClient();

        /** @var LabelRepository $labelRepository */
        $labelRepository = self::getContainer()->get(LabelRepository::class);
        $label = $labelRepository->findOneBy(['name' => 'test']);

        $crawler = $client->request('GET', '/test_tenant1/label/edit/' . $label->getId());
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton('submit');
        $form = $buttonCrawlerNode->form();

        $data = [
            'name' => 'newLabelb',
        ];
        $form['label'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $label = $labelRepository->findOneBy($data);
        $this->assertNotNull($label);
    }

    public function testDelete()
    {
        $client = $this->getDefaultLoggedClient();

        $tenant1 = self::getContainer()->get(TenantRepository::class)->findOneBy(['name' => 'test_tenant1']);
        /** @var EntityManager $entityManager */
        $entityManager = self::getContainer()->get('doctrine')->getManager();
        $label = (new Label())
            ->setName('foo')
            ->setTenant($tenant1);
        $entityManager->persist($label);
        $entityManager->flush();

        $client->request('DELETE', '/test_tenant1/label/delete/' . $label->getId());
        $this->assertResponseIsSuccessful();

        $this->assertEntityDeleted($label);
    }

    public function testExportCsv()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request('GET', '/test_tenant1/label/csv');
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'text/csv; charset=UTF-8');
        $responseBody = $client->getInternalResponse()->getContent();
        $this->assertStringContainsString('Nom,Description,Groupe', $responseBody);
        $this->assertStringContainsString('test,,labelGroup1', $responseBody);
    }
}
