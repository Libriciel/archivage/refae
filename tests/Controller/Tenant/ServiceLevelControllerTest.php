<?php

namespace App\Tests\Controller\Tenant;

use App\Entity\VersionableEntityInterface;
use App\Repository\ActivityRepository;
use App\Repository\ServiceLevelFileRepository;
use App\Repository\ServiceLevelRepository;
use App\Tests\Controller\ClientTrait;
use Override;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ServiceLevelControllerTest extends WebTestCase
{
    use ClientTrait;

    #[Override]
    protected function setUp(): void
    {
        static::createClient(); // doit être appelé avant getManager()
        $this->entityManager = static::getContainer()->get('doctrine')->getManager();
    }

    public function testIndex()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request('GET', '/test_tenant1/service-level');
        $this->assertResponseIsSuccessful();
    }

    public function testIndexPublished()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request('GET', '/test_tenant1/service-level-published');
        $this->assertResponseIsSuccessful();
    }

    public function testIndexReader()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request('GET', '/test_tenant1/reader/service-level');
        $this->assertResponseIsSuccessful();
    }

    public function testView()
    {
        $client = $this->getDefaultLoggedClient();
        $tenant = $this->getLoggedUser()->getActiveTenant();

        /** @var ServiceLevelRepository $serviceLevelRepository */
        $serviceLevelRepository = self::getContainer()->get(ServiceLevelRepository::class);
        $serviceLevel = $serviceLevelRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'serviceLevel1',
            'version' => 1,
        ]);

        $client->request('GET', '/test_tenant1/service-level/view/' . $serviceLevel->getId());
        $this->assertResponseIsSuccessful();
    }

    public function testAdd()
    {
        $client = $this->getDefaultLoggedClient();

        /** @var ServiceLevelRepository $serviceLevelRepository */
        $serviceLevelRepository = self::getContainer()->get(ServiceLevelRepository::class);

        $crawler = $client->request('GET', '/test_tenant1/service-level/add');
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton('submit');
        $form = $buttonCrawlerNode->form();

        $data = [
            'identifier' => 'newServiceLevel',
            'name' => 'newServiceLevel',
            'public' => true,
        ];
        $form['service_level'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $serviceLevel = $serviceLevelRepository->findOneBy($data);
        $this->assertNotNull($serviceLevel);
    }

    public function testEdit()
    {
        $client = $this->getDefaultLoggedClient();

        /** @var ServiceLevelRepository $serviceLevelRepository */
        $serviceLevelRepository = self::getContainer()->get(ServiceLevelRepository::class);
        $serviceLevel = $serviceLevelRepository->findOneBy(
            ['identifier' => 'serviceLevel1', 'state' => VersionableEntityInterface::S_EDITING]
        );

        $crawler = $client->request('GET', '/test_tenant1/service-level/edit/' . $serviceLevel->getId());
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton('submit');
        $form = $buttonCrawlerNode->form();

        // identifier can't change on v2
        $data = [
            'identifier' => 'serviceLevel1b',
            'name' => 'newServiceLevel',
            'public' => true,
        ];
        $form['service_level'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $serviceLevel = $serviceLevelRepository->findOneBy($data);
        $this->assertNull($serviceLevel);

        $data = [
            'name' => 'newServiceLevelb',
            'public' => true,
        ];
        $form['service_level'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $serviceLevel = $serviceLevelRepository->findOneBy($data);
        $this->assertNotNull($serviceLevel);
    }

    public function testPublish()
    {
        $client = $this->getDefaultLoggedClient();
        $tenant = $this->getLoggedUser()->getActiveTenant();

        /** @var ServiceLevelRepository $serviceLevelRepository */
        $serviceLevelRepository = self::getContainer()->get(ServiceLevelRepository::class);
        $serviceLevel = $serviceLevelRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'serviceLevel1',
            'version' => 2,
        ]);

        $client->request('POST', '/test_tenant1/service-level/publish/' . $serviceLevel->getId());
        $this->assertResponseIsSuccessful();

        $serviceLevelv2 = $serviceLevelRepository->find($serviceLevel->getId());
        $this->entityManager->refresh($serviceLevel);
        $this->assertEquals(VersionableEntityInterface::S_PUBLISHED, $serviceLevelv2->getState());
        $serviceLevelv1 = $serviceLevelRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'serviceLevel1',
            'version' => 1,
        ]);
        $this->assertEquals(VersionableEntityInterface::S_DEPRECATED, $serviceLevelv1->getState());
    }

    public function testRevoke()
    {
        $client = $this->getDefaultLoggedClient();
        $tenant = $this->getLoggedUser()->getActiveTenant();
        /** @var ServiceLevelRepository $serviceLevelRepository */
        $serviceLevelRepository = self::getContainer()->get(ServiceLevelRepository::class);

        $serviceLevel = $serviceLevelRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'serviceLevel1',
            'version' => 2,
        ]);

        $client->request('POST', '/test_tenant1/service-level/publish/' . $serviceLevel->getId());
        $client->request('POST', '/test_tenant1/service-level/revoke/' . $serviceLevel->getId());
        $this->assertResponseIsSuccessful();

        $serviceLevel = $serviceLevelRepository->find($serviceLevel->getId());
        $this->entityManager->refresh($serviceLevel);
        $this->assertEquals(VersionableEntityInterface::S_REVOKED, $serviceLevel->getState());
    }

    public function testRestore()
    {
        $client = $this->getDefaultLoggedClient();
        $tenant = $this->getLoggedUser()->getActiveTenant();
        /** @var ServiceLevelRepository $serviceLevelRepository */
        $serviceLevelRepository = self::getContainer()->get(ServiceLevelRepository::class);

        $serviceLevel = $serviceLevelRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'serviceLevel1',
            'version' => 2,
        ]);
        $serviceLevel->setState(VersionableEntityInterface::S_REVOKED);
        $entityManager = self::getContainer()->get('doctrine')->getManager();
        $entityManager->persist($serviceLevel);
        $entityManager->flush();

        $client->request('POST', '/test_tenant1/service-level/restore/' . $serviceLevel->getId());
        $this->assertResponseIsSuccessful();

        $serviceLevel = $serviceLevelRepository->find($serviceLevel->getId());
        $this->entityManager->refresh($serviceLevel);
        $this->assertEquals(VersionableEntityInterface::S_PUBLISHED, $serviceLevel->getState());
    }

    public function testActivate()
    {
        $client = $this->getDefaultLoggedClient();
        $tenant = $this->getLoggedUser()->getActiveTenant();
        /** @var ServiceLevelRepository $serviceLevelRepository */
        $serviceLevelRepository = self::getContainer()->get(ServiceLevelRepository::class);

        $serviceLevel = $serviceLevelRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'serviceLevel1',
            'version' => 2,
        ]);
        $serviceLevel->setState(VersionableEntityInterface::S_DEACTIVATED);
        $entityManager = self::getContainer()->get('doctrine')->getManager();
        $entityManager->persist($serviceLevel);
        $entityManager->flush();

        $client->request('POST', '/test_tenant1/service-level/activate/' . $serviceLevel->getId());
        $this->assertResponseIsSuccessful();

        $serviceLevel = $serviceLevelRepository->find($serviceLevel->getId());
        $this->entityManager->refresh($serviceLevel);
        $this->assertEquals(VersionableEntityInterface::S_PUBLISHED, $serviceLevel->getState());
    }

    public function testDeactivate()
    {
        $client = $this->getDefaultLoggedClient();
        $tenant = $this->getLoggedUser()->getActiveTenant();
        /** @var ServiceLevelRepository $serviceLevelRepository */
        $serviceLevelRepository = self::getContainer()->get(ServiceLevelRepository::class);

        $serviceLevel = $serviceLevelRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'serviceLevel1',
            'version' => 2,
        ]);
        $serviceLevel->setState(VersionableEntityInterface::S_PUBLISHED);
        $entityManager = self::getContainer()->get('doctrine')->getManager();
        $entityManager->persist($serviceLevel);
        $entityManager->flush();

        $client->request('POST', '/test_tenant1/service-level/deactivate/' . $serviceLevel->getId());
        $this->assertResponseIsSuccessful();

        $serviceLevel = $serviceLevelRepository->find($serviceLevel->getId());
        $this->entityManager->refresh($serviceLevel);
        $this->assertEquals(VersionableEntityInterface::S_DEACTIVATED, $serviceLevel->getState());
    }

    public function testNewVersion()
    {
        $client = $this->getDefaultLoggedClient();

        /** @var ServiceLevelFileRepository $profileFileRepository */
        $profileFileRepository = self::getContainer()->get(ServiceLevelFileRepository::class);
        $profileFileRepository->createQueryBuilder('p')
            ->delete()
            ->getQuery()
            ->execute()
        ;
        /** @var ActivityRepository $activityRepository */
        $activityRepository = self::getContainer()->get(ActivityRepository::class);
        $activityRepository->createQueryBuilder('a')
            ->delete()
            ->getQuery()
            ->execute()
        ;

        /** @var ServiceLevelRepository $serviceLevelRepository */
        $serviceLevelRepository = self::getContainer()->get(ServiceLevelRepository::class);
        $serviceLevelRepository->createQueryBuilder('a')
            ->delete()
            ->where('a.identifier = :identifier')
            ->setParameter('identifier', 'serviceLevel1')
            ->andWhere('a.state != :state')
            ->setParameter('state', VersionableEntityInterface::S_PUBLISHED)
            ->getQuery()
            ->execute()
        ;
        $serviceLevel = $serviceLevelRepository->findOneBy(
            [
                'identifier' => 'serviceLevel1',
                'state' => VersionableEntityInterface::S_PUBLISHED,
            ]
        );
        $currentVersion = $serviceLevel->getVersion();

        $crawler = $client->request(
            'GET',
            '/test_tenant1/service-level/new-version/' . $serviceLevel->getId()
        );
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton('submit');
        $form = $buttonCrawlerNode->form();

        $data = ['reason' => 'nouvelle version'];
        $form['service_level_version'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $serviceLevel = $serviceLevelRepository->findOneBy(
            ['identifier' => 'serviceLevel1', 'state' => VersionableEntityInterface::S_EDITING]
        );
        $this->assertEquals($currentVersion + 1, $serviceLevel->getVersion());
    }

    public function testDelete()
    {
        $client = $this->getDefaultLoggedClient();
        $tenant = $this->getLoggedUser()->getActiveTenant();

        /** @var ServiceLevelRepository $serviceLevelRepository */
        $serviceLevelRepository = self::getContainer()->get(ServiceLevelRepository::class);
        $serviceLevel = $serviceLevelRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'serviceLevel1',
            'version' => 2,
        ]);

        $client->request('DELETE', '/test_tenant1/service-level/delete/' . $serviceLevel->getId());
        $this->assertResponseIsSuccessful();

        $this->assertEntityDeleted($serviceLevel);
    }

    public function testExport()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request('GET', '/test_tenant1/service-level/export');
        $this->assertResponseIsSuccessful();
    }

    public function testExportCsv()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request('GET', '/test_tenant1/service-level/csv');
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'text/csv; charset=UTF-8');
        $responseBody = $client->getInternalResponse()->getContent();
        $this->assertStringContainsString(
            'Identifiant,Nom,Description,Public,Version,Fichiers,Étiquettes',
            $responseBody
        );
        $this->assertStringContainsString(
            'serviceLevel1,serviceLevel1,"Ceci est un niveau de service",1,2,,',
            $responseBody
        );
    }
}
