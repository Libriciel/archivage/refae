<?php

namespace App\Tests\Controller\Tenant;

use App\Entity\Vocabulary;
use App\Repository\TermRepository;
use App\Repository\VocabularyRepository;
use App\Tests\Controller\ClientTrait;
use Override;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TermControllerTest extends WebTestCase
{
    use ClientTrait;

    #[Override]
    protected function setUp(): void
    {
        static::createClient(); // doit être appelé avant getManager()
        $this->entityManager = static::getContainer()->get('doctrine')->getManager();
    }

    protected function getVocabulary(): Vocabulary
    {
        $tenant = $this->getLoggedUser()->getActiveTenant();
        /** @var VocabularyRepository $termRepository */
        $termRepository = self::getContainer()->get(VocabularyRepository::class);
        return $termRepository->findOneBy(['tenant' => $tenant, 'identifier' => 'vocabulary1', 'version' => 2]);
    }

    public function testIndex()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request(
            'GET',
            sprintf('/test_tenant1/vocabulary/%s/term', $this->getVocabulary()->getId())
        );
        $this->assertResponseIsSuccessful();
    }

    public function testAdd()
    {
        $client = $this->getDefaultLoggedClient();

        /** @var TermRepository $termRepository */
        $termRepository = self::getContainer()->get(TermRepository::class);

        $crawler = $client->request(
            'GET',
            sprintf('/test_tenant1/vocabulary/%s/term/add', $this->getVocabulary()->getId())
        );
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton('submit');
        $form = $buttonCrawlerNode->form();

        $data = [
            'identifier' => 'newTerm',
            'name' => 'newTerm',
            'directoryName' => 'newTermDirectory',
        ];
        $form['term'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $term = $termRepository->findOneBy($data);
        $this->assertNotNull($term);
    }

    public function testEdit()
    {
        $client = $this->getDefaultLoggedClient();
        $vocabulary = $this->getVocabulary();

        /** @var TermRepository $termRepository */
        $termRepository = self::getContainer()->get(TermRepository::class);
        $term = $termRepository->findOneBy(['vocabulary' => $vocabulary, 'identifier' => 'term1']);

        $crawler = $client->request(
            'GET',
            sprintf('/test_tenant1/vocabulary/%s/term/edit/%s', $vocabulary->getId(), $term->getId())
        );
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton('submit');
        $form = $buttonCrawlerNode->form();

        $data = ['name' => 'newTerm',];
        $form['term'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $term = $termRepository->findOneBy($data);
        $this->assertNotNull($term);
    }

    public function testDelete()
    {
        $client = $this->getDefaultLoggedClient();
        $vocabulary = $this->getVocabulary();

        /** @var TermRepository $termRepository */
        $termRepository = self::getContainer()->get(TermRepository::class);
        $term = $termRepository->findOneBy(['vocabulary' => $vocabulary, 'identifier' => 'term1']);

        $client->request(
            'DELETE',
            sprintf('/test_tenant1/vocabulary/%s/term/delete/%s', $vocabulary->getId(), $term->getId())
        );
        $this->assertResponseIsSuccessful();

        $this->assertEntityDeleted($term);
    }
}
