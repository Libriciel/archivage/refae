<?php

namespace App\Tests\Controller\Tenant;

use App\Entity\VersionableEntityInterface;
use App\Repository\AuthorityFileRepository;
use App\Repository\AuthorityRepository;
use App\Tests\Controller\ClientTrait;
use Override;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AuthorityControllerTest extends WebTestCase
{
    use ClientTrait;

    #[Override]
    protected function setUp(): void
    {
        static::createClient(); // doit être appelé avant getManager()
        $this->entityManager = static::getContainer()->get('doctrine')->getManager();
    }

    public function testIndex()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request('GET', '/test_tenant1/authority');
        $this->assertResponseIsSuccessful();
    }

    public function testIndexPublished()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request('GET', '/test_tenant1/authority-published');
        $this->assertResponseIsSuccessful();
    }

    public function testIndexReader()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request('GET', '/test_tenant1/reader/authority');
        $this->assertResponseIsSuccessful();
    }

    public function testView()
    {
        $client = $this->getDefaultLoggedClient();
        $tenant = $this->getLoggedUser()->getActiveTenant();

        /** @var AuthorityRepository $authorityRepository */
        $authorityRepository = self::getContainer()->get(AuthorityRepository::class);
        $authority = $authorityRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'authority1',
            'version' => 1,
        ]);

        $client->request('GET', '/test_tenant1/authority/view/' . $authority->getId());
        $this->assertResponseIsSuccessful();
    }

    public function testAdd()
    {
        $client = $this->getDefaultLoggedClient();

        /** @var AuthorityRepository $authorityRepository */
        $authorityRepository = self::getContainer()->get(AuthorityRepository::class);

        $crawler = $client->request('GET', '/test_tenant1/authority/add');
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton('submit');
        $form = $buttonCrawlerNode->form();

        $data = [
            'identifier' => 'newAuthority',
            'name' => 'newAuthority',
            'public' => true,
        ];
        $form['authority'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $authority = $authorityRepository->findOneBy($data);
        $this->assertNotNull($authority);
    }

    public function testEdit()
    {
        $client = $this->getDefaultLoggedClient();
        $tenant = $this->getLoggedUser()->getActiveTenant();

        /** @var AuthorityRepository $authorityRepository */
        $authorityRepository = self::getContainer()->get(AuthorityRepository::class);
        $authority = $authorityRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'authority1',
            'state' => VersionableEntityInterface::S_EDITING,
        ]);

        $crawler = $client->request('GET', '/test_tenant1/authority/edit/' . $authority->getId());
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton('submit');
        $form = $buttonCrawlerNode->form();

        // identifier can't change on v2
        $data = [
            'identifier' => 'authority1b',
            'name' => 'newAuthority',
            'public' => true,
        ];
        $form['authority'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $authority = $authorityRepository->findOneBy($data);
        $this->assertNull($authority);

        $data = [
            'name' => 'newAuthorityb',
            'public' => true,
        ];
        $form['authority'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $authority = $authorityRepository->findOneBy($data);
        $this->assertNotNull($authority);
    }

    public function testPublish()
    {
        $client = $this->getDefaultLoggedClient();

        /** @var AuthorityRepository $authorityRepository */
        $authorityRepository = self::getContainer()->get(AuthorityRepository::class);
        $tenant = $this->getLoggedUser()->getActiveTenant();
        $authority = $authorityRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'authority1',
            'version' => 2,
        ]);

        $client->request('POST', '/test_tenant1/authority/publish/' . $authority->getId());
        $this->assertResponseIsSuccessful();

        $authorityv2 = $this->renewEntity($authority);
        $this->assertEquals(VersionableEntityInterface::S_PUBLISHED, $authorityv2->getState());
        $authorityv1 = $authorityRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'authority1',
            'version' => 1,
        ]);
        $this->assertEquals(VersionableEntityInterface::S_DEPRECATED, $authorityv1->getState());
    }

    public function testRevoke()
    {
        $client = $this->getDefaultLoggedClient();
        /** @var AuthorityRepository $authorityRepository */
        $authorityRepository = self::getContainer()->get(AuthorityRepository::class);

        $tenant = $this->getLoggedUser()->getActiveTenant();
        $authority = $authorityRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'authority1',
            'version' => 2,
        ]);

        $client->request('POST', '/test_tenant1/authority/publish/' . $authority->getId());
        $client->request('POST', '/test_tenant1/authority/revoke/' . $authority->getId());
        $this->assertResponseIsSuccessful();

        $this->renewEntity($authority);
        $this->assertEquals(VersionableEntityInterface::S_REVOKED, $authority->getState());
    }

    public function testRestore()
    {
        $client = $this->getDefaultLoggedClient();
        /** @var AuthorityRepository $authorityRepository */
        $authorityRepository = self::getContainer()->get(AuthorityRepository::class);

        $tenant = $this->getLoggedUser()->getActiveTenant();
        $authority = $authorityRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'authority1',
            'version' => 2,
        ]);
        $authority->setState(VersionableEntityInterface::S_REVOKED);
        $entityManager = self::getContainer()->get('doctrine')->getManager();
        $entityManager->persist($authority);
        $entityManager->flush();

        $client->request('POST', '/test_tenant1/authority/restore/' . $authority->getId());
        $this->assertResponseIsSuccessful();

        $authority = $authorityRepository->find($authority->getId());
        $this->entityManager->refresh($authority);
        $this->assertEquals(VersionableEntityInterface::S_PUBLISHED, $authority->getState());
    }

    public function testActivate()
    {
        $client = $this->getDefaultLoggedClient();
        /** @var AuthorityRepository $authorityRepository */
        $authorityRepository = self::getContainer()->get(AuthorityRepository::class);

        $tenant = $this->getLoggedUser()->getActiveTenant();
        $authority = $authorityRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'authority1',
            'version' => 2,
        ]);
        $authority->setState(VersionableEntityInterface::S_DEACTIVATED);
        $entityManager = self::getContainer()->get('doctrine')->getManager();
        $entityManager->persist($authority);
        $entityManager->flush();

        $client->request('POST', '/test_tenant1/authority/activate/' . $authority->getId());
        $this->assertResponseIsSuccessful();

        $this->renewEntity($authority);
        $this->assertEquals(VersionableEntityInterface::S_PUBLISHED, $authority->getState());
    }

    public function testDeactivate()
    {
        $client = $this->getDefaultLoggedClient();
        /** @var AuthorityRepository $authorityRepository */
        $authorityRepository = self::getContainer()->get(AuthorityRepository::class);

        $tenant = $this->getLoggedUser()->getActiveTenant();
        $authority = $authorityRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'authority1',
            'version' => 2,
        ]);
        $authority->setState(VersionableEntityInterface::S_PUBLISHED);
        $entityManager = self::getContainer()->get('doctrine')->getManager();
        $entityManager->persist($authority);
        $entityManager->flush();

        $client->request('POST', '/test_tenant1/authority/deactivate/' . $authority->getId());
        $this->assertResponseIsSuccessful();

        $authority = $authorityRepository->find($authority->getId());
        $this->entityManager->refresh($authority);
        $this->assertEquals(VersionableEntityInterface::S_DEACTIVATED, $authority->getState());
    }

    public function testNewVersion()
    {
        $client = $this->getDefaultLoggedClient();
        $tenant = $this->getLoggedUser()->getActiveTenant();

        $authorityFileRepository = self::getContainer()->get(AuthorityFileRepository::class);
        $authorityFileRepository->createQueryBuilder('a')
            ->delete()
            ->getQuery()
            ->execute()
        ;
        /** @var AuthorityRepository $authorityRepository */
        $authorityRepository = self::getContainer()->get(AuthorityRepository::class);
        $authorityRepository->createQueryBuilder('a')
            ->delete()
            ->where('a.identifier = :identifier')
            ->setParameter('identifier', 'authority1')
            ->andWhere('a.state != :state')
            ->setParameter('state', VersionableEntityInterface::S_PUBLISHED)
            ->andWhere('a.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->getQuery()
            ->execute()
        ;
        $authority = $authorityRepository->findOneBy(
            [
                'tenant' => $tenant,
                'identifier' => 'authority1',
                'state' => VersionableEntityInterface::S_PUBLISHED,
            ]
        );
        $currentVersion = $authority->getVersion();

        $crawler = $client->request('GET', '/test_tenant1/authority/new-version/' . $authority->getId());
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton('submit');
        $form = $buttonCrawlerNode->form();

        $data = ['reason' => 'nouvelle version'];
        $form['authority_version'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $authority = $authorityRepository->findOneBy(
            [
                'tenant' => $tenant,
                'identifier' => 'authority1',
                'state' => VersionableEntityInterface::S_EDITING,
            ]
        );
        $this->assertEquals($currentVersion + 1, $authority->getVersion());
    }

    public function testDelete()
    {
        $client = $this->getDefaultLoggedClient();

        /** @var AuthorityRepository $authorityRepository */
        $authorityRepository = self::getContainer()->get(AuthorityRepository::class);
        $tenant = $this->getLoggedUser()->getActiveTenant();
        $authority = $authorityRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'authority1',
            'version' => 2,
        ]);

        $client->request('DELETE', '/test_tenant1/authority/delete/' . $authority->getId());
        $this->assertResponseIsSuccessful();

        $this->assertEntityDeleted($authority);
    }

    public function testExport()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request('GET', '/test_tenant1/authority/export');
        $this->assertResponseIsSuccessful();
    }

    public function testExportCsv()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request('GET', '/test_tenant1/authority/csv');
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'text/csv; charset=UTF-8');
        $responseBody = $client->getInternalResponse()->getContent();
        $this->assertStringContainsString(
            'Identifiant,Nom,Description,Publique,Version,"Fichiers liés",Étiquettes',
            $responseBody
        );
        $this->assertStringContainsString(
            'authority1,authority1,"description de authority1",1,2,',
            $responseBody
        );
    }
}
