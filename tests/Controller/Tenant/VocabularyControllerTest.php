<?php

namespace App\Tests\Controller\Tenant;

use App\Entity\VersionableEntityInterface;
use App\Repository\VocabularyRepository;
use App\Tests\Controller\ClientTrait;
use Override;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class VocabularyControllerTest extends WebTestCase
{
    use ClientTrait;

    #[Override]
    protected function setUp(): void
    {
        static::createClient(); // doit être appelé avant getManager()
        $this->entityManager = static::getContainer()->get('doctrine')->getManager();
    }

    public function testIndex()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request('GET', '/test_tenant1/vocabulary');
        $this->assertResponseIsSuccessful();
    }

    public function testIndexPublished()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request('GET', '/test_tenant1/vocabulary-published');
        $this->assertResponseIsSuccessful();
    }

    public function testIndexReader()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request('GET', '/test_tenant1/reader/vocabulary');
        $this->assertResponseIsSuccessful();
    }

    public function testView()
    {
        $client = $this->getDefaultLoggedClient();
        $tenant = $this->getLoggedUser()->getActiveTenant();

        /** @var VocabularyRepository $vocabularyRepository */
        $vocabularyRepository = self::getContainer()->get(VocabularyRepository::class);
        $vocabulary = $vocabularyRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'vocabulary1',
            'version' => 1,
        ]);

        $client->request('GET', '/test_tenant1/vocabulary/view/' . $vocabulary->getId());
        $this->assertResponseIsSuccessful();
    }

    public function testAdd()
    {
        $client = $this->getDefaultLoggedClient();

        /** @var VocabularyRepository $vocabularyRepository */
        $vocabularyRepository = self::getContainer()->get(VocabularyRepository::class);

        $crawler = $client->request('GET', '/test_tenant1/vocabulary/add');
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton('submit');
        $form = $buttonCrawlerNode->form();

        $data = [
            'identifier' => 'newVocabulary',
            'name' => 'newVocabulary',
            'public' => true,
        ];
        $form['vocabulary'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $vocabulary = $vocabularyRepository->findOneBy($data);
        $this->assertNotNull($vocabulary);
    }

    public function testEdit()
    {
        $client = $this->getDefaultLoggedClient();

        /** @var VocabularyRepository $vocabularyRepository */
        $vocabularyRepository = self::getContainer()->get(VocabularyRepository::class);
        $vocabulary = $vocabularyRepository->findOneBy(
            [
                'identifier' => 'vocabulary1',
                'state' => VersionableEntityInterface::S_EDITING,
            ]
        );

        $crawler = $client->request('GET', '/test_tenant1/vocabulary/edit/' . $vocabulary->getId());
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton('submit');
        $form = $buttonCrawlerNode->form();

        // identifier can't change on v2
        $data = [
            'identifier' => 'vocabulary1bis',
        ];
        $form['vocabulary'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $vocabulary = $vocabularyRepository->findOneBy($data);
        $this->assertNull($vocabulary);

        $data = [
            'name' => 'newVocabularyb',
        ];
        $form['vocabulary'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $vocabulary = $vocabularyRepository->findOneBy($data);
        $this->assertNotNull($vocabulary);
    }

    public function testPublish()
    {
        $client = $this->getLoggedClient('test_user2', 'test_tenant1');
        $tenant = $this->getLoggedUser()->getActiveTenant();
        /** @var VocabularyRepository $vocabularyRepository */
        $vocabularyRepository = self::getContainer()->get(VocabularyRepository::class);

        $vocabulary = $vocabularyRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'vocabulary1',
            'version' => 2,
        ]);

        $client->request('POST', '/test_tenant1/vocabulary/publish/' . $vocabulary->getId());
        $this->assertResponseIsSuccessful();

        $vocabularyv2 = $vocabularyRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'vocabulary1',
            'version' => 2,
        ]);
        $this->assertEquals(VersionableEntityInterface::S_PUBLISHED, $vocabularyv2->getState());
        $vocabularyv1 = $vocabularyRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'vocabulary1',
            'version' => 1,
        ]);
        $this->assertEquals(VersionableEntityInterface::S_DEPRECATED, $vocabularyv1->getState());
    }

    public function testRevoke()
    {
        $client = $this->getLoggedClient('test_user2', 'test_tenant1');
        $tenant = $this->getLoggedUser()->getActiveTenant();
        /** @var VocabularyRepository $vocabularyRepository */
        $vocabularyRepository = self::getContainer()->get(VocabularyRepository::class);

        $vocabulary = $vocabularyRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'vocabulary1',
            'version' => 2,
        ]);

        $client->request('POST', '/test_tenant1/vocabulary/publish/' . $vocabulary->getId());
        $client->request('POST', '/test_tenant1/vocabulary/revoke/' . $vocabulary->getId());
        $this->assertResponseIsSuccessful();

        $this->renewEntity($vocabulary);
        $this->assertEquals(VersionableEntityInterface::S_REVOKED, $vocabulary->getState());
    }

    public function testExport()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request('GET', '/test_tenant1/vocabulary/export');
        $this->assertResponseIsSuccessful();
    }

    public function testExportCsv()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request('GET', '/test_tenant1/vocabulary/csv');
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'text/csv; charset=UTF-8');
        $responseBody = $client->getInternalResponse()->getContent();
        $this->assertStringContainsString(
            'Identifiant,Nom,Description,Public,Version,Fichiers,"Nombre de termes",Étiquettes',
            $responseBody
        );
        $this->assertStringContainsString(
            'vocabulary1,vocabulary1,"this is vocabulary 1",1,2,,1,test',
            $responseBody
        );
    }
}
