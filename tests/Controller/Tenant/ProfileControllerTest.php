<?php

namespace App\Tests\Controller\Tenant;

use App\Entity\VersionableEntityInterface;
use App\Repository\ActivityRepository;
use App\Repository\ProfileFileRepository;
use App\Repository\ProfileRepository;
use App\Tests\Controller\ClientTrait;
use Override;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProfileControllerTest extends WebTestCase
{
    use ClientTrait;

    #[Override]
    protected function setUp(): void
    {
        static::createClient(); // doit être appelé avant getManager()
        $this->entityManager = static::getContainer()->get('doctrine')->getManager();
    }

    public function testIndex()
    {
        $client = $this->getLoggedClient('test_user2', 'test_tenant1');
        $client->request('GET', '/test_tenant1/profile');
        $this->assertResponseIsSuccessful();
    }

    public function testIndexPublished()
    {
        $client = $this->getLoggedClient('test_user2', 'test_tenant1');
        $client->request('GET', '/test_tenant1/profile-published');
        $this->assertResponseIsSuccessful();
    }
    public function testIndexReader()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request('GET', '/test_tenant1/reader/profile');
        $this->assertResponseIsSuccessful();
    }

    public function testView()
    {
        $client = $this->getLoggedClient('test_user2', 'test_tenant1');
        $tenant = $this->getLoggedUser()->getActiveTenant();

        /** @var ProfileRepository $profileRepository */
        $profileRepository = self::getContainer()->get(ProfileRepository::class);
        $profile = $profileRepository->findOneBy(['tenant' => $tenant, 'identifier' => 'profil1', 'version' => 1]);

        $client->request('GET', '/test_tenant1/profile/view/' . $profile->getId());
        $this->assertResponseIsSuccessful();
    }

    public function testAdd()
    {
        $client = $this->getLoggedClient('test_user2', 'test_tenant1');

        /** @var ProfileRepository $profileRepository */
        $profileRepository = self::getContainer()->get(ProfileRepository::class);

        $crawler = $client->request('GET', '/test_tenant1/profile/add');
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton('submit');
        $form = $buttonCrawlerNode->form();

        $data = [
            'identifier' => 'newProfile',
            'name' => 'newProfile',
            'public' => true,
        ];
        $form['profile'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $profile = $profileRepository->findOneBy($data);
        $this->assertNotNull($profile);
    }

    public function testEdit()
    {
        $client = $this->getLoggedClient('test_user2', 'test_tenant1');
        $tenant = $this->getLoggedUser()->getActiveTenant();

        /** @var ProfileRepository $profileRepository */
        $profileRepository = self::getContainer()->get(ProfileRepository::class);
        $profile = $profileRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'profil1',
            'state' => VersionableEntityInterface::S_EDITING,
        ]);

        $crawler = $client->request('GET', '/test_tenant1/profile/edit/' . $profile->getId());
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton('submit');
        $form = $buttonCrawlerNode->form();

        // identifier can't change on v2
        $data = [
            'identifier' => 'profil1b',
            'name' => 'newProfile',
            'public' => true,
        ];
        $form['profile'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $profile = $profileRepository->findOneBy($data);
        $this->assertNull($profile);

        $data = [
            'name' => 'newProfileb',
            'public' => true,
        ];
        $form['profile'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $profile = $profileRepository->findOneBy($data);
        $this->assertNotNull($profile);
    }

    public function testPublish()
    {
        $client = $this->getLoggedClient('test_user2', 'test_tenant1');
        $tenant = $this->getLoggedUser()->getActiveTenant();

        /** @var ProfileRepository $profileRepository */
        $profileRepository = self::getContainer()->get(ProfileRepository::class);
        $profile = $profileRepository->findOneBy(['tenant' => $tenant, 'identifier' => 'profil1', 'version' => 2]);

        $client->request('POST', '/test_tenant1/profile/publish/' . $profile->getId());
        $this->assertResponseIsSuccessful();

        $profilv2 = $profileRepository->find($profile->getId());
        $this->entityManager->refresh($profilv2);
        $this->assertEquals(VersionableEntityInterface::S_PUBLISHED, $profilv2->getState());
        $profilv1 = $profileRepository->findOneBy(['tenant' => $tenant, 'identifier' => 'profil1', 'version' => 1]);
        $this->assertEquals(VersionableEntityInterface::S_DEPRECATED, $profilv1->getState());
    }

    public function testRevoke()
    {
        $client = $this->getLoggedClient('test_user2', 'test_tenant1');
        $tenant = $this->getLoggedUser()->getActiveTenant();
        /** @var ProfileRepository $profileRepository */
        $profileRepository = self::getContainer()->get(ProfileRepository::class);

        $profile = $profileRepository->findOneBy(['tenant' => $tenant, 'identifier' => 'profil1', 'version' => 2]);

        $client->request('POST', '/test_tenant1/profile/publish/' . $profile->getId());
        $client->request('POST', '/test_tenant1/profile/revoke/' . $profile->getId());
        $this->assertResponseIsSuccessful();

        $profile = $profileRepository->find($profile->getId());
        $this->entityManager->refresh($profile);
        $this->assertEquals(VersionableEntityInterface::S_REVOKED, $profile->getState());
    }

    public function testRestore()
    {
        $client = $this->getLoggedClient('test_user2', 'test_tenant1');
        $tenant = $this->getLoggedUser()->getActiveTenant();
        /** @var ProfileRepository $profileRepository */
        $profileRepository = self::getContainer()->get(ProfileRepository::class);

        $profile = $profileRepository->findOneBy(['tenant' => $tenant, 'identifier' => 'profil1', 'version' => 2]);
        $profile->setState(VersionableEntityInterface::S_REVOKED);
        $entityManager = self::getContainer()->get('doctrine')->getManager();
        $entityManager->persist($profile);
        $entityManager->flush();

        $client->request('POST', '/test_tenant1/profile/restore/' . $profile->getId());
        $this->assertResponseIsSuccessful();

        $profile = $profileRepository->find($profile->getId());
        $this->entityManager->refresh($profile);
        $this->assertEquals(VersionableEntityInterface::S_PUBLISHED, $profile->getState());
    }

    public function testActivate()
    {
        $client = $this->getLoggedClient('test_user2', 'test_tenant1');
        $tenant = $this->getLoggedUser()->getActiveTenant();
        /** @var ProfileRepository $profileRepository */
        $profileRepository = self::getContainer()->get(ProfileRepository::class);

        $profile = $profileRepository->findOneBy(['tenant' => $tenant, 'identifier' => 'profil1', 'version' => 2]);
        $profile->setState(VersionableEntityInterface::S_DEACTIVATED);
        $entityManager = self::getContainer()->get('doctrine')->getManager();
        $entityManager->persist($profile);
        $entityManager->flush();

        $client->request('POST', '/test_tenant1/profile/activate/' . $profile->getId());
        $this->assertResponseIsSuccessful();

        $profile = $profileRepository->find($profile->getId());
        $this->entityManager->refresh($profile);
        $this->assertEquals(VersionableEntityInterface::S_PUBLISHED, $profile->getState());
    }

    public function testDeactivate()
    {
        $client = $this->getLoggedClient('test_user2', 'test_tenant1');
        $tenant = $this->getLoggedUser()->getActiveTenant();
        /** @var ProfileRepository $profileRepository */
        $profileRepository = self::getContainer()->get(ProfileRepository::class);

        $profile = $profileRepository->findOneBy(['tenant' => $tenant, 'identifier' => 'profil1', 'version' => 2]);
        $profile->setState(VersionableEntityInterface::S_PUBLISHED);
        $entityManager = self::getContainer()->get('doctrine')->getManager();
        $entityManager->persist($profile);
        $entityManager->flush();

        $client->request('POST', '/test_tenant1/profile/deactivate/' . $profile->getId());
        $this->assertResponseIsSuccessful();

        $profile = $profileRepository->find($profile->getId());
        $this->entityManager->refresh($profile);
        $this->assertEquals(VersionableEntityInterface::S_DEACTIVATED, $profile->getState());
    }

    public function testNewVersion()
    {
        $client = $this->getLoggedClient('test_user2', 'test_tenant1');
        $tenant = $this->getLoggedUser()->getActiveTenant();

        /** @var ProfileFileRepository $profileFileRepository */
        $profileFileRepository = self::getContainer()->get(ProfileFileRepository::class);
        $profileFileRepository->createQueryBuilder('p')
            ->delete()
            ->getQuery()
            ->execute()
        ;
        /** @var ActivityRepository $activityRepository */
        $activityRepository = self::getContainer()->get(ActivityRepository::class);
        $activityRepository->createQueryBuilder('a')
            ->delete()
            ->getQuery()
            ->execute()
        ;
        /** @var ProfileRepository $profileRepository */
        $profileRepository = self::getContainer()->get(ProfileRepository::class);
        $profileRepository->createQueryBuilder('p')
            ->delete()
            ->where('p.identifier = :identifier')
            ->setParameter('identifier', 'profil1')
            ->andWhere('p.state != :state')
            ->setParameter('state', VersionableEntityInterface::S_PUBLISHED)
            ->getQuery()
            ->execute()
        ;
        $profile = $profileRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'profil1',
            'state' => VersionableEntityInterface::S_PUBLISHED,
        ]);
        $currentVersion = $profile->getVersion();

        $crawler = $client->request('GET', '/test_tenant1/profile/new-version/' . $profile->getId());
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton('submit');
        $form = $buttonCrawlerNode->form();

        $data = ['reason' => 'nouvelle version'];
        $form['profile_version'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $profile = $profileRepository->findOneBy([
            'tenant' => $tenant,
            'identifier' => 'profil1',
            'state' => VersionableEntityInterface::S_EDITING,
        ]);
        $this->assertEquals($currentVersion + 1, $profile->getVersion());
    }

    public function testDelete()
    {
        $client = $this->getLoggedClient('test_user2', 'test_tenant1');
        $tenant = $this->getLoggedUser()->getActiveTenant();

        /** @var ProfileRepository $profileRepository */
        $profileRepository = self::getContainer()->get(ProfileRepository::class);
        $profile = $profileRepository->findOneBy(['tenant' => $tenant, 'identifier' => 'profil1', 'version' => 2]);

        $client->request('DELETE', '/test_tenant1/profile/delete/' . $profile->getId());
        $this->assertResponseIsSuccessful();

        $this->assertEntityDeleted($profile);
    }

    public function testExport()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request('GET', '/test_tenant1/profile/export');
        $this->assertResponseIsSuccessful();
    }

    public function testExportCsv()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request('GET', '/test_tenant1/profile/csv');
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'text/csv; charset=UTF-8');
        $responseBody = $client->getInternalResponse()->getContent();
        $this->assertStringContainsString(
            'Identifiant,Nom,Description,Public,Version,Schémas,Étiquettes',
            $responseBody
        );
        $this->assertStringContainsString('profil1,profil1,,1,2,,', $responseBody);
    }
}
