<?php

namespace App\Tests\Controller\Tenant;

use App\Entity\Category;
use App\Entity\DocumentRule;
use App\Entity\DocumentRuleImportSession;
use App\Entity\ManagementRule;
use App\Entity\RuleType;
use App\Entity\Tenant;
use App\Entity\VersionableEntityInterface;
use App\Repository\CategoryRepository;
use App\Repository\DocumentRuleImportSessionRepository;
use App\Repository\DocumentRuleRepository;
use App\Repository\RuleTypeRepository;
use App\Tests\Controller\ClientTrait;
use Doctrine\ORM\EntityRepository;
use Override;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DocumentRuleControllerTest extends WebTestCase
{
    use ClientTrait;

    private KernelBrowser $client;

    #[Override]
    protected function setUp(): void
    {
        static::createClient(); // doit être appelé avant getManager()
        $this->client = $this->getDefaultLoggedClient();
        $this->getLoggedUser();

        $this->entityManager = static::getContainer()->get('doctrine')->getManager();
    }

    public function testIndex()
    {
        $this->client->request('GET', '/test_tenant1/document-rule');
        $this->assertResponseIsSuccessful();
    }

    public function testIndexPublished()
    {
        $this->client->request('GET', '/test_tenant1/document-rule-published');
        $this->assertResponseIsSuccessful();
    }

    public function testIndexReader()
    {
        $this->client->request('GET', '/test_tenant1/reader/document-rule');
        $this->assertResponseIsSuccessful();
    }

    /** @noinspection PhpConditionAlreadyCheckedInspection faux positif */
    public function testImport()
    {
        $crawler = $this->httpGet(
            'app_document_rule_import_csv3',
            ['tenant_url' => 'test_tenant1', 'username' => 'test_user1']
        );
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton("Enregistrer la catégorie");
        $form = $buttonCrawlerNode->form();

        /** @var DocumentRuleImportSessionRepository $documentRuleImportSessionRepository */
        $documentRuleImportSessionRepository = $this->entityManager
            ->getRepository(DocumentRuleImportSession::class)
        ;

        /** @var CategoryRepository|EntityRepository $categoryRepository */
        $categoryRepository = $this->entityManager->getRepository(Category::class);

        $initialCategoryCount = $categoryRepository->count([]);

        $documentRuleImportSession = $documentRuleImportSessionRepository->findOneBy(
            [
                'tenant' => $this->loggedTenant,
                'user' => $this->loggedUser,
            ]
        );
        $previousOffset = $documentRuleImportSession->getLastSeekOffset();
        $this->assertNull($documentRuleImportSession->getLastCategory());
        $this->assertEquals(2, $documentRuleImportSession->getLastRow());

        /*
         * 2e ligne, ajout de la 1ère catégorie
         */
        $this->assertEquals(
            "Catégorie de test d’import",
            $form->getValues()['document_rule_import_csv_row[category_name]'] ?? ''
        );
        $crawler = $this->getDefaultLoggedClient()->submit($form, $form->getValues());
        $this->assertResponseIsSuccessful();
        $buttonCrawlerNode = $crawler->selectButton("Enregistrer la catégorie");
        $form = $buttonCrawlerNode->form();

        // etrange mais il faut à la fois faire un find et un refresh pour que ça marche
        $documentRuleImportSession = $documentRuleImportSessionRepository->findOneBy(
            [
                'tenant' => $this->loggedTenant,
                'user' => $this->loggedUser,
            ]
        );
        $this->entityManager->refresh($documentRuleImportSession);
        $this->assertEquals(3, $documentRuleImportSession->getLastRow());
        $this->assertEquals(0, $documentRuleImportSession->getLastCategoryLevel());
        $this->assertEquals($initialCategoryCount + 1, $categoryRepository->count([]));
        $this->assertNotNull($documentRuleImportSession->getLastCategory());
        $categories = [$documentRuleImportSession->getLastCategory()];

        /*
         * 3e ligne, ajout de la 2ème catégorie
         */
        $this->assertEquals(
            "1er niveau de cat",
            $form->getValues()['document_rule_import_csv_row[category_name]'] ?? ''
        );
        $crawler = $this->getDefaultLoggedClient()->submit($form, $form->getValues());
        $this->assertResponseIsSuccessful();
        $buttonCrawlerNode = $crawler->selectButton("Enregistrer la catégorie");
        $form = $buttonCrawlerNode->form();
        $this->entityManager->refresh($documentRuleImportSession);
        $this->assertEquals(4, $documentRuleImportSession->getLastRow());
        $this->assertEquals(1, $documentRuleImportSession->getLastCategoryLevel());
        $this->assertEquals($initialCategoryCount + 2, $categoryRepository->count([]));
        $this->assertGreaterThan($previousOffset, $documentRuleImportSession->getLastSeekOffset());
        $this->assertNotNull($documentRuleImportSession->getLastCategory());
        $categories[] = $documentRuleImportSession->getLastCategory();

        /*
         * 4e ligne, ajout de la 2ème catégorie
         */
        $this->assertEquals(
            "1ère cat du 2e niveau",
            $form->getValues()['document_rule_import_csv_row[category_name]'] ?? ''
        );
        $crawler = $this->getDefaultLoggedClient()->submit($form, $form->getValues());
        $this->entityManager->refresh($documentRuleImportSession);
        $this->assertResponseIsSuccessful();
        $buttonCrawlerNode = $crawler->selectButton("Enregistrer la règle documentaire");
        $form = $buttonCrawlerNode->form();
        $this->assertEquals(5, $documentRuleImportSession->getLastRow());
        $this->assertEquals(2, $documentRuleImportSession->getLastCategoryLevel());
        $this->assertEquals($initialCategoryCount + 3, $categoryRepository->count([]));
        $categories[] = $documentRuleImportSession->getLastCategory();

        /*
         * 5e ligne, ajout de la 1ère règle documentaire
         */
        $this->assertEquals(
            "La DUA \"5 ans\" n'a pas été trouvée.",
            $crawler->filter('.alert-warning')->text(),
        );
        $this->assertEquals(
            "1ere règle documentaire",
            $form->getValues()['document_rule[identifier]'] ?? ''
        );
        $this->assertEquals(
            "1ere règle documentaire",
            $form->getValues()['document_rule[name]'] ?? ''
        );
        $this->assertEquals(
            "Test 1",
            $form->getValues()['document_rule[observation]'] ?? ''
        );
        $this->assertEquals(
            "paper",
            $form->getValues()['document_rule[documentSupport]'] ?? ''
        );
        $this->assertEquals(
            "destroy",
            $form->getValues()['document_rule[appraisalRuleFinalAction]'] ?? ''
        );
        $this->assertEquals(
            "à partir du blabla",
            $form->getValues()['document_rule[appraisalRuleNote]'] ?? ''
        );
        $this->assertEquals(
            "1",
            $form->getValues()['document_rule[public]'] ?? ''
        );
        $this->assertNotEmpty(
            $form->getValues()['document_rule[categories]'] ?? ''
        );
        $lastCategoryId = (string)$documentRuleImportSession->getLastCategory()->getId();
        $this->assertEquals($lastCategoryId, $form->getValues()['document_rule[categories]'][0]);

        /*
         * On ajoute la DUA manquante et on recharge la page
         */
        /** @var RuleTypeRepository|EntityRepository $ruleTypeRepository */
        $ruleTypeRepository = $this->entityManager->getRepository(RuleType::class);
        $ruleTypes = [];
        foreach ($ruleTypeRepository->findBy(['tenant' => null]) as $ruleType) {
            $ruleTypes[$ruleType->getIdentifier()] = $ruleType;
        }
        $tenantRepository = $this->entityManager->getRepository(Tenant::class);
        $this->loggedTenant = $tenantRepository->find($this->loggedTenant->getId());
        $this->entityManager->refresh($this->loggedTenant);

        $appraisalRule = new ManagementRule();
        $appraisalRule
            ->setTenant($this->loggedTenant)
            ->setRuleType($ruleTypes['AppraisalRule'])
            ->setName('5 ans')
            ->setDescription('5 ans')
            ->setIdentifier('5 ans')
            ->setDuration('P5Y')
            ->setPublic(true)
            ->setState(VersionableEntityInterface::S_PUBLISHED)
            ->setVersion(1)
        ;
        $this->entityManager->persist($appraisalRule);
        $this->entityManager->flush();

        $crawler = $this->httpGet(
            'app_document_rule_import_csv3',
            ['tenant_url' => 'test_tenant1', 'username' => 'test_user1']
        );
        $this->assertResponseIsSuccessful();

        /*
         * On ajoute la Communicabilité manquante et on recharge la page
         */
        $this->assertEquals(
            "Le code de communicabilité \"Immédiat\" n'a pas été trouvé.",
            $crawler->filter('.alert-warning')->text(),
        );
        $accessRule = new ManagementRule();
        $accessRule
            ->setTenant($this->loggedTenant)
            ->setRuleType($ruleTypes['AccessRule'])
            ->setName('Immédiat')
            ->setDescription('Immédiat')
            ->setIdentifier('Immédiat')
            ->setDuration('P0Y')
            ->setPublic(true)
            ->setState(VersionableEntityInterface::S_PUBLISHED)
            ->setVersion(1)
        ;
        $this->entityManager->persist($accessRule);
        $this->entityManager->flush();

        $crawler = $this->httpGet(
            'app_document_rule_import_csv3',
            ['tenant_url' => 'test_tenant1', 'username' => 'test_user1']
        );
        $this->assertResponseIsSuccessful();

        /**
         * La règle est prète à importer
         */
        $this->assertCount(0, $crawler->filter('.alert-warning'));
        $buttonCrawlerNode = $crawler->selectButton("Enregistrer la règle documentaire");
        $form = $buttonCrawlerNode->form();

        /** @var DocumentRuleRepository|EntityRepository $ruleTypeRepository */
        $documentRuleRepository = $this->entityManager->getRepository(DocumentRule::class);
        $initialDocumentCount = $documentRuleRepository->count([]);
        $this->entityManager->refresh($documentRuleImportSession);
        $crawler = $this->getDefaultLoggedClient()->submit($form, $form->getValues());
        $this->assertResponseIsSuccessful();
        $this->assertEquals($initialDocumentCount + 1, $documentRuleRepository->count([]));

        /*
         * 6e ligne, ajout de la 2e règle documentaire
         */
        $this->assertEquals(
            "La DUA \"2ans\" n'a pas été trouvée.",
            $crawler->filter('.alert-warning')->text()
        );
        $this->assertEquals(
            "Le code de communicabilité \"1an\" n'a pas été trouvé.",
            trim($crawler->filter('.alert-warning')->getNode(1)?->nodeValue ?? '')
        );
        $accessRule = new ManagementRule();
        $accessRule
            ->setTenant($this->loggedTenant)
            ->setRuleType($ruleTypes['AccessRule'])
            ->setName('1an')
            ->setDescription('1an')
            ->setIdentifier('1an')
            ->setDuration('P1Y')
            ->setPublic(true)
            ->setState(VersionableEntityInterface::S_PUBLISHED)
            ->setVersion(1)
        ;
        $this->entityManager->persist($accessRule);

        $appraisalRule = new ManagementRule();
        $appraisalRule
            ->setTenant($this->loggedTenant)
            ->setRuleType($ruleTypes['AppraisalRule'])
            ->setName('2ans')
            ->setDescription('2ans')
            ->setIdentifier('2ans')
            ->setDuration('P2Y')
            ->setPublic(true)
            ->setState(VersionableEntityInterface::S_PUBLISHED)
            ->setVersion(1)
        ;
        $this->entityManager->persist($appraisalRule);
        $this->entityManager->flush();

        $crawler = $this->httpGet(
            'app_document_rule_import_csv3',
            ['tenant_url' => 'test_tenant1', 'username' => 'test_user1']
        );
        $this->assertResponseIsSuccessful();

        /**
         * La règle est prète à importer
         */
        $this->assertCount(0, $crawler->filter('.alert-warning'));
        $buttonCrawlerNode = $crawler->selectButton("Enregistrer la règle documentaire");
        $form = $buttonCrawlerNode->form();
        $crawler = $this->getDefaultLoggedClient()->submit($form, $form->getValues());

        $this->entityManager->refresh($documentRuleImportSession);
        $this->assertEquals(2, $documentRuleImportSession->getLastCategoryLevel());

        /*
         * 7e ligne, ajout de la 3e catégorie dans la categorie parente
         */
        $buttonCrawlerNode = $crawler->selectButton("Enregistrer la catégorie");
        $form = $buttonCrawlerNode->form();
        $this->assertNotEmpty(
            $form->getValues()['document_rule_import_csv_row[category_parent]'] ?? ''
        );
        $categoryId = (string)$categories[1]->getId();
        $this->assertEquals($categoryId, $form->getValues()['document_rule_import_csv_row[category_parent]']);
        $this->assertEquals(
            "2eme cat du 2e niveau",
            $form->getValues()['document_rule_import_csv_row[category_name]'] ?? ''
        );
        $crawler = $this->getDefaultLoggedClient()->submit($form, $form->getValues());
        $this->assertResponseIsSuccessful();
        $this->entityManager->refresh($documentRuleImportSession);
        $this->assertEquals(2, $documentRuleImportSession->getLastCategoryLevel());
        $this->assertEquals("2eme cat du 2e niveau", $documentRuleImportSession->getLastCategory()->getName());

        /*
         * 8e ligne, ajout de la 3e règle documentaire
         */
        $buttonCrawlerNode = $crawler->selectButton("Enregistrer la règle documentaire");
        $form = $buttonCrawlerNode->form();
        $this->assertEquals(
            "3e règle documentaire",
            $form->getValues()['document_rule[identifier]'] ?? ''
        );
        $lastCategoryId = (string)$documentRuleImportSession->getLastCategory()->getId();
        $this->assertEquals($lastCategoryId, $form->getValues()['document_rule[categories]'][0]);
        $crawler = $this->getDefaultLoggedClient()->submit($form, $form->getValues());
        $this->assertResponseIsSuccessful();
        $this->assertEquals(
            "L'import du fichier CSV est terminé.",
            trim((string) $crawler->filter('.alert-success')->getNode(1)->nodeValue),
        );
    }
}
