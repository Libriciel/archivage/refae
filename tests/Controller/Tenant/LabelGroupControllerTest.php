<?php

namespace App\Tests\Controller\Tenant;

use App\Entity\LabelGroup;
use App\Repository\LabelGroupRepository;
use App\Repository\TenantRepository;
use App\Tests\Controller\ClientTrait;
use Override;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LabelGroupControllerTest extends WebTestCase
{
    use ClientTrait;

    #[Override]
    protected function setUp(): void
    {
        static::createClient(); // doit être appelé avant getManager()
        $this->entityManager = static::getContainer()->get('doctrine')->getManager();
    }

    public function testIndex()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request('GET', '/test_tenant1/label-group');
        $this->assertResponseIsSuccessful();
    }

    public function testView()
    {
        $client = $this->getDefaultLoggedClient();

        /** @var LabelGroupRepository $labelGroupRepository */
        $labelGroupRepository = self::getContainer()->get(LabelGroupRepository::class);
        $labelGroup = $labelGroupRepository->findOneBy(['name' => 'labelGroup1']);

        $client->request('GET', '/test_tenant1/label-group/view/' . $labelGroup->getId());
        $this->assertResponseIsSuccessful();
    }

    public function testAdd()
    {
        $client = $this->getDefaultLoggedClient();

        /** @var LabelGroupRepository $labelGroupRepository */
        $labelGroupRepository = self::getContainer()->get(LabelGroupRepository::class);

        $crawler = $client->request('GET', '/test_tenant1/label-group/add');
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton('submit');
        $form = $buttonCrawlerNode->form();

        $data = [
            'name' => 'newLabelGroup',
        ];
        $form['label_group'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $labelGroup = $labelGroupRepository->findOneBy($data);
        $this->assertNotNull($labelGroup);
    }

    public function testEdit()
    {
        $client = $this->getDefaultLoggedClient();

        /** @var LabelGroupRepository $labelGroupRepository */
        $labelGroupRepository = self::getContainer()->get(LabelGroupRepository::class);
        $labelGroup = $labelGroupRepository->findOneBy(['name' => 'labelGroup1']);

        $crawler = $client->request('GET', '/test_tenant1/label-group/edit/' . $labelGroup->getId());
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton('submit');
        $form = $buttonCrawlerNode->form();

        $data = [
            'name' => 'newLabelb',
        ];
        $form['label_group'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $labelGroup = $labelGroupRepository->findOneBy($data);
        $this->assertNotNull($labelGroup);
    }

    public function testDelete()
    {
        $client = $this->getDefaultLoggedClient();
        $tenant1 = self::getContainer()->get(TenantRepository::class)->findOneBy(['name' => 'test_tenant1']);

        $labelGroup = new LabelGroup();
        $labelGroup->setTenant($tenant1);
        $labelGroup->setName('testtest');
        $entityManager = self::getContainer()->get('doctrine')->getManager();
        $entityManager->persist($labelGroup);
        $entityManager->flush();

        $client->request('DELETE', '/test_tenant1/label-group/delete/' . $labelGroup->getId());
        $this->assertResponseIsSuccessful();

        $this->assertEntityDeleted($labelGroup);
    }
}
