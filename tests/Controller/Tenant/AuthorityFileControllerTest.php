<?php

namespace App\Tests\Controller\Tenant;

use App\Entity\Authority;
use App\Entity\AuthorityFile;
use App\Repository\AuthorityRepository;
use App\Tests\Controller\ClientTrait;
use Override;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AuthorityFileControllerTest extends WebTestCase
{
    use ClientTrait;

    private ?Authority $authority = null;

    #[Override]
    protected function setUp(): void
    {
        static::createClient(); // doit être appelé avant getManager()
        $this->entityManager = static::getContainer()->get('doctrine')->getManager();
    }

    protected function getAuthority(): Authority
    {
        if (!$this->authority) {
            $tenant = $this->getLoggedUser()->getActiveTenant();
            /** @var AuthorityRepository $authorityFileRepository */
            $authorityFileRepository = self::getContainer()->get(AuthorityRepository::class);
            $authority = $authorityFileRepository->findOneBy(
                ['tenant' => $tenant, 'identifier' => 'authority1', 'version' => 2]
            );
            $this->authority = $authority;
        }
        return $this->authority;
    }

    public function testIndex()
    {
        $client = $this->getDefaultLoggedClient();
        $client->request(
            'GET',
            sprintf('/test_tenant1/authority/%s/file', $this->getAuthority()->getId())
        );
        $this->assertResponseIsSuccessful();
    }

    public function testEdit()
    {
        $client = $this->getDefaultLoggedClient();
        $authorityFile = $this->getAuthority()->getAuthorityFiles()->first();

        $crawler = $client->request(
            'GET',
            sprintf(
                '/test_tenant1/authority/%s/file/edit/%s',
                $this->getAuthority()->getId(),
                $authorityFile->getId()
            )
        );
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton('submit');
        $form = $buttonCrawlerNode->form();

        $data = [
            'type' => AuthorityFile::TYPE_EACCPF_2010,
        ];
        $form['authority_file'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $this->renewEntity($authorityFile);
        $this->assertEquals(AuthorityFile::TYPE_EACCPF_2010, $authorityFile->getType());
    }

    public function testDownload()
    {
        $client = $this->getDefaultLoggedClient();
        $authorityFile = $this->getAuthority()->getAuthorityFiles()->first();

        $client->request(
            'GET',
            sprintf('/test_tenant1/authority-file/%s/download', $authorityFile->getId())
        );

        $this->assertResponseIsSuccessful();
    }

    public function testDelete()
    {
        $client = $this->getDefaultLoggedClient();
        $authority = $this->getAuthority();
        $authorityFile = $authority->getAuthorityFiles()->first();

        $client->request(
            'DELETE',
            sprintf('/test_tenant1/authority/%s/file/delete/%s', $authority->getId(), $authorityFile->getId())
        );
        $this->assertResponseIsSuccessful();

        $this->assertEntityDeleted($authorityFile);
    }
}
