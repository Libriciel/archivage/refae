<?php

namespace App\Tests\Controller\Admin;

use App\Tests\Controller\ClientTrait;
use Override;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    use ClientTrait;

    #[Override]
    protected function setUp(): void
    {
        static::createClient(); // doit être appelé avant getManager()
        $this->entityManager = static::getContainer()->get('doctrine')->getManager();
    }

    public function testDeleteMyself()
    {
        $client = $this->getDefaultAdminLoggedClient();
        $client->request('DELETE', '/admin/user/delete/' . $this->getLoggedUser()->getUsername());
        $this->assertResponseStatusCodeSame(403);
    }
}
