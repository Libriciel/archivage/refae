<?php

namespace App\Tests\Controller\Admin;

use App\Entity\Ldap;
use App\Repository\LdapRepository;
use App\Tests\Controller\ClientTrait;
use Override;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LdapControllerTest extends WebTestCase
{
    use ClientTrait;

    #[Override]
    protected function setUp(): void
    {
        static::createClient(); // doit être appelé avant getManager()
        $this->entityManager = static::getContainer()->get('doctrine')->getManager();
    }

    public function testIndex()
    {
        $client = $this->getDefaultAdminLoggedClient();
        $client->request('GET', '/admin/ldap');
        $this->assertResponseIsSuccessful();
    }

    public function testView()
    {
        $client = $this->getDefaultAdminLoggedClient();
        $entityManager = $this->getContainer()->get('doctrine')->getManager();
        /** @var LdapRepository $ldapRepository */
        $ldap = (new Ldap())
            ->setName('newLdap')
            ->setHost('ldap')
            ->setPort(1)
            ->setLdapRootSearch('ou=people,dc=planetexpress,dc=com')
            ->setUserLoginAttribute('cn')
            ->setSchema('Adldap\Schemas\ActiveDirectory')
            ->setFollowReferrals(false)
            ->setVersion(3)
            ->setTimeout(5)
            ->setCustomOptions('[]');
        $entityManager->persist($ldap);
        $entityManager->flush();
        $client->request('GET', '/admin/ldap/view/' . $ldap->getId());
        $this->assertResponseIsSuccessful();
    }

    public function testAdd()
    {
        $client = $this->getDefaultAdminLoggedClient();
        /** @var LdapRepository $ldapRepository */
        $ldapRepository = $this->getContainer()->get(LdapRepository::class);

        $crawler = $client->request('GET', '/admin/ldap/add');
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton('submit');
        $form = $buttonCrawlerNode->form();

        $data = [
            'name' => 'newLdap',
            'host' => 'ldap',
            'user_login_attribute' => 'cn',
            'ldap_root_search' => 'ou=people,dc=planetexpress,dc=com',
            'custom_options' => '[]',
        ];
        $form['ldap'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $ldap = $ldapRepository->findOneBy($data);
        $this->assertNotNull($ldap);
    }

    public function testEdit()
    {
        $client = $this->getDefaultAdminLoggedClient();
        $entityManager = $this->getContainer()->get('doctrine')->getManager();
        /** @var LdapRepository $ldapRepository */
        $ldapRepository = $this->getContainer()->get(LdapRepository::class);
        $ldap = (new Ldap())
            ->setName('newLdap')
            ->setHost('ldap')
            ->setPort(1)
            ->setLdapRootSearch('ou=people,dc=planetexpress,dc=com')
            ->setUserLoginAttribute('cn')
            ->setSchema('Adldap\Schemas\ActiveDirectory')
            ->setFollowReferrals(false)
            ->setVersion(3)
            ->setTimeout(5)
            ->setCustomOptions('[]');
        $entityManager->persist($ldap);
        $entityManager->flush();

        $crawler = $client->request(
            'GET',
            sprintf('/admin/ldap/edit/%s', $ldap->getId())
        );
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton('submit');
        $form = $buttonCrawlerNode->form();

        $data = [
            'name' => 'newLdapName',
        ];
        $form['ldap'] = $data;
        $client->submit($form);
        $this->assertResponseIsSuccessful();

        $ldap = $ldapRepository->findOneBy($data);
        $this->assertNotNull($ldap);
    }

    public function testDelete()
    {
        $client = $this->getDefaultAdminLoggedClient();
        $entityManager = $this->getContainer()->get('doctrine')->getManager();
        $ldap = (new Ldap())
            ->setName('newLdap')
            ->setHost('ldap')
            ->setPort(1)
            ->setLdapRootSearch('ou=people,dc=planetexpress,dc=com')
            ->setUserLoginAttribute('cn')
            ->setSchema('Adldap\Schemas\ActiveDirectory')
            ->setFollowReferrals(false)
            ->setVersion(3)
            ->setTimeout(5)
            ->setCustomOptions('[]');
        $entityManager->persist($ldap);
        $entityManager->flush();

        $client->request('DELETE', '/admin/ldap/delete/' . $ldap->getId());
        $this->assertResponseIsSuccessful();

        $this->assertEntityDeleted($ldap);
    }
}
