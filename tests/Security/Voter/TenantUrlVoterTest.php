<?php

namespace App\Tests\Security\Voter;

use App\Repository\UserRepository;
use App\Security\Voter\TenantUrlVoter;
use Generator;
use Override;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

class TenantUrlVoterTest extends KernelTestCase
{
    /**
     * @var MockObject
     */
    protected $token;

    #[Override]
    public function setUp(): void
    {
        $this->token = $this->createMock(TokenInterface::class);
        /** @var UserRepository $userRepo */
        $userRepo = self::getContainer()->get(UserRepository::class);
        $user = $userRepo->findOneBy(['username' => 'test_user1']);

        $this->token
            ->method('getUser')
            ->willReturn($user);
    }

    /**
     * @dataProvider provideCases
     */
    public function testVote(
        string $subject,
        int $expectedVote,
    ) {
        $voter = new TenantUrlVoter();
        $this->assertEquals($expectedVote, $voter->vote($this->token, $subject, [TenantUrlVoter::class]));
    }

    public function provideCases(): Generator
    {
        yield 'test_user1 can access tenant1' => [
            'test_tenant1',
            VoterInterface::ACCESS_GRANTED,
        ];

        yield 'test_user1 cannot access tenant3' => [
            'test_tenant3',
            VoterInterface::ACCESS_DENIED,
        ];
    }
}
