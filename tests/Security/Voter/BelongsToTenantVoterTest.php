<?php

namespace App\Tests\Security\Voter;

use App\Repository\TenantRepository;
use App\Repository\UserRepository;
use App\Security\Voter\BelongsToTenantVoter;
use Generator;
use Override;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

class BelongsToTenantVoterTest extends KernelTestCase
{
    /**
     * @var MockObject
     */
    protected $token;

    #[Override]
    public function setUp(): void
    {
        $this->token = $this->createMock(TokenInterface::class);
        /** @var UserRepository $userRepo */
        $userRepo = self::getContainer()->get(UserRepository::class);
        $user = $userRepo->findOneBy(['username' => 'test_user1']);

        $this->token
            ->method('getUser')
            ->willReturn($user);
    }

    /**
     * @dataProvider provideCases
     */
    public function testVote(
        string $subject,
        int $expectedVote,
    ) {
        $tenantRepository = self::getContainer()->get(TenantRepository::class);
        $request = new Request(attributes: ['tenant_url' => $subject]);
        $requestStack = new RequestStack();
        $requestStack->push($request);
        $user = self::getContainer()->get(UserRepository::class)->findOneByUsername('test_user1');

        $voter = new BelongsToTenantVoter($requestStack, $tenantRepository);
        $this->assertEquals($expectedVote, $voter->vote($this->token, $user, [BelongsToTenantVoter::class]));
    }

    public function provideCases(): Generator
    {
        yield 'test_user1 can be viewed in tenant1' => [
            'test_tenant1',
            VoterInterface::ACCESS_GRANTED,
        ];

        yield 'test_user1 cannot be viewed in tenant3' => [
            'test_tenant3',
            VoterInterface::ACCESS_DENIED,
        ];
    }
}
