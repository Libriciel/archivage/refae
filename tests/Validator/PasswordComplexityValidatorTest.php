<?php

namespace App\Tests\Validator;

use App\Validator\PasswordComplexity;
use App\Validator\PasswordComplexityValidator;
use Override;
use stdClass;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class PasswordComplexityValidatorTest extends ConstraintValidatorTestCase
{
    public function testExpectsStringCompatibleType()
    {
        $this->expectException(UnexpectedValueException::class);
        $this->validator->validate(new stdClass(), new PasswordComplexity());
    }

    public function testInvalidPasswords()
    {
        $this->validator->validate('1234', new PasswordComplexity());

        $this->buildViolation('Ce mot de passe n\'est pas assez complexe')
            ->setParameter('{{ string }}', '1234')
            ->assertRaised();
    }

    #[Override]
    protected function createValidator(): PasswordComplexityValidator
    {
        return new PasswordComplexityValidator();
    }
}
