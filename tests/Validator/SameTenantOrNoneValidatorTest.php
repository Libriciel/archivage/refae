<?php

namespace App\Tests\Validator;

use App\Entity\AccessUser;
use App\Entity\Role;
use App\Entity\Tenant;
use App\Validator\SameTenantOrNone;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SameTenantOrNoneValidatorTest extends KernelTestCase
{
    public function testOtherTenant(): void
    {
        $tenant = new Tenant();
        $tenant
            ->setName('tenant')
            ->setBaseurl('tenant');

        $otherTenant = new Tenant();
        $otherTenant
            ->setName('otherTenant')
            ->setBaseurl('otherTenant');

        $role = new Role();
        $role
            ->setName('role')
            ->setTenant($tenant);

        $accessUser = new AccessUser();
        $accessUser
            ->setRole($role)
            ->setTenant($otherTenant);

        $validator = self::getContainer()->get('validator');
        $errors = (string)$validator->validate($accessUser);
        $this->assertStringContainsString(SameTenantOrNone::$errorMessage, $errors);
    }

    public function testNoTenant(): void
    {
        $tenant = new Tenant();
        $tenant
            ->setName('tenant')
            ->setBaseurl('tenant');

        $role = new Role();
        $role->setName('role');

        $accessUser = new AccessUser();
        $accessUser
            ->setRole($role)
            ->setTenant($tenant);

        $validator = self::getContainer()->get('validator');
        $this->assertEmpty($validator->validate($accessUser));
    }
}
