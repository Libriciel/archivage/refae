<?php

namespace App\Tests\Command;

use App\Repository\AccessUserRepository;
use App\Repository\RoleRepository;
use App\Repository\TenantRepository;
use App\Repository\UserRepository;
use App\Service\Permission;
use Override;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class SetUserRoleCommandTest extends KernelTestCase
{
    private readonly UserRepository $userRepo;
    private readonly TenantRepository $tenantRepo;
    private readonly RoleRepository $roleRepo;
    private readonly AccessUserRepository $accessUserRepository;

    #[Override]
    protected function setUp(): void
    {
        $this->userRepo = self::getContainer()->get(UserRepository::class);
        $this->tenantRepo = self::getContainer()->get(TenantRepository::class);
        $this->roleRepo = self::getContainer()->get(RoleRepository::class);
        $this->accessUserRepository = self::getContainer()->get(AccessUserRepository::class);
    }

    public function testExecute(): void
    {
        $user = $this->userRepo->findOneBy(['username' => 'test_user1']);
        $tenant = $this->tenantRepo->findOneByBaseurl('test_tenant1');
        $prevRole = $this->roleRepo->findOneByName(Permission::ROLE_FUNCTIONAL_ADMIN);
        $newRole = $this->roleRepo->findOneByName(Permission::ROLE_READER);

        $this->assertNotNull(
            $this->accessUserRepository->findOneBy([
                'user' => $user,
                'tenant' => $tenant,
                'role' => $prevRole,
            ])
        );
        $this->assertNull(
            $this->accessUserRepository->findOneBy([
                'user' => $user,
                'tenant' => $tenant,
                'role' => $newRole,
            ])
        );

        $kernel = self::bootKernel();
        $application = new Application($kernel);
        $command = $application->find('app:user:set-role');
        $commandTester = new CommandTester($command,);
        $commandTester->execute([
            'method' => 'override',
            '--username' => $user->getUsername(),
            '--tenant' => $tenant->getBaseurl(),
            '--role' => $newRole->getId(),
        ]);
        $commandTester->assertCommandIsSuccessful();

        $output = str_replace(PHP_EOL, '', $commandTester->getDisplay());
        $output = preg_replace('/  +/', ' ', $output);
        $expected = sprintf(
            "[OK] L'utilisateur %s est désormais lié au tenant %s avec le role %s",
            $user->getUsername(),
            $tenant->getBaseurl(),
            $newRole->getCommonName()
        );
        $this->assertStringContainsString($expected, $output);

        $this->assertNull(
            $this->accessUserRepository->findOneBy([
                'user' => $user,
                'tenant' => $tenant,
                'role' => $prevRole,
            ])
        );
        $this->assertNotNull(
            $this->accessUserRepository->findOneBy([
                'user' => $user,
                'tenant' => $tenant,
                'role' => $newRole,
            ])
        );
    }
}
