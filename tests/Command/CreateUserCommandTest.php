<?php

namespace App\Tests\Command;

use App\Repository\UserRepository;
use Override;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class CreateUserCommandTest extends KernelTestCase
{
    private UserRepository $userRepo;

    #[Override]
    protected function setUp(): void
    {
        $this->userRepo = self::getContainer()->get(UserRepository::class);
    }

    public function testExecute(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);
        $command = $application->find('app:create:user');
        $commandTester = new CommandTester($command);
        /** @noinspection PhpArgumentWithoutNamedIdentifierInspection */
        $commandTester->execute([
            'username' => 'refae_user_command',
            'email' => 'command@user.fr',
            'password' => '1234',
        ]);
        $commandTester->assertCommandIsSuccessful();

        $user = $this->userRepo->findOneBy(['username' => 'refae_user_command']);
        $this->assertNotNull($user);
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString(sprintf('[OK] User créé avec l\'id : %s', $user->getId()), $output);
    }
}
