build-dev:
	docker build . -t refae:dev

run:
	docker compose --env-file .env.docker.local run -it web bash

run-init:
	docker compose --env-file .env.docker.local run -it --build init bash

restart:
	docker compose --env-file .env.docker.local restart

up: ## docker compose up
	docker compose --env-file .env.docker.local up -d

up-build:
	docker compose --env-file .env.docker.local up -d --build

log: ## docker compose logs
	docker compose logs -f

down: ## docker compose down
	docker compose down

bash: ## bash sur le docker web
	docker compose exec -it web bash

reload:
	docker compose exec -it web /usr/sbin/apachectl -k graceful

bash-root:
	docker compose exec --user=root -it web bash

npm: ## bash sur le docker npm
	docker compose exec -it watcher bash

drop: ## drop la base de donnée
	docker compose stop async && \
	docker compose exec -it web php bin/console doc:data:drop --force && \
    docker compose exec -it web php bin/console doc:data:create

migration_diff:
	docker compose exec -it web php bin/console doc:mig:diff

migration_migrate:
	docker compose exec -it web php bin/console doc:mig:mig -n && \
	docker compose --env-file .env.docker.local exec -it web php bin/console doc:fix:load --group recette -n

reset:
	docker compose --env-file .env.docker.local restart refae_db && \
	docker compose --env-file .env.docker.local up -d && \
	docker compose stop async && \
	docker compose exec -it web php bin/console app:meilisearch:purge && \
	docker compose exec -it web php bin/console doc:data:drop --force && \
	docker compose exec -it web php bin/console doc:data:create && \
	docker compose exec -it web php bin/console doc:mig:mig -n && \
	docker compose start async && \
	docker compose --env-file .env.docker.local exec -it web php bin/console doc:fix:load --group recette -n

cache-clear:
	docker compose --env-file .env.docker.local exec -it web php bin/console c:c

cache-clear-test:
	docker compose --env-file .env.docker.local exec -it web php bin/console c:c --env=test

install-composer:
	docker run composer:latest cat /usr/bin/composer > /tmp/composer && \
	docker compose cp /tmp/composer web:/var/www/refae/tmp/composer

#les options --arg=value ne fonctionnent pas

# If the first argument is "phpunit" or "paratest" ...
ifneq (,$(filter $(MAKECMDGOALS), phpunit paratest cs-check cs-fix composer))
  # use the rest as arguments
  COMMAND_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  COMMAND_ARGS := $(subst :,\:,$(COMMAND_ARGS)) # ":" escaping
  # ...and turn them into do-nothing targets
  $(eval $(COMMAND_ARGS):;@:)
endif

composer:
	docker compose --env-file .env.docker.local exec -it web php /var/www/refae/tmp/composer $(COMMAND_ARGS)

phpunit: ## vide le cache de test et lance phpunit
	docker compose --env-file .env.docker.local exec -it web php bin/console c:c --env=test && \
	docker compose exec -it -e XDEBUG_MODE=off web php bin/phpunit $(COMMAND_ARGS)

paratest: ## vide le cache de test et lance paratest
	docker compose --env-file .env.docker.local exec -it web php bin/console c:c --env=test && \
	docker compose exec -it -e XDEBUG_MODE=off web php vendor/bin/paratest $(COMMAND_ARGS)

cs-check:
	docker compose exec -it -e XDEBUG_MODE=off web php /var/www/refae/tmp/composer cs-check $(COMMAND_ARGS)

cs-fix:
	docker compose exec -it -e XDEBUG_MODE=off web php /var/www/refae/tmp/composer cs-fix $(COMMAND_ARGS)

CWD := $(shell cd -P -- '$(shell dirname -- "$0")' && pwd -P)
export-keycloak-db:
	docker compose exec --user=postgres -it refae_dbkeycloak pg_dump --port=1432 --dbname=keycloak --file=/tmp/keycloak-dump.sql
	docker compose cp refae_dbkeycloak:/tmp/keycloak-dump.sql ./dev/keycloak-dump.sql

import-keycloak-db:
	docker compose down
	sudo rm $(CWD)/data/keycloak -R
	docker compose --env-file .env.docker.local  up -d refae_dbkeycloak
	sleep 5
	docker compose cp $(CWD)/dev/keycloak-dump.sql refae_dbkeycloak:/tmp/keycloak-dump.sql
	docker compose exec --user=postgres -it refae_dbkeycloak psql -v ON_ERROR_STOP=1 --port=1432 -d keycloak -f /tmp/keycloak-dump.sql


.DEFAULT_GOAL := help
.PHONY: help

help:
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST)

hash-dir:
	bin/console app:hash-dir --recursive assets/ bin/ config/ migrations/ public/ src/ templates/ translations/ vendor/ > resources/hashes.txt
