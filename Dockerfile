FROM ubuntu:24.04 AS baseimage

ARG APACHE_UID=1000

ENV TIMEZONE=Europe/Paris
ENV APACHE_RUN_USER=refae
ENV APACHE_RUN_GROUP=refae
ENV DEBIAN_FRONTEND=noninteractive
ENV INITIALIZATION_DIR=/data-entrypoint
ENV APACHE_LOG_DIR=/data-logs
ENV APACHE_PID_FILE=/data-run/apache2.pid
ENV APACHE_RUN_DIR=/data-run

COPY ./build/apache-security.conf /etc/apache2/conf-available/
COPY ./build/refae.conf /etc/apache2/sites-available/

RUN echo $TIMEZONE > /etc/timezone \
    && apt-get update \
    && apt dist-upgrade -y \
    && apt install -y --allow-unauthenticated \
      sudo vim curl wget zip unzip rar git \
      apache2 libapache2-mod-php \
      software-properties-common locales \
      php php-curl php-intl php-mbstring php-xml php-zip php-apcu php-xdebug php-pgsql php-ldap \
    && sed -i -e 's/# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/' /etc/locale.gen \
    && sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
    && dpkg-reconfigure --frontend=noninteractive locales \
    && deluser ubuntu \
    && useradd --create-home --uid "$APACHE_UID" --user-group "$APACHE_RUN_USER" || true \
    && sed -i "s|APACHE_RUN_USER=.*|APACHE_RUN_USER=$APACHE_RUN_USER|g" /etc/apache2/envvars \
    && sed -i "s|APACHE_RUN_GROUP=.*|APACHE_RUN_GROUP=$APACHE_RUN_GROUP|g" /etc/apache2/envvars \
    && echo 'ServerName refae' >> /etc/apache2/apache2.conf \
    && mkdir -p /etc/apache2/ssl \
    && openssl req -new -x509 -sha256 -newkey rsa:2048 -nodes -keyout /etc/apache2/ssl/server.key -out /etc/apache2/ssl/server.crt -days 365 -subj "/C=FR/ST=FR/L=Herault/O=AS/OU=PRA/CN=localhost" \
    && a2enmod headers proxy proxy_http rewrite ssl \
    && a2enconf apache-security.conf \
    && a2dissite 000-default.conf && a2ensite refae.conf \
    && mkdir -p /var/www/refae && chown $APACHE_RUN_USER /var/www/refae -R \
    && mkdir -p "$INITIALIZATION_DIR" "$APACHE_LOG_DIR" "$APACHE_RUN_DIR" \
    && cp /etc/apache2/sites-available/refae.conf "$INITIALIZATION_DIR"/ \
    && cp /etc/apache2/ssl/* "$INITIALIZATION_DIR"/ \
    && sed -i "s|APACHE_PID_FILE=.*|APACHE_PID_FILE=$APACHE_PID_FILE|g" /etc/apache2/envvars \
    && sed -i "s|APACHE_RUN_DIR=.*|APACHE_RUN_DIR=$APACHE_RUN_DIR|g" /etc/apache2/envvars \
    && sed -i "s|APACHE_LOG_DIR=.*|APACHE_LOG_DIR=$APACHE_LOG_DIR|g" /etc/apache2/envvars \
    && chown -R $APACHE_RUN_USER /etc/apache2/ "$INITIALIZATION_DIR" "$APACHE_LOG_DIR" "$APACHE_RUN_DIR"

RUN echo "xdebug.max_nesting_level = 1000" >> /etc/php/8.3/apache2/conf.d/20-xdebug.ini

COPY ./build/initialization.sh "$INITIALIZATION_DIR"/

RUN mkdir -p /var/www/refae-volume

CMD ["/data-entrypoint/initialization.sh"]

#-------------------------------------------------------------------------------

FROM baseimage AS composer-install

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer
COPY --chown=$APACHE_RUN_USER composer.json /var/www/refae/composer.json
COPY --chown=$APACHE_RUN_USER composer.lock /var/www/refae/composer.lock

USER $APACHE_RUN_USER
WORKDIR /var/www/refae

COPY --chown=$APACHE_RUN_USER . /var/www/refae/

RUN composer install --optimize-autoloader

#-------------------------------------------------------------------------------
FROM node:20 AS watcher

ARG APACHE_UID=1000

COPY --chown=node package.json /var/www/refae/package.json
COPY --chown=node package-lock.json /var/www/refae/package-lock.json
ENV PATH /var/www/refae/node_modules/.bin:$PATH
WORKDIR /var/www/refae

RUN useradd --create-home --uid "$APACHE_UID" --user-group "$APACHE_RUN_USER" || true \
    && npm install

RUN mkdir -p /.npm && chown "$APACHE_UID" /.npm

COPY --chown=node . /var/www/refae/
COPY --from=composer-install --chown=node /var/www/refae/vendor/ /var/www/refae/vendor/

RUN npm run build

EXPOSE 80
CMD [ "npm", "run", "watch"]

#-------------------------------------------------------------------------------
FROM quay.io/keycloak/keycloak:22.0.0 AS keycloak_builder

ENV KC_DB=postgres
ENV KC_FEATURES="token-exchange,scripts,preview"

WORKDIR /opt/keycloak

RUN /opt/keycloak/bin/kc.sh build --cache=ispn --health-enabled=true --metrics-enabled=true

#-------------------------------------------------------------------------------
FROM quay.io/keycloak/keycloak:22.0.0 AS keycloak

LABEL image.version=22.0.0

COPY --from=keycloak_builder /opt/keycloak/ /opt/keycloak/

# https://github.com/keycloak/keycloak/issues/19185#issuecomment-1480763024
USER root
RUN sed -i '/disabledAlgorithms/ s/ SHA1,//' /etc/crypto-policies/back-ends/java.config
USER keycloak

RUN /opt/keycloak/bin/kc.sh show-config

ENTRYPOINT ["/opt/keycloak/bin/kc.sh"]

#-------------------------------------------------------------------------------

FROM baseimage AS final

USER $APACHE_RUN_USER
WORKDIR /var/www/refae

COPY --chown=$APACHE_RUN_USER . /var/www/refae/
COPY --from=composer-install --chown=$APACHE_RUN_USER /var/www/refae/vendor/ /var/www/refae/vendor/
COPY --from=composer-install --chown=$APACHE_RUN_USER /var/www/refae/public/bundles/ /var/www/refae/public/bundles/
COPY --from=watcher /var/www/refae/public/build /var/www/refae/public/build

RUN mkdir -p resources/ &&  \
    /var/www/refae/bin/console app:hash-dir \
    config/ \
    migrations/ \
    public/ \
    src/ \
    templates/ \
    translations/ \
    vendor/  \
    \-r --output=resources/hashes.txt

# empêche de mettre en cache les commandes suivantes
ADD "https://www.random.org/cgi-bin/randbyte?nbytes=10&format=h" /tmp/skipcache
RUN date > /var/www/refae/.image-dev

CMD ["/usr/sbin/apachectl", "-D FOREGROUND"]
