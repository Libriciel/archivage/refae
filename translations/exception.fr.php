<?php

return [
    "Continue" => "Continuer",
    "Switching Protocols" => "Changement de protocole en cours",
    "Processing" => "Traitement en cours",
    "Early Hints" => "Indications précoces",
    "OK" => "OK",
    "Created" => "Créé",
    "Accepted" => "Accepté",
    "Non-Authoritative Information" => "Information non autorisée",
    "No Content" => "Pas de contenu",
    "Reset Content" => "Contenu réinitialisé",
    "Partial Content" => "Contenu partiel",
    "Multi-Status" => "Multi-Statut",
    "Already Reported" => "Déjà signalé",
    "IM Used" => "IM utilisé",
    "Multiple Choices" => "Choix multiples",
    "Moved Permanently" => "Déplacé définitivement",
    "Found" => "Trouvé",
    "See Other" => "Voir autre",
    "Not Modified" => "Non modifié",
    "Use Proxy" => "Utiliser un mandataire",
    "Temporary Redirect" => "Redirection temporaire",
    "Permanent Redirect" => "Redirection permanente",
    "Bad Request" => "Requête incorrecte",
    "Unauthorized" => "Non autorisé",
    "Payment Required" => "Paiement requis",
    "Forbidden" => "Interdit",
    "Not Found" => "Non trouvé",
    "Method Not Allowed" => "Méthode non autorisée",
    "Not Acceptable" => "Non acceptable",
    "Proxy Authentication Required" => "Authentification du mandataire requise",
    "Request Timeout" => "Temps d’attente de la requête écoulé",
    "Conflict" => "Conflit",
    "Gone" => "Disparu",
    "Length Required" => "Longueur requise",
    "Precondition Failed" => "Échec de la condition préalable",
    "Content Too Large" => "Contenu trop volumineux",
    "URI Too Long" => "URI trop long",
    "Unsupported Media Type" => "Type de média non pris en charge",
    "Range Not Satisfiable" => "Plage de requête non satisfaisante",
    "Expectation Failed" => "Échec de l'attente",
    "I'm a teapot" => "Je suis une théière",
    "Misdirected Request" => "Requête mal dirigée",
    "Unprocessable Content" => "Contenu non traitable",
    "Locked" => "Verrouillé",
    "Failed Dependency" => "Dépendance échouée",
    "Too Early" => "Trop tôt",
    "Upgrade Required" => "Mise à niveau requise",
    "Precondition Required" => "Précondition requise",
    "Too Many Requests" => "Trop de requêtes",
    "Request Header Fields Too Large" => "Champs d'en-tête de requête trop grands",
    "Unavailable For Legal Reasons" => "Indisponible pour des raisons légales",
    "Internal Server Error" => "Erreur interne du serveur",
    "Not Implemented" => "Non implémenté",
    "Bad Gateway" => "Mauvaise passerelle",
    "Service Unavailable" => "Service indisponible",
    "Gateway Timeout" => "Temps d'attente de la passerelle écoulé",
    "HTTP Version Not Supported" => "Version HTTP non prise en charge",
    "Variant Also Negotiates" => "La variante négocie également",
    "Insufficient Storage" => "Stockage insuffisant",
    "Loop Detected" => "Détection de boucle",
    "Not Extended" => "Non étendu",
    "Network Authentication Required" => "Authentification réseau requise",
    "desc-100" => "Le serveur a reçu la requête, mais elle nécessite plus d'informations avant de pouvoir être traitée.",
    "desc-101" => "Le serveur accepte le changement de protocole demandé par le client.",
    "desc-200" => "La requête a été traitée avec succès.",
    "desc-201" => "La requête a été traitée avec succès et une nouvelle ressource a été créée.",
    "desc-202" => "La requête a été acceptée pour traitement, mais le traitement n'est pas encore terminé.",
    "desc-204" => "La requête a été traitée avec succès, mais il n'y a pas de contenu à renvoyer.",
    "desc-206" => "La requête n'a renvoyé qu'une partie des données en raison de l'utilisation de la directive Range dans l'en-tête de la requête.",
    "desc-300" => "La réponse contient une liste des différentes représentations de la ressource ciblée.",
    "desc-301" => "La ressource demandée a été définitivement déplacée vers une autre URI.",
    "desc-302" => "La ressource demandée a été temporairement déplacée vers une autre URI.",
    "desc-303" => "Le client doit effectuer une nouvelle requête en utilisant l'URI fournie.",
    "desc-304" => "La ressource n'a pas été modifiée depuis la dernière requête.",
    "desc-307" => "La redirection est nécessaire pour accéder à la ressource.",
    "desc-400" => "La syntaxe de la requête est erronée ou la requête est incompréhensible.",
    "desc-401" => "Le client doit s'authentifier pour accéder à la ressource.",
    "desc-403" => "Le serveur refuse de traiter la requête en raison de restrictions d'accès.",
    "desc-404" => "La ressource demandée est introuvable sur le serveur.",
    "desc-405" => "La méthode de la requête n'est pas autorisée pour la ressource ciblée.",
    "desc-406" => "La ressource demandée n'est pas disponible dans un format acceptable pour le client.",
    "desc-408" => "Le délai d'attente pour la requête a expiré.",
    "desc-409" => "La requête ne peut pas être traitée en raison d'un conflit avec l'état actuel de la ressource ciblée.",
    "desc-410" => "La ressource demandée n'est plus disponible et aucune redirection n'est connue.",
    "desc-411" => "La longueur requise de la requête a été refusée par le serveur.",
    "desc-413" => "La taille de la requête dépasse la capacité de traitement du serveur.",
    "desc-414" => "L'URI de la requête est trop longue pour être traitée par le serveur.",
    "desc-415" => "Le format de la requête n'est pas pris en charge pour la ressource ciblée.",
    "desc-416" => "Le serveur ne peut pas satisfaire la plage de la requête.",
    "desc-417" => "Le serveur n'est pas capable de répondre aux attentes indiquées dans l'en-tête de la requête.",
    "desc-418" => "Je suis une théière.",
    "desc-422" => "La requête est bien formée, mais semantiquement incorrecte.",
    "desc-423" => "La ressource est verrouillée et actuellement non disponible.",
    "desc-424" => "La requête a échoué en raison d'une dépendance insatisfaite.",
    "desc-425" => "Le client doit se connecter pour accéder à la ressource.",
    "desc-426" => "Le serveur refuse de traiter la requête car la version de protocole est obsolète.",
    "desc-428" => "Le serveur a besoin que la requête soit conditionnelle.",
    "desc-429" => "Le client a envoyé trop de requêtes dans un délai donné.",
    "desc-431" => "La taille de la requête dépasse la limite autorisée par le serveur.",
    "desc-451" => "La ressource est inaccessible pour des raisons légales.",
    "desc-500" => "Une erreur interne du serveur est survenue.",
    "desc-501" => "Le serveur ne prend pas en charge la fonctionnalité requise pour traiter la requête.",
    "desc-502" => "Le serveur a reçu une réponse invalide depuis un serveur en amont.",
    "desc-503" => "Le serveur est temporairement incapable de traiter la requête en raison d'une surcharge ou d'une maintenance.",
    "desc-504" => "Le serveur a dépassé le délai d'attente en attendant une réponse de la part d'un serveur en amont.",
    "desc-505" => "La version HTTP utilisée dans la requête n'est pas supportée par le serveur.",
    "desc-506" => "La négociation de contenu pour la ressource a échoué.",
    "desc-507" => "L'espace de stockage du serveur est insuffisant pour répondre à la demande.",
    "desc-508" => "La boucle de détection a été détectée et interrompue.",
    "desc-510" => "Le client doit passer par un serveur mandataire qui n'est pas pris en charge par le serveur.",
    "desc-511" => "Le client doit s'authentifier pour accéder à la ressource.",
];
