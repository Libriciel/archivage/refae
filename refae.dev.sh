#!/bin/bash

source refae

################################################################################
# Définition des commandes (appelé tout en bas dans select_function())
################################################################################

function build-dev() {
  docker build . -t refae:dev
}

function cache-clear-test() {
  docker compose --env-file="$ENV_FILE" exec -it refae_web php bin/console c:c --env=test
}

function composer() {
  docker run composer:latest cat /usr/bin/composer > /tmp/composer
  docker compose --env-file="$ENV_FILE" cp /tmp/composer refae_web:/var/www/refae/composer
  docker compose --env-file="$ENV_FILE" exec -it refae_web php /var/www/refae/composer "${ARGV[@]}"
  docker compose --env-file="$ENV_FILE" exec -t refae_web rm -f /var/www/refae/composer
}

function cs-check() {
  docker compose --env-file="$ENV_FILE" exec -it -e XDEBUG_MODE=off refae_web php /var/www/refae/composer cs-check "${ARGS[0]}"
}

function cs-fix() {
  docker compose --env-file="$ENV_FILE" exec -it -e XDEBUG_MODE=off refae_web php /var/www/refae/composer cs-fix "${ARGS[0]}"
}
 ## drop la base de donnée
function drop() {
  docker compose --env-file="$ENV_FILE" stop refae_async && \
  docker compose --env-file="$ENV_FILE" exec -it refae_web php bin/console doc:data:drop --force && \
  docker compose --env-file="$ENV_FILE" exec -it refae_web php bin/console doc:data:create
}

function export-keycloak-db() {
  docker compose --env-file="$ENV_FILE" exec --user=postgres -it refae_dbkeycloak pg_dump --port=1432 --dbname=keycloak --file=/tmp/keycloak-dump.sql
  docker compose --env-file="$ENV_FILE" cp refae_dbkeycloak:/tmp/keycloak-dump.sql ./dev/keycloak-dump.sql
}

function import-keycloak-db() {
  docker compose --env-file="$ENV_FILE" down
  sudo rm "$(CWD)"/data/keycloak -R
  docker compose --env-file="$ENV_FILE"  up -d refae_dbkeycloak
  sleep 5
  docker compose --env-file="$ENV_FILE" cp "$(CWD)"/dev/keycloak-dump.sql refae_dbkeycloak:/tmp/keycloak-dump.sql
  docker compose --env-file="$ENV_FILE" exec --user=postgres -it refae_dbkeycloak psql -v ON_ERROR_STOP=1 --port=1432 -d keycloak -f /tmp/keycloak-dump.sql
}

function install-composer() {
  docker run composer:latest cat /usr/bin/composer > /tmp/composer && \
  docker compose --env-file="$ENV_FILE" cp /tmp/composer refae_web:/var/www/refae/composer
}

function migration_diff() {
  docker compose --env-file="$ENV_FILE" exec -it refae_web php bin/console doc:mig:diff
}

function migration_migrate() {
  docker compose --env-file="$ENV_FILE" exec -it refae_web php bin/console doc:mig:mig -n && \
  docker compose --env-file="$ENV_FILE" exec -it refae_web php bin/console doc:fix:load --group recette -n
}
 ## bash sur le docker npm
function npm() {
  docker compose --env-file="$ENV_FILE" exec -it refae_watcher bash
}
 ## vide le cache de test et lance paratest
function paratest() {
  docker compose --env-file="$ENV_FILE" exec -it refae_web php bin/console c:c --env=test && \
  docker compose --env-file="$ENV_FILE" exec -it -e XDEBUG_MODE=off refae_web php vendor/bin/paratest "${ARGS[0]}"
}
 ## vide le cache de test et lance phpunit
function phpunit() {
  docker compose --env-file="$ENV_FILE" exec -it refae_web php bin/console c:c --env=test && \
  docker compose --env-file="$ENV_FILE" exec -it -e XDEBUG_MODE=off refae_web php bin/phpunit "${ARGS[0]}"
}

function reset() {
  docker compose --env-file="$ENV_FILE" restart refae_db && \
  docker compose --env-file="$ENV_FILE" up -d && \
  docker compose --env-file="$ENV_FILE" stop refae_async && \
  docker compose --env-file="$ENV_FILE" exec -it refae_web php bin/console app:meilisearch:purge && \
  docker compose --env-file="$ENV_FILE" exec -it refae_web php bin/console doc:data:drop --force && \
  docker compose --env-file="$ENV_FILE" exec -it refae_web php bin/console doc:data:create && \
  docker compose --env-file="$ENV_FILE" exec -it refae_web php bin/console doc:mig:mig -n && \
  docker compose --env-file="$ENV_FILE" start refae_async && \
  docker compose --env-file="$ENV_FILE" exec -it refae_web php bin/console doc:fix:load --group recette -n
}

function run() {
  docker compose --env-file="$ENV_FILE" run -it refae_web bash
}

function run-init() {
  docker compose --env-file="$ENV_FILE" run -it --build refae_init bash
}
 ## Définition de la fonction test
function test() {
  echo 'function de test'
  for option in "${!OPTIONS[@]}"; do
      echo "[$option => ${OPTIONS[$option]}]"
  done
  for arg in "${ARGS[@]}"; do
      echo "$arg"
  done
}

function up-build() {
  docker compose --env-file="$ENV_FILE" up -d --build
}


################################################################################
# Définition des commandes
################################################################################

# Définir ici les nouvelles commandes
# BEGIN COMMAND EXTRACTOR
function select_function() {
  case $1 in
    build-dev)
      build-dev
      ;;
    cache-clear-test)
      cache-clear-test
      ;;
    composer)
      composer
      ;;
    cs-check)
      cs-check
      ;;
    cs-fix)
      cs-fix
      ;;
    drop)
      drop
      ;;
    export-keycloak-db)
      export-keycloak-db
      ;;
    import-keycloak-db)
      import-keycloak-db
      ;;
    install-composer)
      install-composer
      ;;
    migration_diff)
      migration_diff
      ;;
    migration_migrate)
      migration_migrate
      ;;
    npm)
      npm
      ;;
    paratest)
      paratest
      ;;
    phpunit)
      phpunit
      ;;
    reset)
      reset
      ;;
    run)
      run
      ;;
    run-init)
      run-init
      ;;
    up-build)
      up-build
      ;;
    *)
      select_function_refae "$1"
      ;;
  esac
}

select_function "$ACTION"
