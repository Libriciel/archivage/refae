#!/usr/bin/php -q
<?php

$lockPath = __DIR__ . '/composer.lock';
$composerLock = json_decode(file_get_contents($lockPath), true);

foreach (glob(__DIR__.'/vendor/libriciel/*') as $dir) {
    chdir($dir);
    $hash = exec('git rev-parse HEAD');
    $name = 'libriciel/' . basename($dir);
    foreach ($composerLock['packages'] as &$package) {
        if ($package['name'] === $name) {
            $package['source']['reference'] = $hash;
            break;
        }
    }
}
$json = json_encode(
    $composerLock,
    JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
);

file_put_contents($lockPath, $json);
echo "composer.lock updated\n";
