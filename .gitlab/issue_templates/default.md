### Informations
- Version du logiciel : 2.0.x
- Constaté sur l'instance : (supprimer les non pertinents)
  - recette
  - formation
  - dev
  - client - nom du client : <nom>
- url : https://...
- tickets : (supprimer cette mention si non applicable)
  - https://otrs.libriciel.fr/otrs/index.pl?Action=AgentTicketZoom;TicketID=***


### Description du bug
Brève explication du bug, fournir si possible des screens, fichiers et logs.


### Ce que vous auriez souhaité
Expliquez, si nécessaire, le comportement attendu.


### Comment reproduire le bug
Décrire si possible, le déroulement d'opérations qui mènent au bug.



/label ~"Cible:Patch" ~"Type:Bug" 
