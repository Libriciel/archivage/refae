#!/bin/bash

declare -A OPTIONS
declare -a ARGS
declare -a ARGV

SCRIPT_FILE=$(basename "$0")
ACTION="$1"
shift 1
#
ARGV=("$@")

# Sépare les arguments des options (options sous la forme --<key> <value>)
while [[ $# -gt 0 ]]; do
    key="$1"
    if [[ $key == --* ]]; then
        option="${key#--}"
        value="$2"
        # shellcheck disable=SC2034
        OPTIONS[$option]="$value"
        shift
    else
        ARGS+=("$1")
    fi
    shift
done

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$DIR" || exit

if [ -f .env.docker.local ]; then
  source .env.docker.local
  ENV_FILE=".env.docker.local"
else
  source .env
  ENV_FILE=".env"
fi

################################################################################
# Définition des commandes (appelé tout en bas dans select_function_refae())
################################################################################

 ## bash sur le docker web
function bash() {
	docker compose --env-file="$ENV_FILE" exec -it refae_web bash "${ARGV[@]}"
}

function bash-root() {
	docker compose --env-file="$ENV_FILE" exec --user=root -it refae_web bash "${ARGV[@]}"
}

function cache-clear() {
  docker compose --env-file="$ENV_FILE" exec -it refae_web php bin/console c:c
}

function certbot-hook() {
  if [ ! -d "/etc/letsencrypt/live/" ] || [ -z "$(ls -A /etc/letsencrypt/live/)" ]; then
      echo "Vous devez créer le certificat avant de faire le hook: certbot certonly --standalone"
      exit 1
  fi

  if [ -z "$DATA_DIR" ]; then
    DATA_DIR="./data"
  fi
  DATA_DIR=$(realpath "$DATA_DIR")

    echo "
#!/bin/bash

cd \"$DIR\"
docker compose down

" > /etc/letsencrypt/renewal-hooks/pre/pre-refae.sh

    echo "
#!/bin/bash

cp \"\$RENEWED_LINEAGE/cert.pem\" \"$DATA_DIR/ssl/server.crt\"
cp \"\$RENEWED_LINEAGE/privkey.pem\" \"$DATA_DIR/ssl/server.key\"

cd \"$DIR\"
docker compose down
docker compose up -d

" > /etc/letsencrypt/renewal-hooks/deploy/renew-refae.sh

  sudo chmod +x /etc/letsencrypt/renewal-hooks/deploy/renew-refae.sh
  echo "written /etc/letsencrypt/renewal-hooks/deploy/renew-refae.sh"
}

## prefixe les services dans les commandes docker compose
function compose() {
    # Récupérer la liste des services définis dans docker-compose.yml
    local services
    services=$(docker compose --env-file="$ENV_FILE" config --services)

    local NEW_ARGS=()
    local SERVICE_FOUND=false

    for arg in "${ARGV[@]}"; do
        # Boucle pour vérifier si l'argument correspond à un service
        local is_service=false
        for service in $services; do
            if [[ "refae_$arg" == "$service" ]]; then
                is_service=true
                break
            fi
        done

        # Si l'argument correspond à un service, ajouter le préfixe "asalae_"
        if [[ "$is_service" == true ]] && [ "$SERVICE_FOUND" = false ]; then
            NEW_ARGS+=("refae_$arg")
            SERVICE_FOUND=true
        else
            # Sinon, ajouter l'argument tel quel
            NEW_ARGS+=("$arg")
        fi
    done

    # Exécuter la commande docker compose avec les nouveaux arguments
    docker compose --env-file="$ENV_FILE" "${NEW_ARGS[@]}"
}

function console() {
  docker compose --env-file="$ENV_FILE" exec -it refae_web bin/console "${ARGV[@]}"
}

function copy-certificate() {
  if [ "${#ARGV[@]}" -lt 1 ]; then
    echo "Usage: ./refae copy-certificate /etc/letsencrypt/live/<domain.refae.fr>/" >&2
    exit 1
  fi

  if [ ! -d "${ARGV[0]}" ]; then
    echo "Le dossier ${ARGV[0]} n'existe pas"
    exit 1
  fi

  LETSENCRYPT_DIR=$(realpath "${ARGV[0]}")
  if [ ! -f "${LETSENCRYPT_DIR}/fullchain.pem" ] || [ ! -f "${LETSENCRYPT_DIR}/privkey.pem" ]; then
    echo "Certificat ou clé privée manquant."
    exit 1
  fi

  down

  cp "${LETSENCRYPT_DIR}/fullchain.pem" "$DATA_DIR/ssl/server.crt" || exit 1
  cp "${LETSENCRYPT_DIR}/privkey.pem" "$DATA_DIR/ssl/server.key" || exit 1

  up
}

 ## docker compose down
function down() {
	docker compose --env-file="$ENV_FILE" down
}

function dump() {
  POSTGRES_USER=${POSTGRES_USER:-refae}
  POSTGRES_DB=${POSTGRES_DB:-refae}
  POSTGRES_DBPORT=${POSTGRES_DBPORT:-5432}
  dumpname=dbdump_"$(date +%Y-%m-%d_%H_%M_%S)".sql

  docker compose --env-file="$ENV_FILE" up -d refae_db
  docker compose --env-file="$ENV_FILE" exec -t refae_db pg_dump --blobs \
    -U "$POSTGRES_USER" \
    -d "$POSTGRES_DB" \
    -p "$POSTGRES_DBPORT" \
     | tee "$dumpname" > /dev/null
  if [ -s "$dumpname" ]; then
    echo "$dumpname"
  else
    rm -f "$dumpname"
  fi
}

function help() {
  CURFILE=$(basename "$0")
  echo Liste des commandes:
  linenum=$(grep -n "^# BEGIN COMMAND EXTRACTOR$" "$CURFILE" | head -n 1 | cut -d ':' -f 1)
  sed -n "$linenum,\$p" "$CURFILE" | grep -B1 ";;" | grep -v ";;" | grep -v '\-\-' | grep -v echo | sort
}

function import-refae1() {
  options=""
  for ((i=2; i<${#ARGV[@]}; i++)); do
      options="$options ${ARGV[$i]}"
  done

  # shellcheck disable=SC2086
	docker compose --env-file="$ENV_FILE" cp "${ARGS[1]}" refae_web:/var/www/refae/tmp/import-refae1.json \
	&& docker compose --env-file="$ENV_FILE" exec refae_web bin/console app:import-refae-1-1 "${ARGS[0]}" /var/www/refae/tmp/import-refae1.json $options \
	&& docker compose --env-file="$ENV_FILE" exec refae_web rm /var/www/refae/tmp/import-refae1.json
}

function init-env() {
  if [ -f ".env" ]; then
      echo "Un fichier .env existe déjà"
      exit 1
  fi
  cp .env.dist .env || exit 1
  MERCURE=$(head /dev/urandom | LC_ALL=C tr -dc 'A-Za-z0-9!@^*(_+\-)=' | head -c 64)
  MEILISEARCH=$(head /dev/urandom | LC_ALL=C tr -dc 'A-Za-z0-9!@^*(_+\-)=' | head -c 64)
  POSTGRES=$(head /dev/urandom | LC_ALL=C tr -dc 'A-Za-z0-9!@^*(_+\-)=' | head -c 64)
  sed -i'' -E \
      -e "s/^MERCURE_JWT_SECRET=.*/MERCURE_JWT_SECRET=\"$MERCURE\"/" \
      -e "s/^MEILI_MASTER_KEY=.*/MEILI_MASTER_KEY=\"$MEILISEARCH\"/" \
      -e "s/^POSTGRES_PASSWORD=.*/POSTGRES_PASSWORD=\"$POSTGRES\"/" \
      ".env"
}

function install-autocomplete() {
  AUTOCOMPLETE_SCRIPT=$(realpath "_autocomplete_refae.sh")
  BASHRC_FILE="$HOME/.bashrc"

  # Vérification de l'existence du script d'autocomplétion
  if [ ! -f "$AUTOCOMPLETE_SCRIPT" ]; then
      echo "Le script d'autocomplétion n'est pas trouvé : $AUTOCOMPLETE_SCRIPT"
      exit 1
  fi

  # Vérification si l'autocomplétion est déjà configurée dans ~/.bashrc
  if ! grep -q "$AUTOCOMPLETE_SCRIPT" ~/.bashrc; then
      last_char=$(tail -c 1 "$BASHRC_FILE")
      if [[ "$last_char" != $'\n' ]]; then
          # Ajout d'un retour à la ligne si nécessaire
          echo "" >> "$BASHRC_FILE"
      fi
      # Ajout de la commande pour charger l'autocomplétion dans ~/.bashrc
      echo -e "source $AUTOCOMPLETE_SCRIPT\n" >> ~/.bashrc
      echo "L'autocomplétion a été ajoutée à ~/.bashrc avec succès !"
      echo "veuillez taper la commande suivante pour activer immédiatement l'autocompletion:"
      echo "source ~/.bashrc"
  else
      echo "L'autocomplétion est déjà configurée dans ~/.bashrc."
  fi
}
 ## docker compose logs
function log() {
	docker compose --env-file="$ENV_FILE" logs -f
}

function psql() {
  POSTGRES_USER=${POSTGRES_USER:-refae}
  POSTGRES_DB=${POSTGRES_DB:-refae}
  POSTGRES_DBPORT=${POSTGRES_DBPORT:-5432}
  dumpname=dbdump_"$(date +%Y-%m-%d_%H_%M_%S)".sql

  docker compose --env-file="$ENV_FILE" exec -ti refae_db psql \
    -U "$POSTGRES_USER" \
    -d "$POSTGRES_DB" \
    -p "$POSTGRES_DBPORT" \
    "${ARGV[@]}"
}

function reload() {
	docker compose --env-file="$ENV_FILE" exec -it refae_web /usr/sbin/apachectl -k graceful
}

function restart() {
  docker compose --env-file="$ENV_FILE" restart
}

function restore() {
  if [ "${#ARGV[@]}" -lt 1 ]; then
    echo "Usage: ./refae restore dbdump_<datetime>.sql" >&2
    exit 1
  fi

  sudo docker compose --env-file="$ENV_FILE" down --remove-orphans || exit 1

  if docker compose --env-file="$ENV_FILE" ps refae_db | grep -q refae_db; then
    echo "failed to docker compose down"
    exit 1
  fi

  if [ -z "$DATA_DIR" ]; then
      DATA_DIR="./data"
  fi
  DATA_DIR=$(realpath "$DATA_DIR")
  SUFFIX=$(date +%Y-%m-%d_%H_%M_%S)
  POSTGRES_DIR="${DATA_DIR}/postgres"
  POSTGRES_USER=${POSTGRES_USER:-refae}
  POSTGRES_DB=${POSTGRES_DB:-refae}
  POSTGRES_DBPORT=${POSTGRES_DBPORT:-5432}

  echo "${ARGV[1]}"
  if [ -d "$POSTGRES_DIR" ]
  then
    if [ "${ARGV[1]}" = "delete" ]
    then
      sudo rm -r "$POSTGRES_DIR" || exit 1
    else
      sudo mv "$POSTGRES_DIR" "${POSTGRES_DIR}_$SUFFIX" || exit 1
      echo "backup created ${POSTGRES_DIR}_$SUFFIX"
    fi
  fi

  docker compose --env-file="$ENV_FILE" up -d --wait refae_db || exit 1
  docker compose --env-file="$ENV_FILE" cp "${ARGV[0]}" refae_db:/db-dump/dump.sql || exit 1
  docker compose --env-file="$ENV_FILE" exec -i refae_db psql -v ON_ERROR_STOP=1 \
      -U "$POSTGRES_USER" \
      -p "$POSTGRES_DBPORT" \
      -f /db-dump/dump.sql || exit 1
  docker compose --env-file="$ENV_FILE" exec -i refae_db rm /db-dump/dump.sql || exit 1
  docker compose --env-file="$ENV_FILE" up -d --wait

  echo "dump appliqué avec succès"
  if [ "${ARGV[1]}" != "delete" ]
  then
    echo "vous pouvez vérifier le bon fonctionnement de l'application puis supprimer ${POSTGRES_DIR}_$SUFFIX"
  fi
}

 ## docker compose up
function up() {
  docker compose --env-file="$ENV_FILE" up -d
}

function version() {
  docker compose --env-file="$ENV_FILE" exec -it refae_web php -r '$v=file("VERSION.txt");echo trim(array_pop($v)).PHP_EOL;'
}


################################################################################
# Définition des commandes
################################################################################

# Définir ici les nouvelles commandes
# BEGIN COMMAND EXTRACTOR
function select_function_refae() {
  case $1 in
    bash)
      bash
      ;;
    bash-root)
      bash-root
      ;;
    cache-clear)
      cache-clear
      ;;
    certbot-hook)
      certbot-hook
      ;;
    compose)
      compose
      ;;
    console)
      console
      ;;
    copy-certificate)
      copy-certificate
      ;;
    down)
      down
      ;;
    dump)
      dump
      ;;
    help)
      help
      ;;
    import-refae1)
      import-refae1
      ;;
    init-env)
      init-env
      ;;
    install-autocomplete)
      install-autocomplete
      ;;
    log)
      log
      ;;
    psql)
      psql
      ;;
    reload)
      reload
      ;;
    restart)
      restart
      ;;
    restore)
      restore
      ;;
    up)
      up
      ;;
    version)
      version
      ;;
    *)
      echo "Fonction non définie pour l'argument passé : $1"
      ;;
  esac
}

# Vérification si au moins un argument est passé
if [[ -z "$ACTION" ]]; then
  help
  exit 1
fi

if [ "$SCRIPT_FILE" = "refae" ]; then
    select_function_refae "$ACTION"
fi
