<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
    Locale::setDefault('fr-FR');
    $containerConfigurator->extension('framework', [
        'default_locale' => 'fr',
        'translator' => [
            'default_path' => '%kernel.project_dir%/translations',
            'fallbacks' => [
                'fr',
            ],
        ],
    ]);
};
