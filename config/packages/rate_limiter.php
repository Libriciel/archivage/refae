<?php

// config/packages/rate_limiter.php
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Config\FrameworkConfig;

return static function (FrameworkConfig $framework, ContainerConfigurator $containerConfigurator): void {
    $framework->rateLimiter()
        ->limiter('anonymous_api')
        ->policy('token_bucket')
        ->limit((int)($_ENV['RATE_LIMITER_ANONYMOUS_LIMIT'] ?? 5000))
        ->rate()
        ->interval($_ENV['RATE_LIMITER_ANONYMOUS_INTERVAL'] ?? "15 minutes")
        ->amount((int)($_ENV['RATE_LIMITER_ANONYMOUS_AMOUNT'] ?? 500))
    ;

    $framework->rateLimiter()
        ->limiter('authenticated_api')
        ->policy('token_bucket')
        ->limit((int)($_ENV['RATE_LIMITER_AUTHENTICATED_LIMIT'] ?? 5000))
        ->rate()
        ->interval($_ENV['RATE_LIMITER_AUTHENTICATED_INTERVAL'] ?? "15 minutes")
        ->amount((int)($_ENV['RATE_LIMITER_AUTHENTICATED_AMOUNT'] ?? 500))
    ;

    if ($containerConfigurator->env() === 'test') {
        $limit = 1000000;
        $interval = '1 minute';
        $amount = 1000000;
    } else {
        $limit = (int)($_ENV['RATE_LIMITER_LOGIN_LIMIT'] ?? 5);
        $interval = $_ENV['RATE_LIMITER_LOGIN_INTERVAL'] ?? "15 minutes";
        $amount = (int)($_ENV['RATE_LIMITER_LOGIN_AMOUNT'] ?? 5);
    }

    $framework->rateLimiter()
        ->limiter('login')
        ->policy('token_bucket')
        ->limit($limit)
        ->rate()
        ->interval($interval)
        ->amount($amount)
    ;
};
