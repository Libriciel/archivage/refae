<?php

declare(strict_types=1);

use DoctrineExtensions\Query\Postgresql;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $containerConfigurator->extension('doctrine', [
        'dbal' => [
            'url' => '%env(resolve:DATABASE_URL)%',
            'profiling_collect_backtrace' => '%kernel.debug%',
            'use_savepoints' => true,
        ],
        'orm' => [
            'controller_resolver' => [
                'auto_mapping' => true,
            ],
            'auto_generate_proxy_classes' => true,
            'enable_lazy_ghost_objects' => true,
            'report_fields_where_declared' => true,
            'validate_xml_mapping' => true,
            'naming_strategy' => 'doctrine.orm.naming_strategy.underscore_number_aware',
            'auto_mapping' => true,
            'mappings' => [
                'App' => [
                    'is_bundle' => false,
                    'dir' => '%kernel.project_dir%/src/Entity',
                    'prefix' => 'App\Entity',
                    'alias' => 'App',
                ],
                'gedmo_tree' => [
                    'type' => 'attribute',
                    'prefix' => 'Gedmo\Tree\Entity',
                    'dir' => "%kernel.project_dir%/vendor/gedmo/doctrine-extensions/src/Tree/Entity",
                    'is_bundle' => false,
                ],
            ],
            'dql' => [
                'datetime_functions' => [
                    'second' => Postgresql\Second::class,
                    'minute' => Postgresql\Minute::class,
                    'hour' => Postgresql\Hour::class,
                    'day' => Postgresql\Day::class,
                    'month' => Postgresql\Month::class,
                    'year' => Postgresql\Year::class,
                    'date_format' => Postgresql\DateFormat::class,
                    'at_time_zone' => Postgresql\AtTimeZoneFunction::class,
                    'date_part' => Postgresql\DatePart::class,
                    'extract' => Postgresql\ExtractFunction::class,
                    'date_trunc' => Postgresql\DateTrunc::class,
                    'date' => Postgresql\Date::class,
                ],
                'string_functions' => [
                    'str_to_date' => Postgresql\StrToDate::class,
                    'count_filter' => Postgresql\CountFilterFunction::class,
                    'string_agg' => Postgresql\StringAgg::class,
                    'greatest' => Postgresql\Greatest::class,
                    'least' => Postgresql\Least::class,
                    'regexp_replace' => Postgresql\RegexpReplace::class,
                ],
            ],
        ],
    ]);
    if ($containerConfigurator->env() === 'test') {
        $containerConfigurator->extension('doctrine', [
            'dbal' => [
                'dbname_suffix' => '_test%env(default::TEST_TOKEN)%',
                'logging' => false,
                'use_savepoints' => true,
            ],
        ]);
    }
    if ($containerConfigurator->env() === 'prod') {
        $containerConfigurator->extension('doctrine', [
            'orm' => [
                'auto_generate_proxy_classes' => false,
                'proxy_dir' => '%kernel.build_dir%/doctrine/orm/Proxies',
                'query_cache_driver' => [
                    'type' => 'pool',
                    'pool' => 'doctrine.system_cache_pool',
                ],
                'result_cache_driver' => [
                    'type' => 'pool',
                    'pool' => 'doctrine.result_cache_pool',
                ],
            ],
        ]);
        $containerConfigurator->extension('framework', [
            'cache' => [
                'pools' => [
                    'doctrine.result_cache_pool' => [
                        'adapter' => 'cache.app',
                    ],
                    'doctrine.system_cache_pool' => [
                        'adapter' => 'cache.system',
                    ],
                ],
            ],
        ]);
    }
};
