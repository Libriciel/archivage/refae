<?php

declare(strict_types=1);

use App\Service\LogFormatter;
use Psr\Log\LogLevel;
use Symfony\Config\MonologConfig;

return static function (MonologConfig $monolog) {
    $monolog->handler('main')
        ->type('rotating_file')
        ->formatter(LogFormatter::class)
        ->path('%kernel.logs_dir%/%kernel.environment%.log')
        ->level(LogLevel::ERROR)
        ->maxFiles(10);
};
