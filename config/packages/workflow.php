<?php

use App\Service\Manager\WorkflowManager;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Config\FrameworkConfig;

return static function (FrameworkConfig $framework, ContainerConfigurator $containerConfigurator): void {
    $workflowManager = new WorkflowManager($framework);
    $auditTrail = $containerConfigurator->env() === 'dev';

    foreach ($workflowManager::getVersionableEntityClasses() as $class) {
        $workflowManager->initiateWorkflow($class, $auditTrail);
    }
};
