<?php

declare(strict_types=1);

use App\Entity\User;
use App\Security\ApiBearerAuthenticator;
use App\Security\ApiTenantAuthenticator;
use App\Security\OpenidAuthenticator;
use App\Security\TenantAuthenticator;
use App\Security\UserAuthenticator;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Config\SecurityConfig;

return static function (ContainerConfigurator $containerConfigurator, SecurityConfig $security): void {

    $security->passwordHasher(PasswordAuthenticatedUserInterface::class)->algorithm('auto');

    // si aucun voter ne se prononce, on autorise (utile pour les accès /public/{tenant_url}/...)
    $security->accessDecisionManager()
        ->strategy('unanimous')
        ->allowIfAllAbstain(true);

    $security->provider('app_user_provider')
        ->entity([
            'class' => User::class,
            'property' => 'username',
        ]);

    $security->firewall('dev')
        ->pattern('^/(_(profiler|wdt)|css|images|js)/')
        ->security(false);

    $security->firewall('main')
        ->pattern('^(?!/api).*')
        ->provider('app_user_provider')
        ->customAuthenticators([
            OpenidAuthenticator::class,
            TenantAuthenticator::class,
            UserAuthenticator::class,
        ])
    ;

    $security->firewall('api')
        ->pattern('^/api/?.*')
        ->stateless(true)
        ->provider('app_user_provider')
        ->customAuthenticators([
            ApiBearerAuthenticator::class,
            ApiTenantAuthenticator::class,
        ])
    ;

    if ($containerConfigurator->env() === 'test') {
        $security->passwordHasher(PasswordAuthenticatedUserInterface::class)
            ->algorithm('auto')
            ->cost(4)
            ->timeCost(3)
            ->memoryCost(10);
    }
};
