<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Config\TwigConfig;

return static function (
    TwigConfig $twig,
    ContainerConfigurator $containerConfigurator,
): void {
    $top = [
        "Tableau de gestion" => [
            'icon' => 'fa fa-table',
            "Règles documentaires publiées" => [
                'icon' => 'fa fa-table',
                'href' => 'app_document_rule_index_published',
            ],
            "Règles documentaires" => [
                'icon' => 'fa fa-table',
                'href' => 'app_document_rule',
            ],
            '-------------------------------',
            "Catégories" => [
                'icon' => 'fa fa-layer-group',
                'href' => 'app_category',
            ],
        ],
        "Référentiel archivage" => [
            'icon' => 'far fa-object-group',
            "Notices d'autorité publiées" => [
                'icon' => 'fa fa-id-card',
                'href' => 'app_authority_index_published',
            ],
            "Notices d'autorité" => [
                'icon' => 'fa fa-id-card',
                'href' => 'app_authority',
            ],
            '-------------------------------',
            "Accords de versement publiés" => [
                'icon' => 'fa fa-file-text',
                'href' => 'app_agreement_index_published',
            ],
            "Accords de versement" => [
                'icon' => 'fa fa-file-text',
                'href' => 'app_agreement',
            ],
            '-------------------------------',
            "Profils d'archives publiés" => [
                'icon' => 'fa fa-file-code-o',
                'href' => 'app_profile_index_published',
            ],
            "Profils d'archives" => [
                'icon' => 'fa fa-file-code-o',
                'href' => 'app_profile',
            ],
            '-------------------------------',
            "Vocabulaires contrôlés publiés" => [
                'icon' => 'fas fa-book',
                'href' => 'app_vocabulary_index_published',
            ],
            "Vocabulaires contrôlés" => [
                'icon' => 'fas fa-book',
                'href' => 'app_vocabulary',
            ],
            '-------------------------------',
            "Niveaux de service publiés" => [
                'icon' => 'fa fa-handshake-o',
                'href' => 'app_service_level_index_published',
            ],
            "Niveaux de service" => [
                'icon' => 'fa fa-handshake-o',
                'href' => 'app_service_level',
            ],
            '-------------------------------',
            "Règles de gestion publiées" => [
                'icon' => 'fa fa-gavel',
                'href' => 'app_management_rule_index_published',
            ],
            "Règles de gestion" => [
                'icon' => 'fa fa-gavel',
                'href' => 'app_management_rule',
            ],
        ],
        "Administration" => [
            'icon' => 'fa fa-wrench',
            "Utilisateurs" => [
                'icon' => 'fa fa-user-o',
                'href' => 'app_user',
            ],
            "Demandes de création d'utilisateurs" => [
                'icon' => 'fa fa-user-pen',
                'href' => 'app_user_creation_request',
            ],
            '-------------------------------',
            "Groupes d'étiquettes" => [
                'icon' => 'fa fa-tags',
                'href' => 'app_label_group',
            ],
            "Étiquettes" => [
                'icon' => 'fa fa-tag',
                'href' => 'app_label',
            ],
            '-------------------------------',
            "Types de règles de gestion" => [
                'icon' => 'fa fa-tag',
                'href' => 'app_rule_type',
            ],
            '-------------------------------',
            "RGDP" => [
                'icon' => 'fas fa-user-secret',
                'data-modal' => 'ajax',
                'data-title' => "Modifier les informations liées au RGPD",
                'data-url' => 'app_rgpd_info_edit',
            ],
            "Configuration" => [
                'icon' => 'fa fa-sliders',
                'data-modal' => 'ajax',
                'data-title' => "Modifier la configuration du tenant",
                'data-url' => 'app_tenant_configuration',
            ],
        ],
    ];
    $adminMenu = [
        "Tableau de bord" => [
            'icon' => 'fa fa-tachometer',
            'href' => 'admin_home',
        ],
        '-------------------------------',
        "Tenants" => [
            'icon' => 'fa fa-sitemap',
            'href' => 'admin_tenant',
        ],
        "Administrateurs techniques" => [
            'icon' => 'fa fa-user',
            'href' => 'admin_technical_admin',
        ],
        "RGDP" => [
            'icon' => 'fas fa-user-secret',
            'data-modal' => 'ajax',
            'data-title' => "Modifier les informations liées au RGPD",
            'data-url' => 'admin_rgpd_info',
        ],
        '-------------------------------',
        "Utilisateurs" => [
            'icon' => 'fa fa-user-o',
            'href' => 'admin_user',
        ],
        "Demandes de création d'utilisateurs" => [
            'icon' => 'fa fa-user-pen',
            'href' => 'admin_user_creation_request',
        ],
        "Sessions actives" => [
            'icon' => 'fa fa-link',
            'href' => 'admin_session',
        ],
        '-------------------------------',
        "Annuaires LDAP/AD" => [
            'icon' => 'fa fa-address-book-o',
            'href' => 'admin_ldap',
        ],
        "Connecteurs OpenID" => [
            'icon' => 'fa fa-openid',
            'href' => 'admin_openid',
        ],
    ];
    $twig
        ->defaultPath('%kernel.project_dir%/templates')
        ->formThemes([
            'bootstrap_4_layout.html.twig',
            'form/custom_types.html.twig',
        ])
        ->global('top', ['id' => 'top', 'value' => $top])
    ;
    $twig->global('admin_menu', ['id' => 'admin_menu', 'value' => $adminMenu]);
    $twig->global('min_password_strengh', $_ENV['MIN_PASSWORD_COMPLEXITY'] ?? 78);
    $twig->global('refae_logo_email_src', 'https://ressources.libriciel.fr/public/refae/img/re-light40pxh.png');
    $twig->global('debug', ((int)($_ENV['DEBUG'] ?? 0)) > 0);
    $twig->global('session_maxlifetime', ini_get('session.gc_maxlifetime') ?: 0);
    $versionFile = file(dirname(__DIR__, 2) . '/VERSION.txt');
    $version = trim((string) array_pop($versionFile));
    $twig->global('refae_version', $version);

    // informations RGPD
    $twig->global('refae_name', 'refae');
    $twig->global('refae_vendor_name', 'LIBRICIEL SCOP');
    $twig->global('refae_vendor_address', '140 Rue Aglaonice de Thessalie, 34170 Castelnau-le-Lez');
    $twig->global('refae_vendor_siret', '491 011 698 00025');
    $twig->global('refae_cnil_info_url', 'https://www.cnil.fr/fr/les-droits-pour-maitriser-vos-donnees-personnelles');
    $twig->global('refae_cnil_complaint_url', 'https://www.cnil.fr/fr/plaintes');

    // login default / public
    $twig->global(
        'publicLoginBackgroundColor',
        empty($_ENV['DEFAULT_LOGIN_BACKGROUND_COLOR']) ? '#f7f7f7' : $_ENV['DEFAULT_LOGIN_BACKGROUND_COLOR']
    );
    $twig->global(
        'publicLoginLogo',
        empty($_ENV['DEFAULT_LOGIN_LOGO']) ? 'build/images/ls.svg' : $_ENV['DEFAULT_LOGIN_LOGO']
    );
    $twig->global(
        'publicLoginText',
        empty($_ENV['DEFAULT_LOGIN_TEXT'])
            ? 'Développons ensemble vos projets de demain.'
            : $_ENV['DEFAULT_LOGIN_TEXT']
    );
    $twig->global(
        'publicLoginTextStyle',
        empty($_ENV['DEFAULT_LOGIN_TEXT_STYLE']) ? '' : $_ENV['DEFAULT_LOGIN_TEXT_STYLE']
    );

    if ($containerConfigurator->env() === 'test') {
        $twig->strictVariables(true);
    }
};
