<?php

declare(strict_types=1);

use App\Service\Permission;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    /**
     * Permet le calcul de $genericActions
     */
    $controllers = [
        'Agreement',
        'Authority',
        'Category',
        'DocumentRule',
        'Label',
        'LabelGroup',
        'Ldap',
        'ManagementRule',
        'Openid',
        'Profile',
        'RuleType',
        'ServiceLevel',
        'Tenant',
        'User',
        'Vocabulary',
    ];
    $generic_privileges = [
        'Read' => [
            'view',
        ],
        'Manage' => [
            'add_edit',
            'delete',
            'view',
            'import',
            'export',
        ],
    ];
    $genericActions = [];
    foreach ($controllers as $controller) {
        foreach ($generic_privileges as $privilege => $actions) {
            $genericActions["$controller/$privilege"] = [];
            foreach ($actions as $action) {
                $genericActions["$controller/$privilege"][] = "$controller/$action";
            }
        }
    }

    $services->set(Permission::class)
        ->autowire()
        ->args([
            '$privileges' => [
                'AdminHome/Read' => [
                    'common_name' => "Visualisation dans AdminHome",
                    'actions' => [
                        'AdminHome/view',
                    ],
                ],
                'Agreement/Manage' => [
                    'common_name' => "Ajout/Modification/Suppression des accords de versement",
                    'actions' => $genericActions['Agreement/Manage'],
                ],
                'Agreement/Read' => [
                    'common_name' => "Visualisation des accords de versement",
                    'actions' => $genericActions['Agreement/Read'],
                ],
                'Authority/Manage' => [
                    'common_name' => "Ajout/Modification/Suppression des notices d'autorité",
                    'actions' => $genericActions['Authority/Manage'],
                ],
                'Authority/Read' => [
                    'common_name' => "Visualisation des notices d'autorité",
                    'actions' => $genericActions['Authority/Read'],
                ],
                'Category/Manage' => [
                    'common_name' => "Ajout/Modification/Suppression des catégories",
                    'actions' => $genericActions['Category/Manage'],
                ],
                'Category/Read' => [
                    'common_name' => "Visualisation des catégories",
                    'actions' => $genericActions['Category/Read'],
                ],
                'DocumentRule/Manage' => [
                    'common_name' => "Ajout/Modification/Suppression des règles documentaires",
                    'actions' => $genericActions['DocumentRule/Manage'],
                ],
                'DocumentRule/Read' => [
                    'common_name' => "Visualisation des règles documentaires",
                    'actions' => $genericActions['DocumentRule/Read'],
                ],
                'Home/Read' => [
                    'common_name' => "Visualisation dans Home",
                    'actions' => [
                        'Home/view',
                    ],
                ],
                'Label/Manage' => [
                    'common_name' => "Ajout/Modification/Suppression des étiquettes",
                    'actions' => $genericActions['Label/Manage'],
                ],
                'Label/Read' => [
                    'common_name' => "Visualisation des étiquettes",
                    'actions' => $genericActions['Label/Read'],
                ],
                'LabelGroup/Manage' => [
                    'common_name' => "Ajout/Modification/Suppression des groupes d'étiquettes",
                    'actions' => $genericActions['LabelGroup/Manage'],
                ],
                'LabelGroup/Read' => [
                    'common_name' => "Visualisation dans les groupes d'étiquettes",
                    'actions' => $genericActions['LabelGroup/Read'],
                ],
                'Ldap/Manage' => [
                    'common_name' => "Ajout/Modification/Suppression dans LDAP",
                    'actions' => $genericActions['Ldap/Manage'],
                ],
                'Ldap/Read' => [
                    'common_name' => "Visualisation dans LDAP",
                    'actions' => $genericActions['Ldap/Read'],
                ],
                'ManagementRule/Manage' => [
                    'common_name' => "Ajout/Modification/Suppression dans Règles de gestion",
                    'actions' => $genericActions['ManagementRule/Manage'],
                ],
                'ManagementRule/Read' => [
                    'common_name' => "Visualisation dans Règles de gestion",
                    'actions' => $genericActions['ManagementRule/Read'],
                ],
                'Openid/Manage' => [
                    'common_name' => "Ajout/Modification/Suppression des connecteurs Openid",
                    'actions' => $genericActions['Openid/Manage'],
                ],
                'Openid/Read' => [
                    'common_name' => "Visualisation dans Openid",
                    'actions' => $genericActions['Openid/Read'],
                ],
                'Profile/Manage' => [
                    'common_name' => "Ajout/Modification/Suppression dans Profil",
                    'actions' => $genericActions['Profile/Manage'],
                ],
                'Profile/Read' => [
                    'common_name' => "Visualisation dans Profil",
                    'actions' => $genericActions['Profile/Read'],
                ],
                'RgpdInfo/Read' => [
                    'common_name' => "Visualisation des informations lié au RGPD",
                    'actions' => [
                        'RgpdInfo/view',
                    ],
                ],
                'RgpdInfo/Manage' => [
                    'common_name' => "Modification des informations lié au RGPD",
                    'actions' => [
                        'RgpdInfo/add_edit',
                    ],
                ],
                'RuleType/Manage' => [
                    'common_name' => "Ajout/Modification/Suppression dans Types de règles de gestion",
                    'actions' => $genericActions['RuleType/Manage'],
                ],
                'RuleType/Read' => [
                    'common_name' => "Visualisation dans Types de règles de gestion",
                    'actions' => $genericActions['RuleType/Read'],
                ],
                'ServiceLevel/Manage' => [
                    'common_name' => "Ajout/Modification/Suppression des niveaux de service",
                    'actions' => $genericActions['ServiceLevel/Manage'],
                ],
                'ServiceLevel/Read' => [
                    'common_name' => "Visualisation des niveaux de service",
                    'actions' => $genericActions['ServiceLevel/Read'],
                ],
                'Tenant/Configuration' => [
                    'common_name' => "Édition de la configuration du tenant",
                    'actions' => [
                        'TenantConfiguration/add_edit',
                    ],
                ],
                'Tenant/Manage' => [
                    'common_name' => "Ajout/Modification/Suppression dans Tenant",
                    'actions' => $genericActions['Tenant/Manage'],
                ],
                'Tenant/Read' => [
                    'common_name' => "Visualisation dans Tenant",
                    'actions' => $genericActions['Tenant/Manage'],
                ],
                'User/Manage' => [
                    'common_name' => "Ajout/Modification/Suppression dans Utilisateurs",
                    'actions' => $genericActions['User/Manage'],
                ],
                'User/Read' => [
                    'common_name' => "Visualisation dans Utilisateurs",
                    'actions' => $genericActions['User/Read'],
                ],
                'Vocabulary/Manage' => [
                    'common_name' => "Ajout/Modification/Suppression dans Vocabulaire",
                    'actions' => $genericActions['Vocabulary/Manage'],
                ],
                'Vocabulary/Read' => [
                    'common_name' => "Visualisation dans Vocabulaire",
                    'actions' => $genericActions['Vocabulary/Read'],
                ],
            ],
            '$roles' => [
                Permission::ROLE_FUNCTIONAL_ADMIN => [
                    'common_name' => "Administrateur fonctionnel",
                    'privileges' => [
                        'Agreement/Manage',
                        'Agreement/Read',
                        'Authority/Manage',
                        'Authority/Read',
                        'Category/Manage',
                        'Category/Read',
                        'DocumentRule/Manage',
                        'DocumentRule/Read',
                        'Home/Read',
                        'Label/Manage',
                        'Label/Read',
                        'LabelGroup/Manage',
                        'LabelGroup/Read',
                        'ManagementRule/Manage',
                        'ManagementRule/Read',
                        'Profile/Manage',
                        'Profile/Read',
                        'RgpdInfo/Read',
                        'RgpdInfo/Manage',
                        'RuleType/Manage',
                        'RuleType/Read',
                        'ServiceLevel/Manage',
                        'ServiceLevel/Read',
                        'User/Manage',
                        'User/Read',
                        'Vocabulary/Manage',
                        'Vocabulary/Read',
                        'Tenant/Configuration',
                    ],
                ],
                Permission::ROLE_READER => [
                    'common_name' => "Lecteur",
                    'privileges' => [
                        'Home/Read',
                        'RgpdInfo/Read',
                    ],
                ],
                Permission::ROLE_TECHNICAL_ADMIN => [
                    'common_name' => "Administrateur technique",
                    'privileges' => [
                        'AdminHome/Read',
                        'Ldap/Manage',
                        'Ldap/Read',
                        'Openid/Manage',
                        'Openid/Read',
                        'RgpdInfo/Read',
                        'RgpdInfo/Manage',
                        'Tenant/Manage',
                        'Tenant/Read',
                        'User/Manage',
                        'User/Manage',
                        'User/Read',
                    ],
                ],
                Permission::ROLE_WEBSERVICE_REQUESTER => [
                    'common_name' => "Webservice requêteur",
                    'privileges' => [
                        'Authority/Read',
                        'Agreement/Read',
                        'Profile/Read',
                        'Vocabulary/Read',
                        'ServiceLevel/Read',
                        'ManagementRule/Read',
                        'DocumentRule/Read',
                    ],
                ],
            ],
        ])
    ;
};
