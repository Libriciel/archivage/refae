<?php

declare(strict_types=1);

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

use App\Controller\Admin;
use App\EventSubscriber\AdminIpWhitelistSubscriber;
use App\Router\UrlTenantRouter;
use App\Serializer\ApiNormalizer;
use App\Service\ApiPaginator;
use App\Service\Eaccpf;
use App\Service\FilesizeLimiter;
use App\Service\Meilisearch;
use App\Service\MeilisearchInterface;
use App\Service\Manager\TenantManager;
use App\Service\MockedMeilisearch;
use App\Session\CustomSessionHandler;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;

return static function (ContainerConfigurator $containerConfigurator): void {
    $containerConfigurator->parameters()
        ->set('app.paths.tmp', '/var/www/html/tmp/')
    ;

    $services = $containerConfigurator->services();

    $services->defaults()
        ->autowire()
        ->autoconfigure();

    $services->load('App\\', __DIR__ . '/../src/')
        ->exclude([
        __DIR__ . '/../src/DependencyInjection/',
        __DIR__ . '/../src/Entity/',
        __DIR__ . '/../src/Cron/',
        __DIR__ . '/../src/Kernel.php',
        __DIR__ . '/../src/Resources/functions/',
    ]);

    $services->set(UrlTenantRouter::class)
        ->decorate('router.default')
        // pass the old service as an argument
        ->args([service('.inner')])
    ;

    $services->set(PdoSessionHandler::class)
        ->args([
            env('DATABASE_URL'),
        ])
    ;

    $services->set(Eaccpf::class)
        ->args([
            '$schemas' => [
                Eaccpf::NS_2010 => dirname(__DIR__) . '/assets/schemas/cpf.xsd',
                Eaccpf::NS_V2 => dirname(__DIR__) . '/assets/schemas/eac.xsd',
            ],
        ])
    ;
    $services
        ->load('App\\Cron\\', __DIR__ . '/../src/Cron')
        ->public()
    ;
    $services->set(AdminIpWhitelistSubscriber::class)
        ->args([
            '$isWhitelistEnabled' => (bool)((int)$_ENV['IP_ENABLE_WHITELIST'] ?? 0),
            '$ipWhitelist' => preg_split("/[\s,]+/", $_ENV['IP_WHITELIST'] ?? ''),
        ])
    ;
    $services->set(CustomSessionHandler::class)
        ->args([
            '$pdoOrDsn' => env('DATABASE_URL'),
        ])
    ;

    $containerConfigurator->import('packages/permissions.php');

    $services->set(Admin\HomeController::class)
        ->args([
            '$probeDuration' => env('ADMIN_PROBE_DURATION'),
        ])
    ;

    if ($containerConfigurator->env() === 'test') {
        $services->set(MeilisearchInterface::class, MockedMeilisearch::class);
    } else {
        $services->set(MeilisearchInterface::class, Meilisearch::class);
    }

    $services->set(FilesizeLimiter::class)
        ->args([
            '$defaultMaxFilesize' => env('DEFAULT_MAX_FILESIZE'),
            '$defaultFileRetentionCount' => env('DEFAULT_FILE_RETENTION_COUNT'),
        ])
    ;

    $services->set(TenantManager::class)
        ->args([
            '$accessRestrictionCodesXsd'
                => __DIR__ . '/../assets/schemas/seda_v1-0/v10/codes/seda_v1-0_accessrestriction_code.xsd',
        ])
    ;
    $services->set(ApiPaginator::class)
        ->args([
            '$maxPerPage' => $_ENV['API_MAX_PER_PAGE'] ?? 10,
            '$extensions' => [
                service('api_platform.doctrine.orm.query_extension.filter'),
                service('api_platform.doctrine.orm.query_extension.order'),
                service('api_platform.doctrine.orm.query_extension.pagination'),
                service('api_platform.doctrine.orm.query_extension.eager_loading'),
            ],
        ])
    ;

    $services->set(ApiNormalizer::class)
        ->decorate('api_platform.jsonld.normalizer.item')
    ;
};
