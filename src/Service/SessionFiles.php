<?php

namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SessionFiles
{
    public const string SESSION_KEY = 'tmpfiles';

    private ?Request $request = null;
    private array $tmpFiles = [];
    private ?SessionInterface $session = null;
    private bool $lastChunk = false;
    private ?int $completedChunksCount = 0;
    private ?string $uri = null;
    private ?string $chunkDirectory = null;

    public function __construct(
        private readonly RequestStack $requestStack,
    ) {
    }

    public function getUri(string $uuid): ?string
    {
        $session = $this->getSession();
        if (!$session) {
            return null;
        }
        if (isset($this->tmpFiles[$uuid])) {
            $this->checkPath($this->tmpFiles[$uuid]);
            return $this->tmpFiles[$uuid];
        }
        new NotFoundHttpException("Not Found");
    }

    public function exists(?string $uuid): bool
    {
        $session = $this->getSession();
        if (!$session || !$uuid) {
            return false;
        }
        return isset($this->tmpFiles[$uuid]);
    }

    public function delete(string $uuid): ?string
    {
        if (isset($this->tmpFiles[$uuid])) {
            $targetUri = $this->getUri($uuid);
            $this->sessionRemoveFile($uuid);
        } else {
            $targetUri = sprintf(
                "%s/%s/%s/chunks/",
                sys_get_temp_dir(),
                session_id(),
                $uuid,
            );
        }
        $filesystem = new Filesystem();
        $filesystem->remove($targetUri);

        return session_id() . '/' . $uuid;
    }

    public function checkValidChunk(): void
    {
        $request = $this->getRequest();
        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $request->files->get('file');
        $chunk = $request->get('chunk');
        $chunkSize = (int)$chunk['size'];
        $chunkContent = $uploadedFile->getContent();
        if (strlen($chunkContent) !== $chunkSize) {
            throw new BadRequestHttpException("chunk size does not match chunk content size");
        }
        $chunkHash = hexdec((string) $chunk['hash']);
        if ($chunkHash !== hexdec(hash('crc32b', $chunkContent))) {
            throw new BadRequestHttpException("chunk hash does not match chunk content hash");
        }
        if (preg_match('/[^\w-]/', (string) $request->get('uuid'))) {
            throw new BadRequestHttpException("invalid uuid");
        }
    }

    public function writeChunk(): void
    {
        $request = $this->getRequest();
        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $request->files->get('file');
        $chunk = $request->get('chunk');
        $chunkContent = $uploadedFile->getContent();
        $chunkSize = (int)$chunk['size'];
        $currentChunk = (int)$chunk['i'];
        $totalChunk = (int)$chunk['total'];
        $chunkHash = hexdec((string) $chunk['hash']);
        $len = strlen((string)$totalChunk);
        $targetUri = sprintf(
            "%s/%s/%s/chunks/%0{$len}d",
            sys_get_temp_dir(),
            session_id(),
            $request->get('uuid'),
            $currentChunk,
        );
        $targetBaseDirectory = dirname($targetUri);
        if (!is_dir($targetBaseDirectory)) {
            @mkdir($targetBaseDirectory, 0777, true);
        }
        $written = file_put_contents($targetUri, $chunkContent);
        if ($written !== $chunkSize || hexdec(hash_file('crc32b', $targetUri)) !== $chunkHash) {
            unlink($targetUri);
            throw new BadRequestHttpException("file write error");
        }

        $this->chunkDirectory = dirname($targetUri);
        $chunk = $request->get('chunk');
        $totalChunk = (int)$chunk['total'];

        $existingChunks = $this->completedChunksCount();
        $this->lastChunk = $existingChunks === $totalChunk;
    }

    public function mergeChunks()
    {
        $request = $this->getRequest();
        $chunks = glob($this->chunkDirectory . '/*');
        $neededTime = (int)(300 / 1000000000 * (int)$request->get('size'));// 1Go ~= 300s
        if (PHP_SAPI !== 'cli' && $neededTime > 30) {
            set_time_limit($neededTime);
        }
        $safeFilename = $this->filterFilename($request->get('filename'));
        $uri = dirname((string) $this->chunkDirectory) . '/' . $safeFilename;
        $file = fopen($uri, 'w');
        foreach ($chunks as $chunkFile) {
            fwrite($file, file_get_contents($chunkFile));
        }
        fclose($file);

        $filesystem = new Filesystem();
        $filesystem->remove($this->chunkDirectory);

        $this->sessionAddFile($request->get('uuid'), $uri);
        $this->uri = $uri;
    }

    public function getRelativeUri(): string
    {
        return substr((string) $this->uri, strlen(sys_get_temp_dir()) + 1);
    }

    public function isLastChunk(): bool
    {
        return $this->lastChunk;
    }

    private function getRequest(): ?Request
    {
        if (!$this->request) {
            $this->request = $this->requestStack->getCurrentRequest();
        }
        return $this->request;
    }

    private function getSession(): ?SessionInterface
    {
        if (!$this->session) {
            $this->session = $this->getRequest()?->getSession();
            $this->tmpFiles = $this->session->get(self::SESSION_KEY, []);
        }
        return $this->session;
    }

    public function getCompletedChunksCount(): int
    {
        return $this->completedChunksCount;
    }

    public function getTotalChunkCount(): int
    {
        $chunk = $this->getRequest()?->get('chunk');
        return (int)$chunk['total'] ?? 0;
    }

    private function sessionAddFile(string $uuid, string $uri): void
    {
        $this->checkPath($uri);
        $session = $this->getSession();
        $this->tmpFiles[$uuid] = $uri;
        $session->set(self::SESSION_KEY, $this->tmpFiles);
    }

    private function sessionRemoveFile(string $uuid): void
    {
        $session = $this->getSession();
        unset($this->tmpFiles[$uuid]);
        $session->set(self::SESSION_KEY, $this->tmpFiles);
    }

    private function checkPath(string $path): void
    {
        $realpath = realpath($path);
        if (!str_starts_with($realpath, sys_get_temp_dir() . '/') || !is_writable($realpath)) {
            throw new HttpException(403, "Invalid path");
        }
    }

    private function completedChunksCount(): int
    {
        if (!$this->completedChunksCount) {
            $request = $this->getRequest();
            $chunk = $request->get('chunk');
            $totalChunk = (int)$chunk['total'];
            $chunks = glob($this->chunkDirectory . '/*');
            $count = count($chunks);
            if (count($chunks) === $totalChunk) {
                $size = 0;
                foreach ($chunks as $chunkFile) {
                    $size += filesize($chunkFile);
                }
                if ($size !== (int)$request->get('size')) {
                    $count--; // évite de compter un chunk incomplet
                }
            }
            $this->completedChunksCount = $count;
        }
        return $this->completedChunksCount;
    }

    private function filterFilename($name)
    {
        $name = str_replace(array_merge(
            array_map('chr', range(0, 31)),
            ['<', '>', ':', '"', '/', '\\', '|', '?', '*']
        ), '', (string) $name);
        $ext = pathinfo($name, PATHINFO_EXTENSION);
        return mb_strcut(
            pathinfo($name, PATHINFO_FILENAME),
            0,
            255 - ($ext ? strlen($ext) + 1 : 0),
            mb_detect_encoding($name)
        ) . ($ext ? '.' . $ext : '');
    }
}
