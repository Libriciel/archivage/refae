<?php

namespace App\Service;

/**
 * Change les valeurs time et memory
 *
 * @category    Utility
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class LimitBreak
{
    /**
     * @var int valeur initiale de memory_get_usage
     */
    public static $initialMemory = 0;

    /**
     * Défini un time limit, ne réduit pas la valeur par défaut
     * @param int $sec seconds
     */
    public static function setTimeLimit($sec)
    {
        $sec = (int)$sec;
        $ini = (int)ini_get('max_execution_time');
        if ($ini !== 0 && $ini < $sec) {
            set_time_limit($sec);
        }
    }

    /**
     * Défini un memory limit, ne réduit pas la valeur par défaut
     * @param int $size bytes
     */
    public static function setMemoryLimit($size)
    {
        $size = (int)$size;
        $ini = self::settingToBytes(ini_get('memory_limit'));
        if ($ini !== -1 && $ini < $size) {
            ini_set('memory_limit', $size);
        }
    }

    /**
     * Converti le paramétrage en bytes (128M -> 134217728)
     * @param string $setting
     * @return int
     */
    public static function settingToBytes($setting): int
    {
        if (preg_match('/(\d+) *([a-zA-Z])/', trim($setting), $m)) {
            [, $c, $unit] = $m;
            $unit = strtolower($unit);
            $map = [
                'k' => 0x400,
                'm' => 0x100000,
                'g' => 0x40000000,
            ];
            $c *= $map[$unit] ?? 1;
            return $c;
        } else {
            return (int)$setting;
        }
    }

    /**
     * Mémorise les valeurs memory_get_usage() et memory_get_peak_usage()
     * @return string readable memory_get_usage()
     */
    public static function initMemoryAnalyse(): string
    {
        self::$initialMemory = memory_get_usage();
        return self::formatBytes(memory_get_usage());
    }

    /**
     * Affiche un debug de l'état de la mémoire
     * @return string
     */
    public static function debugMemory(): string
    {
        return sprintf(
            'actual memory: %s ; memory peak: %s',
            self::formatBytes(memory_get_usage() - self::$initialMemory),
            self::formatBytes(memory_get_peak_usage() - self::$initialMemory)
        );
    }

    /**
     * Donne la taille que prend une variable en mémoire
     * Cette opération est gourmante en RAM
     * @param mixed $var      variable à analyser
     * @param bool  $readable retourne un string
     * @return string|int
     */
    public static function getVarSize(mixed $var, bool $readable = true)
    {
        $initial = memory_get_usage();
        $dummy = unserialize(serialize($var));
        $size = abs(memory_get_usage() - $initial);
        unset($dummy);
        return $readable ? self::formatBytes($size) : $size;
    }

    /**
     * Donne le memory peak en readableSize
     * @return string
     */
    public static function memoryPeak(): string
    {
        return self::formatBytes(memory_get_peak_usage());
    }

    /**
     * Donne le memory peak en readableSize
     * @return string
     */
    public static function memory(): string
    {
        return self::formatBytes(memory_get_usage());
    }

    /**
     * Transforme une taille de fichier en octet en taille lisible
     * @return string
     */
    public static function formatBytes(int $bytes, int $precision = 2)
    {
        $units = ['o', 'Ko', 'Mo', 'Go', 'To', 'Po', 'Eo', 'Zo', 'Yo'];
        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);
        $bytes /= 1024 ** $pow;
        return round($bytes, $precision) . ' ' . $units[$pow];
    }
}
