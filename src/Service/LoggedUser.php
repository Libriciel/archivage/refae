<?php

namespace App\Service;

use App\Entity\Tenant;
use App\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;

class LoggedUser
{
    private ?Tenant $tenant = null;
    private ?User $user = null;

    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly Security $security,
    ) {
        /** @var User $user */
        $user = $security->getUser();
        if ($user instanceof User) {
            $this->user = $user;
        }
    }

    public function user(): ?User
    {
        $user = $this->security->getUser();
        if ($user instanceof User) {
            $this->user = $user;
        }
        return $user;
    }

    public function tenant(): ?Tenant
    {
        if (!$this->tenant) {
            $this->tenant = $this->user?->getTenant($this->requestStack->getCurrentRequest());
        }
        return $this->tenant;
    }
}
