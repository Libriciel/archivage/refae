<?php

namespace App\Service\Manager;

use App\Entity\StateMachineEntityInterface;
use Exception;
use ReflectionClass;
use Symfony\Component\Finder\Finder;
use Symfony\Config\Framework\Workflows\WorkflowsConfig;
use Symfony\Config\FrameworkConfig;

/**
 * Permet de générer la config workflow (state machine exactement) automatique
 * à partir d'une entité qui implémente StateMachineEntityInterface
 */
readonly class WorkflowManager
{
    public function __construct(private FrameworkConfig $framework)
    {
    }

    public static function getVersionableEntityClasses(): array
    {
        $classes = [];
        $finder = new Finder();
        $finder->files()->in(__DIR__ . '/../../Entity/')->name('*.php');
        foreach ($finder as $file) {
            $class_name = 'App\\Entity\\' . $file->getFilenameWithoutExtension();
            if (
                class_exists($class_name)
                && (new ReflectionClass($class_name))->implementsInterface(StateMachineEntityInterface::class)
            ) {
                $classes[] = $class_name;
            }
        }
        return $classes;
    }

    public function initiateWorkflow(string $className, bool $auditTrail = false): WorkflowsConfig
    {
        if (!in_array(StateMachineEntityInterface::class, class_implements($className))) {
            throw new Exception(sprintf('%s does not implements %s', $className, StateMachineEntityInterface::class));
        }

        /** @var StateMachineEntityInterface $entity */
        $entity = new $className();
        $states = array_reduce(
            $entity->getTransitions(),
            function ($carry, $item): array {
                foreach ($item as $from => $to) {
                    $carry[$from] = $from;
                    $carry[$to] = $to;
                }
                return $carry;
            },
            []
        );

        $initialState = $entity->getInitialState();
        $path = explode('\\', $className);
        $alias = strtolower(end($path));

        $workflow = $this->framework->workflows()->workflows($alias);
        $workflow
            ->type('state_machine') // à modifier à l'avenir ?
            ->supports([$className])
            ->initialMarking([$initialState]);

        $workflow->auditTrail()->enabled($auditTrail);

        $workflow->markingStore()
            ->type('method')
            ->property('state');

        foreach ($states as $state) {
            $workflow->place()->name($state);
        }

        foreach ($entity->getTransitions() as $name => $transition) {
            foreach ($transition as $from => $to) {
                $workflow->transition()
                    ->name($name)
                    ->from([$from])
                    ->to($to);
            }
        }

        // si besoin de config spécifique, on retourne l'objet
        return $workflow;
    }
}
