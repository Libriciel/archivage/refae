<?php

namespace App\Service\Manager;

use App\Service\ImportZip;
use Doctrine\ORM\QueryBuilder;

interface ImportExportManagerInterface
{
    public const int MAX_QUERY_PARAMS = 10000;

    /**
     * Export data based on the provided query.
     *
     * @param QueryBuilder $query
     * @return string
     */
    public function export(QueryBuilder $query): string;

    /**
     * Check for import feasibility and validate data.
     *
     * @param ImportZip $importZip
     * @return bool
     */
    public function checkForImport(ImportZip $importZip): bool;

    /**
     * Import data for the specified user and tenant.
     *
     * @param ImportZip $importZip
     * @return void
     */
    public function import(ImportZip $importZip): void;

    public static function getFileName(): string;

    public function getClassName(): string;
}
