<?php

namespace App\Service\Manager;

use App\Entity\Tenant;
use App\Entity\Term;
use App\Entity\User;
use App\Entity\Vocabulary;
use App\Repository\TermRepository;
use App\Service\Csv;
use App\Service\LimitBreak;
use Doctrine\ORM\EntityManagerInterface;
use DOMDocument;
use DOMElement;
use DOMXPath;

class TermManager
{
    public ?User $user = null;
    public ?Tenant $tenant = null;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly TermRepository $termRepository,
    ) {
    }

    public function importCsv(Vocabulary $vocabulary, string $csvUri, array $formData): int
    {
        $csv = new Csv($csvUri);
        $csvData = array_slice($csv->getData(), $formData['firstLine'] - 1);
        $map = range('A', 'Z');
        $nameCol = array_search($formData['name'], $map);
        if (isset($formData['identifier'])) {
            $identifierCol = array_search($formData['identifier'], $map);
        }
        if (isset($formData['directoryName'])) {
            $directoryNameCol = array_search($formData['directoryName'], $map);
        }
        $this->entityManager->beginTransaction();
        $imported = 0;
        foreach ($csvData as $cols) {
            $imported += $this->importName(
                $vocabulary,
                $cols[$nameCol],
                isset($identifierCol) ? $cols[$identifierCol] : null,
                isset($directoryNameCol) ? $cols[$directoryNameCol] : null
            );
        }
        $this->entityManager->commit();
        return $imported;
    }

    public function importSkos(Vocabulary $vocabulary, DOMDocument $dom): int
    {
        $xpath = new DOMXPath($dom);
        $skosNs = $dom->documentElement->getAttribute('xmlns:skos');
        if (!$skosNs) {
            return 0;
        }
        $xpath->registerNamespace('skos', $skosNs);
        $query = $xpath->query('*[count(skos:prefLabel) >= 1]');
        if ($query->count() === 0) {
            return 0;
        }
        LimitBreak::setTimeLimit($query->count()); // 1s par mot clé
        $imported = 0;
        foreach ($query as $conceptNode) {
            $name = $this->getSkosLabel($xpath, $conceptNode);
            if ($name) {
                $imported += $this->importName($vocabulary, $name);
            }
        }
        return $imported;
    }

    public function getSkosLabel(DOMXPath $xpath, DOMElement $conceptNode): ?string
    {
        $altLangs = [];
        /** @var DOMElement $labelNode */
        foreach ($xpath->query('skos:prefLabel', $conceptNode) as $labelNode) {
            $lang = substr( // coupe "fr-fr" en "fr"
                strtolower($labelNode->getAttribute('xml:lang')),
                0,
                2
            );
            if ($lang === 'fr') {
                return $labelNode->nodeValue;
            } else {
                $altLangs[$lang] = $labelNode->nodeValue;
            }
        }
        foreach (['en', 'es', 'it', 'de'] as $altLang) {
            if (isset($altLangs[$altLang])) {
                return $altLangs[$altLang];
            }
        }
        return null;
    }

    private function importName(
        Vocabulary $vocabulary,
        string $name,
        ?string $identifier = null,
        ?string $directoryName = null
    ): int {
        $term = $this->termRepository->findOneBy([
            'name' => $name,
            'vocabulary' => $vocabulary,
        ]);
        if ($term) {
            return 0;
        }
        $term = new Term();
        $term->setName($name);
        $term->setVocabulary($vocabulary);
        $term->setIdentifier($identifier);
        $term->setDirectoryName($directoryName);
        $this->entityManager->persist($term);
        $this->entityManager->flush();
        return 1;
    }
}
