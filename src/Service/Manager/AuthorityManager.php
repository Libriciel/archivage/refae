<?php

namespace App\Service\Manager;

use App\Entity\Authority;
use App\Entity\AuthorityFile;
use App\Entity\Label;
use App\Entity\Tenant;
use App\Entity\User;
use App\Entity\VersionableEntityInterface;
use App\Repository\AuthorityRepository;
use App\Repository\LabelRepository;
use App\Service\ArrayToEntity;
use App\Service\Eaccpf;
use App\Service\ExportZip;
use App\Service\ImportZip;
use App\Service\NewVersion;
use App\Service\SessionFiles;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use DOMDocument;
use Exception;
use Override;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use ZipArchive;

class AuthorityManager implements ImportExportManagerInterface
{
    use ImportLabelTrait;

    public ?User $user = null;
    public ?Tenant $tenant = null;

    public function __construct(
        private readonly AuthorityRepository $authorityRepository,
        private readonly EntityManagerInterface $entityManager,
        private readonly ExportZip $exportZip,
        private readonly LabelRepository $labelRepository,
        private readonly NewVersion $newVersion,
        private readonly RequestStack $requestStack,
        private readonly Security $security,
        private readonly SessionFiles $sessionFiles,
        private readonly ValidatorInterface $validator,
        private readonly LabelManager $labelManager,
    ) {
    }

    public function newAuthority(Tenant $tenant): Authority
    {
        $authority = new Authority();
        $authority
            ->setState($authority->getInitialState())
            ->setVersion(1)
            ->setTenant($tenant)
        ;
        return $authority;
    }

    public function save(Authority $authority, ?string $session_tmpfile_uuid = null): void
    {
        $this->entityManager->persist($authority);

        if ($session_tmpfile_uuid) {
            $uri = $this->sessionFiles->getUri($session_tmpfile_uuid);
            $file = FileManager::setDataFromPath($uri);
            $dom = new DOMDocument();
            $dom->load($uri);
            $xmlns = $dom->documentElement->getAttribute('xmlns');
            $authorityFile = new AuthorityFile();
            $authorityFile->setAuthority($authority);
            $type = match ($xmlns) {
                Eaccpf::NS_2010 => AuthorityFile::TYPE_EACCPF_2010,
                Eaccpf::NS_V2 => AuthorityFile::TYPE_EACCPF_V20,
            };
            $authorityFile->setType($type);
            $authorityFile->setFile($file);
            $authorityFile->setDescription("Fichier joint lors de la création de la notice d'autorité");
            $this->entityManager->persist($authorityFile);
            $this->entityManager->persist($file);
        }

        $this->entityManager->flush();
    }

    public function newVersion(Authority $authority, string $reason): Authority
    {
        $newVersion = $this->newVersion->fromEntity($authority, $reason);
        $this->entityManager->persist($newVersion);

        foreach ($authority->getAuthorityFiles() as $authorityFile) {
            $file = FileManager::duplicate($authorityFile->getFile());
            $newAuthorityFile = new AuthorityFile($newVersion);
            $newAuthorityFile
                ->setFile($file)
                ->setDescription($authorityFile->getDescription())
                ->setType($authorityFile->getType());
            $this->entityManager->persist($newAuthorityFile);
        }

        $this->entityManager->flush();
        return $newVersion;
    }

    #[Override]
    public function export(QueryBuilder $query): string
    {
        return $this->exportZip->fromQuery(
            $query,
            Authority::class,
            function (Authority $authority, ZipArchive $zip, array $normalized) {
                foreach ($authority->getLabels() as $label) {
                    $normalized['labels'][] = $label->export();
                }
                foreach ($authority->getAuthorityFiles() as $authorityFile) {
                    $file = $authorityFile->getFile();
                    $zip->addFromString(
                        $file->getId() . '/' . $file->getName(),
                        stream_get_contents($file->getContent())
                    );
                    $normalized['authority_files'][] = $authorityFile->export();
                }
                return $normalized;
            },
        );
    }

    #[Override]
    public function checkForImport(ImportZip $importZip): bool
    {
        $request = $this->requestStack->getCurrentRequest();
        /** @var User $user */
        $user = $this->security->getUser();
        $tenant = $user->getTenant($request);
        if (!$tenant) {
            return true;
        }

        $dataCount = count($importZip->data);
        $duplicates = [];
        $currentLabels = array_map(
            fn(Label $l) => $l->getConcatLabelGroupName(),
            $this->labelRepository->queryForTenant($tenant->getBaseurl())->getQuery()->getResult()
        );
        $importZip->errors['failed'] = [];

        for ($i = 0; $i < $dataCount; $i += self::MAX_QUERY_PARAMS) {
            $identifiers = [];
            for ($j = 0; $j < self::MAX_QUERY_PARAMS && ($i + $j) < $dataCount; $j++) {
                $index = $i + $j;
                if (!$this->checkSingleData($index, $importZip, $currentLabels, $identifiers)) {
                    return false;
                }
            }
            foreach ($this->authorityRepository->findByIdentifiersForTenant($tenant, $identifiers) as $authority) {
                if (!isset($duplicates[$authority->getIdentifier()])) {
                    $duplicates[$authority->getIdentifier()] = true;
                    $importZip->errors['duplicates'][] = $authority->getIdentifier();
                    $importZip->errors['failed'][$authority->getIdentifier()] = $authority->getIdentifier();
                }
            }
        }
        return true;
    }

    private function checkSingleData(
        int $index,
        ImportZip $importZip,
        array $currentLabels,
        array &$identifiers
    ): bool {
        $requiredFields = ['identifier', 'name', 'public'];
        foreach ($requiredFields as $fieldname) {
            if (!isset($importZip->data[$index][$fieldname])) {
                $importZip->errors['fatal'] = sprintf("Format du fichier incorrect : champ '%s' manquant", $fieldname);
                return false;
            }
        }

        $identifiers[] = $importZip->data[$index]['identifier'];
        $this->checkLabels($index, $importZip, $currentLabels);

        return true;
    }

    private function getUser(): User
    {
        if (isset($this->user)) {
            return $this->user;
        }
        /** @var User $user */
        $user = $this->security->getUser();
        return $user;
    }

    private function getTenant(): Tenant
    {
        if (isset($this->tenant)) {
            return $this->tenant;
        }
        $request = $this->requestStack->getCurrentRequest();
        return $this->getUser()->getTenant($request);
    }

    #[Override]
    public function import(ImportZip $importZip): void
    {
        $user = $this->getUser();
        $tenant = $this->getTenant();

        $this->entityManager->beginTransaction();
        foreach ($importZip->labels as $missingLabel) {
            $this->labelManager->importMissingLabel($missingLabel, $tenant);
        }

        $labels = [];
        /** @var Label[] $labelResults */
        $labelResults = $this->labelRepository->queryForTenant($tenant->getBaseurl())->getQuery()->getResult();
        foreach ($labelResults as $label) {
            $labels[$label->getConcatLabelGroupName()] = $label;
        }
        unset($labelResults);
        foreach ($importZip->data as $authorityData) {
            if (isset($importZip->errors['failed'][$authorityData['identifier']])) {
                continue;
            }

            /** @var Authority $entity */
            $entity = ArrayToEntity::arrayToEntity($authorityData, Authority::class);
            $entity->setTenant($tenant);

            $this->addLabelsToEntity($authorityData['labels'] ?? [], $labels, $entity);

            foreach ($authorityData['authority_files'] ?? [] as $authorityFileData) {
                $relativePath = $authorityFileData['file']['id'] . '/' . $authorityFileData['file']['name'];
                if (isset($importZip->files[$relativePath])) {
                    $file = FileManager::setDataFromPath($importZip->files[$relativePath]);
                    $this->entityManager->persist($file);

                    /** @var AuthorityFile $authorityFile */
                    $authorityFile = ArrayToEntity::arrayToEntity($authorityFileData, AuthorityFile::class);
                    $authorityFile
                        ->setAuthority($entity)
                        ->setFile($file);

                    $errors = $this->validator->validate($authorityFile);
                    if (count($errors)) {
                        throw new Exception((string)$errors);
                    }
                    $this->entityManager->persist($authorityFile);
                }
            }

            $activity = ActivityManager::newActivity($entity, 'import', $user);
            $this->entityManager->persist($activity);

            $entity
                ->addActivity($activity)
                ->setVersion(1)
                ->setState(VersionableEntityInterface::S_EDITING)
            ;
            $errors = $this->validator->validate($entity);
            if (count($errors)) {
                throw new Exception((string)$errors);
            }

            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();
        $this->entityManager->commit();
    }

    #[Override]
    public static function getFileName(): string
    {
        return 'notice_autorite';
    }

    #[Override]
    public function getClassName(): string
    {
        return $this->authorityRepository->getClassName();
    }
}
