<?php

namespace App\Service\Manager;

use App\Entity\RuleType;
use App\Entity\User;
use App\Repository\RuleTypeRepository;
use App\Service\ArrayToEntity;
use App\Service\ExportZip;
use App\Service\ImportZip;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Override;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Validator\ValidatorInterface;

readonly class RuleTypeManager implements ImportExportManagerInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private ExportZip $exportZip,
        private RuleTypeRepository $ruleTypeRepository,
        private RequestStack $requestStack,
        private Security $security,
        private ValidatorInterface $validator,
    ) {
    }

    #[Override]
    public function export(QueryBuilder $query): string
    {
        return $this->exportZip->fromQuery(
            $query,
            RuleType::class
        );
    }

    #[Override]
    public function checkForImport(ImportZip $importZip): bool
    {
        $request = $this->requestStack->getCurrentRequest();
        /** @var User $user */
        $user = $this->security->getUser();
        $tenant = $user->getTenant($request);
        $dataCount = count($importZip->data);
        $duplicates = [];

        for ($index = 0; $index < $dataCount; $index++) {
            if (!$this->checkSingleData($index, $importZip)) {
                return false;
            }

            /** @var RuleType|null $exists */
            $ruleType = $importZip->data[$index];
            $exists = $this->ruleTypeRepository->findOneForImport($tenant, $ruleType['identifier']);
            $existIdentifier = $exists?->getIdentifier();

            if ($exists && !isset($duplicates[$existIdentifier])) {
                $duplicates[$existIdentifier] = true;
                $importZip->errors['duplicates'][] = $existIdentifier;
                $importZip->errors['failed'][$existIdentifier] = $existIdentifier;
            }
        }
        return true;
    }

    private function checkSingleData(
        int $index,
        ImportZip $importZip,
    ): bool {
        $requiredFields = ['identifier', 'name'];
        foreach ($requiredFields as $fieldname) {
            if (!isset($importZip->data[$index][$fieldname])) {
                $importZip->errors['fatal'] = sprintf("Format du fichier incorrect : champ '%s' manquant", $fieldname);
                return false;
            }
        }
        return true;
    }

    #[Override]
    public function import(ImportZip $importZip): void
    {
        $request = $this->requestStack->getCurrentRequest();
        /** @var User $user */
        $user = $this->security->getUser();
        $tenant = $user->getTenant($request);

        foreach ($importZip->data as $ruleTypeData) {
            $identifier = $ruleTypeData['identifier'];
            if (isset($importZip->errors['failed'][$identifier])) {
                continue;
            }

            /** @var RuleType $entity */
            $entity = ArrayToEntity::arrayToEntity($ruleTypeData, RuleType::class);
            $entity->setTenant($tenant);

            $errors = $this->validator->validate($entity);
            if (count($errors)) {
                throw new Exception((string)$errors);
            }

            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();
    }

    #[Override]
    public static function getFileName(): string
    {
        return 'type_regles_gestion';
    }

    #[Override]
    public function getClassName(): string
    {
        return $this->ruleTypeRepository->getClassName();
    }
}
