<?php

namespace App\Service\Manager;

use App\Entity\ProfileFile;
use App\Entity\Label;
use App\Entity\Profile;
use App\Entity\Tenant;
use App\Entity\User;
use App\Entity\VersionableEntityInterface;
use App\Repository\LabelRepository;
use App\Repository\ProfileRepository;
use App\Service\ArrayToEntity;
use App\Service\ExportZip;
use App\Service\ImportZip;
use App\Service\NewVersion;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Override;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use ZipArchive;

class ProfileManager implements ImportExportManagerInterface
{
    use ImportLabelTrait;

    public ?User $user = null;
    public ?Tenant $tenant = null;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ExportZip $exportZip,
        private readonly LabelRepository $labelRepository,
        private readonly NewVersion $newVersion,
        private readonly ProfileRepository $profileRepository,
        private readonly RequestStack $requestStack,
        private readonly Security $security,
        private readonly ValidatorInterface $validator,
        private readonly LabelManager $labelManager,
    ) {
    }

    public function newProfile(Tenant $tenant): Profile
    {
        $profile = new Profile();
        $profile
            ->setState($profile->getInitialState())
            ->setVersion(1)
            ->setTenant($tenant);
        return $profile;
    }

    public function save(Profile $profile): void
    {
        $this->entityManager->persist($profile);
        $this->entityManager->flush();
    }

    public function newVersion(Profile $profile, string $reason): Profile
    {
        $newVersion = $this->newVersion->fromEntity($profile, $reason);
        $this->entityManager->persist($newVersion);

        foreach ($profile->getProfileFiles() as $profileFile) {
            $file = FileManager::duplicate($profileFile->getFile());
            $newProfileFile = new ProfileFile($newVersion);
            $newProfileFile
                ->setFile($file)
                ->setDescription($profileFile->getDescription())
                ->setType($profileFile->getType());
            $this->entityManager->persist($newProfileFile);
        }

        $this->entityManager->flush();
        return $newVersion;
    }

    #[Override]
    public function export(QueryBuilder $query): string
    {
        return $this->exportZip->fromQuery(
            $query,
            Profile::class,
            function (Profile $profile, ZipArchive $zip, array $normalized) {
                foreach ($profile->getLabels() as $label) {
                    $normalized['labels'][] = $label->export();
                }
                foreach ($profile->getProfileFiles() as $profileFile) {
                    $file = $profileFile->getFile();
                    $zip->addFromString(
                        $profile->getId() . '/' . $file->getName(),
                        stream_get_contents($file->getContent())
                    );
                    $normalized['profile_files'][] = $file->export();
                }
                return $normalized;
            },
        );
    }

    #[Override]
    public function checkForImport(ImportZip $importZip): bool
    {
        $request = $this->requestStack->getCurrentRequest();
        /** @var User $user */
        $user = $this->security->getUser();
        $tenant = $user->getTenant($request);
        if (!$tenant) {
            return true;
        }

        $dataCount = count($importZip->data);
        $duplicates = [];
        $currentLabels = array_map(
            fn(Label $l) => $l->getConcatLabelGroupName(),
            $this->labelRepository->queryForTenant($tenant->getBaseurl())->getQuery()->getResult()
        );
        $importZip->errors['failed'] = [];

        for ($i = 0; $i < $dataCount; $i += self::MAX_QUERY_PARAMS) {
            $identifiers = [];
            for ($j = 0; $j < self::MAX_QUERY_PARAMS && ($i + $j) < $dataCount; $j++) {
                $index = $i + $j;
                if (!$this->checkSingleData($index, $importZip, $currentLabels, $identifiers)) {
                    return false;
                }
            }
            foreach ($this->profileRepository->findByIdentifiersForTenant($tenant, $identifiers) as $profile) {
                if (!isset($duplicates[$profile->getIdentifier()])) {
                    $duplicates[$profile->getIdentifier()] = true;
                    $importZip->errors['duplicates'][] = $profile->getIdentifier();
                    $importZip->errors['failed'][$profile->getIdentifier()] = $profile->getIdentifier();
                }
            }
        }
        return true;
    }

    private function checkSingleData(
        int $index,
        ImportZip $importZip,
        array $currentLabels,
        array &$identifiers
    ): bool {
        $requiredFields = ['identifier', 'name', 'public'];
        foreach ($requiredFields as $fieldname) {
            if (!isset($importZip->data[$index][$fieldname])) {
                $importZip->errors['fatal'] = sprintf("Format du fichier incorrect : champ '%s' manquant", $fieldname);
                return false;
            }
        }

        $identifiers[] = $importZip->data[$index]['identifier'];
        $this->checkLabels($index, $importZip, $currentLabels);

        return true;
    }

    private function getUser(): User
    {
        if (isset($this->user)) {
            return $this->user;
        }
        /** @var User $user */
        $user = $this->security->getUser();
        return $user;
    }

    private function getTenant(): Tenant
    {
        if (isset($this->tenant)) {
            return $this->tenant;
        }
        $request = $this->requestStack->getCurrentRequest();
        return $this->getUser()->getTenant($request);
    }

    #[Override]
    public function import(ImportZip $importZip): void
    {
        $user = $this->getUser();
        $tenant = $this->getTenant();

        $this->entityManager->beginTransaction();
        foreach ($importZip->labels as $missingLabel) {
            $this->labelManager->importMissingLabel($missingLabel, $tenant);
        }

        $labels = [];
        /** @var Label[] $labelResults */
        $labelResults = $this->labelRepository->queryForTenant($tenant->getBaseurl())->getQuery()->getResult();
        foreach ($labelResults as $label) {
            $labels[$label->getConcatLabelGroupName()] = $label;
        }
        unset($labelResults);
        foreach ($importZip->data as $profileData) {
            if (isset($importZip->errors['failed'][$profileData['identifier']])) {
                continue;
            }

            /** @var Profile $entity */
            $entity = ArrayToEntity::arrayToEntity($profileData, Profile::class);
            $entity->setTenant($tenant);

            $this->addLabelsToEntity($profileData['labels'] ?? [], $labels, $entity);

            foreach ($profileData['profile_files'] ?? [] as $profileFileData) {
                $relativePath = $profileFileData['file']['id'] . '/' . $profileFileData['file']['name'];
                if (isset($importZip->files[$relativePath])) {
                    $file = FileManager::setDataFromPath($importZip->files[$relativePath]);
                    $this->entityManager->persist($file);

                    /** @var ProfileFile $profileFile */
                    $profileFile = ArrayToEntity::arrayToEntity($profileFileData, ProfileFile::class);
                    $profileFile
                        ->setProfile($entity)
                        ->setFile($file);

                    $errors = $this->validator->validate($profileFile);
                    if (count($errors)) {
                        throw new Exception((string)$errors);
                    }
                    $this->entityManager->persist($profileFile);
                }
            }

            $activity = ActivityManager::newActivity($entity, 'import', $user);
            $this->entityManager->persist($activity);

            $entity
                ->addActivity($activity)
                ->setVersion(1)
                ->setState(VersionableEntityInterface::S_EDITING)
            ;
            $errors = $this->validator->validate($entity);
            if (count($errors)) {
                throw new Exception((string)$errors);
            }

            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();
        $this->entityManager->commit();
    }

    #[Override]
    public static function getFileName(): string
    {
        return 'profil';
    }

    #[Override]
    public function getClassName(): string
    {
        return $this->profileRepository->getClassName();
    }
}
