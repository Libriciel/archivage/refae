<?php

namespace App\Service\Manager;

use App\Entity\AccessUser;
use App\Entity\User;
use App\Repository\RoleRepository;
use App\Repository\UserCreationRequestRepository;
use App\Service\Permission;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

readonly class UserManager
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private UserPasswordHasherInterface $passwordHasher,
        private TokenStorageInterface $tokenStorage,
        private UserCreationRequestRepository $userCreationRequestRepository,
        private RoleRepository $roleRepository,
    ) {
    }

    public function add(User $user, string $plainPassword = null, bool $admin = false): bool
    {
        if ($plainPassword) {
            $user->setPassword($this->passwordHasher->hashPassword($user, $plainPassword));
        }

        if ($admin) {
            $accessUser = new AccessUser();
            $role = $this->roleRepository->findOneDefault(Permission::ROLE_TECHNICAL_ADMIN);
            $accessUser->setRole($role);
            $user->addAccessUser($accessUser);
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->userCreationRequestRepository->deleteAccepted($user->getUsername());

        return true;
    }

    public function edit(User $user, string $plainPassword = null): bool
    {
        if ($plainPassword) {
            $user->setPassword($this->passwordHasher->hashPassword($user, $plainPassword));
            $user->setUserDefinedPassword(true);
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return true;
    }

    public function deleteUser(User $user): bool
    {
        /** @var User|null $thisUser */
        $thisUser = $this->tokenStorage->getToken()?->getUser();
        if ($thisUser?->getId() === $user->getId()) {
            return false;
        }

        if (!$user->getDeletable()) {
            return false;
        }

        $this->entityManager->remove($user);
        $this->entityManager->flush();

        return true;
    }
}
