<?php

namespace App\Service\Manager;

use App\Entity\File;
use Exception;

class FileManager
{
    public static function setDataFromPath(string $path, File $file = new File(), $algo = 'sha256'): File
    {
        if (!is_file($path)) {
            throw new Exception(sprintf('File %s not found', $path));
        }
        if (!is_readable($path)) {
            throw new Exception(sprintf('File %s not readable', $path));
        }
        return $file
            ->setName(basename($path))
            ->setContent(file_get_contents($path))
            ->setHash(hash_file($algo, $path))
            ->setHashAlgo($algo)
            ->setMime(mime_content_type($path))
            ->setSize(filesize($path));
    }

    public static function duplicate(File $file): File
    {
        return (new File())
            ->setName($file->getName())
            ->setMime($file->getMime())
            ->setContent($file->getContent())
            ->setHash($file->getHash()) // TODO: recalculer ?
            ->setHashAlgo($file->getHashAlgo())
            ->setSize($file->getSize()) // TODO: recalculer ?
        ;
    }
}
