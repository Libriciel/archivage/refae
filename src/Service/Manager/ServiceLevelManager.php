<?php

namespace App\Service\Manager;

use App\Entity\ServiceLevelFile;
use App\Entity\Label;
use App\Entity\ServiceLevel;
use App\Entity\Tenant;
use App\Entity\User;
use App\Entity\VersionableEntityInterface;
use App\Repository\LabelRepository;
use App\Repository\ServiceLevelRepository;
use App\Service\ArrayToEntity;
use App\Service\ExportZip;
use App\Service\ImportZip;
use App\Service\NewVersion;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Override;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use ZipArchive;

class ServiceLevelManager implements ImportExportManagerInterface
{
    use ImportLabelTrait;

    public ?User $user = null;
    public ?Tenant $tenant = null;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly RequestStack $requestStack,
        private readonly Security $security,
        private readonly ExportZip $exportZip,
        private readonly ServiceLevelRepository $serviceLevelRepository,
        private readonly LabelRepository $labelRepository,
        private readonly ValidatorInterface $validator,
        private readonly NewVersion $newVersion,
        private readonly LabelManager $labelManager,
    ) {
    }

    public function newServiceLevel(Tenant $tenant): ServiceLevel
    {
        $serviceLevel = new ServiceLevel();
        $serviceLevel
            ->setState($serviceLevel->getInitialState())
            ->setVersion(1)
            ->setTenant($tenant)
        ;
        return $serviceLevel;
    }

    public function save(ServiceLevel $serviceLevel): void
    {
        $this->entityManager->persist($serviceLevel);
        $this->entityManager->flush();
    }

    public function newVersion(ServiceLevel $serviceLevel, string $reason): ServiceLevel
    {
        $newVersion = $this->newVersion->fromEntity($serviceLevel, $reason);
        $this->entityManager->persist($newVersion);

        foreach ($serviceLevel->getServiceLevelFiles() as $serviceLevelFile) {
            $file = FileManager::duplicate($serviceLevelFile->getFile());
            $newServiceLevelFile = new ServiceLevelFile($newVersion);
            $newServiceLevelFile
                ->setFile($file)
                ->setDescription($serviceLevelFile->getDescription())
                ->setType($serviceLevelFile->getType());
            $this->entityManager->persist($newServiceLevelFile);
        }

        $this->entityManager->flush();
        return $newVersion;
    }

    #[Override]
    public function export(QueryBuilder $query): string
    {
        return $this->exportZip->fromQuery(
            $query,
            ServiceLevel::class,
            function (ServiceLevel $serviceLevel, ZipArchive $zip, array $normalized) {
                foreach ($serviceLevel->getLabels() as $label) {
                    $normalized['labels'][] = $label->export();
                }
                foreach ($serviceLevel->getServiceLevelFiles() as $serviceLevelFile) {
                    $file = $serviceLevelFile->getFile();
                    $zip->addFromString(
                        $file->getId() . '/' . $file->getName(),
                        stream_get_contents($file->getContent())
                    );
                    $normalized['service_level_files'][] = $serviceLevelFile->export();
                }
                return $normalized;
            },
        );
    }

    #[Override]
    public function checkForImport(ImportZip $importZip): bool
    {
        $request = $this->requestStack->getCurrentRequest();
        /** @var User $user */
        $user = $this->security->getUser();
        $tenant = $user->getTenant($request);
        if (!$tenant) {
            return true;
        }

        $dataCount = count($importZip->data);
        $duplicates = [];
        $currentLabels = array_map(
            fn(Label $l) => $l->getConcatLabelGroupName(),
            $this->labelRepository->queryForTenant($tenant->getBaseurl())->getQuery()->getResult()
        );
        $importZip->errors['failed'] = [];

        for ($i = 0; $i < $dataCount; $i += self::MAX_QUERY_PARAMS) {
            $identifiers = [];
            for ($j = 0; $j < self::MAX_QUERY_PARAMS && ($i + $j) < $dataCount; $j++) {
                $index = $i + $j;
                if (!$this->checkSingleData($index, $importZip, $currentLabels, $identifiers)) {
                    return false;
                }
            }
            foreach (
                $this->serviceLevelRepository->findByIdentifiersForTenant($tenant, $identifiers) as $serviceLevel
            ) {
                if (!isset($duplicates[$serviceLevel->getIdentifier()])) {
                    $duplicates[$serviceLevel->getIdentifier()] = true;
                    $importZip->errors['duplicates'][] = $serviceLevel->getIdentifier();
                    $importZip->errors['failed'][$serviceLevel->getIdentifier()] = $serviceLevel->getIdentifier();
                }
            }
        }
        return true;
    }

    private function checkSingleData(
        int $index,
        ImportZip $importZip,
        array $currentLabels,
        array &$identifiers
    ): bool {
        $requiredFields = ['identifier', 'name', 'public'];
        foreach ($requiredFields as $fieldname) {
            if (!isset($importZip->data[$index][$fieldname])) {
                $importZip->errors['fatal'] = sprintf("Format du fichier incorrect : champ '%s' manquant", $fieldname);
                return false;
            }
        }

        $identifiers[] = $importZip->data[$index]['identifier'];
        $this->checkLabels($index, $importZip, $currentLabels);

        return true;
    }

    private function getUser(): User
    {
        if (isset($this->user)) {
            return $this->user;
        }
        /** @var User $user */
        $user = $this->security->getUser();
        return $user;
    }

    private function getTenant(): Tenant
    {
        if (isset($this->tenant)) {
            return $this->tenant;
        }
        $request = $this->requestStack->getCurrentRequest();
        return $this->getUser()->getTenant($request);
    }

    #[Override]
    public function import(ImportZip $importZip): void
    {
        $user = $this->getUser();
        $tenant = $this->getTenant();

        $this->entityManager->beginTransaction();
        foreach ($importZip->labels as $missingLabel) {
            $this->labelManager->importMissingLabel($missingLabel, $tenant);
        }

        $labels = [];
        /** @var Label[] $labelResults */
        $labelResults = $this->labelRepository->queryForTenant($tenant->getBaseurl())->getQuery()->getResult();
        foreach ($labelResults as $label) {
            $labels[$label->getConcatLabelGroupName()] = $label;
        }
        unset($labelResults);
        foreach ($importZip->data as $serviceLevelData) {
            if (isset($importZip->errors['failed'][$serviceLevelData['identifier']])) {
                continue;
            }

            /** @var ServiceLevel $entity */
            $entity = ArrayToEntity::arrayToEntity($serviceLevelData, ServiceLevel::class);
            $entity->setTenant($tenant);

            $this->addLabelsToEntity($serviceLevelData['labels'] ?? [], $labels, $entity);

            foreach ($serviceLevelData['service_level_files'] ?? [] as $serviceLevelFileData) {
                $relativePath = $serviceLevelFileData['file']['id'] . '/' . $serviceLevelFileData['file']['name'];
                if (isset($importZip->files[$relativePath])) {
                    $file = FileManager::setDataFromPath($importZip->files[$relativePath]);
                    $this->entityManager->persist($file);

                    /** @var ServiceLevelFile $serviceLevelFile */
                    $serviceLevelFile = ArrayToEntity::arrayToEntity($serviceLevelFileData, ServiceLevelFile::class);
                    $serviceLevelFile
                        ->setServiceLevel($entity)
                        ->setFile($file);

                    $errors = $this->validator->validate($serviceLevelFile);
                    if (count($errors)) {
                        throw new Exception((string)$errors);
                    }
                    $this->entityManager->persist($serviceLevelFile);
                }
            }

            $activity = ActivityManager::newActivity($entity, 'import', $user);
            $this->entityManager->persist($activity);

            $entity
                ->addActivity($activity)
                ->setVersion(1)
                ->setState(VersionableEntityInterface::S_EDITING)
            ;
            $errors = $this->validator->validate($entity);
            if (count($errors)) {
                throw new Exception((string)$errors);
            }

            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();
        $this->entityManager->commit();
    }

    #[Override]
    public static function getFileName(): string
    {
        return 'niveau_service';
    }

    #[Override]
    public function getClassName(): string
    {
        return $this->serviceLevelRepository->getClassName();
    }
}
