<?php

namespace App\Service\Manager;

use App\Entity\VocabularyFile;
use App\Entity\Label;
use App\Entity\Tenant;
use App\Entity\Term;
use App\Entity\User;
use App\Entity\VersionableEntityInterface;
use App\Entity\Vocabulary;
use App\Repository\LabelRepository;
use App\Repository\VocabularyRepository;
use App\Service\ArrayToEntity;
use App\Service\ExportZip;
use App\Service\ImportZip;
use App\Service\NewVersion;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Override;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use ZipArchive;

class VocabularyManager implements ImportExportManagerInterface
{
    use ImportLabelTrait;

    public ?User $user = null;
    public ?Tenant $tenant = null;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly RequestStack $requestStack,
        private readonly Security $security,
        private readonly ExportZip $exportZip,
        private readonly VocabularyRepository $vocabularyRepository,
        private readonly LabelRepository $labelRepository,
        private readonly ValidatorInterface $validator,
        private readonly NewVersion $newVersion,
        private readonly LabelManager $labelManager,
    ) {
    }

    public function newVocabulary(Tenant $tenant): Vocabulary
    {
        $vocabulary = new Vocabulary();
        $vocabulary
            ->setState($vocabulary->getInitialState())
            ->setVersion(1)
            ->setTenant($tenant);
        return $vocabulary;
    }

    public function save(Vocabulary $vocabulary): void
    {
        $this->entityManager->persist($vocabulary);
        $this->entityManager->flush();
    }

    public function newVersion(Vocabulary $vocabulary, string $reason): Vocabulary
    {
        /** @var Vocabulary $newVersion */
        $newVersion = $this->newVersion->fromEntity($vocabulary, $reason);
        $this->entityManager->persist($newVersion);

        foreach ($vocabulary->getTerms() as $term) {
            $newTerm = new Term($newVersion);
            $newTerm
                ->setIdentifier($term->getIdentifier())
                ->setName($term->getName())
                ->setDirectoryName($term->getDirectoryName());
            $this->entityManager->persist($newTerm);

            $newVersion->addTerm($newTerm); // permet d'afficher le nombre de termes dans le retour ajax du versionnage
        }

        foreach ($vocabulary->getVocabularyFiles() as $vocabularyFile) {
            $file = FileManager::duplicate($vocabularyFile->getFile());
            $newVocabularyFile = new VocabularyFile($newVersion);
            $newVocabularyFile
                ->setFile($file)
                ->setDescription($vocabularyFile->getDescription())
                ->setType($vocabularyFile->getType());
            $this->entityManager->persist($newVocabularyFile);
        }

        $this->entityManager->flush();
        return $newVersion;
    }

    #[Override]
    public function export(QueryBuilder $query): string
    {
        return $this->exportZip->fromQuery(
            $query,
            Vocabulary::class,
            function (Vocabulary $vocabulary, ZipArchive $zip, array $normalized) {
                foreach ($vocabulary->getLabels() as $label) {
                    $normalized['labels'][] = $label->export();
                }
                foreach ($vocabulary->getTerms() as $term) {
                    $normalized['terms'][] = $term->export();
                }
                foreach ($vocabulary->getVocabularyFiles() as $vocabularyFile) {
                    $file = $vocabularyFile->getFile();
                    $zip->addFromString(
                        $vocabulary->getId() . '/' . $file->getName(),
                        stream_get_contents($file->getContent())
                    );
                    $normalized['files'][] = $file->export();
                }
                return $normalized;
            },
        );
    }

    #[Override]
    public function checkForImport(ImportZip $importZip): bool
    {
        $request = $this->requestStack->getCurrentRequest();
        /** @var User $user */
        $user = $this->security->getUser();
        $tenant = $user->getTenant($request);
        if (!$tenant) {
            return true;
        }

        $dataCount = count($importZip->data);
        $duplicates = [];
        $currentLabels = array_map(
            fn(Label $l) => $l->getConcatLabelGroupName(),
            $this->labelRepository->queryForTenant($tenant->getBaseurl())->getQuery()->getResult()
        );
        $importZip->errors['failed'] = [];

        for ($i = 0; $i < $dataCount; $i += self::MAX_QUERY_PARAMS) {
            $identifiers = [];
            for ($j = 0; $j < self::MAX_QUERY_PARAMS && ($i + $j) < $dataCount; $j++) {
                $index = $i + $j;
                if (!$this->checkSingleData($index, $importZip, $currentLabels, $identifiers)) {
                    return false;
                }
            }
            foreach ($this->vocabularyRepository->findByIdentifiersForTenant($tenant, $identifiers) as $vocabulary) {
                if (!isset($duplicates[$vocabulary->getIdentifier()])) {
                    $duplicates[$vocabulary->getIdentifier()] = true;
                    $importZip->errors['duplicates'][] = $vocabulary->getIdentifier();
                    $importZip->errors['failed'][$vocabulary->getIdentifier()] = $vocabulary->getIdentifier();
                }
            }
        }
        return true;
    }

    private function checkSingleData(
        int $index,
        ImportZip $importZip,
        array $currentLabels,
        array &$identifiers
    ): bool {
        $requiredFields = ['identifier', 'name', 'public'];
        foreach ($requiredFields as $fieldname) {
            if (!isset($importZip->data[$index][$fieldname])) {
                $importZip->errors['fatal'] = sprintf("Format du fichier incorrect : champ '%s' manquant", $fieldname);
                return false;
            }
        }

        $identifiers[] = $importZip->data[$index]['identifier'];
        $this->checkLabels($index, $importZip, $currentLabels);

        return true;
    }

    private function getUser(): User
    {
        if (isset($this->user)) {
            return $this->user;
        }
        /** @var User $user */
        $user = $this->security->getUser();
        return $user;
    }

    private function getTenant(): Tenant
    {
        if (isset($this->tenant)) {
            return $this->tenant;
        }
        $request = $this->requestStack->getCurrentRequest();
        return $this->getUser()->getTenant($request);
    }

    #[Override]
    public function import(ImportZip $importZip): void
    {
        $user = $this->getUser();
        $tenant = $this->getTenant();

        $this->entityManager->beginTransaction();
        foreach ($importZip->labels as $missingLabel) {
            $this->labelManager->importMissingLabel($missingLabel, $tenant);
        }

        $labels = [];
        /** @var Label[] $labelResults */
        $labelResults = $this->labelRepository->queryForTenant($tenant->getBaseurl())->getQuery()->getResult();
        foreach ($labelResults as $label) {
            $labels[$label->getConcatLabelGroupName()] = $label;
        }
        unset($labelResults);
        foreach ($importZip->data as $vocabularyData) {
            if (isset($importZip->errors['failed'][$vocabularyData['identifier']])) {
                continue;
            }

            /** @var Vocabulary $entity */
            $entity = ArrayToEntity::arrayToEntity($vocabularyData, Vocabulary::class);
            $entity->setTenant($tenant);

            $this->addLabelsToEntity($vocabularyData['labels'] ?? [], $labels, $entity);

            foreach ($vocabularyData['terms'] ?? [] as $termData) {
                $term = new Term();
                $term
                    ->setName($termData['name'])
                    ->setIdentifier($termData['identifier'] ?? null)
                    ->setDirectoryName($termData['directoryName'] ?? null)
                    ->setVocabulary($entity);
                $this->entityManager->persist($term);
            }

            foreach ($vocabularyData['vocabularies_files'] ?? [] as $vocabularyFileData) {
                $relativePath = $vocabularyFileData['file']['id'] . '/' . $vocabularyFileData['file']['name'];
                if (isset($importZip->files[$relativePath])) {
                    $file = FileManager::setDataFromPath($importZip->files[$relativePath]);
                    $this->entityManager->persist($file);

                    /** @var VocabularyFile $vocabularyFile */
                    $vocabularyFile = ArrayToEntity::arrayToEntity($vocabularyFileData, VocabularyFile::class);
                    $vocabularyFile
                        ->setVocabulary($entity)
                        ->setFile($file);

                    $errors = $this->validator->validate($vocabularyFile);
                    if (count($errors)) {
                        throw new Exception((string)$errors);
                    }
                    $this->entityManager->persist($vocabularyFile);
                }
            }

            $activity = ActivityManager::newActivity($entity, 'import', $user);
            $this->entityManager->persist($activity);

            $entity
                ->addActivity($activity)
                ->setVersion(1)
                ->setState(VersionableEntityInterface::S_EDITING);
            $errors = $this->validator->validate($entity);
            if (count($errors)) {
                throw new Exception((string)$errors);
            }

            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();
        $this->entityManager->commit();
    }

    #[Override]
    public static function getFileName(): string
    {
        return 'vocabulaire_controle';
    }

    #[Override]
    public function getClassName(): string
    {
        return $this->vocabularyRepository->getClassName();
    }
}
