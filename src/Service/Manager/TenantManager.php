<?php

namespace App\Service\Manager;

use App\Entity\ManagementRule;
use App\Entity\Tenant;
use App\Entity\VersionableEntityInterface;
use App\Repository\AgreementRepository;
use App\Repository\AuthorityRepository;
use App\Repository\CategoryRepository;
use App\Repository\DocumentRuleRepository;
use App\Repository\LabelRepository;
use App\Repository\ManagementRuleRepository;
use App\Repository\ProfileRepository;
use App\Repository\RuleTypeRepository;
use App\Repository\ServiceLevelRepository;
use App\Repository\TenantRepository;
use App\Repository\VocabularyRepository;
use App\Service\ArrayToEntity;
use App\Service\AttributeExtractor;
use App\Service\ImportZip;
use App\Service\SessionFiles;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\QueryBuilder;
use DOMDocument;
use DOMElement;
use DOMXPath;
use Exception;
use FilesystemIterator;
use Override;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Filesystem\Filesystem;
use ZipArchive;

class TenantManager implements ImportExportManagerInterface
{
    public const string EXPORT_FILENAME = 'tenant.json';
    public array $errors = [];
    public array $data = [];
    public array $previewData = [];
    public array $files = [];
    public int $dataCount = 0;
    public int $fileCount = 0;
    public int $failedCount = 0;
    public int $importableCount = 0;
    public bool $hasFatalError = false;
    public ?DateTime $exportDate = null;
    public ?ImportZip $importZip = null;
    /**
     * @var ImportZip[]|null[]
     */
    public array $nestedImports = [];
    private string $tmpDir;

    public function __construct(
        private readonly AgreementManager $agreementManager,
        private readonly AgreementRepository $agreementRepository,
        private readonly AuthorityManager $authorityManager,
        private readonly AuthorityRepository $authorityRepository,
        private readonly CategoryManager $categoryManager,
        private readonly CategoryRepository $categoryRepository,
        private readonly DocumentRuleManager $documentRuleManager,
        private readonly DocumentRuleRepository $documentRuleRepository,
        private readonly LabelManager $labelManager,
        private readonly LabelRepository $labelRepository,
        private readonly ManagementRuleManager $managementRuleManager,
        private readonly ManagementRuleRepository $managementRuleRepository,
        private readonly ProfileManager $profileManager,
        private readonly ProfileRepository $profileRepository,
        private readonly ServiceLevelManager $serviceLevelManager,
        private readonly ServiceLevelRepository $serviceLevelRepository,
        private readonly SessionFiles $sessionFiles,
        private readonly TenantRepository $tenantRepository,
        private readonly VocabularyManager $vocabularyManager,
        private readonly VocabularyRepository $vocabularyRepository,
        private readonly RuleTypeRepository $ruleTypeRepository,
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security,
        private readonly string $accessRestrictionCodesXsd,
    ) {
    }

    #[Override]
    public function export(QueryBuilder $query): string
    {
        $tenant = $query->getQuery()->getResult()[0];
        $extractor = new AttributeExtractor($tenant);
        $properties = $extractor->getPropertiesHaving(ORM\Column::class);
        $normalized = $extractor->normalizeEntityByProperties($properties, $tenant);

        $tmpFile = tmpfile();
        $archiveName = stream_get_meta_data($tmpFile)['uri'];

        $zip = new ZipArchive();
        if (!$zip->open($archiveName, ZipArchive::OVERWRITE)) {
            throw new Exception('Unable to open tmpfile for zip creation');
        }

        $jsonFlags = JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT;
        $zip->addFromString(
            self::EXPORT_FILENAME,
            json_encode($normalized, $jsonFlags)
        );

        $now = (new DateTime())->format('Y-m-d_His');
        $zip->addFromString(
            sprintf('%s_export.agreements.refae', $now),
            $this->agreementManager->export(
                $this->agreementRepository->queryForExport($tenant)
            ),
        );
        $zip->addFromString(
            sprintf('%s_export.authorities.refae', $now),
            $this->authorityManager->export(
                $this->authorityRepository->queryForExport($tenant)
            ),
        );
        $zip->addFromString(
            sprintf('%s_export.categories.refae', $now),
            $this->categoryManager->export(
                $this->categoryRepository->queryForExport($tenant)
            ),
        );
        $zip->addFromString(
            sprintf('%s_export.document_rules.refae', $now),
            $this->documentRuleManager->export(
                $this->documentRuleRepository->queryForExport($tenant)
            ),
        );
        $zip->addFromString(
            sprintf('%s_export.labels.refae', $now),
            $this->labelManager->export(
                $this->labelRepository->queryForExport($tenant)
            ),
        );
        $zip->addFromString(
            sprintf('%s_export.management_rules.refae', $now),
            $this->managementRuleManager->export(
                $this->managementRuleRepository->queryForExport($tenant)
            ),
        );
        $zip->addFromString(
            sprintf('%s_export.profiles.refae', $now),
            $this->profileManager->export(
                $this->profileRepository->queryForExport($tenant)
            ),
        );
        $zip->addFromString(
            sprintf('%s_export.service_levels.refae', $now),
            $this->serviceLevelManager->export(
                $this->serviceLevelRepository->queryForExport($tenant)
            ),
        );
        $zip->addFromString(
            sprintf('%s_export.vocabularies.refae', $now),
            $this->vocabularyManager->export(
                $this->vocabularyRepository->queryForExport($tenant)
            ),
        );

        if (!$zip->close()) {
            throw new Exception("Erreur lors de la fermeture du fichier zip");
        }
        $content = file_get_contents($archiveName);
        fclose($tmpFile);

        return $content;
    }

    #[Override]
    public function import(ImportZip $importZip): void
    {
        $user = $this->security->getUser();
        $this->entityManager->beginTransaction();

        // on commence par créer le tenant
        $tenant = ArrayToEntity::arrayToEntity($importZip->data, Tenant::class);
        $activity = ActivityManager::newActivity($tenant, 'import', $user);
        $this->entityManager->persist($tenant);
        $this->entityManager->persist($activity);

        try {
            $this->entityManager->flush();

            // ensuite on importe les labels
            if (isset($this->nestedImports['labels'])) {
                $this->labelManager->user = $user;
                $this->labelManager->tenant = $tenant;
                $this->labelManager->import($this->nestedImports['labels']);
            }

            // enfin, on importe le reste
            $toImport = [
                'agreements',
                'authorities',
                'categories',
                'document_rules',
                'management_rules',
                'profiles',
                'serviceLevels',
                'vocabularies',
            ];
            foreach ($toImport as $nested) {
                if (!isset($this->nestedImports[$nested])) {
                    continue;
                }
                $manager = $this->nestedImports[$nested]->manager;
                if (
                    property_exists($manager, 'tenant')
                    && property_exists($manager, 'user')
                ) {
                    $manager->user = $user;
                    $manager->tenant = $tenant;
                }
                $manager->import($this->nestedImports[$nested]);
            }

            $this->entityManager->flush();
            $this->entityManager->commit();
        } catch (Exception $e) {
            $this->entityManager->rollback();
            throw $e;
        }
    }

    public function extract(string $session_tmpfile_uuid)
    {
        $zipFile = $this->sessionFiles->getUri($session_tmpfile_uuid);
        $this->tmpDir = sys_get_temp_dir() . '/' . $session_tmpfile_uuid;
        if (is_dir($this->tmpDir)) {
            $filesystem = new Filesystem();
            $filesystem->remove($this->tmpDir);
        }

        $zip = new ZipArchive();
        $this->errors = [];
        $this->dataCount = 0;
        $this->failedCount = 0;
        $this->files = [];
        try {
            if (!$zip->open($zipFile)) {
                throw new Exception("Echec lors de l'ouverture du fichier");
            }
            $this->fileCount = $zip->count() - 1;
            if (!$zip->extractTo($this->tmpDir)) {
                throw new Exception("Echec lors de l'écriture des fichiers temporaire");
            }
            $strlen = strlen($this->tmpDir) + 1;
            $iterator = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($this->tmpDir, FilesystemIterator::SKIP_DOTS),
                RecursiveIteratorIterator::SELF_FIRST
            );
            foreach ($iterator as $path => $info) {
                if ($info->isFile()) {
                    $this->files[substr((string) $path, $strlen)] = $path;
                }
            }
            $jsonName = self::EXPORT_FILENAME;
            if (!isset($this->files[$jsonName])) {
                throw new Exception("Format du fichier incorrect");
            }
            $json = json_decode(file_get_contents($this->files[$jsonName]), true);
            if (empty($json)) {
                throw new Exception("Format du fichier incorrect");
            }
            $this->data = $json;
            $this->dataCount = 1;
            $this->exportDate = (new DateTime())->setTimestamp(filemtime($this->files[$jsonName]));

            $this->hasFatalError = empty($json['baseurl'])
                || $this->tenantRepository->count(['baseurl' => $json['baseurl']]) > 0
            ;
            $this->failedCount = 0;

            $this->extractNested();
        } catch (Exception $e) {
            $this->hasFatalError = true;
            $this->errors['fatal'] = $e->getMessage();
        }
        $this->importableCount = $this->hasFatalError ? 0 : $this->dataCount - $this->failedCount;
        if ($this->importableCount === 0 && empty($this->errors['fatal'])) {
            $this->errors['fatal'] = "Aucun import n'est possible";
            $this->hasFatalError = true;
        }
        $this->previewData = [$this->data];
        $this->importZip = new ImportZip($this->sessionFiles);
        $this->importZip->data = $this->data;
    }

    #[Override]
    public function checkForImport(ImportZip $importZip): bool
    {
        return true;
    }

    #[Override]
    public static function getFileName(): string
    {
        return 'tenant';
    }

    #[Override]
    public function getClassName(): string
    {
        return $this->tenantRepository->getClassName();
    }

    private function getNestedExportedFilename(string $ext): ?string
    {
        foreach ($this->files as $filename) {
            if (str_ends_with((string) $filename, $ext)) {
                return $filename;
            }
        }
        return null;
    }

    private function extractNested()
    {
        $aggrements = $this->getNestedExportedFilename('.agreements.refae');
        if ($aggrements) {
            $this->nestedImports['agreements'] = new ImportZip($this->sessionFiles);
            $this->nestedImports['agreements']->unzip(
                $aggrements,
                $this->tmpDir . '/agreements',
                $this->agreementManager,
            );
            $this->nestedImports['agreements']->manager = $this->agreementManager;
        }

        $authorities = $this->getNestedExportedFilename('.authorities.refae');
        if ($authorities) {
            $this->nestedImports['authorities'] = new ImportZip($this->sessionFiles);
            $this->nestedImports['authorities']->unzip(
                $authorities,
                $this->tmpDir . '/authorities',
                $this->authorityManager,
            );
            $this->nestedImports['authorities']->manager = $this->authorityManager;
        }

        $categories = $this->getNestedExportedFilename('.categories.refae');
        if ($categories) {
            $this->nestedImports['categories'] = new ImportZip($this->sessionFiles);
            $this->nestedImports['categories']->unzip(
                $categories,
                $this->tmpDir . '/categories',
                $this->categoryManager,
            );
            $this->nestedImports['categories']->manager = $this->categoryManager;
        }

        $documentRules = $this->getNestedExportedFilename('.document_rules.refae');
        if ($documentRules) {
            $this->nestedImports['document_rules'] = new ImportZip($this->sessionFiles);
            $this->nestedImports['document_rules']->unzip(
                $documentRules,
                $this->tmpDir . '/document_rules',
                $this->documentRuleManager,
            );
            $this->nestedImports['document_rules']->manager = $this->documentRuleManager;
        }

        $labels = $this->getNestedExportedFilename('.labels.refae');
        if ($labels) {
            $this->nestedImports['labels'] = new ImportZip($this->sessionFiles);
            $this->nestedImports['labels']->unzip(
                $labels,
                $this->tmpDir . '/labels',
                $this->labelManager,
            );
            $this->nestedImports['labels']->manager = $this->labelManager;
        }

        $managementRules = $this->getNestedExportedFilename('.management_rules.refae');
        if ($managementRules) {
            $this->nestedImports['management_rules'] = new ImportZip($this->sessionFiles);
            $this->nestedImports['management_rules']->unzip(
                $managementRules,
                $this->tmpDir . '/management_rules',
                $this->managementRuleManager,
            );
            $this->nestedImports['management_rules']->manager = $this->managementRuleManager;
        }

        $profiles = $this->getNestedExportedFilename('.profiles.refae');
        if ($profiles) {
            $this->nestedImports['profiles'] = new ImportZip($this->sessionFiles);
            $this->nestedImports['profiles']->unzip(
                $profiles,
                $this->tmpDir . '/profiles',
                $this->profileManager,
            );
            $this->nestedImports['profiles']->manager = $this->profileManager;
        }

        $serviceLevels = $this->getNestedExportedFilename('.serviceLevels.refae');
        if ($serviceLevels) {
            $this->nestedImports['serviceLevels'] = new ImportZip($this->sessionFiles);
            $this->nestedImports['serviceLevels']->unzip(
                $serviceLevels,
                $this->tmpDir . '/serviceLevels',
                $this->serviceLevelManager,
            );
            $this->nestedImports['serviceLevels']->manager = $this->serviceLevelManager;
        }

        $vocabularies = $this->getNestedExportedFilename('.vocabularies.refae');
        if ($vocabularies) {
            $this->nestedImports['vocabularies'] = new ImportZip($this->sessionFiles);
            $this->nestedImports['vocabularies']->unzip(
                $vocabularies,
                $this->tmpDir . '/vocabularies',
                $this->vocabularyManager,
            );
            $this->nestedImports['vocabularies']->manager = $this->vocabularyManager;
        }
    }

    public function nestedImportCount(string $nestedName): int
    {
        if (isset($this->nestedImports[$nestedName])) {
            return $this->nestedImports[$nestedName]->dataCount;
        }
        return 0;
    }

    public function deleteByForm(Tenant $tenant, array $formData): void
    {
        $this->entityManager->beginTransaction();
        try {
            if ($formData['delete_users']) {
                $this->deleteOrphanUsers($tenant);
            }
            if ($formData['delete_ldaps']) {
                $this->deleteOrphanLdaps($tenant);
            }
            if ($formData['delete_openids']) {
                $this->deleteOrphanOpenids($tenant);
            }

            $this->entityManager->remove($tenant);
            $this->entityManager->flush();

            $this->entityManager->commit();
        } catch (Exception $e) {
            $this->entityManager->rollback();
            throw $e;
        }
    }

    private function deleteOrphanUsers(Tenant $tenant)
    {
        foreach ($tenant->getUsers() as $user) {
            if (
                $user->getAccessUsers()->count() === 1
                && $user->getAccessUsers()->first()->getTenant() === $tenant
            ) {
                $this->entityManager->remove($user);
            }
        }
    }

    private function deleteOrphanLdaps(Tenant $tenant)
    {
        foreach ($tenant->getLdaps() as $ldap) {
            if (
                $ldap->getTenants()->count() === 1
                && $ldap->getTenants()->first() === $tenant
            ) {
                $this->entityManager->remove($ldap);
            }
        }
    }

    private function deleteOrphanOpenids(Tenant $tenant)
    {
        foreach ($tenant->getOpenids() as $openid) {
            if (
                $openid->getTenants()->count() === 1
                && $openid->getTenants()->first() === $tenant
            ) {
                $this->entityManager->remove($openid);
            }
        }
    }

    public function initialize(Tenant $tenant, array $params)
    {
        if (!$params['initAccessRules'] ?? false) {
            return;
        }
        $dom = new DOMDocument();
        $dom->load($this->accessRestrictionCodesXsd);
        $xpath = new DOMXPath($dom);
        $xpath->registerNamespace('xsd', 'http://www.w3.org/2001/XMLSchema');
        $xpath->registerNamespace(
            'ccts',
            'urn:un:unece:uncefact:documentation:standard:CoreComponentsTechnicalSpecification:2'
        );

        $query = $xpath->query(
            "xsd:simpleType[@name='AccessRestrictionCodeType']"
            . "/xsd:restriction/xsd:enumeration"
        );
        $ruleType = $this->ruleTypeRepository->findOneBy(['identifier' => 'AccessRule', 'tenant' => null]);

        /** @var DOMElement $node */
        foreach ($query as $node) {
            $identifier = $node->getAttribute('value');
            $doc = $xpath->query('.//xsd:documentation', $node)->item(0);
            $name = $xpath->query('.//ccts:Name', $doc)->item(0)->nodeValue;
            $description = $xpath->query('.//ccts:Description', $doc)->item(0)->nodeValue;

            if (preg_match('/^(\d+) ans?/', (string) $name, $m)) {
                $duration = 'P' . $m[1] . 'Y';
            } else {
                $duration = 'P999Y';
            }

            if ($this->managementRuleRepository->count(['tenant' => $tenant, 'identifier' => $identifier]) === 0) {
                $managementRule = new ManagementRule();
                $managementRule->setIdentifier($identifier);
                $managementRule->setName($name);
                $managementRule->setDescription($description);
                $managementRule->setRuleType($ruleType);
                $managementRule->setTenant($tenant);
                $managementRule->setDuration($duration);
                $managementRule->setPublic(true);
                $managementRule->setVersion(1);
                $managementRule->setState(VersionableEntityInterface::S_PUBLISHED);
                $this->entityManager->persist($managementRule);
            }
        }
    }

    public function save(Tenant $tenant, array $params)
    {
        $this->entityManager->persist($tenant);
        $this->initialize($tenant, $params);

        $this->entityManager->flush();
    }
}
