<?php

namespace App\Service\Manager;

use App\Entity\Agreement;
use App\Entity\AgreementArchivalAgencyIdentifier;
use App\Entity\AgreementFile;
use App\Entity\AgreementOriginatingAgencyIdentifier;
use App\Entity\AgreementProfileIdentifier;
use App\Entity\AgreementTransferringAgencyIdentifier;
use App\Entity\Label;
use App\Entity\Tenant;
use App\Entity\User;
use App\Entity\VersionableEntityInterface;
use App\Repository\AgreementRepository;
use App\Repository\LabelRepository;
use App\Service\ArrayToEntity;
use App\Service\ExportZip;
use App\Service\ImportZip;
use App\Service\NewVersion;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Override;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use ZipArchive;

class AgreementManager implements ImportExportManagerInterface
{
    use ImportLabelTrait;

    public ?User $user = null;
    public ?Tenant $tenant = null;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly RequestStack $requestStack,
        private readonly Security $security,
        private readonly ExportZip $exportZip,
        private readonly AgreementRepository $agreementRepository,
        private readonly LabelRepository $labelRepository,
        private readonly ValidatorInterface $validator,
        private readonly NewVersion $newVersion,
        private readonly LabelManager $labelManager,
    ) {
    }

    public function newAgreement(Tenant $tenant): Agreement
    {
        $agreement = new Agreement();
        $agreement
            ->setState($agreement->getInitialState())
            ->setVersion(1)
            ->setTenant($tenant)
        ;
        return $agreement;
    }

    public function save(Agreement $agreement): void
    {
        $this->entityManager->persist($agreement);
        $this->entityManager->flush();
    }

    public function newVersion(Agreement $agreement, string $reason): Agreement
    {
        /** @var Agreement $newVersion */
        $newVersion = $this->newVersion->fromEntity($agreement, $reason);
        $this->entityManager->persist($newVersion);

        foreach ($agreement->getProfileIdentifiers() as $profile) {
            $newProfile = new AgreementProfileIdentifier($profile);
            $newVersion->addProfileIdentifier($newProfile);
            $this->entityManager->persist($newProfile);
        }
        foreach ($agreement->getArchivalAgencyIdentifiers() as $archivalAgency) {
            $newVersion->addArchivalAgencyIdentifier($archivalAgency);
        }
        foreach ($agreement->getOriginatingAgencyIdentifiers() as $originatingAgency) {
            $newVersion->addOriginatingAgencyIdentifier($originatingAgency);
        }
        foreach ($agreement->getTransferringAgencyIdentifiers() as $transferringAgency) {
            $newVersion->addTransferringAgencyIdentifier($transferringAgency);
        }

        foreach ($agreement->getAgreementFiles() as $agreementFile) {
            $file = FileManager::duplicate($agreementFile->getFile());
            $newAgreementFile = new AgreementFile($newVersion);
            $newAgreementFile
                ->setFile($file)
                ->setDescription($agreementFile->getDescription())
                ->setType($agreementFile->getType());
            $this->entityManager->persist($newAgreementFile);
        }

        $this->entityManager->flush();
        return $newVersion;
    }

    #[Override]
    public function export(QueryBuilder $query): string
    {
        return $this->exportZip->fromQuery(
            $query,
            Agreement::class,
            function (Agreement $agreement, ZipArchive $zip, array $normalized) {
                foreach ($agreement->getLabels() as $label) {
                    $normalized['labels'][] = $label->export();
                }
                foreach ($agreement->getProfileIdentifiers() as $profile) {
                    $normalized['profiles'][] = $profile->export();
                }
                foreach ($agreement->getArchivalAgencyIdentifiers() as $archivalAgency) {
                    $normalized['archival_agencies'][] = $archivalAgency->export();
                }
                foreach ($agreement->getOriginatingAgencyIdentifiers() as $originatingAgency) {
                    $normalized['originating_agencies'][] = $originatingAgency->export();
                }
                foreach ($agreement->getTransferringAgencyIdentifiers() as $transferringAgency) {
                    $normalized['transferring_agencies'][] = $transferringAgency->export();
                }
                foreach ($agreement->getAgreementFiles() as $agreementFile) {
                    $file = $agreementFile->getFile();
                    $zip->addFromString(
                        $file->getId() . '/' . $file->getName(),
                        stream_get_contents($file->getContent())
                    );
                    $normalized['agreement_files'][] = $agreementFile->export();
                }
                return $normalized;
            },
        );
    }

    #[Override]
    public function checkForImport(ImportZip $importZip): bool
    {
        $request = $this->requestStack->getCurrentRequest();
        /** @var User $user */
        $user = $this->security->getUser();
        $tenant = $user->getTenant($request);
        if (!$tenant) {
            return true;
        }

        $dataCount = count($importZip->data);
        $duplicates = [];
        $currentLabels = array_map(
            fn(Label $l) => $l->getConcatLabelGroupName(),
            $this->labelRepository->queryForTenant($tenant->getBaseurl())->getQuery()->getResult()
        );
        $importZip->errors['failed'] = [];

        for ($i = 0; $i < $dataCount; $i += self::MAX_QUERY_PARAMS) {
            $identifiers = [];
            for ($j = 0; $j < self::MAX_QUERY_PARAMS && ($i + $j) < $dataCount; $j++) {
                $index = $i + $j;
                if (!$this->checkSingleData($index, $importZip, $currentLabels, $identifiers)) {
                    return false;
                }
            }
            foreach ($this->agreementRepository->findByIdentifiersForTenant($tenant, $identifiers) as $agreement) {
                if (!isset($duplicates[$agreement->getIdentifier()])) {
                    $duplicates[$agreement->getIdentifier()] = true;
                    $importZip->errors['duplicates'][] = $agreement->getIdentifier();
                    $importZip->errors['failed'][$agreement->getIdentifier()] = $agreement->getIdentifier();
                }
            }
        }
        return true;
    }

    private function checkSingleData(
        int $index,
        ImportZip $importZip,
        array $currentLabels,
        array &$identifiers
    ): bool {
        $requiredFields = ['identifier', 'name', 'public'];
        foreach ($requiredFields as $fieldname) {
            if (!isset($importZip->data[$index][$fieldname])) {
                $importZip->errors['fatal'] = sprintf("Format du fichier incorrect : champ '%s' manquant", $fieldname);
                return false;
            }
        }

        $identifiers[] = $importZip->data[$index]['identifier'];
        $this->checkLabels($index, $importZip, $currentLabels);

        return true;
    }

    private function getUser(): User
    {
        if (isset($this->user)) {
            return $this->user;
        }
        /** @var User $user */
        $user = $this->security->getUser();
        return $user;
    }

    private function getTenant(): Tenant
    {
        if (isset($this->tenant)) {
            return $this->tenant;
        }
        $request = $this->requestStack->getCurrentRequest();
        return $this->getUser()->getTenant($request);
    }

    #[Override]
    public function import(ImportZip $importZip): void
    {
        $user = $this->getUser();
        $tenant = $this->getTenant();

        $this->entityManager->beginTransaction();
        foreach ($importZip->labels as $missingLabel) {
            $this->labelManager->importMissingLabel($missingLabel, $tenant);
        }

        $labels = [];
        /** @var Label[] $labelResults */
        $labelResults = $this->labelRepository->queryForTenant($tenant->getBaseurl())->getQuery()->getResult();
        foreach ($labelResults as $label) {
            $labels[$label->getConcatLabelGroupName()] = $label;
        }
        unset($labelResults);
        foreach ($importZip->data as $agreementData) {
            if (isset($importZip->errors['failed'][$agreementData['identifier']])) {
                continue;
            }

            /** @var Agreement $entity */
            $entity = ArrayToEntity::arrayToEntity($agreementData, Agreement::class);
            $entity->setTenant($tenant);

            $this->addLabelsToEntity($agreementData['labels'] ?? [], $labels, $entity);

            foreach ($agreementData['agreement_files'] ?? [] as $agreementFileData) {
                $relativePath = $agreementFileData['file']['id'] . '/' . $agreementFileData['file']['name'];
                if (isset($importZip->files[$relativePath])) {
                    $file = FileManager::setDataFromPath($importZip->files[$relativePath]);
                    $this->entityManager->persist($file);

                    /** @var AgreementFile $agreementFile */
                    $agreementFile = ArrayToEntity::arrayToEntity($agreementFileData, AgreementFile::class);
                    $agreementFile
                        ->setAgreement($entity)
                        ->setFile($file);

                    $errors = $this->validator->validate($agreementFile);
                    if (count($errors)) {
                        throw new Exception((string)$errors);
                    }
                    $this->entityManager->persist($agreementFile);
                }
            }
            foreach ($agreementData['profiles'] ?? [] as $profile) {
                $profileIdentifier = (new AgreementProfileIdentifier())
                    ->setIdentifier($profile['identifier'])
                    ->setName($profile['name'])
                    ->setAgreement($entity);
                $this->entityManager->persist($profileIdentifier);
            }
            foreach ($agreementData['archival_agencies'] ?? [] as $archivalAgency) {
                $archivalAgencyIdentifier = (new AgreementArchivalAgencyIdentifier())
                    ->setIdentifier($archivalAgency['identifier'])
                    ->setName($archivalAgency['name'])
                    ->setAgreement($entity);
                $this->entityManager->persist($archivalAgencyIdentifier);
            }
            foreach ($agreementData['originating_agencies'] ?? [] as $originatingAgencies) {
                $originatingAgenciesIdentifier = (new AgreementOriginatingAgencyIdentifier())
                    ->setIdentifier($originatingAgencies['identifier'])
                    ->setName($originatingAgencies['name'])
                    ->setAgreement($entity);
                $this->entityManager->persist($originatingAgenciesIdentifier);
            }
            foreach ($agreementData['transferring_agencies'] ?? [] as $transferringAgencies) {
                $transferringAgenciesIdentifier = (new AgreementTransferringAgencyIdentifier())
                    ->setIdentifier($transferringAgencies['identifier'])
                    ->setName($transferringAgencies['name'])
                    ->setAgreement($entity);
                $this->entityManager->persist($transferringAgenciesIdentifier);
            }

            $activity = ActivityManager::newActivity($entity, 'import', $user);
            $this->entityManager->persist($activity);

            $entity
                ->addActivity($activity)
                ->setVersion(1)
                ->setState(VersionableEntityInterface::S_EDITING)
            ;
            $errors = $this->validator->validate($entity);
            if (count($errors)) {
                throw new Exception((string)$errors);
            }

            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();
        $this->entityManager->commit();
    }

    #[Override]
    public static function getFileName(): string
    {
        return 'accord_versement';
    }

    #[Override]
    public function getClassName(): string
    {
        return $this->agreementRepository->getClassName();
    }
}
