<?php

/** @noinspection PhpInternalEntityUsedInspection */

namespace App\Service\Manager;

use App\Entity\Activity;
use App\Entity\ActivitySubjectEntityInterface;
use App\Entity\Tenant;
use App\Entity\User;
use App\Repository\ActivityRepository;
use DateInterval;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\Proxy\InternalProxy;
use ReflectionClass;

readonly class ActivityManager
{
    public function __construct(
        private ActivityRepository $activityRepository,
    ) {
    }

    public static function newActivity(
        ActivitySubjectEntityInterface $entity,
        string $action,
        User|string $user,
        ?string $subject = null,
    ): Activity {
        $reflectionClass = new ReflectionClass($entity);
        if ($reflectionClass->implementsInterface(InternalProxy::class)) {
            $reflectionClass = $reflectionClass->getParentClass();
            $foreignModel = $reflectionClass->getName();
        } else {
            $foreignModel = $entity::class;
        }
        $activity = new Activity();
        $activity
            ->setActivityEntity($entity)
            ->setCreated(new DateTime())
            ->setAction($action)
            ->setSubject($subject ?? $entity->getActivityName())
            ->setForeignModel($foreignModel)
            ->setScope($entity->getActivityScope())
        ;

        if ($user instanceof User) {
            $activity->setCreatedUser($user);
        } else {
            $activity->setUserDeletedName($user);
        }

        return $activity;
    }

    public function getChartData(Tenant $tenant): array
    {
        $now = new DateTimeImmutable();
        $sevenDays = new DateInterval('P7D');
        $specificDay = $now->sub($sevenDays);
        $oneDay = new DateInterval('P1D');
        $result = [];
        while ($specificDay < $now) {
            $specificDay = $specificDay->add($oneDay);
            $dayResult = [
                'day' => (int)$specificDay->format('d'),
            ];
            foreach ($this->getChartDataForSpecificDay($specificDay, $tenant) as $model => $actions) {
                foreach ($actions as $action => $count) {
                    $dayResult['count_' . $action] = ($dayResult['count_' . $action] ?? 0) + $count;
                    $dayResult['count_' . $action . '_' . $model] = $count;
                }
            }
            $result[] = $dayResult;
        }
        return $result;
    }

    private function getChartDataForSpecificDay(DateTimeImmutable $specificDay, Tenant $tenant): array
    {
        $queryBuilder = $this->activityRepository->findForChart($specificDay->format('Y-m-d'), $tenant);
        $dayResult = [];
        /** @var Activity $activity */
        foreach ($queryBuilder->getQuery()->getResult() as $activity) {
            $ref =& $dayResult[$activity->getModelName()][$activity->getAction()];
            $ref = ($ref ?: 0) + 1;
        }
        return $dayResult;
    }
}
