<?php

namespace App\Service\Manager;

use App\Entity\Category;
use App\Entity\DocumentRuleFile;
use App\Entity\Label;
use App\Entity\DocumentRule;
use App\Entity\Tenant;
use App\Entity\User;
use App\Entity\VersionableEntityInterface;
use App\Repository\CategoryRepository;
use App\Repository\LabelGroupRepository;
use App\Repository\LabelRepository;
use App\Repository\DocumentRuleRepository;
use App\Service\ArrayToEntity;
use App\Service\ExportZip;
use App\Service\ImportZip;
use App\Service\NewVersion;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Override;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use ZipArchive;

class DocumentRuleManager implements ImportExportManagerInterface
{
    use ImportLabelTrait;

    public ?User $user = null;
    public ?Tenant $tenant = null;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly RequestStack $requestStack,
        private readonly Security $security,
        private readonly ExportZip $exportZip,
        private readonly DocumentRuleRepository $documentRuleRepository,
        private readonly LabelRepository $labelRepository,
        private readonly LabelGroupRepository $labelGroupRepository,
        private readonly CategoryRepository $categoryRepository,
        private readonly ValidatorInterface $validator,
        private readonly NewVersion $newVersion,
        private readonly LabelManager $labelManager,
    ) {
    }

    public function newDocumentRule(Tenant $tenant): DocumentRule
    {
        $documentRule = new DocumentRule();
        $documentRule
            ->setState($documentRule->getInitialState())
            ->setVersion(1)
            ->setTenant($tenant)
        ;
        return $documentRule;
    }

    public function save(DocumentRule $documentRule): void
    {
        $this->entityManager->persist($documentRule);
        $this->entityManager->flush();
    }

    public function newVersion(DocumentRule $documentRule, string $reason): DocumentRule
    {
        /** @var DocumentRule $newVersion */
        $newVersion = $this->newVersion->fromEntity($documentRule, $reason);
        $newVersion
            ->setAppraisalRule($documentRule->getAppraisalRule())
            ->setAccessRule($documentRule->getAccessRule());

        $this->entityManager->persist($newVersion);

        foreach ($documentRule->getCategories() as $category) {
            $newVersion->addCategory($category);
        }

        foreach ($documentRule->getDocumentRuleFiles() as $documentRuleFile) {
            $file = FileManager::duplicate($documentRuleFile->getFile());
            $newDocumentRuleFile = new DocumentRuleFile($newVersion);
            $newDocumentRuleFile
                ->setFile($file)
                ->setDescription($documentRuleFile->getDescription())
                ->setType($documentRuleFile->getType());
            $this->entityManager->persist($newDocumentRuleFile);
        }

        $this->entityManager->flush();
        return $newVersion;
    }

    #[Override]
    public function export(QueryBuilder $query): string
    {
        return $this->exportZip->fromQuery(
            $query,
            DocumentRule::class,
            function (DocumentRule $documentRule, ZipArchive $zip, array $normalized) {
                foreach ($documentRule->getLabels() as $label) {
                    $normalized['labels'][] = $label->export();
                }
                foreach ($documentRule->getCategories() as $category) {
                    $normalized['categories'][] = $this->categoryRepository->getPathAsString($category);
                }
                foreach ($documentRule->getDocumentRuleFiles() as $documentRuleFile) {
                    $file = $documentRuleFile->getFile();
                    $zip->addFromString(
                        $file->getId() . '/' . $file->getName(),
                        stream_get_contents($file->getContent())
                    );
                    $normalized['document_rule_files'][] = $documentRuleFile->export();
                }
                return $normalized;
            },
        );
    }

    #[Override]
    public function checkForImport(ImportZip $importZip): bool
    {
        $request = $this->requestStack->getCurrentRequest();
        /** @var User $user */
        $user = $this->security->getUser();
        $tenant = $user->getTenant($request);
        if (!$tenant) {
            return true;
        }

        $dataCount = count($importZip->data);
        $duplicates = [];
        $currentLabels = array_map(
            fn(Label $l) => $l->getConcatLabelGroupName(),
            $this->labelRepository->queryForTenant($tenant->getBaseurl())->getQuery()->getResult()
        );
        $currentCategories = array_map(
            fn(Category $c) => $this->categoryRepository->getPathAsString($c),
            $this->categoryRepository->queryForTenant($tenant->getBaseurl())->getQuery()->getResult()
        );

        $importZip->errors['failed'] = [];

        for ($i = 0; $i < $dataCount; $i += self::MAX_QUERY_PARAMS) {
            $identifiers = [];
            for ($j = 0; $j < self::MAX_QUERY_PARAMS && ($i + $j) < $dataCount; $j++) {
                $index = $i + $j;
                if (!$this->checkSingleData($index, $importZip, $currentLabels, $currentCategories, $identifiers)) {
                    return false;
                }
            }
            foreach (
                $this->documentRuleRepository->findByIdentifiersForTenant($tenant, $identifiers) as $documentRule
            ) {
                if (!isset($duplicates[$documentRule->getIdentifier()])) {
                    $duplicates[$documentRule->getIdentifier()] = true;
                    $importZip->errors['duplicates'][] = $documentRule->getIdentifier();
                    $importZip->errors['failed'][$documentRule->getIdentifier()] = $documentRule->getIdentifier();
                }
            }
        }
        return true;
    }

    private function checkSingleData(
        int $index,
        ImportZip $importZip,
        array $currentLabels,
        array $currentCategories,
        array &$identifiers
    ): bool {
        $requiredFields = ['identifier', 'name', 'public'];
        foreach ($requiredFields as $fieldname) {
            if (!isset($importZip->data[$index][$fieldname])) {
                $importZip->errors['fatal'] = sprintf("Format du fichier incorrect : champ '%s' manquant", $fieldname);
                return false;
            }
        }

        $identifiers[] = $importZip->data[$index]['identifier'];
        $this->checkLabels($index, $importZip, $currentLabels);
        $this->checkCategories($index, $importZip, $currentCategories);

        return true;
    }

    private function getUser(): User
    {
        if (isset($this->user)) {
            return $this->user;
        }
        /** @var User $user */
        $user = $this->security->getUser();
        return $user;
    }

    private function getTenant(): Tenant
    {
        if (isset($this->tenant)) {
            return $this->tenant;
        }
        $request = $this->requestStack->getCurrentRequest();
        return $this->getUser()->getTenant($request);
    }

    #[Override]
    public function import(ImportZip $importZip): void
    {
        $user = $this->getUser();
        $tenant = $this->getTenant();

        $this->entityManager->beginTransaction();
        foreach (array_keys($importZip->errors['missing_categories'] ?? []) as $missingCategory) {
            $this->importMissingCategory($missingCategory, $tenant);
        }
        foreach ($importZip->labels as $missingLabel) {
            $this->labelManager->importMissingLabel($missingLabel, $tenant);
        }

        $labels = [];
        /** @var Label[] $labelResults */
        $labelResults = $this->labelRepository->queryForTenant($tenant->getBaseurl())->getQuery()->getResult();
        foreach ($labelResults as $label) {
            $labels[$label->getConcatLabelGroupName()] = $label;
        }
        unset($labelResults);

        $categories = [];
        /** @var Category[] $categoryResults */
        $categoryResults = $this->categoryRepository->queryForTenant($tenant->getBaseurl())->getQuery()->getResult();
        foreach ($categoryResults as $category) {
            $categories[$this->categoryRepository->getPathAsString($category)] = $category;
        }
        unset($categoryResults);

        foreach ($importZip->data as $documentRuleData) {
            if (isset($importZip->errors['failed'][$documentRuleData['identifier']])) {
                continue;
            }

            /** @var DocumentRule $entity */
            $entity = ArrayToEntity::arrayToEntity($documentRuleData, DocumentRule::class);
            $entity->setTenant($tenant);

            $this->addLabelsToEntity($documentRuleData['labels'] ?? [], $labels, $entity);
            $this->addCategoriesToEntity($documentRuleData['categories'] ?? [], $categories, $entity);

            foreach ($documentRuleData['document_rule_files'] ?? [] as $documentRuleFileData) {
                $relativePath = $documentRuleFileData['file']['id'] . '/' . $documentRuleFileData['file']['name'];
                if (isset($importZip->files[$relativePath])) {
                    $file = FileManager::setDataFromPath($importZip->files[$relativePath]);
                    $this->entityManager->persist($file);

                    /** @var DocumentRuleFile $documentRuleFile */
                    $documentRuleFile = ArrayToEntity::arrayToEntity($documentRuleFileData, DocumentRuleFile::class);
                    $documentRuleFile
                        ->setDocumentRule($entity)
                        ->setFile($file);

                    $errors = $this->validator->validate($documentRuleFile);
                    if (count($errors)) {
                        throw new Exception((string)$errors);
                    }
                    $this->entityManager->persist($documentRuleFile);
                }
            }

            $activity = ActivityManager::newActivity($entity, 'import', $user);
            $this->entityManager->persist($activity);

            $entity
                ->addActivity($activity)
                ->setVersion(1)
                ->setState(VersionableEntityInterface::S_EDITING)
            ;
            $errors = $this->validator->validate($entity);
            if (count($errors)) {
                throw new Exception((string)$errors);
            }

            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();
        $this->entityManager->commit();
    }

    private function addCategoriesToEntity(
        array $categoryData,
        array $categories,
        DocumentRule $entity
    ): void {
        foreach ($categoryData as $category) {
            $entity->addCategory($categories[$category]);
        }
    }

    private function checkCategories(
        int $index,
        ImportZip $importZip,
        array $currentCategories
    ): void {
        foreach ($importZip->data[$index]['categories'] ?? [] as $category) {
            if (!in_array($category, $currentCategories)) {
                $importZip->errors['missing_categories'][$category][] = $importZip->data[$index]['identifier'];
            }
        }
    }

    #[Override]
    public static function getFileName(): string
    {
        return 'regle_documentaire';
    }

    #[Override]
    public function getClassName(): string
    {
        return $this->documentRuleRepository->getClassName();
    }

    private function importMissingCategory(string $missingCategory, Tenant $tenant)
    {
        $categories = explode(' > ', $missingCategory);
        $rootName = array_shift($categories);
        $root = $this->categoryRepository->findOneBy([
            'tenant' => $tenant,
            'parent' => null,
            'name' => $rootName,
        ]);
        if ($root === null) {
            $root = new Category();
            $root->setTenant($tenant);
            $root->setName($rootName);
            $this->entityManager->persist($root);
            $this->entityManager->flush();
        }
        $parent = $root;
        foreach ($categories as $categoryName) {
            $category = $this->categoryRepository->findOneBy([
                'tenant' => $tenant,
                'parent' => $parent,
                'name' => $categoryName,
            ]);
            if ($category === null) {
                $category = new Category();
                $category->setTenant($tenant);
                $category->setName($categoryName);
                $category->setParent($parent);
                $this->entityManager->persist($category);
                $this->entityManager->flush();
            }
            $parent = $category;
        }
    }
}
