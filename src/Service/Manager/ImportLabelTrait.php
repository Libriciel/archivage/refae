<?php

namespace App\Service\Manager;

use App\Entity\HaveLabelsEntityInterface;
use App\Service\ImportZip;

trait ImportLabelTrait
{
    protected function checkLabels(
        int $index,
        ImportZip $importZip,
        array $currentConcatLabelGroups,
    ): void {
        foreach ($importZip->data[$index]['labels'] ?? [] as $label) {
            $concatLabelGroupName = $this->getConcatLabelGroupName($label);
            if (!in_array($concatLabelGroupName, $currentConcatLabelGroups)) {
                $importZip->errors['missing_labels'][$concatLabelGroupName][] = $importZip->data[$index]['identifier'];
                $importZip->labels[$concatLabelGroupName] = $label;
            }
        }
    }

    protected function getConcatLabelGroupName(array $labelData): string
    {
        return !empty($labelData['group']['name'])
            ? $labelData['group']['name'] . ':' . $labelData['name']
            : $labelData['name'];
    }

    protected function addLabelsToEntity(
        array $labelData,
        array $labels,
        HaveLabelsEntityInterface $entity
    ): void {
        foreach ($labelData as $label) {
            $labelGroup = $this->getConcatLabelGroupName($label);
            $entity->addLabel($labels[$labelGroup]);
        }
    }
}
