<?php

namespace App\Service\Manager;

use App\Entity\Category;
use App\Entity\DocumentRule;
use App\Entity\Tenant;
use App\Entity\User;
use App\Repository\CategoryRepository;
use App\Service\ExportZip;
use App\Service\ImportZip;
use App\Service\NewVersion;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Override;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use ZipArchive;

class CategoryManager implements ImportExportManagerInterface
{
    public ?User $user = null;
    public ?Tenant $tenant = null;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly RequestStack $requestStack,
        private readonly Security $security,
        private readonly ExportZip $exportZip,
        private readonly CategoryRepository $categoryRepository,
        private readonly ValidatorInterface $validator,
        private readonly NewVersion $newVersion,
    ) {
    }

    public function newCategory(Tenant $tenant): Category
    {
        $category = new Category();
        $category
            ->setTenant($tenant)
        ;
        return $category;
    }

    public function save(Category $category): void
    {
        $this->entityManager->persist($category);
        $this->entityManager->flush();
        $category->setPath($this->categoryRepository->getPathAsString($category, ['separator' => ' ⮕ ']));
    }

    public function newVersion(Category $category, string $reason): Category
    {
        $newVersion = $this->newVersion->fromEntity($category, $reason);
        $this->entityManager->persist($newVersion);

        $this->entityManager->flush();
        return $newVersion;
    }

    #[Override]
    public function export(QueryBuilder $query): string
    {
        return $this->exportZip->fromQuery(
            $query,
            Category::class,
            function (Category $category, ZipArchive $zip, array $normalized) {
                $normalized['parent'] = $category->getParent()?->getId();
                $normalized['root'] = $category->getRoot()?->getId() ?? '';
                return $normalized;
            },
        );
    }

    #[Override]
    public function checkForImport(ImportZip $importZip): bool
    {
        $request = $this->requestStack->getCurrentRequest();
        /** @var User $user */
        $user = $this->security->getUser();
        $tenant = $user->getTenant($request);
        if (!$tenant) {
            return true;
        }

        $dataCount = count($importZip->data);
        $duplicates = [];
        $importZip->errors['failed'] = [];

        for ($i = 0; $i < $dataCount; $i += self::MAX_QUERY_PARAMS) {
            $names = [];
            for ($j = 0; $j < self::MAX_QUERY_PARAMS && ($i + $j) < $dataCount; $j++) {
                $index = $i + $j;
                if (!$this->checkSingleData($index, $importZip, $names)) {
                    return false;
                }
            }
            foreach ($this->categoryRepository->findByNameForTenant($tenant, $names) as $category) {
                if (!isset($duplicates[$category->getName()])) {
                    $duplicates[$category->getName()] = true;
                    $importZip->errors['duplicates'][] = $category->getName();
                    $importZip->errors['failed'][$category->getName()] = $category->getName();
                }
            }
        }
        return true;
    }

    private function checkSingleData(
        int $index,
        ImportZip $importZip,
        array &$names
    ): bool {
        $requiredFields = ['name', 'root'];
        foreach ($requiredFields as $fieldname) {
            if (!isset($importZip->data[$index][$fieldname])) {
                $importZip->errors['fatal'] = sprintf("Format du fichier incorrect : champ '%s' manquant", $fieldname);
                return false;
            }
        }

        $names[] = $importZip->data[$index]['name'];
        $ids = [];
        foreach ($importZip->data[$index]['categories'] ?? [] as $category) {
            if (!isset($category['name'])) {
                continue;
            }
            $ids[] = $category['id'];
            if (!in_array($category['root'], $ids)) {
                $importZip->errors['missing_roots'][] = $importZip->data[$index]['name'];
                $importZip->errors['failed'][$importZip->data[$index]['name']]
                    = $importZip->data[$index]['name'];
            }
            if (!in_array($category['parent'], $ids)) {
                $importZip->errors['missing_parents'][] = $importZip->data[$index]['name'];
                $importZip->errors['failed'][$importZip->data[$index]['name']]
                    = $importZip->data[$index]['name'];
            }
        }
        return true;
    }

    private function getUser(): User
    {
        if (isset($this->user)) {
            return $this->user;
        }
        /** @var User $user */
        $user = $this->security->getUser();
        return $user;
    }

    private function getTenant(): Tenant
    {
        if (isset($this->tenant)) {
            return $this->tenant;
        }
        $request = $this->requestStack->getCurrentRequest();
        return $this->getUser()->getTenant($request);
    }

    #[Override]
    public function import(ImportZip $importZip): void
    {
        $user = $this->getUser();
        $tenant = $this->getTenant();

        unset($categoryResults);
        $ids = [];
        foreach ($importZip->data as $categoryData) {
            if (isset($importZip->errors['failed'][$categoryData['name']])) {
                continue;
            }

            /** @var Category $entity */
            $entity = new Category();
            $entity->setTenant($tenant);
            $entity->setName($categoryData['name']);
            $entity->setDescription($categoryData['description']);
            if ($categoryData['parent']) {
                $entity->setParent($ids[$categoryData['parent']]);
            }

            $errors = $this->validator->validate($entity);
            if (count($errors)) {
                throw new Exception((string)$errors);
            }

            $this->entityManager->persist($entity);
            $ids[$categoryData['id']] = $entity;

            $activity = ActivityManager::newActivity($entity, 'import', $user);
            $this->entityManager->persist($activity);
        }
        $this->entityManager->flush();
    }

    public function fillDocumentRulePath(DocumentRule $documentRule, string $separator = ' ⮕ '): DocumentRule
    {
        foreach ($documentRule->getCategories() as $category) {
            $category->setPath($this->categoryRepository->getPathAsString($category, ['separator' => $separator]));
        }
        return $documentRule;
    }

    #[Override]
    public static function getFileName(): string
    {
        return 'categorie';
    }

    #[Override]
    public function getClassName(): string
    {
        return $this->categoryRepository->getClassName();
    }
}
