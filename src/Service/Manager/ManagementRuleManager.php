<?php

namespace App\Service\Manager;

use App\Entity\ManagementRuleFile;
use App\Entity\Label;
use App\Entity\ManagementRule;
use App\Entity\RuleType;
use App\Entity\Tenant;
use App\Entity\User;
use App\Entity\VersionableEntityInterface;
use App\Repository\LabelRepository;
use App\Repository\ManagementRuleRepository;
use App\Repository\RuleTypeRepository;
use App\Service\ArrayToEntity;
use App\Service\ExportZip;
use App\Service\ImportZip;
use App\Service\NewVersion;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Override;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use ZipArchive;

class ManagementRuleManager implements ImportExportManagerInterface
{
    use ImportLabelTrait;

    public ?User $user = null;
    public ?Tenant $tenant = null;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly RequestStack $requestStack,
        private readonly Security $security,
        private readonly ExportZip $exportZip,
        private readonly ManagementRuleRepository $managementRuleRepository,
        private readonly LabelRepository $labelRepository,
        private readonly RuleTypeRepository $ruleTypeRepository,
        private readonly ValidatorInterface $validator,
        private readonly NewVersion $newVersion,
        private readonly LabelManager $labelManager,
    ) {
    }

    public function newManagementRule(Tenant $tenant): ManagementRule
    {
        $managementRule = new ManagementRule();
        $managementRule
            ->setState($managementRule->getInitialState())
            ->setVersion(1)
            ->setTenant($tenant)
        ;
        return $managementRule;
    }

    public function save(ManagementRule $managementRule): void
    {
        $this->entityManager->persist($managementRule);
        $this->entityManager->flush();
    }

    public function newVersion(ManagementRule $managementRule, string $reason): ManagementRule
    {
        /** @var ManagementRule $newVersion */
        $newVersion = $this->newVersion->fromEntity($managementRule, $reason);
        $newVersion->setRuleType($managementRule->getRuleType());
        $this->entityManager->persist($newVersion);

        foreach ($managementRule->getManagementRuleFiles() as $managementRuleFile) {
            $file = FileManager::duplicate($managementRuleFile->getFile());
            $newManagementRuleFile = new ManagementRuleFile($newVersion);
            $newManagementRuleFile
                ->setFile($file)
                ->setDescription($managementRuleFile->getDescription())
                ->setType($managementRuleFile->getType());
            $this->entityManager->persist($newManagementRuleFile);
        }

        $this->entityManager->flush();
        return $newVersion;
    }

    #[Override]
    public function export(QueryBuilder $query): string
    {
        return $this->exportZip->fromQuery(
            $query,
            ManagementRule::class,
            function (ManagementRule $managementRule, ZipArchive $zip, array $normalized) {
                foreach ($managementRule->getLabels() as $label) {
                    $normalized['labels'][] = $label->export();
                }
                foreach ($managementRule->getManagementRuleFiles() as $managementRuleFile) {
                    $file = $managementRuleFile->getFile();
                    $zip->addFromString(
                        $file->getId() . '/' . $file->getName(),
                        stream_get_contents($file->getContent())
                    );
                    $normalized['management_rule_files'][] = $managementRuleFile->export();
                }
                $normalized['rule_type'] = $managementRule->getRuleType()->getIdentifier();
                return $normalized;
            },
        );
    }

    #[Override]
    public function checkForImport(ImportZip $importZip): bool
    {
        $request = $this->requestStack->getCurrentRequest();
        /** @var User $user */
        $user = $this->security->getUser();
        $tenant = $user->getTenant($request);
        if (!$tenant) {
            return true;
        }

        $dataCount = count($importZip->data);
        $duplicates = [];
        $currentLabels = array_map(
            fn(Label $label) => $label->getConcatLabelGroupName(),
            $this->labelRepository->queryForTenant($tenant->getBaseurl())->getQuery()->getResult()
        );

        $currentRuleTypes = array_map(
            fn(RuleType $rule) => $rule->getIdentifier(),
            $this->ruleTypeRepository->queryForTenant($tenant->getBaseurl())->getQuery()->getResult()
        );

        $importZip->errors['failed'] = [];

        for ($i = 0; $i < $dataCount; $i += self::MAX_QUERY_PARAMS) {
            $identifiers = [];
            for ($j = 0; $j < self::MAX_QUERY_PARAMS && ($i + $j) < $dataCount; $j++) {
                $index = $i + $j;
                if (!$this->checkSingleData($index, $importZip, $currentLabels, $currentRuleTypes, $identifiers)) {
                    return false;
                }
            }
            foreach (
                $this->managementRuleRepository->findByIdentifiersForTenant($tenant, $identifiers) as $managementRule
            ) {
                if (!isset($duplicates[$managementRule->getIdentifier()])) {
                    $duplicates[$managementRule->getIdentifier()] = true;
                    $importZip->errors['duplicates'][] = $managementRule->getIdentifier();
                    $importZip->errors['failed'][$managementRule->getIdentifier()] = $managementRule->getIdentifier();
                }
            }
        }
        return true;
    }

    private function checkSingleData(
        int $index,
        ImportZip $importZip,
        array $currentLabels,
        array $currentRuleTypes,
        array &$identifiers
    ): bool {
        $requiredFields = ['identifier', 'name', 'description', 'public', 'rule_type'];
        foreach ($requiredFields as $fieldname) {
            if (!isset($importZip->data[$index][$fieldname])) {
                $importZip->errors['fatal'] = sprintf("Format du fichier incorrect : champ '%s' manquant", $fieldname);
                return false;
            }
        }

        if (!in_array($importZip->data[$index]['rule_type'], $currentRuleTypes)) {
            $importZip->errors['missing_rule_types'][$importZip->data[$index]['rule_type']][]
                = $importZip->data[$index]['identifier'];
            $importZip->errors['failed'][$importZip->data[$index]['identifier']]
                = $importZip->data[$index]['identifier'];
        }

        $identifiers[] = $importZip->data[$index]['identifier'];
        $this->checkLabels($index, $importZip, $currentLabels);

        return true;
    }

    private function getUser(): User
    {
        if (isset($this->user)) {
            return $this->user;
        }
        /** @var User $user */
        $user = $this->security->getUser();
        return $user;
    }

    private function getTenant(): Tenant
    {
        if (isset($this->tenant)) {
            return $this->tenant;
        }
        $request = $this->requestStack->getCurrentRequest();
        return $this->getUser()->getTenant($request);
    }

    #[Override]
    public function import(ImportZip $importZip): void
    {
        $user = $this->getUser();
        $tenant = $this->getTenant();

        $this->entityManager->beginTransaction();
        foreach ($importZip->labels as $missingLabel) {
            $this->labelManager->importMissingLabel($missingLabel, $tenant);
        }

        $ruleTypes = [];
        /** @var RuleType[] $ruleTypesResults */
        $ruleTypesResults = $this->ruleTypeRepository->queryForTenant($tenant->getBaseurl())->getQuery()->getResult();
        foreach ($ruleTypesResults as $ruleType) {
            $ruleTypes[(string)$ruleType->getIdentifier()] = $ruleType;
        }
        unset($ruleTypesResults);

        $labels = [];
        /** @var Label[] $labelResults */
        $labelResults = $this->labelRepository->queryForTenant($tenant->getBaseurl())->getQuery()->getResult();
        foreach ($labelResults as $label) {
            $labels[$label->getConcatLabelGroupName()] = $label;
        }
        unset($labelResults);
        foreach ($importZip->data as $managementRuleData) {
            if (isset($importZip->errors['failed'][$managementRuleData['identifier']])) {
                continue;
            }

            /** @var ManagementRule $entity */
            $entity = ArrayToEntity::arrayToEntity($managementRuleData, ManagementRule::class);
            $entity->setTenant($tenant);

            $entity->setRuleType($ruleTypes[$managementRuleData['rule_type']]);

            $this->addLabelsToEntity($managementRuleData['labels'] ?? [], $labels, $entity);

            foreach ($managementRuleData['management_rule_files'] ?? [] as $managementRuleFileData) {
                $relativePath = $managementRuleFileData['file']['id'] . '/' . $managementRuleFileData['file']['name'];
                if (isset($importZip->files[$relativePath])) {
                    $file = FileManager::setDataFromPath($importZip->files[$relativePath]);
                    $this->entityManager->persist($file);

                    /** @var ManagementRuleFile $managementRuleFile */
                    $managementRuleFile = ArrayToEntity::arrayToEntity(
                        $managementRuleFileData,
                        ManagementRuleFile::class
                    );
                    $managementRuleFile
                        ->setManagementRule($entity)
                        ->setFile($file);
                    $errors = $this->validator->validate($managementRuleFile);
                    if (count($errors)) {
                        throw new Exception((string)$errors);
                    }
                    $this->entityManager->persist($managementRuleFile);
                }
            }

            $activity = ActivityManager::newActivity($entity, 'import', $user);
            $this->entityManager->persist($activity);

            $entity
                ->addActivity($activity)
                ->setVersion(1)
                ->setState(VersionableEntityInterface::S_EDITING);

            $errors = $this->validator->validate($entity);
            if (count($errors)) {
                throw new Exception((string)$errors);
            }

            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();
        $this->entityManager->commit();
    }

    #[Override]
    public static function getFileName(): string
    {
        return 'regle_gestion';
    }

    #[Override]
    public function getClassName(): string
    {
        return $this->managementRuleRepository->getClassName();
    }
}
