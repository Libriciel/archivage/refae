<?php

namespace App\Service\Manager;

use App\Entity\Label;
use App\Entity\LabelGroup;
use App\Entity\Tenant;
use App\Entity\User;
use App\Repository\LabelGroupRepository;
use App\Repository\LabelRepository;
use App\Service\ArrayToEntity;
use App\Service\ExportZip;
use App\Service\ImportZip;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Override;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use ZipArchive;

class LabelManager implements ImportExportManagerInterface
{
    public ?User $user = null;
    public ?Tenant $tenant = null;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ExportZip $exportZip,
        private readonly LabelRepository $labelRepository,
        private readonly LabelGroupRepository $labelGroupRepository,
        private readonly RequestStack $requestStack,
        private readonly Security $security,
        private readonly ValidatorInterface $validator,
    ) {
    }

    #[Override]
    public function export(QueryBuilder $query): string
    {
        return $this->exportZip->fromQuery(
            $query,
            Label::class,
            function (Label $label, ZipArchive $zip, array $normalized) {
                if ($label->getLabelGroupName()) {
                    $normalized['group'] = [
                        'name' => $label->getLabelGroupName(),
                        'description' => $label->getLabelGroupDescription(),
                    ];
                }
                return $normalized;
            },
        );
    }

    #[Override]
    public function checkForImport(ImportZip $importZip): bool
    {
        $request = $this->requestStack->getCurrentRequest();
        /** @var User $user */
        $user = $this->security->getUser();
        $tenant = $user->getTenant($request);
        if (!$tenant) {
            return true;
        }
        $dataCount = count($importZip->data);
        $duplicates = [];

        for ($index = 0; $index < $dataCount; $index++) {
            if (!$this->checkSingleData($index, $importZip)) {
                return false;
            }

            /** @var Label|null $exists */
            $label = $importZip->data[$index];
            $labelName = $label['name'];
            $labelGroupName = $label['group']['name'] ?? null;
            $exists = $this->labelRepository->findOneForImport($tenant, $labelName, $labelGroupName);
            $existConcatName = $exists?->getConcatLabelGroupName();

            if ($exists && !isset($duplicates[$existConcatName])) {
                $duplicates[$existConcatName] = true;
                $importZip->errors['duplicates'][] = $existConcatName;
                $importZip->errors['failed'][$existConcatName] = $existConcatName;
            }
        }
        return true;
    }

    private function checkSingleData(
        int $index,
        ImportZip $importZip,
    ): bool {
        $requiredFields = ['name', 'color'];
        foreach ($requiredFields as $fieldname) {
            if (!isset($importZip->data[$index][$fieldname])) {
                $importZip->errors['fatal'] = sprintf("Format du fichier incorrect : champ '%s' manquant", $fieldname);
                return false;
            }
        }

        return true;
    }

    private function getUser(): User
    {
        if (isset($this->user)) {
            return $this->user;
        }
        /** @var User $user */
        $user = $this->security->getUser();
        return $user;
    }

    private function getTenant(): Tenant
    {
        if (isset($this->tenant)) {
            return $this->tenant;
        }
        $request = $this->requestStack->getCurrentRequest();
        return $this->getUser()->getTenant($request);
    }

    #[Override]
    public function import(ImportZip $importZip): void
    {
        $user = $this->getUser();
        $tenant = $this->getTenant();
        $currentGroups = array_reduce(
            $this->labelGroupRepository->queryForTenant($tenant->getBaseurl())
                ->getQuery()
                ->getResult(),
            function (array $carry, LabelGroup $item): array {
                $carry[$item->getName()] = $item;
                return $carry;
            },
            []
        );

        foreach ($importZip->data as $labelData) {
            $concatName = (isset($labelData['group']) ? $labelData['group']['name'] . ':' : '' ) . $labelData['name'];
            if (isset($importZip->errors['failed'][$concatName])) {
                continue;
            }

            /** @var Label $entity */
            $entity = ArrayToEntity::arrayToEntity($labelData, Label::class);
            $entity->setTenant($tenant);

            if (isset($labelData['group'])) {
                if (!isset($currentGroups[$labelData['group']['name']])) {
                    $group = (new LabelGroup())
                        ->setName($labelData['group']['name'])
                        ->setDescription($labelData['group']['description'] ?? null)
                        ->setTenant($tenant);
                    $this->entityManager->persist($group);
                    $currentGroups[$labelData['group']['name']] = $group;
                } else {
                    $group = $currentGroups[$labelData['group']['name']];
                }
                $entity->setLabelGroup($group);
            }

            $activity = ActivityManager::newActivity($entity, 'import', $user);
            $this->entityManager->persist($activity);

            $entity->addActivity($activity);
            $errors = $this->validator->validate($entity);
            if (count($errors)) {
                throw new Exception((string)$errors);
            }

            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();
    }

    #[Override]
    public static function getFileName(): string
    {
        return 'etiquette';
    }

    #[Override]
    public function getClassName(): string
    {
        return $this->labelRepository->getClassName();
    }

    public function importMissingLabel(array $missingLabel, Tenant $tenant)
    {
        if (isset($missingLabel['group']['name'])) {
            $labelGroupName = $missingLabel['group']['name'];
            $labelGroup = $this->labelGroupRepository->findOneBy([
                'name' => $labelGroupName,
                'tenant' => $tenant
            ]);
            if ($labelGroup === null) {
                $labelGroup = new LabelGroup();
                $labelGroup->setName($labelGroupName);
                $labelGroup->setTenant($tenant);
                $this->entityManager->persist($labelGroup);
                $this->entityManager->flush();
            }
        } else {
            $labelGroup = null;
        }
        $label = $this->labelRepository->findOneBy([
            'name' => $missingLabel['name'],
            'labelGroup' => $labelGroup,
            'tenant' => $tenant,
        ]);
        if ($label === null) {
            $label = new Label();
            $label->setName($missingLabel['name']);
            $label->setLabelGroup($labelGroup);
            $label->setTenant($tenant);
            $label->setColor($missingLabel['color']);
            $this->entityManager->persist($label);
            $this->entityManager->flush();
        }
    }
}
