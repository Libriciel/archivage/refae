<?php

namespace App\Service;

use App\Entity\Authority;
use DateTime;
use DOMDocument;
use DOMElement;
use DOMXPath;

class Eaccpf
{
    public const string NS_2010 = 'urn:isbn:1-931666-33-4';
    public const string NS_V2 = 'https://archivists.org/ns/eac/v2';

    public function __construct(
        private readonly array $schemas,
    ) {
    }

    public function isValid(string $uri): bool
    {
        if (!is_file($uri)) {
            return false;
        }
        $dom = new DOMDocument();
        if (!$dom->load($uri)) {
            return false;
        }
        $xmlns = $dom->documentElement->getAttribute('xmlns');
        if (!isset($this->schemas[$xmlns])) {
            return false;
        }
        $schema = $this->schemas[$xmlns];
        return $dom->schemaValidate($schema);
    }

    public function applyDataToEntity(string $uri, Authority $authority): void
    {
        $dom = new DOMDocument();
        $dom->load($uri);
        $xmlns = $dom->documentElement->getAttribute('xmlns');
        $xpath = new DOMXPath($dom);
        $xpath->registerNamespace('ns', $xmlns);

        $identifierXpath = $xmlns === self::NS_2010 ? '//ns:recordId' : '//ns:identityId';
        $identifierNode = $xpath->query($identifierXpath)->item(0);
        if ($identifierNode) {
            $authority->setIdentifier($this->formatString($identifierNode->nodeValue));
        }
        $nameNode = $xpath->query('//ns:nameEntry/ns:part')->item(0);
        if ($nameNode) {
            $authority->setName($this->formatString($nameNode->nodeValue));
        }
        $descriptiveNode = $xpath->query('//ns:function/ns:descriptiveNote/ns:p')->item(0);
        if ($descriptiveNode) {
            $authority->setDescription($this->formatString($descriptiveNode->nodeValue));
        }
        if ($xmlns === self::NS_2010) {
            $maintenanceStatusNode = $xpath->query('//ns:maintenanceStatus')->item(0);
        } else {
            /** @var DOMElement $controlNode */
            $controlNode = $xpath->query('//ns:control')->item(0);
            $maintenanceStatusNode = $controlNode?->getAttributeNode('maintenanceStatus');
        }
        if ($maintenanceStatusNode) {
            $authority->setMaintenanceStatus($this->formatString($maintenanceStatusNode->nodeValue));
        }

        $from_date = null;
        $to_date = null;
        /** @var DOMElement $existDates */
        $existDates = $xpath->query('//ns:existDates')->item(0);
        if ($existDates) {
            // dateset contient plusieurs dates, on ne prend que la 1ère
            if (!$dateSet = $existDates->getElementsByTagName('dateSet')->item(0)) {
                $dateSet = $existDates;
            }

            /** @var DOMElement $dateRange */
            if ($dateRange = $dateSet->getElementsByTagName('dateRange')->item(0)) {
                if ($fromDate = $dateRange->getElementsByTagName('fromDate')->item(0)) {
                    $from_date = $this->elementToDate($fromDate);
                }
                if ($toDate = $dateRange->getElementsByTagName('toDate')->item(0)) {
                    $to_date = $this->elementToDate($toDate);
                }
            } elseif ($date = $dateSet->getElementsByTagName('date')->item(0)) {
                $from_date = $this->elementToDate($date);
            }
        }
        if ($from_date) {
            $authority->setExistDatesFrom($from_date);
        }
        if ($to_date) {
            $authority->setExistDatesTo($to_date);
        }
        /** @var DOMElement $entityTypeNode */
        $entityTypeNode = $xpath->query('//ns:entityType')->item(0);
        if ($entityTypeNode) {
            $authority->setEntityType(
                $this->formatString(
                    $xmlns === self::NS_2010
                        ? $entityTypeNode->nodeValue
                        : $entityTypeNode->getAttribute('value')
                )
            );
        }
        $legalStatusNode = $xpath->query('//ns:legalStatus/ns:term')->item(0);
        if ($legalStatusNode) {
            $authority->setLegalStatus($this->formatString($legalStatusNode->nodeValue));
        }
        $placeXpath = $xmlns === self::NS_2010
            ? '//ns:place/ns:placeEntry'
            : '//ns:description//ns:place/ns:placeName';
        $placeEntryNode = $xpath->query($placeXpath)->item(0);
        if ($placeEntryNode) {
            $authority->setPlace($this->formatString($placeEntryNode->nodeValue));
        }
        $functionTermNode = $xpath->query('//ns:function/ns:term')->item(0);
        if ($functionTermNode) {
            $authority->setFunction($this->formatString($functionTermNode->nodeValue));
        }
        $occupationTermNode = $xpath->query('//ns:occupation/ns:term')->item(0);
        if ($occupationTermNode) {
            $authority->setOccupation($this->formatString($occupationTermNode->nodeValue));
        }
        $mandateTermNode = $xpath->query('//ns:mandate/ns:term')->item(0);
        if ($mandateTermNode) {
            $authority->setMandate($this->formatString($mandateTermNode->nodeValue));
        }
    }

    private function formatString(string $str): string
    {
        return preg_replace('/\n +/', " ", $str);
    }

    /**
     * Permet d'obtenir une date à partir d'une element date
     *
     * Récupère en priorité dans l'ordre :
     * - l'attribut standardDate
     * - l'attribut notBefore
     * - l'attribut notAfter
     * Sinon essaye de récupérer la date dans le texte
     *
     * @link http://eac.staatsbibliothek-berlin.de/Diagram/cpf.html#id57
     * @link https://en.wikipedia.org/wiki/ISO_8601
     *
     * @return DateTime|null Null si non trouvé
     */
    private function elementToDate(DOMElement $element): ?DateTime
    {
        if (
            ($attr = $element->getAttribute('standardDate'))
            || ($attr = $element->getAttribute('notBefore'))
            || ($attr = $element->getAttribute('notAfter'))
        ) {
            if (preg_match('/^\d{4}$/', $attr)) {
                $format = '!Y';
            } elseif (preg_match('/^\d{4}-\d{2}$/', $attr)) {
                $format = '!Y-m';
            } elseif (preg_match('/^\d{6}$/', $attr)) {
                $format = '!Ym';
            } elseif (preg_match('/^\d{8}$/', $attr)) {
                $format = '!Ymd';
            } else {
                $format = '!Y-m-d';
            }
            return DateTime::createFromFormat($format, $attr);
        }
        // Si une date normé n'est pas trouvé, on tente de récupérer la date dans le texte
        if (preg_match('/(\d{4})-(\d{1,2})-(\d{1,2})/', $element->textContent, $matches)) {
            return DateTime::createFromFormat('!Y-m-d', "$matches[1]-$matches[2]-$matches[3]");
        } elseif (preg_match('/(\d{4})\/(\d{1,2})\/(\d{1,2})/', $element->textContent, $matches)) {
            return DateTime::createFromFormat('!Y-m-d', "$matches[1]-$matches[2]-$matches[3]");
        } elseif (preg_match('/(\d{1,2})\/(\d{1,2})\/(\d{4})/', $element->textContent, $matches)) {
            return DateTime::createFromFormat('!Y-m-d', "$matches[3]-$matches[2]-$matches[1]");
        } elseif (preg_match('/(\d{4})/', $element->textContent, $matches)) {
            return DateTime::createFromFormat('!Y', $matches[1]);
        } else {
            return null;
        }
    }
}
