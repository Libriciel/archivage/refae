<?php

namespace App\Service;

use App\Entity\ActivitySubjectEntityInterface;
use App\Entity\User;
use App\Entity\VersionableEntityInterface;
use App\Repository\VersionableEntityRepositoryInterface;
use App\Service\Manager\ActivityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\HttpException;

readonly class NewVersion
{
    public function __construct(
        private Security $security,
        private RequestStack $requestStack,
        private ActivityManager $activityManager,
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function fromEntity(
        VersionableEntityInterface $entity,
        string $reason = '',
    ) {
        $versionNumber = $entity->getVersion() + 1;
        $repository = $this->entityManager->getRepository($entity::class);
        if ($repository instanceof VersionableEntityRepositoryInterface) {
            /** @var VersionableEntityInterface $lastEntity */
            $lastEntity = $repository->queryOnlyLastVersionForTenant(
                $entity->getTenant()?->getBaseurl(),
                $entity->getIdentifier(),
            )->getQuery()->getResult();
            $lastEntity = $lastEntity ? $lastEntity[0] : null;
            if ($lastEntity && $lastEntity->getVersion() > $entity->getVersion()) {
                throw new HttpException(403, "Une version plus récente existe déjà");
            }
        }

        $entityClassname = $entity::class;
        $newVersion = new $entityClassname();
        $extractor = new AttributeExtractor($newVersion);
        $properties = $extractor->getPropertiesHaving(ORM\Column::class);
        foreach ($properties as $property) {
            $value = $property->getValue($entity);
            if ($value instanceof LazyUuidFromString) {
                continue;
            }
            $property->setValue($newVersion, $property->getValue($entity));
        }
        $newVersion->setVersion($versionNumber);
        $newVersion->setState($newVersion->getInitialState());
        $newVersion->setTenant($entity->getTenant());

        foreach ($entity->getLabels() as $label) {
            $newVersion->addLabel($label);
        }

        if ($newVersion instanceof ActivitySubjectEntityInterface) {
            $request = $this->requestStack->getCurrentRequest();
            $user = $this->security->getUser();
            $activity = $this->activityManager->newActivity(
                entity: $newVersion,
                action: 'new-version',
                user: $user instanceof User ? $user : $request?->getClientIp() ?? '',
                subject: sprintf(
                    '%s version %d : %s',
                    $newVersion->getActivityName(),
                    $versionNumber,
                    $reason
                ),
            );
            $this->entityManager->persist($activity);
        }

        return $newVersion;
    }
}
