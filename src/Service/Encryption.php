<?php

namespace App\Service;

class Encryption
{
    public function encrypt(string $text): string
    {
        $firstKey = base64_decode((string) $_ENV['ENC_KEY1']);
        $secondKey = base64_decode((string) $_ENV['ENC_KEY2']);

        $method = "aes-256-cbc";
        $iv_length = openssl_cipher_iv_length($method);
        $iv = openssl_random_pseudo_bytes($iv_length);

        $firstEncrypted = openssl_encrypt(
            $text,
            $method,
            $firstKey,
            OPENSSL_RAW_DATA,
            $iv
        );
        $secondEncrypted = hash_hmac(
            'sha3-512',
            $firstEncrypted,
            $secondKey,
            true
        );

        return base64_encode($iv . $secondEncrypted . $firstEncrypted);
    }

    public function decrypt(string $text): string
    {
        $firstKey = base64_decode((string) $_ENV['ENC_KEY1']);
        $secondKey = base64_decode((string) $_ENV['ENC_KEY2']);
        $mix = base64_decode($text);

        $method = "aes-256-cbc";
        $ivLength = openssl_cipher_iv_length($method);

        $iv = substr($mix, 0, $ivLength);
        $secondEncrypted = substr($mix, $ivLength, 64);
        $firstEncrypted = substr($mix, $ivLength + 64);

        $data = openssl_decrypt(
            $firstEncrypted,
            $method,
            $firstKey,
            OPENSSL_RAW_DATA,
            $iv
        );
        $second_encrypted_new = hash_hmac(
            'sha3-512',
            $firstEncrypted,
            $secondKey,
            true
        );

        return hash_equals($secondEncrypted, $second_encrypted_new) ? $data : '';
    }
}
