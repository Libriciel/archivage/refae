<?php

namespace App\Service;

use App\Entity\Attribute;
use App\Entity\MeilisearchIndexedInterface;
use App\Entity\VersionableEntityInterface;
use App\Repository\VersionableEntityRepositoryInterface;
use App\Serializer\UuidNormalizer;
use Doctrine\ORM\EntityManagerInterface;
use Meilisearch\Client;
use Meilisearch\Endpoints\Indexes;
use Override;
use ReflectionAttribute;
use ReflectionMethod;
use ReflectionProperty;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AttributeLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

readonly class Meilisearch implements MeilisearchInterface
{
    public Client $client;
    private Serializer $serializer;

    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
        $classMetadataFactory = new ClassMetadataFactory(new AttributeLoader());
        $normalizers = [
            new UuidNormalizer(),
            new ObjectNormalizer($classMetadataFactory),
        ];
        $this->serializer = new Serializer($normalizers);

        $this->client = new Client($_ENV['MEILI_HOST'], $_ENV['MEILI_MASTER_KEY']);
    }

    #[Override]
    public function getIndex(string $indexName): Indexes
    {
        $index = $this->client->index($indexName);
        $index->updateFilterableAttributes([
            'stateTranslation', 'public', 'tenantUrl', 'flattenLabels', 'last_version',
        ]);
        $index->updateSortableAttributes(['version']);
        $index->updateDistinctAttribute('identifier');

        return $index;
    }

    #[Override]
    public function classnameToIndexname(string $classname): string
    {
        return substr($classname, strrpos($classname, '\\') + 1);
    }

    #[Override]
    public function getIndexByObject(MeilisearchIndexedInterface $object): Indexes
    {
        $classname = $object::class;
        $indexName = $this->classnameToIndexname($classname);

        return $this->getIndex($indexName);
    }

    #[Override]
    public function add(MeilisearchIndexedInterface $object): void
    {
        $index = $this->getIndexByObject($object);
        $repository = $this->entityManager->getRepository($object::class);
        if (
            $repository instanceof VersionableEntityRepositoryInterface
            && $object instanceof VersionableEntityInterface
        ) {
            $lastVersion = $repository->findPreviousVersion($object);
            if ($lastVersion instanceof VersionableEntityInterface) {
                $data = $this->normalize($lastVersion);
                $data['last_version'] = false;
                $index->updateDocuments([$data], 'id');
            }
        }
        $data = $this->normalize($object);
        $data['last_version'] = true;
        $index->addDocuments([$data], 'id');
    }

    #[Override]
    public function update(MeilisearchIndexedInterface $object): void
    {
        $data = $this->normalize($object);
        $index = $this->getIndexByObject($object);
        $index->updateDocuments([$data], 'id');
    }

    #[Override]
    public function delete(MeilisearchIndexedInterface $object): void
    {
        $index = $this->getIndexByObject($object);
        $repository = $this->entityManager->getRepository($object::class);
        if (
            $repository instanceof VersionableEntityRepositoryInterface
            && $object instanceof VersionableEntityInterface
        ) {
            $lastVersion = $repository->findPreviousVersion($object);
            if ($lastVersion instanceof VersionableEntityInterface) {
                $data = $this->normalize($lastVersion);
                $data['last_version'] = true;
                $index->updateDocuments([$data], 'id');
            }
        }
        $index->deleteDocument((string)$object->getId());
    }

    #[Override]
    public function search(string $indexName, string $text, array $searchParams = []): array
    {
        $index = $this->getIndex($indexName);
        $result = $index->search($text, $searchParams)->toArray();

        $classname = 'App\\Entity\\' . $indexName;
        if (class_exists($classname)) {
            $extractor = new AttributeExtractor($classname);
            $result['translation'] = [];
            $extractor->getPropertiesHaving(
                Attribute\ResultSet::class,
                function (ReflectionProperty $property, ReflectionAttribute $attribute) use (&$result) {
                    $result['translation'][$property->getName()] = $attribute->getArguments()[0];
                }
            );
            $extractor->getMethodsHaving(
                Attribute\ResultSet::class,
                function (ReflectionMethod $method, ReflectionAttribute $attribute) use (&$result) {
                    $methodName = $method->getName();
                    $name = str_starts_with($methodName, 'get') ? lcfirst(substr($methodName, 3)) : $methodName;
                    $result['translation'][$name] = $attribute->getArguments()[0];
                }
            );
        }

        $result['hits'] = array_filter(
            $result['hits'],
            fn (array $r) => !empty($r['_matchesPosition'])
        );

        return $result;
    }

    #[Override]
    public function normalize(object $object): array
    {
        return $this->serializer->normalize($object, context: ['groups' => 'meilisearch']);
    }
}
