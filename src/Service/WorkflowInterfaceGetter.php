<?php

namespace App\Service;

use App\Entity\Agreement;
use App\Entity\Authority;
use App\Entity\DocumentRule;
use App\Entity\ManagementRule;
use App\Entity\Profile;
use App\Entity\ServiceLevel;
use App\Entity\StateMachineEntityInterface;
use App\Entity\Vocabulary;
use Exception;
use Symfony\Component\Workflow\WorkflowInterface;

readonly class WorkflowInterfaceGetter
{
    public function __construct(
        private WorkflowInterface $agreementStateMachine,
        private WorkflowInterface $authorityStateMachine,
        private WorkflowInterface $documentruleStateMachine,
        private WorkflowInterface $managementruleStateMachine,
        private WorkflowInterface $profileStateMachine,
        private WorkflowInterface $servicelevelStateMachine,
        private WorkflowInterface $vocabularyStateMachine,
    ) {
    }

    public function getWorkflowInterfaceFromEntity(StateMachineEntityInterface $entity): WorkflowInterface
    {
        return match ($entity::class) {
            Agreement::class => $this->agreementStateMachine,
            Authority::class => $this->authorityStateMachine,
            DocumentRule::class => $this->documentruleStateMachine,
            ManagementRule::class => $this->managementruleStateMachine,
            Profile::class => $this->profileStateMachine,
            ServiceLevel::class => $this->servicelevelStateMachine,
            Vocabulary::class => $this->vocabularyStateMachine,
            default => throw new Exception(
                sprintf('Workflow for %s not found', $entity::class)
            ),
        };
    }
}
