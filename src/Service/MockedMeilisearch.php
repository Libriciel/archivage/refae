<?php

namespace App\Service;

use App\Entity\MeilisearchIndexedInterface;
use Meilisearch\Client;
use Meilisearch\Endpoints\Indexes;
use Override;

readonly class MockedMeilisearch implements MeilisearchInterface
{
    public Client $client;

    public function __construct()
    {
        $this->client = new Client($_ENV['MEILI_HOST'], $_ENV['MEILI_MASTER_KEY']);
    }

    #[Override]
    public function getIndex(string $indexName): Indexes
    {
        return $this->client->index($indexName);
    }

    #[Override]
    public function classnameToIndexname(string $classname): string
    {
        return '';
    }

    #[Override]
    public function getIndexByObject(MeilisearchIndexedInterface $object): Indexes
    {
        $classname = $object::class;
        $indexName = $this->classnameToIndexname($classname);

        return $this->getIndex($indexName);
    }

    #[Override]
    public function add(MeilisearchIndexedInterface $object): void
    {
    }

    #[Override]
    public function update(MeilisearchIndexedInterface $object): void
    {
    }

    #[Override]
    public function delete(MeilisearchIndexedInterface $object): void
    {
    }

    #[Override]
    public function search(string $indexName, string $text, array $searchParams = []): array
    {
        return [];
    }

    #[Override]
    public function normalize(object $object): array
    {
        return [];
    }
}
