<?php

namespace App\Service;

use App\Entity\Notify;
use App\Session\CustomSessionHandler;
use DateTime;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

readonly class NotifyService
{
    public function __construct(
        private HubInterface $hub,
        private CustomSessionHandler $customSessionHandler,
        private RouterInterface $router,
    ) {
    }

    public function send(Notify $notify): void
    {
        $sessions = $this->customSessionHandler->findByPage(1000, 0);
        $sessionIds = [];
        $username = $notify->getUser()?->getUsername();
        $message = $notify->getText();
        $class = $notify->getCssClass();
        foreach ($sessions as $session) {
            $id = $session[$this->customSessionHandler->idCol];
            if (
                isset($session[$this->customSessionHandler->dataCol]['username'])
                && $session[$this->customSessionHandler->dataCol]['username'] === $username
            ) {
                $sessionIds[$id] = $id;
            }
        }
        if (empty($sessionIds)) {
            return;
        }
        $topics = array_map(
            fn($id) => $this->router->generate(
                'public_notify_user',
                ['id' => $id],
                referenceType: UrlGeneratorInterface::ABSOLUTE_URL
            ),
            $sessionIds
        );
        $update = new Update(
            $topics,
            json_encode([
                'id' => $notify->getId(),
                'text' => $message,
                'created' => (new DateTime())->format(DATE_RFC3339),
                'css_class' => $class,
            ]),
            true,
        );
        $this->hub->publish($update);
    }
}
