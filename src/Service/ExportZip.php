<?php

namespace App\Service;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\QueryBuilder;
use Exception;
use ReflectionException;
use ZipArchive;

class ExportZip
{
    /**
     * @param string|null   $entityClassname null = auto (query)
     * @param callable|null $callback        fn($entity, $zip, $normalized)
     * @return string                        zip binary string
     * @throws ReflectionException|Exception
     */
    public function fromQuery(
        QueryBuilder $query,
        string $entityClassname = null,
        callable $callback = null,
    ): string {
        if (empty($entityClassname)) {
            $entityClassname = $query->getDQLPart('from')[0];
        }
        $tmpFile = tmpfile();
        $archiveName = stream_get_meta_data($tmpFile)['uri'];

        $zip = new ZipArchive();
        if (!$zip->open($archiveName, ZipArchive::OVERWRITE)) {
            throw new Exception('Unable to open tmpfile for zip creation');
        }

        $json = $this->getJsonData($entityClassname, $query, $callback, $zip);

        $filename = ImportZip::exportDataFilename($entityClassname);
        $zip->addFromString(
            $filename . '.json',
            json_encode($json, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT)
        );

        if (!$zip->close()) {
            throw new Exception("Erreur lors de la fermeture du fichier zip");
        }
        $content = file_get_contents($archiveName);
        fclose($tmpFile);

        return $content;
    }

    public function getJsonData(
        string $entityClassname,
        QueryBuilder $query,
        ?callable $callback = null,
        ?ZipArchive $zip = null
    ): array {
        $extractor = new AttributeExtractor($entityClassname);
        $properties = $extractor->getPropertiesHaving(ORM\Column::class);
        $json = [];
        $offset = 0;

        do {
            $newQuery = (clone $query)->setMaxResults(65535)->setFirstResult($offset);
            $results = $newQuery->getQuery()->getResult();
            if (!count($results)) {
                break;
            }
            foreach ($results as $entity) {
                $offset++;
                $normalized = $extractor->normalizeEntityByProperties($properties, $entity);
                if ($callback) {
                    $normalized = $callback($entity, $zip, $normalized) ?: $normalized;
                }
                if ($normalized) {
                    $json[] = $normalized;
                }
            }
        } while ($offset % 65535 === 0);

        return $json;
    }
}
