<?php

namespace App\Service;

use ApiPlatform\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Doctrine\Orm\Paginator as ApiPlatformPaginator;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGenerator;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface;
use ApiPlatform\Serializer\SerializerContextBuilderInterface;
use App\Entity\User;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator as DoctrinePaginator;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\SerializerInterface;

readonly class ApiPaginator
{
    public function __construct(
        private TokenStorageInterface $tokenStorage,
        private RequestStack $requestStack,
        private SerializerInterface $serializer,
        private SerializerContextBuilderInterface $serializerContextBuilder,
        private ResourceMetadataCollectionFactoryInterface $resourceMetadataFactory,
        private iterable $extensions,
        private int $maxPerPage = 10,
    ) {
    }

    public function getUser(): User
    {
        $user = $this->tokenStorage->getToken()->getUser();
        if (!$user instanceof User || !$user->getActiveTenant()) {
            throw new UnauthorizedHttpException('');
        }
        return $user;
    }

    public function checkPermissions()
    {
        $request = $this->requestStack->getCurrentRequest();
        $tenant_url = $request->attributes->get('tenant_url');
        $user = $this->getUser();
        if ($user->getActiveTenant()->getBaseurl() !== $tenant_url) {
            throw new AccessDeniedException('Access Denied.');
        }
    }

    private function transformIdUrl(string $responseString, string $tenant_url): string
    {
        return preg_replace_callback(
            '#"(\\\?/api\\\?/)([0-9a-f-]+)(\\\?/)([^\\\/]+)(\\\?/)([0-9a-f-]+)"#',
            function ($m) use ($tenant_url) {
                if ($m[2] === $m[6] && $tenant_url !== $m[2]) {
                    return '"' . $m[1] . $tenant_url . $m[3] . $m[4] . $m[5] . $m[6] . '"';
                }
                return $m[0];
            },
            $responseString
        );
    }

    private function getResponseString($data, string $entityClass): string
    {
        $request = $this->requestStack->getCurrentRequest();
        $tenant_url = $request->attributes->get('tenant_url');
        $request->attributes->add([
            '_api_resource_class' => $entityClass,
            '_api_operation_name' => $request->attributes->get('_route'),
        ]);
        $context = $this->serializerContextBuilder->createFromRequest($request, true);
        $context['group'] = 'api';
        $context['uri_variables'] = $request->attributes->get('_route_params');
        $responseString = $this->serializer->serialize($data, 'jsonld', $context);

        return $this->transformIdUrl($responseString, $tenant_url);
    }

    public function getResponseStringFromEntity($entity, string $entityClass)
    {
        return $this->getResponseString($entity, $entityClass);
    }

    public function getResponseStringFromQuery(Query $query, string $entityClass)
    {
        $request = $this->requestStack->getCurrentRequest();
        $page = $request->get('page') ?: 1;
        $maxPerPage = $request->query->get('itemsPerPage') ?: $this->maxPerPage;

        $query
            ->setFirstResult($maxPerPage * ($page - 1))
            ->setMaxResults($maxPerPage)
        ;

        $doctrinePaginator = new DoctrinePaginator($query);
        $paginator = new ApiPlatformPaginator($doctrinePaginator);

        return $this->getResponseString($paginator, $entityClass);
    }

    public function applyFiltersAndPaginate(QueryBuilder $queryBuilder, string $entityClassname)
    {
        $request = $this->requestStack->getCurrentRequest();
        $this->checkPermissions();
        $resourceMetadataCollection = $this->resourceMetadataFactory->create($entityClassname);
        $resourceMetadata = $resourceMetadataCollection->getOperation()->getFilters();

        $operation = new GetCollection();
        $operation = $operation->withFilters($resourceMetadata);

        $queryNameGenerator = new QueryNameGenerator();
        foreach ($this->extensions as $extension) {
            if ($extension instanceof QueryCollectionExtensionInterface) {
                $extension->applyToCollection(
                    $queryBuilder,
                    $queryNameGenerator,
                    $entityClassname,
                    $operation,
                    context: ['filters' => $request->query->all()]
                );
            }
        }

        return $this->getResponseStringFromQuery($queryBuilder->getQuery(), $entityClassname);
    }
}
