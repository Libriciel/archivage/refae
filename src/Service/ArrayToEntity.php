<?php

namespace App\Service;

use DateTime;
use DateTimeInterface;
use Exception;
use Symfony\Component\String\UnicodeString;
use Doctrine\ORM\Mapping as ORM;

class ArrayToEntity
{
    public static function arrayToEntity(array $data, string $className): object
    {
        if (!class_exists($className)) {
            throw new Exception(sprintf('Unknow class "%s" for arrayToEntity()', $className));
        }
        $object = new $className();

        $extractor = new AttributeExtractor($className);
        $attributes = $extractor->getPropertiesHaving(ORM\Column::class);
        foreach ($data as $key => $value) {
            if (!isset($attributes[$key])) {
                continue;
            }

            $camel = ucfirst((new UnicodeString($key))->camel());
            $setter = 'set' . $camel;
            if (method_exists($object, $setter)) {
                if ($attributes[$key]->getType()?->getName() === DateTimeInterface::class) {
                    $value = new DateTime($value);
                } // TODO: autre cas relous en + des dates ?
                $object->$setter($value);
            }
            // TODO: prévoir un mode avec levée d'erreur si la méthode n'existe pas ?
        }

        return $object;
    }
}
