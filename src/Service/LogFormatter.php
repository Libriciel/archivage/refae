<?php

namespace App\Service;

use Monolog\Formatter\FormatterInterface;
use Monolog\Formatter\LineFormatter;
use Monolog\LogRecord;

class LogFormatter implements FormatterInterface
{
    private LineFormatter $formatter;

    public function __construct()
    {
        $this->formatter = new LineFormatter(
            null,
            null,
            true,
            true
        );
    }

    public function format(LogRecord $record)
    {
        return $this->formatter->format($record);
    }

    public function formatBatch(array $records)
    {
        return $this->formatter->formatBatch($records);
    }
}
