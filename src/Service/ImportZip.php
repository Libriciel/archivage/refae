<?php

namespace App\Service;

use App\Service\Manager\ImportExportManagerInterface;
use DateTime;
use Exception;
use FilesystemIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\String\Inflector\EnglishInflector;
use Symfony\Component\String\UnicodeString;
use ZipArchive;

class ImportZip
{
    public array $errors = [];
    public array $data = [];
    public array $previewData = [];
    public array $files = [];
    public array $labels = [];
    public int $dataCount = 0;
    public int $fileCount = 0;
    public int $failedCount = 0;
    public int $importableCount = 0;
    public bool $hasFatalError = false;
    public ?DateTime $exportDate = null;
    public ?ImportExportManagerInterface $manager = null;

    public function __construct(
        private readonly SessionFiles $sessionFiles,
    ) {
    }

    public function extract(string $session_tmpfile_uuid, ImportExportManagerInterface $importExportManager)
    {
        $zipFile = $this->sessionFiles->getUri($session_tmpfile_uuid);
        $tmpDir = sys_get_temp_dir() . '/' . $session_tmpfile_uuid;

        $this->errors = [];
        $this->dataCount = 0;
        $this->failedCount = 0;
        $this->files = [];
        try {
            $this->unzip($zipFile, $tmpDir, $importExportManager);
        } catch (Exception $e) {
            $this->hasFatalError = true;
            $this->errors['fatal'] = $e->getMessage();
        }
        $this->importableCount = $this->hasFatalError ? 0 : $this->dataCount - $this->failedCount;
        if ($this->importableCount === 0 && empty($this->errors['fatal'])) {
            $this->errors['fatal'] = "Aucun import n'est possible";
            $this->hasFatalError = true;
        }
        $this->previewData = array_slice($this->data, 0, 10);
    }

    /**
     * @param string|object $entity
     * @return string snakecase_pluralized_entity_name.json
     * @throws Exception
     */
    public static function exportDataFilename($entity): string
    {
        if (is_string($entity)) {
            $classname = $entity;
        } elseif (is_object($entity)) {
            $classname = $entity::class;
        } else {
            throw new Exception("Missing valid entity name");
        }
        $classname = substr($classname, strrpos($classname, '\\') + 1);
        $inflector = new EnglishInflector();
        return $inflector->pluralize((new UnicodeString($classname))->snake())[0];
    }

    public function unzip(string $zipFile, string $tmpDir, ImportExportManagerInterface $importExportManager)
    {
        $zip = new ZipArchive();
        if (!$zip->open($zipFile)) {
            throw new Exception("Echec lors de l'ouverture du fichier");
        }
        $this->fileCount = $zip->count() - 1;

        if (is_dir($tmpDir)) {
            $filesystem = new Filesystem();
            $filesystem->remove($tmpDir);
        }
        if (!$zip->extractTo($tmpDir)) {
            throw new Exception("Echec lors de l'écriture des fichiers temporaire");
        }
        $strlen = strlen($tmpDir) + 1;
        $iterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($tmpDir, FilesystemIterator::SKIP_DOTS),
            RecursiveIteratorIterator::SELF_FIRST
        );
        foreach ($iterator as $path => $info) {
            if ($info->isFile()) {
                $this->files[substr((string) $path, $strlen)] = $path;
            }
        }
        $jsonName = sprintf('%s.json', ImportZip::exportDataFilename($importExportManager->getClassName()));
        if (!isset($this->files[$jsonName])) {
            throw new Exception("Format du fichier incorrect (json name) " . $jsonName);
        }
        $json = json_decode(file_get_contents($this->files[$jsonName]), true);
        if ($json === false) {
            throw new Exception("Format du fichier incorrect (json formating)");
        }
        $this->data = $json;
        $this->dataCount = count($json);
        $this->exportDate = (new DateTime())->setTimestamp(filemtime($this->files[$jsonName]));

        if (!$importExportManager->checkForImport($this)) {
            $this->hasFatalError = true;
        }
        $this->failedCount = count($this->errors['failed'] ?? []);
    }
}
