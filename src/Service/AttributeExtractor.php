<?php

/** @noinspection PhpInternalEntityUsedInspection */

namespace App\Service;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Proxy\InternalProxy;
use ReflectionAttribute;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use ReflectionProperty;
use Symfony\Component\String\UnicodeString;

class AttributeExtractor
{
    private ReflectionClass $reflectionClass;

    /**
     * @param object|string $objectOrClass objet ou classname
     * @throws ReflectionException
     */
    public function __construct($objectOrClass)
    {
        $this->reflectionClass = new ReflectionClass($objectOrClass);

        if ($this->reflectionClass->implementsInterface(InternalProxy::class)) {
            $this->reflectionClass = $this->reflectionClass->getParentClass();
        }
    }

    public function getClassAttributes(string $attributeClassname, callable $map = null): array
    {
        $result = [];
        foreach ($this->reflectionClass->getAttributes() as $reflectionAttribute) {
            if ($reflectionAttribute->getName() !== $attributeClassname) {
                continue;
            }
            $result[] = $reflectionAttribute;
            if ($map) {
                $map($reflectionAttribute);
            }
        }
        return $result;
    }

    public function getAnyHaving(string $attributeClassname, callable $map = null): array
    {
        return array_merge(
            $this->getPropertiesHaving($attributeClassname, $map),
            $this->getMethodsHaving($attributeClassname, $map),
        );
    }

    /**
     * @param callable|null $map
     * @return ReflectionProperty[]
     */
    public function getPropertiesHaving(string $attributeClassname, callable $map = null): array
    {
        $result = [];
        foreach ($this->reflectionClass->getProperties() as $reflectionProperty) {
            $reflectionAttribute = static::getAttribute($reflectionProperty, $attributeClassname);
            if (!$reflectionAttribute) {
                continue;
            }
            $result[$reflectionProperty->getName()] = $reflectionProperty;
            if ($map) {
                $map($reflectionProperty, $reflectionAttribute);
            }
        }
        return $result;
    }

    /**
     * @param callable|null $map
     * @return ReflectionMethod[]
     */
    public function getMethodsHaving(string $attributeClassname, callable $map = null): array
    {
        $result = [];
        foreach ($this->reflectionClass->getMethods() as $reflectionMethod) {
            $reflectionAttribute = static::getAttribute($reflectionMethod, $attributeClassname);
            if (!$reflectionAttribute) {
                continue;
            }
            $result[$reflectionMethod->getName()] = $reflectionMethod;
            if ($map) {
                $map($reflectionMethod, $reflectionAttribute);
            }
        }
        return $result;
    }

    public static function getAttribute(
        ReflectionProperty|ReflectionMethod $reflection,
        string $attributeClassname
    ): ?ReflectionAttribute {
        foreach ($reflection->getAttributes() as $reflectionAttribute) {
            if ($reflectionAttribute->getName() === $attributeClassname) {
                return $reflectionAttribute;
            }
        }
        return null;
    }

    public function extractNormalisedAttributeArgs(
        ReflectionAttribute $reflectionAttribute
    ): array {
        $initialArgs = $reflectionAttribute->getArguments();
        $normalizedArgs = [];
        $refl = new ReflectionClass($reflectionAttribute->getName());
        $constructor = $refl->getMethod('__construct');
        foreach ($constructor->getParameters() as $parameter) {
            if (isset($initialArgs[$parameter->getName()])) {
                $normalizedArgs[$parameter->getName()] = $initialArgs[$parameter->getName()];
            } elseif (isset($initialArgs[$parameter->getPosition()])) {
                $normalizedArgs[$parameter->getName()] = $initialArgs[$parameter->getPosition()];
            } else {
                $normalizedArgs[$parameter->getName()] = $parameter->isDefaultValueAvailable()
                    ? $parameter->getDefaultValue()
                    : null;
            }
        }
        return $normalizedArgs;
    }

    /**
     * Récupère toutes les propriétés d'un objet qui implémentes un attribut donné
     * en donnant un résultat sous la forme : [
     *     'prop1' => [..arguments de l'attribut],
     * ]
     */
    public static function getPropertiesForAttribute(object $object, string $attributeClassName): array
    {
        $reflection = new ReflectionClass($object);
        $results = [];
        foreach ($reflection->getProperties() as $property) {
            if ($attr = static::getAttribute($property, $attributeClassName)) {
                $results[$property->getName()] = $attr->getArguments();
            }
        }
        foreach ($reflection->getMethods() as $method) {
            if ($attr = static::getAttribute($method, $attributeClassName)) {
                $results[$method->getName()] = $attr->getArguments();
            }
        }
        return $results;
    }

    public function normalizeEntityColumns(array $results): array
    {
        $json = [];
        $properties = $this->getPropertiesHaving(ORM\Column::class);
        foreach ($results as $entity) {
            $normalized = $this->normalizeEntityByProperties($properties, $entity);
            if ($normalized) {
                $json[] = $normalized;
            }
        }
        return $json;
    }

    public function normalizeEntityByProperties(array $properties, object $entity): array
    {
        $normalized = [];
        foreach ($properties as $property) {
            $camel = ucfirst((new UnicodeString($property->getName()))->camel());
            if (method_exists($entity, 'get' . $camel)) {
                $getter = 'get' . $camel;
            } elseif (method_exists($entity, 'is' . $camel)) {
                $getter = 'is' . $camel;
            } else {
                continue;
            }
            $value = $entity->$getter();
            if ($value instanceof DateTimeInterface) {
                $value = $value->format(DATE_RFC3339);
            }
            $normalized[$property->getName()] = $value;
        }
        return $normalized;
    }
}
