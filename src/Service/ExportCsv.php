<?php

namespace App\Service;

use App\ResultSet\CsvExportEntityResultSetInterface;
use DateTime;
use Symfony\Component\HttpFoundation\Response;

class ExportCsv
{
    public static function fromResultSet(
        CsvExportEntityResultSetInterface $resultSet,
        array $groups = ['csv']
    ): Response {
        $resultSet->memory()->max_per_page = 65535;
        $fields = $resultSet->tableFieldsFromEntity($groups, false);

        $columns = array_keys($fields);
        $columnTranslations = [];
        foreach ($columns as $fieldName) {
            if ($fieldName === 'id') {
                continue;
            }
            $columnTranslations[$fieldName] = $fields[$fieldName]['label'] ?? $fieldName;
        }
        $memoryStream = fopen('php://memory', 'w');
        fputcsv($memoryStream, array_values($columnTranslations));

        $pageCount = $resultSet->pageCount();
        for ($page = 1; $page <= $pageCount; $page++) {
            $resultSet->setPage($page);
            $resultSet->setData(null); // delete cache
            $data = $resultSet->data($groups);
            foreach ($data as $value) {
                $row = [];
                foreach (array_keys($columnTranslations) as $columnName) {
                    $val = $value[$columnName] ?? '';
                    $row[] = is_array($val)
                        ? implode("\n", $val)
                        : (string)$val;
                }
                fputcsv($memoryStream, $row);
            }
        }

        rewind($memoryStream);
        $csv = stream_get_contents($memoryStream);
        fclose($memoryStream);
        $filename = sprintf('%s_%s_export.csv', (new DateTime())->format('Y-m-d_His'), $resultSet->getCsvFileName());
        $headers = [
            'Content-Description' => 'File Transfer',
            'Content-Type' => 'text/csv',
            'Content-Length' => strlen($csv),
            'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
            'Expires' => '0',
            'Cache-Control' => 'must-revalidate',
        ];
        return new Response($csv, Response::HTTP_OK, $headers);
    }
}
