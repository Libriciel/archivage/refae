<?php

namespace App\Service;

use App\Entity\Tenant;

readonly class FilesizeLimiter
{
    public function __construct(
        private LoggedUser $loggedUser,
        private int $defaultMaxFilesize,
        private int $defaultFileRetentionCount,
    ) {
    }

    public function maxFilesize(Tenant $tenant = null): int
    {
        $tenant = $tenant ?: $this->loggedUser->tenant();
        if ($tenant) {
            $configuration = $tenant->getConfiguration();
            if ($configuration && $configuration->getMaxFilesize()) {
                return $configuration->getMaxFilesize();
            }
        }
        return $this->defaultMaxFilesize;
    }

    public function fileRetentionCount(Tenant $tenant = null): int
    {
        $tenant = $tenant ?: $this->loggedUser->tenant();
        if ($tenant) {
            $configuration = $tenant->getConfiguration();
            if ($configuration && $configuration->getFileRetentionCount()) {
                return $configuration->getFileRetentionCount();
            }
        }

        return $this->defaultFileRetentionCount;
    }
}
