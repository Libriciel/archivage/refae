<?php

namespace App\Service;

use App\Entity\PermissionPrivilege;
use App\Entity\Privilege;
use App\Entity\PrivilegeUser;
use App\Entity\Role;
use App\Entity\Tenant;
use App\Entity\User;
use App\Repository\AccessUserRepository;
use App\Repository\PermissionPrivilegeRepository;
use App\Repository\PrivilegeRepository;
use App\Repository\PrivilegeUserRepository;
use App\Repository\RoleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use ReflectionAttribute;
use ReflectionException;
use ReflectionMethod;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Attribute\Route as AnnotationRoute;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\SerializerInterface;

class Permission
{
    public const string ROLES = 'roles';
    public const string PRIVILEGES = 'privileges';
    public const string PERMISSIONS = 'permissions';

    public const string ROLE_FUNCTIONAL_ADMIN = "FunctionalAdmin";
    public const string ROLE_READER = "Reader";
    public const string ROLE_TECHNICAL_ADMIN = "TechnicalAdmin";
    public const string ROLE_WEBSERVICE_REQUESTER = "WebserviceRequester";

    public static $commonNames = [];

    private ?Role $role = null;
    private ?Role $roleAdmin = null;
    private ?Tenant $tenant = null;

    /**
     * @var PermissionPrivilege[]|null
     */
    private ?array $cachedPermissionPrivilege = null;
    /**
     * @var PrivilegeUser[]|null
     */
    private ?array $cachedPrivilegeUser = null;

    /**
     * @var Privilege[]
     */
    private array $privilegeEntities = [];

    public function __construct(
        private readonly RouterInterface $router,
        private readonly RoleRepository $roleRepository,
        private readonly PrivilegeRepository $privilegesRepository,
        private readonly PermissionPrivilegeRepository $permissionPrivilegeRepository,
        private readonly PrivilegeUserRepository $privilegeUserRepository,
        private readonly AccessUserRepository $accessUserRepository,
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security,
        private readonly RequestStack $requestStack,
        private readonly SerializerInterface $serializer,
        private readonly array $roles,
        private readonly array $privileges,
    ) {
        foreach ($roles as $roleName => $params) {
            static::$commonNames['roles'][$roleName] = $params['common_name'];
        }
        foreach ($privileges as $privilegeName => $params) {
            static::$commonNames['privileges'][$privilegeName] = $params['common_name'];
        }
    }

    public function getPrivilegeCommonName(string $privilegeName): string
    {
        if (!isset($this->privileges[$privilegeName])) {
            return $privilegeName;
        }
        return $this->privileges[$privilegeName]['common_name'];
    }

    public function getRoleCommonName(string $roleName): string
    {
        if (!isset($this->roles[$roleName])) {
            return $roleName;
        }
        return $this->roles[$roleName]['common_name'];
    }

    public function userCanAccessRoute(string $route): bool
    {
        if (str_starts_with($route, 'public_')) {
            return true;
        }
        $user = $this->security->getUser();
        $request = $this->requestStack->getCurrentRequest();
        if (!$user instanceof User) {
            return false;
        }
        $this->tenant = $this->tenant ?: $user->getTenant($request);
        $aclSessionId = $this->getAclSessionId($this->tenant);
        $session = $request->getSession();
        $aclData = $session->get($aclSessionId);
        if (!$aclData) {
            $aclData = $this->setSessionData($user, $this->tenant);
        }

        return $aclData[$route] ?? false;
    }

    public function getAclSessionId(?Tenant $tenant): string
    {
        if (!$tenant) {
            $aclSessionId = 'acl.admin';
        } else {
            $aclSessionId = sprintf('acl.%s', $tenant->getBaseurl());
        }
        return $aclSessionId;
    }

    public function setSessionData(User $user, ?Tenant $tenant): array
    {
        $request = $this->requestStack->getCurrentRequest();
        $session = $request->getSession();
        if ($tenant) {
            $aclData = $this->getAclData($user, $tenant);
            $session->set($this->getAclSessionId($tenant), $aclData);
        }
        $aclDataAdmin = $this->getAclData($user, null);
        $session->set($this->getAclSessionId(null), $aclDataAdmin);

        if (!empty($this->role) && $tenant) {
            $tenantRoleId = sprintf('role.%s', $tenant->getBaseurl());
            $session->set(
                $tenantRoleId,
                $this->serializer->normalize(
                    $this->role,
                    context: ['groups' => ['session']]
                )
            );
        }
        if (!empty($this->roleAdmin)) {
            $session->set(
                'role.admin',
                $this->serializer->normalize(
                    $this->roleAdmin,
                    context: ['groups' => ['session']]
                )
            );
        }
        $session->save();
        return $aclData ?? $aclDataAdmin;
    }

    /**
     * @return PermissionPrivilege[]
     */
    private function getPermissionPrivilege(
        string $permissionClassname,
        string $action,
    ): array {
        if (!$this->cachedPermissionPrivilege) {
            $this->cachedPermissionPrivilege = $this->permissionPrivilegeRepository->findForCache();
        }
        $permissionPrivileges = [];
        foreach ($this->cachedPermissionPrivilege as $permissionPrivilege) {
            if (
                $permissionPrivilege->getPermissionClassname() === $permissionClassname
                && $permissionPrivilege->getAction() === $action
            ) {
                $permissionPrivileges[] = $permissionPrivilege;
            }
        }
        return $permissionPrivileges;
    }

    private function getPrivilegeUser(
        Privilege $privilege,
        ?Tenant $tenant,
        User $user,
    ): ?PrivilegeUser {
        if ($this->cachedPrivilegeUser === null) {
            $this->cachedPrivilegeUser = $this->privilegeUserRepository->findForCache($tenant, $user);
        }
        foreach ($this->cachedPrivilegeUser as $privilegeUser) {
            if ($privilegeUser->getPrivilege() === $privilege) {
                return $privilegeUser;
            }
        }
        return null;
    }

    public function userCanAccess(
        User $user,
        ?Tenant $tenant,
        string $permissionClassname,
        string $action
    ): bool {
        $permissions = $this->getPermissionPrivilege($permissionClassname, $action);
        // si la permission n'existe pas, on force un update permission
        if (!$permissions) {
            $this->privilege();
            $this->permissionRole();
            $permission = $this->permissionPrivilegeRepository->findOneBy(
                [
                    'permissionClassname' => $permissionClassname,
                    'action' => $action,
                ]
            );
            if (!$permission) {
                throw new BadRequestHttpException(
                    sprintf(
                        "permission %s:%s does not exists",
                        $permissionClassname,
                        $action
                    )
                );
            }
            $permissions = [$permission];
        }

        foreach ($permissions as $permission) {
            $privilege = $permission->getPrivilege();

            // Règles custom sur utilisateur
            $privilegeUser = $this->getPrivilegeUser($privilege, $tenant, $user);
            if ($privilegeUser) {
                return $privilegeUser->isIsGranted();
            }
        }

        // si l'utilisateur n'a pas accès à ce tenant, renvoi faux
        if ($this->role && $tenant) {
            $role = $this->role;
        } elseif ($this->roleAdmin && !$tenant) {
            $role = $this->roleAdmin;
        }
        if (empty($role)) {
            $accessUser = $this->accessUserRepository->findOneBy(
                [
                    'tenant' => $tenant,
                    'user' => $user,
                ]
            );
            if (!$accessUser) {
                return false;
            }
            $role = $accessUser->getRole();
            if ($tenant) {
                $this->role = $role;
            } else {
                $this->roleAdmin = $role;
            }
        }

        foreach ($permissions as $permission) {
            $privilege = $permission->getPrivilege();
            foreach ($privilege->getRole() as $pRole) {
                if ($pRole === $role) {
                    return true;
                }
            }
        }
        return false;
    }

    public function getAclData(User $user, ?Tenant $tenant): array
    {
        $collection = $this->router->getRouteCollection();
        $allRoutes = $collection->all();
        $controllers = []; // cache
        $permissions = [];
        $routes = [];

        /** @var Route $route */
        foreach ($allRoutes as $route) {
            $this->extractPermissionsFromRoute($route, $controllers, $permissions, $routes);
        }

        $acls = [];
        foreach ($routes as $routeName => $permission) {
            if (
                // filtre les routes non pertinentes (seront toujours false)
                (str_starts_with((string) $routeName, 'app_') && !$tenant)
                || (str_starts_with((string) $routeName, 'admin_') && $tenant)
            ) {
                continue;
            }
            [$permissionClass, $action] = explode('::', (string) $permission);
            $acls[$routeName] = $this->userCanAccess($user, $tenant, $permissionClass, $action);
        }
        return $acls;
    }

    /**
     * Créer les privileges de permissions manquants ainsi que leurs liens aux permissions
     * @return array
     */
    public function privilege(): array
    {
        $this->privilegeEntities = [];
        $this->privilegesRepository->removeNonExistentPrivileges(array_keys($this->privileges));
        $allClasses = [];
        foreach ($this->privileges as $privilegeName => $params) {
            $actions = $params['actions'];
            $this->privilegeEntities[$privilegeName] = $this->privilegesRepository
                ->findOrCreateIfNotExistsDefault($privilegeName);
            $classes = [];
            foreach ($actions as $action) {
                [$permissionClassname, $permissionAction] = explode('/', (string) $action);
                $classes[$permissionClassname][] = $permissionAction;
            }
            foreach ($classes as $permissionClassname => $permissionActions) {
                $allClasses[$permissionClassname] = $permissionClassname;
                $this->permissionPrivilegeRepository->findOrCreateIfNotExistsDefault(
                    $this->privilegeEntities[$privilegeName],
                    $permissionClassname,
                    $permissionActions
                );
            }
        }
        $this->permissionPrivilegeRepository
            ->removeNonExistentPermissionPrivileges(array_values($allClasses));
        return $this->privilegeEntities;
    }

    /**
     * Créer les rôles manquants ainsi que leurs liens aux privileges
     * @return array
     * @throws Exception
     */
    public function permissionRole(): array
    {
        $this->roleRepository->removeNonExistentRoles(array_keys($this->roles));
        foreach ($this->roles as $roleName => $params) {
            $privileges = $params['privileges'];
            $roleEntity = $this->roleRepository->findOrCreateIfNotExistsDefault($roleName);
            foreach ($privileges as $privilegePermissionClass) {
                if (!isset($this->privilegeEntities[$privilegePermissionClass])) {
                    throw new Exception("$privilegePermissionClass does not exists");
                }
                $roleEntity->addPrivilege($this->privilegeEntities[$privilegePermissionClass]);
                $this->entityManager->persist($roleEntity);
            }
        }
        $this->entityManager->flush();
        return $this->privilegeEntities;
    }

    /**
     * Donne la liste des permissions demandés par les controlleurs
     * @return array
     * @throws ReflectionException
     */
    public function routePermissions(): array
    {
        $collection = $this->router->getRouteCollection();
        $allRoutes = $collection->all();
        $controllers = []; // cache
        $permissions = [];

        // parcours les routes à la recherche des classes de permissions et des actions
        foreach ($allRoutes as $route) {
            $this->extractPermissionsFromRoute($route, $controllers, $permissions);
        }
        return $permissions;
    }

    /**
     * Alimente la variable $permissions en y ajoutant pour une route donnée :
     * $permissions[$permissionClass][$action] = $action
     * @return void
     * @throws ReflectionException
     */
    private function extractPermissionsFromRoute(
        Route $route,
        array &$controllers,
        array &$permissions,
        array &$routes = []
    ) {
        $controller = $route->getDefault('_controller');
        [$classname] = explode('::', (string) $controller);
        if (isset($controllers[$classname]) || !class_exists($classname)) {
            return;
        }
        $controllers[$classname] = true;
        $extractor = new AttributeExtractor($classname);
        $extractor->getMethodsHaving(
            IsGranted::class,
            function (
                ReflectionMethod $method,
                ReflectionAttribute $attribute
            ) use (
                &$permissions,
                &$routes
            ) {
                if (!preg_match('#^acl/(.*)/(.*)#', (string) $attribute->getArguments()[0], $m)) {
                    return;
                }
                [, $classname, $action] = $m;
                $permissions[$classname][$action] = $action;
                $methodRoute = null;
                foreach ($method->getAttributes() as $attribute) {
                    if ($attribute->getName() === AnnotationRoute::class) {
                        $methodRoute = $attribute->getArguments()['name'];
                    }
                }
                if ($methodRoute) {
                    $routes[$methodRoute] = "$classname::$action";
                }
            }
        );
    }
}
