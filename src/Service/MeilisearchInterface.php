<?php

namespace App\Service;

use App\Entity\MeilisearchIndexedInterface;
use Meilisearch\Endpoints\Indexes;

interface MeilisearchInterface
{
    public function getIndex(string $indexName): Indexes;

    public function classnameToIndexname(string $classname): string;

    public function getIndexByObject(MeilisearchIndexedInterface $object): Indexes;

    public function add(MeilisearchIndexedInterface $object): void;

    public function update(MeilisearchIndexedInterface $object): void;

    public function delete(MeilisearchIndexedInterface $object): void;

    public function search(string $indexName, string $text, array $searchParams = []): array;

    public function normalize(object $object): array;
}
