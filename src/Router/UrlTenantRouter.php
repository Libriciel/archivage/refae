<?php

namespace App\Router;

use App\Repository\TenantRepository;
use Override;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\CacheWarmer\WarmableInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RouterInterface;

readonly class UrlTenantRouter implements RouterInterface, WarmableInterface
{
    public function __construct(
        private RouterInterface $innerRouter,
        private RequestStack $requestStack,
        private TenantRepository $tenantRepository,
    ) {
    }

    #[Override]
    public function setContext(RequestContext $context): void
    {
        $this->innerRouter->setContext($context);
    }

    #[Override]
    public function getContext(): RequestContext
    {
        return $this->innerRouter->getContext();
    }

    #[Override]
    public function getRouteCollection(): RouteCollection
    {
        return $this->innerRouter->getRouteCollection();
    }

    /**
     * Ajout automatique du tenant_url dans les url commançant par app_
     */
    #[Override]
    public function generate(string $name, array $parameters = [], int $referenceType = self::ABSOLUTE_PATH): string
    {
        if (str_starts_with($name, 'app_')) {
            $request = $this->requestStack->getCurrentRequest();
            $parameters['tenant_url'] ??= $request?->get('tenant_url');
        }

        return $this->innerRouter->generate($name, $parameters, $referenceType);
    }

    public function tableUrl(string $name, array $parameters = [])
    {
        $url = $this->generate($name, $parameters);
        do {
            if (preg_match('/%7B([0-9]+)%7D/', $url, $m)) {
                /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
                $url = str_replace('%7B' . $m[1] . '%7D', "{{$m[1]}}", $url);
            }
        } while ($m);
        return $url;
    }

    /**
     * Lève une 404 si le tenant demandé n'existe pas
     */
    #[Override]
    public function match(string $pathinfo): array
    {
        $match = $this->innerRouter->match($pathinfo);
        $baseUrl = $match['tenant_url'] ?? null;
        if ($baseUrl) {
            $tenant = $this->tenantRepository->findOneByBaseurl($baseUrl);
            // rediriger au lieu de 404 ?
            if (!$tenant) {
                throw new NotFoundHttpException();
            }
        }

        return $match;
    }

    #[Override]
    public function warmUp(string $cacheDir, ?string $buildDir = null): array
    {
        if (method_exists($this->innerRouter, 'warmup')) {
            $this->innerRouter->warmUp($cacheDir);
        }
        return [];
    }
}
