<?php

namespace App\Repository;

use App\Entity\Table;
use App\ResultSet\Table\Memory as TableMemory;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @extends ServiceEntityRepository<Table>
 *
 * @method Table|null find($id, $lockMode = null, $lockVersion = null)
 * @method Table|null findOneBy(array $criteria, array $orderBy = null)
 * @method Table[]    findAll()
 * @method Table[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security,
        private readonly RequestStack $requestStack
    ) {
        parent::__construct($registry, Table::class);
    }

    /**
     * @return Table|null Returns an array of Table objects
     * @throws NonUniqueResultException
     */
    public function findByDomId(string $domId): ?Table
    {
        $query = $this->createQueryBuilder('t')
            ->andWhere('t.dom_id = :dom_id')
            ->setParameter('dom_id', $domId);

        $user = $this->security->getUser();
        if ($user) {
            $query->andWhere('t.user_id = :user_id')
                ->setParameter('user_id', $user->getUserIdentifier());
        } else {
            $request = $this->requestStack->getCurrentRequest();
            if ($request) {
                $cookieValue = $request->cookies->get('guest_uuid');
                $query->andWhere('t.guest_token = :guest_token')
                    ->setParameter('guest_token', $cookieValue);
            }
        }

        return $query
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @param string $domId
     * @return TableMemory
     * @throws NonUniqueResultException
     */
    public function getMemory(string $domId): TableMemory
    {
        $table = $this->findByDomId($domId);
        return $table ? $table->getMemory() : new TableMemory();
    }

    /**
     * @param array $data
     * @return void
     * @throws NonUniqueResultException
     */
    public function saveMemoryData(array $data): void
    {
        if (!isset($data['id'])) {
            throw new BadRequestException();
        }
        $table = $this->findByDomId($data['id']);
        if (!$table) {
            $table = new Table();
            $table->setDomId($data['id']);
        }

        $table->setActiveLine($data['activeLine'] ?? null);
        $table->setMaxPerPage($data['max_per_page'] ?? null);
        $table->setColumns(!empty($data['columns']) ? implode(',', $data['columns']) : null);
        $table->setFavorites(!empty($data['favorites']) ? implode(',', $data['favorites']) : null);

        $user = $this->security->getUser();
        if ($user) {
            $table->setUserId($user->getUserIdentifier());
        } else {
            $request = $this->requestStack->getCurrentRequest();
            $cookieValue = $request->cookies->get('guest_uuid');
            $table->setGuestToken($cookieValue);
            $table->setGuestLastAccess(new DateTime());
        }

        $this->entityManager->persist($table);
        $this->entityManager->flush();
    }
}
