<?php

namespace App\Repository;

use App\Entity\Tenant;
use App\Entity\User;
use App\Service\Permission;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Override;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @extends ServiceEntityRepository<User>
 *
 * @implements PasswordUpgraderInterface<User>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface, ResultSetRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(
        ManagerRegistry $registry,
    ) {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    #[Override]
    public function upgradePassword(
        PasswordAuthenticatedUserInterface $user,
        string $newHashedPassword
    ): void {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', $user::class)
            );
        }

        $user->setPassword($newHashedPassword);
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

    public function findOneByUsername(string $username): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.username = :val')
            ->setParameter('val', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneByUsernameTenant(
        string $username,
        string $tenantBaseurl
    ): ?User {
        return $this->createQueryBuilder('u')
            ->innerJoin('u.tenants', 't')
            ->andWhere('u.username = :val')
            ->setParameter('val', $username)
            ->andWhere('t.baseurl = :baseurl')
            ->setParameter('baseurl', $tenantBaseurl)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findTenantConnection(string $username, string $baseurl)
    {
        return $this->createQueryBuilder('u')
            ->select('u.id AS uid', 'lu.ldap_username AS username', 'l.id')
            ->innerJoin('u.tenants', 't')
            ->innerJoin('u.ldapUsers', 'lu')
            ->innerJoin('lu.Ldap', 'l')
            ->andWhere('u.username = :val')
            ->setParameter('val', $username)
            ->andWhere('t.baseurl = :baseurl')
            ->setParameter('baseurl', $baseurl)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function queryForTenantUrl(string $tenantUrl): QueryBuilder
    {
        return $this->createQueryBuilder('u')
            ->leftJoin('u.tenants', 'tenant')
            ->andWhere('tenant.baseurl = :url')
            ->setParameter('url', $tenantUrl)
            ->addSelect('tenant')
            ->orderBy('u.username', 'ASC');
    }

    public function findNotifyUserRegistration(?Tenant $tenant)
    {
        $queryBuilder = $this->createQueryBuilder('u')
            ->innerJoin('u.accessUsers', 'au')
            ->innerJoin('au.role', 'r')
            ->andWhere('u.email IS NOT NULL')
            ->andWhere('u.notifyUserRegistration = true')
            ->andWhere('r.name IN (:roles)')
            ->setParameter('roles', [
                Permission::ROLE_FUNCTIONAL_ADMIN,
                Permission::ROLE_TECHNICAL_ADMIN,
            ])
        ;
        if ($tenant) {
            $queryBuilder
                ->andWhere('au.tenant = :tenant')
                ->setParameter('tenant', $tenant)
            ;
        } else {
            $queryBuilder
                ->andWhere('au.tenant IS NULL')
            ;
        }
        return $queryBuilder
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByUsernameEmail(string $username, string $email): ?User
    {
        return $this->createQueryBuilder('u')
            ->innerJoin('u.accessUsers', 'au')
            ->andWhere('au.ldap IS NULL')
            ->andWhere('au.openid IS NULL') // possède une connexion refae
            ->setParameter('username', $username)
            ->andWhere('u.username = :username')
            ->setParameter('username', $username)
            ->andWhere('u.email = :email')
            ->setParameter('email', $email)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function getTechnicalAdminsQuery()
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->addSelect('tenant')
            ->leftJoin($alias . '.tenants', 'tenant')
            ->innerJoin($alias . '.accessUsers', 'accesses')
            ->innerJoin('accesses.role', 'r')
            ->andWhere('r.name = :role')
            ->setParameter('role', Permission::ROLE_TECHNICAL_ADMIN)
            ->orderBy('u.username', 'ASC')
        ;
    }

    public function findWithAdminTechContext(string $username): ?User
    {
        $user = $this->findOneByUsername($username);
        $user?->setContext('admin_tech');
        return $user;
    }

    public function countAll(): int
    {
        return $this->createQueryBuilder('u')
            ->select('COUNT(u.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countByTenant(?Tenant $tenant): int
    {
        $queryBuilder = $this->createQueryBuilder('u')
            ->select('COUNT(u.id)')
            ->leftJoin('u.tenants', 't');

        if ($tenant) {
            $queryBuilder
                ->andWhere('t.id = :tenant')
                ->setParameter('tenant', $tenant->getId());
        } else {
            $queryBuilder
                ->andWhere('t.id IS NULL');
        }

        return $queryBuilder
            ->getQuery()
            ->getSingleScalarResult();
    }
}
