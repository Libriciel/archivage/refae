<?php

namespace App\Repository;

use App\Entity\Privilege;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Privilege>
 *
 * @method Privilege|null find($id, $lockMode = null, $lockVersion = null)
 * @method Privilege|null findOneBy(array $criteria, array $orderBy = null)
 * @method Privilege[]    findAll()
 * @method Privilege[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PrivilegeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Privilege::class);
    }

    public function findOrCreateIfNotExistsDefault(string $privilegeName): Privilege
    {
        $exists = $this->createQueryBuilder('p')
            ->select('p.id')
            ->andWhere('p.name = :val')
            ->andWhere('p.tenant IS NULL')
            ->setParameter('val', $privilegeName)
            ->getQuery()
            ->getOneOrNullResult()
        ;
        if (!$exists) {
            $privilege = new Privilege();
            $privilege->setName($privilegeName);
            $this->_em->persist($privilege);
            $this->_em->flush();
            return $privilege;
        }
        return $this->find($exists);
    }

    public function removeNonExistentPrivileges(array $privileges)
    {
        $privilegeEntities = $this->createQueryBuilder('p')
            ->andWhere('p.tenant IS NULL')
            ->andWhere('p.name NOT IN (:privileges)')
            ->setParameter('privileges', $privileges)
            ->getQuery()
            ->getResult();
        foreach ($privilegeEntities as $privilegeEntity) {
            $this->_em->remove($this->find($privilegeEntity->getId()));
        }
        $this->_em->flush();
    }

    public function findDefaultByName(string $name)
    {
        $id = $this->createQueryBuilder('p')
            ->select('p.id')
            ->andWhere('p.name = :val')
            ->andWhere('p.tenant IS NULL')
            ->setParameter('val', $name)
            ->getQuery()
            ->getSingleScalarResult()
        ;
        return $this->find($id);
    }

    /**
     * @return Privilege[]
     */
    public function findRoleLess()
    {
        return $this->createQueryBuilder('p')
            ->leftJoin('p.roles', 'r')
            ->where('r.id IS NULL')
            ->getQuery()
            ->getResult()
        ;
    }
}
