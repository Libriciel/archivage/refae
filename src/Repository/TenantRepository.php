<?php

namespace App\Repository;

use App\Entity\AccessUser;
use App\Entity\Tenant;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Tenant>
 *
 * @method Tenant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tenant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tenant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TenantRepository extends ServiceEntityRepository implements ResultSetRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tenant::class);
    }

    public function findOneByBaseurl(string $value): ?Tenant
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->andWhere($alias . '.baseurl = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findOneActiveByBaseurl(string $value): ?Tenant
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->andWhere($alias . '.baseurl = :val')
            ->andWhere($alias . '.active = true')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function queryForAccessUserAdd(User $user): QueryBuilder
    {
        $accessTenants = array_map(
            fn(AccessUser $a): Tenant => $a->getTenant(),
            array_filter(
                $user->getAccessUsers()->toArray(),
                fn(AccessUser $a): bool => (bool)$a->getTenant()
            )
        );
        $alias = $this->getAlias();
        $query = $this->createQueryBuilder($alias)
            ->andWhere('t.active = true')
            ->andWhere('t IN (:tenants)')
            ->setParameter('tenants', $user->getTenants())
            ->orderBy('t.name');

        if ($accessTenants) {
            $query
                ->andWhere('t NOT IN (:accessTenants)')
                ->setParameter('accessTenants', $accessTenants);
        }
        return $query;
    }

    public function getActiveTenants(): array
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.active = true')
            ->orderBy('t.name')
            ->getQuery()->getResult();
    }

    public function countAll(): int
    {
        return $this->createQueryBuilder('e')
            ->select('COUNT(e.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }
}
