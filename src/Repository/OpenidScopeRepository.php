<?php

namespace App\Repository;

use App\Entity\OpenidScope;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OpenidScope>
 *
 * @method OpenidScope|null find($id, $lockMode = null, $lockVersion = null)
 * @method OpenidScope|null findOneBy(array $criteria, array $orderBy = null)
 * @method OpenidScope[]    findAll()
 * @method OpenidScope[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OpenidScopeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OpenidScope::class);
    }
}
