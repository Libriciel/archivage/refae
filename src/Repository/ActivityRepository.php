<?php

namespace App\Repository;

use App\Entity\Activity;
use App\Entity\Tenant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Activity>
 *
 * @method Activity|null find($id, $lockMode = null, $lockVersion = null)
 * @method Activity|null findOneBy(array $criteria, array $orderBy = null)
 * @method Activity[]    findAll()
 * @method Activity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActivityRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
    ) {
        parent::__construct($registry, Activity::class);
    }

    public function findForChart(string $formatedDate, Tenant $tenant)
    {
        return $this->createQueryBuilder('a')
            ->where('a.scope = :tenant')
            ->setParameter('tenant', $tenant)
            ->andWhere('a.created BETWEEN :from AND :to')
            ->setParameter('from', $formatedDate . ' 00:00:00')
            ->setParameter('to', $formatedDate . ' 23:59:59')
        ;
    }
}
