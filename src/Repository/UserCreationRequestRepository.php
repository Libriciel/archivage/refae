<?php

namespace App\Repository;

use App\Entity\UserCreationRequest;
use DateInterval;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Override;

/**
 * @extends ServiceEntityRepository<UserCreationRequest>
 *
 * @method UserCreationRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserCreationRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserCreationRequest[]    findAll()
 * @method UserCreationRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserCreationRequestRepository extends ServiceEntityRepository implements ResultSetRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public const string TOKEN_EXPIRE = 'PT1H';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserCreationRequest::class);
    }

    #[Override]
    public function getResultsetQuery(callable $appendFilters = null): QueryBuilder
    {
        $alias = $this->getAlias();
        $queryBuilder = $this->createQueryBuilder($alias)
            ->orderBy(sprintf('%s.id', $alias), 'ASC')
            ->andWhere("$alias.emailVerified = true")
        ;
        if ($appendFilters) {
            $appendFilters($queryBuilder);
        }

        return $queryBuilder;
    }

    public function deleteExpired(): void
    {
        $limitDate = (new DateTime())->sub(new DateInterval(self::TOKEN_EXPIRE));
        $this->createQueryBuilder('ucr')
            ->delete()
            ->where('ucr.created < :limit')
            ->andWhere('ucr.emailVerified = false')
            ->setParameter('limit', $limitDate)
            ->getQuery()
            ->execute()
        ;
    }

    public function deleteAccepted(?string $username)
    {
        $this->createQueryBuilder('ucr')
            ->delete()
            ->where('ucr.username = :username')
            ->setParameter('username', $username)
            ->getQuery()
            ->execute()
        ;
    }
}
