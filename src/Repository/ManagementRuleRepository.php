<?php

namespace App\Repository;

use App\Entity\ManagementRule;
use App\Entity\Tenant;
use App\Entity\VersionableEntityInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Override;

/**
 * @extends ServiceEntityRepository<ManagementRule>
 *
 * @method ManagementRule|null find($id, $lockMode = null, $lockVersion = null)
 * @method ManagementRule|null findOneBy(array $criteria, array $orderBy = null)
 * @method ManagementRule[]    findAll()
 * @method ManagementRule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ManagementRuleRepository extends ServiceEntityRepository implements
    ResultSetRepositoryInterface,
    VersionableEntityRepositoryInterface
{
    use ResultSetRepositoryTrait {
        queryPublic as queryPublicTrait;
    }

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ManagementRule::class);
    }

    public function queryStorageRuleOptions(string $tenantUrl): QueryBuilder
    {
        return $this->queryOptions($tenantUrl, 'StorageRule');
    }

    public function queryAppraisalOptions(string $tenantUrl): QueryBuilder
    {
        return $this->queryOptions($tenantUrl, 'AppraisalRule');
    }

    public function queryAccessOptions(string $tenantUrl): QueryBuilder
    {
        return $this->queryOptions($tenantUrl, 'AccessRule');
    }

    protected function queryOptions(string $tenantUrl, string $type): QueryBuilder
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->addSelect($alias, 'tenant', 'ruleType')
            ->innerJoin($alias . '.tenant', 'tenant')
            ->innerJoin($alias . '.ruleType', 'ruleType')
            ->where('tenant.baseurl = :baseurl')
            ->andWhere('ruleType.identifier = :type')
            ->andWhere($alias . '.state = :state')
            ->setParameter('baseurl', $tenantUrl)
            ->setParameter('type', $type)
            ->setParameter('state', VersionableEntityInterface::S_PUBLISHED)
            ->orderBy($alias . '.identifier')
        ;
    }

    #[Override]
    public function findPreviousVersion(VersionableEntityInterface $current): ?VersionableEntityInterface
    {
        if ($current->getVersion() === 1 || !$current instanceof ManagementRule) {
            return null;
        }
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->where($alias . '.identifier = :identifier')
            ->setParameter('identifier', $current->getIdentifier())
            ->andWhere($alias . '.version = :version')
            ->andWhere($alias . '.tenant = :tenant')
            ->setParameter('tenant', $current->getTenant())
            ->setParameter('version', $current->getVersion() - 1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findWithPreviousVersions(string $id): ?ManagementRule
    {
        $queryBuilder = $this->createQueryBuilder('managementRule');

        $subqueryIdentifier = $this->createQueryBuilder('i')
            ->select('i.identifier')
            ->where('i.id = :id')
            ->setMaxResults(1);

        $subqueryVersion = $this->createQueryBuilder('v')
            ->select('v.version')
            ->where('v.id = :id')
            ->setMaxResults(1);

        $subqueryTenant = $this->createQueryBuilder('x')
            ->select('tx.id')
            ->leftJoin('x.tenant', 'tx')
            ->where('x.id = :id')
            ->setMaxResults(1);

        $result = $queryBuilder
            ->select('managementRule', 'tenant')
            ->leftJoin('managementRule.tenant', 'tenant')
            ->where(
                $queryBuilder->expr()->in(
                    'managementRule.identifier',
                    $subqueryIdentifier->getDQL()
                )
            )
            ->andWhere(
                $queryBuilder->expr()->lte(
                    'managementRule.version',
                    '(' . $subqueryVersion->getDQL() . ')'
                )
            )
            ->andWhere(
                $queryBuilder->expr()->in(
                    'managementRule.tenant',
                    $subqueryTenant->getDQL()
                )
            )
            ->setParameter('id', $id)
            ->orderBy('managementRule.version', 'DESC')
            ->getQuery()
            ->getResult();

        /** @var ManagementRule $current */
        $current = array_shift($result);
        $current?->setPreviousVersions($result);

        return $current;
    }

    public function findByIdentifierWithPreviousVersions(string $identifier): ?ManagementRule
    {
        $queryBuilder = $this->createQueryBuilder('managementRule');

        $last = $this->createQueryBuilder('managementRule')
            ->where('managementRule.identifier = :identifier')
            ->andWhere('managementRule.state IN (:states)')
            ->orderBy('managementRule.version', 'DESC')
            ->setParameter('identifier', $identifier)
            ->setParameter(
                'states',
                [
                    VersionableEntityInterface::S_PUBLISHED,
                    VersionableEntityInterface::S_DEACTIVATED,
                    VersionableEntityInterface::S_REVOKED
                ]
            )
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        $subqueryIdentifier = $this->createQueryBuilder('i')
            ->select('i.identifier')
            ->where('i.id = :id')
            ->setMaxResults(1);

        $subqueryVersion = $this->createQueryBuilder('v')
            ->select('v.version')
            ->where('v.id = :id')
            ->setMaxResults(1);

        $subqueryTenant = $this->createQueryBuilder('x')
            ->select('tx.id')
            ->leftJoin('x.tenant', 'tx')
            ->where('x.id = :id')
            ->setMaxResults(1);

        $result = $queryBuilder
            ->select('managementRule', 'tenant')
            ->leftJoin('managementRule.tenant', 'tenant')
            ->where(
                $queryBuilder->expr()->in(
                    'managementRule.identifier',
                    $subqueryIdentifier->getDQL()
                )
            )
            ->andWhere(
                $queryBuilder->expr()->lte(
                    'managementRule.version',
                    '(' . $subqueryVersion->getDQL() . ')'
                )
            )
            ->andWhere(
                $queryBuilder->expr()->in(
                    'managementRule.tenant',
                    $subqueryTenant->getDQL()
                )
            )
            ->setParameter('id', $last->getId())
            ->orderBy('managementRule.version', 'DESC')
            ->getQuery()
            ->getResult();

        /** @var ManagementRule $current */
        $current = array_shift($result);
        $current?->setPreviousVersions($result);

        return $current;
    }

    #[Override]
    public function queryOnlyLastVersionForTenant(
        string $tenantUrl,
        ?string $identifier = null
    ): QueryBuilder {
        $alias = $this->getAlias();
        $queryBuilder = $this->createQueryBuilder($alias);
        $subquery = $this->createQueryBuilder('last')
            ->select('max(last.version)')
            ->innerJoin('last.tenant', 'lastTenant')
            ->andWhere('lastTenant.baseurl = :tenant')
            ->andWhere('last.identifier = ' . $alias . '.identifier')
            ->getQuery();

        if ($identifier) {
            $queryBuilder
                ->andWhere($alias . '.identifier = :identifier')
                ->setParameter('identifier', $identifier);
        }

        return $queryBuilder
            ->select($alias, 'tenant', 'ruleType')
            ->leftJoin($alias . '.tenant', 'tenant')
            ->leftJoin($alias . '.ruleType', 'ruleType')
            ->andWhere('tenant.baseurl = :tenant')
            ->andWhere($queryBuilder->expr()->eq($alias . '.version', '(' . $subquery->getDQL() . ')'))
            ->setParameter('tenant', $tenantUrl)
            ->orderBy($alias . '.identifier', 'ASC');
    }

    public function queryPublished(string $tenantUrl): QueryBuilder
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->select($alias, 'tenant')
            ->leftJoin($alias . '.tenant', 'tenant')
            ->andWhere('tenant.baseurl = :tenant')
            ->andWhere($alias . '.state IN (:states)')
            ->setParameter('tenant', $tenantUrl)
            ->setParameter(
                'states',
                [VersionableEntityInterface::S_PUBLISHED, VersionableEntityInterface::S_DEACTIVATED]
            )
            ->orderBy($alias . '.identifier', 'ASC');
    }

    public function queryForExport(Tenant $tenant): QueryBuilder
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->select($alias, 'tenant')
            ->leftJoin($alias . '.tenant', 'tenant')
            ->andWhere($alias . '.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->andWhere($alias . '.state = :state')
            ->setParameter('state', VersionableEntityInterface::S_PUBLISHED)
            ->orderBy($alias . '.version', 'DESC');
    }

    /**
     * @return ManagementRule[]
     */
    public function findByIdentifiersForTenant(Tenant $tenant, array $identifiers): array
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->innerJoin($alias . '.tenant', 'tenant')
            ->andWhere($alias . '.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->andWhere($alias . '.identifier IN (:identifiers)')
            ->setParameter('identifiers', $identifiers)
            ->orderBy($alias . '.version', 'DESC')
            ->addOrderBy($alias . '.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findOneAccessByTenantIdentifier(Tenant $tenant, string $identifier): ?ManagementRule
    {
        $results = $this->createQueryBuilder('mr')
            ->innerJoin('mr.tenant', 'tenant')
            ->innerJoin('mr.ruleType', 'ruleType')
            ->andWhere('mr.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->andWhere('ruleType.tenant IS NULL')
            ->andWhere('ruleType.identifier = :ruleType')
            ->setParameter('ruleType', 'AccessRule')
            ->andWhere('mr.identifier = :identifier')
            ->setParameter('identifier', $identifier)
            ->getQuery()
            ->getResult()
        ;
        return $results ? $results[0] : null;
    }

    public function findOneAppraisalByTenantDuration(Tenant $tenant, int $duration): ?ManagementRule
    {
        $results = $this->createQueryBuilder('mr')
            ->innerJoin('mr.tenant', 'tenant')
            ->innerJoin('mr.ruleType', 'ruleType')
            ->andWhere('mr.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->andWhere('ruleType.tenant IS NULL')
            ->andWhere('ruleType.identifier = :ruleType')
            ->setParameter('ruleType', 'AppraisalRule')
            ->andWhere('mr.duration = :duration1 OR mr.duration = :duration2')
            ->setParameter('duration1', sprintf('P%dY', $duration))
            ->setParameter('duration2', sprintf('P%dY0M0D', $duration))
            ->getQuery()
            ->getResult()
        ;
        return $results ? $results[0] : null;
    }

    public function findOneStorageByTenantDuration(?Tenant $tenant, int $duration): ?ManagementRule
    {
        $results = $this->createQueryBuilder('mr')
            ->innerJoin('mr.tenant', 'tenant')
            ->innerJoin('mr.ruleType', 'ruleType')
            ->andWhere('mr.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->andWhere('ruleType.tenant IS NULL')
            ->andWhere('ruleType.identifier = :ruleType')
            ->setParameter('ruleType', 'StorageRule')
            ->andWhere('mr.duration = :duration1 OR mr.duration = :duration2')
            ->setParameter('duration1', sprintf('P%dY', $duration))
            ->setParameter('duration2', sprintf('P%dY0M0D', $duration))
            ->getQuery()
            ->getResult()
        ;
        return $results ? $results[0] : null;
    }

    /**
     * Utilisé pour les parties publiques ET reader
     * @param Tenant|null $tenant
     * @return QueryBuilder
     */
    public function queryPublic(?Tenant $tenant)
    {
        return $this->queryPublicTrait($tenant)
            ->innerJoin($this->getAlias() . '.ruleType', 'ruleType');
    }
}
