<?php

namespace App\Repository;

use App\Entity\Authority;
use App\Entity\Tenant;
use App\Entity\VersionableEntityInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Override;

/**
 * @extends ServiceEntityRepository<Authority>
 *
 * @method Authority|null findOneBy(array $criteria, array $orderBy = null)
 * @method Authority[]    findAll()
 * @method Authority[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthorityRepository extends ServiceEntityRepository implements
    ResultSetRepositoryInterface,
    VersionableEntityRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Authority::class);
    }

    #[Override]
    public function queryOnlyLastVersionForTenant(string $tenantUrl, ?string $identifier = null): QueryBuilder
    {
        $alias = $this->getAlias();
        $queryBuilder = $this->createQueryBuilder($alias);
        $subquery = $this->createQueryBuilder('last')
            ->select('max(last.version)')
            ->innerJoin('last.tenant', 'lastTenant')
            ->andWhere('lastTenant.baseurl = :tenant')
            ->andWhere('last.identifier = ' . $alias . '.identifier')
            ->getQuery();

        if ($identifier) {
            $queryBuilder
                ->andWhere($alias . '.identifier = :identifier')
                ->setParameter('identifier', $identifier)
            ;
        }

        return $queryBuilder
            ->select($alias, 'tenant')
            ->leftJoin($alias . '.tenant', 'tenant')
            ->andWhere('tenant.baseurl = :tenant')
            ->andWhere($queryBuilder->expr()->eq($alias . '.version', '(' . $subquery->getDQL() . ')'))
            ->setParameter('tenant', $tenantUrl)
            ->orderBy($alias . '.identifier', 'ASC')
        ;
    }

    public function findAllVersions(string $identifier, Tenant $tenant)
    {
        return $this->createQueryBuilder('authority')
            ->select('authority', 'tenant', 'activities')
            ->leftJoin('authority.tenant', 'tenant')
            ->leftJoin('authority.activities', 'activities')
            ->andWhere('authority.identifier = :identifier')
            ->andWhere('authority.tenant = :tenant')
            ->setParameter('identifier', $identifier)
            ->setParameter('tenant', $tenant)
            ->orderBy('authority.version', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findWithPreviousVersions(string $id): ?Authority
    {
        $queryBuilder = $this->createQueryBuilder('authority');

        $subqueryIdentifier = $this->createQueryBuilder('i')
            ->select('i.identifier')
            ->where('i.id = :id')
            ->setMaxResults(1);

        $subqueryVersion = $this->createQueryBuilder('v')
            ->select('v.version')
            ->where('v.id = :id')
            ->setMaxResults(1);

        $subqueryTenant = $this->createQueryBuilder('x')
            ->select('tx.id')
            ->leftJoin('x.tenant', 'tx')
            ->where('x.id = :id')
            ->setMaxResults(1);

        $result = $queryBuilder
            ->select('authority', 'tenant')
            ->leftJoin('authority.tenant', 'tenant')
            ->where(
                $queryBuilder->expr()->in(
                    'authority.identifier',
                    $subqueryIdentifier->getDQL()
                )
            )
            ->andWhere(
                $queryBuilder->expr()->lte(
                    'authority.version',
                    '(' . $subqueryVersion->getDQL() . ')'
                )
            )
            ->andWhere(
                $queryBuilder->expr()->in(
                    'authority.tenant',
                    $subqueryTenant->getDQL()
                )
            )
            ->setParameter('id', $id)
            ->orderBy('authority.version', 'DESC')
            ->getQuery()
            ->getResult();

        /** @var Authority $current */
        $current = array_shift($result);
        $current?->setPreviousVersions($result);

        return $current;
    }

    public function findByIdentifierWithPreviousVersions(string $identifier): ?Authority
    {
        $queryBuilder = $this->createQueryBuilder('authority');

        $last = $this->createQueryBuilder('authority')
            ->where('authority.identifier = :identifier')
            ->andWhere('authority.state IN (:states)')
            ->orderBy('authority.version', 'DESC')
            ->setParameter('identifier', $identifier)
            ->setParameter(
                'states',
                [
                    VersionableEntityInterface::S_PUBLISHED,
                    VersionableEntityInterface::S_DEACTIVATED,
                    VersionableEntityInterface::S_REVOKED
                ]
            )
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        $subqueryIdentifier = $this->createQueryBuilder('i')
            ->select('i.identifier')
            ->where('i.id = :id')
            ->setMaxResults(1);

        $subqueryVersion = $this->createQueryBuilder('v')
            ->select('v.version')
            ->where('v.id = :id')
            ->setMaxResults(1);

        $subqueryTenant = $this->createQueryBuilder('x')
            ->select('tx.id')
            ->leftJoin('x.tenant', 'tx')
            ->where('x.id = :id')
            ->setMaxResults(1);

        $result = $queryBuilder
            ->select('authority', 'tenant')
            ->leftJoin('authority.tenant', 'tenant')
            ->where(
                $queryBuilder->expr()->in(
                    'authority.identifier',
                    $subqueryIdentifier->getDQL()
                )
            )
            ->andWhere(
                $queryBuilder->expr()->lte(
                    'authority.version',
                    '(' . $subqueryVersion->getDQL() . ')'
                )
            )
            ->andWhere(
                $queryBuilder->expr()->in(
                    'authority.tenant',
                    $subqueryTenant->getDQL()
                )
            )
            ->setParameter('id', $last->getId())
            ->orderBy('authority.version', 'DESC')
            ->getQuery()
            ->getResult();

        /** @var Authority $current */
        $current = array_shift($result);
        $current?->setPreviousVersions($result);

        return $current;
    }

    public function queryForExport(Tenant $tenant): QueryBuilder
    {
        return $this->createQueryBuilder('authority')
            ->select('authority', 'tenant')
            ->leftJoin('authority.tenant', 'tenant')
            ->andWhere('authority.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->andWhere('authority.state = :state')
            ->setParameter('state', VersionableEntityInterface::S_PUBLISHED)
            ->orderBy('authority.version', 'DESC')
        ;
    }

    /**
     * @return Authority[]
     */
    public function findByIdentifiersForTenant(Tenant $tenant, array $identifiers): array
    {
        return $this->createQueryBuilder('authority')
            ->innerJoin('authority.tenant', 'tenant')
            ->andWhere('authority.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->andWhere('authority.identifier IN (:identifiers)')
            ->setParameter('identifiers', $identifiers)
            ->orderBy('authority.version', 'DESC')
            ->addOrderBy('authority.name', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function queryPublished(string $tenantUrl, bool $withDeactivated = true): QueryBuilder
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->select($alias, 'tenant')
            ->leftJoin($alias . '.tenant', 'tenant')
            ->andWhere('tenant.baseurl = :tenant')
            ->andWhere($alias . '.state IN (:states)')
            ->setParameter('tenant', $tenantUrl)
            ->setParameter(
                'states',
                $withDeactivated
                    ? [VersionableEntityInterface::S_PUBLISHED, VersionableEntityInterface::S_DEACTIVATED]
                    : [VersionableEntityInterface::S_PUBLISHED]
            )
            ->orderBy($alias . '.identifier', 'ASC');
    }

    #[Override]
    public function findPreviousVersion(VersionableEntityInterface $current): ?VersionableEntityInterface
    {
        if ($current->getVersion() === 1 || !$current instanceof Authority) {
            return null;
        }

        return $this->createQueryBuilder('authority')
            ->where('authority.identifier = :identifier')
            ->setParameter('identifier', $current->getIdentifier())
            ->andWhere('authority.version = :version')
            ->andWhere('authority.tenant = :tenant')
            ->setParameter('tenant', $current->getTenant())
            ->setParameter('version', $current->getVersion() - 1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    #[Override]
    public function exportCsv(callable $appendFilters = null, array $options = []): QueryBuilder
    {
        $query = $options['from'] ?? false === 'index_published'
            ? $this->queryPublished($options['tenant_url'])
            : $this->queryOnlyLastVersionForTenant($options['tenant_url']);
        if ($appendFilters) {
            $appendFilters($query);
        }

        return $query;
    }

    public function queryOptions(string $tenantUrl): QueryBuilder
    {
        return $this->createQueryBuilder('authority')
            ->leftJoin('authority.tenant', 'tenant')
            ->where('authority.state = :state')
            ->andWhere('tenant.baseurl = :baseurl')
            ->setParameter('state', VersionableEntityInterface::S_PUBLISHED)
            ->setParameter('baseurl', $tenantUrl);
    }
}
