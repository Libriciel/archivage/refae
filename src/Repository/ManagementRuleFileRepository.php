<?php

namespace App\Repository;

use App\Entity\ManagementRuleFile;
use App\Entity\ManagementRule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ManagementRuleFile>
 *
 * @method ManagementRuleFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method ManagementRuleFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method ManagementRuleFile[]    findAll()
 * @method ManagementRuleFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ManagementRuleFileRepository extends ServiceEntityRepository implements ResultSetRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ManagementRuleFile::class);
    }

    public function queryByManagementRule(ManagementRule $managementRule): QueryBuilder
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->select($alias, 'managementRule', 'tenant', 'file')
            ->leftJoin($alias . '.managementRule', 'managementRule')
            ->leftJoin('managementRule.tenant', 'tenant')
            ->leftJoin($alias . '.file', 'file')
            ->where('managementRule = :managementRule')
            ->setParameter('managementRule', $managementRule)
            ->orderBy('file.name', 'ASC');
    }
}
