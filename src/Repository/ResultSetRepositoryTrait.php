<?php

namespace App\Repository;

use App\Entity\Tenant;
use App\Entity\VersionableEntityInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * @mixin ServiceEntityRepository
 */
trait ResultSetRepositoryTrait
{
    public function getAlias(): string
    {
        $classname = static::class;
        return strtolower(substr($classname, strrpos($classname, '\\') + 1, 1));
    }

    public function getResultsetQuery(callable $appendFilters = null): QueryBuilder
    {
        $alias = $this->getAlias();
        $queryBuilder = $this->createQueryBuilder($alias)
            ->orderBy(sprintf('%s.id', $alias), 'ASC');
        if ($appendFilters) {
            $appendFilters($queryBuilder);
        }
        return $queryBuilder;
    }

    public function filterCount(QueryBuilder $query): int
    {
        $queryAlias = $query->getAllAliases()[0] ?? $this->getAlias();
        $alias = $queryAlias . 'count';
        return $this->createQueryBuilder($alias)
            ->select('COUNT(' . $alias . '.id)')
            ->where(
                $this->createQueryBuilder($alias)->expr()->in(
                    $alias . '.id',
                    (clone $query)->select($queryAlias . '.id')->getDQL()
                )
            )
            ->setParameters($query->getParameters())
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function exportCsv(callable $appendFilters = null, array $options = []): QueryBuilder
    {
        $alias = $this->getAlias();
        $queryBuilder = $this->createQueryBuilder($alias)
            ->orderBy(sprintf('%s.id', $alias), 'ASC');
        if ($appendFilters) {
            $appendFilters($queryBuilder);
        }
        return $queryBuilder;
    }

    /**
     * Utilisé pour les parties publiques ET reader
     * @param Tenant|null $tenant
     * @return QueryBuilder
     */
    public function queryPublic(?Tenant $tenant)
    {
        $alias = $this->getAlias();
        $queryBuilder = $this->createQueryBuilder($alias)
            ->innerJoin($alias . '.tenant', 'tenant')
            ->andWhere($alias . '.state = :state')
            ->andWhere('tenant.active = true')
            ->setParameter('state', VersionableEntityInterface::S_PUBLISHED)
        ;
        if ($tenant) {
            $queryBuilder
                ->andWhere($alias . '.tenant = :tenant')
                ->setParameter('tenant', $tenant);
        } else { // pour les reader, données publiques et non publiques
            $queryBuilder
                ->andWhere($alias . '.public = true');
        }
        return $queryBuilder;
    }
}
