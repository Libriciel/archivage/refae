<?php

namespace App\Repository;

use App\Entity\DocumentRuleImportSession;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DocumentRuleImportSession>
 *
 * @method DocumentRuleImportSession|null find($id, $lockMode = null, $lockVersion = null)
 * @method DocumentRuleImportSession|null findOneBy(array $criteria, array $orderBy = null)
 * @method DocumentRuleImportSession[]    findAll()
 * @method DocumentRuleImportSession[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DocumentRuleImportSessionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DocumentRuleImportSession::class);
    }
}
