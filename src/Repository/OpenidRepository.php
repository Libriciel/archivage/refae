<?php

namespace App\Repository;

use App\Entity\Openid;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Openid>
 *
 * @method Openid|null find($id, $lockMode = null, $lockVersion = null)
 * @method Openid|null findOneBy(array $criteria, array $orderBy = null)
 * @method Openid[]    findAll()
 * @method Openid[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OpenidRepository extends ServiceEntityRepository implements ResultSetRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Openid::class);
    }

    public function findByTenantBaseurl(string $tenant_url)
    {
        return $this->createQueryBuilder('o')
            ->innerJoin('ou.tenant', 't')
            ->andWhere('t.baseurl = :val')
            ->setParameter('val', $tenant_url)
            ->orderBy('o.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}
