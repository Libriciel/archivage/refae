<?php

namespace App\Repository;

use App\Entity\Tenant;
use App\Entity\VersionableEntityInterface;
use App\Entity\Vocabulary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Override;

/**
 * @extends ServiceEntityRepository<Vocabulary>
 *
 * @method Vocabulary|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vocabulary|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vocabulary[]    findAll()
 * @method Vocabulary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VocabularyRepository extends ServiceEntityRepository implements
    ResultSetRepositoryInterface,
    VersionableEntityRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Vocabulary::class);
    }

    /**
     * @return Vocabulary[]
     */
    public function findByIdentifiersForTenant(Tenant $tenant, array $identifiers): array
    {
        return $this->createQueryBuilder('vocabulary')
            ->innerJoin('vocabulary.tenant', 'tenant')
            ->andWhere('vocabulary.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->andWhere('vocabulary.identifier IN (:identifiers)')
            ->setParameter('identifiers', $identifiers)
            ->orderBy('vocabulary.version', 'DESC')
            ->addOrderBy('vocabulary.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findWithPreviousVersions(string $id): ?Vocabulary
    {
        $queryBuilder = $this->createQueryBuilder('vocabulary');

        $subqueryIdentifier = $this->createQueryBuilder('i')
            ->select('i.identifier')
            ->where('i.id = :id')
            ->setMaxResults(1);

        $subqueryVersion = $this->createQueryBuilder('v')
            ->select('v.version')
            ->where('v.id = :id')
            ->setMaxResults(1);

        $subqueryTenant = $this->createQueryBuilder('x')
            ->select('tx.id')
            ->leftJoin('x.tenant', 'tx')
            ->where('x.id = :id')
            ->setMaxResults(1);

        $result = $this->createQueryBuilder('vocabulary')
            ->select('vocabulary', 'tenant')
            ->leftJoin('vocabulary.tenant', 'tenant')
            ->where(
                $queryBuilder->expr()->in(
                    'vocabulary.identifier',
                    $subqueryIdentifier->getDQL()
                )
            )
            ->andWhere(
                $queryBuilder->expr()->lte(
                    'vocabulary.version',
                    '(' . $subqueryVersion->getDQL() . ')'
                )
            )
            ->andWhere(
                $queryBuilder->expr()->in(
                    'vocabulary.tenant',
                    $subqueryTenant->getDQL()
                )
            )
            ->setParameter('id', $id)
            ->orderBy('vocabulary.version', 'DESC')
            ->getQuery()
            ->getResult();

        /** @var Vocabulary $current */
        $current = array_shift($result);
        $current?->setPreviousVersions($result);

        return $current;
    }

    #[Override]
    public function queryOnlyLastVersionForTenant(string $tenantUrl, ?string $identifier = null): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('v');
        $subquery = $this->createQueryBuilder('vocabulary_last')
            ->select('max(vocabulary_last.version)')
            ->where('vocabulary_last.identifier = v.identifier')
            ->getQuery();

        if ($identifier) {
            $queryBuilder
                ->andWhere('v.identifier = :identifier')
                ->setParameter('identifier', $identifier)
            ;
        }

        return $queryBuilder
            ->select('v', 'tenant')
            ->leftJoin('v.tenant', 'tenant')
            ->andWhere('tenant.baseurl = :tenant')
            ->andWhere($queryBuilder->expr()->eq('v.version', '(' . $subquery->getDQL() . ')'))
            ->setParameter('tenant', $tenantUrl)
            ->orderBy('v.identifier', 'ASC');
    }

    public function queryPublished(string $tenantUrl): QueryBuilder
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->select('v', 'tenant')
            ->leftJoin('v.tenant', 'tenant')
            ->andWhere('tenant.baseurl = :tenant')
            ->andWhere('v.state IN (:states)')
            ->setParameter('tenant', $tenantUrl)
            ->setParameter(
                'states',
                [VersionableEntityInterface::S_PUBLISHED, VersionableEntityInterface::S_DEACTIVATED]
            )
            ->orderBy($alias . '.identifier', 'ASC');
    }

    #[Override]
    public function findPreviousVersion(VersionableEntityInterface $current): ?Vocabulary
    {
        if ($current->getVersion() === 1) {
            return null;
        }

        return $this->createQueryBuilder('vocabulary')
            ->where('vocabulary.identifier = :identifier')
            ->setParameter('identifier', $current->getIdentifier())
            ->andWhere('vocabulary.version = :version')
            ->andWhere('vocabulary.tenant = :tenant')
            ->setParameter('tenant', $current->getTenant())
            ->setParameter('version', $current->getVersion() - 1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByIdentifierWithPreviousVersions(string $identifier): ?Vocabulary
    {
        $queryBuilder = $this->createQueryBuilder('vocabulary');

        $last = $this->createQueryBuilder('vocabulary')
            ->where('vocabulary.identifier = :identifier')
            ->andWhere('vocabulary.state IN (:states)')
            ->orderBy('vocabulary.version', 'DESC')
            ->setParameter('identifier', $identifier)
            ->setParameter(
                'states',
                [
                    VersionableEntityInterface::S_PUBLISHED,
                    VersionableEntityInterface::S_DEACTIVATED,
                    VersionableEntityInterface::S_REVOKED
                ]
            )
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        $subqueryIdentifier = $this->createQueryBuilder('i')
            ->select('i.identifier')
            ->where('i.id = :id')
            ->setMaxResults(1);

        $subqueryVersion = $this->createQueryBuilder('v')
            ->select('v.version')
            ->where('v.id = :id')
            ->setMaxResults(1);

        $subqueryTenant = $this->createQueryBuilder('x')
            ->select('tx.id')
            ->leftJoin('x.tenant', 'tx')
            ->where('x.id = :id')
            ->setMaxResults(1);

        $result = $queryBuilder
            ->select('vocabulary', 'tenant')
            ->leftJoin('vocabulary.tenant', 'tenant')
            ->where(
                $queryBuilder->expr()->in(
                    'vocabulary.identifier',
                    $subqueryIdentifier->getDQL()
                )
            )
            ->andWhere(
                $queryBuilder->expr()->lte(
                    'vocabulary.version',
                    '(' . $subqueryVersion->getDQL() . ')'
                )
            )
            ->andWhere(
                $queryBuilder->expr()->in(
                    'vocabulary.tenant',
                    $subqueryTenant->getDQL()
                )
            )
            ->setParameter('id', $last->getId())
            ->orderBy('vocabulary.version', 'DESC')
            ->getQuery()
            ->getResult();

        /** @var Vocabulary $current */
        $current = array_shift($result);
        $current?->setPreviousVersions($result);

        return $current;
    }

    public function queryForExport(Tenant $tenant): QueryBuilder
    {
        return $this->createQueryBuilder('vocabulary')
            ->select('vocabulary', 'tenant')
            ->leftJoin('vocabulary.tenant', 'tenant')
            ->andWhere('vocabulary.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->andWhere('vocabulary.state = :state')
            ->setParameter('state', VersionableEntityInterface::S_PUBLISHED)
            ->orderBy('vocabulary.version', 'DESC');
    }

    #[Override]
    public function exportCsv(callable $appendFilters = null, array $options = []): QueryBuilder
    {
        $query = $options['from'] ?? false === 'index_published'
            ? $this->queryPublished($options['tenant_url'])
            : $this->queryOnlyLastVersionForTenant($options['tenant_url']);
        if ($appendFilters) {
            $appendFilters($query);
        }
        return $query;
    }
}
