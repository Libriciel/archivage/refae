<?php

namespace App\Repository;

use App\Entity\Notify;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Notify>
 *
 * @method Notify|null find($id, $lockMode = null, $lockVersion = null)
 * @method Notify|null findOneBy(array $criteria, array $orderBy = null)
 * @method Notify[]    findAll()
 * @method Notify[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NotifyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Notify::class);
    }
}
