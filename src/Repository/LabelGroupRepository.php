<?php

namespace App\Repository;

use App\Entity\LabelGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<LabelGroup>
 *
 * @method LabelGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method LabelGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method LabelGroup[]    findAll()
 * @method LabelGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LabelGroupRepository extends ServiceEntityRepository implements ResultSetRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LabelGroup::class);
    }

    public function queryForTenant(string $tenantUrl): QueryBuilder
    {
        return $this->createQueryBuilder('labelGroup')
            ->select('labelGroup')
            ->leftJoin('labelGroup.tenant', 'tenant')
            ->where('tenant.baseurl = :baseurl')
            ->setParameter('baseurl', $tenantUrl)
            ->orderBy('labelGroup.name', 'ASC');
    }

    public function queryOptions(string $tenantUrl): QueryBuilder
    {
        return $this->createQueryBuilder('lg')
            ->innerJoin('lg.tenant', 't')
            ->where('t.baseurl = :baseurl')
            ->setParameter('baseurl', $tenantUrl)
            ->orderBy('lg.name')
        ;
    }
}
