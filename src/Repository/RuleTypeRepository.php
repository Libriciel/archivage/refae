<?php

namespace App\Repository;

use App\Entity\RuleType;
use App\Entity\Tenant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RuleType>
 *
 * @method RuleType|null find($id, $lockMode = null, $lockVersion = null)
 * @method RuleType|null findOneBy(array $criteria, array $orderBy = null)
 * @method RuleType[]    findAll()
 * @method RuleType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RuleTypeRepository extends ServiceEntityRepository implements ResultSetRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RuleType::class);
    }

    public function queryForTenant(string $tenantUrl): QueryBuilder
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->addSelect($alias, 't')
            ->leftJoin($alias . '.tenant', 't')
            ->where('t.baseurl = :baseurl')
            ->orWhere($alias . '.tenant IS NULL')
            ->setParameter('baseurl', $tenantUrl)
            ->orderBy($alias . '.name')
        ;
    }

    public function findOneForImport(Tenant $tenant, string $name): ?RuleType
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->select($alias)
            ->andWhere($alias . '.name = :name')
            ->andWhere($alias . '.tenant = :tenant')
            ->setParameter('name', $name)
            ->setParameter('tenant', $tenant)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function queryForExport(Tenant $tenant): QueryBuilder
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->select($alias)
            ->leftJoin($alias . '.tenant', 'tenant')
            ->andWhere($alias . '.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->orderBy($alias . '.name');
    }

    public function queryOptions(?string $tenantUrl): QueryBuilder
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->leftJoin($alias . '.tenant', 't')
            ->where('t.baseurl = :baseurl')
            ->orWhere($alias . '.tenant IS NULL')
            ->setParameter('baseurl', $tenantUrl)
            ->orderBy($alias . '.name');
    }

    public function queryChoicesForFilter(?string $tenantUrl = null): array
    {
        return array_reduce(
            $this->queryOptions($tenantUrl)
                ->getQuery()
                ->getResult(),
            function (array $carry, RuleType $item): array {
                $carry[$item->getName()] = (string)$item->getIdentifier();
                return $carry;
            },
            []
        );
    }

    public function getByIdentifier(array $criteria): array
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->leftJoin($alias . '.tenant', 't')
            ->where($alias . '.identifier = :identifier')
            ->andWhere($alias . '.tenant = :tenant OR ' . $alias . '.tenant IS NULL')
            ->setParameter('identifier', $criteria['identifier'])
            ->setParameter('tenant', $criteria['tenant'])
            ->getQuery()
            ->getResult();
    }
}
