<?php

namespace App\Repository;

use App\Entity\Label;
use App\Entity\Tenant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Label>
 *
 * @method Label|null find($id, $lockMode = null, $lockVersion = null)
 * @method Label|null findOneBy(array $criteria, array $orderBy = null)
 * @method Label[]    findAll()
 * @method Label[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LabelRepository extends ServiceEntityRepository implements ResultSetRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Label::class);
    }

    public function findList(Tenant $tenant): array
    {
        $query = $this->queryForTenant($tenant->getBaseurl())
            ->orderBy('l.labelGroup, l.name');
        $results = $query->getQuery()->getResult();

        return array_map(fn(Label $label) => $label->getConcatLabelGroupName(), $results);
    }

    public function queryOptions(?string $tenantUrl): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('l')
            ->addSelect('CASE WHEN lg.id IS NULL THEN 0 ELSE 1 END AS HIDDEN noGroup')
            ->leftJoin('l.labelGroup', 'lg')
            ->orderBy('noGroup, lg.name, l.name')
        ;
        if ($tenantUrl) {
            $queryBuilder
                ->innerJoin('l.tenant', 't')
                ->where('t.baseurl = :baseurl')
                ->setParameter('baseurl', $tenantUrl)
            ;
        }
        return $queryBuilder;
    }

    public function queryForTenant(string $tenantUrl): QueryBuilder
    {
        return $this->createQueryBuilder('l')
            ->select('l', 'labelGroup')
            ->leftJoin('l.labelGroup', 'labelGroup')
            ->innerJoin('l.tenant', 'tenant')
            ->where('tenant.baseurl = :baseurl')
            ->setParameter('baseurl', $tenantUrl)
            ->orderBy('l.name', 'ASC')
        ;
    }

    public function queryForExport(Tenant $tenant): QueryBuilder
    {
        return $this->createQueryBuilder('label')
            ->select('label', 'tenant', 'labelGroup')
            ->leftJoin('label.tenant', 'tenant')
            ->leftJoin('label.labelGroup', 'labelGroup')
            ->andWhere('label.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->orderBy('label.name', 'DESC');
    }

    public function findOneForImport(Tenant $tenant, string $name, ?string $group = null): ?Label
    {
        $query = $this->createQueryBuilder('l')
            ->select('l', 'labelGroup')
            ->leftJoin('l.labelGroup', 'labelGroup')
            ->andWhere('l.name = :name')
            ->andWhere('l.tenant = :tenant')
            ->setParameter('name', $name)
            ->setParameter('tenant', $tenant);

        if ($group) {
            $query
                ->andWhere('labelGroup.name = :group')
                ->setParameter('group', $group);
        } else {
            $query
                ->andWhere('l.labelGroup IS NULL');
        }

        return $query
            ->getQuery()
            ->getOneOrNullResult();
    }
}
