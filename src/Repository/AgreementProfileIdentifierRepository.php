<?php

namespace App\Repository;

use App\Entity\AgreementProfileIdentifier;
use App\Entity\Profile;
use App\Entity\VersionableEntityInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AgreementProfileIdentifier>
 *
 * @method AgreementProfileIdentifier|null findOneBy(array $criteria, array $orderBy = null)
 * @method AgreementProfileIdentifier[]    findAll()
 * @method AgreementProfileIdentifier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AgreementProfileIdentifierRepository extends ServiceEntityRepository
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AgreementProfileIdentifier::class);
    }

    public function findForAgreement(string $agreementId): array
    {
        $alias = $this->getAlias();
        $profileIdentifiers = $this->createQueryBuilder($alias)
            ->select($alias)
            ->addSelect(
                '(SELECT COUNT(profile.id) FROM ' . Profile::class . ' profile '
                . 'WHERE profile.identifier = ' . $alias . '.identifier'
                . ') AS exists'
            )
            ->addSelect(
                '(SELECT COUNT(originalPublished.id) FROM ' . Profile::class . ' originalPublished '
                . 'WHERE originalPublished.identifier = ' . $alias . '.identifier '
                . 'AND originalPublished.state = :published '
                . 'AND originalPublished.tenant = agreement.tenant'
                . ') AS published'
            )
            ->leftJoin($alias . '.agreement', 'agreement')
            ->andWhere('agreement.id = :agreement')
            ->setParameter('agreement', $agreementId)
            ->setParameter('published', VersionableEntityInterface::S_PUBLISHED)
            ->getQuery()
            ->getResult();

        return array_map(
            fn(array $a): AgreementProfileIdentifier => $a[0]
                ->setOriginalExists($a['exists'])
                ->setOriginalIsPublished(!$a['exists'] || $a['published']),
            $profileIdentifiers
        );
    }
}
