<?php

namespace App\Repository;

use App\Entity\VocabularyFile;
use App\Entity\Vocabulary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<VocabularyFile>
 *
 * @method VocabularyFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method VocabularyFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method VocabularyFile[]    findAll()
 * @method VocabularyFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VocabularyFileRepository extends ServiceEntityRepository implements ResultSetRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VocabularyFile::class);
    }

    public function queryByVocabulary(Vocabulary $vocabulary)
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->select($alias, 'vocabulary', 'file', 'tenant')
            ->leftJoin($alias . '.vocabulary', 'vocabulary')
            ->leftJoin('vocabulary.tenant', 'tenant')
            ->leftJoin($alias . '.file', 'file')
            ->where('vocabulary = :vocabulary')
            ->setParameter('vocabulary', $vocabulary)
            ->orderBy('file.created', 'ASC');
    }
}
