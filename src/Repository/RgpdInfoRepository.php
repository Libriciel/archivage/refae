<?php

namespace App\Repository;

use App\Entity\RgpdInfo;
use App\Entity\Tenant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RgpdInfo>
 *
 * @method RgpdInfo|null find($id, $lockMode = null, $lockVersion = null)
 * @method RgpdInfo|null findOneBy(array $criteria, array $orderBy = null)
 * @method RgpdInfo[]    findAll()
 * @method RgpdInfo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RgpdInfoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RgpdInfo::class);
    }

    public function findOneForTenant(?Tenant $tenant): ?RgpdInfo
    {
        $queryBuilder = $this->createQueryBuilder('rgpdInfo');
        if ($tenant) {
            $queryBuilder
                ->andWhere('rgpdInfo.tenant = :tenant')
                ->setParameter('tenant', $tenant)
            ;
        } else {
            $queryBuilder->andWhere('rgpdInfo.tenant IS NULL');
        }
        return $queryBuilder
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
