<?php

namespace App\Repository;

use App\Entity\TenantConfiguration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TenantConfiguration>
 *
 * @method TenantConfiguration|null find($id, $lockMode = null, $lockVersion = null)
 * @method TenantConfiguration|null findOneBy(array $criteria, array $orderBy = null)
 * @method TenantConfiguration[]    findAll()
 * @method TenantConfiguration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TenantConfigurationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TenantConfiguration::class);
    }
}
