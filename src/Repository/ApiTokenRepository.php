<?php

namespace App\Repository;

use App\Entity\ApiToken;
use DateInterval;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ApiToken>
 *
 * @method ApiToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApiToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApiToken[]    findAll()
 * @method ApiToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApiTokenRepository extends ServiceEntityRepository
{
    public const string TOKEN_EXPIRE = 'PT24M';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ApiToken::class);
    }

    public function findBytoken(string $token): ?ApiToken
    {
        return $this->createQueryBuilder('api_token')
            ->select('api_token', 'tenant', 'user')
            ->leftJoin('api_token.tenant', 'tenant')
            ->leftJoin('api_token.user', 'user')
            ->where('api_token.token = :token')
            ->setParameter('token', $token)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function deleteExpired(): void
    {
        $limitDate = (new DateTime())->sub(new DateInterval(self::TOKEN_EXPIRE));
        $this->createQueryBuilder('api_token')
            ->delete()
            ->where('api_token.lastAccess < :limit')
            ->setParameter('limit', $limitDate)
            ->getQuery()
            ->execute()
        ;
    }

    public function deleteByToken(string $token): void
    {
        $this->createQueryBuilder('api_token')
            ->delete()
            ->where('api_token.token = :token')
            ->setParameter('token', $token)
            ->getQuery()
            ->execute()
        ;
    }
}
