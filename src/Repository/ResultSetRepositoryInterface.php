<?php

namespace App\Repository;

use Doctrine\ORM\QueryBuilder;

interface ResultSetRepositoryInterface
{
    public function getResultsetQuery(callable $appendFilters = null): QueryBuilder;

    public function filterCount(QueryBuilder $query): int;

    public function exportCsv(callable $appendFilters = null, array $options = []): QueryBuilder;
}
