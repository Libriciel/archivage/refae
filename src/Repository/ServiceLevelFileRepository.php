<?php

namespace App\Repository;

use App\Entity\ServiceLevelFile;
use App\Entity\ServiceLevel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ServiceLevelFile>
 *
 * @method ServiceLevelFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method ServiceLevelFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method ServiceLevelFile[]    findAll()
 * @method ServiceLevelFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServiceLevelFileRepository extends ServiceEntityRepository implements ResultSetRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ServiceLevelFile::class);
    }

    public function queryByServiceLevel(ServiceLevel $serviceLevel): QueryBuilder
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->select($alias, 'serviceLevel', 'file', 'tenant')
            ->leftJoin($alias . '.serviceLevel', 'serviceLevel')
            ->leftJoin('serviceLevel.tenant', 'tenant')
            ->leftJoin($alias . '.file', 'file')
            ->where('serviceLevel = :serviceLevel')
            ->setParameter('serviceLevel', $serviceLevel)
            ->orderBy('file.name', 'ASC');
    }
}
