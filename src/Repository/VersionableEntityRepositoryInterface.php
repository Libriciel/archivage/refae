<?php

namespace App\Repository;

use App\Entity\VersionableEntityInterface;
use Doctrine\ORM\QueryBuilder;

interface VersionableEntityRepositoryInterface
{
    public function findPreviousVersion(VersionableEntityInterface $current): ?VersionableEntityInterface;
    public function queryOnlyLastVersionForTenant(string $tenantUrl, ?string $identifier = null): QueryBuilder;
}
