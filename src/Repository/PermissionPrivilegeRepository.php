<?php

namespace App\Repository;

use App\Entity\PermissionPrivilege;
use App\Entity\Privilege;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PermissionPrivilege>
 *
 * @method PermissionPrivilege|null find($id, $lockMode = null, $lockVersion = null)
 * @method PermissionPrivilege|null findOneBy(array $criteria, array $orderBy = null)
 * @method PermissionPrivilege[]    findAll()
 * @method PermissionPrivilege[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PermissionPrivilegeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PermissionPrivilege::class);
    }

    public function findOrCreateIfNotExistsDefault(
        Privilege $privilegeEntity,
        int|string $permissionClass,
        mixed $actions
    ): array {
        $actionEntities = [];
        foreach ($actions as $action) {
            $exists = $this->createQueryBuilder('p')
                ->select('p.id')
                ->andWhere('p.privilege = :privilege')
                ->andWhere('p.permissionClassname = :pclassname')
                ->andWhere('p.action = :paction')
                ->setParameter('privilege', $privilegeEntity->getId())
                ->setParameter('pclassname', $permissionClass)
                ->setParameter('paction', $action)
                ->getQuery()
                ->getOneOrNullResult()
            ;
            if (!$exists) {
                $permissionPrivilege = new PermissionPrivilege();
                $permissionPrivilege->setPrivilege($privilegeEntity);
                $permissionPrivilege->setPermissionClassname($permissionClass);
                $permissionPrivilege->setAction($action);
                $this->_em->persist($permissionPrivilege);
                $this->_em->flush();
                $actionEntities[] = $permissionPrivilege;
            } else {
                $actionEntities[] = $this->find($exists);
            }
        }

        // delete not in $actions
        $this->createQueryBuilder('p')
            ->delete()
            ->andWhere('p.privilege = :privilege')
            ->andWhere('p.permissionClassname = :pclassname')
            ->andWhere('p.action NOT IN (:pactions)')
            ->setParameter('privilege', $privilegeEntity->getId())
            ->setParameter('pclassname', $permissionClass)
            ->setParameter('pactions', $actions)
            ->getQuery()
            ->execute();

        return $actionEntities;
    }

    public function removeNonExistentPermissionPrivileges(array $allClasses)
    {
        $this->createQueryBuilder('p')
            ->delete()
            ->andWhere('p.permissionClassname NOT IN (:pclassnames)')
            ->setParameter('pclassnames', $allClasses)
            ->getQuery()
            ->execute();
    }

    public function findForCache(): array
    {
        return $this->createQueryBuilder('pp')
            ->select('pp', 'p', 'r')
            ->innerJoin('pp.privilege', 'p')
            ->innerJoin('p.roles', 'r')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return PermissionPrivilege[]
     */
    public function findUseless(): array
    {
        return $this->createQueryBuilder('pp')
            ->select('pp', 'p', 'r')
            ->innerJoin('pp.privilege', 'p')
            ->leftJoin('p.roles', 'r')
            ->where('r.id IS NULL')
            ->getQuery()
            ->getResult()
        ;
    }
}
