<?php

namespace App\Repository;

use App\Entity\Ldap;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Ldap>
 *
 * @method Ldap|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ldap|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ldap[]    findAll()
 * @method Ldap[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LdapRepository extends ServiceEntityRepository implements ResultSetRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ldap::class);
    }
}
