<?php

namespace App\Repository;

use App\Entity\AgreementArchivalAgencyIdentifier;
use App\Entity\Authority;
use App\Entity\VersionableEntityInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AgreementArchivalAgencyIdentifier>
 *
 * @method AgreementArchivalAgencyIdentifier|null findOneBy(array $criteria, array $orderBy = null)
 * @method AgreementArchivalAgencyIdentifier[]    findAll()
 * @method AgreementArchivalAgencyIdentifier[]    findBy(array $crit, array $ord = null, $limit = null, $offset = null)
 */
class AgreementArchivalAgencyIdentifierRepository extends ServiceEntityRepository
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AgreementArchivalAgencyIdentifier::class);
    }

    public function findForAgreement(string $agreementId): array
    {
        $alias = $this->getAlias();
        $archivalIdentifiers = $this->createQueryBuilder($alias)
            ->select($alias)
            ->addSelect(
                '(SELECT COUNT(authority.id) FROM ' . Authority::class . ' authority '
                . 'WHERE authority.identifier = ' . $alias . '.identifier'
                . ') AS exists'
            )
            ->addSelect(
                '(SELECT COUNT(originalPublished.id) FROM ' . Authority::class . ' originalPublished '
                . 'WHERE originalPublished.identifier = ' . $alias . '.identifier '
                . 'AND originalPublished.state = :published '
                . 'AND originalPublished.tenant = agreement.tenant'
                . ') AS published'
            )
            ->leftJoin($alias . '.agreement', 'agreement')
            ->andWhere('agreement.id = :agreement')
            ->setParameter('agreement', $agreementId)
            ->setParameter('published', VersionableEntityInterface::S_PUBLISHED)
            ->getQuery()
            ->getResult();

        return array_map(
            fn(array $a): AgreementArchivalAgencyIdentifier => $a[0]
                ->setOriginalExists($a['exists'])
                ->setOriginalIsPublished(!$a['exists'] || $a['published']),
            $archivalIdentifiers
        );
    }
}
