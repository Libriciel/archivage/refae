<?php

namespace App\Repository;

use App\Entity\Agreement;
use App\Entity\AgreementFile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AgreementFile>
 *
 * @method AgreementFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method AgreementFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method AgreementFile[]    findAll()
 * @method AgreementFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AgreementFileRepository extends ServiceEntityRepository implements ResultSetRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AgreementFile::class);
    }

    public function queryByAgreement(Agreement $agreement): QueryBuilder
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->select($alias, 'agreement', 'file', 'tenant')
            ->leftJoin($alias . '.agreement', 'agreement')
            ->leftJoin('agreement.tenant', 'tenant')
            ->leftJoin($alias . '.file', 'file')
            ->where('agreement = :agreement')
            ->setParameter('agreement', $agreement)
            ->orderBy('file.name', 'ASC');
    }
}
