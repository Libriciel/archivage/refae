<?php

namespace App\Repository;

use App\Entity\CronExecution;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CronExecution>
 *
 * @method CronExecution|null find($id, $lockMode = null, $lockVersion = null)
 * @method CronExecution|null findOneBy(array $criteria, array $orderBy = null)
 * @method CronExecution[]    findAll()
 * @method CronExecution[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CronExecutionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CronExecution::class);
    }

    public function updateReport(string $id, ?string $report): void
    {
        $this->createQueryBuilder('c')
            ->update()
            ->set('c.report', ':report')
            ->setParameter('report', $report)
            ->where('c.id = :id')
            ->setParameter('id', $id)
        ;
    }
}
