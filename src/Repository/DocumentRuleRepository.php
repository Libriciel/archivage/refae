<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\DocumentRule;
use App\Entity\Tenant;
use App\Entity\VersionableEntityInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Override;

/**
 * @extends ServiceEntityRepository<DocumentRule>
 *
 * @method DocumentRule|null find($id, $lockMode = null, $lockVersion = null)
 * @method DocumentRule|null findOneBy(array $criteria, array $orderBy = null)
 * @method DocumentRule[]    findAll()
 * @method DocumentRule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DocumentRuleRepository extends ServiceEntityRepository implements
    ResultSetRepositoryInterface,
    VersionableEntityRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DocumentRule::class);
    }

    #[Override]
    public function findPreviousVersion(VersionableEntityInterface $current): ?VersionableEntityInterface
    {
        if ($current->getVersion() === 1 || !$current instanceof DocumentRule) {
            return null;
        }

        $alias = $this->getAlias();

        return $this->createQueryBuilder($alias)
            ->where($alias . '.identifier = :identifier')
            ->setParameter('identifier', $current->getIdentifier())
            ->andWhere($alias . '.version = :version')
            ->andWhere($alias . '.tenant = :tenant')
            ->setParameter('tenant', $current->getTenant())
            ->setParameter('version', $current->getVersion() - 1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findWithPreviousVersions(string $id): ?DocumentRule
    {
        $queryBuilder = $this->createQueryBuilder('documentRule');

        $subqueryIdentifier = $this->createQueryBuilder('i')
            ->select('i.identifier')
            ->where('i.id = :id')
            ->setMaxResults(1);

        $subqueryVersion = $this->createQueryBuilder('v')
            ->select('v.version')
            ->where('v.id = :id')
            ->setMaxResults(1);

        $subqueryTenant = $this->createQueryBuilder('x')
            ->select('tx.id')
            ->leftJoin('x.tenant', 'tx')
            ->where('x.id = :id')
            ->setMaxResults(1);

        $result = $queryBuilder
            ->select('documentRule', 'tenant')
            ->leftJoin('documentRule.tenant', 'tenant')
            ->where(
                $queryBuilder->expr()->in(
                    'documentRule.identifier',
                    $subqueryIdentifier->getDQL()
                )
            )
            ->andWhere(
                $queryBuilder->expr()->lte(
                    'documentRule.version',
                    '(' . $subqueryVersion->getDQL() . ')'
                )
            )
            ->andWhere(
                $queryBuilder->expr()->in(
                    'documentRule.tenant',
                    $subqueryTenant->getDQL()
                )
            )
            ->setParameter('id', $id)
            ->orderBy('documentRule.version', 'DESC')
            ->getQuery()
            ->getResult();

        /** @var DocumentRule $current */
        $current = array_shift($result);
        $current?->setPreviousVersions($result);

        return $current;
    }

    public function findByIdentifierWithPreviousVersions(string $identifier): ?DocumentRule
    {
        $queryBuilder = $this->createQueryBuilder('documentRule');

        $last = $this->createQueryBuilder('documentRule')
            ->where('documentRule.identifier = :identifier')
            ->andWhere('documentRule.state IN (:states)')
            ->orderBy('documentRule.version', 'DESC')
            ->setParameter('identifier', $identifier)
            ->setParameter(
                'states',
                [
                    VersionableEntityInterface::S_PUBLISHED,
                    VersionableEntityInterface::S_DEACTIVATED,
                    VersionableEntityInterface::S_REVOKED
                ]
            )
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        $subqueryIdentifier = $this->createQueryBuilder('i')
            ->select('i.identifier')
            ->where('i.id = :id')
            ->setMaxResults(1);

        $subqueryVersion = $this->createQueryBuilder('v')
            ->select('v.version')
            ->where('v.id = :id')
            ->setMaxResults(1);

        $subqueryTenant = $this->createQueryBuilder('x')
            ->select('tx.id')
            ->leftJoin('x.tenant', 'tx')
            ->where('x.id = :id')
            ->setMaxResults(1);

        $result = $queryBuilder
            ->select('documentRule', 'tenant')
            ->leftJoin('documentRule.tenant', 'tenant')
            ->where(
                $queryBuilder->expr()->in(
                    'documentRule.identifier',
                    $subqueryIdentifier->getDQL()
                )
            )
            ->andWhere(
                $queryBuilder->expr()->lte(
                    'documentRule.version',
                    '(' . $subqueryVersion->getDQL() . ')'
                )
            )
            ->andWhere(
                $queryBuilder->expr()->in(
                    'documentRule.tenant',
                    $subqueryTenant->getDQL()
                )
            )
            ->setParameter('id', $last->getId())
            ->orderBy('documentRule.version', 'DESC')
            ->getQuery()
            ->getResult();

        /** @var DocumentRule $current */
        $current = array_shift($result);
        $current?->setPreviousVersions($result);

        return $current;
    }

    #[Override]
    public function queryOnlyLastVersionForTenant(
        string $tenantUrl,
        ?string $identifier = null
    ): QueryBuilder {
        $alias = $this->getAlias();
        $queryBuilder = $this->createQueryBuilder($alias);
        $subquery = $this->createQueryBuilder('last')
            ->select('max(last.version)')
            ->innerJoin('last.tenant', 'lastTenant')
            ->andWhere('lastTenant.baseurl = :tenant')
            ->andWhere('last.identifier = ' . $alias . '.identifier')
            ->getQuery();

        if ($identifier) {
            $queryBuilder
                ->andWhere($alias . '.identifier = :identifier')
                ->setParameter('identifier', $identifier)
            ;
        }

        return $queryBuilder
            ->select($alias, 'tenant')
            ->leftJoin($alias . '.tenant', 'tenant')
            ->andWhere('tenant.baseurl = :tenant')
            ->andWhere($queryBuilder->expr()->eq($alias . '.version', '(' . $subquery->getDQL() . ')'))
            ->setParameter('tenant', $tenantUrl)
            ->orderBy($alias . '.identifier', 'ASC');
    }

    public function queryForExport(Tenant $tenant): QueryBuilder
    {
        return $this->createQueryBuilder('documentRule')
            ->select('documentRule', 'tenant')
            ->leftJoin('documentRule.tenant', 'tenant')
            ->andWhere('documentRule.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->andWhere('documentRule.state = :state')
            ->setParameter('state', VersionableEntityInterface::S_PUBLISHED)
            ->orderBy('documentRule.version', 'DESC');
    }

    /**
     * @return DocumentRule[]
     */
    public function findByIdentifiersForTenant(Tenant $tenant, array $identifiers): array
    {
        return $this->createQueryBuilder('documentRule')
            ->innerJoin('documentRule.tenant', 'tenant')
            ->andWhere('documentRule.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->andWhere('documentRule.identifier IN (:identifiers)')
            ->setParameter('identifiers', $identifiers)
            ->orderBy('documentRule.version', 'DESC')
            ->addOrderBy('documentRule.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function queryPublished(string $tenantUrl): QueryBuilder
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->select($alias, 'tenant')
            ->leftJoin($alias . '.tenant', 'tenant')
            ->andWhere('tenant.baseurl = :tenant')
            ->andWhere($alias . '.state IN (:states)')
            ->setParameter('tenant', $tenantUrl)
            ->setParameter(
                'states',
                [VersionableEntityInterface::S_PUBLISHED, VersionableEntityInterface::S_DEACTIVATED]
            )
            ->orderBy($alias . '.identifier', 'ASC');
    }

    /**
     * @param Category[]  $categories
     * @param Tenant|null $tenant
     * @return QueryBuilder
     */
    public function queryCategoriesPublic(array $categories, ?Tenant $tenant)
    {
        $alias = $this->getAlias();
        $queryBuilder = $this->createQueryBuilder($alias)
            ->innerJoin($alias . '.tenant', 'tenant')
            ->andWhere($alias . '.state = :state')
            ->andWhere('tenant.active = true')
            ->setParameter('state', VersionableEntityInterface::S_PUBLISHED)
        ;
        $conditions = [];
        foreach ($categories as $key => $category) {
            $conditions[] = sprintf(
                '(cat.root = :root%1$d AND cat.lft BETWEEN :lft%1$d AND :rght%1$d)',
                $key,
            );
            $queryBuilder
                ->setParameter('root' . $key, $category->getRoot())
                ->setParameter('lft' . $key, $category->getLft())
                ->setParameter('rght' . $key, $category->getRght())
            ;
        }
        if ($conditions) {
            $subquery = $this->createQueryBuilder('documentRuleSub')
                ->select('documentRuleSub.id')
                ->innerJoin($alias . '.categories', 'cat')
                ->andWhere('documentRuleSub.id = ' . $alias . '.id')
                ->andWhere(implode(' OR ', $conditions))
            ;
            $queryBuilder->andWhere(sprintf('%s.id IN (%s)', $alias, $subquery->getDQL()));
        }
        if ($tenant) {
            $queryBuilder
                ->andWhere($alias . '.tenant = :tenant')
                ->setParameter('tenant', $tenant)
            ;
        } else {
            $queryBuilder
                ->andWhere($alias . '.public = true')
            ;
        }
        $queryBuilder->addOrderBy($alias . '.name', 'ASC');
        $queryBuilder->addOrderBy($alias . '.id', 'ASC');
        return $queryBuilder;
    }
}
