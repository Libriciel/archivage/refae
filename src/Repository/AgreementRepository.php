<?php

namespace App\Repository;

use App\Entity\Agreement;
use App\Entity\Tenant;
use App\Entity\VersionableEntityInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Override;

/**
 * @extends ServiceEntityRepository<Agreement>
 *
 * @method Agreement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Agreement[]    findAll()
 * @method Agreement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AgreementRepository extends ServiceEntityRepository implements
    ResultSetRepositoryInterface,
    VersionableEntityRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Agreement::class);
    }

    #[Override]
    public function queryOnlyLastVersionForTenant(string $tenantUrl, ?string $identifier = null): QueryBuilder
    {
        $alias = $this->getAlias();
        $queryBuilder = $this->createQueryBuilder($alias);
        $subquery = $this->createQueryBuilder('last')
            ->select('max(last.version)')
            ->innerJoin('last.tenant', 'lastTenant')
            ->andWhere('lastTenant.baseurl = :tenant')
            ->andWhere('last.identifier = ' . $alias . '.identifier')
            ->getQuery();

        if ($identifier) {
            $queryBuilder
                ->andWhere($alias . '.identifier = :identifier')
                ->setParameter('identifier', $identifier)
            ;
        }

        return $queryBuilder
            ->select($alias, 'tenant')
            ->leftJoin($alias . '.tenant', 'tenant')
            ->andWhere('tenant.baseurl = :tenant')
            ->andWhere($queryBuilder->expr()->eq($alias . '.version', '(' . $subquery->getDQL() . ')'))
            ->setParameter('tenant', $tenantUrl)
            ->orderBy($alias . '.identifier', 'ASC')
        ;
    }

    public function findAllVersions(string $identifier, Tenant $tenant)
    {
        return $this->createQueryBuilder('agreement')
            ->select('agreement', 'tenant')
            ->leftJoin('agreement.tenant', 'tenant')
            ->andWhere('agreement.identifier = :identifier')
            ->andWhere('agreement.tenant = :tenant')
            ->setParameter('identifier', $identifier)
            ->setParameter('tenant', $tenant)
            ->orderBy('agreement.version', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findWithPreviousVersions(string $id): ?Agreement
    {
        $queryBuilder = $this->createQueryBuilder('agreement');

        $subqueryIdentifier = $this->createQueryBuilder('i')
            ->select('i.identifier')
            ->where('i.id = :id')
            ->setMaxResults(1);

        $subqueryVersion = $this->createQueryBuilder('v')
            ->select('v.version')
            ->where('v.id = :id')
            ->setMaxResults(1);

        $subqueryTenant = $this->createQueryBuilder('x')
            ->select('tx.id')
            ->leftJoin('x.tenant', 'tx')
            ->where('x.id = :id')
            ->setMaxResults(1);

        $result = $queryBuilder
            ->select('agreement', 'tenant')
            ->leftJoin('agreement.tenant', 'tenant')
            ->where(
                $queryBuilder->expr()->in(
                    'agreement.identifier',
                    $subqueryIdentifier->getDQL()
                )
            )
            ->andWhere(
                $queryBuilder->expr()->lte(
                    'agreement.version',
                    '(' . $subqueryVersion->getDQL() . ')'
                )
            )
            ->andWhere(
                $queryBuilder->expr()->in(
                    'agreement.tenant',
                    $subqueryTenant->getDQL()
                )
            )
            ->setParameter('id', $id)
            ->orderBy('agreement.version', 'DESC')
            ->getQuery()
            ->getResult();

        /** @var Agreement $current */
        $current = array_shift($result);
        $current?->setPreviousVersions($result);

        return $current;
    }

    public function findByIdentifierWithPreviousVersions(string $identifier): ?Agreement
    {
        $queryBuilder = $this->createQueryBuilder('agreement');

        $last = $this->createQueryBuilder('agreement')
            ->where('agreement.identifier = :identifier')
            ->andWhere('agreement.state IN (:states)')
            ->orderBy('agreement.version', 'DESC')
            ->setParameter('identifier', $identifier)
            ->setParameter(
                'states',
                [
                    VersionableEntityInterface::S_PUBLISHED,
                    VersionableEntityInterface::S_DEACTIVATED,
                    VersionableEntityInterface::S_REVOKED
                ]
            )
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        $subqueryIdentifier = $this->createQueryBuilder('i')
            ->select('i.identifier')
            ->where('i.id = :id')
            ->setMaxResults(1);

        $subqueryVersion = $this->createQueryBuilder('v')
            ->select('v.version')
            ->where('v.id = :id')
            ->setMaxResults(1);

        $subqueryTenant = $this->createQueryBuilder('x')
            ->select('tx.id')
            ->leftJoin('x.tenant', 'tx')
            ->where('x.id = :id')
            ->setMaxResults(1);

        $result = $queryBuilder
            ->select('agreement', 'tenant')
            ->leftJoin('agreement.tenant', 'tenant')
            ->where(
                $queryBuilder->expr()->in(
                    'agreement.identifier',
                    $subqueryIdentifier->getDQL()
                )
            )
            ->andWhere(
                $queryBuilder->expr()->lte(
                    'agreement.version',
                    '(' . $subqueryVersion->getDQL() . ')'
                )
            )
            ->andWhere(
                $queryBuilder->expr()->in(
                    'agreement.tenant',
                    $subqueryTenant->getDQL()
                )
            )
            ->setParameter('id', $last->getId())
            ->orderBy('agreement.version', 'DESC')
            ->getQuery()
            ->getResult();

        /** @var Agreement $current */
        $current = array_shift($result);
        $current?->setPreviousVersions($result);

        return $current;
    }

    public function queryForExport(Tenant $tenant): QueryBuilder
    {
        return $this->createQueryBuilder('agreement')
            ->select('agreement', 'tenant')
            ->leftJoin('agreement.tenant', 'tenant')
            ->andWhere('agreement.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->andWhere('agreement.state = :state')
            ->setParameter('state', VersionableEntityInterface::S_PUBLISHED)
            ->orderBy('agreement.version', 'DESC')
        ;
    }

    /**
     * @return Agreement[]
     */
    public function findByIdentifiersForTenant(Tenant $tenant, array $identifiers): array
    {
        return $this->createQueryBuilder('agreement')
            ->innerJoin('agreement.tenant', 'tenant')
            ->andWhere('agreement.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->andWhere('agreement.identifier IN (:identifiers)')
            ->setParameter('identifiers', $identifiers)
            ->orderBy('agreement.version', 'DESC')
            ->addOrderBy('agreement.name', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function queryPublished(string $tenantUrl): QueryBuilder
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->select($alias, 'tenant')
            ->leftJoin($alias . '.tenant', 'tenant')
            ->andWhere('tenant.baseurl = :tenant')
            ->andWhere($alias . '.state IN (:states)')
            ->setParameter('tenant', $tenantUrl)
            ->setParameter(
                'states',
                [VersionableEntityInterface::S_PUBLISHED, VersionableEntityInterface::S_DEACTIVATED]
            )
            ->orderBy($alias . '.identifier', 'ASC');
    }

    #[Override]
    public function findPreviousVersion(VersionableEntityInterface $current): ?VersionableEntityInterface
    {
        if ($current->getVersion() === 1 || !$current instanceof Agreement) {
            return null;
        }

        return $this->createQueryBuilder('agreement')
            ->where('agreement.identifier = :identifier')
            ->setParameter('identifier', $current->getIdentifier())
            ->andWhere('agreement.version = :version')
            ->andWhere('agreement.tenant = :tenant')
            ->setParameter('tenant', $current->getTenant())
            ->setParameter('version', $current->getVersion() - 1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    #[Override]
    public function exportCsv(callable $appendFilters = null, array $options = []): QueryBuilder
    {
        $query = $options['from'] ?? false === 'index_published'
            ? $this->queryPublished($options['tenant_url'])
            : $this->queryOnlyLastVersionForTenant($options['tenant_url']);
        if ($appendFilters) {
            $appendFilters($query);
        }

        return $query;
    }
}
