<?php

namespace App\Repository;

use App\Entity\Role;
use App\Entity\Tenant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Role>
 *
 * @method Role|null find($id, $lockMode = null, $lockVersion = null)
 * @method Role|null findOneBy(array $criteria, array $orderBy = null)
 * @method Role[]    findAll()
 * @method Role[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Role::class);
    }

    public function removeNonExistentRoles(array $roles)
    {
        $this->createQueryBuilder('r')
            ->delete()
            ->andWhere('r.tenant IS NULL')
            ->andWhere('r.name NOT IN (:roles)')
            ->setParameter('roles', $roles)
            ->getQuery()
            ->execute();
    }

    public function findOrCreateIfNotExistsDefault(string $name)
    {
        $exists = $this->createQueryBuilder('p')
            ->select('p.id')
            ->andWhere('p.name = :val')
            ->andWhere('p.tenant IS NULL')
            ->setParameter('val', $name)
            ->getQuery()
            ->getOneOrNullResult()
        ;
        if (!$exists) {
            $role = new Role();
            $role->setName($name);
            $this->_em->persist($role);
            $this->_em->flush();
            return $role;
        }
        return $this->find($exists);
    }

    public function findOneDefault(string $name): ?Role
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.name = :val')
            ->andWhere('p.tenant IS NULL')
            ->setParameter('val', $name)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findByIdAndTenant(string $id, ?Tenant $tenant): ?Role
    {
        $result = $this->createQueryBuilder('r')
            ->andWhere('r.id = :id')
            ->setParameter('id', $id);
        if ($tenant) {
            $result->andWhere('r.tenant = :tenant')
                ->setParameter('ternant', $tenant);
        } else {
            $result->andWhere('r.tenant IS NULL');
        }

        return $result->getQuery()
            ->getOneOrNullResult();
    }

    public function findForTenant(Tenant $tenant): array
    {
        return $this->createQueryBuilder('r')
            ->where('r.tenant is null')
            ->orWhere('r.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->orderBy('r.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findOneByName(string $name): ?Role
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.name = :val')
            ->setParameter('val', $name)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
