<?php

namespace App\Repository;

use App\Entity\Authority;
use App\Entity\AuthorityFile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AuthorityFile>
 *
 * @method AuthorityFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method AuthorityFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method AuthorityFile[]    findAll()
 * @method AuthorityFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthorityFileRepository extends ServiceEntityRepository implements ResultSetRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AuthorityFile::class);
    }

    public function queryByAuthority(Authority $authority): QueryBuilder
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->select($alias, 'authority', 'file', 'tenant')
            ->leftJoin($alias . '.authority', 'authority')
            ->leftJoin('authority.tenant', 'tenant')
            ->leftJoin($alias . '.file', 'file')
            ->where('authority = :authority')
            ->setParameter('authority', $authority)
            ->orderBy('file.name', 'ASC');
    }
}
