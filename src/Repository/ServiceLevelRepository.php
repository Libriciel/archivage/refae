<?php

namespace App\Repository;

use App\Entity\ServiceLevel;
use App\Entity\Tenant;
use App\Entity\VersionableEntityInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Override;

/**
 * @extends ServiceEntityRepository<ServiceLevel>
 *
 * @method ServiceLevel|null find($id, $lockMode = null, $lockVersion = null)
 * @method ServiceLevel|null findOneBy(array $criteria, array $orderBy = null)
 * @method ServiceLevel[]    findAll()
 * @method ServiceLevel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServiceLevelRepository extends ServiceEntityRepository implements
    ResultSetRepositoryInterface,
    VersionableEntityRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ServiceLevel::class);
    }

    #[Override]
    public function findPreviousVersion(VersionableEntityInterface $current): ?VersionableEntityInterface
    {
        if ($current->getVersion() === 1 || !$current instanceof ServiceLevel) {
            return null;
        }

        $alias = $this->getAlias();

        return $this->createQueryBuilder($alias)
            ->where($alias . '.identifier = :identifier')
            ->setParameter('identifier', $current->getIdentifier())
            ->andWhere($alias . '.version = :version')
            ->andWhere($alias . '.tenant = :tenant')
            ->setParameter('tenant', $current->getTenant())
            ->setParameter('version', $current->getVersion() - 1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findWithPreviousVersions(string $id): ?ServiceLevel
    {
        $queryBuilder = $this->createQueryBuilder('serviceLevel');

        $subqueryIdentifier = $this->createQueryBuilder('i')
            ->select('i.identifier')
            ->where('i.id = :id')
            ->setMaxResults(1);

        $subqueryVersion = $this->createQueryBuilder('v')
            ->select('v.version')
            ->where('v.id = :id')
            ->setMaxResults(1);

        $subqueryTenant = $this->createQueryBuilder('x')
            ->select('tx.id')
            ->leftJoin('x.tenant', 'tx')
            ->where('x.id = :id')
            ->setMaxResults(1);

        $result = $queryBuilder
            ->select('serviceLevel', 'tenant')
            ->leftJoin('serviceLevel.tenant', 'tenant')
            ->where(
                $queryBuilder->expr()->in(
                    'serviceLevel.identifier',
                    $subqueryIdentifier->getDQL()
                )
            )
            ->andWhere(
                $queryBuilder->expr()->lte(
                    'serviceLevel.version',
                    '(' . $subqueryVersion->getDQL() . ')'
                )
            )
            ->andWhere(
                $queryBuilder->expr()->in(
                    'serviceLevel.tenant',
                    $subqueryTenant->getDQL()
                )
            )
            ->setParameter('id', $id)
            ->orderBy('serviceLevel.version', 'DESC')
            ->getQuery()
            ->getResult();

        /** @var ServiceLevel $current */
        $current = array_shift($result);
        $current?->setPreviousVersions($result);

        return $current;
    }

    public function findByIdentifierWithPreviousVersions(string $identifier): ?ServiceLevel
    {
        $queryBuilder = $this->createQueryBuilder('serviceLevel');

        $last = $this->createQueryBuilder('serviceLevel')
            ->where('serviceLevel.identifier = :identifier')
            ->andWhere('serviceLevel.state IN (:states)')
            ->orderBy('serviceLevel.version', 'DESC')
            ->setParameter('identifier', $identifier)
            ->setParameter(
                'states',
                [
                    VersionableEntityInterface::S_PUBLISHED,
                    VersionableEntityInterface::S_DEACTIVATED,
                    VersionableEntityInterface::S_REVOKED
                ]
            )
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        $subqueryIdentifier = $this->createQueryBuilder('i')
            ->select('i.identifier')
            ->where('i.id = :id')
            ->setMaxResults(1);

        $subqueryVersion = $this->createQueryBuilder('v')
            ->select('v.version')
            ->where('v.id = :id')
            ->setMaxResults(1);

        $subqueryTenant = $this->createQueryBuilder('x')
            ->select('tx.id')
            ->leftJoin('x.tenant', 'tx')
            ->where('x.id = :id')
            ->setMaxResults(1);

        $result = $queryBuilder
            ->select('serviceLevel', 'tenant')
            ->leftJoin('serviceLevel.tenant', 'tenant')
            ->where(
                $queryBuilder->expr()->in(
                    'serviceLevel.identifier',
                    $subqueryIdentifier->getDQL()
                )
            )
            ->andWhere(
                $queryBuilder->expr()->lte(
                    'serviceLevel.version',
                    '(' . $subqueryVersion->getDQL() . ')'
                )
            )
            ->andWhere(
                $queryBuilder->expr()->in(
                    'serviceLevel.tenant',
                    $subqueryTenant->getDQL()
                )
            )
            ->setParameter('id', $last->getId())
            ->orderBy('serviceLevel.version', 'DESC')
            ->getQuery()
            ->getResult();

        /** @var ServiceLevel $current */
        $current = array_shift($result);
        $current?->setPreviousVersions($result);

        return $current;
    }

    #[Override]
    public function queryOnlyLastVersionForTenant(
        string $tenantUrl,
        ?string $identifier = null
    ): QueryBuilder {
        $alias = $this->getAlias();
        $queryBuilder = $this->createQueryBuilder($alias);
        $subquery = $this->createQueryBuilder('last')
            ->select('max(last.version)')
            ->innerJoin('last.tenant', 'lastTenant')
            ->andWhere('lastTenant.baseurl = :tenant')
            ->andWhere('last.identifier = ' . $alias . '.identifier')
            ->getQuery();

        if ($identifier) {
            $queryBuilder
                ->andWhere($alias . '.identifier = :identifier')
                ->setParameter('identifier', $identifier)
            ;
        }

        return $queryBuilder
            ->select($alias, 'tenant')
            ->leftJoin($alias . '.tenant', 'tenant')
            ->andWhere('tenant.baseurl = :tenant')
            ->andWhere($queryBuilder->expr()->eq($alias . '.version', '(' . $subquery->getDQL() . ')'))
            ->setParameter('tenant', $tenantUrl)
            ->orderBy($alias . '.identifier', 'ASC');
    }

    public function queryForExport(Tenant $tenant): QueryBuilder
    {
        return $this->createQueryBuilder('serviceLevel')
            ->select('serviceLevel', 'tenant')
            ->leftJoin('serviceLevel.tenant', 'tenant')
            ->andWhere('serviceLevel.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->andWhere('serviceLevel.state = :state')
            ->setParameter('state', VersionableEntityInterface::S_PUBLISHED)
            ->orderBy('serviceLevel.version', 'DESC');
    }

    /**
     * @return ServiceLevel[]
     */
    public function findByIdentifiersForTenant(Tenant $tenant, array $identifiers): array
    {
        return $this->createQueryBuilder('serviceLevel')
            ->innerJoin('serviceLevel.tenant', 'tenant')
            ->andWhere('serviceLevel.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->andWhere('serviceLevel.identifier IN (:identifiers)')
            ->setParameter('identifiers', $identifiers)
            ->orderBy('serviceLevel.version', 'DESC')
            ->addOrderBy('serviceLevel.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function queryPublished(string $tenantUrl): QueryBuilder
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->select($alias, 'tenant')
            ->leftJoin($alias . '.tenant', 'tenant')
            ->andWhere('tenant.baseurl = :tenant')
            ->andWhere($alias . '.state IN (:states)')
            ->setParameter('tenant', $tenantUrl)
            ->setParameter(
                'states',
                [VersionableEntityInterface::S_PUBLISHED, VersionableEntityInterface::S_DEACTIVATED]
            )
            ->orderBy($alias . '.identifier', 'ASC');
    }
}
