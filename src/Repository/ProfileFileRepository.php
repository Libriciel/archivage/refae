<?php

namespace App\Repository;

use App\Entity\ProfileFile;
use App\Entity\Profile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ProfileFile>
 *
 * @method ProfileFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProfileFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProfileFile[]    findAll()
 * @method ProfileFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProfileFileRepository extends ServiceEntityRepository implements ResultSetRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProfileFile::class);
    }

    public function queryByProfile(Profile $profile): QueryBuilder
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->select($alias, 'profile', 'file', 'tenant')
            ->leftJoin($alias . '.profile', 'profile')
            ->leftJoin('profile.tenant', 'tenant')
            ->leftJoin($alias . '.file', 'file')
            ->where('profile = :profile')
            ->setParameter('profile', $profile)
            ->orderBy('file.name', 'ASC');
    }
}
