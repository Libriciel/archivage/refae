<?php

namespace App\Repository;

use App\Entity\PrivilegeUser;
use App\Entity\Tenant;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PrivilegeUser>
 *
 * @method PrivilegeUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method PrivilegeUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method PrivilegeUser[]    findAll()
 * @method PrivilegeUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PrivilegeUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PrivilegeUser::class);
    }

    public function findForCache(
        ?Tenant $tenant,
        User $user
    ): array {
        return $this->createQueryBuilder('pu')
            ->select('pu', 'p')
            ->innerJoin('pu.privilege', 'p')
            ->andWhere('pu.tenant = :tenant')
            ->andWhere('pu.user = :user')
            ->setParameter('tenant', $tenant)
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult()
        ;
    }
}
