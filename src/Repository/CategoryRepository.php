<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Tenant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\QueryBuilder;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

/**
 * @extends ServiceEntityRepository<Category>
 *
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends NestedTreeRepository implements ResultSetRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
    ) {
        parent::__construct($entityManager, new ClassMetadata(Category::class));
    }

    public function queryOptions(string $tenantUrl): QueryBuilder
    {
        return $this->createQueryBuilder('c')
            ->addSelect('CASE WHEN p.id IS NULL THEN 0 ELSE 1 END AS HIDDEN noGroup')
            ->leftJoin('c.parent', 'p')
            ->innerJoin('c.tenant', 't')
            ->where('t.baseurl = :baseurl')
            ->setParameter('baseurl', $tenantUrl)
            ->orderBy('noGroup, p.name, c.name')
        ;
    }

    public function findOptions(string $tenantUrl, ?Category $category = null): array
    {
        $request = $this->queryOptions($tenantUrl);

        if ($category) {
            $request
                ->andWhere('c.lft < :lft OR c.rght > :rght')
                ->setParameter('lft', $category->getLft())
                ->setParameter('rght', $category->getRght());
        }

        $categories = $request->getQuery()->getResult();
        array_map(
            fn (Category $c) => $c->setPath($this->getPathAsString($c)),
            $categories,
        );
        usort(
            $categories,
            fn(Category $a, Category $b) => $a->getPath() <=> $b->getPath()
        );
        return $categories;
    }

    public function queryForExport(Tenant $tenant): QueryBuilder
    {
        return $this->createQueryBuilder('category')
            ->select('category', 'tenant')
            ->leftJoin('category.tenant', 'tenant')
            ->andWhere('category.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->orderBy('category.lft, category.name')
        ;
    }

    public function queryForTenant(string $tenantUrl): QueryBuilder
    {
        return $this->createQueryBuilder('c')
            ->addSelect('c, p, t')
            ->leftJoin('c.parent', 'p')
            ->innerJoin('c.tenant', 't')
            ->where('t.baseurl = :baseurl')
            ->setParameter('baseurl', $tenantUrl)
            ->orderBy('c.name')
        ;
    }

    /**
     * @return Category[]
     */
    public function findByNameForTenant(Tenant $tenant, array $names): array
    {
        return $this->createQueryBuilder('c')
            ->innerJoin('c.tenant', 'tenant')
            ->andWhere('c.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->andWhere('c.name IN (:names)')
            ->setParameter('names', $names)
            ->addOrderBy('c.name', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByPath(array $names, Tenant $tenant = null): array
    {
        $queryBuilder = $this->createQueryBuilder('final');
        $finalName = array_pop($names);
        if ($finalName) {
            $queryBuilder
                ->andWhere('final.name = :finalName')
                ->setParameter('finalName', $finalName)
            ;
        }
        $names = array_reverse($names);
        $prev = 'final';
        foreach ($names as $key => $subName) {
            $alias = 'parent' . $key;
            $queryBuilder
                ->innerJoin($prev . '.parent', $alias)
                ->andWhere($alias . '.name = :' . $alias . 'name')
                ->setParameter($alias . 'name', $subName)
            ;
            $prev = $alias;
        }
        if ($tenant) {
            $queryBuilder
                ->andWhere('final.tenant = :tenant')
                ->setParameter('tenant', $tenant)
            ;
        }

        $results = $queryBuilder
            ->getQuery()
            ->getResult()
        ;
        foreach ($results as $category) {
            $ref = $category;
            $path = [$category->getName()];
            while ($ref = $ref->getParent()) {
                array_unshift($path, $ref->getName());
            }
            $category->setPath(implode(' ⮕ ', $path));
        }
        return $results;
    }
}
