<?php

namespace App\Repository;

use App\Entity\TableFilters;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TableFilters>
 *
 * @method TableFilters|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableFilters|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableFilters[]    findAll()
 * @method TableFilters[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableFiltersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableFilters::class);
    }
}
