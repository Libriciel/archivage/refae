<?php

namespace App\Repository;

use App\Entity\AccessUser;
use App\Entity\Ldap;
use App\Entity\Tenant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AccessUser>
 *
 * @method AccessUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccessUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccessUser[]    findAll()
 * @method AccessUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccessUserRepository extends ServiceEntityRepository implements ResultSetRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccessUser::class);
    }

    public function findOneByUsernameTenant(string $username, string $baseurl): ?AccessUser
    {
        return $this->createQueryBuilder('au')
            ->select('au', 'u', 't', 'l', 'o')
            ->innerJoin('au.user', 'u')
            ->innerJoin('au.tenant', 't')
            ->leftJoin('au.ldap', 'l')
            ->leftJoin('au.openid', 'o')
            ->andWhere('u.username = :username')
            ->setParameter('username', $username)
            ->andWhere('t.baseurl = :baseurl')
            ->setParameter('baseurl', $baseurl)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function getAccessesForLdap(Ldap $ldap, ?Tenant $tenant): array
    {
        $query = $this->createQueryBuilder('au')
            ->select('au')
            ->where('au.ldap = :ldap')
            ->setParameter('ldap', $ldap);

        if ($tenant) {
            $query->andWhere('au.tenant = :tenant')
                ->setParameter('tenant', $tenant);
        } else {
            $query->andWhere('au.tenant IS NULL');
        }

        return $query
            ->getQuery()
            ->getResult();
    }
}
