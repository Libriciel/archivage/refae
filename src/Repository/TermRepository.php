<?php

namespace App\Repository;

use App\Entity\Tenant;
use App\Entity\Term;
use App\Entity\Vocabulary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Term>
 *
 * @method Term|null find($id, $lockMode = null, $lockVersion = null)
 * @method Term|null findOneBy(array $criteria, array $orderBy = null)
 * @method Term[]    findAll()
 * @method Term[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TermRepository extends ServiceEntityRepository implements ResultSetRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Term::class);
    }

    public function queryTermsForVocabulary(Vocabulary $vocabulary): QueryBuilder
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->select($alias, 'vocabulary', 'tenant')
            ->leftJoin($alias . '.vocabulary', 'vocabulary')
            ->leftJoin('vocabulary.tenant', 'tenant')
            ->where('vocabulary = :vocabulary')
            ->setParameter('vocabulary', $vocabulary)
            ->orderBy($alias . '.name', 'ASC');
    }

    public function queryPublicForVocabulary(Vocabulary $vocabulary, ?Tenant $tenant): QueryBuilder
    {
        $alias = $this->getAlias();
        $queryBuilder = $this->createQueryBuilder($alias)
            ->select($alias, 'vocabulary', 'tenant')
            ->leftJoin($alias . '.vocabulary', 'vocabulary')
            ->leftJoin('vocabulary.tenant', 'tenant')
            ->where('vocabulary = :vocabulary')
            ->setParameter('vocabulary', $vocabulary);

        if ($tenant) {
            $queryBuilder
                ->andWhere('vocabulary.tenant = :tenant')
                ->setParameter('tenant', $tenant);
        } else { // pour les reader, données publiques et non publiques
            $queryBuilder
                ->andWhere('vocabulary.public = true');
        }

        return $queryBuilder
            ->orderBy($alias . '.name', 'ASC');
    }
}
