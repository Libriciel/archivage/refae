<?php

namespace App\Repository;

use App\Entity\Profile;
use App\Entity\Tenant;
use App\Entity\VersionableEntityInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Override;

/**
 * @extends ServiceEntityRepository<Profile>
 *
 * @method Profile|null findOneBy(array $criteria, array $orderBy = null)
 * @method Profile[]    findAll()
 * @method Profile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProfileRepository extends ServiceEntityRepository implements
    ResultSetRepositoryInterface,
    VersionableEntityRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Profile::class);
    }

    /**
     * @return Profile[]
     */
    public function findByIdentifiersForTenant(Tenant $tenant, array $identifiers): array
    {
        return $this->createQueryBuilder('profile')
            ->innerJoin('profile.tenant', 'tenant')
            ->andWhere('profile.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->andWhere('profile.identifier IN (:identifiers)')
            ->setParameter('identifiers', $identifiers)
            ->orderBy('profile.version', 'DESC')
            ->addOrderBy('profile.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findWithPreviousVersions(string $id): ?Profile
    {
        $queryBuilder = $this->createQueryBuilder('profile');

        $subqueryIdentifier = $this->createQueryBuilder('i')
            ->select('i.identifier')
            ->where('i.id = :id')
            ->setMaxResults(1);

        $subqueryVersion = $this->createQueryBuilder('v')
            ->select('v.version')
            ->where('v.id = :id')
            ->setMaxResults(1);

        $subqueryTenant = $this->createQueryBuilder('x')
            ->select('tx.id')
            ->leftJoin('x.tenant', 'tx')
            ->where('x.id = :id')
            ->setMaxResults(1);

        $result = $queryBuilder
            ->select('profile', 'tenant')
            ->leftJoin('profile.tenant', 'tenant')
            ->where(
                $queryBuilder->expr()->in(
                    'profile.identifier',
                    $subqueryIdentifier->getDQL()
                )
            )
            ->andWhere(
                $queryBuilder->expr()->lte(
                    'profile.version',
                    '(' . $subqueryVersion->getDQL() . ')'
                )
            )
            ->andWhere(
                $queryBuilder->expr()->in(
                    'profile.tenant',
                    $subqueryTenant->getDQL()
                )
            )
            ->setParameter('id', $id)
            ->orderBy('profile.version', 'DESC')
            ->getQuery()
            ->getResult();

        /** @var Profile $current */
        $current = array_shift($result);
        $current?->setPreviousVersions($result);

        return $current;
    }

    public function findByIdentifierWithPreviousVersions(string $identifier): ?Profile
    {
        $queryBuilder = $this->createQueryBuilder('profile');

        $last = $this->createQueryBuilder('profile')
            ->where('profile.identifier = :identifier')
            ->andWhere('profile.state IN (:states)')
            ->orderBy('profile.version', 'DESC')
            ->setParameter('identifier', $identifier)
            ->setParameter(
                'states',
                [
                    VersionableEntityInterface::S_PUBLISHED,
                    VersionableEntityInterface::S_DEACTIVATED,
                    VersionableEntityInterface::S_REVOKED
                ]
            )
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        $subqueryIdentifier = $this->createQueryBuilder('i')
            ->select('i.identifier')
            ->where('i.id = :id')
            ->setMaxResults(1);

        $subqueryVersion = $this->createQueryBuilder('v')
            ->select('v.version')
            ->where('v.id = :id')
            ->setMaxResults(1);

        $subqueryTenant = $this->createQueryBuilder('x')
            ->select('tx.id')
            ->leftJoin('x.tenant', 'tx')
            ->where('x.id = :id')
            ->setMaxResults(1);

        $result = $queryBuilder
            ->select('profile', 'tenant')
            ->leftJoin('profile.tenant', 'tenant')
            ->where(
                $queryBuilder->expr()->in(
                    'profile.identifier',
                    $subqueryIdentifier->getDQL()
                )
            )
            ->andWhere(
                $queryBuilder->expr()->lte(
                    'profile.version',
                    '(' . $subqueryVersion->getDQL() . ')'
                )
            )
            ->andWhere(
                $queryBuilder->expr()->in(
                    'profile.tenant',
                    $subqueryTenant->getDQL()
                )
            )
            ->setParameter('id', $last->getId())
            ->orderBy('profile.version', 'DESC')
            ->getQuery()
            ->getResult();

        /** @var Profile $current */
        $current = array_shift($result);
        $current?->setPreviousVersions($result);

        return $current;
    }

    #[Override]
    public function queryOnlyLastVersionForTenant(string $tenantUrl, ?string $identifier = null): QueryBuilder
    {
        $alias = $this->getAlias();
        $queryBuilder = $this->createQueryBuilder($alias);
        $subquery = $this->createQueryBuilder('last')
            ->select('max(last.version)')
            ->innerJoin('last.tenant', 'lastTenant')
            ->andWhere('lastTenant.baseurl = :tenant')
            ->andWhere('last.identifier = ' . $alias . '.identifier')
            ->getQuery();

        if ($identifier) {
            $queryBuilder
                ->andWhere($alias . '.identifier = :identifier')
                ->setParameter('identifier', $identifier)
            ;
        }

        return $queryBuilder
            ->select($alias, 'tenant')
            ->leftJoin($alias . '.tenant', 'tenant')
            ->andWhere('tenant.baseurl = :tenant')
            ->andWhere(
                $queryBuilder->expr()->eq(
                    $alias . '.version',
                    '(' . $subquery->getDQL() . ')'
                )
            )
            ->setParameter('tenant', $tenantUrl)
            ->orderBy($alias . '.identifier', 'ASC');
    }

    public function queryPublished(string $tenantUrl, bool $withDeactivated = true): QueryBuilder
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->select($alias, 'tenant')
            ->leftJoin($alias . '.tenant', 'tenant')
            ->andWhere('tenant.baseurl = :tenant')
            ->andWhere($alias . '.state IN (:states)')
            ->setParameter('tenant', $tenantUrl)
            ->setParameter(
                'states',
                $withDeactivated
                    ? [VersionableEntityInterface::S_PUBLISHED, VersionableEntityInterface::S_DEACTIVATED]
                    : [VersionableEntityInterface::S_PUBLISHED]
            )
            ->orderBy($alias . '.identifier', 'ASC');
    }

    #[Override]
    public function findPreviousVersion(VersionableEntityInterface $current): ?Profile
    {
        if ($current->getVersion() === 1) {
            return null;
        }

        return $this->createQueryBuilder('profile')
            ->where('profile.identifier = :identifier')
            ->setParameter('identifier', $current->getIdentifier())
            ->andWhere('profile.version = :version')
            ->andWhere('profile.tenant = :tenant')
            ->setParameter('tenant', $current->getTenant())
            ->setParameter('version', $current->getVersion() - 1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function queryForExport(Tenant $tenant): QueryBuilder
    {
        return $this->createQueryBuilder('profile')
            ->select('profile', 'tenant')
            ->leftJoin('profile.tenant', 'tenant')
            ->andWhere('profile.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->andWhere('profile.state = :state')
            ->setParameter('state', VersionableEntityInterface::S_PUBLISHED)
            ->orderBy('profile.version', 'DESC');
    }

    #[Override]
    public function exportCsv(callable $appendFilters = null, array $options = []): QueryBuilder
    {
        $query = $options['from'] ?? false === 'index_published'
            ? $this->queryPublished($options['tenant_url'])
            : $this->queryOnlyLastVersionForTenant($options['tenant_url']);
        if ($appendFilters) {
            $appendFilters($query);
        }

        return $query;
    }

    public function queryOptions(string $tenantUrl): QueryBuilder
    {
        return $this->createQueryBuilder('profile')
            ->leftJoin('profile.tenant', 'tenant')
            ->where('profile.state = :state')
            ->andWhere('tenant.baseurl = :baseurl')
            ->setParameter('state', VersionableEntityInterface::S_PUBLISHED)
            ->setParameter('baseurl', $tenantUrl);
    }

    public function findOneByTenantIdentifier(?Tenant $tenant, string $identifier): ?Profile
    {
        $profile = $this->createQueryBuilder('profile')
            ->innerJoin('profile.tenant', 'tenant')
            ->andWhere('profile.tenant = :tenant')
            ->setParameter('tenant', $tenant)
            ->andWhere('profile.state = :state')
            ->setParameter('state', VersionableEntityInterface::S_PUBLISHED)
            ->andWhere('profile.identifier = :identifier')
            ->setParameter('identifier', $identifier)
            ->orderBy('profile.version', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
        ;
        return $profile ? $profile[0] : null;
    }
}
