<?php

namespace App\Repository;

use App\Entity\DocumentRuleFile;
use App\Entity\DocumentRule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DocumentRuleFile>
 *
 * @method DocumentRuleFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method DocumentRuleFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method DocumentRuleFile[]    findAll()
 * @method DocumentRuleFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DocumentRuleFileRepository extends ServiceEntityRepository implements ResultSetRepositoryInterface
{
    use ResultSetRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DocumentRuleFile::class);
    }

    public function queryByDocumentRule(DocumentRule $documentRule): QueryBuilder
    {
        $alias = $this->getAlias();
        return $this->createQueryBuilder($alias)
            ->select($alias, 'documentRule', 'file', 'tenant')
            ->leftJoin($alias . '.documentRule', 'documentRule')
            ->leftJoin('documentRule.tenant', 'tenant')
            ->leftJoin($alias . '.file', 'file')
            ->where('documentRule = :documentRule')
            ->setParameter('documentRule', $documentRule)
            ->orderBy('file.name', 'ASC');
    }
}
