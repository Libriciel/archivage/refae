<?php

namespace App\Repository;

use App\Entity\UserPasswordRenewToken;
use DateInterval;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UserPasswordRenewToken>
 *
 * @method UserPasswordRenewToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserPasswordRenewToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserPasswordRenewToken[]    findAll()
 * @method UserPasswordRenewToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserPasswordRenewTokenRepository extends ServiceEntityRepository
{
    public const string TOKEN_EXPIRE = 'PT1H';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserPasswordRenewToken::class);
    }

    public function findBytoken(string $token): ?UserPasswordRenewToken
    {
        return $this->createQueryBuilder('uprt')
            ->select('uprt', 'user')
            ->innerJoin('uprt.user', 'user')
            ->where('uprt.token = :token')
            ->setParameter('token', $token)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function deleteExpired(): void
    {
        $limitDate = (new DateTime())->sub(new DateInterval(self::TOKEN_EXPIRE));
        $this->createQueryBuilder('uprt')
            ->delete()
            ->where('uprt.created < :limit')
            ->setParameter('limit', $limitDate)
            ->getQuery()
            ->execute()
        ;
    }
}
