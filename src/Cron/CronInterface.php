<?php

namespace App\Cron;

use App\Entity\CronExecution;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

interface CronInterface
{
    public function work(InputInterface $input, OutputInterface $output, CronExecution $cronExecution): void;
}
