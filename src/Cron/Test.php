<?php

namespace App\Cron;

use App\Entity\CronExecution;
use Doctrine\ORM\EntityManagerInterface;
use Override;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Test implements CronInterface
{
    private OutputInterface $output;
    private CronExecution $cronExecution;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    #[Override]
    public function work(InputInterface $input, OutputInterface $output, CronExecution $cronExecution): void
    {
        $this->output = $output;
        $this->cronExecution = $cronExecution;
        $this->out('test lancé');
        sleep(2);
        $this->out('test mis à jour');
        sleep(2);
        $this->out('test terminé');
    }

    private function out(string $text): void
    {
        $report = $this->cronExecution->getReport();
        $report = $report ? $report . "\n" : '';
        $text = date('Y-m-d H:i:s - ') . $text;
        $this->cronExecution->setReport($report . $text);
        $this->entityManager->persist($this->cronExecution);
        $this->entityManager->flush();
        $this->output->writeln($text);
    }
}
