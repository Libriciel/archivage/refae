<?php

namespace App\EventSubscriber;

use App\Service\LoggedUser;
use Override;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Twig;

readonly class AdditionnalTwigSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private RouterInterface $router,
        private Twig\Environment $twig,
        private LoggedUser $loggedUser,
    ) {
    }

    /** @noinspection PhpUnusedParameterInspection */
    public function onKernelRequest(RequestEvent $event): void
    {
        if (!$this->loggedUser->user()) {
            $loginUrl = $this->router->generate('public_login');
        } elseif ($this->loggedUser->tenant()) {
            $loginUrl = $this->router->generate(
                'app_tenant_login_username',
                [
                    'tenant_url' => $this->loggedUser->tenant()->getBaseurl(),
                    'username' => $this->loggedUser->user()->getUsername(),
                ],
            );
        } else {
            $loginUrl = $this->router->generate(
                'public_login_username',
                ['username' => $this->loggedUser->user()->getUsername()],
            );
        }
        $accessUser = $this->loggedUser->user()?->getAccessUser();
        if ($accessUser && ($openid = $accessUser->getOpenid())) {
            if ($accessUser->getTenant()) {
                $loginUrl = $this->router->generate(
                    'app_tenant_openid',
                    [
                        'tenant_url' => $accessUser->getTenant()->getBaseurl(),
                        'identifier' => $openid->getIdentifier(),
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );
            } else {
                $loginUrl = $this->router->generate(
                    'public_openid',
                    ['identifier' => $openid->getIdentifier()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );
            }
            $openid->setLoginUrl($loginUrl);
        }
        $this->twig->addGlobal('login_url', $loginUrl);
    }

    #[Override]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => 'onKernelRequest',
        ];
    }
}
