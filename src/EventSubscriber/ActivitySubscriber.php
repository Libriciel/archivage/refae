<?php

namespace App\EventSubscriber;

use App\Entity\Activity;
use App\Entity\ActivityBypassAddEntityInterface;
use App\Entity\ActivitySubjectEntityInterface;
use App\Entity\User;
use App\Entity\Attribute;
use App\Service\AttributeExtractor;
use App\Service\Manager\ActivityManager;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\PreRemoveEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use ReflectionAttribute;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;

#[AsDoctrineListener(event: Events::prePersist)]
#[AsDoctrineListener(event: Events::preUpdate)]
#[AsDoctrineListener(event: Events::postFlush)]
#[AsDoctrineListener(event: Events::preRemove)]
class ActivitySubscriber
{
    private bool $needFlush = false;

    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly Security $security,
        private readonly ActivityManager $activityManager,
    ) {
    }

    public function prePersist(PrePersistEventArgs $event): void
    {
        $request = $this->requestStack->getCurrentRequest();
        $object = $event->getObject();
        $user = $this->security->getUser();
        if ($object instanceof ActivityBypassAddEntityInterface && $object->activityBypassAdd()) {
            return;
        }
        if ($object instanceof ActivitySubjectEntityInterface && $object->getActivities()->count() === 0) {
            $activity = $this->activityManager->newActivity(
                entity: $object,
                action: 'create',
                user: ($user instanceof User ? $user : $request?->getClientIp()) ?: "système",
            );
            $event->getObjectManager()->persist($activity);
        }
    }

    public function postFlush(PostFlushEventArgs $event): void
    {
        if ($this->needFlush) {
            $this->needFlush = false;
            $event->getObjectManager()->flush();
        }
    }

    public function preUpdate(PreUpdateEventArgs $event): void
    {
        $request = $this->requestStack->getCurrentRequest();
        $object = $event->getObject();
        $user = $this->security->getUser();

        if (!$object instanceof ActivitySubjectEntityInterface) {
            return;
        }

        $ignoreFields = $this->getIgnoreFields($object);
        foreach ($event->getEntityChangeSet() as $field => $values) {
            if ($field === 'id') {
                continue;
            }
            if (isset($ignoreFields[$field]) && $ignoreFields[$field] === Attribute\IgnoreActivity::ALL) {
                continue;
            }
            $prevValue = current($values);
            $newValue = end($values);
            if ($prevValue === $newValue) {
                continue;
            }
            if ($prevValue === true) {
                $prevValue = "Oui";
            } elseif ($prevValue === false) {
                $prevValue = "Non";
            }
            if ($newValue === true) {
                $newValue = "Oui";
            } elseif ($newValue === false) {
                $newValue = "Non";
            }
            if (is_object($prevValue)) {
                if (method_exists($prevValue, 'getName')) {
                    $prevValue = $prevValue->getName();
                } elseif (method_exists($prevValue, 'getId')) {
                    $prevValue = $prevValue->getId();
                } else {
                    continue;
                }
            }
            if (is_object($newValue)) {
                if (method_exists($newValue, 'getName')) {
                    $newValue = $newValue->getName();
                } elseif (method_exists($newValue, 'getId')) {
                    $newValue = $newValue->getId();
                } else {
                    continue;
                }
            }

            if (isset($ignoreFields[$field]) && $ignoreFields[$field] === Attribute\IgnoreActivity::VALUE) {
                $prevValue = $newValue = '*****';
            }

            $field = $this->fieldnameToLabel($object, $field);
            if (is_string($prevValue) && is_string($newValue)) {
                $action = 'edit';
                $subject = sprintf(
                    '"%s" ("%s" -> "%s")',
                    $field,
                    $prevValue,
                    $newValue,
                );
            } elseif (is_string($prevValue)) {
                $action = 'delete';
                $subject = sprintf(
                    '"%s" ("%s")',
                    $field,
                    $prevValue,
                );
            } elseif (is_string($newValue)) {
                $action = 'add';
                $subject = sprintf(
                    '"%s" ("%s")',
                    $field,
                    $newValue,
                );
            } else {
                continue;
            }

            $activity = $this->activityManager->newActivity(
                entity: $object,
                action: $action,
                user: ($user instanceof User ? $user : $request?->getClientIp()) ?: "système",
                subject: $subject,
            );
            $event->getObjectManager()->persist($activity);
            $this->needFlush = true;
        }
    }

    public function preRemove(PreRemoveEventArgs $event): void
    {
        $object = $event->getObject();
        $objectManager = $event->getObjectManager();
        if (!$object instanceof User) {
            return;
        }

        $activities = $objectManager->getRepository(Activity::class)
            ->findBy(['user' => $object]);

        /** @var Activity $activity */
        foreach ($activities as $activity) {
            $activity
                ->setUser(null)
                ->setCreatedUser(null)
                ->setUserDeletedName($object->getCommonName())
            ;
            $objectManager->persist($activity);
        }
    }

    private function fieldnameToLabel(object $object, string $fieldname): string
    {
        $extractor = new AttributeExtractor($object);
        $reflection = new ReflectionClass($object);
        foreach ($reflection->getProperties() as $property) {
            if ($property->getName() !== $fieldname) {
                continue;
            }
            $resultsetAttr = $extractor->getAttribute($property, Attribute\ResultSet::class);
            $activityAttr = $extractor->getAttribute($property, Attribute\Activity::class);
            if ($resultsetAttr && $activityAttr) {
                $fieldname = $extractor->extractNormalisedAttributeArgs($resultsetAttr)['translation'];
            } elseif ($activityAttr) {
                $fieldname = $extractor->extractNormalisedAttributeArgs($activityAttr)['label'];
            }
            break;
        }
        return $fieldname;
    }

    /**
     * @return array : ['fieldname' => 'ignoreType']
     * @throws ReflectionException
     */
    private function getIgnoreFields(ActivitySubjectEntityInterface $object): array
    {
        $fields = [];
        $extractor = new AttributeExtractor($object);
        $extractor->getPropertiesHaving(
            Attribute\IgnoreActivity::class,
            function (ReflectionProperty $property, ReflectionAttribute $attribute) use ($extractor, &$fields) {
                $fields[$property->getName()] = $extractor->extractNormalisedAttributeArgs($attribute)['type'];
            }
        );
        return $fields;
    }
}
