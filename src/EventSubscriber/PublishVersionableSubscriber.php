<?php

namespace App\EventSubscriber;

use App\Entity\VersionableEntityInterface;
use App\Repository\VersionableEntityRepositoryInterface;
use App\Service\Manager\WorkflowManager;
use App\Service\WorkflowInterfaceGetter;
use Doctrine\ORM\EntityManagerInterface;
use Override;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Event\CompletedEvent;

readonly class PublishVersionableSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private EntityManagerInterface $manager,
        private WorkflowInterfaceGetter $getWorkflowManager,
    ) {
    }

    public function onPublishTransition(CompletedEvent $event): void
    {
        $current = $event->getSubject();
        /** @var VersionableEntityRepositoryInterface $repo */
        $repo = $this->manager->getRepository($current::class);
        $stateMachine = $this->getWorkflowManager->getWorkflowInterfaceFromEntity($current);

        $prev = $repo->findPreviousVersion($current);
        if ($prev) {
            $stateMachine->apply($prev, VersionableEntityInterface::T_DEPRECIATE);
        }
    }

    #[Override]
    public static function getSubscribedEvents(): array
    {
        return array_reduce(
            WorkflowManager::getVersionableEntityClasses(),
            function (array $carry, string $item): array {
                $path = explode('\\', $item);
                $alias = strtolower(end($path));
                $carry[sprintf(
                    'workflow.%s.completed.publish',
                    $alias
                )] = 'onPublishTransition';
                return $carry;
            },
            []
        );
    }
}
