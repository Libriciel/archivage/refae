<?php

namespace App\EventSubscriber;

use App\ResultSet\InvalidPageException;
use Override;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;

readonly class ResultSetSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private RouterInterface $router,
    ) {
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        // redirection sur la page 1 si la page demandé est plus grande que la limite de pages
        while ($exception && !$exception instanceof InvalidPageException) {
            $exception = $exception->getPrevious();
        }
        if ($exception instanceof InvalidPageException) {
            $request = $event->getRequest();
            $currentRoute = $request->get('_route');

            $targetUrl = $this->router->generate(
                $currentRoute,
                ['page' => 1] + $request->attributes->get('_route_params')
            );
            $filters = $request->getQueryString();
            if ($filters) {
                $targetUrl = sprintf('%s?%s', $targetUrl, $filters);
            }
            $response = new RedirectResponse($targetUrl);
            $event->setResponse($response);
        }
    }

    #[Override]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => ['onKernelException', 1],
        ];
    }
}
