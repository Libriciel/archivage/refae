<?php

namespace App\EventSubscriber;

use App\Entity\Category;
use App\Entity\Tenant;
use App\Repository\CategoryRepository;
use App\Repository\TenantRepository;
use Override;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;
use Twig;

class SideMenuSubscriber implements EventSubscriberInterface
{
    private ?Tenant $tenant = null;
    private array $activeDocumentRuleUrls = [];

    public function __construct(
        private readonly Twig\Environment $twig,
        private readonly RequestStack $requestStack,
        private readonly TenantRepository $tenantRepository,
        private readonly CategoryRepository $categoryRepository,
        private readonly RouterInterface $router,
    ) {
    }

    public function onKernelRequest(): void
    {
        $this->twig->addGlobal('side_menu', $this->getSideMenu());
    }

    #[Override]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => 'onKernelRequest',
        ];
    }

    private function arrayMergeRecursive($array1, $array2): array
    {
        $newArray = $array1;
        foreach ($array2 as $key => $value) {
            if (!isset($newArray[$key])) {
                $newArray[$key] = $value;
            } elseif (is_array($value) && is_array($newArray[$key])) {
                $newArray[$key] = $this->arrayMergeRecursive($newArray[$key], $value);
            }
        }
        return $newArray;
    }

    private function getSideMenu()
    {
        $request = $this->requestStack->getCurrentRequest();
        if (str_starts_with((string) $request->get('_route'), 'admin_')) {
            return [];
        }
        $tenantUrl = $request->get('tenant_url');
        $this->tenant = $tenantUrl ? $this->tenantRepository->findOneByBaseurl($tenantUrl) : null;

        // spécial liens actifs du tableau de gestion
        $documentRuleRoutes = ['public', 'public_home', 'public_document_rule', 'app_reader_document_rule'];
        if (in_array($request->get('_route'), $documentRuleRoutes)) {
            $names = explode('/', trim((string) ($request->get('names') ?: ''), '/'));
            $currentPath = [];
            foreach ($names as $name) {
                $currentPath[] = $name;
                if ($this->tenant) {
                    $params = [
                        'names' => implode('/', $currentPath),
                        'tenant_url' => $this->tenant->getBaseurl()
                    ];
                } else {
                    $params = ['names' => implode('/', $currentPath)];
                }
                $this->activeDocumentRuleUrls[] = $this->router->generate($request->get('_route'), $params);
            }
        }

        if ($this->tenant) {
            $categories = $this->getCategories($this->tenant);
        } else {
            $tenants = $this->tenantRepository->findBy(['active' => true]);
            $categories = [];
            foreach ($tenants as $tenant) {
                $c = $this->getCategories($tenant);
                if ($c) {
                    $categories = $this->arrayMergeRecursive($c, $categories);
                }
            }
        }
        return $categories;
    }

    private function getCategories(Tenant $tenant): array
    {
        $categories = [];
        $query = $this->categoryRepository
            ->findBy(['parent' => null, 'tenant' => $tenant], ['name' => 'ASC']);
        foreach ($query as $category) {
            if ($this->tenant) {
                $params = [
                    'names' => $category->getName(),
                    'tenant_url' => $this->tenant->getBaseurl()
                ];
                $url = $this->router->generate('app_reader_document_rule', $params);
            } else {
                $params = ['names' => $category->getName()];
                $url = $this->router->generate('public_document_rule', $params);
            }
            $menu = ['_url' => $url, '_active' => in_array($url, $this->activeDocumentRuleUrls)];
            foreach ($category->getChildren() as $child) {
                $menu[$child->getName()] = $this->appendChild($child, [$category->getName()]);
            }
            $categories[$category->getName()] = $menu;
        }
        return $categories;
    }

    private function appendChild(Category $child, array $names)
    {
        $names[] = $child->getName();

        if ($this->tenant) {
            $params = [
                'names' => implode('/', $names),
                'tenant_url' => $this->tenant->getBaseurl()
            ];
            $url = $this->router->generate('app_reader_document_rule', $params);
        } else {
            $params = ['names' => implode('/', $names)];
            $url = $this->router->generate('public_document_rule', $params);
        }
        $menu = ['_url' => $url, '_active' => in_array($url, $this->activeDocumentRuleUrls)];
        foreach ($child->getChildren() as $subChild) {
            $menu[$subChild->getName()] = $this->appendChild($subChild, $names);
        }
        return $menu;
    }
}
