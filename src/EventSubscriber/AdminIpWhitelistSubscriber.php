<?php

namespace App\EventSubscriber;

use Override;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\IpUtils;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

readonly class AdminIpWhitelistSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private bool $isWhitelistEnabled,
        private array $ipWhitelist,
    ) {
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();
        $currentRoute = $request->get('_route');
        if (
            $currentRoute
            && str_starts_with((string) $currentRoute, 'admin_')
            && $this->isWhitelistEnabled
            && !IpUtils::checkIp($request->getClientIp(), $this->ipWhitelist)
        ) {
            throw new AccessDeniedHttpException(
                "Votre adresse IP ne correspond pas à la whitelist pour" .
                " l'accès à l'administration technique"
            );
        }
    }

    #[Override]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => 'onKernelRequest',
        ];
    }
}
