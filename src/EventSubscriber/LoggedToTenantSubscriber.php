<?php

namespace App\EventSubscriber;

use App\Controller\LoginController;
use App\Entity\User;
use App\Service\Permission;
use Exception;
use Override;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Mercure\Authorization;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Http\Event\LoginSuccessEvent;
use Twig;

readonly class LoggedToTenantSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private Security $security,
        private RouterInterface $router,
        private Permission $permission,
        private Twig\Environment $twig,
        private Authorization $authorization,
    ) {
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        if ($event->isPropagationStopped()) {
            return;
        }
        $request = $event->getRequest();
        if ($request->attributes->get('_stateless')) {
            return;
        }
        $tenant_url = $request->get('tenant_url');
        $user = $this->security->getUser();
        $session = $request->getSession();
        $session->start();
        $currentRoute = $request->get('_route');
        $isAdmin = false;
        if ($user instanceof User) {
            $session->set('username', $user->getUsername());
            foreach ($user->getAccessUsers() as $accessUser) {
                if ($accessUser->getTenant() === null) {
                    $isAdmin = true;
                    break;
                }
            }
        }
        $topic = $user
            ? $this->router->generate(
                'public_notify_user',
                ['id' => $session->getId()],
                UrlGeneratorInterface::ABSOLUTE_URL,
            )
            : 'null'
        ;
        $this->twig->addGlobal('topic_notify', $topic);
        $request = $event->getRequest();

        $topics = [$topic];
        if ($isAdmin) {
            $topics[] = $this->router->generate(
                'admin_probe',
                referenceType: UrlGeneratorInterface::ABSOLUTE_URL,
            );
        }
        try {
            $this->authorization->setCookie($request, $topics);
        } catch (Exception) {
        }

        if ($currentRoute && str_starts_with((string) $currentRoute, 'public_')) {
            $session->set('last_connection', 'public');
        } elseif ($currentRoute && str_starts_with((string) $currentRoute, 'admin_')) {
            $session->set('last_connection', 'admin');
        }

        if (str_contains((string) $request->get('_controller'), LoginController::class) && $user instanceof User) {
            $tenantUrl = null;
            if ($tenant = $user->getTenant($request)) {
                $tenantUrl = $tenant->getBaseurl();
            } else {
                $tenants = $user->getTenants();
                $tenantCount = $tenants->count();
                if ($tenantCount === 1) {
                    $tenantUrl = $tenants->first()->getBaseurl();
                }
            }
            if ($tenantUrl) {
                $targetUrl = $this->router->generate(
                    'app_home',
                    ['tenant_url' => $tenant_url]
                );
            } else {
                $targetUrl = $this->router->generate('public_home');
            }
            $response = new RedirectResponse($targetUrl);
            $event->setResponse($response);
            return;
        }

        // si on n'est pas sur un tenant, si on n'est pas connecté ou qu'on est
        // sur une page de login
        if (
            !$tenant_url
            || !$user instanceof User
            || str_contains((string) $request->get('_controller'), LoginController::class)
        ) {
            $session->save();
            return;
        }

        $tenantSessionId = sprintf('tenant.%s', $tenant_url);
        $tenantData = $session->get($tenantSessionId);

        // si on est connecté sur le tenant
        if ($tenantData) {
            $session->set('last_connection', $tenantSessionId);
            $session->save();
            return;
        }

        // l'utilisateur n'est pas connecté sur le tenant demandé mais sur un
        // autre tenant, on regarde si il y a besoin d'une reconnexion
        $user->getTenant($request); // setActiveTenant
        $accessUser = $user->getAccessUser();
        if (!$accessUser) {
            return;
        }

        $connectionType = $user->getLoginType();
        $connectionTypeTenantsData = array_filter(
            $session->all() ?: [],
            fn($v, $k) => str_starts_with($k, 'tenant.') && ($v['connection'] ?? '') === $connectionType,
            ARRAY_FILTER_USE_BOTH
        );
        foreach ($connectionTypeTenantsData as $values) {
            if ($accessUser->getLdap()) {
                if ($accessUser->getId() === $values['ldap']) {
                    $tenantData = $values;
                    $tenantData['baseurl'] = $tenant_url;
                    $session->set($tenantSessionId, $tenantData);
                    $session->save();
                    return;
                }
            } elseif ($accessUser->getOpenid()) {
                if ($accessUser->getOpenid()->getId() === $values['openid']) {
                    $tenantData = $values;
                    $tenantData['baseurl'] = $tenant_url;
                    $session->set($tenantSessionId, $tenantData);
                    $session->save();
                    return;
                }
            } else { // type app, on accepte toujours
                $tenantData = $values;
                $tenantData['baseurl'] = $tenant_url;
                $session->set($tenantSessionId, $tenantData);
                $session->save();
                return;
            }
        }

        // il y a besoin d'une reconnexion, on détruit la session en gardant ce
        // qui commence par "tenant." et on redirige sur /tenant/login
        $event->stopPropagation();
        $allTenantsData = array_filter(
            $session->all() ?: [],
            fn($k) => str_starts_with($k, 'tenant.'),
            ARRAY_FILTER_USE_KEY
        );
        $this->security->logout(false);
        foreach ($allTenantsData as $key => $values) {
            $session->set($key, $values);
        }
        if ($accessUser->getOpenid()) {
            $targetUrl = $this->router->generate(
                'app_tenant_openid',
                ['tenant_url' => $tenant_url, 'identifier' => $accessUser->getOpenid()->getIdentifier()]
            );
        } else {
            $targetUrl = $this->router->generate(
                'app_tenant_login_username',
                ['tenant_url' => $tenant_url, 'username' => $user->getUsername()]
            );
        }
        $filters = $request->getQueryString();
        if ($filters) {
            $targetUrl = sprintf('%s?%s', $targetUrl, $filters);
        }
        $response = new RedirectResponse($targetUrl);
        $event->setResponse($response);
        $session->save();
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        $request = $event->getRequest();
        if ($request->isXmlHttpRequest() || $request->attributes->get('_stateless')) {
            return;
        }
        // redirection sur la page 1 si la page demandé est plus grande que la limite de pages
        while ($exception && (!$exception instanceof HttpException || $exception->getStatusCode() !== 401)) {
            $exception = $exception->getPrevious();
        }
        if ($exception) {
            $tenant_url = $request->get('tenant_url');
            $currentRoute = $request->get('_route');
            if ($tenant_url && !str_starts_with((string) $currentRoute, 'admin_')) {
                $targetUrl = $this->router->generate(
                    'app_tenant_login',
                    ['tenant_url' => $tenant_url]
                );
            } else {
                $targetUrl = $this->router->generate('public_login');
            }
            $session = $request->getSession();
            $session->start();
            $session->set('after_login_redirect', $request->getRequestUri());
            $session->getFlashBag()
                ->add('warning', "Vous devez être connecté pour accéder à ce contenu");
            $response = new RedirectResponse($targetUrl);
            $event->setResponse($response);
        }
    }

    public function onLoginSuccessEvent(LoginSuccessEvent $event): void
    {
        if ($event->isPropagationStopped()) {
            return;
        }
        $request = $event->getRequest();
        if ($request->attributes->get('_stateless')) {
            return;
        }
        $session = $request->getSession();
        $url = $session->get('after_login_redirect');
        if ($url) {
            $session->remove('after_login_redirect');
            $session->getFlashBag()
                ->clear();
            $response = new RedirectResponse($url);
            $event->setResponse($response);
        }
        $user = $event->getUser();
        if ($user instanceof User) {
            $this->permission->setSessionData($user, $user->getTenant($request));
        }
    }

    #[Override]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => 'onKernelRequest',
            KernelEvents::EXCEPTION => 'onKernelException',
            LoginSuccessEvent::class => 'onLoginSuccessEvent',
        ];
    }
}
