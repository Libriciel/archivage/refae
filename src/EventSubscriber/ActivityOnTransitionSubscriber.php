<?php

namespace App\EventSubscriber;

use App\Entity\ActivitySubjectEntityInterface;
use App\Service\Manager\ActivityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Workflow\Attribute\AsCompletedListener;
use Symfony\Component\Workflow\Event\CompletedEvent;

/**
 * Enregistre une activitée propre à chaque transition appliquée
 */
readonly class ActivityOnTransitionSubscriber
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private Security $security,
        private ActivityManager $activityManager,
    ) {
    }

    #[AsCompletedListener]
    public function onChangeStateTransition(CompletedEvent $event): void
    {
        $object = $event->getSubject();
        if (!$object instanceof ActivitySubjectEntityInterface) {
            return;
        }

        $user = $this->security->getUser();
        $transition = $event->getTransition();
        $subject = sprintf(
            '"état" ("%s" -> "%s")',
            implode(', ', $transition->getFroms()),
            implode(', ', $transition->getTos()),
        );
        $activity = $this->activityManager->newActivity(
            entity: $object,
            action: $transition->getName(),
            user: $user,
            subject: $subject
        );
        $this->entityManager->persist($activity);
        $this->entityManager->flush();
    }
}
