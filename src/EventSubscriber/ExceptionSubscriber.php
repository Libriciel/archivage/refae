<?php

namespace App\EventSubscriber;

use Override;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;
use Twig\Environment;

readonly class ExceptionSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private Environment $twig,
        private TranslatorInterface $translator,
        private LoggerInterface $logger
    ) {
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $response = new Response();
        $request = $event->getRequest();
        $exception = $event->getThrowable();
        if ($event->getRequest()->attributes->get('_stateless')) {
            $response->headers->add(['Content-Type' => 'text/plain']);
            $response->setContent((string)$event->getThrowable());
        } elseif (
            !$request->isXmlHttpRequest()
            && $exception instanceof HttpException
            && $exception->getStatusCode() === 401
        ) {
            // @see LoggedToTenantSubscriber
            return;
        } else {
            $request = $event->getRequest();
            $template = !$request->isXmlHttpRequest()
                ? $this->twig->load('error/full_page.html.twig')
                : $this->twig->load('error/ajax.html.twig')
            ;
            $response->headers->add(['Rendered-Exception' => 'true']);

            $localTrace = $this->extractLocalTrace($exception);
            $trace = $exception->getTrace();
            $nestedExceptions = $this->nestedExceptionTraces($exception);

            if (!$exception instanceof HttpException) {
                $exception = new HttpException(
                    500,
                    $exception->getMessage(),
                    $exception
                );
            }
            $code = $exception->getStatusCode();
            $response->setStatusCode($code);
            $message = Response::$statusTexts[$code] ?? '';
            $positionTrace = $localTrace ? $localTrace[0] : $trace[0] ?? [];
            $positionTrace += ['file' => '', 'line' => ''];
            $vars = [
                'exception' => $exception::class,
                'route' => $request->getRequestUri(),
                'message' => $code . ' ' . $this->translator->trans($message, domain: 'exception'),
                'message_description' => $this->translator->trans("desc-$code", domain: 'exception'),
                'position' => sprintf(
                    '%s:%d %s%s',
                    $positionTrace['file'],
                    $positionTrace['line'],
                    ($positionTrace['class'] ?? ''),
                    (isset($positionTrace['function']) ? '::' . $positionTrace['function'] . '()' : ''),
                ),
                'nested_exceptions' => $nestedExceptions,
            ];
            if (str_contains((string) $request->headers->get('Accept', ''), 'application/json')) {
                $response->headers->add(['Content-Type' => 'application/json']);
                $json = [
                    "message" => $vars['message'],
                    "description" => $vars['message_description'],
                    "previous_page" => $request->headers->get('referer'),
                    "target_page" => $request->getUri(),
                    "method" => $request->getMethod(),
                    "exception" => $vars['exception'],
                    "datetime" => date('Y-m-d H:i:s'),
                    "position" => $vars['position'],
                ];
                if ($_ENV['DEBUG'] ?? false) {
                    $json += [
                        "location" => $vars['error_in'],
                        "exception_message" => $vars['exception_message'],
                        "trace" => explode("\n", (string) $vars['trace_as_string']),
                    ];
                }
                $response->setContent(
                    json_encode($json, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT)
                );
            } else {
                $response->setContent($template->render($vars));
            }
        }
        $event->setResponse($response);

        switch ($response->getStatusCode()) {
            case 401:
            case 403:
                $this->logger->error("\n" . $exception->getMessage());
                break;
            case 404:
                $this->logger->error("404 - {$request->getRequestUri()}\n{$exception->getMessage()}");
                break;
            default:
                $this->logger->error("\n" . $exception);
        }
    }

    private function nestedExceptionTraces(Throwable $exception): array
    {
        $current = [
            'error_in' => sprintf(
                '%s, line %d',
                $exception->getTrace()[0]['file'] ?? null,
                $exception->getTrace()[0]['line'] ?? null,
            ),
            'exception_message' => $exception->getMessage(),
            'trace_as_string' => $exception->getTraceAsString(),
        ];
        if ($exception->getPrevious()) {
            $previous = $this->nestedExceptionTraces($exception->getPrevious());
            return array_merge([$current], $previous);
        }
        return [$current];
    }

    #[Override]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }

    private function extractLocalTrace(Throwable $e): array
    {
        $localTrace = [];
        foreach ($e->getTrace() as $traceRow) {
            $file = $traceRow['file'] ?? '';
            if (str_starts_with($file, '/var/www/refae/src/')) {
                $traceRow['file'] = 'APP' . substr($file, strlen('/var/www/refae/src'));
                $localTrace[] = $traceRow;
            }
        }
        return $localTrace;
    }
}
