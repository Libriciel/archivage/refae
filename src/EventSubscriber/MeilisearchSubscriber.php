<?php

namespace App\EventSubscriber;

use App\Entity\MeilisearchIndexedInterface;
use App\Form\MeilisearchType;
use App\Service\MeilisearchInterface;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\PostPersistEventArgs;
use Doctrine\ORM\Event\PostUpdateEventArgs;
use Doctrine\ORM\Event\PreRemoveEventArgs;
use Doctrine\ORM\Events;
use Override;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;
use Twig;

#[AsDoctrineListener(event: Events::postPersist)]
#[AsDoctrineListener(event: Events::postUpdate)]
#[AsDoctrineListener(event: Events::preRemove)]
readonly class MeilisearchSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private FormFactoryInterface $formFactory,
        private MeilisearchInterface $meilisearch,
        private RequestStack $requestStack,
        private RouterInterface $router,
        private Twig\Environment $twig,
    ) {
    }

    public function onKernelRequest(): void
    {
        $request = $this->requestStack->getCurrentRequest();
        if ($request->attributes->get('_stateless')) {
            return;
        }
        $data = [];
        if (preg_match('/\\\(\w+)Controller::\w+$/', (string) $request->attributes->get('_controller'), $m)) {
            $data['index'] = $m[1];
        } else {
            return;
        }
        $entityClassname = "App\\Entity\\{$data['index']}";
        if (
            !class_exists($entityClassname) ||
            !in_array(MeilisearchIndexedInterface::class, class_implements($entityClassname))
        ) {
            return;
        }
        if ($tenantUrl = $request->get('tenant_url')) {
            $data['tenant'] = $tenantUrl;
            $data['public'] = 0;
        } else {
            $data['public'] = 1;
        }
        $data['published'] = (int)str_contains($request->getRequestUri(), 'published');
        $formBuilder = $this->formFactory->createNamedBuilder(
            '',
            MeilisearchType::class,
            $data,
            ['action' => $this->router->generate('public_search'),]
        );
        $this->twig->addGlobal('search_form', $formBuilder->getForm()->createView());
    }

    #[Override]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => 'onKernelRequest',
        ];
    }

    public function postPersist(PostPersistEventArgs $event): void
    {
        $object = $event->getObject();
        if ($object instanceof MeilisearchIndexedInterface) {
            $this->meilisearch->add($object);
        }
    }

    public function postUpdate(PostUpdateEventArgs $event): void
    {
        $object = $event->getObject();
        if ($object instanceof MeilisearchIndexedInterface) {
            $this->meilisearch->update($object);
        }
    }

    public function preRemove(PreRemoveEventArgs $event): void
    {
        $object = $event->getObject();
        if ($object instanceof MeilisearchIndexedInterface) {
            $this->meilisearch->delete($object);
        }
    }
}
