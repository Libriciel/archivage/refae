<?php

namespace App\EventSubscriber;

use Override;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\RateLimiter\RateLimiterFactory;

readonly class LimiterSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private RateLimiterFactory $anonymousApiLimiter,
        private RateLimiterFactory $authenticatedApiLimiter,
    ) {
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();
        $limiter = str_starts_with((string) $request->attributes->get('_route'), 'public_')
            ? $this->anonymousApiLimiter->create($request->getClientIp())
            : $this->authenticatedApiLimiter->create($request->getClientIp())
        ;
        $limit = $limiter->consume();
        if (!$limit->isAccepted()) {
            $headers = [
                'X-RateLimit-Remaining' => $limit->getRemainingTokens(),
                'X-RateLimit-Retry-After' => $limit->getRetryAfter()->getTimestamp() - time(),
                'X-RateLimit-Limit' => $limit->getLimit(),
            ];
            $response = new Response(null, Response::HTTP_TOO_MANY_REQUESTS, $headers);
            $event->setResponse($response);
            $event->stopPropagation();
        }
    }

    #[Override]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => 'onKernelRequest',
        ];
    }
}
