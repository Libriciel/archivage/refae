<?php

namespace App\EventSubscriber;

use DateInterval;
use DateTime;
use Override;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Uid\Uuid;

class GuestUuidListenerSubscriber implements EventSubscriberInterface
{
    public function onKernelResponse(ResponseEvent $event): void
    {
        $request = $event->getRequest();
        $tenYearsAfter = new DateTime();
        $tenYearsAfter->add(new DateInterval('P1Y'));
        if (!$request->cookies->has('guest_uuid')) {
            $uuid = Uuid::v7();
        } else {
            $uuid = $request->cookies->get('guest_uuid');
        }
        $cookie = new Cookie('guest_uuid', $uuid, $tenYearsAfter);
        $response = $event->getResponse();
        $response->headers->setCookie($cookie);
        $event->setResponse($response);
    }

    #[Override]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::RESPONSE => 'onKernelResponse',
        ];
    }
}
