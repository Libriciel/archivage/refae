<?php

namespace App\Session;

use DateTime;
use DateTimeZone;
use Override;
use PDO;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;

class CustomSessionHandler extends PdoSessionHandler
{
    public readonly string $table;
    public readonly string $idCol;
    public readonly string $dataCol;
    public readonly string $lifetimeCol;
    public readonly string $timeCol;

    public function __construct(
        private readonly RequestStack $requestStack,
        PDO|string $pdoOrDsn,
    ) {
        $this->table = 'sessions';
        $this->idCol = 'sess_id';
        $this->dataCol = 'sess_data';
        $this->lifetimeCol = 'sess_lifetime';
        $this->timeCol = 'sess_time';
        parent::__construct($pdoOrDsn);
    }

    #[Override]
    public function write(string $sessionId, string $data): bool
    {
        $request = $this->requestStack->getCurrentRequest();
        if ($request->headers->has('No-Session-Renewal')) {
            return true;
        }
        return parent::write($sessionId, $data);
    }

    public function getRemainingTime(string $id): int
    {
        $conn = $this->getConnection();
        $stmt = $conn->prepare("SELECT $this->lifetimeCol FROM $this->table WHERE $this->idCol = :id");
        $stmt->bindParam('id', $id);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($result) {
            return $result[$this->lifetimeCol] - time();
        }
        return 0;
    }

    public function findByPage(int $limit, int $offset): array
    {
        $conn = $this->getConnection();
        $stmt = $conn->prepare(
            "SELECT * FROM $this->table WHERE $this->lifetimeCol > :life"
            . " ORDER BY $this->idCol LIMIT $limit OFFSET $offset"
        );
        $time = time();
        $stmt->bindParam('life', $time);
        $stmt->execute();
        $result = [];
        $timezone = new DateTimeZone(date_default_timezone_get());
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
            $result[] = [
                $this->idCol => $row[$this->idCol],
                $this->dataCol => $this->parseData($row[$this->dataCol]),
                $this->lifetimeCol => DateTime::createFromFormat('U', $row[$this->lifetimeCol])
                    ->setTimezone($timezone),
                $this->timeCol => DateTime::createFromFormat('U', $row[$this->timeCol])
                    ->setTimezone($timezone),
            ];
        }
        return $result;
    }

    /**
     * @param resource $data
     * @return array|null
     */
    public function parseData($data): ?array
    {
        $content = stream_get_contents($data);
        ob_start();
        passthru(
            sprintf(
                "/var/www/refae/bin/session_unserialize.php '%s'",
                escapeshellarg(base64_encode($content))
            )
        );
        $out = ob_get_clean();
        if (!$out) {
            return null;
        }
        $unserialized = @unserialize($out);
        if (!$unserialized) {
            return null;
        }
        $result = [];
        foreach ($unserialized['_sf2_attributes'] ?? [] as $sessionData => $value) {
            if ($sessionData[0] === '_') {
                continue;
            }
            $result[$sessionData] = $value;
        }
        return $result;
    }

    public function findCount(): int
    {
        $conn = $this->getConnection();
        $stmt = $conn->prepare("SELECT COUNT(*) FROM $this->table WHERE $this->lifetimeCol > :life");
        $time = time();
        $stmt->bindParam('life', $time);
        $stmt->execute();
        $result = $stmt->fetch();
        if ($result) {
            return $result[0];
        }
        return 0;
    }

    public function deleteById(string $session): bool
    {
        $conn = $this->getConnection();
        $stmt = $conn->prepare("DELETE FROM $this->table WHERE $this->idCol = :id");
        $stmt->bindParam('id', $session);
        return $stmt->execute();
    }
}
