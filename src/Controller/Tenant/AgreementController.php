<?php

namespace App\Controller\Tenant;

use App\Entity\Agreement;
use App\Entity\User;
use App\Entity\VersionableEntityInterface;
use App\Form\AgreementImportConfirmType;
use App\Form\AgreementImportType;
use App\Form\AgreementType;
use App\Form\AgreementVersionType;
use App\Repository\AgreementRepository;
use App\Repository\TenantRepository;
use App\ResultSet\AgreementFileViewResultSet;
use App\ResultSet\AgreementPublishedResultSet;
use App\ResultSet\AgreementResultSet;
use App\ResultSet\AgreementResultSetPreview;
use App\ResultSet\PublicAgreementResultSet;
use App\Security\Voter\BelongsToTenantVoter;
use App\Security\Voter\EntityIsDeletableVoter;
use App\Security\Voter\EntityIsEditableVoter;
use App\Security\Voter\TenantUrlVoter;
use App\Service\Eaccpf;
use App\Service\ExportCsv;
use App\Service\ImportZip;
use App\Service\Manager\AgreementManager;
use App\Service\SessionFiles;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use IntlDateFormatter;
use Locale;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Workflow\WorkflowInterface;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class AgreementController extends AbstractController
{
    #[IsGranted('acl/Agreement/view')]
    #[Route(
        '/{tenant_url}/agreement/{page?1}',
        name: 'app_agreement',
        requirements: ['page' => '\d+'],
        methods: ['GET'],
    )]
    public function index(
        AgreementResultSet $table,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        return $this->render('agreement/index.html.twig', [
            'table' => $table,
        ]);
    }

    #[IsGranted('acl/Agreement/view')]
    #[Route(
        '/{tenant_url}/agreement-published/{page?1}',
        name: 'app_agreement_index_published',
        requirements: ['page' => '\d+'],
        methods: ['GET'],
    )]
    public function indexPublished(
        string $tenant_url,
        AgreementPublishedResultSet $table,
    ): Response {
        return $this->render('agreement/index_published.html.twig', [
            'table' => $table,
            'tenant_url' => $tenant_url,
        ]);
    }

    #[IsGranted('acl/Agreement/view')]
    #[IsGranted(BelongsToTenantVoter::class, 'agreement')]
    #[Route('/{tenant_url}/agreement/view/{agreement?}', name: 'app_agreement_view', methods: ['GET'])]
    public function view(
        #[MapEntity(expr: 'repository.findWithPreviousVersions(agreement)')]
        Agreement $agreement,
        AgreementFileViewResultSet $agreementFileResultSet,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $agreementFileResultSet->initialize($agreement);
        return $this->render('agreement/view.html.twig', [
            'agreement' => $agreement,
            'files' => $agreementFileResultSet,
        ]);
    }

    #[IsGranted('acl/Agreement/add_edit')]
    #[Route('/{tenant_url}/agreement/add/{session_tmpfile_uuid?}', name: 'app_agreement_add', methods: ['GET', 'POST'])]
    public function add(
        Request $request,
        AgreementManager $agreementManager,
        Eaccpf $eacCpf,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        SessionFiles $sessionFiles,
        string $session_tmpfile_uuid = null,
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        $agreement = $agreementManager->newAgreement($user->getTenant($request));
        if ($session_tmpfile_uuid && $request->isMethod('GET')) {
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);
            $eacCpf->applyDataToEntity($uri, $agreement);
        }

        $form = $this->createForm(
            AgreementType::class,
            $agreement,
            [
                'action' => $this->generateUrl(
                    'app_agreement_add',
                    ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid]
                ),
                'tenant_url' => $tenant_url,
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $agreementManager->save($agreement);

            return new JsonResponse(
                AgreementResultSet::normalize($agreement, ['index', 'action']),
                headers: ['X-Asalae-Success' => 'true']
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('agreement/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/Agreement/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'agreement')]
    #[IsGranted(EntityIsEditableVoter::class, 'agreement')]
    #[Route('/{tenant_url}/agreement/edit/{agreement?}', name: 'app_agreement_edit', methods: ['GET', 'POST'])]
    public function edit(
        Request $request,
        EntityManagerInterface $entityManager,
        Agreement $agreement,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $form = $this->createForm(
            AgreementType::class,
            $agreement,
            [
                'action' => $this->generateUrl('app_agreement_edit', ['agreement' => $agreement->getId()]),
                'tenant_url' => $tenant_url,
                'agreement_id' => $agreement->getId(),
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($agreement);
            $entityManager->flush();
            return new JsonResponse(
                AgreementResultSet::normalize($agreement, ['index', 'action']),
                headers: ['X-Asalae-Success' => 'true']
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('agreement/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/{tenant_url}/agreement/publish/{agreement?}', name: 'app_agreement_publish', methods: ['POST'])]
    #[IsGranted('acl/Agreement/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'agreement')]
    public function publish(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        WorkflowInterface $agreementStateMachine,
        Agreement $agreement,
    ): Response {
        $agreementStateMachine->apply($agreement, VersionableEntityInterface::T_PUBLISH);

        return new JsonResponse(
            AgreementResultSet::normalize($agreement, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/agreement/revoke/{agreement?}', name: 'app_agreement_revoke', methods: ['POST'])]
    #[IsGranted('acl/Agreement/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'agreement')]
    public function revoke(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        WorkflowInterface $agreementStateMachine,
        Agreement $agreement,
    ): Response {
        $agreementStateMachine->apply($agreement, VersionableEntityInterface::T_REVOKE);

        return new JsonResponse(
            AgreementResultSet::normalize($agreement, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/agreement/restore/{agreement?}', name: 'app_agreement_restore', methods: ['POST'])]
    #[IsGranted('acl/Agreement/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'agreement')]
    public function restore(
        WorkflowInterface $agreementStateMachine,
        Agreement $agreement,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $agreementStateMachine->apply($agreement, VersionableEntityInterface::T_RECOVER);

        return new JsonResponse(
            AgreementResultSet::normalize($agreement, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/agreement/activate/{agreement?}', name: 'app_agreement_activate', methods: ['POST'])]
    #[IsGranted('acl/Agreement/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'agreement')]
    public function activate(
        WorkflowInterface $agreementStateMachine,
        Agreement $agreement,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $agreementStateMachine->apply($agreement, VersionableEntityInterface::T_ACTIVATE);

        return new JsonResponse(
            AgreementResultSet::normalize($agreement, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/agreement/deactivate/{agreement?}', name: 'app_agreement_deactivate', methods: ['POST'])]
    #[IsGranted('acl/Agreement/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'agreement')]
    public function deactivate(
        WorkflowInterface $agreementStateMachine,
        Agreement $agreement,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $agreementStateMachine->apply($agreement, VersionableEntityInterface::T_DEACTIVATE);

        return new JsonResponse(
            AgreementResultSet::normalize($agreement, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route(
        '/{tenant_url}/agreement/new_version/{agreement?}',
        name: 'app_agreement_new_version',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/Agreement/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'agreement')]
    public function newVersion(
        Request $request,
        AgreementManager $agreementManager,
        Agreement $agreement,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $form = $this->createForm(
            AgreementVersionType::class,
            options: ['action' => $this->generateUrl('app_agreement_new_version', ['agreement' => $agreement->getId()])]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $newVersion = $agreementManager->newVersion($agreement, $form->get('reason')->getData());

            return new JsonResponse(
                AgreementResultSet::normalize($newVersion, ['index', 'action']),
                headers: [
                    'X-Asalae-Success' => 'true',
                    'X-Asalae-PreviousVersionId' => $agreement->getId(),
                ]
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('agreement/version.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/{tenant_url}/agreement/delete/{agreement?}', name: 'app_agreement_delete', methods: ['DELETE'])]
    #[IsGranted('acl/Agreement/delete')]
    #[IsGranted(EntityIsDeletableVoter::class, 'agreement')]
    #[IsGranted(BelongsToTenantVoter::class, 'agreement')]
    public function delete(
        EntityManagerInterface $entityManager,
        Agreement $agreement,
        AgreementRepository $agreementRepository,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $entityManager->remove($agreement);
        $entityManager->flush();

        if ($previousVersion = $agreementRepository->findPreviousVersion($agreement)) {
            return new JsonResponse(
                AgreementResultSet::normalize($previousVersion, ['index', 'action']),
                headers: [
                    'X-Asalae-Success' => 'true',
                    'X-Asalae-PreviousVersionEntity' => 'true',
                ]
            );
        }

        return new Response('done');
    }

    #[Route('/{tenant_url}/agreement/export', name: 'app_agreement_export', methods: ['GET'])]
    #[IsGranted('acl/Agreement/export')]
    public function export(
        AgreementRepository $agreementRepository,
        AgreementManager $agreementManager,
        AgreementResultSet $agreementResultSet,
        Request $request,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        $tenant = $user->getTenant($request);
        $query = $agreementRepository->queryForExport($tenant);
        $agreementResultSet->appendFilters($query);
        $zip = $agreementManager->export($query);
        $filename = sprintf(
            '%s_export.%s.refae',
            (new DateTime())->format('Y-m-d_His'),
            $agreementManager->getFileName()
        );
        $headers = [
            'Content-Description' => 'File Transfer',
            'Content-Type' => 'application/zip',
            'Content-Length' => strlen($zip),
            'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
            'Expires' => '0',
            'Cache-Control' => 'must-revalidate',
        ];
        return new Response($zip, Response::HTTP_OK, $headers);
    }

    #[Route(
        '/{tenant_url}/agreement/import',
        name: 'app_agreement_import1',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/Agreement/import')]
    public function import1(
        string $tenant_url,
        Request $request,
        SessionFiles $sessionFiles,
    ): Response {
        $form = $this->createForm(
            AgreementImportType::class,
            options: ['action' => $this->generateUrl('app_agreement_import1', ['tenant_url' => $tenant_url])]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $session_tmpfile_uuid = $request->get('agreement_import')['file'] ?? null;
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);

            if (mime_content_type($uri) === 'application/zip') {
                $modalParams = [
                    'btnAcceptContent' => '<i class="fa fa-upload fa-space" aria-hidden="true"></i>'
                        . "<span>Importer</span>",
                    'btnCancelContent' => '<i class="fa fa-trash fa-space" aria-hidden="true"></i>'
                        . "<span>Annuler l'import</span>",
                    'btnCancelUrl' => $this->generateUrl(
                        'app_agreement_delete_import',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ];
                $response->headers->add([
                    'X-Asalae-Modal-Params' => json_encode($modalParams),
                    'X-Asalae-Step-Title' => json_encode("Récapitulatif avant import"),
                    'X-Asalae-Step' => $this->generateUrl(
                        'app_agreement_import2',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ]);
                return $response;
            } else {
                $form->get('file')
                    ->addError(new FormError("Ce fichier ne semble pas contenir des accords de versement"));
            }
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('agreement/import1.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/agreement/import/{session_tmpfile_uuid}',
        name: 'app_agreement_import2',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/Agreement/import')]
    public function import2(
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        Request $request,
        string $session_tmpfile_uuid,
        AgreementManager $agreementManager,
        AgreementResultSetPreview $previewResultset,
        ImportZip $importZip,
    ): Response {
        $importZip->extract($session_tmpfile_uuid, $agreementManager);
        $previewResultset->setDataFromArray($importZip->previewData);

        $form = $this->createForm(
            AgreementImportConfirmType::class,
            null,
            [
                'action' => $this->generateUrl(
                    'app_agreement_import2',
                    ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                )
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $agreementManager->import($importZip);
            $this->addFlash(
                'success',
                $importZip->importableCount > 1
                    ? "$importZip->importableCount accords de versement importés avec succès"
                    : "$importZip->importableCount accord de versement importé avec succès"
            );
            $response->headers->add(['X-Asalae-Redirect' => $this->generateUrl('app_agreement')]);
            return $response;
        }

        $formatter = new IntlDateFormatter(
            Locale::getDefault(),
            IntlDateFormatter::FULL,
            IntlDateFormatter::SHORT
        );
        return $this->render('agreement/import2.html.twig', [
            'view_data' => [
                "Date du fichier d'export" => $formatter->format($importZip->exportDate),
                "Nombre d'accords de versement'" => $importZip->dataCount,
                "Nombre de fichiers liés" => $importZip->fileCount,
                "Nombre d'étiquettes à importer" => count($importZip->labels),
            ],
            'content_preview_resultset' => $previewResultset,
            'summerize_data' => [
                "Nombre d'accords de versement impossibles à importer" => $importZip->failedCount,
                "Total importable" => $importZip->importableCount,
            ],
            'errors' => $importZip->errors,
            'fatalError' => $importZip->hasFatalError,
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/agreement/delete_import/{session_tmpfile_uuid}',
        name: 'app_agreement_delete_import',
        methods: ['DELETE'],
    )]
    #[IsGranted('acl/Agreement/import')]
    public function deleteImport(
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        SessionFiles $sessionFiles,
        string $session_tmpfile_uuid,
    ): Response {
        $sessionFiles->delete($session_tmpfile_uuid);
        $response = new Response();
        $response->setContent('ok');
        $response->headers->add(['X-Asalae-Success' => 'true']);
        return $response;
    }

    #[Route('/{tenant_url}/agreement/csv', name: 'app_agreement_csv', methods: ['GET'])]
    #[IsGranted('acl/Agreement/view')]
    public function csv(
        AgreementResultSet $agreementResultSet,
        AgreementPublishedResultSet $agreementPublishedResultSet,
        ExportCsv $exportCsv,
        Request $request,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        return $exportCsv->fromResultSet(
            $request->get('index_published')
                ? $agreementPublishedResultSet
                : $agreementResultSet
        );
    }

    #[Route(
        '/{tenant_url}/reader/agreement/{page?1}',
        name: 'app_reader_agreement',
        requirements: ['page' => '\d+']
    )]
    public function readerAgreement(
        PublicAgreementResultSet $table,
        TenantRepository $tenantRepository,
        string $tenant_url = null,
    ): Response {
        $tenant = $tenant_url ? $tenantRepository->findOneByBaseurl($tenant_url) : null;
        $table->initialize($tenant);
        return $this->render('reader/agreement.html.twig', [
            'table' => $table,
        ]);
    }

    #[Route('/{tenant_url}/reader/agreement/csv', name: 'app_reader_agreement_csv', methods: ['GET'])]
    public function readerCsv(
        PublicAgreementResultSet $publicAgreementResultSet,
        ExportCsv $exportCsv,
        TenantRepository $tenantRepository,
        string $tenant_url = null,
    ): Response {
        $tenant = $tenant_url ? $tenantRepository->findOneByBaseurl($tenant_url) : null;
        $publicAgreementResultSet->initialize($tenant);
        return $exportCsv->fromResultSet($publicAgreementResultSet);
    }
}
