<?php

namespace App\Controller\Tenant;

use App\Controller\DownloadFileTrait;
use App\Entity\AuthorityFile;
use App\Entity\Authority;
use App\Form\AuthorityFileType;
use App\ResultSet\AuthorityFileResultSet;
use App\Security\Voter\BelongsToTenantVoter;
use App\Security\Voter\EntityIsEditableVoter;
use App\Security\Voter\TenantUrlVoter;
use App\Service\Manager\FileManager;
use App\Service\SessionFiles;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class AuthorityFileController extends AbstractController
{
    use DownloadFileTrait;

    #[IsGranted('acl/Authority/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'authority')]
    #[IsGranted(EntityIsEditableVoter::class, 'authority')]
    #[Route(
        '/{tenant_url}/authority/{authority}/file/{page?1}',
        name: 'app_authority_file_index',
        requirements: ['page' => '\d+'],
        methods: ['GET'],
    )]
    public function index(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        Authority $authority,
        AuthorityFileResultSet $resultSet,
    ): Response {
        $resultSet->initialize($authority);
        return $this->render('authority_file/index.html.twig', [
            'table' => $resultSet,
            'authority' => $authority,
        ]);
    }

    #[IsGranted('acl/Authority/view')]
    #[Route(
        '/{tenant_url}/authority-file/{authorityFile}/download',
        name: 'app_authority_file_download',
        methods: ['GET']
    )]
    #[IsGranted(BelongsToTenantVoter::class, 'authorityFile')]
    public function download(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        AuthorityFile $authorityFile,
    ): Response {
        return $this->getFileDownloadResponse($authorityFile->getFile());
    }

    #[IsGranted('acl/Authority/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'authority')]
    #[IsGranted(EntityIsEditableVoter::class, 'authority')]
    #[Route('/{tenant_url}/authority/{authority}/file/add', name: 'app_authority_file_add', methods: ['GET', 'POST'])]
    public function add(
        Authority $authority,
        Request $request,
        EntityManagerInterface $entityManager,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        SessionFiles $sessionFiles,
    ): Response {
        $authorityFile = new AuthorityFile();
        $authorityFile->setAuthority($authority);
        $authorityFile->setType(AuthorityFile::TYPE_OTHER);
        $form = $this->createForm(
            AuthorityFileType::class,
            $authorityFile,
            ['action' => $this->generateUrl(
                'app_authority_file_add',
                ['tenant_url' => $tenant_url, 'authority' => $authority->getId()]
            )]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $session_tmpfile_uuid = $request->get('authority_file')['file'] ?? null;
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);
            $file = FileManager::setDataFromPath($uri);
            $authorityFile->setFile($file);
            $entityManager->persist($authorityFile);

            $entityManager->persist($file);
            $entityManager->flush();

            $response = new JsonResponse(AuthorityFileResultSet::normalize($authorityFile, ['index', 'action']));
            $response->headers->add(['X-Asalae-Success' => 'true']);
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('authority_file/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/Authority/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'authority')]
    #[IsGranted(EntityIsEditableVoter::class, 'authority')]
    #[Route(
        '/{tenant_url}/authority/{authority}/file/edit/{authorityFile}',
        name: 'app_authority_file_edit',
        methods: ['GET', 'POST'],
    )]
    public function edit(
        Request $request,
        EntityManagerInterface $entityManager,
        Authority $authority,
        AuthorityFile $authorityFile,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $form = $this->createForm(
            AuthorityFileType::class,
            $authorityFile,
            [
                'action' => $this->generateUrl('app_authority_file_edit', [
                    'tenant_url' => $tenant_url,
                    'authority' => $authority->getId(),
                    'authorityFile' => $authorityFile->getId(),
                ]),
                'withFileUpload' => false,
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($authorityFile);
            $entityManager->flush();

            $response = new JsonResponse(AuthorityFileResultSet::normalize($authorityFile, ['index', 'action']));
            $response->headers->add(['X-Asalae-Success' => 'true']);

            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('authority_file/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/Authority/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'authority')]
    #[IsGranted(EntityIsEditableVoter::class, 'authority')]
    #[Route(
        '/{tenant_url}/authority/{authority}/file/delete/{authorityFile}',
        name: 'app_authority_file_delete',
        methods: ['DELETE'],
    )]
    public function delete(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        Authority $authority,
        AuthorityFile $authorityFile,
        EntityManagerInterface $entityManager,
    ): Response {
        $entityManager->remove($authorityFile->getFile());
        $entityManager->flush();

        return new Response('done');
    }
}
