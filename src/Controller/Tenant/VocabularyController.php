<?php

namespace App\Controller\Tenant;

use App\Entity\User;
use App\Entity\VersionableEntityInterface;
use App\Entity\Vocabulary;
use App\Form\VocabularyImportConfirmType;
use App\Form\VocabularyImportType;
use App\Form\VocabularyType;
use App\Form\VocabularyVersionType;
use App\Repository\TenantRepository;
use App\Repository\VocabularyRepository;
use App\ResultSet\VocabularyFileViewResultSet;
use App\ResultSet\PublicVocabularyResultSet;
use App\ResultSet\VocabularyPublishedResultSet;
use App\ResultSet\VocabularyResultSet;
use App\ResultSet\VocabularyResultSetPreview;
use App\Security\Voter\BelongsToTenantVoter;
use App\Security\Voter\EntityIsDeletableVoter;
use App\Security\Voter\EntityIsEditableVoter;
use App\Security\Voter\TenantUrlVoter;
use App\Service\ExportCsv;
use App\Service\ImportZip;
use App\Service\Manager\VocabularyManager;
use App\Service\SessionFiles;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use IntlDateFormatter;
use Locale;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Workflow\WorkflowInterface;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class VocabularyController extends AbstractController
{
    #[IsGranted('acl/Vocabulary/view')]
    #[Route(
        '/{tenant_url}/vocabulary/{page?1}',
        name: 'app_vocabulary',
        requirements: ['page' => '\d+'],
        methods: ['GET'],
    )]
    public function index(
        string $tenant_url,
        VocabularyResultSet $table,
    ): Response {
        return $this->render('vocabulary/index.html.twig', [
            'table' => $table,
            'tenant_url' => $tenant_url,
        ]);
    }

    #[IsGranted('acl/Vocabulary/view')]
    #[Route(
        '/{tenant_url}/vocabulary-published/{page?1}',
        name: 'app_vocabulary_index_published',
        requirements: ['page' => '\d+'],
        methods: ['GET'],
    )]
    public function indexPublished(
        string $tenant_url,
        VocabularyPublishedResultSet $table,
    ): Response {
        return $this->render('vocabulary/index_published.html.twig', [
            'table' => $table,
            'tenant_url' => $tenant_url,
        ]);
    }

    #[IsGranted('acl/Vocabulary/view')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    #[Route('/{tenant_url}/vocabulary/view/{vocabulary?}', name: 'app_vocabulary_view', methods: ['GET'])]
    public function view(
        #[MapEntity(expr: 'repository.findWithPreviousVersions(vocabulary)')]
        Vocabulary $vocabulary,
        VocabularyFileViewResultSet $vocabularyFileResultSet,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $vocabularyFileResultSet->initialize($vocabulary);
        return $this->render('vocabulary/view.html.twig', [
            'vocabulary' => $vocabulary,
            'files' => $vocabularyFileResultSet,
        ]);
    }

    #[IsGranted('acl/Vocabulary/add_edit')]
    #[Route('/{tenant_url}/vocabulary/add', name: 'app_vocabulary_add', methods: ['GET', 'POST'])]
    public function add(
        Request $request,
        VocabularyManager $vocabularyManager,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        $vocabulary = $vocabularyManager->newVocabulary($user->getTenant($request));

        $form = $this->createForm(
            VocabularyType::class,
            $vocabulary,
            [
                'action' => $this->generateUrl('app_vocabulary_add'),
                'tenant_url' => $tenant_url,
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $vocabularyManager->save($vocabulary);

            return new JsonResponse(
                VocabularyResultSet::normalize($vocabulary, ['index', 'action']),
                headers: ['X-Asalae-Success' => 'true']
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('vocabulary/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/Vocabulary/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    #[IsGranted(EntityIsEditableVoter::class, 'vocabulary')]
    #[Route('/{tenant_url}/vocabulary/edit/{vocabulary?}', name: 'app_vocabulary_edit', methods: ['GET', 'POST'])]
    public function edit(
        Request $request,
        EntityManagerInterface $entityManager,
        Vocabulary $vocabulary,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $form = $this->createForm(
            VocabularyType::class,
            $vocabulary,
            [
                'action' => $this->generateUrl('app_vocabulary_edit', ['vocabulary' => $vocabulary->getId()]),
                'tenant_url' => $tenant_url,
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($vocabulary);
            $entityManager->flush();

            return new JsonResponse(
                VocabularyResultSet::normalize($vocabulary, ['index', 'action']),
                headers: ['X-Asalae-Success' => 'true']
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('vocabulary/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/{tenant_url}/vocabulary/publish/{vocabulary?}', name: 'app_vocabulary_publish', methods: ['POST'])]
    #[IsGranted('acl/Vocabulary/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    public function publish(
        WorkflowInterface $vocabularyStateMachine,
        Vocabulary $vocabulary,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $vocabularyStateMachine->apply($vocabulary, VersionableEntityInterface::T_PUBLISH);

        return new JsonResponse(
            VocabularyResultSet::normalize($vocabulary, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/vocabulary/revoke/{vocabulary?}', name: 'app_vocabulary_revoke', methods: ['POST'])]
    #[IsGranted('acl/Vocabulary/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    public function revoke(
        WorkflowInterface $vocabularyStateMachine,
        Vocabulary $vocabulary,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $vocabularyStateMachine->apply($vocabulary, VersionableEntityInterface::T_REVOKE);

        return new JsonResponse(
            VocabularyResultSet::normalize($vocabulary, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/vocabulary/restore/{vocabulary?}', name: 'app_vocabulary_restore', methods: ['POST'])]
    #[IsGranted('acl/Vocabulary/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    public function restore(
        WorkflowInterface $vocabularyStateMachine,
        Vocabulary $vocabulary,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $vocabularyStateMachine->apply($vocabulary, VersionableEntityInterface::T_RECOVER);

        return new JsonResponse(
            VocabularyResultSet::normalize($vocabulary, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/vocabulary/activate/{vocabulary?}', name: 'app_vocabulary_activate', methods: ['POST'])]
    #[IsGranted('acl/Vocabulary/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    public function activate(
        WorkflowInterface $vocabularyStateMachine,
        Vocabulary $vocabulary,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $vocabularyStateMachine->apply($vocabulary, VersionableEntityInterface::T_ACTIVATE);

        return new JsonResponse(
            VocabularyResultSet::normalize($vocabulary, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/vocabulary/deactivate/{vocabulary?}', name: 'app_vocabulary_deactivate', methods: ['POST'])]
    #[IsGranted('acl/Vocabulary/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    public function deactivate(
        WorkflowInterface $vocabularyStateMachine,
        Vocabulary $vocabulary,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $vocabularyStateMachine->apply($vocabulary, VersionableEntityInterface::T_DEACTIVATE);

        return new JsonResponse(
            VocabularyResultSet::normalize($vocabulary, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route(
        '/{tenant_url}/vocabulary/new-version/{vocabulary?}',
        name: 'app_vocabulary_new_version',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/Vocabulary/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    public function newVersion(
        Request $request,
        VocabularyManager $vocabularyManager,
        Vocabulary $vocabulary,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $form = $this->createForm(
            VocabularyVersionType::class,
            options: [
                'action' => $this->generateUrl('app_vocabulary_new_version', ['vocabulary' => $vocabulary->getId()])
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $newVersion = $vocabularyManager->newVersion($vocabulary, $form->get('reason')->getData());

            return new JsonResponse(
                VocabularyResultSet::normalize($newVersion, ['index', 'action']),
                headers: [
                    'X-Asalae-Success' => 'true',
                    'X-Asalae-PreviousVersionId' => $vocabulary->getId(),
                ]
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('vocabulary/version.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/{tenant_url}/vocabulary/delete/{vocabulary?}', name: 'app_vocabulary_delete', methods: ['DELETE'])]
    #[IsGranted('acl/Vocabulary/delete')]
    #[IsGranted(EntityIsDeletableVoter::class, 'vocabulary')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    public function delete(
        EntityManagerInterface $entityManager,
        Vocabulary $vocabulary,
        VocabularyRepository $vocabularyRepository,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $entityManager->remove($vocabulary);
        $entityManager->flush();

        if ($previousVersion = $vocabularyRepository->findPreviousVersion($vocabulary)) {
            return new JsonResponse(
                VocabularyResultSet::normalize($previousVersion, ['index', 'action']),
                headers: [
                    'X-Asalae-Success' => 'true',
                    'X-Asalae-PreviousVersionEntity' => 'true',
                ]
            );
        }

        return new Response('done');
    }

    #[Route('/{tenant_url}/vocabulary/export', name: 'app_vocabulary_export', methods: ['GET'])]
    #[IsGranted('acl/Vocabulary/view')]
    public function export(
        VocabularyRepository $vocabularyRepository,
        TenantRepository $tenantRepository,
        VocabularyManager $vocabularyManager,
        VocabularyResultSet $vocabularyResultSet,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $tenant = $tenantRepository->findOneByBaseurl($tenant_url);
        $query = $vocabularyRepository->queryForExport($tenant);
        $vocabularyResultSet->appendFilters($query);

        $zip = $vocabularyManager->export($query);
        $filename = sprintf(
            '%s_export.%s.refae',
            (new DateTime())->format('Y-m-d_His'),
            $vocabularyManager->getFileName()
        );
        $headers = [
            'Content-Description' => 'File Transfer',
            'Content-Type' => 'application/zip',
            'Content-Length' => strlen($zip),
            'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
            'Expires' => '0',
            'Cache-Control' => 'must-revalidate',
        ];
        return new Response($zip, Response::HTTP_OK, $headers);
    }

    #[Route('/{tenant_url}/vocabulary/import', name: 'app_vocabulary_import1', methods: ['GET', 'POST'])]
    #[IsGranted('acl/Vocabulary/view')]
    public function import1(
        string $tenant_url,
        Request $request,
        SessionFiles $sessionFiles,
    ): Response {
        $form = $this->createForm(
            VocabularyImportType::class,
            options: ['action' => $this->generateUrl('app_vocabulary_import1', ['tenant_url' => $tenant_url])]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $session_tmpfile_uuid = $request->get('vocabulary_import')['file'] ?? null;
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);

            if (mime_content_type($uri) === 'application/zip') {
                $modalParams = [
                    'btnAcceptContent' => '<i class="fa fa-upload fa-space" aria-hidden="true"></i>'
                        . "<span>Importer</span>",
                    'btnCancelContent' => '<i class="fa fa-trash fa-space" aria-hidden="true"></i>'
                        . "<span>Annuler l'import</span>",
                    'btnCancelUrl' => $this->generateUrl(
                        'app_vocabulary_delete_import',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ];
                $response->headers->add([
                    'X-Asalae-Modal-Params' => json_encode($modalParams),
                    'X-Asalae-Step-Title' => json_encode("Récapitulatif avant import"),
                    'X-Asalae-Step' => $this->generateUrl(
                        'app_vocabulary_import2',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ]);
                return $response;
            } else {
                $form->get('file')
                    ->addError(new FormError("Ce fichier ne semble pas contenir des vocabulares contrôlés"));
            }
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('vocabulary/import1.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/vocabulary/import/{session_tmpfile_uuid}',
        name: 'app_vocabulary_import2',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/Vocabulary/import')]
    public function import2(
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        Request $request,
        string $session_tmpfile_uuid,
        VocabularyManager $vocabularyManager,
        VocabularyResultSetPreview $previewResultset,
        ImportZip $importZip,
    ): Response {
        $importZip->extract($session_tmpfile_uuid, $vocabularyManager);
        $previewResultset->setDataFromArray($importZip->previewData);

        $form = $this->createForm(
            VocabularyImportConfirmType::class,
            null,
            [
                'action' => $this->generateUrl(
                    'app_vocabulary_import2',
                    ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                )
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $vocabularyManager->import($importZip);
            $this->addFlash(
                'success',
                $importZip->importableCount > 1
                    ? "$importZip->importableCount vocabulaires importés avec succès"
                    : "$importZip->importableCount vocabulaire importé avec succès"
            );
            $response->headers->add(['X-Asalae-Redirect' => $this->generateUrl('app_vocabulary')]);
            return $response;
        }

        $formatter = new IntlDateFormatter(
            Locale::getDefault(),
            IntlDateFormatter::FULL,
            IntlDateFormatter::SHORT
        );
        return $this->render('vocabulary/import2.html.twig', [
            'view_data' => [
                "Date du fichier d'export" => $formatter->format($importZip->exportDate),
                "Nombre de vocabulaires contrôlés" => $importZip->dataCount,
                "Nombre de fichiers liés" => $importZip->fileCount,
                "Nombre d'étiquettes à importer" => count($importZip->labels),
            ],
            'content_preview_resultset' => $previewResultset,
            'summerize_data' => [
                "Nombre de vocabulaires contrôlés impossibles à importer" => $importZip->failedCount,
                "Total importable" => $importZip->importableCount,
            ],
            'errors' => $importZip->errors,
            'fatalError' => $importZip->hasFatalError,
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/vocabulary/delete-import/{session_tmpfile_uuid}',
        name: 'app_vocabulary_delete_import',
        methods: ['DELETE'],
    )]
    #[IsGranted('acl/Vocabulary/import')]
    public function deleteImport(
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        string $session_tmpfile_uuid,
        SessionFiles $sessionFiles,
    ): Response {
        $sessionFiles->delete($session_tmpfile_uuid);
        $response = new Response();
        $response->setContent('ok');
        $response->headers->add(['X-Asalae-Success' => 'true']);
        return $response;
    }

    #[Route('/{tenant_url}/vocabulary/csv', name: 'app_vocabulary_csv', methods: ['GET'])]
    #[IsGranted('acl/Vocabulary/view')]
    public function csv(
        ExportCsv $exportCsv,
        Request $request,
        VocabularyResultSet $vocabularyResultSet,
        VocabularyPublishedResultSet $vocabularyPublishedResultSet,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        return $exportCsv->fromResultSet(
            $request->get('index_published')
                ? $vocabularyPublishedResultSet
                : $vocabularyResultSet
        );
    }

    #[Route(
        '/{tenant_url}/reader/vocabulary/{page?1}',
        name: 'app_reader_vocabulary',
        requirements: ['page' => '\d+']
    )]
    public function readerVocabulary(
        PublicVocabularyResultSet $table,
        TenantRepository $tenantRepository,
        string $tenant_url = null,
    ): Response {
        $tenant = $tenant_url ? $tenantRepository->findOneByBaseurl($tenant_url) : null;
        $table->initialize($tenant);
        return $this->render('reader/vocabulary.html.twig', [
            'table' => $table,
        ]);
    }

    #[Route('/{tenant_url}/reader/vocabulary/csv', name: 'app_reader_vocabulary_csv', methods: ['GET'])]
    public function readerCsv(
        PublicVocabularyResultSet $publicVocabularyResultSet,
        ExportCsv $exportCsv,
        TenantRepository $tenantRepository,
        string $tenant_url = null,
    ): Response {
        $tenant = $tenant_url ? $tenantRepository->findOneByBaseurl($tenant_url) : null;
        $publicVocabularyResultSet->initialize($tenant);
        return $exportCsv->fromResultSet($publicVocabularyResultSet);
    }
}
