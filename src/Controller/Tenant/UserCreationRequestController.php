<?php

namespace App\Controller\Tenant;

use App\Entity\UserCreationRequest;
use App\Security\Voter\BelongsToTenantVoter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(priority: 1)]
class UserCreationRequestController extends AbstractController
{
    /** @noinspection PhpUnusedParameterInspection */
    #[IsGranted('acl/User/delete')]
    #[IsGranted(BelongsToTenantVoter::class, 'userCreationRequest')]
    #[Route('/{tenant_url}/user-creation-request/delete/{username?}', name: 'app_user_delete_creation_request')]
    public function delete(
        string $tenant_url,
        UserCreationRequest $userCreationRequest,
        EntityManagerInterface $entityManager
    ): Response {
        $entityManager->remove($userCreationRequest);
        $entityManager->flush();

        $response = new Response();
        $response->setContent('done');
        return $response;
    }
}
