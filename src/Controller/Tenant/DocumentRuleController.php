<?php

namespace App\Controller\Tenant;

use App\Entity\Category;
use App\Entity\DocumentRule;
use App\Entity\DocumentRuleImportSession;
use App\Entity\Tenant;
use App\Entity\User;
use App\Entity\VersionableEntityInterface;
use App\Form\DocumentRuleImportConfirmType;
use App\Form\DocumentRuleImportCsvColumnsType;
use App\Form\DocumentRuleImportCsvRowType;
use App\Form\DocumentRuleImportCsvType;
use App\Form\DocumentRuleImportType;
use App\Form\DocumentRuleType;
use App\Form\DocumentRuleVersionType;
use App\Repository\CategoryRepository;
use App\Repository\DocumentRuleImportSessionRepository;
use App\Repository\DocumentRuleRepository;
use App\Repository\TenantRepository;
use App\ResultSet\DocumentRuleFileViewResultSet;
use App\ResultSet\DocumentRulePublishedResultSet;
use App\ResultSet\DocumentRuleResultSet;
use App\ResultSet\DocumentRuleResultSetPreview;
use App\ResultSet\PublicDocumentRuleResultSet;
use App\Security\Voter\BelongsToTenantVoter;
use App\Security\Voter\EntityIsDeletableVoter;
use App\Security\Voter\EntityIsEditableVoter;
use App\Security\Voter\TenantUrlVoter;
use App\Service\Csv;
use App\Service\Eaccpf;
use App\Service\ExportCsv;
use App\Service\ImportZip;
use App\Service\LoggedUser;
use App\Service\Manager\CategoryManager;
use App\Service\Manager\DocumentRuleManager;
use App\Service\Manager\FileManager;
use App\Service\SessionFiles;
use App\Service\Translit;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use IntlDateFormatter;
use Locale;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Workflow\WorkflowInterface;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class DocumentRuleController extends AbstractController
{
    #[IsGranted('acl/DocumentRule/view')]
    #[Route(
        '/{tenant_url}/document-rule/{page?1}',
        name: 'app_document_rule',
        requirements: ['page' => '\d+'],
        methods: ['GET'],
    )]
    public function index(
        DocumentRuleResultSet $table,
        DocumentRuleImportSessionRepository $documentRuleImportSesionRepository,
        LoggedUser $loggedUser,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $importSession = $documentRuleImportSesionRepository->findOneBy(
            ['tenant' => $loggedUser->tenant(), 'user' => $loggedUser->user()]
        );
        return $this->render('document_rule/index.html.twig', [
            'table' => $table,
            'import_session' => $importSession,
        ]);
    }

    #[IsGranted('acl/DocumentRule/view')]
    #[Route(
        '/{tenant_url}/document-rule-published/{page?1}',
        name: 'app_document_rule_index_published',
        requirements: ['page' => '\d+'],
        methods: ['GET'],
    )]
    public function indexPublished(
        string $tenant_url,
        DocumentRulePublishedResultSet $table,
    ): Response {
        return $this->render('document_rule/index_published.html.twig', [
            'table' => $table,
            'tenant_url' => $tenant_url,
        ]);
    }

    #[IsGranted('acl/DocumentRule/view')]
    #[IsGranted(BelongsToTenantVoter::class, 'documentRule')]
    #[Route('/{tenant_url}/document-rule/view/{documentRule?}', name: 'app_document_rule_view', methods: ['GET'])]
    public function view(
        #[MapEntity(expr: 'repository.findWithPreviousVersions(documentRule)')]
        DocumentRule $documentRule,
        CategoryManager $categoryManager,
        DocumentRuleFileViewResultSet $documentRuleFileResultSet,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $documentRuleFileResultSet->initialize($documentRule);
        return $this->render('document_rule/view.html.twig', [
            'documentRule' => $categoryManager->fillDocumentRulePath($documentRule),
            'files' => $documentRuleFileResultSet,
        ]);
    }

    #[IsGranted('acl/DocumentRule/add_edit')]
    #[Route(
        '/{tenant_url}/document-rule/add/{session_tmpfile_uuid?}',
        name: 'app_document_rule_add',
        methods: ['GET', 'POST'],
    )]
    public function add(
        Request $request,
        DocumentRuleManager $documentRuleManager,
        Eaccpf $eacCpf,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        SessionFiles $sessionFiles,
        string $session_tmpfile_uuid = null,
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        $documentRule = $documentRuleManager->newDocumentRule($user->getTenant($request));
        if ($session_tmpfile_uuid && $request->isMethod('GET')) {
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);
            $eacCpf->applyDataToEntity($uri, $documentRule);
        }

        $form = $this->createForm(
            DocumentRuleType::class,
            $documentRule,
            ['action' => $this->generateUrl(
                'app_document_rule_add',
                ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid]
            )]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $documentRuleManager->save($documentRule);

            return new JsonResponse(
                DocumentRuleResultSet::normalize($documentRule, ['index', 'action']),
                headers: ['X-Asalae-Success' => 'true']
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('document_rule/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/DocumentRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'documentRule')]
    #[IsGranted(EntityIsEditableVoter::class, 'documentRule')]
    #[Route(
        '/{tenant_url}/document-rule/edit/{documentRule?}',
        name: 'app_document_rule_edit',
        methods: ['GET', 'POST'],
    )]
    public function edit(
        Request $request,
        EntityManagerInterface $entityManager,
        DocumentRule $documentRule,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $form = $this->createForm(
            DocumentRuleType::class,
            $documentRule,
            ['action' => $this->generateUrl('app_document_rule_edit', ['documentRule' => $documentRule->getId()])]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($documentRule);
            $entityManager->flush();
            return new JsonResponse(
                DocumentRuleResultSet::normalize($documentRule, ['index', 'action']),
                headers: ['X-Asalae-Success' => 'true']
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('document_rule/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/document-rule/publish/{documentRule?}',
        name: 'app_document_rule_publish',
        methods: ['POST'],
    )]
    #[IsGranted('acl/DocumentRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'documentRule')]
    public function publish(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        WorkflowInterface $documentruleStateMachine,
        DocumentRule $documentRule,
    ): Response {
        $documentruleStateMachine->apply($documentRule, VersionableEntityInterface::T_PUBLISH);

        return new JsonResponse(
            DocumentRuleResultSet::normalize($documentRule, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route(
        '/{tenant_url}/document-rule/revoke/{documentRule?}',
        name: 'app_document_rule_revoke',
        methods: ['POST']
    )]
    #[IsGranted('acl/DocumentRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'documentRule')]
    public function revoke(
        WorkflowInterface $servicelevelStateMachine,
        DocumentRule $documentRule,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $servicelevelStateMachine->apply($documentRule, VersionableEntityInterface::T_REVOKE);

        return new JsonResponse(
            DocumentRuleResultSet::normalize($documentRule, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route(
        '/{tenant_url}/document-rule/restore/{documentRule?}',
        name: 'app_document_rule_restore',
        methods: ['POST'],
    )]
    #[IsGranted('acl/DocumentRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'documentRule')]
    public function restore(
        WorkflowInterface $servicelevelStateMachine,
        DocumentRule $documentRule,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $servicelevelStateMachine->apply($documentRule, VersionableEntityInterface::T_RECOVER);

        return new JsonResponse(
            DocumentRuleResultSet::normalize($documentRule, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route(
        '/{tenant_url}/document-rule/activate/{documentRule?}',
        name: 'app_document_rule_activate',
        methods: ['POST'],
    )]
    #[IsGranted('acl/DocumentRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'documentRule')]
    public function activate(
        WorkflowInterface $servicelevelStateMachine,
        DocumentRule $documentRule,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $servicelevelStateMachine->apply($documentRule, VersionableEntityInterface::T_ACTIVATE);

        return new JsonResponse(
            DocumentRuleResultSet::normalize($documentRule, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route(
        '/{tenant_url}/document-rule/deactivate/{documentRule?}',
        name: 'app_document_rule_deactivate',
        methods: ['POST'],
    )]
    #[IsGranted('acl/DocumentRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'documentRule')]
    public function deactivate(
        WorkflowInterface $servicelevelStateMachine,
        DocumentRule $documentRule,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $servicelevelStateMachine->apply($documentRule, VersionableEntityInterface::T_DEACTIVATE);

        return new JsonResponse(
            DocumentRuleResultSet::normalize($documentRule, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route(
        '/{tenant_url}/document-rule/new-version/{documentRule?}',
        name: 'app_document_rule_new_version',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/DocumentRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'documentRule')]
    public function newVersion(
        Request $request,
        DocumentRuleManager $documentRuleManager,
        DocumentRule $documentRule,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $form = $this->createForm(
            DocumentRuleVersionType::class,
            options: [
                'action' => $this->generateUrl(
                    'app_document_rule_new_version',
                    ['documentRule' => $documentRule->getId()]
                )
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $newVersion = $documentRuleManager->newVersion($documentRule, $form->get('reason')->getData());

            return new JsonResponse(
                DocumentRuleResultSet::normalize($newVersion, ['index', 'action']),
                headers: [
                    'X-Asalae-Success' => 'true',
                    'X-Asalae-PreviousVersionId' => $documentRule->getId(),
                ]
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('document_rule/version.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/document-rule/delete/{documentRule?}',
        name: 'app_document_rule_delete',
        methods: ['DELETE'],
    )]
    #[IsGranted('acl/DocumentRule/delete')]
    #[IsGranted(EntityIsDeletableVoter::class, 'documentRule')]
    #[IsGranted(BelongsToTenantVoter::class, 'documentRule')]
    public function delete(
        EntityManagerInterface $entityManager,
        DocumentRule $documentRule,
        DocumentRuleRepository $documentRuleRepository,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $entityManager->remove($documentRule);
        $entityManager->flush();

        if ($previousVersion = $documentRuleRepository->findPreviousVersion($documentRule)) {
            return new JsonResponse(
                DocumentRuleResultSet::normalize($previousVersion, ['index', 'action']),
                headers: [
                    'X-Asalae-Success' => 'true',
                    'X-Asalae-PreviousVersionEntity' => 'true',
                ]
            );
        }

        return new Response('done');
    }

    #[Route('/{tenant_url}/document-rule/export', name: 'app_document_rule_export', methods: ['GET'])]
    #[IsGranted('acl/DocumentRule/export')]
    public function export(
        DocumentRuleRepository $documentRuleRepository,
        DocumentRuleManager $documentRuleManager,
        DocumentRuleResultSet $documentRuleResultSet,
        Request $request,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        $tenant = $user->getTenant($request);
        $query = $documentRuleRepository->queryForExport($tenant);
        $documentRuleResultSet->appendFilters($query);

        $zip = $documentRuleManager->export($query);
        $filename = sprintf(
            '%s_export.%s.refae',
            (new DateTime())->format('Y-m-d_His'),
            $documentRuleManager->getFileName()
        );
        $headers = [
            'Content-Description' => 'File Transfer',
            'Content-Type' => 'application/zip',
            'Content-Length' => strlen($zip),
            'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
            'Expires' => '0',
            'Cache-Control' => 'must-revalidate',
        ];
        return new Response($zip, Response::HTTP_OK, $headers);
    }

    #[Route(
        '/{tenant_url}/document-rule/import-csv',
        name: 'app_document_rule_import_csv1',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/DocumentRule/import')]
    public function importCsv1(
        string $tenant_url,
        Request $request,
        SessionFiles $sessionFiles,
        DocumentRuleImportSessionRepository $documentRuleImportSesionRepository,
        LoggedUser $loggedUser,
    ): Response {
        $importSession = $documentRuleImportSesionRepository->findOneBy(
            ['tenant' => $loggedUser->tenant(), 'user' => $loggedUser->user()]
        );
        if ($importSession) {
            throw new ConflictHttpException("Une session d'import existe déjà");
        }
        $form = $this->createForm(
            DocumentRuleImportCsvType::class,
            options: [
                'action' => $this->generateUrl(
                    'app_document_rule_import_csv1',
                    ['tenant_url' => $tenant_url]
                )
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $session_tmpfile_uuid = $request->get('document_rule_import_csv')['file'] ?? null;
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);

            if (in_array(mime_content_type($uri), ['text/csv', 'text/plain'])) {
                $modalParams = [
                    'btnAcceptContent' => '<i class="fa fa-upload fa-space" aria-hidden="true"></i>'
                        . "<span>Importer</span>",
                    'btnCancelContent' => '<i class="fa fa-trash fa-space" aria-hidden="true"></i>'
                        . "<span>Annuler l'import</span>",
                    'btnCancelUrl' => $this->generateUrl(
                        'app_document_rule_delete_import',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ];
                $response->headers->add([
                    'X-Asalae-Modal-Params' => json_encode($modalParams),
                    'X-Asalae-Step-Title' => json_encode("Sélection des types de colonnes"),
                    'X-Asalae-Step' => $this->generateUrl(
                        'app_document_rule_import_csv2',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ]);
                return $response;
            } else {
                $form->get('file')
                    ->addError(new FormError("Ce fichier ne semble pas contenir des règles documentaires"));
            }
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('document_rule/import_csv1.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/document-rule/import-csv2/{session_tmpfile_uuid}',
        name: 'app_document_rule_import_csv2',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/DocumentRule/import')]
    public function importCsv2(
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        Request $request,
        string $session_tmpfile_uuid,
        DocumentRuleManager $documentRuleManager,
        DocumentRuleResultSetPreview $previewResultset,
        SessionFiles $sessionFiles,
        LoggedUser $loggedUser,
        EntityManagerInterface $entityManager,
        DocumentRuleImportSessionRepository $documentRuleImportSesionRepository,
    ): Response {
        $importSession = $documentRuleImportSesionRepository->findOneBy(
            ['tenant' => $loggedUser->tenant(), 'user' => $loggedUser->user()]
        );
        if ($importSession) {
            throw new ConflictHttpException("Une session d'import existe déjà");
        }
        $importSession = new DocumentRuleImportSession();
        $handle = fopen($sessionFiles->getUri($session_tmpfile_uuid), 'r');
        $firstLine = fgets($handle);
        $importSession->setLastSeekOffset(ftell($handle));
        fclose($handle);
        $csv = new Csv($firstLine);

        $form = $this->createForm(
            DocumentRuleImportCsvColumnsType::class,
            null,
            [
                'columns' => array_combine(
                    $csv->getData()[0],
                    array_map(
                        Translit::asciiToLower(...),
                        $csv->getData()[0]
                    )
                ),
                'action' => $this->generateUrl(
                    'app_document_rule_import_csv2',
                    ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                )
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $importSession->setTenant($loggedUser->tenant());
            $importSession->setUser($loggedUser->user());
            $importSession->setColumnTypes(json_encode($form->getData()));
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);
            $file = FileManager::setDataFromPath($uri);
            $importSession->setCsvFile($file);
            $importSession->setLastRow(2);

            $entityManager->persist($importSession);
            $entityManager->persist($file);
            $entityManager->flush();

            $response->headers->add(
                [
                    'X-Asalae-Redirect' => $this->generateUrl(
                        'app_document_rule_import_csv3',
                        ['username' => $loggedUser->user()->getUsername()]
                    ),
                ]
            );
            return $response;
        }

        return $this->render('document_rule/import_csv2.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    /** @noinspection PhpUnusedParameterInspection */
    #[IsGranted('acl/DocumentRule/import')]
    #[Route(
        '/{tenant_url}/document-rule/import-csv3/{username}',
        name: 'app_document_rule_import_csv3',
        methods: ['GET', 'POST'],
    )]
    public function importCsv3(
        DocumentRuleImportSessionRepository $documentRuleImportSesionRepository,
        LoggedUser $loggedUser,
        string $tenant_url,
        string $username,
        EntityManagerInterface $entityManager,
        Request $request,
        CategoryRepository $categoryRepository,
        DocumentRuleRepository $documentRuleRepository,
    ): Response {
        $importSession = $documentRuleImportSesionRepository->findOneBy(
            ['tenant' => $loggedUser->tenant(), 'user' => $loggedUser->user()]
        );
        if (!$importSession) {
            $this->addFlash(
                'danger',
                "La session d'import n'a pas été trouvée. Veuillez recharger le fichier CSV.",
            );
            return $this->redirectToRoute('app_document_rule');
        }
        $handle = $importSession->getCsvFile()->getContent();
        $firstLine = fgets($handle);
        $posAfterFirstLine = ftell($handle);
        $isFirstPage = $importSession->getLastSeekOffset() <= $posAfterFirstLine;
        fseek($handle, $importSession->getLastSeekOffset());
        $currentLine = fgets($handle);
        if ($currentLine === false) {
            $this->addFlash(
                'success',
                "L'import du fichier CSV est terminé.",
            );
            $entityManager->remove($importSession);
            $entityManager->flush();
            return $this->redirectToRoute('app_document_rule');
        }
        $isLastPage = feof($handle);
        $importSession->setLastSeekOffset(ftell($handle));
        if (!$isLastPage) {
            fgetc($handle); // un dernier char en cas de derniere ligne == \n
            $isLastPage = feof($handle);
        }
        fclose($handle);
        $csv = new Csv($firstLine . $currentLine);

        $form = $this->createForm(
            DocumentRuleImportCsvRowType::class,
            null,
            [
                'importSession' => $importSession,
                'rawData' => $csv->getData()[1],
            ]
        );
        $form->handleRequest($request);

        $data = $form->getData();
        /** @var DocumentRule $documentRule */
        $documentRule = $data['documentRule'];
        $formDocument = $this->createForm(
            DocumentRuleType::class,
            $data['documentRule'],
        );
        $formDocument->handleRequest($request);

        $existCategory = false;
        $existDocument = false;
        if (!empty($data['identified']['label'])) {
            $parentCategory = $data['identified']['parent_category'];
            if (
                $parentCategory instanceof Category
                && $data['identified']['label'] === $parentCategory->getName()
            ) {
                $existCategory = $parentCategory;
            } elseif ($parentCategory instanceof Category) {
                $existCategory = $categoryRepository->findOneBy([
                    'tenant' => $loggedUser->tenant(),
                    'name' => $data['identified']['label'],
                    'parent' => $parentCategory,
                ]);
            } else {
                $existCategory = $categoryRepository->findOneBy([
                    'tenant' => $loggedUser->tenant(),
                    'name' => $data['identified']['label'],
                    'parent' => null,
                ]);
            }
            $existDocument = $documentRuleRepository->findOneBy([
                'tenant' => $loggedUser->tenant(),
                'name' => $data['identified']['label'],
            ]);
        }
        $missingAppraisalRule = $data['missings']['appraisal_rule'] ?? null;
        $missingAccessRule = $data['missings']['access_rule'] ?? null;

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $category = new Category();
            $category->setTenant($loggedUser->tenant());
            $category->setName($data['category_name']);
            $category->setDescription($data['category_description']);
            $category->setParent($data['category_parent']);
            $entityManager->persist($category);
            $importSession->setLastCategory($category);
            $importSession->setLastCategoryLevel($data['level']);
            $this->addFlash('success', "La catégorie {$category->getName()} a bien été ajouté");
            $importSession->setLastRow($importSession->getLastRow() + 1);

            $entityManager->persist($importSession);
            $entityManager->flush();

            return $this->redirectToRoute('app_document_rule_import_csv3', ['username' => $username]);
        } elseif ($formDocument->isSubmitted() && $formDocument->isValid()) {
            $entityManager->persist($documentRule);
            $this->addFlash('success', "La règle documentaire {$documentRule->getName()} a bien été ajouté");
            $importSession->setLastRow($importSession->getLastRow() + 1);

            $entityManager->persist($importSession);
            $entityManager->flush();

            if ($isLastPage) {
                $this->addFlash(
                    'success',
                    "L'import du fichier CSV est terminé.",
                );
                $entityManager->remove($importSession);
                $entityManager->flush();
                return $this->redirectToRoute('app_document_rule');
            }
            return $this->redirectToRoute('app_document_rule_import_csv3', ['username' => $username]);
        }

        return $this->render('document_rule/import_csv3.html.twig', [
            'import_session' => $importSession,
            'csv' => $csv->getData(),
            'is_first_page' => $isFirstPage,
            'is_last_page' => $isLastPage,
            'last_category' => $importSession->getLastCategory()?->getPath(),
            'form' => $form->createView(),
            'formDocument' => $formDocument->createView(),
            'exist_category' => $existCategory,
            'exist_document' => $existDocument,
            'missing_appraisal_rule' => $missingAppraisalRule,
            'missing_access_rule' => $missingAccessRule,
            'level' => $data['identified']['level'] ?? -1,
        ], $response);
    }

    /** @noinspection PhpUnusedParameterInspection */
    #[IsGranted('acl/DocumentRule/import')]
    #[Route(
        '/{tenant_url}/document-rule/delete-import-csv3/{username}',
        name: 'app_document_rule_delete_import_csv3',
        methods: ['DELETE'],
    )]
    public function importCsv3Delete(
        DocumentRuleImportSessionRepository $documentRuleImportSesionRepository,
        LoggedUser $loggedUser,
        string $tenant_url,
        string $username,
        EntityManagerInterface $entityManager,
    ): Response {
        $importSession = $documentRuleImportSesionRepository->findOneBy(
            ['tenant' => $loggedUser->tenant(), 'user' => $loggedUser->user()]
        );
        if (!$importSession) {
            throw $this->createNotFoundException();
        }
        $entityManager->remove($importSession);
        $entityManager->flush();
        $this->addFlash(
            'success',
            "La session d'import a été supprimée avec succès",
        );
        $response = new Response();
        $response->headers->add(['X-Asalae-Redirect' => $this->generateUrl('app_document_rule')]);
        return $response;
    }

    #[IsGranted('acl/DocumentRule/import')]
    #[Route(
        '/{tenant_url}/document-rule/import-csv3-prev/{username}',
        name: 'app_document_rule_import_csv3_prev',
        methods: ['GET'],
    )]
    public function importCsv3Prev(
        DocumentRuleImportSessionRepository $documentRuleImportSesionRepository,
        LoggedUser $loggedUser,
        string $tenant_url,
        string $username,
        EntityManagerInterface $entityManager,
    ): Response {
        $importSession = $documentRuleImportSesionRepository->findOneBy(
            ['tenant' => $loggedUser->tenant(), 'user' => $loggedUser->user()]
        );
        if (!$importSession) {
            $this->addFlash(
                'danger',
                "La session d'import n'a pas été trouvée. Veuillez recharger le fichier CSV.",
            );
            return $this->redirectToRoute('app_document_rule');
        }

        $handle = $importSession->getCsvFile()->getContent();
        $currOffset = $importSession->getLastSeekOffset() - 2;
        $currChar = null;
        while ($currOffset > 0 && $currChar !== PHP_EOL) {
            fseek($handle, $currOffset);
            $currChar = fgetc($handle);
            $currOffset -= 1;
        }
        $importSession->setLastSeekOffset(ftell($handle));
        $importSession->setLastRow(max(2, $importSession->getLastRow() - 1));
        fclose($handle);

        $entityManager->persist($importSession);
        $entityManager->flush();

        return $this->redirectToRoute(
            'app_document_rule_import_csv3',
            ['tenant_url' => $tenant_url, 'username' => $username]
        );
    }

    #[IsGranted('acl/DocumentRule/import')]
    #[Route(
        '/{tenant_url}/document-rule/import-csv3-next/{username}/{level?}/{category?}',
        name: 'app_document_rule_import_csv3_next',
        methods: ['GET'],
    )]
    public function importCsv3Next(
        DocumentRuleImportSessionRepository $documentRuleImportSesionRepository,
        LoggedUser $loggedUser,
        string $tenant_url,
        string $username,
        EntityManagerInterface $entityManager,
        CategoryRepository $categoryRepository,
        ?string $category,
        ?int $level,
    ): Response {
        $importSession = $documentRuleImportSesionRepository->findOneBy(
            ['tenant' => $loggedUser->tenant(), 'user' => $loggedUser->user()]
        );
        if (!$importSession) {
            $this->addFlash(
                'danger',
                "La session d'import n'a pas été trouvée. Veuillez recharger le fichier CSV.",
            );
            return $this->redirectToRoute('app_document_rule');
        }
        if ($category) {
            $sessionCategory = $importSession->getLastCategory();
            if ($sessionCategory) {
                $sessionCategory->setPath(
                    $categoryRepository->getPathAsString($sessionCategory, ['separator' => ' ⮕ '])
                );
                $names = explode(' ⮕ ', (string) $sessionCategory->getPath());
            } else {
                $names = [];
            }
            $names[] = $category;
            $category = $categoryRepository->findByNameForTenant($loggedUser->tenant(), $names);
            $category = $category ? end($category) : $sessionCategory;
            $importSession->setLastCategoryLevel($level);
            $importSession->setLastCategory($category);
        }

        $handle = $importSession->getCsvFile()->getContent();
        fseek($handle, $importSession->getLastSeekOffset());
        fgets($handle);
        $importSession->setLastSeekOffset(ftell($handle));
        $importSession->setLastRow($importSession->getLastRow() + 1);
        fclose($handle);
        $entityManager->persist($importSession);
        $entityManager->flush();

        return $this->redirectToRoute(
            'app_document_rule_import_csv3',
            ['tenant_url' => $tenant_url, 'username' => $username]
        );
    }

    #[Route(
        '/{tenant_url}/document-rule/import',
        name: 'app_document_rule_import1',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/DocumentRule/import')]
    public function import1(
        string $tenant_url,
        Request $request,
        SessionFiles $sessionFiles,
    ): Response {
        $form = $this->createForm(
            DocumentRuleImportType::class,
            options: ['action' => $this->generateUrl('app_document_rule_import1', ['tenant_url' => $tenant_url])]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $session_tmpfile_uuid = $request->get('document_rule_import')['file'] ?? null;
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);

            if (mime_content_type($uri) === 'application/zip') {
                $modalParams = [
                    'btnAcceptContent' => '<i class="fa fa-upload fa-space" aria-hidden="true"></i>'
                        . "<span>Importer</span>",
                    'btnCancelContent' => '<i class="fa fa-trash fa-space" aria-hidden="true"></i>'
                        . "<span>Annuler l'import</span>",
                    'btnCancelUrl' => $this->generateUrl(
                        'app_document_rule_delete_import',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ];
                $response->headers->add([
                    'X-Asalae-Modal-Params' => json_encode($modalParams),
                    'X-Asalae-Step-Title' => json_encode("Récapitulatif avant import"),
                    'X-Asalae-Step' => $this->generateUrl(
                        'app_document_rule_import2',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ]);
                return $response;
            } else {
                $form->get('file')
                    ->addError(new FormError("Ce fichier ne semble pas contenir des règles documentaires"));
            }
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('document_rule/import1.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/document-rule/import/{session_tmpfile_uuid}',
        name: 'app_document_rule_import2',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/DocumentRule/import')]
    public function import2(
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        Request $request,
        string $session_tmpfile_uuid,
        DocumentRuleManager $documentRuleManager,
        DocumentRuleResultSetPreview $previewResultset,
        ImportZip $importZip,
    ): Response {
        $importZip->extract($session_tmpfile_uuid, $documentRuleManager);
        $previewResultset->setDataFromArray($importZip->previewData);

        $form = $this->createForm(
            DocumentRuleImportConfirmType::class,
            null,
            [
                'action' => $this->generateUrl(
                    'app_document_rule_import2',
                    ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                )
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $documentRuleManager->import($importZip);
            $this->addFlash('success', "$importZip->importableCount règles documentaires importées avec succès");
            $response->headers->add(['X-Asalae-Redirect' => $this->generateUrl('app_document_rule')]);
            return $response;
        }

        $formatter = new IntlDateFormatter(
            Locale::getDefault(),
            IntlDateFormatter::FULL,
            IntlDateFormatter::SHORT
        );
        return $this->render('document_rule/import2.html.twig', [
            'view_data' => [
                "Date du fichier d'export" => $formatter->format($importZip->exportDate),
                "Nombre de règles documentaires" => $importZip->dataCount,
                "Nombre de fichiers liés" => $importZip->fileCount,
                "Nombre de catégories à importer" => count($importZip->errors['missing_categories']),
                "Nombre d'étiquettes à importer" => count($importZip->labels),
            ],
            'content_preview_resultset' => $previewResultset,
            'summerize_data' => [
                "Nombre de règles documentaires impossibles à importer" => $importZip->failedCount,
                "Total importable" => $importZip->importableCount,
            ],
            'errors' => $importZip->errors,
            'fatalError' => $importZip->hasFatalError,
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/document-rule/delete-import/{session_tmpfile_uuid}',
        name: 'app_document_rule_delete_import',
        methods: ['DELETE'],
    )]
    #[IsGranted('acl/DocumentRule/import')]
    public function deleteImport(
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        SessionFiles $sessionFiles,
        string $session_tmpfile_uuid,
    ): Response {
        $sessionFiles->delete($session_tmpfile_uuid);
        $response = new Response();
        $response->setContent('ok');
        $response->headers->add(['X-Asalae-Success' => 'true']);
        return $response;
    }

    #[Route('/{tenant_url}/document-rule/csv', name: 'app_document_rule_csv', methods: ['GET'])]
    #[IsGranted('acl/DocumentRule/view')]
    public function csv(
        ExportCsv $exportCsv,
        Request $request,
        DocumentRuleResultSet $documentRuleResultSet,
        DocumentRulePublishedResultSet $documentRulePublishedResultSet,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        return $exportCsv->fromResultSet(
            $request->get('index_published')
                ? $documentRulePublishedResultSet
                : $documentRuleResultSet
        );
    }

    #[Route(
        '/{tenant_url}/reader/document-rule/{page?1}/{names?}',
        name: 'app_reader_document_rule',
        requirements: ['page' => '\d+', 'names' => '.+'],
    )]
    public function readerDocumentRule(
        ?string $names,
        CategoryRepository $categoryRepository,
        PublicDocumentRuleResultSet $publicCategoryResultSet,
        TenantRepository $tenantRepository,
        string $tenant_url,
    ): Response {
        $tenant = $tenantRepository->findOneByBaseurl($tenant_url);
        $childCategories = [];
        $category = $this->getCategory(
            $names,
            $categoryRepository,
            $publicCategoryResultSet,
            $tenant,
            $childCategories,
        );
        $childCategories = array_unique($childCategories);
        sort($childCategories);

        return $this->render('reader/document_rule.html.twig', [
            'table' => $publicCategoryResultSet,
            'category' => $category,
            'childCategories' => $childCategories,
        ]);
    }

    #[Route(
        '/{tenant_url}/reader/document-rule/csv/{names?}',
        name: 'app_reader_document_rule_csv',
        requirements: ['names' => '.+']
    )]
    public function readerCsv(
        ?string $names,
        CategoryRepository $categoryRepository,
        PublicDocumentRuleResultSet $publicCategoryResultSet,
        TenantRepository $tenantRepository,
        string $tenant_url,
        ExportCsv $exportCsv,
    ): Response {
        $tenant = $tenantRepository->findOneByBaseurl($tenant_url);
        $childCategories = [];
        $this->getCategory(
            $names,
            $categoryRepository,
            $publicCategoryResultSet,
            $tenant,
            $childCategories,
        );
        return $exportCsv->fromResultSet($publicCategoryResultSet);
    }

    private function getCategory(
        ?string $names,
        CategoryRepository $categoryRepository,
        PublicDocumentRuleResultSet $publicCategoryResultSet,
        Tenant $tenant,
        &$childCategories
    ): ?Category {
        $names = $names ? explode('/', $names) : [];
        $category = null;
        if ($names) {
            $categories = $categoryRepository->findByPath($names, $tenant);
            if (!$categories) {
                throw $this->createNotFoundException("Cette catégorie n'existe pas");
            }
            $childCategories = [];
            foreach ($categories as $category) {
                $categoryNames = $category->getChildren()->map(fn (Category $cat) => $cat->getName())->toArray();
                $childCategories = array_merge($childCategories, $categoryNames);
            }
            $category = $categories[0];
            $publicCategoryResultSet->initialize($categories, $tenant);
        } else {
            $childCategories = array_map(
                fn (Category $cat) => $cat->getName(),
                $categoryRepository->findBy(
                    ['parent' => null, 'tenant' => $tenant]
                )
            );
            $publicCategoryResultSet->initialize([], $tenant);
        }
        $childCategories = array_unique($childCategories);
        sort($childCategories);
        return $category;
    }
}
