<?php

namespace App\Controller\Tenant;

use App\Entity\AccessUser;
use App\Entity\User;
use App\Entity\UserCreationRequest;
use App\Entity\UserPasswordRenewToken;
use App\Form\AccessUserType;
use App\Form\UserCreationRequestRefusalType;
use App\Form\UserType;
use App\Repository\UserCreationRequestRepository;
use App\ResultSet\UserCreationRequestResultSet;
use App\ResultSet\UserResultSet;
use App\Security\Voter\BelongsToTenantVoter;
use App\Security\Voter\EntityIsDeletableVoter;
use App\Security\Voter\TenantUrlVoter;
use App\Service\ExportCsv;
use App\Service\LoggedUser;
use App\Service\Manager\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class UserController extends AbstractController
{
    /** @noinspection PhpUnusedParameterInspection */
    #[IsGranted('acl/User/view')]
    #[Route('/{tenant_url}/user/{page?1}', name: 'app_user', requirements: ['page' => '\d+'], methods: ['GET'])]
    public function index(
        UserResultSet $resultSet,
        string $tenant_url,
    ): Response {
        return $this->render('user/index.html.twig', [
            'table' => $resultSet,
        ]);
    }

    /** @noinspection PhpUnusedParameterInspection */
    #[IsGranted('acl/User/view')]
    #[IsGranted(BelongsToTenantVoter::class, 'user')]
    #[Route('/{tenant_url}/user/view/{username?}', name: 'app_user_view', methods: ['GET'])]
    public function view(
        string $tenant_url,
        User $user,
        Request $request,
    ) {
        $user->getTenant($request);
        if ($user->getAccessUser()?->getOpenid()) {
            $openid = $user->getAccessUser()?->getOpenid();
            $loginUrl = $this->generateUrl(
                'app_tenant_openid',
                ['tenant_url' => $tenant_url, 'identifier' => $openid->getIdentifier()],
                UrlGeneratorInterface::ABSOLUTE_URL
            );
            $openid->setLoginUrl($loginUrl);
        }
        return $this->render('user/view.html.twig', [
            'user' => $user,
        ]);
    }

    /** @noinspection PhpUnusedParameterInspection */
    #[IsGranted('acl/User/add_edit')]
    #[Route('/{tenant_url}/user/add', name: 'app_user_add', methods: ['GET', 'POST'])]
    public function add(
        string $tenant_url,
        Request $request,
        UserManager $userManager,
        EntityManagerInterface $entityManager,
        UserCreationRequestRepository $userCreationRequestRepository,
        LoggedUser $loggedUser,
    ) {
        $user = new User();
        $user->setNotifyUserRegistration(true);
        $user->addTenant($loggedUser->tenant());
        $user->setActiveTenant($loggedUser->tenant());
        $user->setUserDefinedPassword(false);

        $action = $this->generateUrl('app_user_add');
        if ($creationRequestId = $request->get('user_creation_request')) {
            $userCreationRequest = $userCreationRequestRepository->find($creationRequestId);
            if (!$userCreationRequest) {
                throw $this->createNotFoundException("Cette demande de création d'utilisateur n'existe plus");
            }
            if ($userCreationRequest->getTenant() !== $loggedUser->tenant()) {
                throw new AccessDeniedHttpException();
            }
            $action = $this->generateUrl('app_user_add', ['user_creation_request' => $creationRequestId]);
            $user->setUsername($userCreationRequest->getUsername());
            $user->setName($userCreationRequest->getName());
            $user->setEmail($userCreationRequest->getEmail());
        }

        $form = $this->createForm(
            UserType::class,
            $user,
            ['action' => $action]
        );
        $form->handleRequest($request);

        $response = new JsonResponse();
        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->add($user);

            $response->setData(UserResultSet::normalize($user, ['index']));
            $response->headers->add([
                'X-Asalae-Step-Title' => json_encode("Ajout d'un accès à l'utilisateur"),
                'X-Asalae-Step' => $this->generateUrl(
                    'app_user_add_access',
                    ['username' => $user->getUsername()]
                    + ($creationRequestId && isset($userCreationRequest)
                        ? ['user_creation_request' => 'true']
                        : []),
                ),
            ]);

            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('user/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /** @noinspection PhpUnusedParameterInspection */
    #[IsGranted('acl/User/add_edit')]
    #[Route('/{tenant_url}/user/add-access/{username}', name: 'app_user_add_access', methods: ['GET', 'POST'])]
    public function addAccess(
        string $tenant_url,
        string $username,
        User $user,
        LoggedUser $loggedUser,
        Request $request,
        EntityManagerInterface $entityManager,
        MailerInterface $mailer,
    ) {
        $accessUser = new AccessUser();
        $accessUser->setUser($user);
        $accessUser->setTenant($loggedUser->tenant());
        $routeParams = $request->get('_route_params');
        $queryParams = $request->query->all();
        $form = $this->createForm(
            AccessUserType::class,
            $accessUser,
            ['action' => $this->generateUrl('app_user_add_access', $routeParams + $queryParams)]
        );
        $form->handleRequest($request);

        $response = new JsonResponse();
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setActiveTenant($loggedUser->tenant());
            $entityManager->persist($accessUser);
            $entityManager->flush();

            if ($user->getLoginType() === AccessUser::LOGIN_TYPE_REFAE) {
                $userPasswordRenewToken = new UserPasswordRenewToken();
                $userPasswordRenewToken->setUser($user);
                $entityManager->persist($userPasswordRenewToken);
                $entityManager->flush();
                $this->sendPasswordInitMail($mailer, $userPasswordRenewToken);
            } else {
                $this->sendNewAccessMail($mailer, $user);
            }
            $openid = $accessUser->getOpenid();
            if ($openid) {
                $loginUrl = $this->generateUrl(
                    'app_tenant_openid',
                    ['tenant_url' => $accessUser->getTenant()->getBaseurl(), 'identifier' => $openid->getIdentifier()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );
                $openid->setLoginUrl($loginUrl);
            }

            $response->setData(UserResultSet::normalize($user, ['index', 'action']));
            $response->headers->add(['X-Asalae-Success' => 'true']);
            if ($request->get('user_creation_request')) {
                $this->addFlash('success', "{$user->getCommonName()} a bien été ajouté");
                $response->headers->add(['X-Asalae-Redirect' => $this->generateUrl('app_user_creation_request')]);
            }

            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('user/add-access.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /** @noinspection PhpUnusedParameterInspection */
    #[IsGranted('acl/User/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'user')]
    #[Route('/{tenant_url}/user/edit/{username?}', name: 'app_user_edit', methods: ['GET', 'POST'])]
    public function edit(
        string $tenant_url,
        string $username,
        User $user,
        Request $request,
        UserManager $userManager,
        LoggedUser $loggedUser,
        EntityManagerInterface $entityManager,
        MailerInterface $mailer,
    ) {
        $form = $this->createForm(
            UserType::class,
            $user->setActiveTenant($loggedUser->tenant()),
            [
                'action' => $this->generateUrl('app_user_edit', ['username' => $username]),
            ]
        );
        $form->handleRequest($request);

        $response = new JsonResponse();
        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->edit($user);
            $user->getTenant($request);

            $response->setData(UserResultSet::normalize($user, ['index', 'action']));
            $response->headers->add(['X-Asalae-Success' => 'true']);
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('user/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /** @noinspection PhpUnusedParameterInspection */
    #[IsGranted('acl/User/delete')]
    #[IsGranted(EntityIsDeletableVoter::class, 'user')]
    #[IsGranted(BelongsToTenantVoter::class, 'user')]
    #[Route('/{tenant_url}/user/delete/{username?}', name: 'app_user_delete')]
    public function delete(
        string $tenant_url,
        User $user,
        EntityManagerInterface $entityManager
    ): Response {
        $entityManager->remove($user);
        $entityManager->flush();

        $response = new Response();
        $response->setContent('done');
        return $response;
    }

    /** @noinspection PhpUnusedParameterInspection */
    #[IsGranted('acl/User/delete')]
    #[IsGranted(BelongsToTenantVoter::class, 'user')]
    #[Route('/{tenant_url}/user/delete-access/{username?}', name: 'app_user_delete_access')]
    public function deleteAccess(
        User $user,
        string $tenant_url,
        EntityManagerInterface $entityManager,
        Request $request,
    ): Response {
        $user->getTenant($request);
        if (!$user->getDeletableAccess()) {
            throw $this->createAccessDeniedException();
        }

        $entityManager->remove($user->getAccessUser());
        $entityManager->flush();

        $response = new JsonResponse();
        $response->setData(UserResultSet::normalize($user, ['index', 'action']));
        $response->headers->add(['X-Asalae-Success' => 'true']);
        return $response;
    }

    /** @noinspection PhpUnusedParameterInspection */
    #[IsGranted('acl/User/delete')]
    #[IsGranted(BelongsToTenantVoter::class, 'user')]
    #[Route('/{tenant_url}/user/detach/{username}', name: 'app_user_detach')]
    public function detach(
        User $user,
        string $tenant_url,
        EntityManagerInterface $entityManager,
        Request $request,
    ): Response {
        $tenant = $user->getTenant($request);
        if (!$tenant) {
            throw $this->createNotFoundException();
        }
        $user->removeTenant($tenant);
        $entityManager->persist($user);
        $entityManager->flush();

        $response = new JsonResponse();
        $response->setData(UserResultSet::normalize($user, ['index', 'action']));
        $response->headers->add(['X-Asalae-Success' => 'true']);
        return $response;
    }

    #[Route('/{tenant_url}/user/csv', name: 'app_user_csv', methods: ['GET'])]
    #[IsGranted('acl/User/view')]
    public function csv(
        ExportCsv $exportCsv,
        UserResultSet $userResultSet,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        return $exportCsv->fromResultSet($userResultSet);
    }

    /** @noinspection PhpUnusedParameterInspection */
    #[IsGranted('acl/User/add_edit')]
    #[Route(
        '/{tenant_url}/user-creation-request/{page?1}',
        name: 'app_user_creation_request',
        requirements: ['page' => '\d+'],
        methods: ['GET']
    )]
    public function indexUserCreationRequest(
        UserCreationRequestResultSet $resultSet,
        string $tenant_url,
    ): Response {
        return $this->render('admin/user_creation_request/index.html.twig', [
            'table' => $resultSet,
        ]);
    }

    private function sendPasswordInitMail(
        MailerInterface $mailer,
        UserPasswordRenewToken $userPasswordRenewToken,
    ): void {
        $user = $userPasswordRenewToken->getUser();
        $tenant = $user->getActiveTenant();
        $url = $this->generateUrl(
            'public_user_select_password',
            ['token' => $userPasswordRenewToken->getToken()],
            UrlGeneratorInterface::ABSOLUTE_URL,
        );
        $urlAlt = $this->generateUrl(
            'app_tenant_login_username',
            ['tenant_url' => $tenant->getBaseurl(), 'username' => $user->getUsername()],
            UrlGeneratorInterface::ABSOLUTE_URL,
        );
        $configuration = $tenant->getConfiguration();
        $mailPrefix = $configuration?->getMailPrefix() ?: '[refae]';
        $email = (new TemplatedEmail())
            ->from($_ENV['MAILER_FROM'] ?? 'refae@test.fr')
            ->to($user->getEmail())
            ->subject($mailPrefix . 'Création de votre compte')
            ->htmlTemplate('email/user-created-ask-password.html.twig')
            ->context([
                'user' => $user,
                'url' => $url,
                'url_alt' => $urlAlt,
                'tenant' => $tenant,
                'signature' => $configuration?->getMailSignature(),
                'choosePassword' => $user->isUserDefinedPassword() === false,
            ])
        ;
        $mailer->send($email);
    }

    private function sendNewAccessMail(
        MailerInterface $mailer,
        User $user,
    ): void {
        $tenant = $user->getActiveTenant();
        $urlAlt = $this->generateUrl(
            'app_tenant_login_username',
            ['tenant_url' => $tenant->getBaseurl(), 'username' => $user->getUsername()],
            UrlGeneratorInterface::ABSOLUTE_URL,
        );
        $configuration = $tenant->getConfiguration();
        $mailPrefix = $configuration?->getMailPrefix() ?: '[refae]';
        $email = (new TemplatedEmail())
            ->from($_ENV['MAILER_FROM'] ?? 'refae@test.fr')
            ->to($user->getEmail())
            ->subject($mailPrefix . 'Création de votre compte')
            ->htmlTemplate('email/user-created-ask-password.html.twig')
            ->context([
                'user' => $user,
                'url' => false,
                'url_alt' => $urlAlt,
                'tenant' => $tenant,
                'signature' => $configuration?->getMailSignature(),
                'choosePassword' => false,
            ])
        ;
        $mailer->send($email);
    }

    /** @noinspection PhpUnusedParameterInspection */
    #[IsGranted('acl/User/add_edit')]
    #[Route(
        '/{tenant_url}/user-creation-request-deny/{userCreationRequest}',
        name: 'app_user_creation_request_deny',
        methods: ['GET', 'POST']
    )]
    public function indexUserCreationRequestDeny(
        string $tenant_url,
        UserCreationRequest $userCreationRequest,
        Request $request,
        EntityManagerInterface $entityManager,
        MailerInterface $mailer,
    ): Response {
        $form = $this->createForm(
            UserCreationRequestRefusalType::class,
            options: [
                'action' => $this->generateUrl(
                    'app_user_creation_request_deny',
                    ['userCreationRequest' => $userCreationRequest->getId()]
                ),
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $reason = $form->get('reason')->getData();

            $entityManager->remove($userCreationRequest);
            $entityManager->flush();

            $this->sendRefusalEmail($mailer, $userCreationRequest, $reason);
            $this->addFlash(
                'success',
                "Email envoyé avec succès"
            );
            $response->headers->add(['X-Asalae-Redirect' => $this->generateUrl('app_user_creation_request')]);
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('admin/user_creation_request/deny.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    private function sendRefusalEmail(
        MailerInterface $mailer,
        UserCreationRequest $userCreationRequest,
        string $reason,
    ): void {
        $tenant = $userCreationRequest->getTenant();
        $configuration = $tenant?->getConfiguration();
        $mailPrefix = $configuration?->getMailPrefix() ?: '[refae]';
        $email = (new TemplatedEmail())
            ->from($_ENV['MAILER_FROM'] ?? 'refae@test.fr')
            ->to($userCreationRequest->getEmail())
            ->subject($mailPrefix . 'Refus de création de votre utilisateur')
            ->htmlTemplate('email/user-creation-refusal.html.twig')
            ->context([
                'name' => $userCreationRequest->getName(),
                'reason' => $reason,
                'signature' => $configuration?->getMailSignature(),
            ])
        ;
        $mailer->send($email);
    }
}
