<?php

namespace App\Controller\Tenant;

use App\Entity\ManagementRule;
use App\Entity\User;
use App\Entity\VersionableEntityInterface;
use App\Form\ManagementRuleImportConfirmType;
use App\Form\ManagementRuleImportType;
use App\Form\ManagementRuleType;
use App\Form\ManagementRuleVersionType;
use App\Repository\ManagementRuleRepository;
use App\Repository\RuleTypeRepository;
use App\Repository\TenantRepository;
use App\ResultSet\ManagementRuleFileViewResultSet;
use App\ResultSet\ManagementRulePublishedResultSet;
use App\ResultSet\ManagementRuleResultSet;
use App\ResultSet\ManagementRuleResultSetPreview;
use App\ResultSet\PublicManagementRuleResultSet;
use App\Security\Voter\BelongsToTenantVoter;
use App\Security\Voter\EntityIsDeletableVoter;
use App\Security\Voter\EntityIsEditableVoter;
use App\Security\Voter\TenantUrlVoter;
use App\Service\ExportCsv;
use App\Service\ImportZip;
use App\Service\LoggedUser;
use App\Service\Manager\ManagementRuleManager;
use App\Service\SessionFiles;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use IntlDateFormatter;
use Locale;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Workflow\WorkflowInterface;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class ManagementRuleController extends AbstractController
{
    /** @noinspection PhpUnusedParameterInspection **/
    #[IsGranted('acl/ManagementRule/view')]
    #[Route(
        '/{tenant_url}/management-rule/{page?1}',
        name: 'app_management_rule',
        requirements: ['page' => '\d+'],
        methods: ['GET'],
    )]
    public function index(
        ManagementRuleResultSet $table,
        string $tenant_url,
    ): Response {
        return $this->render('management_rule/index.html.twig', [
            'table' => $table,
        ]);
    }

    #[IsGranted('acl/ManagementRule/view')]
    #[Route(
        '/{tenant_url}/management-rule-published/{page?1}',
        name: 'app_management_rule_index_published',
        requirements: ['page' => '\d+'],
        methods: ['GET'],
    )]
    public function indexPublished(
        string $tenant_url,
        ManagementRulePublishedResultSet $table,
    ): Response {
        return $this->render('management_rule/index_published.html.twig', [
            'table' => $table,
            'tenant_url' => $tenant_url,
        ]);
    }

    #[IsGranted('acl/ManagementRule/view')]
    #[IsGranted(BelongsToTenantVoter::class, 'managementRule')]
    #[Route('/{tenant_url}/management-rule/view/{managementRule}', name: 'app_management_rule_view', methods: ['GET'])]
    public function view(
        #[MapEntity(expr: 'repository.findWithPreviousVersions(managementRule)')]
        ManagementRule $managementRule,
        ManagementRuleFileViewResultSet $managementRuleFileResultSet,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $managementRuleFileResultSet->initialize($managementRule);
        return $this->render('management_rule/view.html.twig', [
            'managementRule' => $managementRule,
            'files' => $managementRuleFileResultSet,
        ]);
    }

    /** @noinspection PhpUnusedParameterInspection **/
    #[IsGranted('acl/ManagementRule/add_edit')]
    #[Route(
        '/{tenant_url}/management-rule/add/{session_tmpfile_uuid?}',
        name: 'app_management_rule_add',
        methods: ['GET', 'POST'],
    )]
    public function add(
        Request $request,
        ManagementRuleManager $managementRuleManager,
        RuleTypeRepository $ruleTypeRepository,
        string $tenant_url,
        LoggedUser $loggedUser,
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        $managementRule = $managementRuleManager->newManagementRule($user->getTenant($request));
        if ($request->get('autopublish')) {
            $managementRule->setName($request->get('name'));
            $managementRule->setIdentifier($request->get('name'));
            $managementRule->setPublic(true);
            $ruleType = $ruleTypeRepository->findOneBy(
                ['tenant' => null, 'identifier' => $request->get('autopublish')]
            );
            $managementRule->setRuleType($ruleType);
            if (preg_match('/^(\d+) ?an/', (string) $request->get('name'), $m)) {
                $managementRule->setDuration('P' . $m[1] . 'Y');
            }
        }

        $form = $this->createForm(
            ManagementRuleType::class,
            $managementRule,
            ['action' => $this->generateUrl(
                'app_management_rule_add',
                ['tenant_url' => $tenant_url]
            )]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('autopublish')->getData()) {
                $managementRule->setState(VersionableEntityInterface::S_PUBLISHED);
                $this->addFlash(
                    'success',
                    "La DUA \"{$managementRule->getName()}\" a bien été créé",
                );
                $managementRuleManager->save($managementRule);
                $response->headers->add(
                    [
                        'X-Asalae-Redirect' => $this->generateUrl(
                            'app_document_rule_import_csv3',
                            ['username' => $loggedUser->user()->getUsername()]
                        )
                    ]
                );
                return $response;
            }
            $managementRuleManager->save($managementRule);

            return new JsonResponse(
                ManagementRuleResultSet::normalize($managementRule, ['index', 'action']),
                headers: ['X-Asalae-Success' => 'true']
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('management_rule/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/ManagementRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'managementRule')]
    #[IsGranted(EntityIsEditableVoter::class, 'managementRule')]
    #[Route(
        '/{tenant_url}/management-rule/edit/{managementRule}',
        name: 'app_management_rule_edit',
        methods: ['GET', 'POST'],
    )]
    public function edit(
        Request $request,
        EntityManagerInterface $entityManager,
        ManagementRule $managementRule,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $form = $this->createForm(
            ManagementRuleType::class,
            $managementRule,
            ['action' => $this->generateUrl('app_management_rule_edit', ['managementRule' => $managementRule->getId()])]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($managementRule);
            $entityManager->flush();
            return new JsonResponse(
                ManagementRuleResultSet::normalize($managementRule, ['index', 'action']),
                headers: ['X-Asalae-Success' => 'true']
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('management_rule/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/management-rule/publish/{managementRule}',
        name: 'app_management_rule_publish',
        methods: ['POST'],
    )]
    #[IsGranted('acl/ManagementRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'managementRule')]
    public function publish(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        WorkflowInterface $managementruleStateMachine,
        ManagementRule $managementRule,
    ): Response {
        $managementruleStateMachine->apply($managementRule, VersionableEntityInterface::T_PUBLISH);

        return new JsonResponse(
            ManagementRuleResultSet::normalize($managementRule, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route(
        '/{tenant_url}/management-rule/revoke/{managementRule}',
        name: 'app_management_rule_revoke',
        methods: ['POST']
    )]
    #[IsGranted('acl/ManagementRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'managementRule')]
    public function revoke(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        WorkflowInterface $servicelevelStateMachine,
        ManagementRule $managementRule,
    ): Response {
        $servicelevelStateMachine->apply($managementRule, VersionableEntityInterface::T_REVOKE);

        return new JsonResponse(
            ManagementRuleResultSet::normalize($managementRule, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route(
        '/{tenant_url}/management-rule/restore/{managementRule}',
        name: 'app_management_rule_restore',
        methods: ['POST'],
    )]
    #[IsGranted('acl/ManagementRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'managementRule')]
    public function restore(
        WorkflowInterface $servicelevelStateMachine,
        ManagementRule $managementRule,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $servicelevelStateMachine->apply($managementRule, VersionableEntityInterface::T_RECOVER);

        return new JsonResponse(
            ManagementRuleResultSet::normalize($managementRule, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route(
        '/{tenant_url}/management-rule/activate/{managementRule}',
        name: 'app_management_rule_activate',
        methods: ['POST'],
    )]
    #[IsGranted('acl/ManagementRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'managementRule')]
    public function activate(
        WorkflowInterface $servicelevelStateMachine,
        ManagementRule $managementRule,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $servicelevelStateMachine->apply($managementRule, VersionableEntityInterface::T_ACTIVATE);

        return new JsonResponse(
            ManagementRuleResultSet::normalize($managementRule, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route(
        '/{tenant_url}/management-rule/deactivate/{managementRule}',
        name: 'app_management_rule_deactivate',
        methods: ['POST'],
    )]
    #[IsGranted('acl/ManagementRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'managementRule')]
    public function deactivate(
        WorkflowInterface $servicelevelStateMachine,
        ManagementRule $managementRule,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $servicelevelStateMachine->apply($managementRule, VersionableEntityInterface::T_DEACTIVATE);

        return new JsonResponse(
            ManagementRuleResultSet::normalize($managementRule, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route(
        '/{tenant_url}/management-rule/new-version/{managementRule}',
        name: 'app_management_rule_new_version',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/ManagementRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'managementRule')]
    public function newVersion(
        Request $request,
        ManagementRuleManager $managementRuleManager,
        ManagementRule $managementRule,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $form = $this->createForm(
            ManagementRuleVersionType::class,
            options: [
                'action' => $this->generateUrl(
                    'app_management_rule_new_version',
                    ['managementRule' => $managementRule->getId()]
                )
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $newVersion = $managementRuleManager->newVersion($managementRule, $form->get('reason')->getData());

            return new JsonResponse(
                ManagementRuleResultSet::normalize($newVersion, ['index', 'action']),
                headers: [
                    'X-Asalae-Success' => 'true',
                    'X-Asalae-PreviousVersionId' => $managementRule->getId(),
                ]
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('management_rule/version.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/management-rule/delete/{managementRule}',
        name: 'app_management_rule_delete',
        methods: ['DELETE'],
    )]
    #[IsGranted('acl/ManagementRule/delete')]
    #[IsGranted(EntityIsDeletableVoter::class, 'managementRule')]
    #[IsGranted(BelongsToTenantVoter::class, 'managementRule')]
    public function delete(
        EntityManagerInterface $entityManager,
        ManagementRule $managementRule,
        ManagementRuleRepository $managementRuleRepository,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $entityManager->remove($managementRule);
        $entityManager->flush();

        if ($previousVersion = $managementRuleRepository->findPreviousVersion($managementRule)) {
            return new JsonResponse(
                ManagementRuleResultSet::normalize($previousVersion, ['index', 'action']),
                headers: [
                    'X-Asalae-Success' => 'true',
                    'X-Asalae-PreviousVersionEntity' => 'true',
                ]
            );
        }

        return new Response('done');
    }

    #[Route('/{tenant_url}/management-rule/export', name: 'app_management_rule_export', methods: ['GET'])]
    #[IsGranted('acl/ManagementRule/export')]
    public function export(
        ManagementRuleRepository $managementRuleRepository,
        ManagementRuleManager $managementRuleManager,
        ManagementRuleResultSet $managementRuleResultSet,
        Request $request,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        $tenant = $user->getTenant($request);
        $query = $managementRuleRepository->queryForExport($tenant);
        $managementRuleResultSet->appendFilters($query);

        $zip = $managementRuleManager->export($query);
        $filename = sprintf(
            '%s_export.%s.refae',
            (new DateTime())->format('Y-m-d_His'),
            $managementRuleManager->getFileName()
        );
        $headers = [
            'Content-Description' => 'File Transfer',
            'Content-Type' => 'application/zip',
            'Content-Length' => strlen($zip),
            'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
            'Expires' => '0',
            'Cache-Control' => 'must-revalidate',
        ];
        return new Response($zip, Response::HTTP_OK, $headers);
    }

    #[Route(
        '/{tenant_url}/management-rule/import',
        name: 'app_management_rule_import1',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/ManagementRule/import')]
    public function import1(
        string $tenant_url,
        Request $request,
        SessionFiles $sessionFiles,
    ): Response {
        $form = $this->createForm(
            ManagementRuleImportType::class,
            options: ['action' => $this->generateUrl('app_management_rule_import1', ['tenant_url' => $tenant_url])]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $session_tmpfile_uuid = $request->get('management_rule_import')['file'] ?? null;
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);

            if (mime_content_type($uri) === 'application/zip') {
                $modalParams = [
                    'btnAcceptContent' => '<i class="fa fa-upload fa-space" aria-hidden="true"></i>'
                        . "<span>Importer</span>",
                    'btnCancelContent' => '<i class="fa fa-trash fa-space" aria-hidden="true"></i>'
                        . "<span>Annuler l'import</span>",
                    'btnCancelUrl' => $this->generateUrl(
                        'app_management_rule_delete_import',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ];
                $response->headers->add([
                    'X-Asalae-Modal-Params' => json_encode($modalParams),
                    'X-Asalae-Step-Title' => json_encode("Récapitulatif avant import"),
                    'X-Asalae-Step' => $this->generateUrl(
                        'app_management_rule_import2',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ]);
                return $response;
            } else {
                $form->get('file')
                    ->addError(new FormError("Ce fichier ne semble pas contenir des règles de gestion"));
            }
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('management_rule/import1.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/management-rule/import/{session_tmpfile_uuid}',
        name: 'app_management_rule_import2',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/ManagementRule/import')]
    public function import2(
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        Request $request,
        string $session_tmpfile_uuid,
        ManagementRuleManager $managementRuleManager,
        ManagementRuleResultSetPreview $previewResultset,
        ImportZip $importZip,
    ): Response {
        $importZip->extract($session_tmpfile_uuid, $managementRuleManager);
        $previewResultset->setDataFromArray($importZip->previewData);

        $form = $this->createForm(
            ManagementRuleImportConfirmType::class,
            null,
            [
                'action' => $this->generateUrl(
                    'app_management_rule_import2',
                    ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                )
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $managementRuleManager->import($importZip);
            $this->addFlash(
                'success',
                $importZip->importableCount > 1
                    ? "$importZip->importableCount règles de gestion importées avec succès"
                    : "$importZip->importableCount règle de gestion importée avec succès"
            );
            $response->headers->add(['X-Asalae-Redirect' => $this->generateUrl('app_management_rule')]);
            return $response;
        }

        $formatter = new IntlDateFormatter(
            Locale::getDefault(),
            IntlDateFormatter::FULL,
            IntlDateFormatter::SHORT
        );
        return $this->render('management_rule/import2.html.twig', [
            'view_data' => [
                "Date du fichier d'export" => $formatter->format($importZip->exportDate),
                "Nombre de règles de gestion" => $importZip->dataCount,
                "Nombre de fichiers liés" => $importZip->fileCount,
                "Nombre d'étiquettes à importer" => count($importZip->labels),
            ],
            'content_preview_resultset' => $previewResultset,
            'summerize_data' => [
                "Nombre de règles de gestion impossibles à importer" => $importZip->failedCount,
                "Total importable" => $importZip->importableCount,
            ],
            'errors' => $importZip->errors,
            'fatalError' => $importZip->hasFatalError,
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/management-rule/delete-import/{session_tmpfile_uuid}',
        name: 'app_management_rule_delete_import',
        methods: ['DELETE'],
    )]
    #[IsGranted('acl/ManagementRule/import')]
    public function deleteImport(
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        SessionFiles $sessionFiles,
        string $session_tmpfile_uuid,
    ): Response {
        $sessionFiles->delete($session_tmpfile_uuid);
        $response = new Response();
        $response->setContent('ok');
        $response->headers->add(['X-Asalae-Success' => 'true']);
        return $response;
    }

    #[Route('/{tenant_url}/management-rule/csv', name: 'app_management_rule_csv', methods: ['GET'])]
    #[IsGranted('acl/ManagementRule/view')]
    public function csv(
        ExportCsv $exportCsv,
        Request $request,
        ManagementRuleResultSet $managementRuleResultSet,
        ManagementRulePublishedResultSet $managementRulePublishedResultSet,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        return $exportCsv->fromResultSet(
            $request->get('index_published')
                ? $managementRulePublishedResultSet
                : $managementRuleResultSet
        );
    }

    #[Route(
        '/{tenant_url}/reader/management-rule/{page?1}',
        name: 'app_reader_management_rule',
        requirements: ['page' => '\d+']
    )]
    public function readerManagementRule(
        PublicManagementRuleResultSet $table,
        TenantRepository $tenantRepository,
        string $tenant_url = null,
    ): Response {
        $tenant = $tenant_url ? $tenantRepository->findOneByBaseurl($tenant_url) : null;
        $table->initialize($tenant);
        return $this->render('reader/management-rule.html.twig', [
            'table' => $table,
        ]);
    }

    #[Route('/{tenant_url}/reader/management-rule/csv', name: 'app_reader_management_rule_csv', methods: ['GET'])]
    public function readerCsv(
        PublicManagementRuleResultSet $publicManagementRuleResultSet,
        ExportCsv $exportCsv,
        TenantRepository $tenantRepository,
        string $tenant_url = null,
    ): Response {
        $tenant = $tenant_url ? $tenantRepository->findOneByBaseurl($tenant_url) : null;
        $publicManagementRuleResultSet->initialize($tenant);
        return $exportCsv->fromResultSet($publicManagementRuleResultSet);
    }
}
