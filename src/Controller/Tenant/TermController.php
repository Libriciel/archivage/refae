<?php

namespace App\Controller\Tenant;

use App\Entity\Term;
use App\Entity\Vocabulary;
use App\Form\TermImportCsvColumnsType;
use App\Form\TermImportCsvType;
use App\Form\TermType;
use App\Repository\TenantRepository;
use App\ResultSet\PublicTermResultSet;
use App\ResultSet\TermResultSet;
use App\ResultSet\TermResultSetPreview;
use App\Security\Voter\BelongsToTenantVoter;
use App\Security\Voter\EntityIsEditableVoter;
use App\Security\Voter\TenantUrlVoter;
use App\Service\Csv;
use App\Service\Manager\TermManager;
use App\Service\SessionFiles;
use Doctrine\ORM\EntityManagerInterface;
use DOMDocument;
use DOMXPath;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class TermController extends AbstractController
{
    #[IsGranted('acl/Vocabulary/view')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    #[Route(
        '/{tenant_url}/vocabulary/{vocabulary}/term/{page?1}',
        name: 'app_term',
        requirements: ['page' => '\d+'],
        methods: ['GET']
    )]
    public function index(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        Vocabulary $vocabulary,
        TermResultSet $resultSet,
    ): Response {
        $resultSet->setVocabulary($vocabulary);

        return $this->render('term/index.html.twig', [
            'table' => $resultSet,
            'vocabulary' => $vocabulary,
        ]);
    }

    #[IsGranted('acl/Vocabulary/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    #[IsGranted(EntityIsEditableVoter::class, 'vocabulary')]
    #[Route(
        '/{tenant_url}/vocabulary/{vocabulary}/edit/term/{page?1}',
        name: 'app_term_index_edit',
        requirements: ['page' => '\d+'],
        methods: ['GET']
    )]
    public function indexEdit(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        Vocabulary $vocabulary,
        TermResultSet $resultSet,
    ): Response {
        $resultSet->setVocabulary($vocabulary);

        return $this->render('term/index_edit.html.twig', [
            'table' => $resultSet,
            'vocabulary' => $vocabulary,
        ]);
    }

    #[IsGranted('acl/Vocabulary/view')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    #[Route(
        '/{tenant_url}/reader/vocabulary/{vocabulary}/term/{page?1}',
        name: 'app_reader_term',
        requirements: ['page' => '\d+'],
        methods: ['GET']
    )]
    public function readerTerm(
        PublicTermResultSet $table,
        TenantRepository $tenantRepository,
        Vocabulary $vocabulary,
        string $tenant_url = null,
    ): Response {
        $tenant = $tenant_url ? $tenantRepository->findOneByBaseurl($tenant_url) : null;
        $table->initialize($tenant);
        $table->setVocabulary($vocabulary);
        return $this->render('reader/term.html.twig', [
            'table' => $table,
            'vocabulary' => $vocabulary,
        ]);
    }

    #[IsGranted('acl/Vocabulary/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    #[IsGranted(EntityIsEditableVoter::class, 'vocabulary')]
    #[Route(
        '/{tenant_url}/vocabulary/{vocabulary}/term/add',
        name: 'app_term_add',
        methods: ['GET', 'POST']
    )]
    public function add(
        string $tenant_url,
        Vocabulary $vocabulary,
        Request $request,
        EntityManagerInterface $entityManager,
    ): Response {
        $term = new Term(vocabulary: $vocabulary);
        $form = $this->createForm(
            TermType::class,
            $term,
            [
                'action' => $this->generateUrl(
                    'app_term_add',
                    ['tenant_url' => $tenant_url, 'vocabulary' => $vocabulary->getId()]
                ),
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($term);
            $entityManager->flush();

            return new JsonResponse(
                TermResultSet::normalize($term, ['index', 'action']),
                headers: ['X-Asalae-Success' => 'true']
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('term/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/Vocabulary/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    #[IsGranted(EntityIsEditableVoter::class, 'vocabulary')]
    #[Route(
        '/{tenant_url}/vocabulary/{vocabulary}/term/edit/{term}',
        name: 'app_term_edit',
        methods: ['GET', 'POST']
    )]
    public function edit(
        string $tenant_url,
        Vocabulary $vocabulary,
        Term $term,
        Request $request,
        EntityManagerInterface $entityManager,
    ): Response {
        $form = $this->createForm(
            TermType::class,
            $term,
            [
                'action' => $this->generateUrl('app_term_edit', [
                    'tenant_url' => $tenant_url,
                    'vocabulary' => $vocabulary->getId(),
                    'term' => $term->getId(),
                ]),
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($term);
            $entityManager->flush();

            return new JsonResponse(
                TermResultSet::normalize($term, ['index', 'action']),
                headers: ['X-Asalae-Success' => 'true']
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('term/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/Vocabulary/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    #[IsGranted(EntityIsEditableVoter::class, 'vocabulary')]
    #[Route(
        '/{tenant_url}/vocabulary/{vocabulary}/term/delete/{term}',
        name: 'app_term_delete',
        methods: ['DELETE']
    )]
    public function delete(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        Vocabulary $vocabulary,
        Term $term,
        EntityManagerInterface $em,
    ): Response {
        $em->remove($term);
        $em->flush();

        return new Response('done');
    }

    #[IsGranted('acl/Vocabulary/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    #[IsGranted(EntityIsEditableVoter::class, 'vocabulary')]
    #[Route(
        '/{tenant_url}/vocabulary/{vocabulary}/term/import-csv',
        name: 'app_term_import_csv1',
        methods: ['GET', 'POST']
    )]
    public function importCsv1(
        string $tenant_url,
        Vocabulary $vocabulary,
        Request $request,
    ): Response {
        $form = $this->createForm(
            TermImportCsvType::class,
            options: ['action' => $this->generateUrl(
                'app_term_import_csv1',
                ['tenant_url' => $tenant_url, 'vocabulary' => $vocabulary->getId()]
            )]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $session_tmpfile_uuid = $request->get('term_import_csv')['file'] ?? null;
            $modalParams = [
                'btnAcceptContent' => '<i class="fa fa-upload fa-space" aria-hidden="true"></i>'
                    . "<span>Importer</span>",
                'btnCancelContent' => '<i class="fa fa-trash fa-space" aria-hidden="true"></i>'
                    . "<span>Annuler l'import</span>",
                'btnCancelUrl' => $this->generateUrl(
                    'app_term_delete_import',
                    [
                        'tenant_url' => $tenant_url,
                        'vocabulary' => $vocabulary->getId(),
                        'session_tmpfile_uuid' => $session_tmpfile_uuid,
                    ],
                ),
            ];
            $response->headers->add([
                'X-Asalae-Modal-Params' => json_encode($modalParams),
                'X-Asalae-Step-Title' => json_encode("Récapitulatif avant import"),
                'X-Asalae-Step' => $this->generateUrl(
                    'app_term_import_csv2',
                    [
                        'tenant_url' => $tenant_url,
                        'vocabulary' => $vocabulary->getId(),
                        'session_tmpfile_uuid' => $session_tmpfile_uuid,
                    ],
                ),
            ]);
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('term/import_csv1.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/vocabulary/{vocabulary}/term/import-csv2/{session_tmpfile_uuid}',
        name: 'app_term_import_csv2',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/DocumentRule/import')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    #[IsGranted(EntityIsEditableVoter::class, 'vocabulary')]
    public function importCsv2(
        string $tenant_url,
        Vocabulary $vocabulary,
        Request $request,
        string $session_tmpfile_uuid,
        TermManager $termManager,
        TermResultSetPreview $previewResultset,
        SessionFiles $sessionFiles,
    ): Response {
        $handle = fopen($sessionFiles->getUri($session_tmpfile_uuid), 'r');

        $previousValue = libxml_use_internal_errors(true);
        $dom = new DOMDocument();
        $type = $dom->load($sessionFiles->getUri($session_tmpfile_uuid)) ? 'skos' : 'csv';
        libxml_clear_errors();
        libxml_use_internal_errors($previousValue);

        $errors = [];
        $fatalError = false;

        $previewResultset->initialize(['type' => $type]);

        if ($type === 'csv') {
            $csvContent = '';
            for ($i = 0; $i < 10; $i++) {
                $csvContent .= fgets($handle);
            }
            fclose($handle);
            $csv = new Csv($csvContent);
            $previewResultset->setDataFromArray($csv->getData());
            if (count($csv->getData()) <= 1) {
                $fatalError = true;
                $errors['fatal'] = "Ce fichier ne semble pas être un fichier CSV valide";
            }
        } else {
            $xpath = new DOMXPath($dom);
            $skosNs = $dom->documentElement->getAttribute('xmlns:skos');
            if ($skosNs) {
                $xpath->registerNamespace('skos', $skosNs);
                $query = $xpath->query('*[count(skos:prefLabel) >= 1][position() <= 10]');
                if ($query->count() === 0) {
                    $fatalError = true;
                    $errors['fatal'] = "Aucun mot clé n'a été trouvé dans le document";
                }
                $skosData = [];
                foreach ($query as $conceptNode) {
                    $label = $termManager->getSkosLabel($xpath, $conceptNode);
                    if ($label) {
                        $skosData[] = $label;
                    }
                }
                $previewResultset->setDataFromArray($skosData);
            } else {
                $fatalError = true;
                $errors['fatal'] = "Ce fichier ne semble pas être un fichier Skos valide";
            }
        }

        $form = $this->createForm(
            TermImportCsvColumnsType::class,
            null,
            [
                'columns' => $previewResultset->fields(),
                'action' => $this->generateUrl(
                    'app_term_import_csv2',
                    [
                        'tenant_url' => $tenant_url,
                        'session_tmpfile_uuid' => $session_tmpfile_uuid,
                        'vocabulary' => $vocabulary->getId(),
                    ],
                ),
                'type' => $type,
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            if ($type === 'csv') {
                $imported = $termManager->importCsv(
                    $vocabulary,
                    $sessionFiles->getUri($session_tmpfile_uuid),
                    $form->getData(),
                );
            } else {
                $imported = $termManager->importSkos($vocabulary, $dom);
            }
            $this->addFlash(
                'success',
                "$imported termes importés avec succès"
            );
            $sessionFiles->delete($session_tmpfile_uuid);
            $response->headers->add(
                [
                    'X-Asalae-Redirect' => $this->generateUrl(
                        'app_term_index_edit',
                        ['tenant_url' => $tenant_url, 'vocabulary' => $vocabulary->getId()]
                    ),
                ]
            );
            return $response;
        }

        return $this->render('term/import_csv2.html.twig', [
            'content_preview_resultset' => $previewResultset,
            'form' => $form->createView(),
            'fatalError' => $fatalError,
            'errors' => $errors,
            'fileType' => $type
        ], $response);
    }

    #[Route(
        '/{tenant_url}/vocabulary/{vocabulary}/term/delete-import/{session_tmpfile_uuid}',
        name: 'app_term_delete_import',
        methods: ['DELETE'],
    )]
    #[IsGranted('acl/Vocabulary/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    #[IsGranted(EntityIsEditableVoter::class, 'vocabulary')]
    public function deleteImport(
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        Vocabulary $vocabulary,
        string $session_tmpfile_uuid,
        SessionFiles $sessionFiles,
    ): Response {
        $sessionFiles->delete($session_tmpfile_uuid);
        $response = new Response();
        $response->setContent('ok');
        $response->headers->add(['X-Asalae-Success' => 'true']);
        return $response;
    }
}
