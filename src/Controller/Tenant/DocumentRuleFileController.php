<?php

namespace App\Controller\Tenant;

use App\Controller\DownloadFileTrait;
use App\Entity\DocumentRuleFile;
use App\Entity\DocumentRule;
use App\Form\DocumentRuleFileType;
use App\ResultSet\DocumentRuleFileResultSet;
use App\Security\Voter\BelongsToTenantVoter;
use App\Security\Voter\EntityIsEditableVoter;
use App\Security\Voter\TenantUrlVoter;
use App\Service\Manager\FileManager;
use App\Service\SessionFiles;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class DocumentRuleFileController extends AbstractController
{
    use DownloadFileTrait;

    #[IsGranted('acl/DocumentRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'documentRule')]
    #[IsGranted(EntityIsEditableVoter::class, 'documentRule')]
    #[Route(
        '/{tenant_url}/document-rule/{documentRule}/file/{page?1}',
        name: 'app_document_rule_file_index',
        requirements: ['page' => '\d+'],
        methods: ['GET'],
    )]
    public function index(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        DocumentRule $documentRule,
        DocumentRuleFileResultSet $resultSet,
    ): Response {
        $resultSet->initialize($documentRule);
        return $this->render('document_rule_file/index.html.twig', [
            'table' => $resultSet,
            'documentRule' => $documentRule,
        ]);
    }

    #[IsGranted('acl/DocumentRule/view')]
    #[Route(
        '/{tenant_url}/document-rule-file/{documentRuleFile}/download',
        name: 'app_document_rule_file_download',
        methods: ['GET']
    )]
    #[IsGranted(BelongsToTenantVoter::class, 'documentRuleFile')]
    public function download(
        DocumentRuleFile $documentRuleFile,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        return $this->getFileDownloadResponse($documentRuleFile->getFile());
    }

    #[IsGranted('acl/DocumentRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'documentRule')]
    #[IsGranted(EntityIsEditableVoter::class, 'documentRule')]
    #[Route(
        '/{tenant_url}/document-rule/{documentRule}/file/add',
        name: 'app_document_rule_file_add',
        methods: ['GET', 'POST'],
    )]
    public function add(
        DocumentRule $documentRule,
        Request $request,
        EntityManagerInterface $entityManager,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        SessionFiles $sessionFiles,
    ): Response {
        $documentRuleFile = new DocumentRuleFile();
        $documentRuleFile->setDocumentRule($documentRule);
        $documentRuleFile->setType(DocumentRuleFile::TYPE_OTHER);
        $form = $this->createForm(
            DocumentRuleFileType::class,
            $documentRuleFile,
            ['action' => $this->generateUrl(
                'app_document_rule_file_add',
                ['tenant_url' => $tenant_url, 'documentRule' => $documentRule->getId()]
            )]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $session_tmpfile_uuid = $request->get('document_rule_file')['file'] ?? null;
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);
            $file = FileManager::setDataFromPath($uri);
            $documentRuleFile->setFile($file);
            $entityManager->persist($documentRuleFile);

            $entityManager->persist($file);
            $entityManager->flush();

            $response = new JsonResponse(DocumentRuleFileResultSet::normalize($documentRuleFile, ['index', 'action']));
            $response->headers->add(['X-Asalae-Success' => 'true']);
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('document_rule_file/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/DocumentRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'documentRule')]
    #[IsGranted(EntityIsEditableVoter::class, 'documentRule')]
    #[Route(
        '/{tenant_url}/document-rule/{documentRule}/file/edit/{documentRuleFile}',
        name: 'app_document_rule_file_edit',
        methods: ['GET', 'POST'],
    )]
    public function edit(
        Request $request,
        EntityManagerInterface $entityManager,
        DocumentRule $documentRule,
        DocumentRuleFile $documentRuleFile,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $form = $this->createForm(
            DocumentRuleFileType::class,
            $documentRuleFile,
            [
                'action' => $this->generateUrl('app_document_rule_file_edit', [
                    'tenant_url' => $tenant_url,
                    'documentRule' => $documentRule->getId(),
                    'documentRuleFile' => $documentRuleFile->getId(),
                ]),
                'withFileUpload' => false,
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($documentRuleFile);
            $entityManager->flush();

            $response = new JsonResponse(DocumentRuleFileResultSet::normalize($documentRuleFile, ['index', 'action']));
            $response->headers->add(['X-Asalae-Success' => 'true']);

            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('document_rule_file/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/DocumentRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'documentRule')]
    #[IsGranted(EntityIsEditableVoter::class, 'documentRule')]
    #[Route(
        '/{tenant_url}/document-rule/{documentRule}/file/delete/{documentRuleFile}',
        name: 'app_document_rule_file_delete',
        methods: ['DELETE'],
    )]
    public function delete(
        DocumentRuleFile $documentRuleFile,
        EntityManagerInterface $entityManager,
        /** @noinspection PhpUnusedParameterInspection **/
        DocumentRule $documentRule,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $entityManager->remove($documentRuleFile->getFile());
        $entityManager->flush();

        return new Response('done');
    }
}
