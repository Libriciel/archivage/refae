<?php

/** @noinspection PhpUnusedParameterInspection */

namespace App\Controller\Tenant;

use App\Repository\TableRepository;
use App\Security\Voter\TenantUrlVoter;
use App\Service\LoggedUser;
use App\Service\Manager\ActivityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class HomeController extends AbstractController
{
    public function __construct(private readonly TableRepository $table)
    {
    }

    #[Route('/{tenant_url}/home', name: 'app_home', requirements: ['page' => '\d+'])]
    #[IsGranted('acl/Home/view')]
    public function index(
        string $tenant_url,
        ActivityManager $activityManager,
        LoggedUser $loggedUser,
        EntityManagerInterface $manager,
    ): Response {
        $chartData = $activityManager->getChartData($loggedUser->tenant());
        return $this->render('home/index.html.twig', [
            'chartData' => $chartData,
        ]);
    }

    #[Route('/{tenant_url}/home/view/{id?}', name: 'app_home_view')]
    #[IsGranted('acl/Home/view')]
    public function view(int $id, string $tenant_url): Response
    {
        return $this->render('home/view.html.twig', ['id' => $id]);
    }

    #[Route('/{tenant_url}/home/save-table', name: 'app_home_save_table')]
    #[IsGranted('acl/Home/view')]
    public function saveTable(Request $request, string $tenant_url): Response
    {
        $data = json_decode($request->getContent(), true);
        $this->table->saveMemoryData($data);
        return new Response('OK');
    }
}
