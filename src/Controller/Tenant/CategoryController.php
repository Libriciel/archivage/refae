<?php

namespace App\Controller\Tenant;

use App\Entity\Category;
use App\Entity\User;
use App\Entity\VersionableEntityInterface;
use App\Form\CategoryImportConfirmType;
use App\Form\CategoryImportType;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use App\ResultSet\CategoryResultSet;
use App\ResultSet\CategoryResultSetPreview;
use App\Security\Voter\BelongsToTenantVoter;
use App\Security\Voter\EntityIsDeletableVoter;
use App\Security\Voter\EntityIsEditableVoter;
use App\Security\Voter\TenantUrlVoter;
use App\Service\ExportCsv;
use App\Service\ImportZip;
use App\Service\Manager\CategoryManager;
use App\Service\SessionFiles;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use IntlDateFormatter;
use Locale;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Workflow\WorkflowInterface;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class CategoryController extends AbstractController
{
    #[IsGranted('acl/Category/view')]
    #[Route(
        '/{tenant_url}/category/{page?1}',
        name: 'app_category',
        requirements: ['page' => '\d+'],
        methods: ['GET'],
    )]
    public function index(
        CategoryResultSet $categories,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        return $this->render('category/index.html.twig', [
            'table' => $categories,
        ]);
    }

    #[IsGranted('acl/Category/view')]
    #[IsGranted(BelongsToTenantVoter::class, 'category')]
    #[Route('/{tenant_url}/category/view/{category?}', name: 'app_category_view', methods: ['GET'])]
    public function view(
        Category $category,
        CategoryRepository $categoryRepository,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $category->setPath($categoryRepository->getPathAsString($category, ['separator' => ' ⮕ ']));
        return $this->render('category/view.html.twig', [
            'category' => $category,
        ]);
    }

    #[IsGranted('acl/Category/add_edit')]
    #[Route('/{tenant_url}/category/add/{session_tmpfile_uuid?}', name: 'app_category_add', methods: ['GET', 'POST'])]
    public function add(
        Request $request,
        CategoryManager $categoryManager,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        $category = $categoryManager->newCategory($user->getTenant($request));

        $form = $this->createForm(
            CategoryType::class,
            $category,
            ['action' => $this->generateUrl(
                'app_category_add',
                ['tenant_url' => $tenant_url]
            )]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $categoryManager->save($category);

            return new JsonResponse(
                CategoryResultSet::normalize($category, ['index', 'action']),
                headers: ['X-Asalae-Success' => 'true']
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('category/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/Category/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'entity')]
    #[IsGranted(EntityIsEditableVoter::class, 'entity')]
    #[Route('/{tenant_url}/category/edit/{entity?}', name: 'app_category_edit', methods: ['GET', 'POST'])]
    public function edit(
        Request $request,
        CategoryManager $categoryManager,
        Category $entity,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $form = $this->createForm(
            CategoryType::class,
            $entity,
            ['action' => $this->generateUrl('app_category_edit', ['entity' => $entity->getId()])]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            if (!$request->get('category')['parent']) {
                $entity->setParent(null);
            }

            $categoryManager->save($entity);
            return new JsonResponse(
                CategoryResultSet::normalize($entity, ['index', 'action']),
                headers: ['X-Asalae-Success' => 'true']
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('category/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/{tenant_url}/category/publish/{category?}', name: 'app_category_publish', methods: ['POST'])]
    #[IsGranted('acl/Category/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'category')]
    public function publish(
        WorkflowInterface $categoryStateMachine,
        Category $category,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $categoryStateMachine->apply($category, VersionableEntityInterface::T_PUBLISH);

        return new JsonResponse(
            CategoryResultSet::normalize($category, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/category/revoke/{category?}', name: 'app_category_revoke', methods: ['POST'])]
    #[IsGranted('acl/Category/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'category')]
    public function revoke(
        WorkflowInterface $categoryStateMachine,
        Category $category,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $categoryStateMachine->apply($category, VersionableEntityInterface::T_REVOKE);

        return new JsonResponse(
            CategoryResultSet::normalize($category, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/category/restore/{category?}', name: 'app_category_restore', methods: ['POST'])]
    #[IsGranted('acl/Category/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'category')]
    public function restore(
        WorkflowInterface $categoryStateMachine,
        Category $category,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $categoryStateMachine->apply($category, VersionableEntityInterface::T_RECOVER);

        return new JsonResponse(
            CategoryResultSet::normalize($category, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/category/activate/{category?}', name: 'app_category_activate', methods: ['POST'])]
    #[IsGranted('acl/Category/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'category')]
    public function activate(
        WorkflowInterface $categoryStateMachine,
        Category $category,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $categoryStateMachine->apply($category, VersionableEntityInterface::T_ACTIVATE);

        return new JsonResponse(
            CategoryResultSet::normalize($category, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/category/deactivate/{category?}', name: 'app_category_deactivate', methods: ['POST'])]
    #[IsGranted('acl/Category/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'category')]
    public function deactivate(
        WorkflowInterface $categoryStateMachine,
        Category $category,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $categoryStateMachine->apply($category, VersionableEntityInterface::T_DEACTIVATE);

        return new JsonResponse(
            CategoryResultSet::normalize($category, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/category/delete/{category?}', name: 'app_category_delete', methods: ['DELETE'])]
    #[IsGranted('acl/Category/delete')]
    #[IsGranted(EntityIsDeletableVoter::class, 'category')]
    #[IsGranted(BelongsToTenantVoter::class, 'category')]
    public function delete(
        EntityManagerInterface $entityManager,
        Category $category,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $entityManager->remove($category);
        $entityManager->flush();

        return new Response('done');
    }

    #[Route('/{tenant_url}/category/export', name: 'app_category_export', methods: ['GET'])]
    #[IsGranted('acl/Category/export')]
    public function export(
        CategoryRepository $categoryRepository,
        CategoryManager $categoryManager,
        CategoryResultSet $categoryResultSet,
        Request $request,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        $tenant = $user->getTenant($request);
        $query = $categoryRepository->queryForExport($tenant);
        $categoryResultSet->appendFilters($query);

        $zip = $categoryManager->export($query);
        $filename = sprintf(
            '%s_export.%s.refae',
            (new DateTime())->format('Y-m-d_His'),
            $categoryManager->getFileName()
        );
        $headers = [
            'Content-Description' => 'File Transfer',
            'Content-Type' => 'application/zip',
            'Content-Length' => strlen($zip),
            'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
            'Expires' => '0',
            'Cache-Control' => 'must-revalidate',
        ];
        return new Response($zip, Response::HTTP_OK, $headers);
    }

    #[Route(
        '/{tenant_url}/category/import',
        name: 'app_category_import1',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/Category/import')]
    public function import1(
        string $tenant_url,
        Request $request,
        SessionFiles $sessionFiles,
    ): Response {
        $form = $this->createForm(
            CategoryImportType::class,
            options: ['action' => $this->generateUrl('app_category_import1', ['tenant_url' => $tenant_url])]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $session_tmpfile_uuid = $request->get('category_import')['file'] ?? null;
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);

            if (mime_content_type($uri) === 'application/zip') {
                $modalParams = [
                    'btnAcceptContent' => '<i class="fa fa-upload fa-space" aria-hidden="true"></i>'
                        . "<span>Importer</span>",
                    'btnCancelContent' => '<i class="fa fa-trash fa-space" aria-hidden="true"></i>'
                        . "<span>Annuler l'import</span>",
                    'btnCancelUrl' => $this->generateUrl(
                        'app_category_delete_import',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ];
                $response->headers->add([
                    'X-Asalae-Modal-Params' => json_encode($modalParams),
                    'X-Asalae-Step-Title' => json_encode("Récapitulatif avant import"),
                    'X-Asalae-Step' => $this->generateUrl(
                        'app_category_import2',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ]);
                return $response;
            } else {
                $form->get('file')
                    ->addError(new FormError("Ce fichier ne semble pas contenir des catégories"));
            }
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('category/import1.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/category/import/{session_tmpfile_uuid}',
        name: 'app_category_import2',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/Category/import')]
    public function import2(
        Request $request,
        string $session_tmpfile_uuid,
        CategoryManager $categoryManager,
        CategoryResultSetPreview $previewResultset,
        ImportZip $importZip,
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
    ): Response {
        $importZip->extract($session_tmpfile_uuid, $categoryManager);
        $previewResultset->setDataFromArray($importZip->previewData);

        $form = $this->createForm(
            CategoryImportConfirmType::class,
            null,
            [
                'action' => $this->generateUrl(
                    'app_category_import2',
                    ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                )
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $categoryManager->import($importZip);
            $this->addFlash(
                'success',
                $importZip->importableCount > 1
                    ? "$importZip->importableCount catégories importées avec succès"
                    : "$importZip->importableCount catégorie importée avec succès"
            );
            $response->headers->add(['X-Asalae-Redirect' => $this->generateUrl('app_category')]);
            return $response;
        }

        $formatter = new IntlDateFormatter(
            Locale::getDefault(),
            IntlDateFormatter::FULL,
            IntlDateFormatter::SHORT
        );
        return $this->render('category/import2.html.twig', [
            'view_data' => [
                "Date du fichier d'export" => $formatter->format($importZip->exportDate),
                "Nombre de catégories" => $importZip->dataCount,
            ],
            'content_preview_resultset' => $previewResultset,
            'summerize_data' => [
                "Nombre de catégories impossibles à importer" => $importZip->failedCount,
                "Total importable" => $importZip->importableCount,
            ],
            'errors' => $importZip->errors,
            'fatalError' => $importZip->hasFatalError,
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/category/delete-import/{session_tmpfile_uuid}',
        name: 'app_category_delete_import',
        methods: ['DELETE'],
    )]
    #[IsGranted('acl/Category/import')]
    public function deleteImport(
        string $session_tmpfile_uuid,
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        SessionFiles $sessionFiles,
    ): Response {
        $sessionFiles->delete($session_tmpfile_uuid);
        $response = new Response();
        $response->setContent('ok');
        $response->headers->add(['X-Asalae-Success' => 'true']);
        return $response;
    }

    #[Route('/{tenant_url}/category/csv', name: 'app_category_csv', methods: ['GET'])]
    #[IsGranted('acl/Category/view')]
    public function csv(
        ExportCsv $exportCsv,
        CategoryResultSet $categoryResultSet,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        return $exportCsv->fromResultSet($categoryResultSet);
    }
}
