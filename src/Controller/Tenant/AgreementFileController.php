<?php

namespace App\Controller\Tenant;

use App\Controller\DownloadFileTrait;
use App\Entity\AgreementFile;
use App\Entity\Agreement;
use App\Form\AgreementFileType;
use App\ResultSet\AgreementFileResultSet;
use App\Security\Voter\BelongsToTenantVoter;
use App\Security\Voter\EntityIsEditableVoter;
use App\Security\Voter\TenantUrlVoter;
use App\Service\Manager\FileManager;
use App\Service\SessionFiles;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class AgreementFileController extends AbstractController
{
    use DownloadFileTrait;

    #[IsGranted('acl/Agreement/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'agreement')]
    #[IsGranted(EntityIsEditableVoter::class, 'agreement')]
    #[Route(
        '/{tenant_url}/agreement/{agreement}/file/{page?1}',
        name: 'app_agreement_file_index',
        requirements: ['page' => '\d+'],
        methods: ['GET'],
    )]
    public function index(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        Agreement $agreement,
        AgreementFileResultSet $resultSet,
    ): Response {
        $resultSet->initialize($agreement);
        return $this->render('agreement_file/index.html.twig', [
            'table' => $resultSet,
            'agreement' => $agreement,
        ]);
    }

    #[IsGranted('acl/Agreement/view')]
    #[Route(
        '/{tenant_url}/agreement-file/{agreementFile}/download',
        name: 'app_agreement_file_download',
        methods: ['GET']
    )]
    #[IsGranted(BelongsToTenantVoter::class, 'agreementFile')]
    public function download(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        AgreementFile $agreementFile,
    ): Response {
        return $this->getFileDownloadResponse($agreementFile->getFile());
    }

    #[IsGranted('acl/Agreement/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'agreement')]
    #[IsGranted(EntityIsEditableVoter::class, 'agreement')]
    #[Route('/{tenant_url}/agreement/{agreement}/file/add', name: 'app_agreement_file_add', methods: ['GET', 'POST'])]
    public function add(
        Agreement $agreement,
        Request $request,
        EntityManagerInterface $entityManager,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        SessionFiles $sessionFiles,
    ): Response {
        $agreementFile = new AgreementFile();
        $agreementFile->setAgreement($agreement);
        $agreementFile->setType(AgreementFile::TYPE_OTHER);
        $form = $this->createForm(
            AgreementFileType::class,
            $agreementFile,
            ['action' => $this->generateUrl(
                'app_agreement_file_add',
                ['tenant_url' => $tenant_url, 'agreement' => $agreement->getId()]
            )]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $session_tmpfile_uuid = $request->get('agreement_file')['file'] ?? null;
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);
            $file = FileManager::setDataFromPath($uri);
            $agreementFile->setFile($file);
            $entityManager->persist($agreementFile);

            $entityManager->persist($file);
            $entityManager->flush();

            $response = new JsonResponse(AgreementFileResultSet::normalize($agreementFile, ['index', 'action']));
            $response->headers->add(['X-Asalae-Success' => 'true']);
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('agreement_file/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/Agreement/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'agreement')]
    #[IsGranted(EntityIsEditableVoter::class, 'agreement')]
    #[Route(
        '/{tenant_url}/agreement/{agreement}/file/edit/{agreementFile}',
        name: 'app_agreement_file_edit',
        methods: ['GET', 'POST'],
    )]
    public function edit(
        Request $request,
        EntityManagerInterface $entityManager,
        Agreement $agreement,
        AgreementFile $agreementFile,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $form = $this->createForm(
            AgreementFileType::class,
            $agreementFile,
            [
                'action' => $this->generateUrl('app_agreement_file_edit', [
                    'tenant_url' => $tenant_url,
                    'agreement' => $agreement->getId(),
                    'agreementFile' => $agreementFile->getId(),
                ]),
                'withFileUpload' => false,
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($agreementFile);
            $entityManager->flush();

            $response = new JsonResponse(AgreementFileResultSet::normalize($agreementFile, ['index', 'action']));
            $response->headers->add(['X-Asalae-Success' => 'true']);

            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('agreement_file/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/Agreement/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'agreement')]
    #[IsGranted(EntityIsEditableVoter::class, 'agreement')]
    #[Route(
        '/{tenant_url}/agreement/{agreement}/file/delete/{agreementFile}',
        name: 'app_agreement_file_delete',
        methods: ['DELETE'],
    )]
    public function delete(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        Agreement $agreement,
        AgreementFile $agreementFile,
        EntityManagerInterface $entityManager,
    ): Response {
        $entityManager->remove($agreementFile->getFile());
        $entityManager->flush();

        return new Response('done');
    }
}
