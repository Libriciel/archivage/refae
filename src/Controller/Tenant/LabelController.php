<?php

namespace App\Controller\Tenant;

use App\Entity\Label;
use App\Entity\User;
use App\Form\LabelImportConfirmType;
use App\Form\LabelImportType;
use App\Form\LabelType;
use App\Repository\LabelRepository;
use App\Repository\TenantRepository;
use App\ResultSet\LabelResultSet;
use App\ResultSet\LabelResultSetPreview;
use App\Security\Voter\BelongsToTenantVoter;
use App\Security\Voter\EntityIsDeletableVoter;
use App\Security\Voter\TenantUrlVoter;
use App\Service\ExportCsv;
use App\Service\ImportZip;
use App\Service\Manager\LabelManager;
use App\Service\SessionFiles;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use IntlDateFormatter;
use Locale;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class LabelController extends AbstractController
{
    #[Route('/{tenant_url}/label/{page?1}', name: 'app_label', requirements: ['page' => '\d+'], methods: ['GET'])]
    #[IsGranted('acl/Label/view')]
    public function index(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        LabelResultSet $resultSet,
    ): Response {
        return $this->render('label/index.html.twig', [
            'table' => $resultSet,
        ]);
    }

    #[Route('/{tenant_url}/label/add', name: 'app_label_add', methods: ['GET', 'POST'])]
    #[IsGranted('acl/Label/add_edit')]
    public function add(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        Request $request,
        EntityManagerInterface $entityManager,
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        $label = new Label();
        $label->setTenant($user->getTenant($request));
        $form = $this->createForm(
            LabelType::class,
            $label,
            ['action' => $this->generateUrl('app_label_add')]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($label);
            $entityManager->flush();
            $response = new JsonResponse(LabelResultSet::normalize($label, ['index', 'action']));
            $response->headers->add(['X-Asalae-Success' => 'true']);
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('label/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/Label/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'label')]
    #[Route('/{tenant_url}/label/edit/{label?}', name: 'app_label_edit', methods: ['GET', 'POST'])]
    public function edit(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        Request $request,
        EntityManagerInterface $entityManager,
        Label $label,
    ): Response {
        $form = $this->createForm(
            LabelType::class,
            $label,
            [
                'action' => $this->generateUrl('app_label_edit', ['label' => $label->getId()]),
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($label);
            $entityManager->flush();

            $response = new JsonResponse(LabelResultSet::normalize($label, ['index', 'action']));
            $response->headers->add(['X-Asalae-Success' => 'true']);
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('label/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/{tenant_url}/label/view/{id?}', name: 'app_label_view', methods: ['GET'])]
    #[IsGranted('acl/Label/view')]
    #[IsGranted(BelongsToTenantVoter::class, 'label')]
    public function view(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        Label $label,
    ): Response {
        return $this->render('label/view.html.twig', [
            'label' => $label,
        ]);
    }

    #[Route('/{tenant_url}/label/delete/{id?}', name: 'app_label_delete', methods: ['DELETE'])]
    #[IsGranted('acl/Label/delete')]
    #[IsGranted(EntityIsDeletableVoter::class, 'label')]
    #[IsGranted(BelongsToTenantVoter::class, 'label')]
    public function delete(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        Label $label,
        EntityManagerInterface $entityManager,
    ): Response {
        $entityManager->remove($label);
        $entityManager->flush();
        $response = new Response();
        $response->setContent('done');
        return $response;
    }

    #[Route('/{tenant_url}/label/csv', name: 'app_label_csv', methods: ['GET'])]
    #[IsGranted('acl/Label/view')]
    public function csv(
        LabelResultSet $labelResulSet,
        ExportCsv $exportCsv,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        return $exportCsv->fromResultSet($labelResulSet);
    }

    #[Route('/{tenant_url}/label/export', name: 'app_label_export', methods: ['GET'])]
    #[IsGranted('acl/Label/view')]
    public function export(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        LabelRepository $labelRepository,
        TenantRepository $tenantRepository,
        LabelManager $labelManager,
        LabelResultSet $labelResultSet,
    ): Response {
        $tenant = $tenantRepository->findOneByBaseurl($tenant_url);
        $query = $labelRepository->queryForExport($tenant);
        $labelResultSet->appendFilters($query);

        $zip = $labelManager->export($query);
        $filename = sprintf('%s_export.%s.refae', (new DateTime())->format('Y-m-d_His'), $labelManager->getFileName());
        $headers = [
            'Content-Description' => 'File Transfer',
            'Content-Type' => 'application/zip',
            'Content-Length' => strlen($zip),
            'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
            'Expires' => '0',
            'Cache-Control' => 'must-revalidate',
        ];
        return new Response($zip, Response::HTTP_OK, $headers);
    }

    #[Route('/{tenant_url}/label/import', name: 'app_label_import1', methods: ['GET', 'POST'])]
    #[IsGranted('acl/Label/view')]
    public function import1(
        string $tenant_url,
        Request $request,
        SessionFiles $sessionFiles,
    ): Response {
        $form = $this->createForm(
            LabelImportType::class,
            options: ['action' => $this->generateUrl('app_label_import1', ['tenant_url' => $tenant_url])]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $session_tmpfile_uuid = $request->get('label_import')['file'] ?? null;
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);

            if (mime_content_type($uri) === 'application/zip') {
                $modalParams = [
                    'btnAcceptContent' => '<i class="fa fa-upload fa-space" aria-hidden="true"></i>'
                        . "<span>Importer</span>",
                    'btnCancelContent' => '<i class="fa fa-trash fa-space" aria-hidden="true"></i>'
                        . "<span>Annuler l'import</span>",
                    'btnCancelUrl' => $this->generateUrl(
                        'app_label_delete_import',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ];
                $response->headers->add([
                    'X-Asalae-Modal-Params' => json_encode($modalParams),
                    'X-Asalae-Step-Title' => json_encode("Récapitulatif avant import"),
                    'X-Asalae-Step' => $this->generateUrl(
                        'app_label_import2',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ]);
                return $response;
            } else {
                $form->get('file')
                    ->addError(new FormError("Ce fichier ne semble pas contenir des étiquettes"));
            }
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('label/import1.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/label/import/{session_tmpfile_uuid}',
        name: 'app_label_import2',
        methods: ['GET', 'POST']
    )]
    #[IsGranted('acl/Label/import')]
    public function import2(
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        Request $request,
        string $session_tmpfile_uuid,
        LabelManager $labelManager,
        LabelResultSetPreview $previewResultset,
        ImportZip $importZip,
    ): Response {
        $importZip->extract($session_tmpfile_uuid, $labelManager);
        $previewResultset->setDataFromArray($importZip->previewData);

        $form = $this->createForm(
            LabelImportConfirmType::class,
            null,
            [
                'action' => $this->generateUrl(
                    'app_label_import2',
                    ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                )
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $labelManager->import($importZip);
            $this->addFlash(
                'success',
                $importZip->importableCount > 1
                    ? "$importZip->importableCount étiquettes importées avec succès"
                    : "$importZip->importableCount étiquette importée avec succès"
            );
            $response->headers->add(['X-Asalae-Redirect' => $this->generateUrl('app_label')]);
            return $response;
        }

        $formatter = new IntlDateFormatter(
            Locale::getDefault(),
            IntlDateFormatter::FULL,
            IntlDateFormatter::SHORT
        );
        return $this->render('label/import2.html.twig', [
            'view_data' => [
                "Date du fichier d'export" => $formatter->format($importZip->exportDate),
                "Nombre d'étiquettes" => $importZip->dataCount,
            ],
            'content_preview_resultset' => $previewResultset,
            'summerize_data' => [
                "Nombre d'étiquettes impossibles à importer" => $importZip->failedCount,
                "Total importable" => $importZip->importableCount,
            ],
            'errors' => $importZip->errors,
            'fatalError' => $importZip->hasFatalError,
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/label/delete-import/{session_tmpfile_uuid}',
        name: 'app_label_delete_import',
        methods: ['DELETE'],
    )]
    #[IsGranted('acl/Label/import')]
    public function deleteImport(
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        string $session_tmpfile_uuid,
        SessionFiles $sessionFiles,
    ): Response {
        $sessionFiles->delete($session_tmpfile_uuid);
        $response = new Response();
        $response->setContent('ok');
        $response->headers->add(['X-Asalae-Success' => 'true']);
        return $response;
    }
}
