<?php

namespace App\Controller\Tenant;

use App\Entity\TenantConfiguration;
use App\Form\TenantConfigurationType;
use App\Repository\TenantRepository;
use App\Security\Voter\TenantUrlVoter;
use App\Service\LoggedUser;
use App\Service\Manager\FileManager;
use App\Service\SessionFiles;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\EventListener\AbstractSessionListener;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class TenantConfigurationController extends AbstractController
{
    #[IsGranted('acl/TenantConfiguration/add_edit')]
    #[Route(
        '/{tenant_url}/configuration',
        name: 'app_tenant_configuration',
        methods: ['GET', 'POST']
    )]
    public function edit(
        string $tenant_url,
        Request $request,
        EntityManagerInterface $entityManager,
        TenantRepository $tenantRepository,
        SessionFiles $sessionFiles,
        LoggedUser $loggedUser,
    ): Response {
        $tenant = $tenantRepository->findOneByBaseurl($tenant_url);
        if ($loggedUser->tenant() !== $tenant) {
            throw $this->createAccessDeniedException();
        }
        $tenantConfiguration = $tenant->getConfiguration();
        if (!$tenantConfiguration) {
            $tenantConfiguration = new TenantConfiguration();
            $tenantConfiguration->setTenant($tenant);
            $tenantConfiguration->setLogoLoginBgColor('#f6f5f4');
        }

        $form = $this->createForm(
            TenantConfigurationType::class,
            $tenantConfiguration,
            [
                'action' => $this->generateUrl('app_tenant_configuration', [
                    'tenant_url' => $tenant_url,
                ]),
                'deleteLogoMain' => $this->generateUrl('app_tenant_configuration_delete_main_logo'),
                'deleteLogoLogin' => $this->generateUrl('app_tenant_configuration_delete_login_logo'),
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $session_tmpfile_uuid_main = $request->get('tenant_configuration')['logoMainHandler'] ?? null;
            if ($sessionFiles->exists($session_tmpfile_uuid_main)) {
                $uri = $sessionFiles->getUri($session_tmpfile_uuid_main);
                $file = FileManager::setDataFromPath($uri);
                $tenantConfiguration->setLogoMain($file);
                $entityManager->persist($file);
            }
            $session_tmpfile_uuid_logo = $request->get('tenant_configuration')['logoLoginHandler'] ?? null;
            if ($sessionFiles->exists($session_tmpfile_uuid_logo)) {
                $uri = $sessionFiles->getUri($session_tmpfile_uuid_logo);
                $file = FileManager::setDataFromPath($uri);
                $tenantConfiguration->setLogoLogin($file);
                $entityManager->persist($file);
            }

            $entityManager->persist($tenant);
            $entityManager->persist($tenantConfiguration);
            $entityManager->flush();

            return new JsonResponse(
                "OK",
                headers: ['X-Asalae-Success' => 'true']
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('tenant_configuration/configuration.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/TenantConfiguration/add_edit')]
    #[Route(
        '/{tenant_url}/configuration/delete-main-logo',
        name: 'app_tenant_configuration_delete_main_logo',
        methods: ['DELETE']
    )]
    public function deleteMainLogo(
        string $tenant_url,
        EntityManagerInterface $entityManager,
        TenantRepository $tenantRepository,
        LoggedUser $loggedUser,
    ): Response {
        $tenant = $tenantRepository->findOneByBaseurl($tenant_url);
        if ($loggedUser->tenant() !== $tenant) {
            throw $this->createAccessDeniedException();
        }
        $tenantConfiguration = $tenant->getConfiguration();
        $file = $tenantConfiguration->getLogoMain();
        $tenantConfiguration->setLogoMain(null);
        $entityManager->flush();

        $entityManager->remove($file);
        $entityManager->flush();

        return new Response('deleted logo');
    }

    #[IsGranted('acl/TenantConfiguration/add_edit')]
    #[Route(
        '/{tenant_url}/configuration/delete-login-logo',
        name: 'app_tenant_configuration_delete_login_logo',
        methods: ['DELETE']
    )]
    public function deleteLoginLogo(
        string $tenant_url,
        EntityManagerInterface $entityManager,
        TenantRepository $tenantRepository,
        LoggedUser $loggedUser,
    ): Response {
        $tenant = $tenantRepository->findOneByBaseurl($tenant_url);
        if ($loggedUser->tenant() !== $tenant) {
            throw $this->createAccessDeniedException();
        }
        $tenantConfiguration = $tenant->getConfiguration();
        $file = $tenantConfiguration->getLogoLogin();
        $tenantConfiguration->setLogoLogin(null);
        $entityManager->flush();

        $entityManager->remove($file);
        $entityManager->flush();

        return new Response('deleted logo');
    }

    #[Route('/{tenant_url}/logo-main/{filename}', name: 'public_tenant_logo_main')]
    public function tenantLogoMain(
        string $tenant_url,
        TenantRepository $tenantRepository,
    ): Response {
        $tenant = $tenantRepository->findOneByBaseurl($tenant_url);
        if (!$tenant) {
            throw $this->createNotFoundException();
        }
        $tenantConfiguration = $tenant->getConfiguration();
        if (!$tenantConfiguration) {
            throw $this->createNotFoundException();
        }
        $logoMain = $tenantConfiguration->getLogoMain();
        if (!$logoMain) {
            throw $this->createNotFoundException();
        }
        $response = new Response();
        $response
            ->setContent(stream_get_contents($logoMain->getContent()))
        ;
        $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
        $response->headers->add(['cache-control' => 'public, max-age=604800, immutable']);
        $response->headers->add(['content-type' => $logoMain->getMime()]);
        $response->headers->add(['content-length' => $logoMain->getSize()]);
        return $response;
    }
}
