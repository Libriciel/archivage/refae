<?php

namespace App\Controller\Tenant;

use App\Controller\DownloadFileTrait;
use App\Entity\ManagementRuleFile;
use App\Entity\ManagementRule;
use App\Form\ManagementRuleFileType;
use App\ResultSet\ManagementRuleFileResultSet;
use App\Security\Voter\BelongsToTenantVoter;
use App\Security\Voter\EntityIsEditableVoter;
use App\Security\Voter\TenantUrlVoter;
use App\Service\Manager\FileManager;
use App\Service\SessionFiles;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class ManagementRuleFileController extends AbstractController
{
    use DownloadFileTrait;

    #[IsGranted('acl/ManagementRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'managementRule')]
    #[IsGranted(EntityIsEditableVoter::class, 'managementRule')]
    #[Route(
        '/{tenant_url}/management-rule/{managementRule}/file/{page?1}',
        name: 'app_management_rule_file_index',
        requirements: ['page' => '\d+'],
        methods: ['GET'],
    )]
    public function index(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        ManagementRule $managementRule,
        ManagementRuleFileResultSet $resultSet,
    ): Response {
        $resultSet->initialize($managementRule);
        return $this->render('management_rule_file/index.html.twig', [
            'table' => $resultSet,
            'managementRule' => $managementRule,
        ]);
    }

    #[IsGranted('acl/ManagementRule/view')]
    #[Route(
        '/{tenant_url}/management-rule-file/{managementRuleFile}/download',
        name: 'app_management_rule_file_download',
        methods: ['GET']
    )]
    #[IsGranted(BelongsToTenantVoter::class, 'managementRuleFile')]
    public function download(
        ManagementRuleFile $managementRuleFile,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        return $this->getFileDownloadResponse($managementRuleFile->getFile());
    }

    #[IsGranted('acl/ManagementRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'managementRule')]
    #[IsGranted(EntityIsEditableVoter::class, 'managementRule')]
    #[Route(
        '/{tenant_url}/management-rule/{managementRule}/file/add',
        name: 'app_management_rule_file_add',
        methods: ['GET', 'POST'],
    )]
    public function add(
        ManagementRule $managementRule,
        Request $request,
        EntityManagerInterface $entityManager,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        SessionFiles $sessionFiles,
    ): Response {
        $managementRuleFile = new ManagementRuleFile();
        $managementRuleFile->setManagementRule($managementRule);
        $managementRuleFile->setType(ManagementRuleFile::TYPE_OTHER);
        $form = $this->createForm(
            ManagementRuleFileType::class,
            $managementRuleFile,
            ['action' => $this->generateUrl(
                'app_management_rule_file_add',
                ['tenant_url' => $tenant_url, 'managementRule' => $managementRule->getId()]
            )]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $session_tmpfile_uuid = $request->get('management_rule_file')['file'] ?? null;
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);
            $file = FileManager::setDataFromPath($uri);
            $managementRuleFile->setFile($file);
            $entityManager->persist($managementRuleFile);

            $entityManager->persist($file);
            $entityManager->flush();

            $response = new JsonResponse(
                ManagementRuleFileResultSet::normalize($managementRuleFile, ['index', 'action'])
            );
            $response->headers->add(['X-Asalae-Success' => 'true']);
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('management_rule_file/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/ManagementRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'managementRule')]
    #[IsGranted(EntityIsEditableVoter::class, 'managementRule')]
    #[Route(
        '/{tenant_url}/management-rule/{managementRule}/file/edit/{managementRuleFile}',
        name: 'app_management_rule_file_edit',
        methods: ['GET', 'POST'],
    )]
    public function edit(
        Request $request,
        EntityManagerInterface $entityManager,
        ManagementRule $managementRule,
        ManagementRuleFile $managementRuleFile,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $form = $this->createForm(
            ManagementRuleFileType::class,
            $managementRuleFile,
            [
                'action' => $this->generateUrl('app_management_rule_file_edit', [
                    'tenant_url' => $tenant_url,
                    'managementRule' => $managementRule->getId(),
                    'managementRuleFile' => $managementRuleFile->getId(),
                ]),
                'withFileUpload' => false,
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($managementRuleFile);
            $entityManager->flush();

            $response = new JsonResponse(
                ManagementRuleFileResultSet::normalize($managementRuleFile, ['index', 'action'])
            );
            $response->headers->add(['X-Asalae-Success' => 'true']);

            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('management_rule_file/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/ManagementRule/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'managementRule')]
    #[IsGranted(EntityIsEditableVoter::class, 'managementRule')]
    #[Route(
        '/{tenant_url}/management-rule/{managementRule}/file/delete/{managementRuleFile}',
        name: 'app_management_rule_file_delete',
        methods: ['DELETE'],
    )]
    public function delete(
        ManagementRuleFile $managementRuleFile,
        EntityManagerInterface $entityManager,
        /** @noinspection PhpUnusedParameterInspection **/
        ManagementRule $managementRule,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $entityManager->remove($managementRuleFile->getFile());
        $entityManager->flush();

        return new Response('done');
    }
}
