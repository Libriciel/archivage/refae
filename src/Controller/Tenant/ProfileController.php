<?php

namespace App\Controller\Tenant;

use App\Entity\Profile;
use App\Entity\User;
use App\Entity\VersionableEntityInterface;
use App\Form\ProfileImportConfirmType;
use App\Form\ProfileImportType;
use App\Form\ProfileType;
use App\Form\ProfileVersionType;
use App\Repository\ProfileRepository;
use App\Repository\TenantRepository;
use App\ResultSet\ProfileFileViewResultSet;
use App\ResultSet\ProfilePublishedResultSet;
use App\ResultSet\ProfileResultSet;
use App\ResultSet\ProfileResultSetPreview;
use App\ResultSet\PublicProfileResultSet;
use App\Security\Voter\BelongsToTenantVoter;
use App\Security\Voter\EntityIsDeletableVoter;
use App\Security\Voter\EntityIsEditableVoter;
use App\Security\Voter\TenantUrlVoter;
use App\Service\ExportCsv;
use App\Service\ImportZip;
use App\Service\Manager\ProfileManager;
use App\Service\SessionFiles;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use IntlDateFormatter;
use Locale;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Workflow\WorkflowInterface;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class ProfileController extends AbstractController
{
    #[IsGranted('acl/Profile/view')]
    #[Route(
        '/{tenant_url}/profile/{page?1}',
        name: 'app_profile',
        requirements: ['page' => '\d+'],
        methods: ['GET']
    )]
    public function index(
        string $tenant_url,
        ProfileResultSet $table,
    ): Response {
        return $this->render('profile/index.html.twig', [
            'table' => $table,
            'tenant_url' => $tenant_url,
        ]);
    }

    #[IsGranted('acl/Profile/view')]
    #[Route(
        '/{tenant_url}/profile-published/{page?1}',
        name: 'app_profile_index_published',
        requirements: ['page' => '\d+'],
        methods: ['GET']
    )]
    public function indexPublished(
        string $tenant_url,
        ProfilePublishedResultSet $table,
    ): Response {
        return $this->render('profile/index_published.html.twig', [
            'table' => $table,
            'tenant_url' => $tenant_url,
        ]);
    }

    #[IsGranted('acl/Profile/view')]
    #[IsGranted(BelongsToTenantVoter::class, 'profile')]
    #[Route('/{tenant_url}/profile/view/{profile?}', name: 'app_profile_view', methods: ['GET'])]
    public function view(
        #[MapEntity(expr: 'repository.findWithPreviousVersions(profile)')]
        Profile $profile,
        ProfileFileViewResultSet $profileFileResultSet,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $profileFileResultSet->initialize($profile);
        return $this->render('profile/view.html.twig', [
            'profile' => $profile,
            'files' => $profileFileResultSet,
        ]);
    }

    #[IsGranted('acl/Profile/add_edit')]
    #[Route('/{tenant_url}/profile/add', name: 'app_profile_add', methods: ['GET', 'POST'])]
    public function add(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        Request $request,
        EntityManagerInterface $entityManager,
        ProfileManager $profileManager,
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        $profile = $profileManager->newProfile($user->getTenant($request));

        $form = $this->createForm(
            ProfileType::class,
            $profile,
            [
                'action' => $this->generateUrl('app_profile_add'),
                'tenant_url' => $tenant_url,
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $profileManager->save($profile);

            return new JsonResponse(
                ProfileResultSet::normalize($profile, ['index', 'action']),
                headers: ['X-Asalae-Success' => 'true']
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('profile/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/Profile/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'profile')]
    #[IsGranted(EntityIsEditableVoter::class, 'profile')]
    #[Route('/{tenant_url}/profile/edit/{profile?}', name: 'app_profile_edit', methods: ['GET', 'POST'])]
    public function edit(
        Request $request,
        EntityManagerInterface $entityManager,
        Profile $profile,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $form = $this->createForm(
            ProfileType::class,
            $profile,
            [
                'action' => $this->generateUrl('app_profile_edit', ['profile' => $profile->getId()]),
                'tenant_url' => $tenant_url,
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($profile);
            $entityManager->flush();

            return new JsonResponse(
                ProfileResultSet::normalize($profile, ['index', 'action']),
                headers: ['X-Asalae-Success' => 'true']
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('profile/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/{tenant_url}/profile/publish/{profile?}', name: 'app_profile_publish', methods: ['POST'])]
    #[IsGranted('acl/Profile/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'profile')]
    public function publish(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        WorkflowInterface $profileStateMachine,
        Profile $profile,
    ): Response {
        $profileStateMachine->apply($profile, VersionableEntityInterface::T_PUBLISH);

        return new JsonResponse(
            ProfileResultSet::normalize($profile, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/profile/revoke/{profile?}', name: 'app_profile_revoke', methods: ['POST'])]
    #[IsGranted('acl/Profile/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'profile')]
    public function revoke(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        WorkflowInterface $profileStateMachine,
        Profile $profile,
    ): Response {
        $profileStateMachine->apply($profile, VersionableEntityInterface::T_REVOKE);

        return new JsonResponse(
            ProfileResultSet::normalize($profile, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/profile/restore/{profile?}', name: 'app_profile_restore', methods: ['POST'])]
    #[IsGranted('acl/Profile/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'profile')]
    public function restore(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        WorkflowInterface $profileStateMachine,
        Profile $profile,
    ): Response {
        $profileStateMachine->apply($profile, VersionableEntityInterface::T_RECOVER);

        return new JsonResponse(
            ProfileResultSet::normalize($profile, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/profile/activate/{profile?}', name: 'app_profile_activate', methods: ['POST'])]
    #[IsGranted('acl/Profile/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'profile')]
    public function activate(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        WorkflowInterface $profileStateMachine,
        Profile $profile,
    ): Response {
        $profileStateMachine->apply($profile, VersionableEntityInterface::T_ACTIVATE);

        return new JsonResponse(
            ProfileResultSet::normalize($profile, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/profile/deactivate/{profile?}', name: 'app_profile_deactivate', methods: ['POST'])]
    #[IsGranted('acl/Profile/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'profile')]
    public function deactivate(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        WorkflowInterface $profileStateMachine,
        Profile $profile,
    ): Response {
        $profileStateMachine->apply($profile, VersionableEntityInterface::T_DEACTIVATE);

        return new JsonResponse(
            ProfileResultSet::normalize($profile, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/profile/new-version/{profile?}', name: 'app_profile_new_version', methods: ['GET', 'POST'])]
    #[IsGranted('acl/Profile/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'profile')]
    public function newVersion(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        Request $request,
        EntityManagerInterface $entityManager,
        ProfileManager $profileManager,
        Profile $profile,
    ): Response {
        $form = $this->createForm(
            ProfileVersionType::class,
            options: ['action' => $this->generateUrl(
                'app_profile_new_version',
                ['profile' => $profile->getId()]
            )]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $newVersion = $profileManager->newVersion($profile, $form->get('reason')->getData());

            return new JsonResponse(
                ProfileResultSet::normalize($newVersion, ['index', 'action']),
                headers: [
                    'X-Asalae-Success' => 'true',
                    'X-Asalae-PreviousVersionId' => $profile->getId(),
                ]
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('profile/version.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/{tenant_url}/profile/delete/{profile?}', name: 'app_profile_delete', methods: ['DELETE'])]
    #[IsGranted('acl/Profile/delete')]
    #[IsGranted(EntityIsDeletableVoter::class, 'profile')]
    #[IsGranted(BelongsToTenantVoter::class, 'profile')]
    public function delete(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        EntityManagerInterface $entityManager,
        ProfileRepository $profileRepository,
        Profile $profile,
    ): Response {
        $entityManager->remove($profile);
        $entityManager->flush();

        if ($previousVersion = $profileRepository->findPreviousVersion($profile)) {
            return new JsonResponse(
                ProfileResultSet::normalize($previousVersion, ['index', 'action']),
                headers: [
                    'X-Asalae-Success' => 'true',
                    'X-Asalae-PreviousVersionEntity' => 'true',
                ]
            );
        }

        return new Response('done');
    }

    #[Route('/{tenant_url}/profile/export', name: 'app_profile_export', methods: ['GET'])]
    #[IsGranted('acl/Profile/view')]
    public function export(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        ProfileRepository $profileRepository,
        TenantRepository $tenantRepository,
        ProfileManager $profileManager,
        ProfileResultSet $profileResultSet,
    ): Response {
        $tenant = $tenantRepository->findOneByBaseurl($tenant_url);
        $query = $profileRepository->queryForExport($tenant);
        $profileResultSet->appendFilters($query);

        $zip = $profileManager->export($query);
        $filename = sprintf(
            '%s_export.%s.refae',
            (new DateTime())->format('Y-m-d_His'),
            $profileManager->getFileName()
        );
        $headers = [
            'Content-Description' => 'File Transfer',
            'Content-Type' => 'application/zip',
            'Content-Length' => strlen($zip),
            'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
            'Expires' => '0',
            'Cache-Control' => 'must-revalidate',
        ];
        return new Response($zip, Response::HTTP_OK, $headers);
    }

    #[Route('/{tenant_url}/profile/import', name: 'app_profile_import1', methods: ['GET', 'POST'])]
    #[IsGranted('acl/Profile/view')]
    public function import1(
        string $tenant_url,
        Request $request,
        SessionFiles $sessionFiles,
    ): Response {
        $form = $this->createForm(
            ProfileImportType::class,
            options: ['action' => $this->generateUrl('app_profile_import1', ['tenant_url' => $tenant_url])]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $session_tmpfile_uuid = $request->get('profile_import')['file'] ?? null;
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);

            if (mime_content_type($uri) === 'application/zip') {
                $modalParams = [
                    'btnAcceptContent' => '<i class="fa fa-upload fa-space" aria-hidden="true"></i>'
                        . "<span>Importer</span>",
                    'btnCancelContent' => '<i class="fa fa-trash fa-space" aria-hidden="true"></i>'
                        . "<span>Annuler l'import</span>",
                    'btnCancelUrl' => $this->generateUrl(
                        'app_profile_delete_import',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ];
                $response->headers->add([
                    'X-Asalae-Modal-Params' => json_encode($modalParams),
                    'X-Asalae-Step-Title' => json_encode("Récapitulatif avant import"),
                    'X-Asalae-Step' => $this->generateUrl(
                        'app_profile_import2',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ]);
                return $response;
            } else {
                $form->get('file')
                    ->addError(new FormError("Ce fichier ne semble pas contenir des profils d'archives"));
            }
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('profile/import1.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/profile/import/{session_tmpfile_uuid}',
        name: 'app_profile_import2',
        methods: ['GET', 'POST']
    )]
    #[IsGranted('acl/Profile/import')]
    public function import2(
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        Request $request,
        string $session_tmpfile_uuid,
        ProfileManager $profileManager,
        ProfileResultSetPreview $previewResultset,
        ImportZip $importZip,
    ): Response {
        $importZip->extract($session_tmpfile_uuid, $profileManager);
        $previewResultset->setDataFromArray($importZip->previewData);

        $form = $this->createForm(
            ProfileImportConfirmType::class,
            null,
            [
                'action' => $this->generateUrl(
                    'app_profile_import2',
                    ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                )
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $profileManager->import($importZip);
            $this->addFlash(
                'success',
                $importZip->importableCount > 1
                    ? "$importZip->importableCount profils importés avec succès"
                    : "$importZip->importableCount profil importé avec succès"
            );
            $response->headers->add(['X-Asalae-Redirect' => $this->generateUrl('app_profile')]);
            return $response;
        }

        $formatter = new IntlDateFormatter(
            Locale::getDefault(),
            IntlDateFormatter::FULL,
            IntlDateFormatter::SHORT
        );
        return $this->render('profile/import2.html.twig', [
            'view_data' => [
                "Date du fichier d'export" => $formatter->format($importZip->exportDate),
                "Nombre de profils d'archives" => $importZip->dataCount,
                "Nombre de fichiers liés" => $importZip->fileCount,
                "Nombre d'étiquettes à importer" => count($importZip->labels),
            ],
            'content_preview_resultset' => $previewResultset,
            'summerize_data' => [
                "Nombre de profils d'archives impossibles à importer" => $importZip->failedCount,
                "Total importable" => $importZip->importableCount,
            ],
            'errors' => $importZip->errors,
            'fatalError' => $importZip->hasFatalError,
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/profile/delete-import/{session_tmpfile_uuid}',
        name: 'app_profile_delete_import',
        methods: ['DELETE'],
    )]
    #[IsGranted('acl/Profile/import')]
    public function deleteImport(
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        string $session_tmpfile_uuid,
        SessionFiles $sessionFiles,
    ): Response {
        $sessionFiles->delete($session_tmpfile_uuid);
        $response = new Response();
        $response->setContent('ok');
        $response->headers->add(['X-Asalae-Success' => 'true']);
        return $response;
    }

    #[Route('/{tenant_url}/profile/csv', name: 'app_profile_csv', methods: ['GET'])]
    #[IsGranted('acl/Profile/view')]
    public function csv(
        ProfileResultSet $profileResultSet,
        ProfilePublishedResultSet $profilePublishedResultSet,
        ExportCsv $exportCsv,
        Request $request,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        return $exportCsv->fromResultSet(
            $request->get('index_published')
                ? $profilePublishedResultSet
                : $profileResultSet
        );
    }

    #[Route(
        '/{tenant_url}/reader/profile/{page?1}',
        name: 'app_reader_profile',
        requirements: ['page' => '\d+']
    )]
    public function readerProfile(
        PublicProfileResultSet $table,
        TenantRepository $tenantRepository,
        string $tenant_url = null,
    ): Response {
        $tenant = $tenant_url ? $tenantRepository->findOneByBaseurl($tenant_url) : null;
        $table->initialize($tenant);
        return $this->render('reader/profile.html.twig', [
            'table' => $table,
        ]);
    }

    #[Route('/{tenant_url}/reader/profile/csv', name: 'app_reader_profile_csv', methods: ['GET'])]
    public function readerCsv(
        PublicProfileResultSet $publicProfileResultSet,
        ExportCsv $exportCsv,
        TenantRepository $tenantRepository,
        string $tenant_url = null,
    ): Response {
        $tenant = $tenant_url ? $tenantRepository->findOneByBaseurl($tenant_url) : null;
        $publicProfileResultSet->initialize($tenant);
        return $exportCsv->fromResultSet($publicProfileResultSet);
    }
}
