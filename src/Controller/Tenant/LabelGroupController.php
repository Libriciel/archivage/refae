<?php

namespace App\Controller\Tenant;

use App\Entity\LabelGroup;
use App\Entity\User;
use App\Form\LabelGroupType;
use App\ResultSet\LabelGroupResultSet;
use App\Security\Voter\TenantUrlVoter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class LabelGroupController extends AbstractController
{
    #[Route(
        '/{tenant_url}/label-group/{page?1}',
        name: 'app_label_group',
        requirements: ['page' => '\d+'],
        methods: ['GET'],
    )]
    #[IsGranted('acl/LabelGroup/view')]
    public function index(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        LabelGroupResultSet $resultSet
    ): Response {
        return $this->render('label-group/index.html.twig', [
            'table' => $resultSet,
        ]);
    }

    #[Route('/{tenant_url}/label-group/add', name: 'app_label_group_add', methods: ['GET', 'POST'])]
    #[IsGranted('acl/LabelGroup/add_edit')]
    public function add(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        Request $request,
        EntityManagerInterface $entityManager,
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        $labelGroup = new LabelGroup();
        $labelGroup->setTenant($user->getTenant($request));
        $form = $this->createForm(
            LabelGroupType::class,
            $labelGroup,
            ['action' => $this->generateUrl('app_label_group_add')]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($labelGroup);
            $entityManager->flush();

            $response->setContent(
                json_encode(
                    LabelGroupResultSet::normalize($labelGroup, ['index', 'action']),
                    JSON_THROW_ON_ERROR
                )
            );
            $response->headers->add(
                [
                    'Content-Type' => 'application/json',
                    'X-Asalae-Success' => 'true',
                ]
            );
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('label-group/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/{tenant_url}/label-group/edit/{id?}', name: 'app_label_group_edit', methods: ['GET', 'POST'])]
    #[IsGranted('acl/LabelGroup/add_edit')]
    public function edit(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        string $id,
        Request $request,
        EntityManagerInterface $entityManager,
        LabelGroup $labelGroup,
    ): Response {
        $form = $this->createForm(
            LabelGroupType::class,
            $labelGroup,
            [
                'action' => $this->generateUrl('app_label_group_edit', ['id' => $id]),
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($labelGroup);
            $entityManager->flush();

            $response->setContent(
                json_encode(
                    LabelGroupResultSet::normalize($labelGroup, ['index', 'action']),
                    JSON_THROW_ON_ERROR
                )
            );
            $response->headers->add(
                [
                    'Content-Type' => 'application/json',
                    'X-Asalae-Success' => 'true',
                ]
            );
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('label-group/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/{tenant_url}/label-group/view/{id?}', name: 'app_label_group_view', methods: ['GET'])]
    #[IsGranted('acl/LabelGroup/view')]
    public function view(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        LabelGroup $labelGroup,
    ): Response {
        return $this->render('label-group/view.html.twig', [
            'labelGroup' => $labelGroup,
        ]);
    }

    #[Route('/{tenant_url}/label-group/delete/{id?}', name: 'app_label_group_delete', methods: ['DELETE'])]
    #[IsGranted('acl/LabelGroup/delete')]
    public function delete(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        LabelGroup $labelGroup,
        EntityManagerInterface $entityManager,
    ): Response {
        $entityManager->remove($labelGroup);
        $entityManager->flush();

        $response = new Response();
        $response->setContent('done');
        return $response;
    }
}
