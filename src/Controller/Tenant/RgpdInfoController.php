<?php

namespace App\Controller\Tenant;

use App\Entity\RgpdInfo;
use App\Entity\User;
use App\Form\RgpdInfoType;
use App\Repository\RgpdInfoRepository;
use App\Security\Voter\TenantUrlVoter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class RgpdInfoController extends AbstractController
{
    #[Route('/{tenant_url}/rgpd-info/edit', name: 'app_rgpd_info_edit')]
    #[IsGranted('acl/RgpdInfo/add_edit')]
    public function addEdit(
        Request $request,
        EntityManagerInterface $entityManager,
        RgpdInfoRepository $rgpdInfoRepository,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        $tenant = $user->getTenant($request);
        $rgpdInfo = $rgpdInfoRepository->findOneForTenant($tenant);
        if (!$rgpdInfo) {
            $rgpdInfo = new RgpdInfo();
            $rgpdInfo->setTenant($tenant);
        }
        $form = $this->createForm(
            RgpdInfoType::class,
            $rgpdInfo,
            [
                'action' => $this->generateUrl('app_rgpd_info_edit'),
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($rgpdInfo);
            $entityManager->flush();

            $response->headers->add(
                [
                    'X-Asalae-Success' => 'true',
                ]
            );
            $response->setContent('OK');
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('rgpd_info/add-edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/{tenant_url}/rgpd-info', name: 'app_rgpd_info', methods: ['GET'])]
    #[IsGranted('acl/RgpdInfo/view')]
    public function view(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        RgpdInfoRepository $rgpdInfoRepository,
        Request $request,
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        $tenant = $user->getTenant($request);
        $adminRgpdInfo = $rgpdInfoRepository->findOneForTenant(null);
        $rgpdInfo = $rgpdInfoRepository->findOneForTenant($tenant);
        return $this->render('rgpd_info/view.html.twig', [
            'adminRgpdInfo' => $adminRgpdInfo ?: new RgpdInfo(),
            'rgpdInfo' => $rgpdInfo ?: new RgpdInfo(),
        ]);
    }
}
