<?php

namespace App\Controller\Tenant;

use App\Controller\DownloadFileTrait;
use App\Entity\ProfileFile;
use App\Entity\Profile;
use App\Form\ProfileFileType;
use App\ResultSet\ProfileFileResultSet;
use App\Security\Voter\BelongsToTenantVoter;
use App\Security\Voter\EntityIsEditableVoter;
use App\Security\Voter\TenantUrlVoter;
use App\Service\Manager\FileManager;
use App\Service\SessionFiles;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class ProfileFileController extends AbstractController
{
    use DownloadFileTrait;

    #[IsGranted('acl/Profile/view')]
    #[IsGranted(BelongsToTenantVoter::class, 'profile')]
    #[IsGranted(EntityIsEditableVoter::class, 'profile')]
    #[Route(
        '/{tenant_url}/profile/{profile}/file/{page?1}',
        name: 'app_profile_file_index',
        requirements: ['page' => '\d+'],
        methods: ['GET'],
    )]
    public function index(
        Profile $profile,
        ProfileFileResultSet $resultSet,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url
    ): Response {
        $resultSet->setProfile($profile);

        return $this->render('profile_file/index.html.twig', [
            'table' => $resultSet,
            'profile' => $profile,
        ]);
    }

    #[IsGranted('acl/Profile/view')]
    #[Route(
        '/{tenant_url}/profile-file/{profileFile}/download',
        name: 'app_profile_file_download',
        methods: ['GET']
    )]
    #[IsGranted(BelongsToTenantVoter::class, 'profileFile')]
    public function download(
        ProfileFile $profileFile,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        return $this->getFileDownloadResponse($profileFile->getFile());
    }

    #[IsGranted('acl/Profile/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'profile')]
    #[IsGranted(EntityIsEditableVoter::class, 'profile')]
    #[Route('/{tenant_url}/profile/{profile}/file/add', name: 'app_profile_file_add', methods: ['GET', 'POST'])]
    public function add(
        Profile $profile,
        Request $request,
        EntityManagerInterface $entityManager,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        SessionFiles $sessionFiles,
    ): Response {
        $profileFile = new ProfileFile(profile: $profile);
        $form = $this->createForm(
            ProfileFileType::class,
            $profileFile,
            ['action' => $this->generateUrl(
                'app_profile_file_add',
                ['tenant_url' => $tenant_url, 'profile' => $profile->getId()]
            )]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            // TODO: Manager ?
            $session_tmpfile_uuid = $request->get('profile_file')['file'] ?? null;
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);
            $file = FileManager::setDataFromPath($uri);
            $profileFile->setFile($file);

            $entityManager->persist($file);
            $entityManager->persist($profileFile);
            $entityManager->flush();

            $response = new JsonResponse(ProfileFileResultSet::normalize($profileFile, ['index', 'action']));
            $response->headers->add(['X-Asalae-Success' => 'true']);
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('profile_file/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/Profile/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'profile')]
    #[IsGranted(EntityIsEditableVoter::class, 'profile')]
    #[Route(
        '/{tenant_url}/profile/{profile}/file/edit/{profileFile}',
        name: 'app_profile_file_edit',
        methods: ['GET', 'POST'],
    )]
    public function edit(
        Request $request,
        EntityManagerInterface $entityManager,
        Profile $profile,
        ProfileFile $profileFile,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $form = $this->createForm(
            ProfileFileType::class,
            $profileFile,
            [
                'action' => $this->generateUrl('app_profile_file_edit', [
                    'tenant_url' => $tenant_url,
                    'profile' => $profile->getId(),
                    'profileFile' => $profileFile->getId(),
                ]),
                'withFileUpload' => false,
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($profileFile);
            $entityManager->flush();

            $response = new JsonResponse(ProfileFileResultSet::normalize($profileFile, ['index', 'action']));
            $response->headers->add(['X-Asalae-Success' => 'true']);

            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('profile_file/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/Profile/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'profile')]
    #[IsGranted(EntityIsEditableVoter::class, 'profile')]
    #[Route(
        '/{tenant_url}/profile/{profile}/file/delete/{profileFile}',
        name: 'app_profile_file_delete',
        methods: ['DELETE'],
    )]
    public function delete(
        ProfileFile $profileFile,
        EntityManagerInterface $em,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        /** @noinspection PhpUnusedParameterInspection **/
        Profile $profile,
    ): Response {
        $em->remove($profileFile);
        $em->flush();

        return new Response('done');
    }
}
