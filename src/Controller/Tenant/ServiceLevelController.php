<?php

namespace App\Controller\Tenant;

use App\Entity\ServiceLevel;
use App\Entity\User;
use App\Entity\VersionableEntityInterface;
use App\Form\ServiceLevelImportConfirmType;
use App\Form\ServiceLevelImportType;
use App\Form\ServiceLevelType;
use App\Form\ServiceLevelVersionType;
use App\Repository\ServiceLevelRepository;
use App\Repository\TenantRepository;
use App\ResultSet\PublicServiceLevelResultSet;
use App\ResultSet\ServiceLevelFileViewResultSet;
use App\ResultSet\ServiceLevelPublishedResultSet;
use App\ResultSet\ServiceLevelResultSet;
use App\ResultSet\ServiceLevelResultSetPreview;
use App\Security\Voter\BelongsToTenantVoter;
use App\Security\Voter\EntityIsDeletableVoter;
use App\Security\Voter\EntityIsEditableVoter;
use App\Security\Voter\TenantUrlVoter;
use App\Service\Eaccpf;
use App\Service\ExportCsv;
use App\Service\ImportZip;
use App\Service\Manager\ServiceLevelManager;
use App\Service\SessionFiles;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use IntlDateFormatter;
use Locale;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Workflow\WorkflowInterface;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class ServiceLevelController extends AbstractController
{
    #[IsGranted('acl/ServiceLevel/view')]
    #[Route(
        '/{tenant_url}/service-level/{page?1}',
        name: 'app_service_level',
        requirements: ['page' => '\d+'],
        methods: ['GET'],
    )]
    public function index(
        ServiceLevelResultSet $table,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        return $this->render('service_level/index.html.twig', [
            'table' => $table,
        ]);
    }

    #[IsGranted('acl/ServiceLevel/view')]
    #[Route(
        '/{tenant_url}/service-level-published/{page?1}',
        name: 'app_service_level_index_published',
        requirements: ['page' => '\d+'],
        methods: ['GET'],
    )]
    public function indexPublished(
        string $tenant_url,
        ServiceLevelPublishedResultSet $table,
    ): Response {
        return $this->render('service_level/index_published.html.twig', [
            'table' => $table,
            'tenant_url' => $tenant_url,
        ]);
    }

    #[IsGranted('acl/ServiceLevel/view')]
    #[IsGranted(BelongsToTenantVoter::class, 'serviceLevel')]
    #[Route('/{tenant_url}/service-level/view/{serviceLevel?}', name: 'app_service_level_view', methods: ['GET'])]
    public function view(
        #[MapEntity(expr: 'repository.findWithPreviousVersions(serviceLevel)')]
        ServiceLevel $serviceLevel,
        ServiceLevelFileViewResultSet $serviceLevelFileResultSet,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $serviceLevelFileResultSet->initialize($serviceLevel);
        return $this->render('service_level/view.html.twig', [
            'serviceLevel' => $serviceLevel,
            'files' => $serviceLevelFileResultSet,
        ]);
    }

    #[IsGranted('acl/ServiceLevel/add_edit')]
    #[Route(
        '/{tenant_url}/service-level/add/{session_tmpfile_uuid?}',
        name: 'app_service_level_add',
        methods: ['GET', 'POST'],
    )]
    public function add(
        Request $request,
        ServiceLevelManager $serviceLevelManager,
        Eaccpf $eacCpf,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        SessionFiles $sessionFiles,
        string $session_tmpfile_uuid = null,
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        $serviceLevel = $serviceLevelManager->newServiceLevel($user->getTenant($request));
        if ($session_tmpfile_uuid && $request->isMethod('GET')) {
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);
            $eacCpf->applyDataToEntity($uri, $serviceLevel);
        }

        $form = $this->createForm(
            ServiceLevelType::class,
            $serviceLevel,
            ['action' => $this->generateUrl(
                'app_service_level_add',
                ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid]
            )]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $serviceLevelManager->save($serviceLevel);

            return new JsonResponse(
                ServiceLevelResultSet::normalize($serviceLevel, ['index', 'action']),
                headers: ['X-Asalae-Success' => 'true']
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('service_level/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/ServiceLevel/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'serviceLevel')]
    #[IsGranted(EntityIsEditableVoter::class, 'serviceLevel')]
    #[Route(
        '/{tenant_url}/service-level/edit/{serviceLevel?}',
        name: 'app_service_level_edit',
        methods: ['GET', 'POST'],
    )]
    public function edit(
        Request $request,
        EntityManagerInterface $entityManager,
        ServiceLevel $serviceLevel,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $form = $this->createForm(
            ServiceLevelType::class,
            $serviceLevel,
            ['action' => $this->generateUrl('app_service_level_edit', ['serviceLevel' => $serviceLevel->getId()])]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($serviceLevel);
            $entityManager->flush();
            return new JsonResponse(
                ServiceLevelResultSet::normalize($serviceLevel, ['index', 'action']),
                headers: ['X-Asalae-Success' => 'true']
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('service_level/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/service-level/publish/{serviceLevel?}',
        name: 'app_service_level_publish',
        methods: ['POST'],
    )]
    #[IsGranted('acl/ServiceLevel/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'serviceLevel')]
    public function publish(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        WorkflowInterface $servicelevelStateMachine,
        ServiceLevel $serviceLevel,
    ): Response {
        $servicelevelStateMachine->apply($serviceLevel, VersionableEntityInterface::T_PUBLISH);

        return new JsonResponse(
            ServiceLevelResultSet::normalize($serviceLevel, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/service-level/revoke/{serviceLevel?}', name: 'app_service_level_revoke', methods: ['POST'])]
    #[IsGranted('acl/ServiceLevel/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'serviceLevel')]
    public function revoke(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        WorkflowInterface $servicelevelStateMachine,
        ServiceLevel $serviceLevel,
    ): Response {
        $servicelevelStateMachine->apply($serviceLevel, VersionableEntityInterface::T_REVOKE);

        return new JsonResponse(
            ServiceLevelResultSet::normalize($serviceLevel, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route(
        '/{tenant_url}/service-level/restore/{serviceLevel?}',
        name: 'app_service_level_restore',
        methods: ['POST'],
    )]
    #[IsGranted('acl/ServiceLevel/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'serviceLevel')]
    public function restore(
        WorkflowInterface $servicelevelStateMachine,
        ServiceLevel $serviceLevel,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $servicelevelStateMachine->apply($serviceLevel, VersionableEntityInterface::T_RECOVER);

        return new JsonResponse(
            ServiceLevelResultSet::normalize($serviceLevel, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route(
        '/{tenant_url}/service-level/activate/{serviceLevel?}',
        name: 'app_service_level_activate',
        methods: ['POST'],
    )]
    #[IsGranted('acl/ServiceLevel/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'serviceLevel')]
    public function activate(
        WorkflowInterface $servicelevelStateMachine,
        ServiceLevel $serviceLevel,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $servicelevelStateMachine->apply($serviceLevel, VersionableEntityInterface::T_ACTIVATE);

        return new JsonResponse(
            ServiceLevelResultSet::normalize($serviceLevel, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route(
        '/{tenant_url}/service-level/deactivate/{serviceLevel?}',
        name: 'app_service_level_deactivate',
        methods: ['POST'],
    )]
    #[IsGranted('acl/ServiceLevel/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'serviceLevel')]
    public function deactivate(
        WorkflowInterface $servicelevelStateMachine,
        ServiceLevel $serviceLevel,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $servicelevelStateMachine->apply($serviceLevel, VersionableEntityInterface::T_DEACTIVATE);

        return new JsonResponse(
            ServiceLevelResultSet::normalize($serviceLevel, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route(
        '/{tenant_url}/service-level/new-version/{serviceLevel?}',
        name: 'app_service_level_new_version',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/ServiceLevel/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'serviceLevel')]
    public function newVersion(
        Request $request,
        ServiceLevelManager $serviceLevelManager,
        ServiceLevel $serviceLevel,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $form = $this->createForm(
            ServiceLevelVersionType::class,
            options: [
                'action' => $this->generateUrl(
                    'app_service_level_new_version',
                    ['serviceLevel' => $serviceLevel->getId()]
                )
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $newVersion = $serviceLevelManager->newVersion($serviceLevel, $form->get('reason')->getData());

            return new JsonResponse(
                ServiceLevelResultSet::normalize($newVersion, ['index', 'action']),
                headers: [
                    'X-Asalae-Success' => 'true',
                    'X-Asalae-PreviousVersionId' => $serviceLevel->getId(),
                ]
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('service_level/version.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/service-level/delete/{serviceLevel?}',
        name: 'app_service_level_delete',
        methods: ['DELETE'],
    )]
    #[IsGranted('acl/ServiceLevel/delete')]
    #[IsGranted(EntityIsDeletableVoter::class, 'serviceLevel')]
    #[IsGranted(BelongsToTenantVoter::class, 'serviceLevel')]
    public function delete(
        EntityManagerInterface $entityManager,
        ServiceLevel $serviceLevel,
        ServiceLevelRepository $serviceLevelRepository,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $entityManager->remove($serviceLevel);
        $entityManager->flush();

        if ($previousVersion = $serviceLevelRepository->findPreviousVersion($serviceLevel)) {
            return new JsonResponse(
                ServiceLevelResultSet::normalize($previousVersion, ['index', 'action']),
                headers: [
                    'X-Asalae-Success' => 'true',
                    'X-Asalae-PreviousVersionEntity' => 'true',
                ]
            );
        }

        return new Response('done');
    }

    #[Route('/{tenant_url}/service-level/export', name: 'app_service_level_export', methods: ['GET'])]
    #[IsGranted('acl/ServiceLevel/export')]
    public function export(
        ServiceLevelRepository $serviceLevelRepository,
        ServiceLevelManager $serviceLevelManager,
        ServiceLevelResultSet $serviceLevelResultSet,
        Request $request,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        $tenant = $user->getTenant($request);
        $query = $serviceLevelRepository->queryForExport($tenant);
        $serviceLevelResultSet->appendFilters($query);

        $zip = $serviceLevelManager->export($query);
        $filename = sprintf(
            '%s_export.%s.refae',
            (new DateTime())->format('Y-m-d_His'),
            $serviceLevelManager->getFileName()
        );
        $headers = [
            'Content-Description' => 'File Transfer',
            'Content-Type' => 'application/zip',
            'Content-Length' => strlen($zip),
            'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
            'Expires' => '0',
            'Cache-Control' => 'must-revalidate',
        ];
        return new Response($zip, Response::HTTP_OK, $headers);
    }

    #[Route(
        '/{tenant_url}/service-level/import',
        name: 'app_service_level_import1',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/ServiceLevel/import')]
    public function import1(
        string $tenant_url,
        Request $request,
        SessionFiles $sessionFiles,
    ): Response {
        $form = $this->createForm(
            ServiceLevelImportType::class,
            options: ['action' => $this->generateUrl('app_service_level_import1', ['tenant_url' => $tenant_url])]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $session_tmpfile_uuid = $request->get('service_level_import')['file'] ?? null;
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);

            if (mime_content_type($uri) === 'application/zip') {
                $modalParams = [
                    'btnAcceptContent' => '<i class="fa fa-upload fa-space" aria-hidden="true"></i>'
                        . "<span>Importer</span>",
                    'btnCancelContent' => '<i class="fa fa-trash fa-space" aria-hidden="true"></i>'
                        . "<span>Annuler l'import</span>",
                    'btnCancelUrl' => $this->generateUrl(
                        'app_service_level_delete_import',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ];
                $response->headers->add([
                    'X-Asalae-Modal-Params' => json_encode($modalParams),
                    'X-Asalae-Step-Title' => json_encode("Récapitulatif avant import"),
                    'X-Asalae-Step' => $this->generateUrl(
                        'app_service_level_import2',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ]);
                return $response;
            } else {
                $form->get('file')
                    ->addError(new FormError("Ce fichier ne semble pas contenir des niveaux de service"));
            }
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('service_level/import1.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/service-level/import/{session_tmpfile_uuid}',
        name: 'app_service_level_import2',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/ServiceLevel/import')]
    public function import2(
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        Request $request,
        string $session_tmpfile_uuid,
        ServiceLevelManager $serviceLevelManager,
        ServiceLevelResultSetPreview $previewResultset,
        ImportZip $importZip,
    ): Response {
        $importZip->extract($session_tmpfile_uuid, $serviceLevelManager);
        $previewResultset->setDataFromArray($importZip->previewData);

        $form = $this->createForm(
            ServiceLevelImportConfirmType::class,
            null,
            [
                'action' => $this->generateUrl(
                    'app_service_level_import2',
                    ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                )
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $serviceLevelManager->import($importZip);
            $this->addFlash(
                'success',
                $importZip->importableCount > 1
                    ? "$importZip->importableCount niveaux de service importés avec succès"
                    : "$importZip->importableCount niveau de service importé avec succès"
            );
            $response->headers->add(['X-Asalae-Redirect' => $this->generateUrl('app_service_level')]);
            return $response;
        }

        $formatter = new IntlDateFormatter(
            Locale::getDefault(),
            IntlDateFormatter::FULL,
            IntlDateFormatter::SHORT
        );
        return $this->render('service_level/import2.html.twig', [
            'view_data' => [
                "Date du fichier d'export" => $formatter->format($importZip->exportDate),
                "Nombre de niveaux de service" => $importZip->dataCount,
                "Nombre de fichiers liés" => $importZip->fileCount,
                "Nombre d'étiquettes à importer" => count($importZip->labels),
            ],
            'content_preview_resultset' => $previewResultset,
            'summerize_data' => [
                "Nombre de niveaux de service impossibles à importer" => $importZip->failedCount,
                "Total importable" => $importZip->importableCount,
            ],
            'errors' => $importZip->errors,
            'fatalError' => $importZip->hasFatalError,
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/service-level/delete-import/{session_tmpfile_uuid}',
        name: 'app_service_level_delete_import',
        methods: ['DELETE'],
    )]
    #[IsGranted('acl/ServiceLevel/import')]
    public function deleteImport(
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        SessionFiles $sessionFiles,
        string $session_tmpfile_uuid,
    ): Response {
        $sessionFiles->delete($session_tmpfile_uuid);
        $response = new Response();
        $response->setContent('ok');
        $response->headers->add(['X-Asalae-Success' => 'true']);
        return $response;
    }

    #[Route('/{tenant_url}/service-level/csv', name: 'app_service_level_csv', methods: ['GET'])]
    #[IsGranted('acl/ServiceLevel/view')]
    public function csv(
        ExportCsv $exportCsv,
        Request $request,
        ServiceLevelResultSet $serviceLevelResultSet,
        ServiceLevelPublishedResultSet $serviceLevelPublishedResultSet,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        return $exportCsv->fromResultSet(
            $request->get('index_published')
                ? $serviceLevelPublishedResultSet
                : $serviceLevelResultSet
        );
    }

    #[Route(
        '/{tenant_url}/reader/service-level/{page?1}',
        name: 'app_reader_service_level',
        requirements: ['page' => '\d+']
    )]
    public function readerAgreement(
        PublicServiceLevelResultSet $table,
        TenantRepository $tenantRepository,
        string $tenant_url = null,
    ): Response {
        $tenant = $tenant_url ? $tenantRepository->findOneByBaseurl($tenant_url) : null;
        $table->initialize($tenant);
        return $this->render('reader/service-level.html.twig', [
            'table' => $table,
        ]);
    }

    #[Route('/{tenant_url}/reader/service-level/csv', name: 'app_reader_service_level_csv', methods: ['GET'])]
    public function readerCsv(
        PublicServiceLevelResultSet $publicServiceLevelResultSet,
        ExportCsv $exportCsv,
        TenantRepository $tenantRepository,
        string $tenant_url = null,
    ): Response {
        $tenant = $tenant_url ? $tenantRepository->findOneByBaseurl($tenant_url) : null;
        $publicServiceLevelResultSet->initialize($tenant);
        return $exportCsv->fromResultSet($publicServiceLevelResultSet);
    }
}
