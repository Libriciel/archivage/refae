<?php

namespace App\Controller\Tenant;

use App\Controller\DownloadFileTrait;
use App\Entity\ServiceLevelFile;
use App\Entity\ServiceLevel;
use App\Form\ServiceLevelFileType;
use App\ResultSet\ServiceLevelFileResultSet;
use App\Security\Voter\BelongsToTenantVoter;
use App\Security\Voter\EntityIsEditableVoter;
use App\Security\Voter\TenantUrlVoter;
use App\Service\Manager\FileManager;
use App\Service\SessionFiles;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class ServiceLevelFileController extends AbstractController
{
    use DownloadFileTrait;

    #[IsGranted('acl/ServiceLevel/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'serviceLevel')]
    #[IsGranted(EntityIsEditableVoter::class, 'serviceLevel')]
    #[Route(
        '/{tenant_url}/service-level/{serviceLevel}/file/{page?1}',
        name: 'app_service_level_file_index',
        requirements: ['page' => '\d+'],
        methods: ['GET'],
    )]
    public function index(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        ServiceLevel $serviceLevel,
        ServiceLevelFileResultSet $resultSet,
    ): Response {
        $resultSet->initialize($serviceLevel);
        return $this->render('service_level_file/index.html.twig', [
            'table' => $resultSet,
            'serviceLevel' => $serviceLevel,
        ]);
    }

    #[IsGranted('acl/ServiceLevel/view')]
    #[Route(
        '/{tenant_url}/service-level-file/{serviceLevelFile}/download',
        name: 'app_service_level_file_download',
        methods: ['GET']
    )]
    #[IsGranted(BelongsToTenantVoter::class, 'serviceLevelFile')]
    public function download(
        ServiceLevelFile $serviceLevelFile,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        return $this->getFileDownloadResponse($serviceLevelFile->getFile());
    }

    #[IsGranted('acl/ServiceLevel/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'serviceLevel')]
    #[IsGranted(EntityIsEditableVoter::class, 'serviceLevel')]
    #[Route(
        '/{tenant_url}/service-level/{serviceLevel}/file/add',
        name: 'app_service_level_file_add',
        methods: ['GET', 'POST'],
    )]
    public function add(
        ServiceLevel $serviceLevel,
        Request $request,
        EntityManagerInterface $entityManager,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        SessionFiles $sessionFiles,
    ): Response {
        $serviceLevelFile = new ServiceLevelFile();
        $serviceLevelFile->setServiceLevel($serviceLevel);
        $serviceLevelFile->setType(ServiceLevelFile::TYPE_OTHER);
        $form = $this->createForm(
            ServiceLevelFileType::class,
            $serviceLevelFile,
            ['action' => $this->generateUrl(
                'app_service_level_file_add',
                ['tenant_url' => $tenant_url, 'serviceLevel' => $serviceLevel->getId()]
            )]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $session_tmpfile_uuid = $request->get('service_level_file')['file'] ?? null;
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);
            $file = FileManager::setDataFromPath($uri);
            $serviceLevelFile->setFile($file);
            $entityManager->persist($serviceLevelFile);

            $entityManager->persist($file);
            $entityManager->flush();

            $response = new JsonResponse(ServiceLevelFileResultSet::normalize($serviceLevelFile, ['index', 'action']));
            $response->headers->add(['X-Asalae-Success' => 'true']);
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('service_level_file/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/ServiceLevel/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'serviceLevel')]
    #[IsGranted(EntityIsEditableVoter::class, 'serviceLevel')]
    #[Route(
        '/{tenant_url}/service-level/{serviceLevel}/file/edit/{serviceLevelFile}',
        name: 'app_service_level_file_edit',
        methods: ['GET', 'POST'],
    )]
    public function edit(
        Request $request,
        EntityManagerInterface $entityManager,
        ServiceLevel $serviceLevel,
        ServiceLevelFile $serviceLevelFile,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $form = $this->createForm(
            ServiceLevelFileType::class,
            $serviceLevelFile,
            [
                'action' => $this->generateUrl('app_service_level_file_edit', [
                    'tenant_url' => $tenant_url,
                    'serviceLevel' => $serviceLevel->getId(),
                    'serviceLevelFile' => $serviceLevelFile->getId(),
                ]),
                'withFileUpload' => false,
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($serviceLevelFile);
            $entityManager->flush();

            $response = new JsonResponse(ServiceLevelFileResultSet::normalize($serviceLevelFile, ['index', 'action']));
            $response->headers->add(['X-Asalae-Success' => 'true']);

            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('service_level_file/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/ServiceLevel/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'serviceLevel')]
    #[IsGranted(EntityIsEditableVoter::class, 'serviceLevel')]
    #[Route(
        '/{tenant_url}/service-level/{serviceLevel}/file/delete/{serviceLevelFile}',
        name: 'app_service_level_file_delete',
        methods: ['DELETE'],
    )]
    public function delete(
        ServiceLevelFile $serviceLevelFile,
        EntityManagerInterface $entityManager,
        /** @noinspection PhpUnusedParameterInspection **/
        ServiceLevel $serviceLevel,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $entityManager->remove($serviceLevelFile->getFile());
        $entityManager->flush();

        return new Response('done');
    }
}
