<?php

namespace App\Controller\Tenant;

use App\Entity\AccessUser;
use App\Entity\Ldap;
use App\Repository\AccessUserRepository;
use App\Repository\TenantRepository;
use App\Security\Voter\BelongsToTenantVoter;
use App\Security\Voter\TenantUrlVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class LdapController extends AbstractController
{
    #[Route('/{tenant_url}/ldap/search/{id}', name: 'app_ldap_search', methods: ['POST'])]
    #[IsGranted(BelongsToTenantVoter::class, 'ldap')]
    #[IsGranted('acl/User/add_edit')]
    public function search(
        string $tenant_url,
        Ldap $ldap,
        Request $request,
        AccessUserRepository $accessUserRepository,
        TenantRepository $tenantRepository,
    ): Response {
        $tenant = $tenantRepository->findOneByBaseurl($tenant_url);

        $usedAccesses = array_map(
            fn(AccessUser $au) => $au->getLdapUsername(),
            $accessUserRepository->getAccessesForLdap($ldap, $tenant)
        );

        $results = [];
        $entries = $ldap->connect()
            ?->query()
            ->whereContains($request->get('field'), $request->get('value'))
            ->paginate(10);
        unset($entries['count']);
        foreach ($entries ?? [] as $entry) {
            if (
                !is_array($entry)
                || empty($entry[$ldap->getUserLoginAttribute()][0])
                || in_array($entry[$ldap->getUserLoginAttribute()][0], $usedAccesses)
            ) {
                continue;
            }
            $results[] = $entry[$ldap->getUserLoginAttribute()][0];
        }
        sort($results);
        return new JsonResponse($results);
    }
}
