<?php

namespace App\Controller\Tenant;

use App\Entity\RuleType;
use App\Entity\User;
use App\Form\RuleTypeImportConfirmType;
use App\Form\RuleTypeImportType;
use App\Form\RuleTypeType;
use App\Repository\RuleTypeRepository;
use App\Repository\TenantRepository;
use App\ResultSet\RuleTypeResultSet;
use App\ResultSet\RuleTypeResultSetPreview;
use App\Security\Voter\BelongsToTenantVoter;
use App\Security\Voter\EntityIsDeletableVoter;
use App\Security\Voter\EntityIsEditableVoter;
use App\Security\Voter\TenantUrlVoter;
use App\Service\ExportCsv;
use App\Service\ImportZip;
use App\Service\Manager\RuleTypeManager;
use App\Service\SessionFiles;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use IntlDateFormatter;
use Locale;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class RuleTypeController extends AbstractController
{
    #[Route(
        '/{tenant_url}/rule-type/{page?1}',
        name: 'app_rule_type',
        requirements: ['page' => '\d+'],
        methods: ['GET']
    )]
    #[IsGranted('acl/RuleType/view')]
    public function index(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        RuleTypeResultSet $resultSet,
    ): Response {
        return $this->render('rule_type/index.html.twig', [
            'table' => $resultSet,
        ]);
    }

    #[Route('/{tenant_url}/rule-type/add', name: 'app_rule_type_add', methods: ['GET', 'POST'])]
    #[IsGranted('acl/RuleType/add_edit')]
    public function add(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        Request $request,
        EntityManagerInterface $entityManager,
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        $ruleType = new RuleType();
        $ruleType->setTenant($user->getTenant($request));
        $form = $this->createForm(
            RuleTypeType::class,
            $ruleType,
            ['action' => $this->generateUrl('app_rule_type_add')]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($ruleType);
            $entityManager->flush();
            $response = new JsonResponse(RuleTypeResultSet::normalize($ruleType, ['index', 'action']));
            $response->headers->add(['X-Asalae-Success' => 'true']);
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('rule_type/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/RuleType/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'ruleType')]
    #[IsGranted(EntityIsEditableVoter::class, 'ruleType')]
    #[Route('/{tenant_url}/rule-type/edit/{ruleType}', name: 'app_rule_type_edit', methods: ['GET', 'POST'])]
    public function edit(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        Request $request,
        EntityManagerInterface $entityManager,
        RuleType $ruleType,
    ): Response {
        $form = $this->createForm(
            RuleTypeType::class,
            $ruleType,
            [
                'action' => $this->generateUrl('app_rule_type_edit', ['ruleType' => $ruleType->getId()]),
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($ruleType);
            $entityManager->flush();

            $response = new JsonResponse(RuleTypeResultSet::normalize($ruleType, ['index', 'action']));
            $response->headers->add(['X-Asalae-Success' => 'true']);
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('rule_type/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/{tenant_url}/rule-type/view/{ruleType}', name: 'app_rule_type_view', methods: ['GET'])]
    #[IsGranted('acl/RuleType/view')]
    #[IsGranted(BelongsToTenantVoter::class, 'ruleType')]
    public function view(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        RuleType $ruleType,
    ): Response {
        return $this->render('rule_type/view.html.twig', [
            'ruleType' => $ruleType,
        ]);
    }

    #[Route('/{tenant_url}/rule-type/delete/{ruleType}', name: 'app_rule_type_delete', methods: ['DELETE'])]
    #[IsGranted('acl/RuleType/delete')]
    #[IsGranted(EntityIsDeletableVoter::class, 'ruleType')]
    #[IsGranted(BelongsToTenantVoter::class, 'ruleType')]
    public function delete(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        RuleType $ruleType,
        EntityManagerInterface $entityManager,
    ): Response {
        $entityManager->remove($ruleType);
        $entityManager->flush();
        $response = new Response();
        $response->setContent('done');
        return $response;
    }

    #[Route('/{tenant_url}/rule-type/csv', name: 'app_rule_type_csv', methods: ['GET'])]
    #[IsGranted('acl/RuleType/view')]
    public function csv(
        RuleTypeResultSet $ruleTypeResulSet,
        ExportCsv $exportCsv,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        return $exportCsv->fromResultSet($ruleTypeResulSet);
    }

    #[Route('/{tenant_url}/rule-type/export', name: 'app_rule_type_export', methods: ['GET'])]
    #[IsGranted('acl/RuleType/view')]
    public function export(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        RuleTypeRepository $ruleTypeRepository,
        TenantRepository $tenantRepository,
        RuleTypeManager $ruleTypeManager,
        RuleTypeResultSet $ruleTypeResultSet,
    ): Response {
        $tenant = $tenantRepository->findOneByBaseurl($tenant_url);
        $query = $ruleTypeRepository->queryForExport($tenant);
        $ruleTypeResultSet->appendFilters($query);

        $zip = $ruleTypeManager->export($query);
        $filename = sprintf(
            '%s_export.%s.refae',
            (new DateTime())->format('Y-m-d_His'),
            $ruleTypeManager->getFileName()
        );
        $headers = [
            'Content-Description' => 'File Transfer',
            'Content-Type' => 'application/zip',
            'Content-Length' => strlen($zip),
            'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
            'Expires' => '0',
            'Cache-Control' => 'must-revalidate',
        ];
        return new Response($zip, Response::HTTP_OK, $headers);
    }

    #[Route('/{tenant_url}/rule-type/import', name: 'app_rule_type_import1', methods: ['GET', 'POST'])]
    #[IsGranted('acl/RuleType/view')]
    public function import1(
        string $tenant_url,
        Request $request,
        SessionFiles $sessionFiles,
    ): Response {
        $form = $this->createForm(
            RuleTypeImportType::class,
            options: ['action' => $this->generateUrl('app_rule_type_import1', ['tenant_url' => $tenant_url])]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $session_tmpfile_uuid = $request->get('rule_type_import')['file'] ?? null;
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);

            if (mime_content_type($uri) === 'application/zip') {
                $modalParams = [
                    'btnAcceptContent' => '<i class="fa fa-upload fa-space" aria-hidden="true"></i>'
                        . "<span>Importer</span>",
                    'btnCancelContent' => '<i class="fa fa-trash fa-space" aria-hidden="true"></i>'
                        . "<span>Annuler l'import</span>",
                    'btnCancelUrl' => $this->generateUrl(
                        'app_rule_type_delete_import',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ];
                $response->headers->add([
                    'X-Asalae-Modal-Params' => json_encode($modalParams),
                    'X-Asalae-Step-Title' => json_encode("Récapitulatif avant import"),
                    'X-Asalae-Step' => $this->generateUrl(
                        'app_rule_type_import2',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ]);
                return $response;
            } else {
                $form->get('file')
                    ->addError(new FormError("Ce fichier ne semble pas contenir des étiquettes"));
            }
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('rule_type/import1.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/rule-type/import/{session_tmpfile_uuid}',
        name: 'app_rule_type_import2',
        methods: ['GET', 'POST']
    )]
    #[IsGranted('acl/RuleType/import')]
    public function import2(
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        Request $request,
        string $session_tmpfile_uuid,
        RuleTypeManager $ruleTypeManager,
        RuleTypeResultSetPreview $previewResultset,
        ImportZip $importZip,
    ): Response {
        $importZip->extract($session_tmpfile_uuid, $ruleTypeManager);
        $previewResultset->setDataFromArray($importZip->previewData);

        $form = $this->createForm(
            RuleTypeImportConfirmType::class,
            null,
            [
                'action' => $this->generateUrl(
                    'app_rule_type_import2',
                    ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                )
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $ruleTypeManager->import($importZip);
            $this->addFlash(
                'success',
                $importZip->importableCount > 1
                    ? "$importZip->importableCount types de règles de gestion importés avec succès"
                    : "$importZip->importableCount type de règles de gestion importé avec succès"
            );
            $response->headers->add(['X-Asalae-Redirect' => $this->generateUrl('app_rule_type')]);
            return $response;
        }

        $formatter = new IntlDateFormatter(
            Locale::getDefault(),
            IntlDateFormatter::FULL,
            IntlDateFormatter::SHORT
        );
        return $this->render('rule_type/import2.html.twig', [
            'view_data' => [
                "Date du fichier d'export" => $formatter->format($importZip->exportDate),
                "Nombre de règles de gestion" => $importZip->dataCount,
            ],
            'content_preview_resultset' => $previewResultset,
            'summerize_data' => [
                "Nombre de règles de gestion impossibles à importer" => $importZip->failedCount,
                "Total importable" => $importZip->importableCount,
            ],
            'errors' => $importZip->errors,
            'fatalError' => $importZip->hasFatalError,
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/rule-type/delete-import/{session_tmpfile_uuid}',
        name: 'app_rule_type_delete_import',
        methods: ['DELETE'],
    )]
    #[IsGranted('acl/RuleType/import')]
    public function deleteImport(
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        string $session_tmpfile_uuid,
        SessionFiles $sessionFiles,
    ): Response {
        $sessionFiles->delete($session_tmpfile_uuid);
        $response = new Response();
        $response->setContent('ok');
        $response->headers->add(['X-Asalae-Success' => 'true']);
        return $response;
    }
}
