<?php

namespace App\Controller\Tenant;

use App\Entity\Authority;
use App\Entity\User;
use App\Entity\VersionableEntityInterface;
use App\Form\AuthorityAddByXmlType;
use App\Form\AuthorityImportConfirmType;
use App\Form\AuthorityImportType;
use App\Form\AuthorityType;
use App\Form\AuthorityVersionType;
use App\Repository\AuthorityRepository;
use App\Repository\TenantRepository;
use App\ResultSet\AuthorityFileViewResultSet;
use App\ResultSet\AuthorityPublishedResultSet;
use App\ResultSet\AuthorityResultSet;
use App\ResultSet\AuthorityResultSetPreview;
use App\ResultSet\PublicAuthorityResultSet;
use App\Security\Voter\BelongsToTenantVoter;
use App\Security\Voter\EntityIsDeletableVoter;
use App\Security\Voter\EntityIsEditableVoter;
use App\Security\Voter\TenantUrlVoter;
use App\Service\Eaccpf;
use App\Service\ExportCsv;
use App\Service\ImportZip;
use App\Service\Manager\AuthorityManager;
use App\Service\SessionFiles;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use IntlDateFormatter;
use Locale;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Workflow\WorkflowInterface;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class AuthorityController extends AbstractController
{
    #[IsGranted('acl/Authority/view')]
    #[Route(
        '/{tenant_url}/authority/{page?1}',
        name: 'app_authority',
        requirements: ['page' => '\d+'],
        methods: ['GET'],
    )]
    public function index(
        AuthorityResultSet $table,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        return $this->render('authority/index.html.twig', [
            'table' => $table,
        ]);
    }

    #[IsGranted('acl/Authority/view')]
    #[Route(
        '/{tenant_url}/authority-published/{page?1}',
        name: 'app_authority_index_published',
        requirements: ['page' => '\d+'],
        methods: ['GET'],
    )]
    public function indexPublished(
        string $tenant_url,
        AuthorityPublishedResultSet $table,
    ): Response {
        return $this->render('authority/index_published.html.twig', [
            'table' => $table,
            'tenant_url' => $tenant_url,
        ]);
    }

    #[IsGranted('acl/Authority/view')]
    #[IsGranted(BelongsToTenantVoter::class, 'authority')]
    #[Route('/{tenant_url}/authority/view/{authority?}', name: 'app_authority_view', methods: ['GET'])]
    public function view(
        #[MapEntity(expr: 'repository.findWithPreviousVersions(authority)')]
        Authority $authority,
        AuthorityFileViewResultSet $authorityFileResultSet,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $authorityFileResultSet->initialize($authority);
        return $this->render('authority/view.html.twig', [
            'authority' => $authority,
            'files' => $authorityFileResultSet,
        ]);
    }

    #[IsGranted('acl/Authority/add_edit')]
    #[Route('/{tenant_url}/authority/add/{session_tmpfile_uuid?}', name: 'app_authority_add', methods: ['GET', 'POST'])]
    public function add(
        Request $request,
        AuthorityManager $authorityManager,
        Eaccpf $eacCpf,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        SessionFiles $sessionFiles,
        string $session_tmpfile_uuid = null,
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        $authority = $authorityManager->newAuthority($user->getTenant($request));
        if ($session_tmpfile_uuid && $request->isMethod('GET')) {
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);
            $eacCpf->applyDataToEntity($uri, $authority);
        }

        $form = $this->createForm(
            AuthorityType::class,
            $authority,
            ['action' => $this->generateUrl(
                'app_authority_add',
                ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid]
            )]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $authorityManager->save($authority, $session_tmpfile_uuid);

            return new JsonResponse(
                AuthorityResultSet::normalize($authority, ['index', 'action']),
                headers: ['X-Asalae-Success' => 'true']
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('authority/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/Authority/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'authority')]
    #[IsGranted(EntityIsEditableVoter::class, 'authority')]
    #[Route('/{tenant_url}/authority/edit/{authority?}', name: 'app_authority_edit', methods: ['GET', 'POST'])]
    public function edit(
        Request $request,
        EntityManagerInterface $entityManager,
        Authority $authority,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $form = $this->createForm(
            AuthorityType::class,
            $authority,
            [
                'action' => $this->generateUrl('app_authority_edit', ['authority' => $authority->getId()])
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($authority);
            $entityManager->flush();
            return new JsonResponse(
                AuthorityResultSet::normalize($authority, ['index', 'action']),
                headers: ['X-Asalae-Success' => 'true']
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('authority/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/{tenant_url}/authority/publish/{authority?}', name: 'app_authority_publish', methods: ['POST'])]
    #[IsGranted('acl/Authority/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'authority')]
    public function publish(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        WorkflowInterface $authorityStateMachine,
        Authority $authority,
    ): Response {
        $authorityStateMachine->apply($authority, VersionableEntityInterface::T_PUBLISH);

        return new JsonResponse(
            AuthorityResultSet::normalize($authority, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/authority/revoke/{authority?}', name: 'app_authority_revoke', methods: ['POST'])]
    #[IsGranted('acl/Authority/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'authority')]
    public function revoke(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        WorkflowInterface $authorityStateMachine,
        Authority $authority,
    ): Response {
        $authorityStateMachine->apply($authority, VersionableEntityInterface::T_REVOKE);

        return new JsonResponse(
            AuthorityResultSet::normalize($authority, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/authority/restore/{authority?}', name: 'app_authority_restore', methods: ['POST'])]
    #[IsGranted('acl/Authority/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'authority')]
    public function restore(
        WorkflowInterface $authorityStateMachine,
        Authority $authority,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $authorityStateMachine->apply($authority, VersionableEntityInterface::T_RECOVER);

        return new JsonResponse(
            AuthorityResultSet::normalize($authority, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/authority/activate/{authority?}', name: 'app_authority_activate', methods: ['POST'])]
    #[IsGranted('acl/Authority/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'authority')]
    public function activate(
        WorkflowInterface $authorityStateMachine,
        Authority $authority,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $authorityStateMachine->apply($authority, VersionableEntityInterface::T_ACTIVATE);

        return new JsonResponse(
            AuthorityResultSet::normalize($authority, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route('/{tenant_url}/authority/deactivate/{authority?}', name: 'app_authority_deactivate', methods: ['POST'])]
    #[IsGranted('acl/Authority/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'authority')]
    public function deactivate(
        WorkflowInterface $authorityStateMachine,
        Authority $authority,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $authorityStateMachine->apply($authority, VersionableEntityInterface::T_DEACTIVATE);

        return new JsonResponse(
            AuthorityResultSet::normalize($authority, ['index', 'action']),
            headers: ['X-Asalae-Success' => 'true']
        );
    }

    #[Route(
        '/{tenant_url}/authority/new-version/{authority?}',
        name: 'app_authority_new_version',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/Authority/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'authority')]
    public function newVersion(
        Request $request,
        AuthorityManager $authorityManager,
        Authority $authority,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $form = $this->createForm(
            AuthorityVersionType::class,
            options: ['action' => $this->generateUrl('app_authority_new_version', ['authority' => $authority->getId()])]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $newVersion = $authorityManager->newVersion($authority, $form->get('reason')->getData());

            return new JsonResponse(
                AuthorityResultSet::normalize($newVersion, ['index', 'action']),
                headers: [
                    'X-Asalae-Success' => 'true',
                    'X-Asalae-PreviousVersionId' => $authority->getId(),
                ]
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('authority/version.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/{tenant_url}/authority/delete/{authority?}', name: 'app_authority_delete', methods: ['DELETE'])]
    #[IsGranted('acl/Authority/delete')]
    #[IsGranted(EntityIsDeletableVoter::class, 'authority')]
    #[IsGranted(BelongsToTenantVoter::class, 'authority')]
    public function delete(
        EntityManagerInterface $entityManager,
        Authority $authority,
        AuthorityRepository $authorityRepository,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $entityManager->remove($authority);
        $entityManager->flush();

        if ($previousVersion = $authorityRepository->findPreviousVersion($authority)) {
            return new JsonResponse(
                AuthorityResultSet::normalize($previousVersion, ['index', 'action']),
                headers: [
                    'X-Asalae-Success' => 'true',
                    'X-Asalae-PreviousVersionEntity' => 'true',
                ]
            );
        }

        return new Response('done');
    }

    #[Route('/{tenant_url}/authority/export', name: 'app_authority_export', methods: ['GET'])]
    #[IsGranted('acl/Authority/export')]
    public function export(
        AuthorityRepository $authorityRepository,
        AuthorityManager $authorityManager,
        AuthorityResultSet $authorityResultSet,
        Request $request,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        $tenant = $user->getTenant($request);
        $query = $authorityRepository->queryForExport($tenant);
        $authorityResultSet->appendFilters($query);

        $zip = $authorityManager->export($query);
        $filename = sprintf(
            '%s_export.%s.refae',
            (new DateTime())->format('Y-m-d_His'),
            $authorityManager->getFileName()
        );
        $headers = [
            'Content-Description' => 'File Transfer',
            'Content-Type' => 'application/zip',
            'Content-Length' => strlen($zip),
            'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
            'Expires' => '0',
            'Cache-Control' => 'must-revalidate',
        ];
        return new Response($zip, Response::HTTP_OK, $headers);
    }

    #[Route(
        '/{tenant_url}/authority/import',
        name: 'app_authority_import1',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/Authority/import')]
    public function import1(
        string $tenant_url,
        Request $request,
        SessionFiles $sessionFiles,
    ): Response {
        $form = $this->createForm(
            AuthorityImportType::class,
            options: ['action' => $this->generateUrl('app_authority_import1', ['tenant_url' => $tenant_url])]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $session_tmpfile_uuid = $request->get('authority_import')['file'] ?? null;
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);

            if (mime_content_type($uri) === 'application/zip') {
                $modalParams = [
                    'btnAcceptContent' => '<i class="fa fa-upload fa-space" aria-hidden="true"></i>'
                        . "<span>Importer</span>",
                    'btnCancelContent' => '<i class="fa fa-trash fa-space" aria-hidden="true"></i>'
                        . "<span>Annuler l'import</span>",
                    'btnCancelUrl' => $this->generateUrl(
                        'app_authority_delete_import',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ];
                $response->headers->add([
                    'X-Asalae-Modal-Params' => json_encode($modalParams),
                    'X-Asalae-Step-Title' => json_encode("Récapitulatif avant import"),
                    'X-Asalae-Step' => $this->generateUrl(
                        'app_authority_import2',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ]);
                return $response;
            } else {
                $form->get('file')
                    ->addError(new FormError("Ce fichier ne semble pas contenir des notices d'autorité"));
            }
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('authority/import1.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/authority/add_by_xml',
        name: 'app_authority_add_by_xml',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/Authority/add_edit')]
    public function addByXml(
        string $tenant_url,
        Request $request,
        Eaccpf $eacCpf,
        SessionFiles $sessionFiles,
    ): Response {
        $form = $this->createForm(
            AuthorityAddByXmlType::class,
            options: ['action' => $this->generateUrl('app_authority_add_by_xml', ['tenant_url' => $tenant_url])]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $session_tmpfile_uuid = $request->get('authority_add_by_xml')['file'] ?? null;
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);

            if ($eacCpf->isValid($uri)) {
                $modalParams = [
                    'btnAcceptContent' => '<i class="fa fa-upload fa-space" aria-hidden="true"></i>'
                        . "<span>Importer</span>",
                    'btnCancelContent' => '<i class="fa fa-trash fa-space" aria-hidden="true"></i>'
                        . "<span>Annuler l'import</span>",
                    'btnCancelUrl' => $this->generateUrl(
                        'app_authority_delete_import',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ];
                $response->headers->add([
                    'X-Asalae-Modal-Params' => json_encode($modalParams),
                    'X-Asalae-Step-Title' => json_encode("Ajout à partir d'un fichier xml"),
                    'X-Asalae-Step' => $this->generateUrl(
                        'app_authority_add',
                        ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ]);
                return $response;
            } else {
                $form->get('file')
                    ->addError(new FormError("Ce fichier ne semble pas contenir des notices d'autorité"));
            }
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('authority/import1.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/authority/import/{session_tmpfile_uuid}',
        name: 'app_authority_import2',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/Authority/import')]
    public function import2(
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        Request $request,
        string $session_tmpfile_uuid,
        AuthorityManager $authorityManager,
        AuthorityResultSetPreview $previewResultset,
        ImportZip $importZip,
    ): Response {
        $importZip->extract($session_tmpfile_uuid, $authorityManager);
        $previewResultset->setDataFromArray($importZip->previewData);

        $form = $this->createForm(
            AuthorityImportConfirmType::class,
            null,
            [
                'action' => $this->generateUrl(
                    'app_authority_import2',
                    ['tenant_url' => $tenant_url, 'session_tmpfile_uuid' => $session_tmpfile_uuid],
                )
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $authorityManager->import($importZip);
            $this->addFlash(
                'success',
                $importZip->importableCount > 1
                    ? "$importZip->importableCount notices d'autorité importées avec succès"
                    : "$importZip->importableCount notice d'autorité importée avec succès"
            );
            $response->headers->add(['X-Asalae-Redirect' => $this->generateUrl('app_authority')]);
            return $response;
        }

        $formatter = new IntlDateFormatter(
            Locale::getDefault(),
            IntlDateFormatter::FULL,
            IntlDateFormatter::SHORT
        );
        return $this->render('authority/import2.html.twig', [
            'view_data' => [
                "Date du fichier d'export" => $formatter->format($importZip->exportDate),
                "Nombre de notices d'autorité" => $importZip->dataCount,
                "Nombre de fichiers liés" => $importZip->fileCount,
                "Nombre d'étiquettes à importer" => count($importZip->labels),
            ],
            'content_preview_resultset' => $previewResultset,
            'summerize_data' => [
                "Nombre de notices d'autorité impossibles à importer" => $importZip->failedCount,
                "Total importable" => $importZip->importableCount,
            ],
            'errors' => $importZip->errors,
            'fatalError' => $importZip->hasFatalError,
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/{tenant_url}/authority/delete-import/{session_tmpfile_uuid}',
        name: 'app_authority_delete_import',
        methods: ['DELETE'],
    )]
    #[IsGranted('acl/Authority/import')]
    public function deleteImport(
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        SessionFiles $sessionFiles,
        string $session_tmpfile_uuid,
    ): Response {
        $sessionFiles->delete($session_tmpfile_uuid);
        $response = new Response();
        $response->setContent('ok');
        $response->headers->add(['X-Asalae-Success' => 'true']);
        return $response;
    }

    #[Route('/{tenant_url}/authority/csv', name: 'app_authority_csv', methods: ['GET'])]
    #[IsGranted('acl/Authority/view')]
    public function csv(
        AuthorityResultSet $authorityResultSet,
        AuthorityPublishedResultSet $authorityPublishedResultSet,
        ExportCsv $exportCsv,
        Request $request,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        return $exportCsv->fromResultSet(
            $request->get('index_published')
                ? $authorityPublishedResultSet
                : $authorityResultSet
        );
    }

    #[Route(
        '/{tenant_url}/reader/authority/{page?1}',
        name: 'app_reader_authority',
        requirements: ['page' => '\d+']
    )]
    public function readerAuthority(
        PublicAuthorityResultSet $table,
        TenantRepository $tenantRepository,
        string $tenant_url = null,
    ): Response {
        $tenant = $tenant_url ? $tenantRepository->findOneByBaseurl($tenant_url) : null;
        $table->initialize($tenant);
        return $this->render('reader/authority.html.twig', [
            'table' => $table,
        ]);
    }

    #[Route('/{tenant_url}/reader/authority/csv', name: 'app_reader_authority_csv', methods: ['GET'])]
    public function readerCsv(
        PublicAuthorityResultSet $publicAuthorityResultSet,
        ExportCsv $exportCsv,
        TenantRepository $tenantRepository,
        string $tenant_url = null,
    ): Response {
        $tenant = $tenant_url ? $tenantRepository->findOneByBaseurl($tenant_url) : null;
        $publicAuthorityResultSet->initialize($tenant);
        return $exportCsv->fromResultSet($publicAuthorityResultSet);
    }
}
