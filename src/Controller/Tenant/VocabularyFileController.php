<?php

namespace App\Controller\Tenant;

use App\Controller\DownloadFileTrait;
use App\Entity\VocabularyFile;
use App\Entity\Vocabulary;
use App\Form\VocabularyFileType;
use App\ResultSet\VocabularyFileResultSet;
use App\Security\Voter\BelongsToTenantVoter;
use App\Security\Voter\EntityIsEditableVoter;
use App\Security\Voter\TenantUrlVoter;
use App\Service\Manager\FileManager;
use App\Service\SessionFiles;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted(TenantUrlVoter::class, 'tenant_url')]
class VocabularyFileController extends AbstractController
{
    use DownloadFileTrait;

    #[IsGranted('acl/Vocabulary/view')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    #[IsGranted(EntityIsEditableVoter::class, 'vocabulary')]
    #[Route(
        '/{tenant_url}/vocabulary/{vocabulary}/file/{page?1}',
        name: 'app_vocabulary_file_index',
        requirements: ['page' => '\d+'],
        methods: ['GET']
    )]
    public function index(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        Vocabulary $vocabulary,
        VocabularyFileResultSet $resultSet,
    ): Response {
        $resultSet->setVocabulary($vocabulary);

        return $this->render('vocabulary_file/index.html.twig', [
            'table' => $resultSet,
            'vocabulary' => $vocabulary,
        ]);
    }

    #[IsGranted('acl/Vocabulary/view')]
    #[Route(
        '/{tenant_url}/vocabulary-file/{vocabularyFile}/download',
        name: 'app_vocabulary_file_download',
        methods: ['GET']
    )]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabularyFile')]
    public function download(
        VocabularyFile $vocabularyFile,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        return $this->getFileDownloadResponse($vocabularyFile->getFile());
    }

    #[IsGranted('acl/Vocabulary/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    #[IsGranted(EntityIsEditableVoter::class, 'vocabulary')]
    #[Route(
        '/{tenant_url}/vocabulary/{vocabulary}/file/add',
        name: 'app_vocabulary_file_add',
        methods: ['GET', 'POST']
    )]
    public function add(
        Vocabulary $vocabulary,
        Request $request,
        EntityManagerInterface $entityManager,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        SessionFiles $sessionFiles,
    ): Response {
        $vocabularyFile = new VocabularyFile(vocabulary: $vocabulary);
        $vocabularyFile->setType(VocabularyFile::OTHER);
        $form = $this->createForm(
            VocabularyFileType::class,
            $vocabularyFile,
            [
                'action' => $this->generateUrl(
                    'app_vocabulary_file_add',
                    ['tenant_url' => $tenant_url, 'vocabulary' => $vocabulary->getId()]
                )
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            // TODO: Manager ?
            $session_tmpfile_uuid = $request->get('vocabulary_file')['file'] ?? null;
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);
            $file = FileManager::setDataFromPath($uri);
            $vocabularyFile->setFile($file);

            $entityManager->persist($file);
            $entityManager->persist($vocabularyFile);
            $entityManager->flush();

            $response = new JsonResponse(
                VocabularyFileResultSet::normalize($vocabularyFile, ['index', 'action'])
            );
            $response->headers->add(['X-Asalae-Success' => 'true']);
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('vocabulary_file/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/Vocabulary/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    #[IsGranted(EntityIsEditableVoter::class, 'vocabulary')]
    #[Route(
        '/{tenant_url}/vocabulary/{vocabulary}/file/edit/{vocabularyFile}',
        name: 'app_vocabulary_file_edit',
        methods: ['GET', 'POST']
    )]
    public function edit(
        Request $request,
        EntityManagerInterface $entityManager,
        Vocabulary $vocabulary,
        VocabularyFile $vocabularyFile,
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
    ): Response {
        $form = $this->createForm(
            VocabularyFileType::class,
            $vocabularyFile,
            [
                'action' => $this->generateUrl('app_vocabulary_file_edit', [
                    'tenant_url' => $tenant_url,
                    'vocabulary' => $vocabulary->getId(),
                    'vocabularyFile' => $vocabularyFile->getId(),
                ]),
                'withFileUpload' => false,
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($vocabularyFile);
            $entityManager->flush();

            $response = new JsonResponse(
                VocabularyFileResultSet::normalize($vocabularyFile, ['index', 'action'])
            );
            $response->headers->add(['X-Asalae-Success' => 'true']);

            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('vocabulary_file/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[IsGranted('acl/Vocabulary/add_edit')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    #[IsGranted(EntityIsEditableVoter::class, 'vocabulary')]
    #[Route(
        '/{tenant_url}/vocabulary/{vocabulary}/file/delete/{vocabularyFile}',
        name: 'app_vocabulary_file_delete',
        methods: ['DELETE']
    )]
    public function delete(
        /** @noinspection PhpUnusedParameterInspection **/
        string $tenant_url,
        Vocabulary $vocabulary,
        VocabularyFile $vocabularyFile,
        EntityManagerInterface $em,
    ): Response {
        $em->remove($vocabularyFile);
        $em->flush();

        return new Response('done');
    }
}
