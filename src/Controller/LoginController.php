<?php

/** @noinspection PhpUnusedParameterInspection */

namespace App\Controller;

use App\Entity\Openid;
use App\Entity\Tenant;
use App\Entity\UserCreationRequest;
use App\Entity\UserPasswordRenewToken;
use App\Form\ForgottenPasswordType;
use App\Form\LoginPasswordType;
use App\Form\LoginUsernameType;
use App\Form\PublicLoginUsernameType;
use App\Repository\AccessUserRepository;
use App\Repository\TenantRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\EventListener\AbstractSessionListener;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\SecurityRequestAttributes;

class LoginController extends AbstractController
{
    #[Route('/public/login', name: 'public_login', priority: 1)]
    public function login(
        Request $request,
        UserRepository $userRepository,
        TenantRepository $tenantRepository,
    ): Response {
        $form = $this->createForm(PublicLoginUsernameType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $username = $form->get('username')->getData();
            return $this->redirectToRoute(
                'public_login_username',
                ['username' => $username]
            );
        }

        return $this->render('login/public-login-username.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/public/login/{username}', name: 'public_login_username', priority: 1)]
    public function loginUsername(
        string $username,
        Request $request,
    ): Response {
        $form = $this->createForm(LoginPasswordType::class);

        $error = $request->getSession()->get(SecurityRequestAttributes::AUTHENTICATION_ERROR);
        if ($error instanceof Exception) {
            $form->get('password')->addError(new FormError($error->getMessage()));
            $request->getSession()->remove(SecurityRequestAttributes::AUTHENTICATION_ERROR);
        }

        return $this->render('login/public-login-password.html.twig', [
            'form' => $form->createView(),
            'username' => $username,
            'back_url' => $this->generateUrl('public_login'),
        ]);
    }

    #[Route('/{tenant_url}/login', name: 'app_tenant_login')]
    public function tenantLogin(
        string $tenant_url,
        Request $request,
        AccessUserRepository $accessUserRepository,
        TenantRepository $tenantRepository,
    ): Response {
        $form = $this->createForm(LoginUsernameType::class);
        $form->handleRequest($request);
        $tenant = $tenantRepository->findOneActiveByBaseurl($tenant_url);
        if (!$tenant) {
            throw $this->createNotFoundException();
        }

        if ($form->isSubmitted()) {
            $username = $form->get('username')->getData();
            return $this->redirectToRoute(
                'app_tenant_login_username',
                ['tenant_url' => $tenant_url, 'username' => $username]
            );
        }

        return $this->render('login/login-username.html.twig', [
            'form' => $form->createView(),
            'tenant_configuration' => $tenant->getConfiguration(),
        ]);
    }

    #[Route('/{tenant_url}/login/{username}', name: 'app_tenant_login_username')]
    public function tenantLoginUsername(
        string $tenant_url,
        string $username,
        Request $request,
        TenantRepository $tenantRepository,
    ): Response {
        $form = $this->createForm(LoginPasswordType::class);
        $tenant = $tenantRepository->findOneActiveByBaseurl($tenant_url);
        if (!$tenant) {
            throw $this->createNotFoundException();
        }

        $error = $request->getSession()->get(SecurityRequestAttributes::AUTHENTICATION_ERROR);
        if ($error instanceof Exception) {
            $form->get('password')->addError(new FormError($error->getMessage()));
            $request->getSession()->remove(SecurityRequestAttributes::AUTHENTICATION_ERROR);
        }

        return $this->render('login/login-password.html.twig', [
            'form' => $form->createView(),
            'username' => $username,
            'tenant_configuration' => $tenant->getConfiguration(),
            'back_url' => $this->generateUrl('app_tenant_login', ['tenant_url' => $tenant_url]),
        ]);
    }

    #[Route('/{tenant_url}/openid/{identifier}', name: 'app_tenant_openid')]
    public function tenantOpenid(
        string $tenant_url,
        Openid $openid,
    ): Response {
        $oidc = $openid->getOpenidClient();
        $oidc->setRedirectURL(
            $this->generateUrl(
                'app_tenant_openid_redirect',
                ['tenant_url' => $tenant_url, 'openid' => $openid->getId()],
                UrlGeneratorInterface::ABSOLUTE_URL
            )
        );
        $oidc->authenticate();

        if ($oidc->redirectEndpoint) {
            return $this->redirect($oidc->redirectEndpoint);
        }

        return $this->redirectToRoute('app_tenant_login', [$tenant_url]);
    }

    #[Route('/{tenant_url}/openid-redirect/{openid}', name: 'app_tenant_openid_redirect')]
    public function tenantOpenidRedirect(
        string $tenant_url,
        Openid $openid,
    ): Response {
        return $this->redirectToRoute('app_tenant_login', [$tenant_url]);
    }

    #[Route('/public/openid/{identifier}', name: 'public_openid', priority: 1)]
    public function openid(
        Openid $openid,
    ): Response {
        $oidc = $openid->getOpenidClient();
        $oidc->setRedirectURL(
            $this->generateUrl(
                'public_openid_redirect',
                ['openid' => $openid->getId()],
                UrlGeneratorInterface::ABSOLUTE_URL
            )
        );
        $oidc->authenticate();

        if ($oidc->redirectEndpoint) {
            return $this->redirect($oidc->redirectEndpoint);
        }

        return $this->redirectToRoute('public_login');
    }

    #[Route('/public/openid-redirect/{openid}', name: 'public_openid_redirect', priority: 1)]
    public function openidRedirect(
        Openid $openid,
    ): Response {
        return $this->redirectToRoute('public_login');
    }

    #[Route('/public/forgotten-password', name: 'public_forgotten_password')]
    public function forgottenPassword(
        Request $request,
        EntityManagerInterface $entityManager,
        MailerInterface $mailer,
        UserRepository $userRepository,
        TenantRepository $tenantRepository,
    ): Response {
        $userCreationRequest =  new UserCreationRequest();
        if ($username = $request->get('username')) {
            $userCreationRequest->setUsername($username);
        }
        $form = $this->createForm(ForgottenPasswordType::class, $userCreationRequest);
        $form->handleRequest($request);

        $paramsUrl = ['username' => $username];
        if ($tenantUrl = $request->get('tenant_url')) {
            $tenant = $tenantRepository->findOneActiveByBaseurl($tenantUrl);
            $paramsUrl['tenant_url'] = $tenantUrl;
            $backUrl = $this->generateUrl(
                'app_tenant_login_username',
                $paramsUrl,
                UrlGeneratorInterface::ABSOLUTE_URL,
            );
        } else {
            $backUrl = $this->generateUrl(
                'public_login_username',
                $paramsUrl,
                UrlGeneratorInterface::ABSOLUTE_URL,
            );
        }

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UserCreationRequest $userCreationRequest */
            $userCreationRequest = $form->getData();

            $user = $userRepository->findByUsernameEmail(
                $userCreationRequest->getUsername(),
                $userCreationRequest->getEmail()
            );
            if ($user) {
                $userPasswordRenewToken = new UserPasswordRenewToken();
                $userPasswordRenewToken->setUser($user);
                $entityManager->persist($userPasswordRenewToken);
                $entityManager->flush();
                $this->sendForgottenPasswordMail($mailer, $userPasswordRenewToken, $backUrl, $tenant ?? null);
            }

            $paramsUrl['forgotten'] = 'true';
            if ($tenantUrl) {
                return $this->redirectToRoute('app_tenant_login_username', $paramsUrl);
            } else {
                return $this->redirectToRoute('public_login_username', $paramsUrl);
            }
        }

        return $this->render('login/forgotten-password.html.twig', [
            'form' => $form->createView(),
            'back_url' => $backUrl,
        ], $response);
    }

    private function sendForgottenPasswordMail(
        MailerInterface $mailer,
        UserPasswordRenewToken $userPasswordRenewToken,
        string $urlAlt,
        ?Tenant $tenant,
    ): void {
        $user = $userPasswordRenewToken->getUser();
        $configuration = $tenant?->getConfiguration();
        $mailPrefix = $configuration?->getMailPrefix() ?: '[refae]';
        $url = $this->generateUrl(
            'public_user_select_password',
            ['token' => $userPasswordRenewToken->getToken()],
            UrlGeneratorInterface::ABSOLUTE_URL,
        );
        $email = (new TemplatedEmail())
            ->from($_ENV['MAILER_FROM'] ?? 'refae@test.fr')
            ->to($user->getEmail())
            ->subject($mailPrefix . 'Récupération de votre mot de passe')
            ->htmlTemplate('email/forgotten-password.html.twig')
            ->context([
                'user' => $user,
                'url' => $url,
                'url_alt' => $urlAlt,
                'tenant' => $tenant,
                'signature' => $configuration?->getMailSignature(),
            ])
        ;
        $mailer->send($email);
    }

    #[Route('/public/after-forgotten-password', name: 'public_after_forgotten_password')]
    public function afterForgottenPassword(): Response
    {
        return $this->render('login/forgotten-password.html.twig');
    }

    #[Route('/{tenant_url}/logo-login/{filename}', name: 'public_tenant_logo_login')]
    public function tenantLogoLogin(
        string $tenant_url,
        TenantRepository $tenantRepository,
    ): Response {
        $tenant = $tenantRepository->findOneActiveByBaseurl($tenant_url);
        if (!$tenant) {
            throw $this->createNotFoundException();
        }
        $tenantConfiguration = $tenant->getConfiguration();
        if (!$tenantConfiguration) {
            throw $this->createNotFoundException();
        }
        $logoLogin = $tenantConfiguration->getLogoLogin();
        if (!$logoLogin) {
            throw $this->createNotFoundException();
        }
        $response = new Response();
        $response
            ->setContent(stream_get_contents($logoLogin->getContent()))
        ;
        $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
        $response->headers->add(['cache-control' => 'public, max-age=604800, immutable']);
        $response->headers->add(['content-type' => $logoLogin->getMime()]);
        $response->headers->add(['content-length' => $logoLogin->getSize()]);
        return $response;
    }
}
