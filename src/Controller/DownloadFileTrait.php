<?php

namespace App\Controller;

use App\Entity\File;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

trait DownloadFileTrait
{
    /**
     * @param File $file
     * @return Response
     */
    protected function getFileDownloadResponse(File $file): Response
    {
        // Return a response with a specific content
        $response = new Response(stream_get_contents($file->getContent()));

        // Create the disposition of the file
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $file->getName()
        );

        // Set the content disposition
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', $file->getMime());
        $response->headers->set('Content-Length', $file->getSize());

        // Dispatch request
        return $response;
    }
}
