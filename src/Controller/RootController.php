<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class RootController extends AbstractController
{
    /**
     * @return Response
     */
    #[Route('/', name: 'public_root')]
    public function index(): Response
    {
        $user = $this->getUser();

        if ($user instanceof User) {
            $accesses = $user->getAccessUsers();
            if ($accesses->count() === 1 && $accesses->first()->getTenant()) {
                $tenant = $accesses->first()->getTenant()->getBaseurl();
            }
        }
        if (isset($tenant)) {
            return $this->redirectToRoute('app_home', ['tenant_url' => $tenant]);
        } else {
            return $this->redirectToRoute('public_home');
        }
    }

    #[Route('/{tenant_url}', name: 'app_root', priority: -1)]
    public function tenant(string $tenant_url): Response
    {
        $user = $this->getUser();
        if ($user instanceof User) {
            return $this->redirectToRoute('app_home', ['tenant_url' => $tenant_url]);
        }
        return $this->redirectToRoute('app_tenant_login', ['tenant_url' => $tenant_url]);
    }
}
