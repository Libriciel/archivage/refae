<?php

namespace App\Controller\Api;

use App\Repository\ApiTokenRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Attribute\Route;

#[AsController]
class ApiTokenController extends AbstractController
{
    #[Route(
        path: '/api/{tenant_url}/api-token',
        name: 'api_token_create',
        methods: ['GET', 'POST'],
    )]
    public function create(): Response
    {
        // @see ApiTenantAuthenticator
        return new Response('Unexpected outcome', Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    #[Route(
        path: '/api/{tenant_url}/api-token',
        name: 'api_token_delete',
        methods: ['DELETE'],
    )]
    public function delete(
        Request $request,
        ApiTokenRepository $apiTokenRepository
    ): Response {
        [$type, $token] = explode(' ', $request->headers->get('Authorization') . ' ');
        if (strtolower($type) === 'bearer' && $token) {
            $apiTokenRepository->deleteByToken($token);
            return $this->json('done');
        }
        return new Response('Unexpected outcome', Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
