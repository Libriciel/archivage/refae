<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Repository\LabelRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Attribute\Route;

#[AsController]
class LabelController extends AbstractController
{
    #[Route(
        path: '/api/{tenant_url}/label-list',
        name: 'api_label_list',
        methods: ['GET'],
    )]
    public function list(
        LabelRepository $labelRepository,
    ): Response {
        $user = $this->getUser();
        if ($user instanceof User) {
            $list = $labelRepository->findList($user->getActiveTenant());
            return $this->json($list);
        }

        throw $this->createAccessDeniedException();
    }
}
