<?php

namespace App\Controller\Api;

use App\Entity\Agreement;
use App\Repository\AgreementRepository;
use App\Security\Voter\BelongsToTenantVoter;
use App\Service\ApiPaginator;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[AsController]
class AgreementController extends AbstractController
{
    #[IsGranted('acl/Agreement/view')]
    #[Route(
        path: '/api/{tenant_url}/agreement-published',
        name: 'api_agreement_published_list',
        methods: ['GET'],
        priority: 1
    )]
    public function publishedList(
        AgreementRepository $agreementRepository,
        ApiPaginator $paginator,
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
    ): Response {
        $user = $paginator->getUser();
        $queryBuilder = $agreementRepository->queryPublic($user->getActiveTenant());
        $responseString = $paginator->applyFiltersAndPaginate($queryBuilder, Agreement::class);

        return new Response($responseString, Response::HTTP_OK, [
            'Content-Type' => 'application/ld+json',
        ]);
    }

    #[IsGranted('acl/Agreement/view')]
    #[Route(
        path: '/api/{tenant_url}/agreement-published/{id}',
        name: 'api_agreement_published_id',
        methods: ['GET'],
    )]
    public function publishedListId(
        AgreementRepository $agreementRepository,
        ApiPaginator $paginator,
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        string $id,
    ): Response {
        $paginator->checkPermissions();
        $user = $paginator->getUser();
        $queryBuilder = $agreementRepository->queryPublic($user->getActiveTenant());
        $queryBuilder
            ->andWhere($queryBuilder->getAllAliases()[0] . '.id = :id')
            ->setParameter('id', $id)
        ;
        $entity = $queryBuilder->getQuery()->getOneOrNullResult();
        if (!$entity) {
            throw $this->createNotFoundException();
        }
        $responseString = $paginator->getResponseStringFromEntity($entity, Agreement::class);

        return new Response($responseString, Response::HTTP_OK, [
            'Content-Type' => 'application/ld+json',
        ]);
    }

    #[IsGranted('acl/Agreement/view')]
    #[IsGranted(BelongsToTenantVoter::class, 'agreement')]
    #[Route(
        path: '/api/{tenant_url}/agreement-by-identifier/{identifier}',
        name: 'api_agreement_published_identifier',
        methods: ['GET'],
    )]
    public function publishedListIdentifier(
        #[MapEntity(expr: 'repository.findByIdentifierWithPreviousVersions(identifier)')]
        Agreement $agreement,
        ApiPaginator $paginator,
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
    ): Response {
        $paginator->checkPermissions();

        $responseString = $paginator->getResponseStringFromEntity($agreement, Agreement::class);

        return new Response($responseString, Response::HTTP_OK, [
            'Content-Type' => 'application/ld+json',
        ]);
    }
}
