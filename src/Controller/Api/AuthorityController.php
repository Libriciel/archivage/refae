<?php

namespace App\Controller\Api;

use App\Entity\Authority;
use App\Repository\AuthorityRepository;
use App\Security\Voter\BelongsToTenantVoter;
use App\Service\ApiPaginator;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[AsController]
class AuthorityController extends AbstractController
{
    #[IsGranted('acl/Authority/view')]
    #[Route(
        path: '/api/{tenant_url}/authority-published',
        name: 'api_authority_published_list',
        methods: ['GET'],
        priority: 1
    )]
    public function publishedList(
        AuthorityRepository $authorityRepository,
        ApiPaginator $paginator,
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
    ): Response {
        $user = $paginator->getUser();
        $queryBuilder = $authorityRepository->queryPublic($user->getActiveTenant());
        $responseString = $paginator->applyFiltersAndPaginate($queryBuilder, Authority::class);

        return new Response($responseString, Response::HTTP_OK, [
            'Content-Type' => 'application/ld+json',
        ]);
    }

    #[IsGranted('acl/Authority/view')]
    #[Route(
        path: '/api/{tenant_url}/authority-published/{id}',
        name: 'api_authority_published_id',
        methods: ['GET'],
    )]
    public function publishedListId(
        AuthorityRepository $authorityRepository,
        ApiPaginator $paginator,
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        string $id,
    ): Response {
        $paginator->checkPermissions();
        $user = $paginator->getUser();
        $queryBuilder = $authorityRepository->queryPublic($user->getActiveTenant());
        $queryBuilder
            ->andWhere($queryBuilder->getAllAliases()[0] . '.id = :id')
            ->setParameter('id', $id)
        ;
        $entity = $queryBuilder->getQuery()->getOneOrNullResult();
        if (!$entity) {
            throw $this->createNotFoundException();
        }
        $responseString = $paginator->getResponseStringFromEntity($entity, Authority::class);

        return new Response($responseString, Response::HTTP_OK, [
            'Content-Type' => 'application/ld+json',
        ]);
    }

    #[IsGranted('acl/Authority/view')]
    #[IsGranted(BelongsToTenantVoter::class, 'authority')]
    #[Route(
        path: '/api/{tenant_url}/authority-by-identifier/{identifier}',
        name: 'api_authority_published_identifier',
        methods: ['GET'],
    )]
    public function publishedListIdentifier(
        #[MapEntity(expr: 'repository.findByIdentifierWithPreviousVersions(identifier)')]
        Authority $authority,
        ApiPaginator $paginator,
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
    ): Response {
        $paginator->checkPermissions();

        $responseString = $paginator->getResponseStringFromEntity($authority, Authority::class);

        return new Response($responseString, Response::HTTP_OK, [
            'Content-Type' => 'application/ld+json',
        ]);
    }
}
