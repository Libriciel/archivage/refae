<?php

namespace App\Controller\Api;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Attribute\Route;

#[AsController]
class WhoamiController extends AbstractController
{
    #[Route(
        path: '/api/{tenant_url}/whoami',
        name: 'api_whoami',
        defaults: [
            '_api_resource_class' => User::class,
        ],
        methods: ['GET'],
        priority: 1
    )]
    public function whoami(string $tenant_url): Response
    {
        $versionFile = file(dirname(__DIR__, 3) . '/VERSION.txt');
        $version = trim((string) array_pop($versionFile));
        /** @var User $user */
        $user = $this->getUser();

        return new JsonResponse(
            [
                'username' => $user?->getUsername(),
                'name' => $user?->getName(),
                'email' => $user?->getEmail(),
                'tenant' => $tenant_url,
                'app.version' => $version,
            ]
        );
    }
}
