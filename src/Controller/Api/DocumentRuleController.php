<?php

namespace App\Controller\Api;

use App\Entity\DocumentRule;
use App\Repository\DocumentRuleRepository;
use App\Security\Voter\BelongsToTenantVoter;
use App\Service\ApiPaginator;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[AsController]
class DocumentRuleController extends AbstractController
{
    #[IsGranted('acl/DocumentRule/view')]
    #[Route(
        path: '/api/{tenant_url}/document-rule-published',
        name: 'api_document_rule_published_list',
        methods: ['GET'],
        priority: 1
    )]
    public function publishedList(
        DocumentRuleRepository $documentRuleRepository,
        ApiPaginator $paginator,
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
    ): Response {
        $user = $paginator->getUser();
        $queryBuilder = $documentRuleRepository->queryPublic($user->getActiveTenant());
        $responseString = $paginator->applyFiltersAndPaginate($queryBuilder, DocumentRule::class);

        return new Response($responseString, Response::HTTP_OK, [
            'Content-Type' => 'application/ld+json',
        ]);
    }

    #[IsGranted('acl/DocumentRule/view')]
    #[Route(
        path: '/api/{tenant_url}/document-rule-published/{id}',
        name: 'api_document_rule_published_id',
        methods: ['GET'],
    )]
    public function publishedListId(
        DocumentRuleRepository $documentRuleRepository,
        ApiPaginator $paginator,
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        string $id,
    ): Response {
        $paginator->checkPermissions();
        $user = $paginator->getUser();
        $queryBuilder = $documentRuleRepository->queryPublic($user->getActiveTenant());
        $queryBuilder
            ->andWhere($queryBuilder->getAllAliases()[0] . '.id = :id')
            ->setParameter('id', $id)
        ;
        $entity = $queryBuilder->getQuery()->getOneOrNullResult();
        if (!$entity) {
            throw $this->createNotFoundException();
        }
        $responseString = $paginator->getResponseStringFromEntity($entity, DocumentRule::class);

        return new Response($responseString, Response::HTTP_OK, [
            'Content-Type' => 'application/ld+json',
        ]);
    }

    #[IsGranted('acl/DocumentRule/view')]
    #[IsGranted(BelongsToTenantVoter::class, 'documentRule')]
    #[Route(
        path: '/api/{tenant_url}/document-rule-by-identifier/{identifier}',
        name: 'api_document_rule_published_identifier',
        methods: ['GET'],
    )]
    public function publishedListIdentifier(
        #[MapEntity(expr: 'repository.findByIdentifierWithPreviousVersions(identifier)')]
        DocumentRule $documentRule,
        ApiPaginator $paginator,
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
    ): Response {
        $paginator->checkPermissions();

        $responseString = $paginator->getResponseStringFromEntity($documentRule, DocumentRule::class);

        return new Response($responseString, Response::HTTP_OK, [
            'Content-Type' => 'application/ld+json',
        ]);
    }
}
