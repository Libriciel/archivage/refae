<?php

namespace App\Controller\Api;

use App\Entity\ServiceLevel;
use App\Entity\Vocabulary;
use App\Repository\VocabularyRepository;
use App\Security\Voter\BelongsToTenantVoter;
use App\Service\ApiPaginator;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[AsController]
class VocabularyController extends AbstractController
{
    #[IsGranted('acl/Vocabulary/view')]
    #[Route(
        path: '/api/{tenant_url}/vocabulary-published',
        name: 'api_vocabulary_published_list',
        methods: ['GET'],
        priority: 1
    )]
    public function publishedList(
        VocabularyRepository $vocabularyRepository,
        ApiPaginator $paginator,
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
    ): Response {
        $user = $paginator->getUser();
        $queryBuilder = $vocabularyRepository->queryPublic($user->getActiveTenant());
        $responseString = $paginator->applyFiltersAndPaginate($queryBuilder, Vocabulary::class);

        return new Response($responseString, Response::HTTP_OK, [
            'Content-Type' => 'application/ld+json',
        ]);
    }

    #[IsGranted('acl/Vocabulary/view')]
    #[Route(
        path: '/api/{tenant_url}/vocabulary-published/{id}',
        name: 'api_vocabulary_published_id',
        methods: ['GET'],
    )]
    public function publishedListId(
        VocabularyRepository $vocabularyRepository,
        ApiPaginator $paginator,
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        string $id,
    ): Response {
        $paginator->checkPermissions();
        $user = $paginator->getUser();
        $queryBuilder = $vocabularyRepository->queryPublic($user->getActiveTenant());
        $queryBuilder
            ->andWhere($queryBuilder->getAllAliases()[0] . '.id = :id')
            ->setParameter('id', $id)
        ;
        $entity = $queryBuilder->getQuery()->getOneOrNullResult();
        if (!$entity) {
            throw $this->createNotFoundException();
        }
        $responseString = $paginator->getResponseStringFromEntity($entity, Vocabulary::class);

        return new Response($responseString, Response::HTTP_OK, [
            'Content-Type' => 'application/ld+json',
        ]);
    }

    #[IsGranted('acl/Vocabulary/view')]
    #[IsGranted(BelongsToTenantVoter::class, 'vocabulary')]
    #[Route(
        path: '/api/{tenant_url}/vocabulary-by-identifier/{identifier}',
        name: 'api_vocabulary_published_identifier',
        methods: ['GET'],
    )]
    public function publishedListIdentifier(
        #[MapEntity(expr: 'repository.findByIdentifierWithPreviousVersions(identifier)')]
        Vocabulary $vocabulary,
        ApiPaginator $paginator,
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
    ): Response {
        $paginator->checkPermissions();

        $responseString = $paginator->getResponseStringFromEntity($vocabulary, Vocabulary::class);

        return new Response($responseString, Response::HTTP_OK, [
            'Content-Type' => 'application/ld+json',
        ]);
    }
}
