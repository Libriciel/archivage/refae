<?php

namespace App\Controller\Api;

use App\Entity\ManagementRule;
use App\Repository\ManagementRuleRepository;
use App\Security\Voter\BelongsToTenantVoter;
use App\Service\ApiPaginator;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[AsController]
class ManagementRuleController extends AbstractController
{
    #[IsGranted('acl/ManagementRule/view')]
    #[Route(
        path: '/api/{tenant_url}/management-rule-published',
        name: 'api_management_rule_published_list',
        methods: ['GET'],
        priority: 1
    )]
    public function publishedList(
        ManagementRuleRepository $managementRuleRepository,
        ApiPaginator $paginator,
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
    ): Response {
        $user = $paginator->getUser();
        $queryBuilder = $managementRuleRepository->queryPublic($user->getActiveTenant());
        $responseString = $paginator->applyFiltersAndPaginate($queryBuilder, ManagementRule::class);

        return new Response($responseString, Response::HTTP_OK, [
            'Content-Type' => 'application/ld+json',
        ]);
    }

    #[IsGranted('acl/ManagementRule/view')]
    #[Route(
        path: '/api/{tenant_url}/management-rule-published/{id}',
        name: 'api_management_rule_published_id',
        methods: ['GET'],
    )]
    public function publishedListId(
        ManagementRuleRepository $managementRuleRepository,
        ApiPaginator $paginator,
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        string $id,
    ): Response {
        $paginator->checkPermissions();
        $user = $paginator->getUser();
        $queryBuilder = $managementRuleRepository->queryPublic($user->getActiveTenant());
        $queryBuilder
            ->andWhere($queryBuilder->getAllAliases()[0] . '.id = :id')
            ->setParameter('id', $id)
        ;
        $entity = $queryBuilder->getQuery()->getOneOrNullResult();
        if (!$entity) {
            throw $this->createNotFoundException();
        }
        $responseString = $paginator->getResponseStringFromEntity($entity, ManagementRule::class);

        return new Response($responseString, Response::HTTP_OK, [
            'Content-Type' => 'application/ld+json',
        ]);
    }

    #[IsGranted('acl/ManagementRule/view')]
    #[IsGranted(BelongsToTenantVoter::class, 'managementRule')]
    #[Route(
        path: '/api/{tenant_url}/management-rule-by-identifier/{identifier}',
        name: 'api_management_rule_published_identifier',
        methods: ['GET'],
    )]
    public function publishedListIdentifier(
        #[MapEntity(expr: 'repository.findByIdentifierWithPreviousVersions(identifier)')]
        ManagementRule $managementRule,
        ApiPaginator $paginator,
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
    ): Response {
        $paginator->checkPermissions();

        $responseString = $paginator->getResponseStringFromEntity($managementRule, ManagementRule::class);

        return new Response($responseString, Response::HTTP_OK, [
            'Content-Type' => 'application/ld+json',
        ]);
    }
}
