<?php

namespace App\Controller\Api;

use App\Entity\Profile;
use App\Entity\ServiceLevel;
use App\Repository\ServiceLevelRepository;
use App\Security\Voter\BelongsToTenantVoter;
use App\Service\ApiPaginator;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[AsController]
class ServiceLevelController extends AbstractController
{
    #[IsGranted('acl/ServiceLevel/view')]
    #[Route(
        path: '/api/{tenant_url}/service-level-published',
        name: 'api_service_level_published_list',
        methods: ['GET'],
        priority: 1
    )]
    public function publishedList(
        ServiceLevelRepository $serviceLevelRepository,
        ApiPaginator $paginator,
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
    ): Response {
        $user = $paginator->getUser();
        $queryBuilder = $serviceLevelRepository->queryPublic($user->getActiveTenant());
        $responseString = $paginator->applyFiltersAndPaginate($queryBuilder, ServiceLevel::class);

        return new Response($responseString, Response::HTTP_OK, [
            'Content-Type' => 'application/ld+json',
        ]);
    }

    #[IsGranted('acl/ServiceLevel/view')]
    #[Route(
        path: '/api/{tenant_url}/service-level-published/{id}',
        name: 'api_service_level_published_id',
        methods: ['GET'],
    )]
    public function publishedListId(
        ServiceLevelRepository $serviceLevelRepository,
        ApiPaginator $paginator,
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
        string $id,
    ): Response {
        $paginator->checkPermissions();
        $user = $paginator->getUser();
        $queryBuilder = $serviceLevelRepository->queryPublic($user->getActiveTenant());
        $queryBuilder
            ->andWhere($queryBuilder->getAllAliases()[0] . '.id = :id')
            ->setParameter('id', $id)
        ;
        $entity = $queryBuilder->getQuery()->getOneOrNullResult();
        if (!$entity) {
            throw $this->createNotFoundException();
        }
        $responseString = $paginator->getResponseStringFromEntity($entity, ServiceLevel::class);

        return new Response($responseString, Response::HTTP_OK, [
            'Content-Type' => 'application/ld+json',
        ]);
    }

    #[IsGranted('acl/ServiceLevel/view')]
    #[IsGranted(BelongsToTenantVoter::class, 'serviceLevel')]
    #[Route(
        path: '/api/{tenant_url}/service-level-by-identifier/{identifier}',
        name: 'api_service_level_published_identifier',
        methods: ['GET'],
    )]
    public function publishedListIdentifier(
        #[MapEntity(expr: 'repository.findByIdentifierWithPreviousVersions(identifier)')]
        ServiceLevel $serviceLevel,
        ApiPaginator $paginator,
        /** @noinspection PhpUnusedParameterInspection */
        string $tenant_url,
    ): Response {
        $paginator->checkPermissions();

        $responseString = $paginator->getResponseStringFromEntity($serviceLevel, ServiceLevel::class);

        return new Response($responseString, Response::HTTP_OK, [
            'Content-Type' => 'application/ld+json',
        ]);
    }
}
