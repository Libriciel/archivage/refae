<?php

namespace App\Controller;

use App\Repository\OpenidRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class LogoutController extends AbstractController
{
    #[Route(path: '/logout', name: '_logout', methods: ['GET'])]
    public function logout(
        Request $request,
        Security $security,
        OpenidRepository $openidRepository,
    ) {
        // On redirige sur /tenant/login si on étais sur un tenant sur la précédente page
        $lastConnexion = $request->getSession()->get('last_connection');
        if ($lastConnexion && str_starts_with((string) $lastConnexion, 'tenant.')) {
            $tenantData = $request->getSession()->get($lastConnexion);
            $this->destroySession($request, $security);

            $afterRedirectUrl = $this->generateUrl(
                'app_tenant_login',
                ['tenant_url' => $tenantData['baseurl']],
                UrlGeneratorInterface::ABSOLUTE_URL
            );
            if ($tenantData['connection'] === 'openid') {
                $openid = $openidRepository->find($tenantData['openid']);
                $oidc = $openid->getOpenidClient();
                $oidc->signOut($tenantData['id_token'], $afterRedirectUrl);
                if ($oidc->redirectEndpoint) {
                    return $this->redirect($oidc->redirectEndpoint);
                }
            }

            /** @noinspection UseControllerShortcuts */
            return $this->redirect($afterRedirectUrl);
        }
        // On redirige sur la page logout si on est dans un openid
        foreach ($request->getSession()->all() as $key => $tenantData) {
            if (!str_starts_with($key, 'tenant.') && !str_starts_with($key, 'public.')) {
                continue;
            }
            if ($tenantData['connection'] === 'openid') {
                $afterRedirectUrl = $this->generateUrl(
                    'app_tenant_login',
                    ['tenant_url' => $tenantData['baseurl']],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );
                $openid = $openidRepository->find($tenantData['openid']);
                $oidc = $openid->getOpenidClient();
                $oidc->signOut($tenantData['id_token'], $afterRedirectUrl);
                if ($oidc->redirectEndpoint) {
                    $this->destroySession($request, $security);
                    return $this->redirect($oidc->redirectEndpoint);
                }
            }
        }

        // on redirige sur la page d'accueil public sinon
        $this->destroySession($request, $security);
        return $this->redirectToRoute('public_root');
    }

    private function destroySession(
        Request $request,
        Security $security,
    ) {
        if ($security->getUser()) {
            $security->logout(false);
        }
        $request->getSession()->clear();
    }
}
