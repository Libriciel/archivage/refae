<?php

namespace App\Controller\Admin;

use App\Entity\AccessUser;
use App\Entity\Ldap;
use App\Form\LdapType;
use App\Form\LoginPasswordType;
use App\Repository\AccessUserRepository;
use App\Repository\LdapRepository;
use App\Repository\TenantRepository;
use App\ResultSet\LdapListEntriesResultSet;
use App\ResultSet\LdapResultSet;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;

#[Route(priority: 1)]
class LdapController extends AbstractController
{
    #[Route('/admin/ldap/{page?1}', name: 'admin_ldap', requirements: ['page' => '\d+'], methods: ['GET'])]
    #[IsGranted('acl/Ldap/view')]
    public function index(LdapResultSet $resultSet): Response
    {
        return $this->render('admin/ldap/index.html.twig', [
            'table' => $resultSet,
        ]);
    }

    #[Route('/admin/ldap/add', name: 'admin_ldap_add', methods: ['GET', 'POST'])]
    #[IsGranted('acl/Ldap/add_edit')]
    public function add(
        Request $request,
        EntityManagerInterface $entityManager
    ): Response {
        $ldap = new Ldap();
        $ldap->setPort(389);
        $ldap->setUseProxy(true);
        $ldap->setTimeout(5);
        $ldap->setVersion(3);
        $form = $this->createForm(
            LdapType::class,
            $ldap,
            [
                'action' => $this->generateUrl('admin_ldap_add'),
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($ldap);
            $entityManager->flush();

            $response->setContent(
                json_encode(
                    LdapResultSet::normalize($ldap, ['index', 'action']),
                    JSON_THROW_ON_ERROR
                )
            );
            $response->headers->add(
                [
                    'Content-Type' => 'application/json',
                    'X-Asalae-Success' => 'true',
                ]
            );
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('admin/ldap/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/admin/ldap/edit/{ldap?}', name: 'admin_ldap_edit', methods: ['GET', 'POST'])]
    #[IsGranted('acl/Ldap/add_edit')]
    public function edit(
        Ldap $ldap,
        Request $request,
        EntityManagerInterface $entityManager,
    ): Response {
        $form = $this->createForm(
            LdapType::class,
            $ldap,
            [
                'action' => $this->generateUrl('admin_ldap_edit', ['ldap' => $ldap->getId()]),
            ]
        );
        $form->get('id')->setData($ldap->getId());
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($ldap);
            $entityManager->flush();

            $response->setContent(
                json_encode(
                    LdapResultSet::normalize($ldap, ['index', 'action']),
                    JSON_THROW_ON_ERROR
                )
            );
            $response->headers->add(
                [
                    'Content-Type' => 'application/json',
                    'X-Asalae-Success' => 'true',
                ]
            );
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('admin/ldap/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/admin/ldap/view/{id?}', name: 'admin_ldap_view', methods: ['GET'])]
    #[IsGranted('acl/Ldap/view')]
    public function view(
        Ldap $ldap
    ): Response {
        return $this->render('admin/ldap/view.html.twig', [
            'ldap' => $ldap,
        ]);
    }

    #[Route('/admin/ldap/delete/{id?}', name: 'admin_ldap_delete', methods: ['DELETE'])]
    #[IsGranted('acl/Ldap/delete')]
    public function delete(
        Ldap $ldap,
        EntityManagerInterface $entityManager
    ): Response {
        $entityManager->remove($ldap);
        $entityManager->flush();

        $response = new Response();
        $response->setContent('done');
        return $response;
    }

    #[Route('/admin/ldap/ping', name: 'admin_ping_ldap', methods: ['POST'])]
    #[IsGranted('acl/Ldap/add_edit')]
    public function ping(
        Request $request,
        LdapRepository $ldapRepository,
    ): Response {
        try {
            $ldap = $this->getLdapEntity($request, $ldapRepository);
        } catch (UnexpectedValueException $e) {
            $response = new JsonResponse($e->getMessage());
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            return $response;
        }

        if ($ldap->ping()) {
            return new JsonResponse('ping');
        }

        $response = new JsonResponse('error');
        $response->setStatusCode(Response::HTTP_NOT_FOUND);
        return $response;
    }

    #[Route('/admin/ldap/random-entry', name: 'admin_random_entry_ldap', methods: ['POST'])]
    #[IsGranted('acl/Ldap/add_edit')]
    public function randomEntry(
        Request $request,
        LdapRepository $ldapRepository,
    ): JsonResponse {
        try {
            $ldap = $this->getLdapEntity($request, $ldapRepository);
        } catch (UnexpectedValueException $e) {
            $response = new JsonResponse($e->getMessage());
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            return $response;
        }

        return new JsonResponse($ldap->getRandomEntry());
    }

    #[Route('/admin/ldap/count', name: 'admin_ldap_count', methods: ['POST'])]
    #[IsGranted('acl/Ldap/add_edit')]
    public function count(
        Request $request,
        LdapRepository $ldapRepository,
    ): JsonResponse {
        try {
            $ldap = $this->getLdapEntity($request, $ldapRepository);
        } catch (UnexpectedValueException $e) {
            $response = new JsonResponse($e->getMessage());
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            return $response;
        }

        return new JsonResponse($ldap->getCount());
    }

    #[Route('/admin/ldap/filtered', name: 'admin_ldap_filtered', methods: ['POST'])]
    #[IsGranted('acl/Ldap/add_edit')]
    public function filtered(
        Request $request,
        LdapRepository $ldapRepository,
    ): JsonResponse {
        try {
            $ldap = $this->getLdapEntity($request, $ldapRepository);
        } catch (UnexpectedValueException $e) {
            $response = new JsonResponse($e->getMessage());
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            return $response;
        }

        return new JsonResponse($ldap->getFiltered());
    }

    #[Route('/admin/ldap/test', name: 'admin_ldap_test', methods: ['POST'])]
    #[IsGranted('acl/Ldap/add_edit')]
    public function test(
        Request $request,
        LdapRepository $ldapRepository,
    ): JsonResponse {
        try {
            $ldap = $this->getLdapEntity($request, $ldapRepository);
        } catch (UnexpectedValueException $e) {
            $response = new JsonResponse($e->getMessage());
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            return $response;
        }
        $requestLdap = $request->get('ldap');
        $passwordOk = $ldap->auth()->attempt(
            $ldap->getAffixedLogin($requestLdap['test_username']),
            $requestLdap['test_password'],
            true
        );
        if ($passwordOk) {
            $message = "Connexion au LDAP avec l'utilisateur '{$requestLdap['test_username']}' réussie";
        } else {
            $message = "Connexion refusée";
        }

        return new JsonResponse($message);
    }

    #[Route('/admin/ldap/{id}/list/{page?1}', name: 'admin_ldap_list_entries', methods: ['GET'])]
    #[IsGranted('acl/Ldap/add_edit')]
    public function listLdapEntries(
        LdapListEntriesResultSet $resultSet,
        Ldap $ldap,
    ) {
        $resultSet->initialize($ldap);
        return $this->render('admin/ldap/list_entries.html.twig', [
            'table' => $resultSet,
            'ldap' => $ldap,
            'ldap_params' => LdapResultSet::normalize($ldap, ['attribute'])
        ]);
    }

    private function getLdapEntity(
        Request $request,
        LdapRepository $ldapRepository,
    ): Ldap {
        $ldap = new Ldap();
        $form = $this->createForm(
            LdapType::class,
            $ldap,
        );
        $form->handleRequest($request);
        $id = $request->get('ldap')['id'] ?? null;
        $newPassword = $request->get('ldap')['user_query_password'] ?? null;
        if (empty($newPassword) && $id) {
            $ldap->setUserQueryPassword($ldapRepository->find($id)->getUserQueryPassword());
        }
        return $ldap;
    }

    #[Route('/admin/ldap/{id}/view/{login?}', name: 'admin_ldap_view_entry', methods: ['GET'])]
    #[IsGranted('acl/Ldap/add_edit')]
    public function viewEntry(
        Ldap $ldap,
        string $login,
    ) {
        return $this->render('admin/ldap/entry.html.twig', [
            'ldap' => $ldap,
            'entry' => $ldap->findEntry($login),
        ]);
    }

    #[Route('/admin/ldap/{id}/try-entry-login/{login?}', name: 'admin_ldap_try_entry_login', methods: ['GET', 'POST'])]
    #[IsGranted('acl/Ldap/add_edit')]
    public function tryEntryLogin(
        Ldap $ldap,
        string $login,
        Request $request,
    ) {
        $form = $this->createForm(
            LoginPasswordType::class,
            null,
            [
                'action' => $this->generateUrl(
                    'admin_ldap_try_entry_login',
                    ['id' => $ldap->getId(), 'login' => $login]
                ),
            ]
        );

        $form->handleRequest($request);
        $entry = $ldap->findEntry($login);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $ldapLogin = $ldap->getAffixedLogin(
                html_entity_decode((string) $entry[$ldap->getUserLoginAttribute()])
            );
            $password = $form->get('password')->getData();

            $message = "Mot de passe incorrect";
            if (!$ldap->auth()) {
                $success = false;
                $message = "Impossible de contacter le LDAP";
            } else {
                try {
                    $success = $ldap->auth()->attempt($ldapLogin, $password, true);
                } catch (Exception $e) {
                    $success = false;
                    $message = $e->getMessage();
                }
            }
            if (!$success) {
                $form->get('password')
                    ->addError(new FormError($message));
            } else {
                return $this->render('admin/ldap/try-entry-login-success.html.twig', [], $response);
            }
        }
        return $this->render('admin/ldap/try-entry-login.html.twig', [
            'form' => $form->createView(),
            'entry' => $entry,
            'login' => $login,
            'loginQuery' => $ldap->getAccountPrefix()
                . html_entity_decode((string) $entry[$ldap->getUserLoginAttribute()], ENT_NOQUOTES)
                . $ldap->getAccountSuffix(),
        ], $response);
    }

    #[Route('/admin/ldap/list-available-attributes', name: 'admin_ldap_list_available_attributes', methods: ['POST'])]
    #[IsGranted('acl/Ldap/add_edit')]
    public function listAvailableAttributes(
        Request $request,
        LdapRepository $ldapRepository,
    ) {
        try {
            $ldap = $this->getLdapEntity($request, $ldapRepository);
        } catch (UnexpectedValueException $e) {
            $response = new JsonResponse($e->getMessage());
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            return $response;
        }

        return new JsonResponse($ldap->getlistAvailableAttributes());
    }

    #[Route('/admin/ldap/search/{id}', name: 'admin_ldap_search', methods: ['POST'])]
    #[IsGranted('acl/Ldap/view')]
    public function search(
        Ldap $ldap,
        Request $request,
        AccessUserRepository $accessUserRepository,
        TenantRepository $tenantRepository,
    ): Response {
        $tenantId = $request->get('tenant');
        $tenant = $tenantId ? $tenantRepository->find($tenantId) : null;

        $usedAccesses = array_map(
            fn(AccessUser $au) => $au->getLdapUsername(),
            $accessUserRepository->getAccessesForLdap($ldap, $tenant)
        );
        $usedAccesses = array_diff($usedAccesses, [$request->get('currentLdapUsername')]);

        $results = [];
        $entries = $ldap->connect()
            ?->query()
            ->whereContains($request->get('field'), $request->get('value'))
            ->paginate(10);
        unset($entries['count']);
        foreach ($entries ?? [] as $entry) {
            if (
                !is_array($entry)
                || empty($entry[$ldap->getUserLoginAttribute()][0])
                || in_array($entry[$ldap->getUserLoginAttribute()][0], $usedAccesses)
            ) {
                continue;
            }
            $results[] = $entry[$ldap->getUserLoginAttribute()][0];
        }
        sort($results);
        return new JsonResponse($results);
    }
}
