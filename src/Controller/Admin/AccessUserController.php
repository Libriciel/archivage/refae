<?php

namespace App\Controller\Admin;

use App\Entity\AccessUser;
use App\Entity\User;
use App\Entity\UserPasswordRenewToken;
use App\Form\AdminAccessUserType;
use App\ResultSet\AccessUserResultSet;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(priority: 1)]
class AccessUserController extends AbstractController
{
    #[Route(
        '/admin/access-user/{username}',
        name: 'admin_access_user',
        methods: ['GET'],
    )]
    #[IsGranted('acl/User/view')]
    public function index(
        AccessUserResultSet $accessUserResultSet,
        User $user,
    ): Response {
        $accessUserResultSet->initialize($user);
        return $this->render('admin/access_user/access-user.html.twig', [
            'table' => $accessUserResultSet,
            'user' => $user,
        ]);
    }
    #[Route(
        '/admin/access-user/{username}/add',
        name: 'admin_user_access_add',
        methods: ['GET', 'POST'],
    )]
    #[IsGranted('acl/User/view')]
    public function add(
        User $user,
        Request $request,
        EntityManagerInterface $entityManager,
        MailerInterface $mailer,
    ): Response {
        $accessUser = new AccessUser();
        $user->addAccessUser($accessUser);

        $form = $this->createForm(
            AdminAccessUserType::class,
            $accessUser,
            [
                'action' => $this->generateUrl(
                    'admin_user_access_add',
                    ['username' => $user->getUsername()]
                ),
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($accessUser);
            $entityManager->flush();
            $user->setActiveTenant($accessUser->getTenant());
            if ($user->getLoginType() === AccessUser::LOGIN_TYPE_REFAE) {
                $userPasswordRenewToken = new UserPasswordRenewToken();
                $userPasswordRenewToken->setUser($user);
                $entityManager->persist($userPasswordRenewToken);
                $entityManager->flush();
                $this->sendPasswordInitMail($mailer, $userPasswordRenewToken);
            } else {
                $this->sendNewAccessMail($mailer, $user);
            }
            $openid = $accessUser->getOpenid();
            if ($openid) {
                $loginUrl = $this->generateUrl(
                    'app_tenant_openid',
                    ['tenant_url' => $accessUser->getTenant()->getBaseurl(), 'identifier' => $openid->getIdentifier()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );
                $openid->setLoginUrl($loginUrl);
            }

            return new JsonResponse(
                AccessUserResultSet::normalize($accessUser, ['index', 'action']),
                headers: [
                    'X-Asalae-Success' => 'true',
                    'X-Asalae-Available-Tenants' => $user->hasAvailableTenants() ? 'true' : 'false',
                ]
            );
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('admin/access_user/add-access-user.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/admin/access-user/{username}/delete/{accessUser}', name: 'admin_user_access_delete', methods: ['DELETE'])]
    #[IsGranted('acl/User/delete')]
    public function delete(
        User $user,
        EntityManagerInterface $entityManager,
        AccessUser $accessUser,
    ): Response {
        $entityManager->remove($accessUser);
        $entityManager->flush();

        return new JsonResponse(
            'done',
            headers: [
                'X-Asalae-Success' => 'true',
                'X-Asalae-Available-Tenants' => $user->hasAvailableTenants() ? 'true' : 'false',
            ]
        );
    }

    private function sendPasswordInitMail(
        MailerInterface $mailer,
        UserPasswordRenewToken $userPasswordRenewToken,
    ): void {
        $user = $userPasswordRenewToken->getUser();
        $tenant = $user->getActiveTenant();
        $url = $this->generateUrl(
            'public_user_select_password',
            ['token' => $userPasswordRenewToken->getToken()],
            UrlGeneratorInterface::ABSOLUTE_URL,
        );
        $urlAlt = $this->generateUrl(
            'app_tenant_login_username',
            ['tenant_url' => $tenant->getBaseurl(), 'username' => $user->getUsername()],
            UrlGeneratorInterface::ABSOLUTE_URL,
        );
        $configuration = $tenant->getConfiguration();
        $mailPrefix = $configuration?->getMailPrefix() ?: '[refae]';
        $email = (new TemplatedEmail())
            ->from($_ENV['MAILER_FROM'] ?? 'refae@test.fr')
            ->to($user->getEmail())
            ->subject($mailPrefix . 'Création de votre compte')
            ->htmlTemplate('email/user-created-ask-password.html.twig')
            ->context([
                'user' => $user,
                'url' => $url,
                'url_alt' => $urlAlt,
                'tenant' => $tenant,
                'signature' => $configuration?->getMailSignature(),
                'choosePassword' => $user->isUserDefinedPassword() === false,
            ])
        ;
        $mailer->send($email);
    }

    private function sendNewAccessMail(
        MailerInterface $mailer,
        User $user,
    ): void {
        $tenant = $user->getActiveTenant();
        $urlAlt = $this->generateUrl(
            'app_tenant_login_username',
            ['tenant_url' => $tenant->getBaseurl(), 'username' => $user->getUsername()],
            UrlGeneratorInterface::ABSOLUTE_URL,
        );
        $configuration = $tenant->getConfiguration();
        $mailPrefix = $configuration?->getMailPrefix() ?: '[refae]';
        $email = (new TemplatedEmail())
            ->from($_ENV['MAILER_FROM'] ?? 'refae@test.fr')
            ->to($user->getEmail())
            ->subject($mailPrefix . 'Création de votre compte')
            ->htmlTemplate('email/user-created-ask-password.html.twig')
            ->context([
                'user' => $user,
                'url' => false,
                'url_alt' => $urlAlt,
                'tenant' => $tenant,
                'signature' => $configuration?->getMailSignature(),
                'choosePassword' => false,
            ])
        ;
        $mailer->send($email);
    }
}
