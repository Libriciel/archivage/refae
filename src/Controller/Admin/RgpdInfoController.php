<?php

namespace App\Controller\Admin;

use App\Entity\RgpdInfo;
use App\Form\AdminRgpdInfoType;
use App\Repository\RgpdInfoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(priority: 1)]
class RgpdInfoController extends AbstractController
{
    #[Route('/admin/rgpd-info', name: 'admin_rgpd_info')]
    #[IsGranted('acl/RgpdInfo/add_edit')]
    public function addEdit(
        Request $request,
        EntityManagerInterface $entityManager,
        RgpdInfoRepository $rgpdInfoRepository,
    ): Response {
        $rgpdInfo = $rgpdInfoRepository->findOneForTenant(null);
        if (!$rgpdInfo) {
            $rgpdInfo = new RgpdInfo();
        }
        $form = $this->createForm(
            AdminRgpdInfoType::class,
            $rgpdInfo,
            [
                'action' => $this->generateUrl('admin_rgpd_info'),
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($rgpdInfo);
            $entityManager->flush();

            $response->headers->add(
                [
                    'X-Asalae-Success' => 'true',
                ]
            );
            $response->setContent('OK');
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('admin/rgpd_info/add-edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }
}
