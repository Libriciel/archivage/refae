<?php

namespace App\Controller\Admin;

use App\Entity\UserCreationRequest;
use App\Form\UserCreationRequestRefusalType;
use App\ResultSet\AdminUserCreationRequestResultSet;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(priority: 1)]
class UserCreationRequestController extends AbstractController
{
    #[IsGranted('acl/User/add_edit')]
    #[Route(
        '/admin/user-creation-request/{page?1}',
        name: 'admin_user_creation_request',
        requirements: ['page' => '\d+'],
        methods: ['GET']
    )]
    public function indexUserCreationRequest(
        AdminUserCreationRequestResultSet $resultSet,
    ): Response {
        return $this->render('admin/user_creation_request/index.html.twig', [
            'table' => $resultSet,
        ]);
    }

    #[IsGranted('acl/User/add_edit')]
    #[Route(
        '/admin/user-creation-request-deny/{userCreationRequest}',
        name: 'admin_user_creation_request_deny',
        methods: ['GET', 'POST']
    )]
    public function indexUserCreationRequestDeny(
        UserCreationRequest $userCreationRequest,
        Request $request,
        EntityManagerInterface $entityManager,
        MailerInterface $mailer,
    ): Response {
        $form = $this->createForm(
            UserCreationRequestRefusalType::class,
            options: [
                'action' => $this->generateUrl(
                    'admin_user_creation_request_deny',
                    ['userCreationRequest' => $userCreationRequest->getId()]
                ),
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $reason = $form->get('reason')->getData();

            $entityManager->remove($userCreationRequest);
            $entityManager->flush();

            $this->sendRefusalEmail($mailer, $userCreationRequest, $reason);
            $this->addFlash(
                'success',
                "E-mail envoyé avec succès"
            );
            $response->headers->add(['X-Asalae-Redirect' => $this->generateUrl('admin_user_creation_request')]);
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('admin/user_creation_request/deny.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    private function sendRefusalEmail(
        MailerInterface $mailer,
        UserCreationRequest $userCreationRequest,
        string $reason,
    ): void {
        $tenant = $userCreationRequest->getTenant();
        $configuration = $tenant?->getConfiguration();
        $mailPrefix = $configuration?->getMailPrefix() ?: '[refae]';
        $email = (new TemplatedEmail())
            ->from($_ENV['MAILER_FROM'] ?? 'refae@test.fr')
            ->to($userCreationRequest->getEmail())
            ->subject($mailPrefix . 'Refus de création de votre utilisateur')
            ->htmlTemplate('email/user-creation-refusal.html.twig')
            ->context([
                'name' => $userCreationRequest->getName(),
                'reason' => $reason,
                'signature' => $configuration?->getMailSignature(),
            ])
        ;
        $mailer->send($email);
    }

    #[IsGranted('acl/User/delete')]
    #[Route('/admin/user-creation-request/delete/{username?}', name: 'admin_user_delete_creation_request')]
    public function delete(
        UserCreationRequest $userCreationRequest,
        EntityManagerInterface $entityManager
    ): Response {
        $entityManager->remove($userCreationRequest);
        $entityManager->flush();

        $response = new Response();
        $response->setContent('done');
        return $response;
    }
}
