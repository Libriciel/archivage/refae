<?php

namespace App\Controller\Admin;

use App\Entity\Openid;
use App\Form\OpenidType;
use App\ResultSet\OpenidResultSet;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Component\HttpClient\TraceableHttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[Route(priority: 1)]
class OpenidController extends AbstractController
{
    #[Route('/admin/openid/{page?1}', name: 'admin_openid', requirements: ['page' => '\d+'], methods: ['GET'])]
    #[IsGranted('acl/Openid/view')]
    public function index(OpenidResultSet $resultSet): Response
    {
        return $this->render('admin/openid/index.html.twig', [
            'table' => $resultSet,
        ]);
    }

    #[Route('/admin/openid/add', name: 'admin_openid_add', methods: ['GET', 'POST'])]
    #[IsGranted('acl/Openid/add_edit')]
    public function add(
        Request $request,
        EntityManagerInterface $entityManager
    ): Response {
        $openid = new Openid();
        $form = $this->createForm(
            OpenidType::class,
            $openid,
            [
                'action' => $this->generateUrl('admin_openid_add'),
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($openid);
            $entityManager->flush();

            $response->setContent(
                json_encode(
                    OpenidResultSet::normalize($openid, ['index', 'action']),
                    JSON_THROW_ON_ERROR
                )
            );
            $response->headers->add(
                [
                    'Content-Type' => 'application/json',
                    'X-Asalae-Success' => 'true',
                ]
            );
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('admin/openid/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/admin/openid/edit/{id?}', name: 'admin_openid_edit', methods: ['GET', 'POST'])]
    #[IsGranted('acl/Openid/add_edit')]
    public function edit(
        string $id,
        Request $request,
        EntityManagerInterface $entityManager,
        Openid $openid
    ): Response {
        $form = $this->createForm(
            OpenidType::class,
            $openid,
            [
                'action' => $this->generateUrl('admin_openid_edit', ['id' => $id]),
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($openid);
            $entityManager->flush();

            $response->setContent(
                json_encode(
                    OpenidResultSet::normalize($openid, ['index', 'action']),
                    JSON_THROW_ON_ERROR
                )
            );
            $response->headers->add(
                [
                    'Content-Type' => 'application/json',
                    'X-Asalae-Success' => 'true',
                ]
            );
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('admin/openid/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/admin/openid/view/{id?}', name: 'admin_openid_view', methods: ['GET'])]
    #[IsGranted('acl/Openid/view')]
    public function view(
        Openid $openid
    ): Response {
        return $this->render('admin/openid/view.html.twig', [
            'openid' => $openid,
        ]);
    }

    #[Route('/admin/openid/delete/{id?}', name: 'admin_openid_delete', methods: ['DELETE'])]
    #[IsGranted('acl/Openid/delete')]
    public function delete(
        Openid $openid,
        EntityManagerInterface $entityManager
    ): Response {
        $entityManager->remove($openid);
        $entityManager->flush();

        $response = new Response();
        $response->setContent('done');
        return $response;
    }

    #[Route('/admin/openid/query', name: 'admin_query_openid_configuration')]
    #[IsGranted('acl/Openid/add_edit')]
    public function query(
        Request $request,
        HttpClientInterface $client,
    ): Response {
        // TODO: vérifier si utile encore ou pas
        $httpResponseArray = null;
        try {
            /** @var TraceableHttpClient $client */
            $httpResponse = $client->request(
                'GET',
                $request->get('url')
            )->getContent();
            $httpResponseArray = $httpResponse
                ? json_decode($httpResponse, true)
                : null;
        } catch (TransportException) {
            $httpResponse = null;
        }
        $response = new Response();
        if (!empty($httpResponseArray['issuer'])) {
            $response->setContent($httpResponse);
            $response->headers->set('Content-Type', 'application/json');
        } else {
            $response->setContent("Failed");
        }
        return $response;
    }
}
