<?php

namespace App\Controller\Admin;

use App\Entity\AccessUser;
use App\Entity\User;
use App\Entity\UserPasswordRenewToken;
use App\Form\AdminTechnicalAdminType;
use App\Form\AdminTechPromoteAccessUserType;
use App\Form\SelectPasswordType;
use App\Repository\RoleRepository;
use App\Repository\UserPasswordRenewTokenRepository;
use App\ResultSet\AdminUserResultSet;
use App\ResultSet\TechnicalAdminResultSet;
use App\Service\ExportCsv;
use App\Service\Manager\UserManager;
use App\Service\Permission;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(priority: 1)]
class TechnicalAdminController extends AbstractController
{
    #[Route(
        '/admin/technical-admin/{page?1}',
        name: 'admin_technical_admin',
        requirements: ['page' => '\d+'],
        methods: ['GET']
    )]
    #[IsGranted('acl/User/view')]
    public function adminUser(TechnicalAdminResultSet $resultSet): Response
    {
        return $this->render('admin/user_technical_admin/index.html.twig', [
            'table' => $resultSet,
        ]);
    }

    #[Route('/admin/technical-admin/add', name: 'admin_technical_admin_add', methods: ['GET', 'POST'])]
    #[IsGranted('acl/User/add_edit')]
    public function add(
        Request $request,
        UserManager $userManager,
        EntityManagerInterface $entityManager,
        MailerInterface $mailer,
    ): Response {
        $user = new User();
        $user->setNotifyUserRegistration(true);
        $user->setUserDefinedPassword(false);

        $form = $this->createForm(
            AdminTechnicalAdminType::class,
            $user,
            ['action' => $this->generateUrl('admin_technical_admin_add')]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->add($user);

            if ($user->getLoginType() === AccessUser::LOGIN_TYPE_REFAE) {
                $userPasswordRenewToken = new UserPasswordRenewToken();
                $userPasswordRenewToken->setUser($user);
                $entityManager->persist($userPasswordRenewToken);
                $entityManager->flush();
                $this->sendPasswordInitMail($mailer, $userPasswordRenewToken);
            } else {
                $this->sendNewAccessMail($mailer, $user);
            }

            $response->setContent(
                json_encode(
                    AdminUserResultSet::normalize($user, ['index_admin', 'action']),
                    JSON_THROW_ON_ERROR
                )
            );
            $response->headers->add(
                [
                    'Content-Type' => 'application/json',
                    'X-Asalae-Success' => 'true',
                ]
            );

            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('admin/user_technical_admin/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/admin/technical-admin/edit/{username}', name: 'admin_technical_admin_edit', methods: ['GET', 'POST'])]
    #[IsGranted('acl/User/add_edit')]
    public function edit(
        string $username,
        Request $request,
        User $user,
        EntityManagerInterface $entityManager,
    ): Response {
        $form = $this->createForm(
            AdminTechnicalAdminType::class,
            $user,
            [
                'action' => $this->generateUrl('admin_technical_admin_edit', ['username' => $username]),
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($user);
            $entityManager->flush();

            $response->setContent(
                json_encode(
                    AdminUserResultSet::normalize($user, ['index_admin', 'action']),
                    JSON_THROW_ON_ERROR
                )
            );
            $response->headers->add(
                [
                    'Content-Type' => 'application/json',
                    'X-Asalae-Success' => 'true',
                ]
            );
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('admin/user_technical_admin/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/admin/technical-admin/promote', name: 'admin_technical_admin_promote', methods: ['GET', 'POST'])]
    #[IsGranted('acl/User/add_edit')]
    public function promote(
        Request $request,
        EntityManagerInterface $entityManager,
        RoleRepository $roleRepository,
    ): Response {
        $accessUser = new AccessUser();
        $role = $roleRepository->findOneDefault(Permission::ROLE_TECHNICAL_ADMIN);
        $accessUser->setRole($role);

        $form = $this->createForm(
            AdminTechPromoteAccessUserType::class,
            $accessUser,
            ['action' => $this->generateUrl('admin_technical_admin_promote')]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($accessUser);
            $entityManager->flush();
            $user = $accessUser->getUser();

            $response->setContent(
                json_encode(
                    AdminUserResultSet::normalize($user, ['index_admin', 'action']),
                    JSON_THROW_ON_ERROR
                )
            );
            $response->headers->add(
                [
                    'Content-Type' => 'application/json',
                    'X-Asalae-Success' => 'true',
                ]
            );

            return $response;
        }

        return $this->render('admin/user_technical_admin/promote.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    private function sendPasswordInitMail(
        MailerInterface $mailer,
        UserPasswordRenewToken $userPasswordRenewToken,
    ): void {
        $user = $userPasswordRenewToken->getUser();
        $url = $this->generateUrl(
            'admin_technical_admin_select_password',
            ['token' => $userPasswordRenewToken->getToken()],
            UrlGeneratorInterface::ABSOLUTE_URL,
        );
        $urlAlt = $this->generateUrl(
            'public_login_username',
            ['username' => $user->getUsername()],
            UrlGeneratorInterface::ABSOLUTE_URL,
        );
        $mailPrefix = '[refae]';
        $email = (new TemplatedEmail())
            ->from($_ENV['MAILER_FROM'] ?? 'refae@test.fr')
            ->to($user->getEmail())
            ->subject($mailPrefix . 'Création de votre compte')
            ->htmlTemplate('email/technical-admin-created-ask-password.html.twig')
            ->context([
                'user' => $user,
                'url' => $url,
                'url_alt' => $urlAlt,
                'choosePassword' => $user->isUserDefinedPassword() === false,
            ])
        ;
        $mailer->send($email);
    }

    private function sendNewAccessMail(
        MailerInterface $mailer,
        User $user,
    ): void {
        $urlAlt = $this->generateUrl(
            'public_login_username',
            ['username' => $user->getUsername()],
            UrlGeneratorInterface::ABSOLUTE_URL,
        );
        $mailPrefix = '[refae]';
        $email = (new TemplatedEmail())
            ->from($_ENV['MAILER_FROM'] ?? 'refae@test.fr')
            ->to($user->getEmail())
            ->subject($mailPrefix . 'Création de votre compte')
            ->htmlTemplate('email/technical-admin-created-ask-password.html.twig')
            ->context([
                'user' => $user,
                'url' => false,
                'url_alt' => $urlAlt,
                'choosePassword' => false,
            ])
        ;
        $mailer->send($email);
    }

    #[Route('/admin/technical-admin/select-password/{token}', name: 'admin_technical_admin_select_password')]
    public function selectPassword(
        string $token,
        UserPasswordRenewTokenRepository $passwordRenewTokenRepository,
        Request $request,
        UserManager $userManager,
        EntityManagerInterface $entityManager,
        Security $security,
    ) {
        $passwordRenewTokenRepository->deleteExpired();
        $passwordRenewToken = $passwordRenewTokenRepository->findOneBy(['token' => $token]);
        if (!$passwordRenewToken) {
            throw $this->createNotFoundException("Lien de changement de mot de passe non valide ou expiré");
        }

        $user = $passwordRenewToken->getUser();
        $form = $this->createForm(
            SelectPasswordType::class,
            $user,
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->edit($user, $form->get('password')->getData());
            $entityManager->remove($passwordRenewToken);
            $entityManager->flush();

            if ($security->getUser()) {
                $security->logout(false);
            }
            $request->getSession()->clear();

            return $this->redirectToRoute('admin_home');
        }

        return $this->render('public/select-password.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
        ], $response);
    }

    #[Route('/admin/technical-admin/csv', name: 'admin_technical_admin_csv', methods: ['GET'])]
    #[IsGranted('acl/User/view')]
    public function csv(
        ExportCsv $exportCsv,
        TechnicalAdminResultSet $technicalAdminResultSet,
    ): Response {
        return $exportCsv->fromResultSet($technicalAdminResultSet, ['admin_csv']);
    }

    #[Route('/admin/technical-admin/view/{username?}', name: 'admin_technical_admin_view', methods: ['GET'])]
    #[IsGranted('acl/User/view')]
    public function view(
        #[MapEntity(expr: 'repository.findWithAdminTechContext(username)')]
        User $user,
    ): Response {
        if ($user->getAdminTechAccessUser()?->getOpenid()) {
            $openid = $user->getAdminTechAccessUser()?->getOpenid();
            $loginUrl = $this->generateUrl(
                'public_openid',
                ['identifier' => $openid->getIdentifier()],
                UrlGeneratorInterface::ABSOLUTE_URL
            );
            $openid->setLoginUrl($loginUrl);
        }
        return $this->render('user/view.html.twig', [
            'user' => $user,
        ]);
    }
}
