<?php

namespace App\Controller\Admin;

use App\Entity\AccessUser;
use App\Entity\Notify;
use App\Entity\User;
use App\Form\AdminUserNotifyType;
use App\Form\AdminUserType;
use App\Repository\RoleRepository;
use App\Repository\UserCreationRequestRepository;
use App\ResultSet\AdminUserResultSet;
use App\Security\Voter\EntityIsDeletableVoter;
use App\Service\ExportCsv;
use App\Service\Manager\UserManager;
use App\Service\NotifyService;
use App\Service\Permission;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(priority: 1)]
class UserController extends AbstractController
{
    #[Route('/admin/user/{page?1}', name: 'admin_user', requirements: ['page' => '\d+'], methods: ['GET'])]
    #[IsGranted('acl/User/view')]
    public function adminUser(AdminUserResultSet $resultSet): Response
    {
        return $this->render('admin/user/index.html.twig', [
            'table' => $resultSet,
        ]);
    }

    #[Route('/admin/user/view/{username?}', name: 'admin_user_view', methods: ['GET'])]
    #[IsGranted('acl/User/view')]
    public function view(
        User $user,
    ): Response {
        return $this->render('user/view.html.twig', [
            'user' => $user,
        ]);
    }

    #[Route('/admin/user/add', name: 'admin_user_add', methods: ['GET', 'POST'])]
    #[IsGranted('acl/User/add_edit')]
    public function add(
        Request $request,
        UserManager $userManager,
        UserCreationRequestRepository $userCreationRequestRepository,
    ): Response {
        $user = new User();
        $user->setNotifyUserRegistration(true);
        $user->setUserDefinedPassword(false);

        $action = $this->generateUrl('admin_user_add');
        if ($creationRequestId = $request->get('user_creation_request')) {
            $userCreationRequest = $userCreationRequestRepository->find($creationRequestId);
            if (!$userCreationRequest) {
                throw $this->createNotFoundException("Cette demande de création d'utilisateur n'existe plus");
            }
            $action = $this->generateUrl('admin_user_add', ['user_creation_request' => $creationRequestId]);
            $user->setUsername($userCreationRequest->getUsername());
            $user->setName($userCreationRequest->getName());
            $user->setEmail($userCreationRequest->getEmail());
            if ($userCreationRequest->getTenant()) {
                $user->addTenant($userCreationRequest->getTenant());
            }
        }

        $form = $this->createForm(
            AdminUserType::class,
            $user,
            ['action' => $action]
        );
        $form->handleRequest($request);

        $response = new JsonResponse();
        if ($form->isSubmitted() && $form->isValid()) {
            $success = $userManager->add($user);

            $response->setData(AdminUserResultSet::normalize($user, ['index_admin', 'action']));
            $response->headers->add(['X-Asalae-Success' => $success ? 'true' : 'false']);

            if ($creationRequestId && isset($userCreationRequest)) {
                $this->addFlash('success', "{$user->getCommonName()} a bien été ajouté");
                $response->headers->add(['X-Asalae-Redirect' => $this->generateUrl('admin_user_creation_request')]);
            }

            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('admin/user/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/admin/user/edit/{username?}', name: 'admin_user_edit', methods: ['GET', 'POST'])]
    #[IsGranted('acl/User/add_edit')]
    public function editUser(
        string $username,
        User $user,
        Request $request,
        EntityManagerInterface $entityManager,
    ): Response {
        $form = $this->createForm(
            AdminUserType::class,
            $user,
            [
                'action' => $this->generateUrl('admin_user_edit', ['username' => $username]),
            ]
        );
        $form->handleRequest($request);

        $response = new JsonResponse();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($user);
            $entityManager->flush();

            $response->setData(AdminUserResultSet::normalize($user, ['index_admin', 'action']));
            $response->headers->add(['X-Asalae-Success' => 'true']);
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('admin/user/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/admin/user/delete/{username?}', name: 'admin_user_delete', methods: ['DELETE'])]
    #[IsGranted('acl/User/delete')]
    #[IsGranted(EntityIsDeletableVoter::class, 'user')]
    public function delete(
        #[MapEntity(expr: 'repository.findWithAdminTechContext(username)')]
        User $user,
        UserManager $userManager,
        TokenStorageInterface $tokenStorage,
    ): Response {
        /** @var User|null $tokenUser */
        $tokenUser = $tokenStorage->getToken()?->getUser();
        if ($tokenUser === $user) {
            throw $this->createAccessDeniedException();
        }

        $response = new Response();

        if ($userManager->deleteUser($user)) {
            return $response->setContent('done');
        }
        return $response->setContent('error');
    }

    #[Route('/admin/user/json/{username?}', name: 'admin_user_view_json', methods: ['GET'])]
    #[IsGranted('acl/User/view')]
    public function userJson(User $user): JsonResponse
    {
        return new JsonResponse(AdminUserResultSet::normalize($user, ['view']));
    }

    #[Route('/admin/user/csv', name: 'admin_user_csv', methods: ['GET'])]
    #[IsGranted('acl/User/view')]
    public function csv(
        ExportCsv $exportCsv,
        AdminUserResultSet $adminUserResultSet,
    ): Response {
        return $exportCsv->fromResultSet($adminUserResultSet, ['admin_csv']);
    }

    #[Route('/admin/user/notify/{username}', name: 'admin_user_notify', methods: ['GET', 'POST'])]
    #[IsGranted('acl/User/view')]
    public function notify(
        string $username,
        Request $request,
        EntityManagerInterface $entityManager,
        User $user,
        NotifyService $notifyService,
    ): Response {
        $notify = new Notify();
        $notify->setUser($user);
        $notify->setText("<h4>Message de l'administrateur</h4><p></p>");
        $form = $this->createForm(
            AdminUserNotifyType::class,
            $notify,
            [
                'action' => $this->generateUrl('admin_user_notify', ['username' => $username]),
            ]
        );
        $form->handleRequest($request);

        $response = new JsonResponse();
        if ($form->isSubmitted() && $form->isValid()) {
            $notify->setText($notify->getText());
            $entityManager->persist($notify);
            $entityManager->flush();

            $notifyService->send($notify);

            $response->headers->add(['X-Asalae-Success' => 'true']);
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('admin/user/notify.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/admin/user/promote/{username}', name: 'admin_user_promote_admin', methods: ['UPDATE'])]
    #[IsGranted('acl/User/add_edit')]
    public function promoteTechnicalAdmin(
        User $user,
        RoleRepository $roleRepository,
        EntityManagerInterface $entityManager,
    ): Response {
        if ($user->hasAdminAccess()) {
            throw $this->createAccessDeniedException();
        }
        $role = $roleRepository->findOneBy(['tenant' => null, 'name' => Permission::ROLE_TECHNICAL_ADMIN]);
        $accessUser = new AccessUser();
        $accessUser
            ->setUser($user)
            ->setRole($role)
        ;
        $user->addAccessUser($accessUser);
        $entityManager->persist($accessUser);
        $entityManager->flush();

        return new JsonResponse(
            AdminUserResultSet::normalize($user, ['index_admin', 'action']),
            headers: [
                'X-Asalae-Success' => 'true',
            ]
        );
    }

    #[Route('/admin/user/revoke/{username}', name: 'admin_user_revoke_admin', methods: ['DELETE'])]
    #[IsGranted('acl/User/add_edit')]
    public function revokeTechnicalAdmin(
        User $user,
        EntityManagerInterface $entityManager,
    ): Response {
        if (!$user->hasAdminAccess()) {
            throw $this->createAccessDeniedException();
        }
        $accessUser = $user->getAccessUsers()
            ->filter(fn (AccessUser $a) => $a->getTenant() === null)
            ->first()
        ;
        $entityManager->remove($accessUser);
        $entityManager->flush();

        return new JsonResponse(
            AdminUserResultSet::normalize($user, ['index_admin', 'action']),
            headers: [
                'X-Asalae-Success' => 'true',
            ]
        );
    }
}
