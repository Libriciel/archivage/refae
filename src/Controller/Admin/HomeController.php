<?php

namespace App\Controller\Admin;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(priority: 1)]
class HomeController extends AbstractController
{
    public function __construct(
        private readonly int $probeDuration,
    ) {
    }

    #[Route('/admin', name: 'admin')]
    #[Route('/admin/home', name: 'admin_home')]
    #[IsGranted('acl/AdminHome/view')]
    public function index(
        Request $request,
    ): Response {
        $topic = $this->generateUrl('admin_probe', referenceType: UrlGeneratorInterface::ABSOLUTE_URL);
        $dsnMail = preg_replace('#(.*://.*):.*?@#', '$1:************@', $_ENV['MAILER_DSN'] ?? 'null://null');
        return $this->render('admin/home.html.twig', [
            'topic_probe' => $topic,
            'meminfo' => $this->meminfo(),
            'disk' => $this->infodisk(),
            'web_port' => $request->getPort(),
            'mercure' => $_ENV['MERCURE_PUBLIC_URL'] ?? '',
            'dsn_mail' => $dsnMail,
        ]);
    }

    #[Route('/admin/probe', name: 'admin_probe')]
    #[IsGranted('acl/AdminHome/view')]
    public function probe(
        HubInterface $hub,
    ): Response {
        $lockfile = '/var/www/refae/tmp/probe.lock';
        $duration = $this->probeDuration;
        if (is_file($lockfile) && filemtime($lockfile) + $duration > microtime(true)) {
            throw new ConflictHttpException("another probe is running");
        }
        file_put_contents($lockfile, date(DATE_RFC3339));
        register_shutdown_function(fn() => is_file($lockfile) && @unlink($lockfile));

        set_time_limit($duration + 30);
        $limit = microtime(true) + $duration;
        $topic = $this->generateUrl('admin_probe', referenceType: UrlGeneratorInterface::ABSOLUTE_URL);
        while (microtime(true) < $limit) {
            $update = new Update(
                $topic,
                $this->meminfo(),
                true,
            );
            $hub->publish($update);
            sleep(1);
        }
        unlink($lockfile);
        return new Response("done");
    }

    #[Route('/admin/test-method', name: 'admin_test_method')]
    #[IsGranted('acl/AdminHome/view')]
    public function testMethod(
        Request $request,
    ): Response {
        return new Response($request->getMethod());
    }

    private function meminfo(): string
    {
        exec('cat /proc/meminfo', $output);
        $parsed = [];
        foreach ($output as $str) {
            if (strpos($str, ':')) {
                [$key, $value] = explode(':', $str, 2);
                $parsed[$key] = (int)$value;
            }
        }
        $probe = [
            'memory' => [
                'total' => $parsed['MemTotal'] ?? 0,
                'available' => $parsed['MemAvailable'] ?? 0,
            ],
            'swap' => [
                'total' => $parsed['SwapTotal'] ?? 0,
                'available' => $parsed['SwapFree'] ?? 0,
            ],
            'loadavg' => sys_getloadavg(),
        ];
        return json_encode($probe);
    }

    private function infodisk(): array
    {
        $keys = [
            'filesystem',
            'size',
            'used',
            'avail',
            'use',
            'mounted_on'
        ];
        exec('df -h', $disks);
        return array_combine($keys, preg_split('/\s+/', $disks[1], 6));
    }

    #[Route('/admin/send-test-mail', name: 'admin_send_test_mail')]
    #[IsGranted('acl/AdminHome/view')]
    public function sendTestMail(
        Request $request,
        MailerInterface $mailer,
    ): Response {
        $mailPrefix = '[refae]';
        $email = (new TemplatedEmail())
            ->from($_ENV['MAILER_FROM'] ?? 'refae@test.fr')
            ->to($request->get('email'))
            ->subject($mailPrefix . 'E-mail de test')
            ->htmlTemplate('email/test.html.twig')
        ;
        $mailer->send($email);
        return new JsonResponse(['success' => true]);
    }
}
