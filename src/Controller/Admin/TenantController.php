<?php

namespace App\Controller\Admin;

use App\Entity\Tenant;
use App\Form\TenantAddType;
use App\Form\TenantDeleteType;
use App\Form\TenantImportConfirmType;
use App\Form\TenantImportType;
use App\Form\TenantType;
use App\Repository\TenantRepository;
use App\ResultSet\TenantResultSet;
use App\ResultSet\TenantResultSetPreview;
use App\Security\Voter\EntityIsDeletableVoter;
use App\Service\Manager\TenantManager;
use App\Service\SessionFiles;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use IntlDateFormatter;
use Locale;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(priority: 1)]
class TenantController extends AbstractController
{
    #[Route('/admin/tenant/{page?1}', name: 'admin_tenant', requirements: ['page' => '\d+'], methods: ['GET'])]
    #[IsGranted('acl/Tenant/view')]
    public function index(TenantResultSet $resultSet): Response
    {
        return $this->render('admin/tenant/index.html.twig', [
            'table' => $resultSet,
        ]);
    }

    #[Route('/admin/tenant/add', name: 'admin_tenant_add', methods: ['GET', 'POST'])]
    #[IsGranted('acl/Tenant/add_edit')]
    public function add(
        Request $request,
        TenantManager $tenantManager,
    ): Response {
        $tenant = new Tenant();
        $tenant->setActive(true);
        $form = $this->createForm(
            TenantAddType::class,
            $tenant,
            ['action' => $this->generateUrl('admin_tenant_add')]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $initAccessRules = $form->get('initAccessRules')->getData();
            $tenantManager->save($tenant, ['initAccessRules' => $initAccessRules]);

            $response->setContent(
                json_encode(
                    TenantResultSet::normalize($tenant, ['index', 'action']),
                    JSON_THROW_ON_ERROR
                )
            );
            $response->headers->add(
                [
                    'Content-Type' => 'application/json',
                    'X-Asalae-Success' => 'true',
                ]
            );
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('admin/tenant/add.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/admin/tenant/edit/{tenant?}', name: 'admin_tenant_edit', methods: ['GET', 'POST'])]
    #[IsGranted('acl/Tenant/add_edit')]
    public function edit(
        Request $request,
        EntityManagerInterface $entityManager,
        Tenant $tenant,
    ): Response {
        $form = $this->createForm(
            TenantType::class,
            $tenant,
            [
                'action' => $this->generateUrl('admin_tenant_edit', ['tenant' => $tenant->getId()]),
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($tenant);
            $entityManager->flush();

            $response->setContent(
                json_encode(
                    TenantResultSet::normalize($tenant, ['index', 'action']),
                    JSON_THROW_ON_ERROR
                )
            );
            $response->headers->add(
                [
                    'Content-Type' => 'application/json',
                    'X-Asalae-Success' => 'true',
                ]
            );
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('admin/tenant/edit.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/admin/tenant/view/{tenant?}', name: 'admin_tenant_view', methods: ['GET'])]
    #[IsGranted('acl/Tenant/view')]
    public function view(Tenant $tenant): Response
    {
        return $this->render('admin/tenant/view.html.twig', [
            'tenant' => $tenant,
        ]);
    }

    #[Route('/admin/tenant/delete/{tenant?}', name: 'admin_tenant_delete', methods: ['GET', 'POST'])]
    #[IsGranted('acl/Tenant/delete')]
    #[IsGranted(EntityIsDeletableVoter::class, 'tenant')]
    public function delete(
        Tenant $tenant,
        TenantManager $tenantManager,
        Request $request,
    ): Response {
        $form = $this->createForm(
            TenantDeleteType::class,
            null,
            [
                'action' => $this->generateUrl('admin_tenant_delete', ['tenant' => $tenant->getId()]),
                'baseurl' => $tenant->getBaseurl(),
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            if ($formData['confirm'] !== $tenant->getBaseurl()) {
                throw new BadRequestException();
            }
            $tenantManager->deleteByForm($tenant, $formData);

            $response->setContent('done');
            $response->headers->add(
                [
                    'X-Asalae-Success' => 'true',
                    'X-Asalae-Redirect' => $this->generateUrl('admin_tenant'),
                ]
            );
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('admin/tenant/delete.html.twig', [
            'form' => $form->createView(),
            'baseurl' => $tenant->getBaseurl(),
        ], $response);
    }

    #[Route('/admin/tenant/export/{baseurl}', name: 'admin_tenant_export', methods: ['GET'])]
    #[IsGranted('acl/Tenant/export')]
    public function export(
        Tenant $tenant,
        TenantRepository $tenantRepository,
        TenantManager $tenantManager,
    ): Response {
        $query = $tenantRepository->createQueryBuilder('t')
            ->andWhere('t = :tenant')
            ->setParameter('tenant', $tenant)
        ;
        $zip = $tenantManager->export($query);
        $filename = sprintf(
            '%s_export_%s.%s.refae',
            (new DateTime())->format('Y-m-d_His'),
            $tenant->getBaseurl(),
            $tenantManager->getFileName()
        );
        $headers = [
            'Content-Description' => 'File Transfer',
            'Content-Type' => 'application/zip',
            'Content-Length' => strlen($zip),
            'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
            'Expires' => '0',
            'Cache-Control' => 'must-revalidate',
        ];
        return new Response($zip, Response::HTTP_OK, $headers);
    }

    #[Route(
        '/admin/tenant/import',
        name: 'admin_tenant_import1',
        methods: ['GET', 'POST']
    )]
    #[IsGranted('acl/Tenant/import')]
    public function import1(
        Request $request,
        SessionFiles $sessionFiles,
    ): Response {
        $form = $this->createForm(
            TenantImportType::class,
            options: ['action' => $this->generateUrl('admin_tenant_import1')]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $session_tmpfile_uuid = $request->get('tenant_import')['file'] ?? null;
            $uri = $sessionFiles->getUri($session_tmpfile_uuid);

            if (mime_content_type($uri) === 'application/zip') {
                $modalParams = [
                    'btnAcceptContent' => '<i class="fa fa-upload fa-space" aria-hidden="true"></i>'
                        . "<span>Importer</span>",
                    'btnCancelContent' => '<i class="fa fa-trash fa-space" aria-hidden="true"></i>'
                        . "<span>Annuler l'import</span>",
                    'btnCancelUrl' => $this->generateUrl(
                        'admin_tenant_delete_import',
                        ['session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ];
                $response->headers->add([
                    'X-Asalae-Modal-Params' => json_encode($modalParams),
                    'X-Asalae-Step-Title' => json_encode("Récapitulatif avant import"),
                    'X-Asalae-Step' => $this->generateUrl(
                        'admin_tenant_import2',
                        ['session_tmpfile_uuid' => $session_tmpfile_uuid],
                    ),
                ]);
                return $response;
            } else {
                $form->get('file')
                    ->addError(new FormError("Ce fichier ne semble pas contenir un tenant"));
            }
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('admin/tenant/import1.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/admin/tenant/import/{session_tmpfile_uuid}',
        name: 'admin_tenant_import2',
        methods: ['GET', 'POST']
    )]
    #[IsGranted('acl/Tenant/import')]
    public function import2(
        Request $request,
        string $session_tmpfile_uuid,
        TenantManager $tenantManager,
        TenantResultSetPreview $previewResultset,
    ): Response {
        $tenantManager->extract($session_tmpfile_uuid);
        $previewResultset->setDataFromArray($tenantManager->previewData);

        $form = $this->createForm(
            TenantImportConfirmType::class,
            null,
            [
                'action' => $this->generateUrl(
                    'admin_tenant_import2',
                    ['session_tmpfile_uuid' => $session_tmpfile_uuid],
                )
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $tenantManager->import($tenantManager->importZip);
            $this->addFlash(
                'success',
                $tenantManager->importableCount > 1
                    ? "$tenantManager->importableCount tenants importés avec succès"
                    : "$tenantManager->importableCount tenant importé avec succès"
            );
            $response->headers->add(['X-Asalae-Redirect' => $this->generateUrl('admin_tenant')]);
            return $response;
        }

        $formatter = new IntlDateFormatter(
            Locale::getDefault(),
            IntlDateFormatter::FULL,
            IntlDateFormatter::SHORT
        );
        return $this->render('admin/tenant/import2.html.twig', [
            'view_data' => [
                "Date du fichier d'export" => $formatter->format($tenantManager->exportDate),
                "Tenant" => $tenantManager->data['baseurl'],
                "Nombre d'accords de versement" => $tenantManager->nestedImportCount('agreements'),
                "Nombre de notices d'autorité" => $tenantManager->nestedImportCount('authorities'),
                "Nombre de catégories" => $tenantManager->nestedImportCount('categories'),
                "Nombre d'étiquettes" => $tenantManager->nestedImportCount('labels'),
                "Nombre de profils d'archives" => $tenantManager->nestedImportCount('profiles'),
                "Nombre de niveaux de service" => $tenantManager->nestedImportCount('serviceLevels'),
                "Nombre de vocabulaires contrôlés" => $tenantManager->nestedImportCount('vocabularies'),
            ],
            'errors' => $tenantManager->errors,
            'fatalError' => $tenantManager->hasFatalError,
            'form' => $form->createView(),
        ], $response);
    }

    #[Route(
        '/admin/tenant/delete-import/{session_tmpfile_uuid}',
        name: 'admin_tenant_delete_import',
        methods: ['DELETE'],
    )]
    #[IsGranted('acl/Tenant/import')]
    public function deleteImport(
        SessionFiles $sessionFiles,
        string $session_tmpfile_uuid,
    ): Response {
        $sessionFiles->delete($session_tmpfile_uuid);
        $response = new Response();
        $response->setContent('ok');
        $response->headers->add(['X-Asalae-Success' => 'true']);
        return $response;
    }

    #[Route(
        '/admin/tenant/close/{tenant}',
        name: 'admin_tenant_close',
        methods: ['UPDATE'],
    )]
    #[IsGranted('acl/Tenant/delete')]
    public function close(
        Tenant $tenant,
        EntityManagerInterface $entityManager,
    ): Response {
        $tenant->setActive(false);
        $entityManager->persist($tenant);
        $entityManager->flush();

        $response = new Response();
        $response->setContent(
            json_encode(
                TenantResultSet::normalize($tenant, ['index', 'action']),
                JSON_THROW_ON_ERROR
            )
        );
        $response->headers->add(
            [
                'Content-Type' => 'application/json',
                'X-Asalae-Success' => 'true',
            ]
        );
        return $response;
    }

    #[Route(
        '/admin/tenant/open/{tenant}',
        name: 'admin_tenant_open',
        methods: ['UPDATE'],
    )]
    #[IsGranted('acl/Tenant/delete')]
    public function open(
        Tenant $tenant,
        EntityManagerInterface $entityManager,
    ): Response {
        $tenant->setActive(true);
        $entityManager->persist($tenant);
        $entityManager->flush();

        $response = new Response();
        $response->setContent(
            json_encode(
                TenantResultSet::normalize($tenant, ['index', 'action']),
                JSON_THROW_ON_ERROR
            )
        );
        $response->headers->add(
            [
                'Content-Type' => 'application/json',
                'X-Asalae-Success' => 'true',
            ]
        );
        return $response;
    }
}
