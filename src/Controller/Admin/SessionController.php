<?php

namespace App\Controller\Admin;

use App\ResultSet\SessionResultSet;
use App\Session\CustomSessionHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(priority: 1)]
class SessionController extends AbstractController
{
    #[Route('/admin/session/{page?1}', name: 'admin_session', requirements: ['page' => '\d+'], methods: ['GET'])]
    #[IsGranted('acl/User/view')]
    public function index(
        SessionResultSet $sessionResultSet,
    ): Response {
        return $this->render('admin/session/index.html.twig', [
            'table' => $sessionResultSet,
        ]);
    }

    #[Route(
        '/admin/session/delete/{session}',
        name: 'admin_session_delete',
        requirements: ['page' => '\d+'],
        methods: ['DELETE'],
    )]
    #[IsGranted('acl/User/delete')]
    public function delete(
        string $session,
        CustomSessionHandler $customSessionHandler,
    ): Response {
        $customSessionHandler->deleteById($session);
        return new Response('done');
    }
}
