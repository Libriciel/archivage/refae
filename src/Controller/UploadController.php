<?php

namespace App\Controller;

use App\Service\SessionFiles;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Attribute\Route;

class UploadController extends AbstractController
{
    #[Route('/public/upload', name: 'public_upload')]
    public function upload(
        Request $request,
        SessionFiles $sessionFiles,
    ): Response {
        if (preg_match('/[^\w-]/', (string) $request->get('uuid'))) {
            throw new BadRequestHttpException("invalid uuid");
        }
        if ($request->isMethod('DELETE')) {
            $relativeUri = $sessionFiles->delete($request->get('uuid'));
            $response = new Response();
            $response->setContent("deleted $relativeUri");
            return $response;
        }
        $sessionFiles->checkValidChunk();
        $sessionFiles->writeChunk();

        $response = new Response();
        if ($sessionFiles->isLastChunk()) {
            $sessionFiles->mergeChunks();
            $relativeUri = $sessionFiles->getRelativeUri();
            $response->setContent("done " . $relativeUri);
        } else {
            $existingChunks = $sessionFiles->getCompletedChunksCount();
            $totalChunk = $sessionFiles->getTotalChunkCount();
            $response->setContent(sprintf("partial %d/%d", $existingChunks, $totalChunk));
        }
        return $response;
    }
}
