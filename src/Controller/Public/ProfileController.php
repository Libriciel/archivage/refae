<?php

namespace App\Controller\Public;

use App\Entity\Profile;
use App\ResultSet\PublicProfileFileViewResultSet;
use App\ResultSet\PublicProfileResultSet;
use App\Security\Voter\IsPublicVoter;
use App\Service\ExportCsv;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class ProfileController extends AbstractController
{
    #[Route('/public/profile/{page?1}', name: 'public_profile', requirements: ['page' => '\d+'])]
    public function index(
        PublicProfileResultSet $table,
    ): Response {
        return $this->render('public/profile.html.twig', [
            'table' => $table,
        ]);
    }

    #[IsGranted(IsPublicVoter::class, 'profile')]
    #[Route(
        '/public/profile/view/{profile?}',
        name: 'public_profile_view',
        methods: ['GET'],
        priority: 1,
    )]
    public function view(
        #[MapEntity(expr: 'repository.findWithPreviousVersions(profile)')]
        Profile $profile,
        PublicProfileFileViewResultSet $publicProfileFileViewResultSet,
    ): Response {
        $publicProfileFileViewResultSet->initialize($profile);
        return $this->render('profile/view.html.twig', [
            'profile' => $profile,
            'files' => $publicProfileFileViewResultSet,
        ]);
    }

    #[Route('/public/profile/csv', name: 'public_profile_csv', methods: ['GET'])]
    public function csv(
        PublicProfileResultSet $publicProfileResultSet,
        ExportCsv $exportCsv,
    ): Response {
        return $exportCsv->fromResultSet($publicProfileResultSet, ['csv', 'index_public']);
    }
}
