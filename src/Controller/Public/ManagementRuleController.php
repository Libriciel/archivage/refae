<?php

namespace App\Controller\Public;

use App\Entity\ManagementRule;
use App\ResultSet\PublicManagementRuleFileViewResultSet;
use App\ResultSet\PublicManagementRuleResultSet;
use App\Security\Voter\IsPublicVoter;
use App\Service\ExportCsv;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class ManagementRuleController extends AbstractController
{
    #[Route('/public/management-rule/{page?1}', name: 'public_management_rule', requirements: ['page' => '\d+'])]
    public function index(
        PublicManagementRuleResultSet $table,
    ): Response {
        return $this->render('public/management-rule.html.twig', [
            'table' => $table,
        ]);
    }

    #[IsGranted(IsPublicVoter::class, 'managementRule')]
    #[Route(
        '/public/management-rule/view/{managementRule?}',
        name: 'public_management_rule_view',
        methods: ['GET'],
        priority: 1,
    )]
    public function view(
        #[MapEntity(expr: 'repository.findWithPreviousVersions(managementRule)')]
        ManagementRule $managementRule,
        PublicManagementRuleFileViewResultSet $publicManagementRuleFileViewResultSet,
    ): Response {
        $publicManagementRuleFileViewResultSet->initialize($managementRule);
        return $this->render('management_rule/view.html.twig', [
            'managementRule' => $managementRule,
            'files' => $publicManagementRuleFileViewResultSet,
        ]);
    }

    #[Route('/public/management-rule/csv', name: 'public_management_rule_csv', methods: ['GET'])]
    public function csv(
        PublicManagementRuleResultSet $publicManagementRuleResultSet,
        ExportCsv $exportCsv,
    ): Response {
        return $exportCsv->fromResultSet($publicManagementRuleResultSet, ['csv', 'index_public']);
    }
}
