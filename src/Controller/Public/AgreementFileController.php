<?php

namespace App\Controller\Public;

use App\Controller\DownloadFileTrait;
use App\Entity\AgreementFile;
use App\Entity\VersionableEntityInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class AgreementFileController extends AbstractController
{
    use DownloadFileTrait;

    #[Route(
        '/public/agreement-file/{agreementFile}/download',
        name: 'public_agreement_file_download',
        methods: ['GET']
    )]
    public function download(
        AgreementFile $agreementFile,
    ): Response {
        $agreement = $agreementFile->getAgreement();
        if (
            $agreement->isPublic() === false
            || $agreement->getState() !== VersionableEntityInterface::S_PUBLISHED
        ) {
            throw $this->createAccessDeniedException();
        }
        return $this->getFileDownloadResponse($agreementFile->getFile());
    }
}
