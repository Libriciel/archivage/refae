<?php

namespace App\Controller\Public;

use App\Entity\ServiceLevel;
use App\ResultSet\PublicServiceLevelFileViewResultSet;
use App\ResultSet\PublicServiceLevelResultSet;
use App\Security\Voter\IsPublicVoter;
use App\Service\ExportCsv;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class ServiceLevelController extends AbstractController
{
    #[Route('/public/service-level/{page?1}', name: 'public_service_level', requirements: ['page' => '\d+'])]
    public function index(
        PublicServiceLevelResultSet $table,
    ): Response {
        return $this->render('public/service-level.html.twig', [
            'table' => $table,
        ]);
    }

    #[IsGranted(IsPublicVoter::class, 'serviceLevel')]
    #[Route(
        '/public/service-level/view/{serviceLevel?}',
        name: 'public_service_level_view',
        methods: ['GET'],
        priority: 1,
    )]
    public function view(
        #[MapEntity(expr: 'repository.findWithPreviousVersions(serviceLevel)')]
        ServiceLevel $serviceLevel,
        PublicServiceLevelFileViewResultSet $publicServiceLevelFileViewResultSet,
    ): Response {
        $publicServiceLevelFileViewResultSet->initialize($serviceLevel);
        return $this->render('service_level/view.html.twig', [
            'serviceLevel' => $serviceLevel,
            'files' => $publicServiceLevelFileViewResultSet,
        ]);
    }

    #[Route('/public/service-level/csv', name: 'public_service_level_csv', methods: ['GET'])]
    public function csv(
        PublicServiceLevelResultSet $publicServiceLevelResultSet,
        ExportCsv $exportCsv,
    ): Response {
        return $exportCsv->fromResultSet($publicServiceLevelResultSet, ['csv', 'index_public']);
    }
}
