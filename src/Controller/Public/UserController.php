<?php

namespace App\Controller\Public;

use App\Entity\AccessUser;
use App\Entity\Notify;
use App\Entity\Tenant;
use App\Entity\User;
use App\Entity\UserCreationRequest;
use App\Form\MyUserType;
use App\Form\SelectPasswordType;
use App\Form\UserCreationRequestType;
use App\Repository\TenantRepository;
use App\Repository\UserCreationRequestRepository;
use App\Repository\UserPasswordRenewTokenRepository;
use App\Repository\UserRepository;
use App\Security\TenantAuthenticator;
use App\Service\Manager\UserManager;
use App\Service\NotifyService;
use App\Session\CustomSessionHandler;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UserController extends AbstractController
{
    #[Route('/public/edit-my-user', name: 'public_edit_my_user')]
    public function editMyUser(
        Security $security,
        Request $request,
        UserManager $userManager,
    ): Response {
        $user = $security->getUser();
        if (!$user instanceof User) {
            throw new HttpException(401);
        }

        $form = $this->createForm(
            MyUserType::class,
            $user,
            [
                'action' => $this->generateUrl('public_edit_my_user'),
            ]
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->edit($user, $form->get('password')->getData());
            $response->headers->add(['X-Asalae-Success' => 'true']);
            return $response;
        } elseif ($form->isSubmitted()) {
            $response->headers->add(['X-Asalae-Success' => 'false']);
        }

        return $this->render('public/edit_user.html.twig', [
            'form' => $form->createView(),
        ], $response);
    }

    #[Route('/public/user/register', name: 'public_register_user')]
    public function createUser(
        Request $request,
        EntityManagerInterface $entityManager,
        MailerInterface $mailer,
        TenantRepository $tenantRepository,
    ): Response {
        /** @var EntityRepository $tenantRepository */
        $count = $tenantRepository->count([]);
        $tenant = null;
        if ($count === 1) {
            $tenant = $tenantRepository->findOneBy([]);
        }
        $form = $this->createUserCreationRequest($tenant, $request, $entityManager, $mailer);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('public_root');
        }

        return $this->render('login/register-user.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{tenant_url}/user/register', name: 'public_register_tenant_user')]
    public function createTenantUser(
        string $tenant_url,
        Request $request,
        EntityManagerInterface $entityManager,
        MailerInterface $mailer,
        TenantRepository $tenantRepository,
    ): Response {
        $tenant = $tenantRepository->findOneByBaseurl($tenant_url);
        if (!$tenant) {
            throw $this->createNotFoundException("Tenant $tenant_url not found");
        }
        $form = $this->createUserCreationRequest($tenant, $request, $entityManager, $mailer);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('public_root');
        }

        return $this->render('login/register-user.html.twig', [
            'form' => $form->createView(),
            'tenant_configuration' => $tenant->getConfiguration(),
        ]);
    }

    private function createUserCreationRequest(
        ?Tenant $tenant,
        Request $request,
        EntityManagerInterface $entityManager,
        MailerInterface $mailer,
    ): FormInterface {
        $userCreationRequest =  new UserCreationRequest();
        if ($username = $request->get('username')) {
            $userCreationRequest->setUsername($username);
        }
        $userCreationRequest->setIp($request->getClientIp());
        $userCreationRequest->setTenant($tenant);
        $form = $this->createForm(UserCreationRequestType::class, $userCreationRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UserCreationRequest $userCreationRequest */
            $userCreationRequest = $form->getData();

            $entityManager->persist($userCreationRequest);
            $entityManager->flush();

            $url = $this->generateUrl(
                'public_user_validate_email',
                ['token' => $userCreationRequest->getEmailToken()],
                UrlGeneratorInterface::ABSOLUTE_URL,
            );
            $this->sendVerificationEmail($mailer, $userCreationRequest, $url);
            $this->addFlash(
                'success',
                "La demande de création d'utilisateur a bien été envoyée, un email vient de vous être envoyé",
            );
        }

        return $form;
    }

    private function sendVerificationEmail(
        MailerInterface $mailer,
        UserCreationRequest $userCreationRequest,
        string $signedUrl
    ): void {
        $configuration = $userCreationRequest->getTenant()?->getConfiguration();
        $mailPrefix = $configuration?->getMailPrefix() ?: '[refae]';
        $email = (new TemplatedEmail())
            ->from($_ENV['MAILER_FROM'] ?? 'refae@test.fr')
            ->to($userCreationRequest->getEmail())
            ->subject($mailPrefix . ' Vérification de votre email')
            ->htmlTemplate('email/verification-mail.html.twig')
            ->context([
                'name' => $userCreationRequest->getName(),
                'url' => $signedUrl,
                'signature' => $configuration?->getMailSignature(),
            ])
        ;
        $mailer->send($email);
    }

    private function sendMailToAdmins(
        MailerInterface $mailer,
        UserCreationRequest $userCreationRequest,
        UserRepository $userRepository,
        EntityManagerInterface $entityManager,
        NotifyService $notifyService,
    ): void {
        $tenant = $userCreationRequest->getTenant();
        $toNotify = $userRepository->findNotifyUserRegistration($tenant);
        if (!$toNotify) {
            return;
        }
        $to = [];

        $name = htmlentities((string) $userCreationRequest->getCommonName());
        if ($tenant) {
            $urlRequest = $this->generateUrl(
                'app_user_creation_request',
                ['tenant_url' => $tenant->getBaseurl()],
            );
        } else {
            $urlRequest = $this->generateUrl('admin_user_creation_request');
        }
        $link = '<a href="' . $urlRequest . '">'
            . 'Administration technique/Demandes de création d\'utilisateurs</a>'
        ;
        $text = "<h4>Nouvelle demande de création d'utilisateur</h4><p>" .
            "Une demande de création d'utilisateur pour $name vient d'être émise." .
            " L'adresse email a été confirmée." .
            "</p><p>Vous pouvez consulter les demandes dans le menu $link</p>"
        ;

        /** @var User $user */
        foreach ($toNotify as $user) {
            $to[$user->getEmail()] = true;
            $notify = new Notify();
            $notify->setText($text);
            $notify->setUser($user);
            $notify->setCssClass('alert-info');
            $entityManager->persist($notify);
            $entityManager->flush();
            $notifyService->send($notify);
        }
        $to = array_keys($to);

        if ($tenant ?? null) {
            $url = $this->generateUrl(
                'app_user_creation_request',
                ['tenant_url' => $tenant->getBaseurl(), 'id[1]' => $userCreationRequest->getId()],
                UrlGeneratorInterface::ABSOLUTE_URL
            );
        } else {
            $url = $this->generateUrl(
                'admin_user_creation_request',
                ['id[1]' => $userCreationRequest->getId()],
                UrlGeneratorInterface::ABSOLUTE_URL
            );
        }
        $unsubscribeUrl = $this->generateUrl(
            'public_unsubscribe',
            ['type' => 'user-creation-requests'],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
        $configuration = $tenant?->getConfiguration();
        $mailPrefix = $configuration?->getMailPrefix() ?: '[refae]';
        $username = $userCreationRequest->getUsername();
        $email = (new TemplatedEmail())
            ->from($_ENV['MAILER_FROM'] ?? 'refae@test.fr')
            ->subject("$mailPrefix Demande de création de l'utilisateur $username")
            ->htmlTemplate('email/user-creation-request.html.twig')
            ->context([
                'user_creation_request' => $userCreationRequest,
                'url' => $url,
                'signature' => $configuration?->getMailSignature(),
                'unsubscribe' => $unsubscribeUrl,
            ])
        ;
        foreach ($to as $recipient) {
            $email->to($recipient);
            $mailer->send($email);
        }
    }

    #[Route('/public/user/validate-email/{token}', name: 'public_user_validate_email')]
    public function validateEmail(
        string $token,
        UserCreationRequestRepository $creationRequestRepository,
        UserRepository $userRepository,
        EntityManagerInterface $entityManager,
        MailerInterface $mailer,
        NotifyService $notifyService,
    ) {
        $creationRequestRepository->deleteExpired();
        $userCreationRequest = $creationRequestRepository->findOneBy(['emailToken' => $token]);
        if (
            !$userCreationRequest
            || $userRepository->count(['username' => $userCreationRequest->getUsername()]) > 0
            || $userCreationRequest->isEmailVerified()
        ) {
            $this->addFlash(
                'danger',
                "Lien de validation non valide ou expiré",
            );
            return $this->redirectToRoute('public_root');
        }
        $userCreationRequest->setEmailVerified(true);
        $entityManager->persist($userCreationRequest);
        $entityManager->flush();

        $this->sendMailToAdmins(
            $mailer,
            $userCreationRequest,
            $userRepository,
            $entityManager,
            $notifyService,
        );

        $this->addFlash(
            'success',
            "Votre email a été validé avec succès, votre demande de " .
            "création a été transmise aux administrateurs",
        );
        return $this->redirectToRoute('public_root');
    }

    #[Route('/public/user/select-password/{token}', name: 'public_user_select_password')]
    public function selectPassword(
        string $token,
        UserPasswordRenewTokenRepository $passwordRenewTokenRepository,
        Request $request,
        UserManager $userManager,
        EntityManagerInterface $entityManager,
        Security $security,
        TenantAuthenticator $tenantAuthenticator,
    ) {
        $passwordRenewTokenRepository->deleteExpired();
        $passwordRenewToken = $passwordRenewTokenRepository->findOneBy(['token' => $token]);
        if (!$passwordRenewToken) {
            throw $this->createNotFoundException("Lien de changement de mot de passe non valide ou expiré");
        }

        $user = $passwordRenewToken->getUser();
        $form = $this->createForm(
            SelectPasswordType::class,
            $user,
        );
        $form->handleRequest($request);

        $response = new Response();
        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->edit($user, $form->get('password')->getData());
            $entityManager->remove($passwordRenewToken);
            $entityManager->flush();

            if ($security->getUser()) {
                $security->logout(false);
            }
            $request->getSession()->clear();

            $tenant = $user->getTenants()->count() === 1 ? $user->getTenants()->first() : null;

            if ($user->getAccessUsers()->count() === 1) {
                $accessUser = $user->getAccessUsers()->first();
                if ($accessUser->getTenant() !== $tenant) {
                    $accessUser = null;
                }
            } elseif ($tenant) {
                $accessUser = $user->getAccessUsers()
                    ->filter(fn(AccessUser $au) => $au->getTenant() === $tenant)
                    ->first();
            } else {
                $accessUser = null;
            }
            if ($accessUser && $tenant) {
                $tenantAuthenticator->user = $user;
                $tenantAuthenticator->link = $accessUser;
                $tenantAuthenticator->username = $user->getUsername();
                $tenantAuthenticator->tenant_url = $tenant->getBaseurl();
                $security->login($user, TenantAuthenticator::class);
            }

            if ($tenant) {
                return $this->redirectToRoute('app_home', ['tenant_url' => $tenant->getBaseurl()]);
            }
            return $this->redirectToRoute('public_home');
        }

        return $this->render('public/select-password.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
        ], $response);
    }

    #[Route('/public/session-check', name: 'public_user_session_check')]
    public function sessionCheck(
        Security $security,
        Request $request,
        CustomSessionHandler $sessionHandler,
    ): Response {
        $user = $security->getUser();
        $connected = $user instanceof User;
        $session = $request->getSession();
        if ($request->get('tenant_url')) {
            $loginUrl = $this->generateUrl(
                'app_tenant_login_username',
                [
                    'tenant_url' => $request->get('tenant_url'),
                    'username' => $request->get('username')
                ]
            );
        } else {
            $loginUrl = $this->generateUrl(
                'public_login_username',
                ['username' => $request->get('username')]
            );
        }
        $result = [
            'connected' => $connected,
            'time_remaining' => $sessionHandler->getRemainingTime($session->getId()),
            'login_url' => $loginUrl,
        ];
        return new JsonResponse($result);
    }
}
