<?php

namespace App\Controller\Public;

use App\Entity\Category;
use App\Entity\DocumentRule;
use App\Repository\CategoryRepository;
use App\ResultSet\PublicDocumentRuleFileViewResultSet;
use App\ResultSet\PublicDocumentRuleResultSet;
use App\Security\Voter\IsPublicVoter;
use App\Service\ExportCsv;
use App\Service\Manager\CategoryManager;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class DocumentRuleController extends AbstractController
{
    #[Route('/public/{page?1}', name: 'public', requirements: ['page' => '\d+'])]
    #[Route('/public/document-rule/{page?1}', name: 'public_home', requirements: ['page' => '\d+'])]
    #[Route(
        '/public/document-rule/{page?1}/{names?}',
        name: 'public_document_rule',
        requirements: ['page' => '\d+', 'names' => '.+']
    )]
    public function tenantDocumentRule(
        ?string $names,
        CategoryRepository $categoryRepository,
        PublicDocumentRuleResultSet $publicCategoryResultSet,
    ): Response {
        $childCategories = [];
        $category = $this->getCategory($names, $categoryRepository, $publicCategoryResultSet, $childCategories);
        $childCategories = array_unique($childCategories);
        sort($childCategories);

        return $this->render('public/document_rule.html.twig', [
            'table' => $publicCategoryResultSet,
            'category' => $category,
            'childCategories' => $childCategories,
        ]);
    }

    #[IsGranted(IsPublicVoter::class, 'documentRule')]
    #[Route(
        '/public/document-rule/view/{documentRule?}',
        name: 'public_document_rule_view',
        methods: ['GET'],
        priority: 1,
    )]
    public function view(
        #[MapEntity(expr: 'repository.findWithPreviousVersions(documentRule)')]
        DocumentRule $documentRule,
        CategoryManager $categoryManager,
        PublicDocumentRuleFileViewResultSet $documentRuleFileViewResultSet,
    ): Response {
        $documentRuleFileViewResultSet->initialize($documentRule);
        return $this->render('document_rule/view.html.twig', [
            'documentRule' => $categoryManager->fillDocumentRulePath($documentRule),
            'files' => $documentRuleFileViewResultSet,
        ]);
    }

    #[Route(
        '/public/document-rule/csv/{names?}',
        name: 'public_document_rule_csv',
        requirements: ['names' => '.+']
    )]
    public function csv(
        ?string $names,
        PublicDocumentRuleResultSet $publicDocumentRuleResultSet,
        ExportCsv $exportCsv,
        CategoryRepository $categoryRepository,
    ): Response {
        $childCategories = [];
        $this->getCategory($names, $categoryRepository, $publicDocumentRuleResultSet, $childCategories);
        return $exportCsv->fromResultSet($publicDocumentRuleResultSet, ['csv', 'index_public']);
    }

    private function getCategory(
        ?string $names,
        CategoryRepository $categoryRepository,
        PublicDocumentRuleResultSet $publicCategoryResultSet,
        &$childCategories
    ): ?Category {
        $names = $names ? explode('/', $names) : [];
        $category = null;
        if ($names) {
            $categories = $categoryRepository->findByPath($names);
            if (!$categories) {
                throw $this->createNotFoundException("Cette catégorie n'existe pas");
            }
            $childCategories = [];
            foreach ($categories as $category) {
                $categoryNames = $category->getChildren()->map(fn (Category $cat) => $cat->getName())->toArray();
                $childCategories = array_merge($childCategories, $categoryNames);
            }
            $category = $categories[0];
            $publicCategoryResultSet->initialize($categories);
        } else {
            $childCategories = array_map(
                fn (Category $cat) => $cat->getName(),
                $categoryRepository->findBy(
                    ['parent' => null]
                )
            );
            $publicCategoryResultSet->initialize([]);
        }
        return $category;
    }
}
