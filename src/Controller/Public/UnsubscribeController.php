<?php

namespace App\Controller\Public;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Attribute\Route;

class UnsubscribeController extends AbstractController
{
    #[Route('/public/unsubscribe/{type}', name: 'public_unsubscribe')]
    public function doUnsubscribe(
        string $type,
        EntityManagerInterface $entityManager,
    ): Response {
        $user = $this->getUser();
        if (!$user instanceof User) {
            throw new UnauthorizedHttpException('Basic realm="refae"');
        }
        if ($type === 'user-creation-requests') {
            $user->setNotifyUserRegistration(false);
            $entityManager->persist($user);
            $entityManager->flush();
            $this->addFlash(
                'success',
                "Vous n'êtes plus abonné aux demandes de création " .
                "d'utilisateurs, vous pouvez paramétrer vos abonnements dans " .
                "\"Modifier mon compte utilisateur\"",
            );
        }
        $tenants = $user->getTenants();
        if ($tenants->count([]) === 1) {
            $tenant = $tenants->first()->getBaseurl();
        }
        if (isset($tenant)) {
            return $this->redirectToRoute('app_home', ['tenant_url' => $tenant]);
        } else {
            return $this->redirectToRoute('public_home');
        }
    }
}
