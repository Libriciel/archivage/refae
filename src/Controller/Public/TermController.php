<?php

namespace App\Controller\Public;

use App\Entity\Vocabulary;
use App\ResultSet\PublicTermResultSet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class TermController extends AbstractController
{
    #[Route('/public/vocabulary/{vocabulary}/term/{page?1}', name: 'public_term', requirements: ['page' => '\d+'])]
    public function index(
        PublicTermResultSet $table,
        Vocabulary $vocabulary,
    ): Response {
        $table->setVocabulary($vocabulary);
        return $this->render('public/term.html.twig', [
            'table' => $table,
            'vocabulary' => $vocabulary,
        ]);
    }
}
