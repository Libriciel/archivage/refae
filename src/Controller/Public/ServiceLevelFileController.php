<?php

namespace App\Controller\Public;

use App\Controller\DownloadFileTrait;
use App\Entity\ServiceLevelFile;
use App\Entity\VersionableEntityInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ServiceLevelFileController extends AbstractController
{
    use DownloadFileTrait;

    #[Route(
        '/public/service-level-file/{serviceLevelFile}/download',
        name: 'public_service_level_file_download',
        methods: ['GET']
    )]
    public function download(
        ServiceLevelFile $serviceLevelFile,
    ): Response {
        $serviceLevel = $serviceLevelFile->getServiceLevel();
        if (
            $serviceLevel->isPublic() === false
            || $serviceLevel->getState() !== VersionableEntityInterface::S_PUBLISHED
        ) {
            throw $this->createAccessDeniedException();
        }
        return $this->getFileDownloadResponse($serviceLevelFile->getFile());
    }
}
