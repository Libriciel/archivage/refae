<?php

namespace App\Controller\Public;

use App\Controller\DownloadFileTrait;
use App\Entity\VersionableEntityInterface;
use App\Entity\VocabularyFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class VocabularyFileController extends AbstractController
{
    use DownloadFileTrait;

    #[Route(
        '/public/vocabulary-file/{vocabularyFile}/download',
        name: 'public_vocabulary_file_download',
        methods: ['GET']
    )]
    public function download(
        VocabularyFile $vocabularyFile,
    ): Response {
        $vocabulary = $vocabularyFile->getVocabulary();
        if (
            $vocabulary->isPublic() === false
            || $vocabulary->getState() !== VersionableEntityInterface::S_PUBLISHED
        ) {
            throw $this->createAccessDeniedException();
        }
        return $this->getFileDownloadResponse($vocabularyFile->getFile());
    }
}
