<?php

namespace App\Controller\Public;

use App\Entity\Vocabulary;
use App\ResultSet\PublicVocabularyFileViewResultSet;
use App\ResultSet\PublicVocabularyResultSet;
use App\Security\Voter\IsPublicVoter;
use App\Service\ExportCsv;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class VocabularyController extends AbstractController
{
    #[Route('/public/vocabulary/{page?1}', name: 'public_vocabulary', requirements: ['page' => '\d+'])]
    public function index(
        PublicVocabularyResultSet $table,
    ): Response {
        return $this->render('public/vocabulary.html.twig', [
            'table' => $table,
        ]);
    }

    #[IsGranted(IsPublicVoter::class, 'vocabulary')]
    #[Route(
        '/public/vocabulary/view/{vocabulary?}',
        name: 'public_vocabulary_view',
        methods: ['GET'],
        priority: 1,
    )]
    public function view(
        #[MapEntity(expr: 'repository.findWithPreviousVersions(vocabulary)')]
        Vocabulary $vocabulary,
        PublicVocabularyFileViewResultSet $publicVocabularyFileViewResultSet,
    ): Response {
        $publicVocabularyFileViewResultSet->initialize($vocabulary);
        return $this->render('vocabulary/view.html.twig', [
            'vocabulary' => $vocabulary,
            'files' => $publicVocabularyFileViewResultSet,
        ]);
    }

    #[Route('/public/vocabulary/csv', name: 'public_vocabulary_csv', methods: ['GET'])]
    public function csv(
        PublicVocabularyResultSet $publicVocabularyResultSet,
        ExportCsv $exportCsv,
    ): Response {
        return $exportCsv->fromResultSet($publicVocabularyResultSet, ['csv', 'index_public']);
    }
}
