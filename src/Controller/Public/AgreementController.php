<?php

namespace App\Controller\Public;

use App\Entity\Agreement;
use App\ResultSet\PublicAgreementFileViewResultSet;
use App\ResultSet\PublicAgreementResultSet;
use App\Security\Voter\IsPublicVoter;
use App\Service\ExportCsv;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class AgreementController extends AbstractController
{
    #[Route('/public/agreement/{page?1}', name: 'public_agreement', requirements: ['page' => '\d+'])]
    public function index(
        PublicAgreementResultSet $table,
    ): Response {
        return $this->render('public/agreement.html.twig', [
            'table' => $table,
        ]);
    }

    #[IsGranted(IsPublicVoter::class, 'agreement')]
    #[Route(
        '/public/agreement/view/{agreement?}',
        name: 'public_agreement_view',
        methods: ['GET'],
        priority: 1,
    )]
    public function view(
        #[MapEntity(expr: 'repository.findWithPreviousVersions(agreement)')]
        Agreement $agreement,
        PublicAgreementFileViewResultSet $publicAgreementFileViewResultSet,
    ): Response {
        $publicAgreementFileViewResultSet->initialize($agreement);
        return $this->render('agreement/view.html.twig', [
            'agreement' => $agreement,
            'files' => $publicAgreementFileViewResultSet,
        ]);
    }

    #[Route('/public/agreement/csv', name: 'public_agreement_csv', methods: ['GET'])]
    public function csv(
        PublicAgreementResultSet $publicAgreementResultSet,
        ExportCsv $exportCsv,
    ): Response {
        return $exportCsv->fromResultSet($publicAgreementResultSet, ['csv', 'index_public']);
    }
}
