<?php

namespace App\Controller\Public;

use App\Entity\Notify;
use App\Entity\TableFilters;
use App\Entity\User;
use App\Form\TableFiltersType;
use App\Repository\TableRepository;
use App\Service\MeilisearchInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Attribute\Route;

class PublicController extends AbstractController
{
    #[Route('/public/view/{id?}', name: 'public_view')]
    public function view(int $id): Response
    {
        return $this->render('home/view.html.twig', ['id' => $id]);
    }

    #[Route('/public/save-table', name: 'public_save_table')]
    public function saveTable(
        Request $request,
        TableRepository $tableRepository,
    ): Response {
        $data = json_decode($request->getContent(), true);
        $tableRepository->saveMemoryData($data);
        return new Response('OK');
    }

    #[Route('/public/save-filters', name: 'public_save_filters')]
    public function saveFilters(
        Request $request,
        TableRepository $tableRepository,
        EntityManagerInterface $entityManager,
    ): Response {
        $tableFilters = new TableFilters();
        $tableFilters->setTable($tableRepository->find($request->get('table_filters')['table']));

        $form = $this->createForm(
            TableFiltersType::class,
            $tableFilters
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($tableFilters);
            $entityManager->flush();
            $this->addFlash(
                'success',
                "Sauvegarde effectuée avec succès",
            );
        } else {
            $this->addFlash(
                'danger',
                "Echec lors de la sauvegarde",
            );
        }

        $route = $request->headers->get('referer');

        return $this->redirect($route);
    }

    #[Route('/public/delete-filters/{id?}', name: 'public_filters_delete')]
    public function deleteFilters(
        TableFilters $tableFilters,
        Request $request,
        Security $security,
        EntityManagerInterface $entityManager,
    ): Response {
        $user = $security->getUser();
        $table = $tableFilters->getTable();
        if ($user) {
            $isTableOwner = $user->getUserIdentifier() === $table->getUserId();
        } else {
            $isTableOwner = $request->cookies->get('guest_uuid') === $table->getGuestToken();
        }
        if (!$isTableOwner) {
            throw new HttpException(403);
        }
        $entityManager->remove($tableFilters);
        $entityManager->flush();

        $response = new Response();
        $response->setContent('done');
        return $response;
    }

    #[Route('/public/search', name: 'public_search', priority: 1)]
    public function search(
        Request $request,
        MeilisearchInterface $meilisearch,
    ): Response {
        if ($request->get('tenant')) {
            /** @var User $user */
            $user = $this->getUser();
            $found = false;
            foreach ($user?->getTenants() ?: [] as $tenant) {
                if ($tenant->getBaseurl() === $request->get('tenant')) {
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                throw new HttpException(403);
            }

            $searchParams['filter'] = [
                'tenantUrl = ' . $request->get('tenant'),
            ];
            if ($request->get('public')) {
                $searchParams['filter'][] = 'public = true';
            }
            if ($request->get('published')) {
                $searchParams['filter'][] = 'stateTranslation = Publié';
            } else {
                $searchParams['filter'][] = 'last_version = true';
            }
        } else {
            $searchParams['filter'] = [
                'public = true',
                'stateTranslation = Publié',
            ];
        }
        $searchParams['sort'] = ['version:desc'];
        $searchParams['showMatchesPosition'] = true;
        $result = $meilisearch->search(
            $request->get('index'),
            $request->get('search'),
            $searchParams,
        );
        return $this->json($result);
    }

    /**
     * Juste une route mercure
     * @return mixed
     */
    #[Route('/public/notify/user/{id}', name: 'public_notify_user')]
    public function notifyUser(): never
    {
        throw new HttpException(404);
    }

    #[Route('/public/notify/delete/{id?}', name: 'public_user_delete_notification')]
    public function notifyDelete(
        Notify $notify,
        Security $security,
        EntityManagerInterface $entityManager,
    ): Response {
        $user = $security->getUser();
        if (!$user) {
            throw new HttpException(401);
        } elseif ($notify->getUser() !== $user) {
            throw new HttpException(403);
        }
        $entityManager->remove($notify);
        $entityManager->flush();

        $response = new Response();
        $response->setContent('done');
        return $response;
    }
}
