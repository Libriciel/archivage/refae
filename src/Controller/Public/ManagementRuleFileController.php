<?php

namespace App\Controller\Public;

use App\Controller\DownloadFileTrait;
use App\Entity\ManagementRuleFile;
use App\Entity\VersionableEntityInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ManagementRuleFileController extends AbstractController
{
    use DownloadFileTrait;

    #[Route(
        '/public/management-rule-file/{managementRuleFile}/download',
        name: 'public_management_rule_file_download',
        methods: ['GET']
    )]
    public function download(
        ManagementRuleFile $managementRuleFile,
    ): Response {
        $managementRule = $managementRuleFile->getManagementRule();
        if (
            $managementRule->isPublic() === false
            || $managementRule->getState() !== VersionableEntityInterface::S_PUBLISHED
        ) {
            throw $this->createAccessDeniedException();
        }
        return $this->getFileDownloadResponse($managementRuleFile->getFile());
    }
}
