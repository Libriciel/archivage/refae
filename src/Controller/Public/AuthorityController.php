<?php

namespace App\Controller\Public;

use App\Entity\Authority;
use App\ResultSet\PublicAuthorityFileViewResultSet;
use App\ResultSet\PublicAuthorityResultSet;
use App\Security\Voter\IsPublicVoter;
use App\Service\ExportCsv;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class AuthorityController extends AbstractController
{
    #[Route('/public/authority/{page?1}', name: 'public_authority', requirements: ['page' => '\d+'])]
    public function index(
        PublicAuthorityResultSet $table,
    ): Response {
        return $this->render('public/authority.html.twig', [
            'table' => $table,
        ]);
    }

    #[IsGranted(IsPublicVoter::class, 'authority')]
    #[Route(
        '/public/authority/view/{authority?}',
        name: 'public_authority_view',
        methods: ['GET'],
        priority: 1,
    )]
    public function view(
        #[MapEntity(expr: 'repository.findWithPreviousVersions(authority)')]
        Authority $authority,
        PublicAuthorityFileViewResultSet $publicAuthorityFileViewResultSet,
    ): Response {
        $publicAuthorityFileViewResultSet->initialize($authority);
        return $this->render('authority/view.html.twig', [
            'authority' => $authority,
            'files' => $publicAuthorityFileViewResultSet,
        ]);
    }

    #[Route('/public/authority/csv', name: 'public_authority_csv', methods: ['GET'])]
    public function csv(
        PublicAuthorityResultSet $publicAuthorityResultSet,
        ExportCsv $exportCsv,
    ): Response {
        return $exportCsv->fromResultSet($publicAuthorityResultSet, ['csv', 'index_public']);
    }
}
