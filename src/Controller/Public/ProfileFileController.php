<?php

namespace App\Controller\Public;

use App\Controller\DownloadFileTrait;
use App\Entity\ProfileFile;
use App\Entity\VersionableEntityInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ProfileFileController extends AbstractController
{
    use DownloadFileTrait;

    #[Route(
        '/public/profile-file/{profileFile}/download',
        name: 'public_profile_file_download',
        methods: ['GET']
    )]
    public function download(
        ProfileFile $profileFile,
    ): Response {
        $profile = $profileFile->getProfile();
        if (
            $profile->isPublic() === false
            || $profile->getState() !== VersionableEntityInterface::S_PUBLISHED
        ) {
            throw $this->createAccessDeniedException();
        }
        return $this->getFileDownloadResponse($profileFile->getFile());
    }
}
