<?php

namespace App\Controller\Public;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\GoneHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Attribute\Route;

class TestErrorController extends AbstractController
{
    #[Route('/public/test-error/code/{code}', name: 'public_test_error_code')]
    public function code(int $code)
    {
        /** @noinspection UseControllerShortcuts */
        throw match ($code) {
            400 => new BadRequestHttpException(),
            401 => new UnauthorizedHttpException(''),
            403 => new AccessDeniedHttpException(),
            404 => new NotFoundHttpException(),
            405 => new MethodNotAllowedHttpException([]),
            406 => new NotAcceptableHttpException(),
            409 => new ConflictHttpException(),
            410 => new GoneHttpException(),
            503 => new ServiceUnavailableHttpException(),
            default => new HttpException($code, Response::$statusTexts[$code] ?? ''),
        };
    }

    #[Route('/public/test-error/throw', name: 'public_test_error_throw')]
    public function throwException(
        Request $request,
    ): void {
        $classname = $request->get('classname');
        $params = $request->get('params', []);
        throw new $classname(...$params);
    }
}
