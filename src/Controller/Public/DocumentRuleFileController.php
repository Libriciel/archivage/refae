<?php

namespace App\Controller\Public;

use App\Controller\DownloadFileTrait;
use App\Entity\DocumentRuleFile;
use App\Entity\VersionableEntityInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class DocumentRuleFileController extends AbstractController
{
    use DownloadFileTrait;

    #[Route(
        '/public/document-rule-file/{documentRuleFile}/download',
        name: 'public_document_rule_file_download',
        methods: ['GET']
    )]
    public function download(
        DocumentRuleFile $documentRuleFile,
    ): Response {
        $documentRule = $documentRuleFile->getDocumentRule();
        if (
            $documentRule->isPublic() === false
            || $documentRule->getState() !== VersionableEntityInterface::S_PUBLISHED
        ) {
            throw $this->createAccessDeniedException();
        }
        return $this->getFileDownloadResponse($documentRuleFile->getFile());
    }
}
