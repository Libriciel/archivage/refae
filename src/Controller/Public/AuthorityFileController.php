<?php

namespace App\Controller\Public;

use App\Controller\DownloadFileTrait;
use App\Entity\AuthorityFile;
use App\Entity\VersionableEntityInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class AuthorityFileController extends AbstractController
{
    use DownloadFileTrait;

    #[Route(
        '/public/authority-file/{authorityFile}/download',
        name: 'public_authority_file_download',
        methods: ['GET']
    )]
    public function download(
        AuthorityFile $authorityFile,
    ): Response {
        $authority = $authorityFile->getAuthority();
        if (
            $authority->isPublic() === false
            || $authority->getState() !== VersionableEntityInterface::S_PUBLISHED
        ) {
            throw $this->createAccessDeniedException();
        }
        return $this->getFileDownloadResponse($authorityFile->getFile());
    }
}
