<?php

namespace App\Controller\Public;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class LexiconController extends AbstractController
{
    #[Route(
        '/public/lexicon',
        name: 'public_lexicon',
        methods: ['GET']
    )]
    public function lexicon(): Response
    {
        return $this->render('public/lexicon.html.twig');
    }
}
