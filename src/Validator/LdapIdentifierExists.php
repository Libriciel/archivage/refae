<?php

namespace App\Validator;

use Attribute;
use Override;
use Symfony\Component\Validator\Constraint;

#[Attribute]
class LdapIdentifierExists extends Constraint
{
    public static string $errorMessage = "Cet identifiant LDAP n'existe pas.";

    #[Override]
    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }

    #[Override]
    public function validatedBy(): string
    {
        return static::class . 'Validator';
    }
}
