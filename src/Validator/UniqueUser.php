<?php

namespace App\Validator;

use Attribute;
use Override;
use Symfony\Component\Validator\Constraint;

#[Attribute]
class UniqueUser extends Constraint
{
    public static string $errorMessage
        = "Cet utilisateur existe sur un autre tenant, seul un administrateur "
        . "technique peut le rattacher à votre tenant";

    #[Override]
    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }
}
