<?php

namespace App\Validator;

use Attribute;
use Override;
use Symfony\Component\Validator\Constraint;

#[Attribute]
class PasswordComplexity extends Constraint
{
    public static string $errorMessage = "Ce mot de passe n'est pas assez complexe";

    #[Override]
    public function getTargets(): string
    {
        return self::PROPERTY_CONSTRAINT;
    }
}
