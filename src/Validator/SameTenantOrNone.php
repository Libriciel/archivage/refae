<?php

namespace App\Validator;

use Attribute;
use Override;
use Symfony\Component\Validator\Constraint;

#[Attribute]
/**
 * Vérification que tenant_id de l'entité validée est la même que celle de $target,
 * ou que $target n'ai pas de tenant_id
 */
class SameTenantOrNone extends Constraint
{
    public static string $errorMessage = 'Wrong tenant'; // FIXME plus parlant

    public function __construct(
        public readonly string $target,
        mixed $options = null,
        array $groups = null,
        mixed $payload = null
    ) {
        parent::__construct($options, $groups, $payload);
    }

    #[Override]
    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }
}
