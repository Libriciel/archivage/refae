<?php

namespace App\Validator;

use Exception;
use Override;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class SameTenantOrNoneValidator extends ConstraintValidator
{
    #[Override]
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof SameTenantOrNone) {
            throw new UnexpectedValueException($constraint, SameTenantOrNone::class);
        }

        $path = explode('\\', $constraint->target);
        $class = end($path);
        $method = 'get' . $class;
        if (!method_exists($value, $method)) {
            throw new Exception('get' . $class . ' does not exists on ' . $value::class);
        }

        $tenantFromTarget = $value->{$method}()->getTenant();
        $tenantFromEntity = $value->getTenant();

        // si la cible a un tenant, il faut que l'entité ait le même tenant
        if ($tenantFromTarget && $tenantFromTarget !== $tenantFromEntity) {
            $this->context
                ->buildViolation($constraint::$errorMessage)
                ->atPath(strtolower($class))
                ->addViolation();
        }
    }
}
