<?php

namespace App\Validator;

use App\Entity\Ldap;
use Exception;
use Override;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class LdapRespondsValidator extends ConstraintValidator
{
    #[Override]
    public function validate($value, Constraint $constraint)
    {
        if (!$value) {
            return;
        }
        if (!$value instanceof Ldap) {
            throw new Exception();
        }

        if (!$value->ping()) {
            $this->context->buildViolation(LdapResponds::$errorMessage)
                ->addViolation();
        }
    }
}
