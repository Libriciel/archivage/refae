<?php

namespace App\Validator;

use Attribute;
use Override;
use Symfony\Component\Validator\Constraint;

#[Attribute]
class LdapResponds extends Constraint
{
    public static string $errorMessage = "Le LDAP ne répond pas, veuillez en vérifier la configuration";

    #[Override]
    public function validatedBy(): string
    {
        return static::class . 'Validator';
    }
}
