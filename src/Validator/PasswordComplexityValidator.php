<?php

namespace App\Validator;

use Override;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class PasswordComplexityValidator extends ConstraintValidator
{
    #[Override]
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof PasswordComplexity) {
            throw new UnexpectedValueException($constraint, PasswordComplexity::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!is_string($value)) {
            // throw this exception if your validator cannot handle the passed type so that it can be marked as invalid
            throw new UnexpectedValueException($value, 'string');

            // separate multiple types using pipes
            // throw new UnexpectedValueException($value, 'string|int');
        }

        if (self::scorePassword($value) < ($_ENV['MIN_PASSWORD_COMPLEXITY'] ?? 78)) {
            $this->context->buildViolation($constraint::$errorMessage)
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        }
    }

    /**
     * Permet d'obtenir un score à partir d'un password
     * @see https://www.ssi.gouv.fr/administration/precautions-elementaires/calculer-la-force-dun-mot-de-passe/
     * @param string $password
     * @return float
     */
    public static function scorePassword(string $password): float
    {
        $charLists = [
            '0123456789',
            'abcdefghijklmnopqrstuvwxyz',
            'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'éèçàù',
            'âêîôûŷäëïöüÿ',
            'ÉÈÇÀÙÂÊÎÔÛŶÄËÏÖÜŸ',
            preg_quote('^$,;:!/*-+.<&"\'(_)=°>¨£%µ?./§~#{[|`\@]}€¤', '/'),
        ];
        $chars = 0;
        foreach ($charLists as $list) {
            if (preg_match("/[$list]/", $password)) {
                $chars += strlen($list);
            }
        }
        // Si un char n'est pas listé, on ajoute 20
        if (preg_match('/[^' . implode('', $charLists) . ']/', $password)) {
            $chars += 20;
        }
        return strlen($password) * log($chars, 2);
    }
}
