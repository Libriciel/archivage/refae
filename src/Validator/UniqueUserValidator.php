<?php

namespace App\Validator;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\LoggedUser;
use Exception;
use Override;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueUserValidator extends ConstraintValidator
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly LoggedUser $loggedUser,
    ) {
    }

    #[Override]
    public function validate($value, Constraint $constraint)
    {
        if (!$value instanceof User) {
            throw new Exception();
        }
        $existingUser = $this->userRepository->findOneBy([
            'username' => $value->getUsername(),
        ]);
        if (!$existingUser || !$this->loggedUser->tenant()) {
            return;
        }
        foreach ($existingUser->getTenants() as $tenant) {
            if ($tenant === $this->loggedUser->tenant()) {
                return;
            }
        }

        $this->context->buildViolation(UniqueUser::$errorMessage)
            ->addViolation();
    }
}
