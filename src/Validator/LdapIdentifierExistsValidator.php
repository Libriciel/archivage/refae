<?php

namespace App\Validator;

use App\Entity\AccessUser;
use Exception;
use Override;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class LdapIdentifierExistsValidator extends ConstraintValidator
{
    #[Override]
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof LdapIdentifierExists) {
            throw new UnexpectedValueException($constraint, LdapIdentifierExists::class);
        }

        if (!$value instanceof AccessUser) {
            throw new Exception('Wrong entity : ' . get_class($value) . ' instead of AccessUser');
        }

        if ($value->getLoginType() !== AccessUser::LOGIN_TYPE_LDAP) {
            return;
        }

        if ($value->getLdap() === null) {
            throw new Exception('No Ldap for this AccessUser');
        }

        $ldap = $value->getLdap();

        $connection = $ldap->connect();

        if (!$connection) {
            // on laisse faire App\Validator\LdapResponds
            return;
        }

        $exists = $connection->query()
            ->whereEquals($ldap->getUserNameAttribute(), $value->getLdapUsername())
            ->exists();

        if (!$exists) {
            $this->context->buildViolation(LdapIdentifierExists::$errorMessage)
                ->addViolation();
        }
    }
}
