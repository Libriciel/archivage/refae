<?php

namespace App\ApiPlatform;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use Doctrine\ORM\QueryBuilder;
use Override;

class RuleTypeFilter extends AbstractFilter
{
    public const string FILTER_NAME = 'rule_type_identifier';

    #[Override]
    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        Operation $operation = null,
        array $context = []
    ): void {
        if ($property !== self::FILTER_NAME) {
            return;
        }
        $alias = $queryBuilder->getAllAliases()[0];
        $targetField = array_keys($this->getProperties())[0];
        if (!in_array('ruleType', $queryBuilder->getAllAliases())) {
               $queryBuilder->innerJoin(sprintf('%s.%s', $alias, $targetField), 'ruleType');
        }
        $queryBuilder
            ->andWhere('ruleType.identifier = :identifier')
            ->setParameter('identifier', $value)
        ;
    }

    #[Override]
    public function getDescription(string $resourceClass): array
    {
        return [
            self::FILTER_NAME => [
                'property' => null,
                'type' => 'string',
                'required' => false,
                'openapi' => [
                    'description' => "Filtre par type",
                ],
            ]
        ];
    }
}
