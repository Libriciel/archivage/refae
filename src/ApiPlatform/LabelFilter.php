<?php

namespace App\ApiPlatform;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use Doctrine\ORM\QueryBuilder;
use Override;

class LabelFilter extends AbstractFilter
{
    public const string FILTER_NAME = 'label';

    #[Override]
    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        Operation $operation = null,
        array $context = []
    ): void {
        if ($property !== self::FILTER_NAME) {
            return;
        }
        $alias = $queryBuilder->getAllAliases()[0];
        $targetField = array_keys($this->getProperties())[0];

        $value = is_array($value) ? $value : [$value];
        $or = [];

        foreach ($value as $i => $v) {
            $and = [];
            if (str_contains((string) $v, ':')) {
                [$groupName, $labelName] = explode(':', (string) $v);
                $and[] = 'labelGroup.name = :labelGroup' . $i;
                $queryBuilder->setParameter('labelGroup' . $i, $groupName);
            } else {
                $labelName = $v;
                $and[] = 'labelGroup.id IS NULL';
            }
            $and[] = 'label.name = :labelName' . $i;
            $queryBuilder
                ->setParameter('labelName' . $i, $labelName);
            $or[] = $queryBuilder->expr()->andX(...$and);
        }

        $queryBuilder
            ->innerJoin(sprintf('%s.%s', $alias, $targetField), 'label')
            ->leftJoin('label.labelGroup', 'labelGroup')
            ->andWhere(sprintf('label.tenant = %s.tenant', $alias))
            ->andWhere($queryBuilder->expr()->orX(...$or))
        ;
    }

    #[Override]
    public function getDescription(string $resourceClass): array
    {
        return [
            self::FILTER_NAME => [
                'property' => null,
                'type' => 'string',
                'required' => false,
                'openapi' => [
                    'description' => "Filtre par label (groupe:étiquette)",
                    'allowEmptyValue' => false,
                ],
            ],
            self::FILTER_NAME . '[]' => [
                'property' => null,
                'type' => 'string',
                'required' => false,
                'is_collection' => true,
                'openapi' => [
                    'description' => "Filtre par label (groupe:étiquette)",
                    'allowEmptyValue' => false,
                ],
            ],
        ];
    }
}
