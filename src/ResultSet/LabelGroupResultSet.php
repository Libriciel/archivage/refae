<?php

namespace App\ResultSet;

use App\Entity\LabelGroup;
use App\Form\Type\FavoriteType;
use App\Form\Type\WildcardType;
use App\Repository\LabelGroupRepository;
use App\Repository\TableRepository;
use App\Router\UrlTenantRouter;
use App\Service\Permission;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Component\HttpFoundation\RequestStack;

class LabelGroupResultSet extends AbstractResultSet implements ResultSetInterface
{
    use EntityResultSetTrait;

    protected ?string $tenantUrl = null;

    public function __construct(
        LabelGroupRepository $labelGroup,
        TableRepository $table,
        RequestStack $requestStack,
        private readonly Permission $permission,
        private readonly UrlTenantRouter $router,
    ) {
        $this->repository = $labelGroup;
        parent::__construct($table, $requestStack);
    }

    protected function getDataQuery(): QueryBuilder
    {
        return $this->repository->queryForTenant($this->getTenantUrl());
    }

    public function mapResults(array $groups = ['index', 'action']): callable
    {
        return fn(LabelGroup $labelGroup) => $this->normalize($labelGroup, $groups);
    }

    #[Override]
    public function fields(): array
    {
        return $this->tableFieldsFromEntity();
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-eye" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('app_label_group_view', ['id' => '{0}']),
                'data-title' => $title = "Visualiser {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_label_group_view'),
                'params' => ['id', 'name'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('app_label_group_edit', ['id' => '{0}']),
                'data-title' => $title = "Modifier {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_label_group_edit'),
                'params' => ['id', 'name'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-action' => "Supprimer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer ce groupe d'étiquettes ?",
                'data-url' => $this->router->tableUrl('app_label_group_delete', ['id' => '{0}']),
                'data-title' => $title = "Supprimer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_label_group_delete'),
                'displayEval' => 'data[{index}].deletable',
                'params' => ['id', 'name'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [
            'name' => [
                'label' => "Nom",
                'type' => WildcardType::class,
                'query' => WildcardType::query('name'),
            ],
            'favorite' => [
                'label' => "Favoris seulement",
                'type' => FavoriteType::class,
                'query' => $this->queryFavorite(),
            ],
        ];
    }
}
