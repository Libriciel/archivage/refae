<?php

namespace App\ResultSet;

use App\Entity\AccessUser;
use App\Entity\User;
use App\Form\Type\FavoriteType;
use App\Form\Type\WildcardType;
use App\Repository\AccessUserRepository;
use App\Repository\TableRepository;
use App\Router\UrlTenantRouter;
use App\Service\LoggedUser;
use App\Service\Permission;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AccessUserResultSet extends AbstractResultSet implements EntityResultSetInterface
{
    use EntityResultSetTrait;

    /**
     * @var User
     */
    private User $user;

    public function __construct(
        TableRepository $table,
        RequestStack $requestStack,
        private readonly Permission $permission,
        private readonly AccessUserRepository $userRepository,
        private readonly UrlTenantRouter $router,
        private readonly LoggedUser $loggedUser,
    ) {
        $this->repository = $userRepository;
        parent::__construct($table, $requestStack);
    }

    #[Override]
    public function initialize(...$args): void
    {
        if (!isset($args[0]) || !$args[0] instanceof User) {
            throw new BadRequestException();
        }
        $this->user = $args[0];
    }

    private function getDataQuery(): QueryBuilder
    {
        return $this->repository->getResultsetQuery($this->appendFilters(...))
            ->select('a', 'user', 'role', 'tenant', 'openid')
            ->innerJoin('a.user', 'user')
            ->leftJoin('a.openid', 'openid')
            ->innerJoin('a.role', 'role')
            ->leftJoin('a.tenant', 'tenant')
            ->andWhere('a.user = :user')
            ->andWhere('a.tenant IS NOT NULL')
            ->setParameter('user', $this->user)
            ->orderBy('tenant.name')
        ;
    }

    public function mapResults(array $groups = ['index', 'action']): callable
    {
        return function (AccessUser $accessUser) use ($groups) {
            if (($openid = $accessUser->getOpenid()) && ($tenant = $accessUser->getTenant())) {
                $loginUrl = $this->router->generate(
                    'app_tenant_openid',
                    ['tenant_url' => $tenant->getBaseurl(), 'identifier' => $openid->getIdentifier()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );
                $openid->setLoginUrl($loginUrl);
            }
            return $this->normalize($accessUser, $groups);
        };
    }

    #[Override]
    public function fields(): array
    {
        $fields = $this->tableFieldsFromEntity();
        $fields['loginTypeName'] += ['callback' => 'RefaeTable.insertAnchor'];
        return $fields;
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer cet accès ?",
                'data-url' => $this->router->tableUrl(
                    'admin_user_access_delete',
                    [
                        'username' => '{1}',
                        'accessUser' => '{0}',
                    ]
                ),
                'data-title' => $title = "Supprimer l'accès sur {2}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_user_access_delete'),
                'params' => ['id', 'userUsername', 'tenantBaseUrl'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [
            'username' => [
                'label' => "Identifiant de connexion",
                'type' => WildcardType::class,
                'query' => WildcardType::query('username'),
            ],
            'name' => [
                'label' => "Nom",
                'type' => WildcardType::class,
                'query' => WildcardType::query('name'),
            ],
            'email' => [
                'label' => "E-mail",
                'type' => WildcardType::class,
                'query' => WildcardType::query('email'),
            ],
            'favorite' => [
                'label' => "Favoris seulement",
                'type' => FavoriteType::class,
                'query' => $this->queryFavorite(),
            ],
        ];
    }

    #[Override]
    public function limit(): int
    {
        return 10000;
    }
}
