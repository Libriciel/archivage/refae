<?php

namespace App\ResultSet;

use App\Entity\User;
use App\Form\Type\FavoriteType;
use App\Form\Type\WildcardType;
use App\Repository\TableRepository;
use App\Repository\UserRepository;
use App\Router\UrlTenantRouter;
use App\Service\LoggedUser;
use App\Service\Permission;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Component\HttpFoundation\RequestStack;

class TechnicalAdminResultSet extends AbstractResultSet implements CsvExportEntityResultSetInterface
{
    use EntityResultSetTrait;

    public function __construct(
        TableRepository $table,
        RequestStack $requestStack,
        protected readonly Permission $permission,
        protected readonly UserRepository $userRepository,
        protected readonly LoggedUser $loggedUser,
        protected readonly UrlTenantRouter $router,
    ) {
        $this->repository = $userRepository;
        parent::__construct($table, $requestStack);
    }

    protected function getDataQuery(): QueryBuilder
    {
        return $this->repository->getTechnicalAdminsQuery();
    }

    public function mapResults(): callable
    {
        return fn(User $user) => $this->normalize(
            $user->setContext('admin_tech'),
            ['index_admin', 'action']
        );
    }

    #[Override]
    public function fields(): array
    {
        $base = $this->tableFieldsFromEntity(['index_admin', 'action']);
        return [
            'username' => $base['username'],
            'name' => $base['name'],
            'email' => $base['email'],
            'accesses' => [
                'label' => "Accès",
                'callback' => 'TableHelper.array',
            ],
        ];
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-eye" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('admin_technical_admin_view', ['username' => '{0}']),
                'data-title' => $title = "Visualiser {0}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_technical_admin_view'),
                'params' => ['username'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('admin_technical_admin_edit', ['username' => '{0}']),
                'data-title' => $title = "Modifier {0}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_technical_admin_edit'),
                'params' => ['username'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-action' => "Supprimer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer l'utilisateur {1} ?",
                'data-url' => $this->router->tableUrl('admin_user_delete', ['username' => '{1}']),
                'data-title' => $title = "Supprimer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_user_delete'),
                'displayEval' => sprintf(
                    'data[{index}].deletable && data[{index}].id !== "%s"',
                    $this->loggedUser->user()->getId()
                ),
                'params' => ['id', 'username'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fas fa-arrow-down" aria-hidden="true"></i>',
                'data-action' => "Révoquer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-ajax-method' => 'DELETE',
                'data-url' => $this->router->tableUrl(
                    'admin_user_revoke_admin',
                    [
                        'username' => '{0}',
                    ]
                ),
                'data-confirm' => "Êtes-vous sûr de vouloir retirer l'accès de {0}" .
                    " à l'interface d'administration technique ?",
                'data-title' => $title = "Retirer l'accès de {0} à l'interface d'administration technique",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_user_revoke_admin'),
                'displayEval' => sprintf(
                    'data[{index}].adminAccess && data[{index}].id !== "%s"',
                    $this->loggedUser->user()->getId()
                ),
                'params' => ['username'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [
            'username' => [
                'label' => "Identifiant de connexion",
                'type' => WildcardType::class,
                'query' => WildcardType::query('username'),
            ],
            'name' => [
                'label' => "Nom",
                'type' => WildcardType::class,
                'query' => WildcardType::query('name'),
            ],
            'email' => [
                'label' => "E-mail",
                'type' => WildcardType::class,
                'query' => WildcardType::query('email'),
            ],
            'favorite' => [
                'label' => "Favoris seulement",
                'type' => FavoriteType::class,
                'query' => $this->queryFavorite(),
            ],
        ];
    }

    #[Override]
    public function getCsvFileName(): string
    {
        return 'administrateurs';
    }
}
