<?php

namespace App\ResultSet;

interface CsvExportEntityResultSetInterface extends EntityResultSetInterface
{
    /**
     * @return string
     */
    public function getCsvFileName(): string;
}
