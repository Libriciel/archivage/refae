<?php

namespace App\ResultSet;

use App\Entity\Tenant;
use App\Repository\TermRepository;
use App\Repository\ResultSetRepositoryInterface;
use Doctrine\ORM\QueryBuilder;
use Override;

class PublicTermResultSet extends TermResultSet implements EntityResultSetInterface
{
    /**
     * @var TermRepository
     */
    public ResultSetRepositoryInterface $repository;

    /**
     * @var Tenant|null
     */
    private ?Tenant $tenant = null;

    #[Override]
    public function initialize(...$args): void
    {
        parent::initialize($args);
        $this->tenant = $args[0] ?? null;
    }

    #[Override]
    protected function getDataQuery(): QueryBuilder
    {
        return $this->repository->queryPublicForVocabulary($this->vocabulary, $this->tenant);
    }

    #[Override]
    public function actions(): array
    {
        return [];
    }
}
