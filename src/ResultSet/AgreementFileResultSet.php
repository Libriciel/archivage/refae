<?php

namespace App\ResultSet;

use App\Entity\Agreement;
use App\Entity\AgreementFile;
use App\Repository\AgreementFileRepository;
use App\Repository\TableRepository;
use App\Router\UrlTenantRouter;
use App\Service\Permission;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Override;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\RequestStack;

class AgreementFileResultSet extends AbstractResultSet implements CsvExportEntityResultSetInterface
{
    use EntityResultSetTrait;

    protected ?Agreement $agreement = null;

    public function __construct(
        AgreementFileRepository $agreementFilerepository,
        TableRepository $table,
        RequestStack $requestStack,
        protected readonly Permission $permission,
        protected readonly UrlTenantRouter $router,
    ) {
        $this->repository = $agreementFilerepository;
        parent::__construct($table, $requestStack);
    }

    #[Override]
    public function initialize(...$args): void
    {
        if (!isset($args[0]) || !$args[0] instanceof Agreement) {
            throw new BadRequestException();
        }
        $this->agreement = $args[0];
    }

    protected function getDataQuery(): QueryBuilder
    {
        return $this->repository->queryByAgreement($this->agreement);
    }

    public function mapResults(array $groups = ['index', 'action']): callable
    {
        return fn(AgreementFile $agreementFile) => $this->normalize($agreementFile, $groups);
    }

    #[Override]
    public function fields(): array
    {
        return $this->tableFieldsFromEntity();
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        if ($this->agreement === null) {
            throw new Exception('Agreement should be set before rendering data');
        }

        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl(
                    'app_agreement_file_edit',
                    [
                        'tenant_url' => $this->getTenantUrl(),
                        'agreementFile' => '{0}',
                        'agreement' => $this->agreement->getId(),
                    ]
                ),
                'data-title' => $title = "Modifier {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_agreement_file_edit'),
                'displayEval' => $this->agreement->getEditable() ? 'true' : 'false',
                'params' => ['id', 'fileName'],
            ],
            $this->getDownloadAction(),
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-action' => "Supprimer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer ce fichier ?",
                'data-url' => $this->router->tableUrl(
                    'app_agreement_file_delete',
                    [
                        'tenant_url' => $this->getTenantUrl(),
                        'agreementFile' => '{0}',
                        'agreement' => $this->agreement->getId(),
                    ]
                ),
                'data-title' => $title = "Supprimer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_agreement_file_delete'),
                'displayEval' => $this->agreement->getEditable() ? 'true' : 'false',
                'params' => ['id', 'fileName'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [];
    }

    public function setAgreement(Agreement $agreement): static
    {
        $this->agreement = $agreement;
        return $this;
    }

    protected function getDownloadAction(): array
    {
        return [
            'type' => 'button',
            'class' => 'btn-link',
            'label' => '<i class="fa fa-download" aria-hidden="true"></i>',
            'data-toggle' => 'tooltip',
            'data-table' => sprintf('#%s', $this->id()),
            'href' => $this->router->tableUrl(
                'app_agreement_file_download',
                ['tenant_url' => $this->getTenantUrl(), 'agreementFile' => '{1}']
            ),
            'data-title' => $title = "Télécharger {0}",
            'title' => $title,
            'aria-label' => $title,
            'display' => $this->permission->userCanAccessRoute('app_agreement_file_download'),
            'params' => ['fileName', 'id'],
        ];
    }

    #[Override]
    public function getCsvFileName(): string
    {
        return 'accord';
    }
}
