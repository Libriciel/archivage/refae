<?php

namespace App\ResultSet;

use Exception;
use Override;

class PublicProfileFileViewResultSet extends ProfileFileViewResultSet
{
    #[Override]
    public function actions(): array
    {
        if ($this->profile === null) {
            throw new Exception('Profile should be set before rendering data');
        }

        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-download" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'href' => $this->router->tableUrl(
                    'public_profile_file_download',
                    ['profileFile' => '{1}']
                ),
                'data-title' => $title = "Télécharger {0}",
                'title' => $title,
                'aria-label' => $title,
                'params' => ['fileName', 'id'],
            ]
        ];
    }
}
