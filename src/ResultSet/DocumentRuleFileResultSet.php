<?php

namespace App\ResultSet;

use App\Entity\DocumentRuleFile;
use App\Entity\DocumentRule;
use App\Repository\DocumentRuleFileRepository;
use App\Repository\TableRepository;
use App\Router\UrlTenantRouter;
use App\Service\Permission;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Override;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\RequestStack;

class DocumentRuleFileResultSet extends AbstractResultSet implements EntityResultSetInterface
{
    use EntityResultSetTrait;

    protected ?DocumentRule $documentRule = null;

    public function __construct(
        DocumentRuleFileRepository $documentRuleFileRepository,
        TableRepository $table,
        RequestStack $requestStack,
        protected readonly Permission $permission,
        protected readonly UrlTenantRouter $router,
    ) {
        $this->repository = $documentRuleFileRepository;
        parent::__construct($table, $requestStack);
    }

    #[Override]
    public function initialize(...$args): void
    {
        if (!isset($args[0]) || !$args[0] instanceof DocumentRule) {
            throw new BadRequestException();
        }
        $this->documentRule = $args[0];
    }

    protected function getDataQuery(): QueryBuilder
    {
        return $this->repository->queryByDocumentRule($this->documentRule);
    }

    public function mapResults(array $groups = ['index', 'action']): callable
    {
        return fn(DocumentRuleFile $documentRuleFile) => $this->normalize($documentRuleFile, $groups);
    }

    #[Override]
    public function fields(): array
    {
        return $this->tableFieldsFromEntity();
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        if ($this->documentRule === null) {
            throw new Exception('DocumentRule should be set before rendering data');
        }

        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl(
                    'app_document_rule_file_edit',
                    [
                        'tenant_url' => $this->getTenantUrl(),
                        'documentRuleFile' => '{0}',
                        'documentRule' => $this->documentRule->getId(),
                    ]
                ),
                'data-title' => $title = "Modifier {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_document_rule_file_edit'),
                'displayEval' => $this->documentRule->getEditable() ? 'true' : 'false',
                'params' => ['id', 'fileName'],
            ],
            $this->getDownloadAction(),
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-action' => "Supprimer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer ce fichier ?",
                'data-url' => $this->router->tableUrl(
                    'app_document_rule_file_delete',
                    [
                        'tenant_url' => $this->getTenantUrl(),
                        'documentRuleFile' => '{0}',
                        'documentRule' => $this->documentRule->getId(),
                    ]
                ),
                'data-title' => $title = "Supprimer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_document_rule_file_delete'),
                'displayEval' => $this->documentRule->getEditable() ? 'true' : 'false',
                'params' => ['id', 'fileName'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [
           'type' => [
               'label' => "Type",
               'type' => ChoiceType::class,
               'choices' => array_flip(DocumentRuleFile::getTypeTranslations()),
               'query' => function (QueryBuilder $query, int $index, $value) {
                   $alias = $query->getAllAliases()[0];
                   $query
                       ->andWhere(sprintf('%s.type = :type%d', $alias, $index))
                       ->setParameter(sprintf('type%d', $index), $value);
               },
            ],
        ];
    }

    public function setDocumentRule(DocumentRule $documentRule): static
    {
        $this->documentRule = $documentRule;
        return $this;
    }

    protected function getDownloadAction(): array
    {
        return [
            'type' => 'button',
            'class' => 'btn-link',
            'label' => '<i class="fa fa-download" aria-hidden="true"></i>',
            'data-toggle' => 'tooltip',
            'data-table' => sprintf('#%s', $this->id()),
            'href' => $this->router->tableUrl(
                'app_document_rule_file_download',
                ['tenant_url' => $this->getTenantUrl(), 'documentRuleFile' => '{1}']
            ),
            'data-title' => $title = "Télécharger {0}",
            'title' => $title,
            'aria-label' => $title,
            'display' => $this->permission->userCanAccessRoute('app_document_rule_file_download'),
            'params' => ['fileName', 'id'],
        ];
    }
}
