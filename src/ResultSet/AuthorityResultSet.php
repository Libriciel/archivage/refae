<?php

namespace App\ResultSet;

use App\Entity\Authority;
use App\Form\Type\BooleanType;
use App\Form\Type\FavoriteType;
use App\Form\Type\FilterDateType;
use App\Form\Type\LabelsType;
use App\Form\Type\StateType;
use App\Form\Type\WildcardType;
use App\Repository\AuthorityRepository;
use App\Repository\TableRepository;
use App\Router\UrlTenantRouter;
use App\Service\Permission;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class AuthorityResultSet extends AbstractResultSet implements CsvExportEntityResultSetInterface
{
    use EntityResultSetTrait;

    protected ?string $tenantUrl = null;

    public function __construct(
        AuthorityRepository $authorityRepository,
        TableRepository $table,
        RequestStack $requestStack,
        protected readonly Permission $permission,
        protected readonly NormalizerInterface $normalizer,
        protected readonly UrlTenantRouter $router,
        protected readonly LabelsType $labelsType,
    ) {
        $this->repository = $authorityRepository;
        parent::__construct($table, $requestStack);
    }

    protected function getDataQuery(): QueryBuilder
    {
        return $this->repository->queryOnlyLastVersionForTenant($this->getTenantUrl());
    }

    public function mapResults(array $groups = ['index', 'action']): callable
    {
        return fn(Authority $authority) => $this->normalize($authority, $groups);
    }

    #[Override]
    public function fields(): array
    {
        return $this->tableFieldsFromEntity() + [
            'labels' => [
                'label' => "Étiquettes",
                'callback' => '(v, data) => RefaeLabel.callbackMultipleTableData(data.labels)',
            ],
        ];
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-eye" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('app_authority_view', ['authority' => '{0}']),
                'data-title' => $title = "Visualiser {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_authority_view'),
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('app_authority_edit', ['authority' => '{0}']),
                'data-title' => $title = "Modifier {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_authority_edit'),
                'displayEval' => 'data[{index}].editable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-globe" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-ajax-method' => 'POST',
                'data-url' => $this->router->tableUrl('app_authority_publish', ['authority' => '{0}']),
                'confirm' => "Êtes-vous sûr de vouloir publier cette notice d'autorité ?",
                'data-title' => $title = "Publier {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_authority_publish'),
                'displayEval' => 'data[{index}].publishable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-code-fork" aria-hidden="true"></i>',
                'data-action' => "Versionner",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('app_authority_new_version', ['authority' => '{0}']),
                'data-title' => $title = "Créer une nouvelle version de {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_authority_new_version'),
                'displayEval' => 'data[{index}].versionable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-times-circle-o" aria-hidden="true"></i>',
                'data-action' => "Révoquer",
                'data-toggle' => 'tooltip',
                'data-ajax-method' => 'POST',
                'data-url' => $this->router->tableUrl('app_authority_revoke', ['authority' => '{0}']),
                'confirm' => "Êtes-vous sûr de vouloir révoquer cette notice d'autorité ?",
                'data-title' => $title = "Révoquer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_authority_revoke'),
                'displayEval' => 'data[{index}].revokable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-check-circle-o" aria-hidden="true"></i>',
                'data-action' => "Restaurer",
                'data-toggle' => 'tooltip',
                'data-ajax-method' => 'POST',
                'data-url' => $this->router->tableUrl(
                    'app_authority_restore',
                    ['tenant_url' => $this->getTenantUrl(), 'authority' => '{0}']
                ),
                'confirm' => "Êtes-vous sûr de vouloir restaurer cette notice d'autorité ?",
                'data-title' => $title = "Restaurer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_authority_restore'),
                'displayEval' => 'data[{index}].recoverable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-toggle-off" aria-hidden="true"></i>',
                'data-action' => "Activer",
                'data-toggle' => 'tooltip',
                'data-ajax-method' => 'POST',
                'data-url' => $this->router->tableUrl('app_authority_activate', ['authority' => '{0}']),
                'confirm' => "Êtes-vous sûr de vouloir activer cette notice d'autorité ?",
                'data-title' => $title = "Activer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_authority_activate'),
                'displayEval' => 'data[{index}].activatable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-toggle-on" aria-hidden="true"></i>',
                'data-action' => "Désactiver",
                'data-toggle' => 'tooltip',
                'data-ajax-method' => 'POST',
                'data-url' => $this->router->tableUrl('app_authority_deactivate', ['authority' => '{0}']),
                'confirm' => "Êtes-vous sûr de vouloir désactiver cette notice d'autorité ?",
                'data-title' => $title = "Désactiver {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_authority_deactivate'),
                'displayEval' => 'data[{index}].deactivatable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'href' => $this->router->tableUrl(
                    'app_authority_file_index',
                    ['tenant_url' => $this->getTenantUrl(), 'authority' => '{0}']
                ),
                'label' => '<i class="fa fa-folder-o" aria-hidden="true"></i>',
                'data-action' => "Gérer les fichiers",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-title' => $title = "Gérer les fichiers de {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_authority_file_index'),
                'displayEval' => 'data[{index}].editable',
                'params' => ['id', 'name'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-action' => "Supprimer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer cette notice d'autorité ?",
                'data-url' => $this->router->tableUrl('app_authority_delete', ['authority' => '{0}']),
                'data-title' => $title = "Supprimer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_authority_delete'),
                'displayEval' => 'data[{index}].deletable',
                'params' => ['id', 'name'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [
            'name' => [
                'label' => "Nom",
                'type' => WildcardType::class,
                'query' => WildcardType::query('name'),
            ],
            'identifier' => [
                'label' => "Identifiant",
                'type' => WildcardType::class,
                'query' => WildcardType::query('identifier'),
            ],
            'labels' => $this->labelsType->getOptions() + [
                'label' => "Étiquettes",
                'type' => LabelsType::class,
                'query' => LabelsType::query($this->repository, 'labels'),
            ],
            'exist_dates_from' => [
                'label' => "Date de début d'existence",
                'type' => FilterDateType::class,
                'query' => FilterDateType::query('exist_dates_from'),
            ],
            'exist_dates_to' => [
                'label' => "Date de fin d'existence",
                'type' => FilterDateType::class,
                'query' => FilterDateType::query('exist_dates_to'),
            ],
            'stateTranslation' => [
                'label' => "État",
                'type' => StateType::class,
                'choices' => array_flip(Authority::$stateTranslations),
                'query' => StateType::query(),
            ],
            'public' => [
                'label' => "Publique",
                'type' => BooleanType::class,
                'query' => BooleanType::query('public'),
            ],
            'favorite' => [
                'label' => "Favoris seulement",
                'type' => FavoriteType::class,
                'query' => $this->queryFavorite(),
            ],
        ];
    }

    #[Override]
    public function getCsvFileName(): string
    {
        return 'notices';
    }
}
