<?php

namespace App\ResultSet;

use App\Entity\Label;
use App\Entity\LabelGroup;
use App\Service\ArrayToEntity;
use Override;

class LabelResultSetPreview extends LabelResultSet
{
    public string $id = 'label_import_preview';

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
        ];
    }

    #[Override]
    public function fields(): array
    {
        return [
            'name' => ['label' => "Nom"],
            'description' => ['label' => "Description"],
            'labelGroupName' => ['label' => "Groupe"],
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [];
    }

    public function setDataFromArray(array $data): void
    {
        $this->data = array_map(
            fn(array $d) => $this->mapResults()(
                ArrayToEntity::arrayToEntity($d, Label::class)
                    ->setLabelGroup(
                        $d['group'] ?? []
                            ? ArrayToEntity::arrayToEntity($d['group'], LabelGroup::class)
                            : null
                    )
            ),
            $data
        );
    }

    #[Override]
    public function filters(): array
    {
        return [];
    }
}
