<?php

namespace App\ResultSet;

use Exception;
use Override;

class AgreementFileViewResultSet extends AgreementFileResultSet
{
    #[Override]
    public function fields(): array
    {
        return array_map(
            function ($a) {
                unset($a['order']);
                return $a;
            },
            $this->tableFieldsFromEntity()
        );
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => false,
        ];
    }

    #[Override]
    public function actions(): array
    {
        if ($this->agreement === null) {
            throw new Exception('Agreement should be set before rendering data');
        }

        return [$this->getDownloadAction()];
    }

    #[Override]
    public function filters(): array
    {
        return [];
    }
}
