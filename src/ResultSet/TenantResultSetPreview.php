<?php

namespace App\ResultSet;

use App\Entity\Tenant;
use App\Service\ArrayToEntity;
use Override;

class TenantResultSetPreview extends TenantResultSet
{
    public string $id = 'tenant_import_preview';

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
        ];
    }

    #[Override]
    public function fields(): array
    {
        return [
            "baseurl",
            "name",
            "description",
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [];
    }

    public function setDataFromArray(array $data): void
    {
        $this->data = array_map(
            fn(array $d) => $this->mapResults()(ArrayToEntity::arrayToEntity($d, Tenant::class)),
            $data
        );
    }
}
