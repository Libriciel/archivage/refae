<?php

namespace App\ResultSet;

use App\Entity\User;
use App\Form\Type\FavoriteType;
use App\Form\Type\WildcardType;
use App\Repository\TableRepository;
use App\Repository\UserRepository;
use App\Router\UrlTenantRouter;
use App\Service\LoggedUser;
use App\Service\Permission;
use Override;
use Symfony\Component\HttpFoundation\RequestStack;

class AdminUserResultSet extends AbstractResultSet implements CsvExportEntityResultSetInterface
{
    use EntityResultSetTrait;

    public function __construct(
        TableRepository $table,
        RequestStack $requestStack,
        protected readonly Permission $permission,
        protected readonly UserRepository $userRepository,
        protected readonly LoggedUser $loggedUser,
        protected readonly UrlTenantRouter $router,
    ) {
        $this->repository = $userRepository;
        parent::__construct($table, $requestStack);
    }

    public function mapResults(): callable
    {
        return fn(User $user) => $this->normalize($user, ['index_admin', 'action']);
    }

    #[Override]
    public function fields(): array
    {
        $base = $this->tableFieldsFromEntity(['index_admin', 'action']);
        return [
            'username' => $base['username'],
            'name' => $base['name'],
            'email' => $base['email'],
            'createdDateTime' => $base['createdDateTime'],
            'tenantNames' => [
                'label' => "Tenants",
                'callback' => 'TableHelper.array',
            ],
            'accesses' => [
                'label' => "Accès",
                'callback' => 'TableHelper.array',
            ],
            'technicalAdmin' => $base['technicalAdmin'],
        ];
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-eye" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('admin_user_view', ['username' => '{0}']),
                'data-title' => $title = "Visualiser {0}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_user_view'),
                'params' => ['username'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('admin_user_edit', ['username' => '{0}']),
                'data-title' => $title = "Modifier {0}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_user_edit'),
                'params' => ['username'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-action' => "Supprimer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer l'utilisateur {1} ?",
                'data-url' => $this->router->tableUrl('admin_user_delete', ['username' => '{1}']),
                'data-title' => $title = "Supprimer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_user_delete'),
                'displayEval' => sprintf(
                    'data[{index}].deletable && data[{index}].id !== "%s"',
                    $this->loggedUser->user()->getId()
                ),
                'params' => ['id', 'username'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'href' => $this->router->tableUrl(
                    'admin_access_user',
                    ['username' => '{0}']
                ),
                'label' => '<i class="fas fa-key" aria-hidden="true"></i>',
                'data-action' => "Gérer les accès",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-title' => $title = "Gérer les accès de {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_access_user'),
                'params' => ['username', 'commonName'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [
            'username' => [
                'label' => "Identifiant de connexion",
                'type' => WildcardType::class,
                'query' => WildcardType::query('username'),
            ],
            'name' => [
                'label' => "Nom",
                'type' => WildcardType::class,
                'query' => WildcardType::query('name'),
            ],
            'email' => [
                'label' => "E-mail",
                'type' => WildcardType::class,
                'query' => WildcardType::query('email'),
            ],
            'favorite' => [
                'label' => "Favoris seulement",
                'type' => FavoriteType::class,
                'query' => $this->queryFavorite(),
            ],
        ];
    }

    #[Override]
    public function getCsvFileName(): string
    {
        return 'utilisateurs';
    }
}
