<?php

namespace App\ResultSet\Table;

use ArrayAccess;
use ArrayObject;
use Countable;
use IteratorAggregate;
use Override;
use Serializable;
use Stringable;

class Memory extends ArrayObject implements IteratorAggregate, ArrayAccess, Serializable, Countable, Stringable
{
    /**
     * @var string|null
     */
    public ?string $activeLine;
    /**
     * @var string|null
     */
    public ?string $view;
    /**
     * @var array
     */
    public array $favorites;
    /**
     * @var int|null
     */
    public ?int $max_per_page;
    /**
     * @var array
     */
    public array $columns;

    /**
     * Construct a new array object
     * @link https://php.net/manual/en/arrayobject.construct.php
     * @param array|object $array         The input parameter accepts an array or an Object.
     * @param int          $flags         Flags to control the behaviour of the ArrayObject object.
     * @param string       $iteratorClass Specify the class that will be used for iteration of the ArrayObject object.
     *                                    ArrayIterator is the default class used.
     */
    public function __construct($array = [], $flags = 0, $iteratorClass = "ArrayIterator")
    {
        $this->activeLine = $array['activeLine'] ?? null;
        $this->view = $array['view'] ?? null;
        $this->favorites = $array['favorites'] ?? [];
        $this->max_per_page = $array['max_per_page'] ?? ($_ENV['TABLE_PAGE_LIMIT'] ?? 50);
        $this->columns = $array['columns'] ?? [];
        parent::__construct($array, $flags, $iteratorClass);
    }

    /**
     * Rendu du champ sous la forme json_encode
     * @return string
     */
    #[Override]
    public function __toString(): string
    {
        return (string) json_encode(
            [
                'activeLine' => $this->activeLine,
                'view' => $this->view,
                'favorites' => $this->favorites,
                'max_per_page' => $this->max_per_page,
                'columns' => $this->columns,
            ]
        );
    }
}
