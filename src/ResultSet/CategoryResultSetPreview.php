<?php

namespace App\ResultSet;

use App\Entity\Category;
use App\Service\ArrayToEntity;
use Override;

class CategoryResultSetPreview extends CategoryResultSet
{
    public string $id = 'category_import_preview';

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
        ];
    }

    #[Override]
    public function fields(): array
    {
        return [
            'name' => ['label' => "Nom"],
            'description' => ['label' => "Description"],
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [];
    }

    public function setDataFromArray(array $data): void
    {
        $this->data = array_map(
            fn(array $d) => $this->mapResults()(ArrayToEntity::arrayToEntity($d, Category::class)),
            $data
        );
    }

    #[Override]
    public function filters(): array
    {
        return [];
    }
}
