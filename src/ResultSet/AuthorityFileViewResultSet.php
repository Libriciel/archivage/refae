<?php

namespace App\ResultSet;

use Exception;
use Override;

class AuthorityFileViewResultSet extends AuthorityFileResultSet
{
    #[Override]
    public function fields(): array
    {
        return array_map(
            function ($a) {
                unset($a['order']);
                return $a;
            },
            $this->tableFieldsFromEntity()
        );
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => false,
        ];
    }

    #[Override]
    public function actions(): array
    {
        if ($this->authority === null) {
            throw new Exception('Authority should be set before rendering data');
        }

        return [$this->getDownloadAction()];
    }

    #[Override]
    public function filters(): array
    {
        return [];
    }
}
