<?php

namespace App\ResultSet;

use App\Entity\Authority;
use App\Entity\AuthorityFile;
use App\Repository\AuthorityFileRepository;
use App\Repository\AuthorityRepository;
use App\Repository\TableRepository;
use App\Router\UrlTenantRouter;
use App\Service\Permission;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Override;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\RequestStack;

class AuthorityFileResultSet extends AbstractResultSet implements EntityResultSetInterface
{
    use EntityResultSetTrait;

    protected ?Authority $authority = null;

    public function __construct(
        AuthorityFileRepository $authorityFilerepository,
        TableRepository $table,
        RequestStack $requestStack,
        protected readonly AuthorityRepository $authorityRepository,
        protected readonly Permission $permission,
        protected readonly UrlTenantRouter $router,
    ) {
        $this->repository = $authorityFilerepository;
        parent::__construct($table, $requestStack);
    }

    #[Override]
    public function initialize(...$args): void
    {
        if (!isset($args[0]) || !$args[0] instanceof Authority) {
            throw new BadRequestException();
        }
        $this->authority = $args[0];
    }

    protected function getDataQuery(): QueryBuilder
    {
        return $this->repository->queryByAuthority($this->authority);
    }

    public function mapResults(array $groups = ['index', 'action']): callable
    {
        return fn(AuthorityFile $authorityFile) => $this->normalize($authorityFile, $groups);
    }

    #[Override]
    public function fields(): array
    {
        return $this->tableFieldsFromEntity();
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        if ($this->authority === null) {
            throw new Exception('Authority should be set before rendering data');
        }

        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl(
                    'app_authority_file_edit',
                    [
                        'tenant_url' => $this->getTenantUrl(),
                        'authorityFile' => '{0}',
                        'authority' => $this->authority->getId(),
                    ]
                ),
                'data-title' => $title = "Modifier {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_authority_file_edit'),
                'displayEval' => $this->authority->getEditable() ? 'true' : 'false',
                'params' => ['id', 'fileName'],
            ],
            $this->getDownloadAction(),
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-action' => "Supprimer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer ce fichier ?",
                'data-url' => $this->router->tableUrl(
                    'app_authority_file_delete',
                    [
                        'tenant_url' => $this->getTenantUrl(),
                        'authorityFile' => '{0}',
                        'authority' => $this->authority->getId(),
                    ]
                ),
                'data-title' => $title = "Supprimer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_authority_file_delete'),
                'displayEval' => $this->authority->getEditable() ? 'true' : 'false',
                'params' => ['id', 'fileName'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [
           'type' => [
               'label' => "Type",
               'type' => ChoiceType::class,
               'choices' => array_flip(AuthorityFile::getTypeTranslations()),
               'query' => function (QueryBuilder $query, int $index, $value) {
                   $alias = $query->getAllAliases()[0];
                   $query
                       ->andWhere(sprintf('%s.type = :type%d', $alias, $index))
                       ->setParameter(sprintf('type%d', $index), $value);
               },
            ],
        ];
    }

    public function setAuthority(Authority $authority): static
    {
        $this->authority = $authority;
        return $this;
    }

    protected function getDownloadAction(): array
    {
        return [
            'type' => 'button',
            'class' => 'btn-link',
            'label' => '<i class="fa fa-download" aria-hidden="true"></i>',
            'data-toggle' => 'tooltip',
            'data-table' => sprintf('#%s', $this->id()),
            'href' => $this->router->tableUrl(
                'app_authority_file_download',
                ['tenant_url' => $this->getTenantUrl(), 'authorityFile' => '{1}']
            ),
            'data-title' => $title = "Télécharger {0}",
            'title' => $title,
            'aria-label' => $title,
            'display' => $this->permission->userCanAccessRoute('app_authority_file_download'),
            'params' => ['fileName', 'id'],
        ];
    }
}
