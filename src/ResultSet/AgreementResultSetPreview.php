<?php

namespace App\ResultSet;

use App\Entity\Agreement;
use App\Service\ArrayToEntity;
use Override;

class AgreementResultSetPreview extends AgreementResultSet
{
    public string $id = 'agreement_import_preview';

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
        ];
    }

    #[Override]
    public function fields(): array
    {
        return [
            'identifier' => ['label' => "Identifiant"],
            'name' => ['label' => "Nom"],
            'description' => ['label' => "Description"],
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [];
    }

    public function setDataFromArray(array $data): void
    {
        $this->data = array_map(
            fn(array $d) => $this->mapResults()(ArrayToEntity::arrayToEntity($d, Agreement::class)),
            $data
        );
    }

    #[Override]
    public function filters(): array
    {
        return [];
    }
}
