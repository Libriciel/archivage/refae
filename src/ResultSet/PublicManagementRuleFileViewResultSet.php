<?php

namespace App\ResultSet;

use Exception;
use Override;

class PublicManagementRuleFileViewResultSet extends ManagementRuleFileViewResultSet
{
    #[Override]
    public function actions(): array
    {
        if ($this->managementRule === null) {
            throw new Exception('ManagementRule should be set before rendering data');
        }

        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-download" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'href' => $this->router->tableUrl(
                    'public_management_rule_file_download',
                    ['managementRuleFile' => '{1}']
                ),
                'data-title' => $title = "Télécharger {0}",
                'title' => $title,
                'aria-label' => $title,
                'params' => ['fileName', 'id'],
            ],
        ];
    }
}
