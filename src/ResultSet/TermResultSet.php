<?php

namespace App\ResultSet;

use App\Entity\Term;
use App\Entity\Vocabulary;
use App\Form\Type\WildcardType;
use App\Repository\TableRepository;
use App\Repository\TermRepository;
use App\Router\UrlTenantRouter;
use App\Service\Permission;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Override;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class TermResultSet extends AbstractResultSet implements EntityResultSetInterface
{
    use EntityResultSetTrait;

    protected ?Vocabulary $vocabulary = null;
    protected ?string $tenantUrl = null;

    public function __construct(
        TermRepository $repository,
        TableRepository $table,
        RequestStack $requestStack,
        private readonly Permission $permission,
        private readonly UrlTenantRouter $router,
        private readonly NormalizerInterface $normalizer,
    ) {
        $this->repository = $repository;
        parent::__construct($table, $requestStack);
    }

    protected function getDataQuery(): QueryBuilder
    {
        return $this->repository->queryTermsForVocabulary($this->vocabulary);
    }

    public function mapResults(array $groups = ['index', 'action']): callable
    {
        return fn(Term $t) => $this->normalize($t, $groups);
    }

    #[Override]
    public function fields(): array
    {
        return $this->tableFieldsFromEntity();
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        if ($this->vocabulary === null) {
            throw new Exception('Vocabulary should be set before rendering data');
        }
        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl(
                    'app_term_edit',
                    ['tenant_url' => $this->getTenantUrl(), 'term' => '{0}', 'vocabulary' => '{1}']
                ),
                'data-title' => $title = "Modifier {2}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_term_edit'),
                'displayEval' => $this->vocabulary->getEditable() ? 'true' : 'false',
                'params' => ['id', 'vocabularyId', 'name'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-action' => "Supprimer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer ce Terme ?",
                'data-url' => $this->router->tableUrl(
                    'app_term_delete',
                    ['tenant_url' => $this->getTenantUrl(), 'term' => '{0}', 'vocabulary' => '{1}']
                ),
                'data-title' => $title = "Supprimer {2}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_term_delete'),
                'displayEval' => $this->vocabulary->getEditable() ? 'true' : 'false',
                'params' => ['id', 'vocabularyId', 'name'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [
            'type' => [
                'label' => "Nom",
                'type' => WildcardType::class,
                'query' => WildcardType::query('name'),
            ],
            'identifier' => [
                'label' => "Identifiant",
                'type' => WildcardType::class,
                'query' => WildcardType::query('identifier'),
            ],
        ];
    }

    public function setVocabulary(Vocabulary $vocabulary): void
    {
        $this->vocabulary = $vocabulary;
    }
}
