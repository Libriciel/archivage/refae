<?php

namespace App\ResultSet;

use Exception;
use Override;

class PublicAuthorityFileViewResultSet extends AuthorityFileViewResultSet
{
    #[Override]
    public function actions(): array
    {
        if ($this->authority === null) {
            throw new Exception('Authority should be set before rendering data');
        }

        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-download" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'href' => $this->router->tableUrl(
                    'public_authority_file_download',
                    ['authorityFile' => '{1}']
                ),
                'data-title' => $title = "Télécharger {0}",
                'title' => $title,
                'aria-label' => $title,
                'params' => ['fileName', 'id'],
            ],
        ];
    }
}
