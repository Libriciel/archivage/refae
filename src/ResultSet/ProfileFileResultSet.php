<?php

namespace App\ResultSet;

use App\Entity\ProfileFile;
use App\Entity\Profile;
use App\Repository\ProfileFileRepository;
use App\Repository\TableRepository;
use App\Router\UrlTenantRouter;
use App\Service\Permission;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Override;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ProfileFileResultSet extends AbstractResultSet implements EntityResultSetInterface
{
    use EntityResultSetTrait;

    protected ?Profile $profile = null;

    public function __construct(
        ProfileFileRepository $repository,
        TableRepository $table,
        RequestStack $requestStack,
        protected readonly Permission $permission,
        protected readonly UrlTenantRouter $router,
        protected readonly NormalizerInterface $normalizer,
    ) {
        $this->repository = $repository;
        parent::__construct($table, $requestStack);
    }

    #[Override]
    public function initialize(...$args): void
    {
        if (!isset($args[0]) || !$args[0] instanceof Profile) {
            throw new BadRequestException();
        }
        $this->profile = $args[0];
    }

    protected function getDataQuery(): QueryBuilder
    {
        return $this->repository->queryByProfile($this->profile);
    }

    public function mapResults(array $groups = ['index', 'action']): callable
    {
        return fn(ProfileFile $fp) => $this->normalize($fp, $groups);
    }

    #[Override]
    public function fields(): array
    {
        return $this->tableFieldsFromEntity();
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        if ($this->profile === null) {
            throw new Exception('Profile should be set before rendering data');
        }

        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl(
                    'app_profile_file_edit',
                    ['tenant_url' => $this->getTenantUrl(), 'profileFile' => '{0}', 'profile' => '{1}']
                ),
                'data-title' => $title = "Modifier {2}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_profile_file_edit'),
                'displayEval' => $this->profile->getEditable() ? 'true' : 'false',
                'params' => ['id', 'profileId', 'fileName'],
            ],
            $this->getDownloadAction(),
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-action' => "Supprimer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer ce fichier ?",
                'data-url' => $this->router->tableUrl(
                    'app_profile_file_delete',
                    ['tenant_url' => $this->getTenantUrl(), 'profileFile' => '{0}', 'profile' => '{1}']
                ),
                'data-title' => $title = "Supprimer {2}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_profile_file_delete'),
                'displayEval' => $this->profile->getEditable() ? 'true' : 'false',
                'params' => ['id', 'profileId', 'fileName'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [
           'type' => [
               'label' => "Type",
               'type' => ChoiceType::class,
               'choices' => array_flip(ProfileFile::getTypeTranslations()),
               'query' => function (QueryBuilder $query, int $index, $value) {
                   $alias = $query->getAllAliases()[0];
                   $query
                       ->andWhere(sprintf('%s.type = :type%d', $alias, $index))
                       ->setParameter(sprintf('type%d', $index), $value);
               },
            ],
        ];
    }

    public function setProfile(Profile $profile): void
    {
        $this->profile = $profile;
    }

    protected function getDownloadAction(): array
    {
        return [
            'type' => 'button',
            'class' => 'btn-link',
            'label' => '<i class="fa fa-download" aria-hidden="true"></i>',
            'data-toggle' => 'tooltip',
            'data-table' => sprintf('#%s', $this->id()),
            'href' => $this->router->tableUrl(
                'app_profile_file_download',
                ['tenant_url' => $this->getTenantUrl(), 'profileFile' => '{1}']
            ),
            'data-title' => $title = "Télécharger {0}",
            'title' => $title,
            'aria-label' => $title,
            'display' => $this->permission->userCanAccessRoute('app_profile_file_download'),
            'params' => ['fileName', 'id'],
        ];
    }
}
