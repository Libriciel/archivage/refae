<?php

namespace App\ResultSet;

use Exception;
use Override;

class PublicDocumentRuleFileViewResultSet extends DocumentRuleFileResultSet
{
    #[Override]
    public function fields(): array
    {
        return array_map(
            function ($a) {
                unset($a['order']);
                return $a;
            },
            $this->tableFieldsFromEntity()
        );
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => false,
        ];
    }

    #[Override]
    public function actions(): array
    {
        if ($this->documentRule === null) {
            throw new Exception('DocumentRule should be set before rendering data');
        }

        return [
            'type' => 'button',
            'class' => 'btn-link',
            'label' => '<i class="fa fa-download" aria-hidden="true"></i>',
            'data-toggle' => 'tooltip',
            'data-table' => sprintf('#%s', $this->id()),
            'href' => $this->router->tableUrl(
                'public_document_rule_file_download',
                ['documentRuleFile' => '{1}']
            ),
            'data-title' => $title = "Télécharger {0}",
            'title' => $title,
            'aria-label' => $title,
            'display' => $this->permission->userCanAccessRoute('public_document_rule_file_download'),
            'params' => ['fileName', 'id'],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [];
    }
}
