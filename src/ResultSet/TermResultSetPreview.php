<?php

namespace App\ResultSet;

use Override;

class TermResultSetPreview extends TermResultSet
{
    public string $id = 'term_import_preview';
    /**
     * @var string
     */
    private string $type;

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
        ];
    }

    #[Override]
    public function initialize(...$args): void
    {
        $this->type = $args[0]['type'] ?? 'csv';
    }

    #[Override]
    public function fields(): array
    {
        return (array)$this->fields;
    }

    #[Override]
    public function actions(): array
    {
        return [];
    }

    public function setDataFromArray(array $data): void
    {
        $this->data = [];
        if ($this->type === 'csv') {
            $map = range('A', 'Z');
            if (isset($data[0])) {
                $this->fields = ['id' => ['label' => 'Ligne']];
                foreach (array_keys($data[0]) as $key) {
                    $this->fields[$map[$key]] = ['label' => $map[$key]];
                }
            }
            foreach ($data as $line => $row) {
                $rowData = ['id' => $line + 1];
                foreach ($row as $key => $value) {
                    $rowData[$map[$key]] = mb_substr($value, 0, 100);
                }
                $this->data[] = $rowData;
            }
        } else {
            $this->fields = ['id' => ['label' => 'Nom']];
            foreach ($data as $name) {
                $this->data[] = ['id' => $name];
            }
        }
    }

    #[Override]
    public function filters(): array
    {
        return [];
    }
}
