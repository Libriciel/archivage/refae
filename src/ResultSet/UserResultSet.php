<?php

namespace App\ResultSet;

use App\Entity\User;
use App\Form\Type\FavoriteType;
use App\Form\Type\WildcardType;
use App\Repository\TableRepository;
use App\Repository\UserRepository;
use App\Router\UrlTenantRouter;
use App\Service\LoggedUser;
use App\Service\Permission;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UserResultSet extends AbstractResultSet implements CsvExportEntityResultSetInterface
{
    use EntityResultSetTrait;

    public function __construct(
        TableRepository $table,
        RequestStack $requestStack,
        protected readonly Permission $permission,
        protected readonly UserRepository $userRepository,
        protected readonly UrlTenantRouter $router,
        protected readonly LoggedUser $loggedUser,
    ) {
        $this->repository = $userRepository;
        parent::__construct($table, $requestStack);
    }

    protected function getDataQuery(): QueryBuilder
    {
        return $this->repository->getResultsetQuery($this->appendFilters(...))
            ->innerJoin('u.tenants', 't')
            ->andWhere(':baseurl IN (t.baseurl)')
            ->setParameter('baseurl', (string)$this->loggedUser->tenant()->getBaseurl())
        ;
    }

    public function mapResults(array $groups = ['index', 'action']): callable
    {
        return function (User $user) use ($groups): array {
            $user->setActiveTenant($this->loggedUser->tenant());
            $accessUser = $user->getAccessUser();
            $openid = $accessUser?->getOpenid();
            if ($openid) {
                $loginUrl = $this->router->generate(
                    'public_openid',
                    ['identifier' => $openid->getIdentifier()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );
                $openid->setLoginUrl($loginUrl);
            }
            return $this->normalize($user, $groups);
        };
    }

    #[Override]
    public function fields(): array
    {
        $fields = $this->tableFieldsFromEntity();
        $fields['loginTypeName'] += ['callback' => 'RefaeTable.insertAnchor'];
        return $fields;
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-eye" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('app_user_view', ['username' => '{0}']),
                'data-title' => $title = "Visualiser {0}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_user_view'),
                'params' => ['username'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('app_user_edit', ['username' => '{0}']),
                'data-title' => $title = "Modifier {0}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_user_edit'),
                'params' => ['username'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-user-plus" aria-hidden="true"></i>',
                'data-action' => "Ajouter l'accès",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('app_user_add_access', ['username' => '{1}']),
                'data-title' => $title = "Ajouter l'accès pour l'utilisateur {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_user_add_access'),
                'displayEval' => 'data[{index}].addableAccess',
                'params' => ['id', 'username'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-user-minus text-danger" aria-hidden="true"></i>',
                'data-action' => "Retirer l'accès",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-update' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir retirer l'accès de l'utilisateur {1} ?",
                'data-url' => $this->router->tableUrl('app_user_delete_access', ['username' => '{1}']),
                'data-title' => $title = "Retirer l'accès utilisateur de {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_user_delete_access'),
                'displayEval' => sprintf(
                    'data[{index}].deletableAccess && data[{index}].id !== "%s"',
                    $this->loggedUser->user()->getId()
                ),
                'params' => ['id', 'username'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-user-xmark text-danger" aria-hidden="true"></i>',
                'data-action' => "Détacher du tenant",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-update' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer le lien " .
                    "entre l'utilisateur {1} et le tenant ? Seul un administrateur" .
                    " technique pourra rattacher l'utilisateur au tenant ou le supprimer.",
                'data-url' => $this->router->tableUrl('app_user_detach', ['username' => '{1}']),
                'data-title' => $title = "Détacher l'utilisateur {1} du tenant",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_user_detach'),
                'displayEval' => sprintf(
                    'data[{index}].addableAccess && ' .
                    'data[{index}].id !== "%s"',
                    $this->loggedUser->user()->getId()
                ),
                'params' => ['id', 'username'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-action' => "Supprimer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer l'utilisateur {1} ?",
                'data-url' => $this->router->tableUrl('app_user_delete', ['username' => '{1}']),
                'data-title' => $title = "Supprimer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_user_delete'),
                'displayEval' => sprintf(
                    'data[{index}].deletable && data[{index}].id !== "%s"',
                    $this->loggedUser->user()->getId()
                ),
                'params' => ['id', 'username'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [
            'username' => [
                'label' => "Identifiant de connexion",
                'type' => WildcardType::class,
                'query' => WildcardType::query('username'),
            ],
            'name' => [
                'label' => "Nom",
                'type' => WildcardType::class,
                'query' => WildcardType::query('name'),
            ],
            'email' => [
                'label' => "E-mail",
                'type' => WildcardType::class,
                'query' => WildcardType::query('email'),
            ],
            'favorite' => [
                'label' => "Favoris seulement",
                'type' => FavoriteType::class,
                'query' => $this->queryFavorite(),
            ],
        ];
    }

    #[Override]
    public function getCsvFileName(): string
    {
        return 'utilisateurs';
    }
}
