<?php

namespace App\ResultSet;

use App\Entity\Ldap;
use App\Repository\TableRepository;
use App\Router\UrlTenantRouter;
use App\Service\Permission;
use Override;
use Symfony\Component\HttpFoundation\RequestStack;

class LdapListEntriesResultSet implements ResultSetInterface
{
    use EntityResultSetTrait;

    public string $id = 'ldap_list_entries';
    protected ?int $page = null;
    protected ?array $data = null;
    private Ldap $ldap;
    private Table\Memory $memory;

    #[Override]
    public function id(): string
    {
        return $this->id;
    }

    #[Override]
    public function options(): array
    {
        return ['class' => 'table table-striped table-hover smart-td-size'];
    }

    public function __construct(
        private readonly TableRepository $table,
        private readonly Permission $permission,
        private readonly UrlTenantRouter $router,
        private readonly RequestStack $requestStack,
    ) {
        $called = static::class;
        $classname = substr($called, strrpos($called, '\\') + 1);
        $this->id = str_ends_with($classname, 'ResultSet')
            ? strtolower(substr($classname, 0, -9))
            : strtolower($classname);
    }

    #[Override]
    public function initialize(...$args): void
    {
        $request = $this->requestStack->getCurrentRequest();
        $this->ldap = current($args);
        $this->setPage((int)$request->get('page', '1'));
        $this->setData($this->ldap->paginateLdap($this->getPage(), $this->limit()));
        $this->count = $this->ldap->getCount();
        if ($this->getPage() > 1 && $this->pageCount() < $this->getPage()) {
            throw new InvalidPageException();
        }
    }

    #[Override]
    public function fields(): array
    {
        $fields = [
            'conf-login' => [
                'label' => "conf-login",
                'display' => false,
            ],
            'conf-username' => [
                'label' => "conf-username",
                'display' => false,
            ],
            'conf-name' => [
                'label' => "conf-name",
                'display' => false,
            ],
            'conf-mail' => [
                'label' => "conf-mail",
                'display' => false,
            ],
        ];
        $displayAttributes = [];
        $displayAttributes[] = $this->ldap->getUserLoginAttribute();
        $displayAttributes[] = $this->ldap->getUserUsernameAttribute();
        $displayAttributes[] = $this->ldap->getUserNameAttribute();
        $displayAttributes[] = $this->ldap->getUserMailAttribute();
        foreach (array_unique($displayAttributes) as $attr) {
            $fields[$attr] = ['label' => $attr];
        }
        return $fields;
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => $this->ldap->getUserLoginAttribute(),
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-eye" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl(
                    'admin_ldap_view_entry',
                    ['id' => $this->ldap->getId(), 'login' => '{0}']
                ),
                'data-title' => $title = "Visualiser {0}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_ldap_view_entry'),
                'params' => [$this->ldap->getUserUsernameAttribute()],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-sign-in" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl(
                    'admin_ldap_try_entry_login',
                    ['id' => $this->ldap->getId(), 'login' => '{0}']
                ),
                'data-title' => $title = "Tester la connexion avec {0}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_ldap_try_entry_login'),
                'params' => [$this->ldap->getUserUsernameAttribute()],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [];
    }

    #[Override]
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    #[Override]
    public function getPage(): int
    {
        return $this->page;
    }

    #[Override]
    public function setData(?array $data): void
    {
        $this->data = $data;
    }

    #[Override]
    public function data(): array
    {
        return $this->data;
    }

    #[Override]
    public function memory(): Table\Memory
    {
        if (empty($this->memory)) {
            $this->memory = $this->table->getMemory($this->id());
        }
        return $this->memory;
    }

    #[Override]
    public function limit(): int
    {
        return (int)($_ENV['TABLE_PAGE_LIMIT'] ?? '50');
    }

    #[Override]
    public function offset(): int
    {
        return $this->limit() * $this->getPage() - $this->limit();
    }

    #[Override]
    public function from(): int
    {
        return min($this->offset() + 1, $this->count());
    }

    #[Override]
    public function to(): int
    {
        return max(count($this->data()) + $this->offset(), $this->from());
    }

    #[Override]
    public function pageCount(): int
    {
        return ceil($this->count() / $this->limit());
    }
}
