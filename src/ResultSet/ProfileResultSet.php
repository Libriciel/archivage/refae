<?php

namespace App\ResultSet;

use App\Entity\Profile;
use App\Form\Type\BooleanType;
use App\Form\Type\FavoriteType;
use App\Form\Type\LabelsType;
use App\Form\Type\StateType;
use App\Form\Type\WildcardType;
use App\Repository\ProfileRepository;
use App\Repository\TableRepository;
use App\Router\UrlTenantRouter;
use App\Service\Permission;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ProfileResultSet extends AbstractResultSet implements CsvExportEntityResultSetInterface
{
    use EntityResultSetTrait;

    protected ?string $tenantUrl = null;

    public function __construct(
        ProfileRepository $profileRepo,
        TableRepository $table,
        RequestStack $requestStack,
        protected readonly Permission $permission,
        protected readonly NormalizerInterface $normalizer,
        protected readonly UrlTenantRouter $router,
        protected readonly LabelsType $labelsType,
    ) {
        $this->repository = $profileRepo;
        parent::__construct($table, $requestStack);
    }

    protected function getDataQuery(): QueryBuilder
    {
        return $this->repository->queryOnlyLastVersionForTenant($this->getTenantUrl());
    }

    public function mapResults(array $groups = ['index', 'action']): callable
    {
        return fn(Profile $profile) => static::normalize($profile, $groups);
    }

    #[Override]
    public function fields(): array
    {
        return $this->tableFieldsFromEntity() + [
            'labels' => [
                'label' => "Étiquettes",
                'callback' => '(v, data) => RefaeLabel.callbackMultipleTableData(data.labels)',
            ]
        ];
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-eye" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('app_profile_view', ['profile' => '{0}']),
                'data-title' => $title = "Visualiser {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_profile_view'),
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('app_profile_edit', ['profile' => '{0}']),
                'data-title' => $title = "Modifier {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_profile_edit'),
                'displayEval' => 'data[{index}].editable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-globe" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-ajax-method' => 'POST',
                'data-url' => $this->router->tableUrl('app_profile_publish', ['profile' => '{0}']),
                'confirm' => "Êtes-vous sûr de vouloir publier ce profil ?",
                'data-title' => $title = "Publier {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_profile_publish'),
                'displayEval' => 'data[{index}].publishable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-code-fork" aria-hidden="true"></i>',
                'data-action' => "Versionner",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('app_profile_new_version', ['profile' => '{0}']),
                'data-title' => $title = "Créer une nouvelle version de {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_profile_new_version'),
                'displayEval' => 'data[{index}].versionable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-times-circle-o" aria-hidden="true"></i>',
                'data-action' => "Révoquer",
                'data-toggle' => 'tooltip',
                'data-ajax-method' => 'POST',
                'data-url' => $this->router->tableUrl('app_profile_revoke', ['profile' => '{0}']),
                'confirm' => "Êtes-vous sûr de vouloir révoquer ce profil ?",
                'data-title' => $title = "Révoquer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_profile_revoke'),
                'displayEval' => 'data[{index}].revokable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-check-circle-o" aria-hidden="true"></i>',
                'data-action' => "Restaurer",
                'data-toggle' => 'tooltip',
                'data-ajax-method' => 'POST',
                'data-url' => $this->router->tableUrl('app_profile_restore', ['profile' => '{0}']),
                'confirm' => "Êtes-vous sûr de vouloir restaurer ce profil ?",
                'data-title' => $title = "Restaurer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_profile_restore'),
                'displayEval' => 'data[{index}].recoverable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-toggle-off" aria-hidden="true"></i>',
                'data-action' => "Activer",
                'data-toggle' => 'tooltip',
                'data-ajax-method' => 'POST',
                'data-url' => $this->router->tableUrl('app_profile_activate', ['profile' => '{0}']),
                'confirm' => "Êtes-vous sûr de vouloir activer ce profil ?",
                'data-title' => $title = "Activer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_profile_activate'),
                'displayEval' => 'data[{index}].activatable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-toggle-on" aria-hidden="true"></i>',
                'data-action' => "Désactiver",
                'data-toggle' => 'tooltip',
                'data-ajax-method' => 'POST',
                'data-url' => $this->router->tableUrl('app_profile_deactivate', ['profile' => '{0}']),
                'confirm' => "Êtes-vous sûr de vouloir désactiver ce profil ?",
                'data-title' => $title = "Désactiver {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_profile_deactivate'),
                'displayEval' => 'data[{index}].deactivatable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'href' => $this->router->tableUrl(
                    'app_profile_file_index',
                    ['tenant_url' => $this->getTenantUrl(), 'profile' => '{0}']
                ),
                'label' => '<i class="fa fa-folder-o" aria-hidden="true"></i>',
                'data-action' => "Gérer les fichiers",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-title' => $title = "Gérer les fichiers de {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_profile_file_index'),
                'displayEval' => 'data[{index}].editable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-action' => "Supprimer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer ce profil ?",
                'data-url' => $this->router->tableUrl('app_profile_delete', ['profile' => '{0}']),
                'data-title' => $title = "Supprimer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_profile_delete'),
                'displayEval' => 'data[{index}].deletable',
                'params' => ['id', 'identifier'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [
            'name' => [
                'label' => "Nom",
                'type' => WildcardType::class,
                'query' => WildcardType::query('name'),
            ],
            'identifier' => [
                'label' => "Identifiant",
                'type' => WildcardType::class,
                'query' => WildcardType::query('identifier'),
            ],
            'labels' => $this->labelsType->getOptions() + [
                'label' => "Étiquettes",
                'type' => LabelsType::class,
                'query' => LabelsType::query($this->repository, 'labels'),
            ],
            'stateTranslation' => [
                'label' => "État",
                'type' => StateType::class,
                'choices' => array_flip(Profile::$stateTranslations),
                'query' => StateType::query(),
            ],
            'public' => [
                'label' => "Public",
                'type' => BooleanType::class,
                'query' => BooleanType::query('public'),
            ],
            'favorite' => [
                'label' => "Favoris seulement",
                'type' => FavoriteType::class,
                'query' => $this->queryFavorite(),
            ],
        ];
    }

    #[Override]
    public function getCsvFileName(): string
    {
        return 'profils';
    }
}
