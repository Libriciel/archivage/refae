<?php

namespace App\ResultSet;

use App\Repository\TableRepository;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Component\HttpFoundation\RequestStack;

abstract class AbstractResultSet implements ResultSetInterface
{
    public string $id;
    protected ?int $page = null;
    protected ?array $data = null;
    private Table\Memory $memory;

    public function __construct(
        protected readonly TableRepository $table,
        protected readonly RequestStack $requestStack,
    ) {
        $called = static::class;
        $classname = substr($called, strrpos($called, '\\') + 1);
        $this->id = str_ends_with($classname, 'ResultSet')
            ? strtolower(substr($classname, 0, -9))
            : strtolower($classname);
    }

    #[Override]
    public function initialize(...$args): void
    {
    }

    #[Override]
    public function id(): string
    {
        return $this->id;
    }

    #[Override]
    public function options(): array
    {
        return ['class' => 'table table-striped table-hover smart-td-size'];
    }

    #[Override]
    public function memory(): Table\Memory
    {
        if (empty($this->memory)) {
            $this->memory = $this->table->getMemory($this->id());
        }
        return $this->memory;
    }

    #[Override]
    public function limit(): int
    {
        return (int)($_ENV['TABLE_PAGE_LIMIT'] ?? '50');
    }

    #[Override]
    public function offset(): int
    {
        return $this->limit() * $this->getPage() - $this->limit();
    }

    #[Override]
    public function from(): int
    {
        return min($this->offset() + 1, $this->count());
    }

    #[Override]
    public function to(): int
    {
        return max(count($this->data()) + $this->offset(), $this->from());
    }

    #[Override]
    public function pageCount(): int
    {
        return ceil($this->count() / $this->limit());
    }

    public function queryFavorite(string $alias = null)
    {
        return function (QueryBuilder $query, int $index) use ($alias) {
            if (!$alias) {
                $alias = $query->getAllAliases()[0];
            }
            $memory = $this->memory();
            if ($memory->favorites) {
                $query
                    ->andWhere(sprintf('%s.id IN (:favorites%d)', $alias, $index))
                    ->setParameter(sprintf('favorites%d', $index), $memory->favorites);
            } else {
                $query->andWhere("1 = 0");
            }
        };
    }

    #[Override]
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    #[Override]
    public function getPage(): int
    {
        if (!$this->page) {
            $this->setPage((int)$this->requestStack->getCurrentRequest()->get('page', '1'));
        }
        return $this->page;
    }

    #[Override]
    public function setData(?array $data): void
    {
        $this->data = $data;
    }

    protected function getTenantUrl(): ?string
    {
        return $this->requestStack->getCurrentRequest()->get('tenant_url');
    }
}
