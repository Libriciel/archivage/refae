<?php

namespace App\ResultSet;

use App\Entity\ManagementRuleFile;
use App\Entity\ManagementRule;
use App\Repository\ManagementRuleFileRepository;
use App\Repository\TableRepository;
use App\Router\UrlTenantRouter;
use App\Service\Permission;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Override;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\RequestStack;

class ManagementRuleFileResultSet extends AbstractResultSet implements EntityResultSetInterface
{
    use EntityResultSetTrait;

    protected ?ManagementRule $managementRule = null;

    public function __construct(
        ManagementRuleFileRepository $managementRuleFileRepository,
        TableRepository $table,
        RequestStack $requestStack,
        protected readonly Permission $permission,
        protected readonly UrlTenantRouter $router,
    ) {
        $this->repository = $managementRuleFileRepository;
        parent::__construct($table, $requestStack);
    }

    #[Override]
    public function initialize(...$args): void
    {
        if (!isset($args[0]) || !$args[0] instanceof ManagementRule) {
            throw new BadRequestException();
        }
        $this->managementRule = $args[0];
    }

    protected function getDataQuery(): QueryBuilder
    {
        return $this->repository->queryByManagementRule($this->managementRule);
    }

    public function mapResults(array $groups = ['index', 'action']): callable
    {
        return fn(ManagementRuleFile $managementRuleFile) => $this->normalize($managementRuleFile, $groups);
    }

    #[Override]
    public function fields(): array
    {
        return $this->tableFieldsFromEntity();
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        if ($this->managementRule === null) {
            throw new Exception('ManagementRule should be set before rendering data');
        }

        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl(
                    'app_management_rule_file_edit',
                    [
                        'managementRuleFile' => '{0}',
                        'managementRule' => $this->managementRule->getId(),
                    ]
                ),
                'data-title' => $title = "Modifier {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_management_rule_file_edit'),
                'displayEval' => $this->managementRule->getEditable() ? 'true' : 'false',
                'params' => ['id', 'fileName'],
            ],
            $this->getDownloadAction(),
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-action' => "Supprimer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer ce fichier ?",
                'data-url' => $this->router->tableUrl(
                    'app_management_rule_file_delete',
                    [
                        'tenant_url' => $this->getTenantUrl(),
                        'managementRuleFile' => '{0}',
                        'managementRule' => $this->managementRule->getId(),
                    ]
                ),
                'data-title' => $title = "Supprimer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_management_rule_file_delete'),
                'displayEval' => $this->managementRule->getEditable() ? 'true' : 'false',
                'params' => ['id', 'fileName'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [];
    }

    public function setManagementRule(ManagementRule $managementRule): static
    {
        $this->managementRule = $managementRule;
        return $this;
    }

    protected function getDownloadAction(): array
    {
        return [
            'type' => 'button',
            'class' => 'btn-link',
            'label' => '<i class="fa fa-download" aria-hidden="true"></i>',
            'data-toggle' => 'tooltip',
            'data-table' => sprintf('#%s', $this->id()),
            'href' => $this->router->tableUrl(
                'app_management_rule_file_download',
                ['tenant_url' => $this->getTenantUrl(), 'managementRuleFile' => '{1}']
            ),
            'data-title' => $title = "Télécharger {0}",
            'title' => $title,
            'aria-label' => $title,
            'display' => $this->permission->userCanAccessRoute('app_management_rule_file_download'),
            'params' => ['fileName', 'id'],
        ];
    }
}
