<?php

namespace App\ResultSet;

use App\Repository\TableRepository;
use App\Router\UrlTenantRouter;
use App\Service\Permission;
use App\Session\CustomSessionHandler;
use Override;
use Symfony\Component\HttpFoundation\RequestStack;

class SessionResultSet implements ResultSetInterface
{
    public string $id = 'session';
    private ?array $data = null;
    private ?int $page = null;
    private ?int $count = null;

    #[Override]
    public function id(): string
    {
        return $this->id;
    }

    #[Override]
    public function options(): array
    {
        return ['class' => 'table table-striped table-hover smart-td-size'];
    }

    public function __construct(
        private readonly TableRepository $table,
        private readonly CustomSessionHandler $customSessionHandler,
        private readonly RequestStack $requestStack,
        private readonly Permission $permission,
        private readonly UrlTenantRouter $router,
    ) {
    }

    #[Override]
    public function initialize(...$args): void
    {
    }

    #[Override]
    public function fields(): array
    {
        return [
            'sess_id' => [
                'label' => "ID",
                'display' => false,
            ],
            'sess_data' => [
                'label' => "Données",
                'callback' => 'callbackSessionData',
            ],
            'sess_lifetime' => [
                'label' => "Expire à",
                'callback' => 'TableHelper.date("H:mm:ss")',
            ],
            'sess_time' => [
                'label' => "Dernière action",
                'callback' => 'TableHelper.date("H:mm:ss")',
            ],
        ];
    }

    #[Override]
    public function data(): array
    {
        if (!$this->data) {
            $this->data = $this->customSessionHandler->findByPage($this->limit(), $this->offset());
            if ($this->getPage() > 1 && $this->pageCount() < $this->getPage()) {
                throw new InvalidPageException();
            }
        }
        return $this->data;
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'sess_id',
            'favorites' => true,
            'checkbox' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-bell" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl(
                    'admin_user_notify',
                    ['username' => '{0}']
                ),
                'data-title' => $title = "Envoyer une notification à {0}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_user_notify'),
                'params' => ['sess_data.username'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir provoquer la déconnexion de cet utilisateur ?",
                'data-url' => $this->router->tableUrl(
                    'admin_session_delete',
                    ['session' => '{0}']
                ),
                'data-title' => $title = "Supprimer",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_session_delete'),
                'params' => ['sess_id'],
            ],
        ];
    }

    #[Override]
    public function memory(): Table\Memory
    {
        return $this->table->getMemory($this->id());
    }

    #[Override]
    public function limit(): int
    {
        return $_ENV['TABLE_PAGE_LIMIT'] ?? '50';
    }

    #[Override]
    public function offset(): int
    {
        return $this->limit() * $this->getPage() - $this->limit();
    }

    #[Override]
    public function count(): int
    {
        if (!$this->count) {
            $this->count = $this->customSessionHandler->findCount();
        }
        return $this->count;
    }

    #[Override]
    public function pageCount(): int
    {
        return ceil($this->count() / $this->limit());
    }

    #[Override]
    public function from(): int
    {
        return min($this->offset() + 1, $this->count());
    }

    #[Override]
    public function to(): int
    {
        return max(count($this->data()) + $this->offset(), $this->from());
    }

    #[Override]
    public function filters(): array
    {
        return [];
    }

    public function saveTableUrl(): string
    {
        return $this->router->generate('public_save_table');
    }

    #[Override]
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    #[Override]
    public function getPage(): int
    {
        if (!$this->page) {
            $this->setPage((int)$this->requestStack->getCurrentRequest()->get('page', '1'));
        }
        return $this->page;
    }

    #[Override]
    public function setData(?array $data): void
    {
        $this->data = $data;
    }
}
