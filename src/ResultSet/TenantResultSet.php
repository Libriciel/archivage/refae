<?php

namespace App\ResultSet;

use App\Entity\Tenant;
use App\Form\Type\BooleanType;
use App\Form\Type\FavoriteType;
use App\Form\Type\FilterDateType;
use App\Form\Type\WildcardType;
use App\Repository\TableRepository;
use App\Repository\TenantRepository;
use App\Router\UrlTenantRouter;
use App\Service\Permission;
use DateTime;
use Override;
use Symfony\Component\HttpFoundation\RequestStack;

class TenantResultSet extends AbstractResultSet implements CsvExportEntityResultSetInterface
{
    use EntityResultSetTrait;

    public function __construct(
        TenantRepository $tenant,
        TableRepository $table,
        private readonly UrlTenantRouter $router,
        RequestStack $requestStack,
        private readonly Permission $permission,
    ) {
        $this->repository = $tenant;
        parent::__construct($table, $requestStack);
    }

    public function mapResults(array $groups = ['index', 'action']): callable
    {
        return fn(Tenant $t) => $this->normalize($t, $groups);
    }

    #[Override]
    public function fields(): array
    {
        return $this->tableFieldsFromEntity();
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-eye" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('admin_tenant_view', ['tenant' => '{0}']),
                'data-title' => $title = "Visualiser {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_tenant_view'),
                'params' => ['id', 'name'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('admin_tenant_edit', ['tenant' => '{0}']),
                'data-title' => $title = "Modifier {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_tenant_edit'),
                'params' => ['id', 'name'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-download" aria-hidden="true"></i>',
                'data-action' => "Exporter",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-confirm' => "Êtes-vous sûr de vouloir exporter ce " .
                    "tenant ? Cette opération peut prendre un certain temps.",
                'data-ajax-download' => sprintf(
                    '%s_export_%s.tenant.refae',
                    (new DateTime())->format('Y-m-d_His'),
                    '{2}',
                ),
                'data-url' =>  $this->router->tableUrl(
                    'admin_tenant_export',
                    [
                        'baseurl' => '{2}',
                    ]
                ),
                'data-title' => $title = "Exporter {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_tenant_export'),
                'params' => ['id', 'name', 'baseurl'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-check-circle text-success" aria-hidden="true"></i>',
                'data-action' => "Ouvrir",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-update' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir ouvrir ce tenant ?",
                'data-url' => $this->router->tableUrl('admin_tenant_open', ['tenant' => '{0}']),
                'data-title' => $title = "Ouvrir {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_tenant_open'),
                'displayEval' => 'data[{index}].openable',
                'params' => ['id', 'name'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-times-circle text-danger" aria-hidden="true"></i>',
                'data-action' => "Fermer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-update' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir fermer ce tenant ?",
                'data-url' => $this->router->tableUrl('admin_tenant_close', ['tenant' => '{0}']),
                'data-title' => $title = "Fermer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_tenant_close'),
                'displayEval' => 'data[{index}].closable',
                'params' => ['id', 'name'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-action' => "Supprimer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('admin_tenant_delete', ['tenant' => '{0}']),
                'data-title' => $title = "Supprimer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_tenant_delete'),
                'displayEval' => 'data[{index}].deletable',
                'params' => ['id', 'name'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [
            'name' => [
                'label' => "Nom",
                'type' => WildcardType::class,
                'query' => WildcardType::query('name'),
            ],
            'baseurl' => [
                'label' => "Base de l'url",
                'type' => WildcardType::class,
                'query' => WildcardType::query('baseurl'),
            ],
            'created' => [
                'label' => "Date de création",
                'type' => FilterDateType::class,
                'query' => FilterDateType::query('created'),
            ],
            'active' => [
                'label' => "Actif ?",
                'type' => BooleanType::class,
                'query' => BooleanType::query('active'),
            ],
            'favorite' => [
                'label' => "Favoris seulement",
                'type' => FavoriteType::class,
                'query' => $this->queryFavorite(),
            ],
        ];
    }

    #[Override]
    public function getCsvFileName(): string
    {
        return 'tenants';
    }
}
