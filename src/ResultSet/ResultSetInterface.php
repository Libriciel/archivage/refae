<?php

namespace App\ResultSet;

interface ResultSetInterface
{
    /**
     * Remplace le constructeur de classe
     * @param ...$args
     * @return void
     */
    public function initialize(...$args): void;

    /**
     * Identifiant DOM du tableau
     * @return string
     */
    public function id(): string;

    /**
     * Options de génération du tableau (ex: ['class' => 'table table-striped'])
     * @return array
     */
    public function options(): array;

    /**
     * Liste des champs à afficher
     * @return array
     */
    public function fields(): array;

    /**
     * Données de resultats (lignes)
     * @return array
     */
    public function data(): array;

    /**
     * Paramètres supplémentaires (ex: identifier, favorites, sortable...)
     * @return array
     */
    public function params(): array;

    /**
     * Liste des actions pour chaque lignes
     *
     * Les valeurs avec {0} ou un autre chiffre sont remplacés (voir "params")
     * Les clés seront interprétés comme attribut HTML, a l'exception de
     * certaines qui ont des fonctions spéciales:
     *
     * - label: Donne le contenu HTML du bouton
     * - params: si on a ['id', 'name'], lorsque une valeur sous la forme {0}
     *           est donné, elle est remplacé par l'id de la ligne active, {1}
     *           sera remplacé par le name de la ligne active.
     * - confirm: ajoute un confirm() javascript au click
     * - display: condition d'affichage donné par le php (lié aux permissions)
     * - displayEval: condition d'affichage évaluée en javascript, la variable
     *                "data" permet d'accéder aux données du tableau et {index}
     *                est transformé tel que data[{index}].id donne l'id de la
     *                ligne de l'action.
     * - title: attribut HTML, sera également échappé
     * - data-action: transforme l'action en action rangée dans le ...
     *
     * De plus, certains attributs spéciaux permettent d'avantage de souplesse :
     * - data-toggle: Si la valeur tooltip est donnée, le title est affiché avec
     *                une infobulle
     * - data-modal: Si la valeur ajax est donnée, contrôle l'ouverture de la
     *               modale avec une requête ajax. Il est nécessaire de
     *               renseigner les attributs data-url et data-title
     * - data-url: Donne une url lié à l'action
     * - data-title: Donne un titre à la modale
     * - data-table: donne l'identifiant du tableau de résultats, utile pour
     *               les callbacks qui mettent à jour le tableau après l'envoi
     *               du formulaire dans la modale
     * - data-update: Si la valeur ajax est donnée, utilisera data-url en method
     *                UPDATE et remplacera la ligne active par le contenu de la
     *                réponse si code retour 200
     * - data-delete: Si la valeur ajax est donnée, utilisera data-url en method
     *                DELETE et supprimera la ligne active si code retour 200
     * - data-confirm: confirm() js pour les méthodes data-ajax-download,
     *                 data-ajax-method, data-delete et data-update
     * - data-ajax-method: ex GET, POST, UPDATE... transforme l'action en simple
     *                     requête HTTP, appel ensuite data-ajax-success en cas
     *                     de succès
     *                     (valeur par défaut: TableGenericAction.afterEdit)
     * - data-ajax-success: window[{data-ajax-success}] sera appelé au success
     *                      de l'appel ajax donné par data-ajax-method
     * - data-ajax-download: Permet de déclencher un téléchargement sur data-url
     *
     *  @return array
     */
    public function actions(): array;

    /**
     * Donne la configuration du tableau de l'utilisateur
     * @return Table\Memory
     */
    public function memory(): Table\Memory;

    /**
     * Nombre de résultats par page
     * @return int
     */
    public function limit(): int;

    /**
     * Offset pour la pagination
     * @return int
     */
    public function offset(): int;

    /**
     * Nombre de résultats au total
     * @return int
     */
    public function count(): int;

    /**
     * Nombre de pages au total
     * @return int
     */
    public function pageCount(): int;

    /**
     * Pagination "from"
     * @return int
     */
    public function from(): int;

    /**
     * Pagination "to"
     * @return int
     */
    public function to(): int;

    /**
     * Liste les filtres de recherches disponibles
     * @return array
     */
    public function filters(): array;

    /**
     * Défini le numéro de page actuel
     * @param int $page
     * @return void
     */
    public function setPage(int $page): void;

    /**
     * Récupère le numéro de page actuel
     * @return void
     */
    public function getPage(): int;

    /**
     * Défini le data
     * @param array|null $data
     * @return void
     */
    public function setData(?array $data): void;
}
