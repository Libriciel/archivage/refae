<?php

namespace App\ResultSet;

use App\Entity\Openid;
use App\Form\Type\BooleanType;
use App\Form\Type\FavoriteType;
use App\Form\Type\WildcardType;
use App\Repository\OpenidRepository;
use App\Repository\TableRepository;
use App\Router\UrlTenantRouter;
use App\Service\Permission;
use Override;
use Symfony\Component\HttpFoundation\RequestStack;

class OpenidResultSet extends AbstractResultSet implements EntityResultSetInterface
{
    use EntityResultSetTrait;

    public function __construct(
        OpenidRepository $openidRepository,
        TableRepository $tableRepository,
        RequestStack $requestStack,
        private readonly Permission $permission,
        private readonly UrlTenantRouter $router,
    ) {
        $this->repository = $openidRepository;
        parent::__construct($tableRepository, $requestStack);
    }

    public function mapResults(array $groups = ['index', 'action']): callable
    {
        return fn(Openid $openid) => $this->normalize($openid, $groups);
    }

    #[Override]
    public function fields(): array
    {
        return $this->tableFieldsFromEntity();
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-eye" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('admin_openid_view', ['id' => '{0}']),
                'data-title' => $title = "Visualiser {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_openid_view'),
                'params' => ['id', 'name'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('admin_openid_edit', ['id' => '{0}']),
                'data-title' => $title = "Modifier {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_openid_edit'),
                'params' => ['id', 'name'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-action' => "Supprimer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer ce openid ?",
                'data-url' => $this->router->tableUrl('admin_openid_delete', ['id' => '{0}']),
                'data-title' => $title = "Supprimer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_openid_delete'),
                'displayEval' => 'data[{index}].deletable',
                'params' => ['id', 'name'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [
            'name' => [
                'label' => "Nom",
                'type' => WildcardType::class,
                'query' => WildcardType::query('name'),
            ],
            'url' => [
                'label' => "Url de base",
                'type' => WildcardType::class,
                'query' => WildcardType::query('url'),
            ],
            'client_id' => [
                'label' => "Id client",
                'type' => BooleanType::class,
                'query' => BooleanType::query('client_id'),
            ],
            'client_secret' => [
                'label' => "Favoris seulement",
                'type' => FavoriteType::class,
                'query' => $this->queryFavorite(),
            ],
        ];
    }
}
