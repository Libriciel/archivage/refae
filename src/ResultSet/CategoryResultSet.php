<?php

namespace App\ResultSet;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Form\Type\FavoriteType;
use App\Form\Type\WildcardType;
use App\Repository\CategoryRepository;
use App\Repository\TableRepository;
use App\Router\UrlTenantRouter;
use App\Service\Permission;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\RequestStack;

class CategoryResultSet extends AbstractResultSet implements CsvExportEntityResultSetInterface
{
    use EntityResultSetTrait;

    private ?string $tenantUrl = null;

    public function __construct(
        CategoryRepository $label,
        TableRepository $table,
        RequestStack $requestStack,
        private readonly Permission $permission,
        private readonly UrlTenantRouter $router,
        private readonly CategoryType $categoryType,
    ) {
        $this->repository = $label;
        parent::__construct($table, $requestStack);
    }

    protected function getDataQuery(): QueryBuilder
    {
        return $this->repository->queryForTenant($this->getTenantUrl());
    }

    #[Override]
    public function fields(): array
    {
        return $this->tableFieldsFromEntity();
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-eye" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('app_category_view', ['category' => '{0}']),
                'data-title' => $title = "Visualiser {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_category_view'),
                'params' => ['id', 'name'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('app_category_edit', ['entity' => '{0}']),
                'data-title' => $title = "Modifier {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_category_edit'),
                'params' => ['id', 'name'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-action' => "Supprimer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer cette catégorie ?",
                'data-url' => $this->router->tableUrl('app_category_delete', ['category' => '{0}']),
                'data-title' => $title = "Supprimer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_category_delete'),
                'displayEval' => 'data[{index}].deletable',
                'params' => ['id', 'name'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [
            'name' => [
                'label' => "Nom",
                'type' => WildcardType::class,
                'query' => WildcardType::query('name'),
            ],
            'parent_id' => [
                'label' => "Parent",
                'type' => ChoiceType::class,
                'choices' => $this->getParentChoices(),
                'query' => function (QueryBuilder $query, int $index, $value) {
                    $alias = $query->getAllAliases()[0];
                    $query
                        ->andWhere(sprintf('%s.parent = :parent', $alias))
                        ->setParameter('parent', $value);
                },
            ],
            'favorite' => [
                'label' => "Favoris seulement",
                'type' => FavoriteType::class,
                'query' => $this->queryFavorite(),
            ],
        ];
    }

    public function mapResults(array $groups = ['index', 'action']): callable
    {
        return function (Category $category) use ($groups) {
            if ($category->getId()) {
                $category->setPath($this->repository->getPathAsString($category, ['separator' => ' ⮕ ']));
            }
            return $this->normalize($category, $groups);
        };
    }

    #[Override]
    public function getCsvFileName(): string
    {
        return 'categories';
    }

    private function getParentChoices(): array
    {
        return array_reduce(
            $this->repository->findOptions($this->getTenantUrl()),
            function (array $carry, Category $item): array {
                $carry[$item->getPath()] = (string)$item->getId();
                return $carry;
            },
            []
        );
    }
}
