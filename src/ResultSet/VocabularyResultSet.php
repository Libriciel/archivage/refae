<?php

namespace App\ResultSet;

use App\Entity\VersionableEntityInterface;
use App\Entity\Vocabulary;
use App\Form\Type\BooleanType;
use App\Form\Type\FavoriteType;
use App\Form\Type\LabelsType;
use App\Form\Type\StateType;
use App\Form\Type\WildcardType;
use App\Repository\TableRepository;
use App\Repository\VocabularyRepository;
use App\Router\UrlTenantRouter;
use App\Service\Permission;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class VocabularyResultSet extends AbstractResultSet implements CsvExportEntityResultSetInterface
{
    use EntityResultSetTrait;

    protected ?string $tenantUrl = null;

    public function __construct(
        VocabularyRepository $vocabularyRepo,
        TableRepository $table,
        RequestStack $requestStack,
        protected readonly Permission $permission,
        protected readonly NormalizerInterface $normalizer,
        protected readonly UrlTenantRouter $router,
        protected readonly LabelsType $labelsType,
    ) {
        $this->repository = $vocabularyRepo;
        parent::__construct($table, $requestStack);
    }

    protected function getDataQuery(): QueryBuilder
    {
        return $this->repository->queryOnlyLastVersionForTenant($this->getTenantUrl());
    }

    public function mapResults(array $groups = ['index', 'action']): callable
    {
        return fn(Vocabulary $vocabulary) => static::normalize($vocabulary, $groups);
    }

    #[Override]
    public function fields(): array
    {
        return $this->tableFieldsFromEntity() + [
            'labels' => [
                'label' => "Étiquettes",
                'callback' => '(v, data) => RefaeLabel.callbackMultipleTableData(data.labels)',
            ]
        ];
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-eye" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl(
                    'app_vocabulary_view',
                    ['tenant_url' => $this->getTenantUrl(), 'vocabulary' => '{0}']
                ),
                'data-title' => $title = "Visualiser {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_vocabulary_view'),
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl(
                    'app_vocabulary_edit',
                    ['tenant_url' => $this->getTenantUrl(), 'vocabulary' => '{0}']
                ),
                'data-title' => $title = "Modifier {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_vocabulary_edit'),
                'displayEval' => 'data[{index}].editable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-globe" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-ajax-method' => 'POST',
                'data-url' => $this->router->tableUrl(
                    'app_vocabulary_publish',
                    ['tenant_url' => $this->getTenantUrl(), 'vocabulary' => '{0}']
                ),
                'confirm' => "Êtes-vous sûr de vouloir publier ce vocabulaire contrôlé ?",
                'data-title' => $title = "Publier {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_vocabulary_publish'),
                'displayEval' => 'data[{index}].publishable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-code-fork" aria-hidden="true"></i>',
                'data-action' => "Versionner",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl(
                    'app_vocabulary_new_version',
                    ['tenant_url' => $this->getTenantUrl(), 'vocabulary' => '{0}']
                ),
                'data-title' => $title = "Créer une nouvelle version de {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_vocabulary_new_version'),
                'displayEval' => 'data[{index}].versionable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-times-circle-o" aria-hidden="true"></i>',
                'data-action' => "Révoquer",
                'data-toggle' => 'tooltip',
                'data-ajax-method' => 'POST',
                'data-url' => $this->router->tableUrl(
                    'app_vocabulary_revoke',
                    ['tenant_url' => $this->getTenantUrl(), 'vocabulary' => '{0}']
                ),
                'confirm' => "Êtes-vous sûr de vouloir révoquer ce vocabulaire contrôlé ?",
                'data-title' => $title = "Révoquer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_vocabulary_revoke'),
                'displayEval' => 'data[{index}].revokable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-check-circle-o" aria-hidden="true"></i>',
                'data-action' => "Restaurer",
                'data-toggle' => 'tooltip',
                'data-ajax-method' => 'POST',
                'data-url' => $this->router->tableUrl(
                    'app_vocabulary_restore',
                    ['tenant_url' => $this->getTenantUrl(), 'vocabulary' => '{0}']
                ),
                'confirm' => "Êtes-vous sûr de vouloir restaurer ce vocabulaire contrôlé ?",
                'data-title' => $title = "Restaurer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_vocabulary_restore'),
                'displayEval' => 'data[{index}].recoverable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-toggle-off" aria-hidden="true"></i>',
                'data-action' => "Activer",
                'data-toggle' => 'tooltip',
                'data-ajax-method' => 'POST',
                'data-url' => $this->router->tableUrl(
                    'app_vocabulary_activate',
                    ['tenant_url' => $this->getTenantUrl(), 'vocabulary' => '{0}']
                ),
                'confirm' => "Êtes-vous sûr de vouloir activer ce vocabulaire contrôlé ?",
                'data-title' => $title = "Activer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_vocabulary_activate'),
                'displayEval' => 'data[{index}].activatable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-toggle-on" aria-hidden="true"></i>',
                'data-action' => "Désactiver",
                'data-toggle' => 'tooltip',
                'data-ajax-method' => 'POST',
                'data-url' => $this->router->tableUrl(
                    'app_vocabulary_deactivate',
                    ['tenant_url' => $this->getTenantUrl(), 'vocabulary' => '{0}']
                ),
                'confirm' => "Êtes-vous sûr de vouloir désactiver ce vocabulaire contrôlé ?",
                'data-title' => $title = "Désactiver {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_vocabulary_deactivate'),
                'displayEval' => 'data[{index}].deactivatable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'href' => $this->router->tableUrl(
                    'app_vocabulary_file_index',
                    ['tenant_url' => $this->getTenantUrl(), 'vocabulary' => '{0}']
                ),
                'label' => '<i class="fa fa-folder-o" aria-hidden="true"></i>',
                'data-action' => "Gérer les fichiers",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-title' => $title = "Gérer les fichiers de {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_vocabulary_file_index'),
                'displayEval' => 'data[{index}].editable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'href' => $this->router->tableUrl(
                    'app_term_index_edit',
                    ['tenant_url' => $this->getTenantUrl(), 'vocabulary' => '{0}']
                ),
                'label' => '<i class="fa fa-book-bookmark" aria-hidden="true"></i>',
                'data-action' => "Gérer les termes",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-title' => $title = "Gérer les Termes de {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_term_index_edit'),
                'displayEval' => 'data[{index}].editable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-action' => "Supprimer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer ce vocabulaire contrôlé ?",
                'data-url' => $this->router->tableUrl(
                    'app_vocabulary_delete',
                    ['tenant_url' => $this->getTenantUrl(), 'vocabulary' => '{0}']
                ),
                'data-title' => $title = "Supprimer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_vocabulary_delete'),
                'displayEval' => 'data[{index}].deletable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'href' => $this->router->tableUrl(
                    'app_term',
                    ['vocabulary' => '{0}']
                ),
                'label' => '<i class="fa fa-book-bookmark" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-title' => $title = "Liste des Termes de {1}",
                'title' => $title,
                'aria-label' => $title,
                'params' => ['id', 'identifier'],
                'displayEval' => sprintf('data[{index}].state === "%s"', VersionableEntityInterface::S_PUBLISHED),
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [
            'identifier' => [
                'label' => "Identifiant",
                'type' => WildcardType::class,
                'query' => WildcardType::query('identifier'),
            ],
            'name' => [
                'label' => "Nom",
                'type' => WildcardType::class,
                'query' => WildcardType::query('name'),
            ],
            'labels' => $this->labelsType->getOptions() + [
                'label' => "Étiquettes",
                'type' => LabelsType::class,
                'query' => LabelsType::query($this->repository, 'labels'),
            ],
            'stateTranslation' => [
                'label' => "État",
                'type' => StateType::class,
                'choices' => array_flip(Vocabulary::$stateTranslations),
                'query' => StateType::query(),
            ],
            'public' => [
                'label' => "Public",
                'type' => BooleanType::class,
                'query' => BooleanType::query('public'),
            ],
            'favorite' => [
                'label' => "Favoris seulement",
                'type' => FavoriteType::class,
                'query' => $this->queryFavorite(),
            ],
        ];
    }

    #[Override]
    public function getCsvFileName(): string
    {
        return 'vocabulaires';
    }
}
