<?php

namespace App\ResultSet;

use App\Repository\ProfileRepository;
use App\Repository\ResultSetRepositoryInterface;
use Doctrine\ORM\QueryBuilder;
use Override;

class ProfilePublishedResultSet extends ProfileResultSet implements ResultSetInterface
{
    public ProfileRepository|ResultSetRepositoryInterface $repository;

    #[Override]
    protected function getDataQuery(): QueryBuilder
    {
        return $this->repository->queryPublished($this->getTenantUrl());
    }

    #[Override]
    public function fields(): array
    {
        $fields = parent::fields();
        unset($fields['stateTranslation']);
        return $fields;
    }

    #[Override]
    public function actions(): array
    {
        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-eye" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('app_profile_view', ['profile' => '{0}']),
                'data-title' => $title = "Visualiser {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_profile_view'),
                'params' => ['id', 'identifier'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        $filters = parent::filters();
        unset($filters['stateTranslation']);
        return $filters;
    }
}
