<?php

namespace App\ResultSet;

use Exception;
use Override;

class DocumentRuleFileViewResultSet extends DocumentRuleFileResultSet
{
    #[Override]
    public function fields(): array
    {
        return array_map(
            function ($a) {
                unset($a['order']);
                return $a;
            },
            $this->tableFieldsFromEntity()
        );
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => false,
        ];
    }

    #[Override]
    public function actions(): array
    {
        if ($this->documentRule === null) {
            throw new Exception('DocumentRule should be set before rendering data');
        }

        return [$this->getDownloadAction()];
    }

    #[Override]
    public function filters(): array
    {
        return [];
    }
}
