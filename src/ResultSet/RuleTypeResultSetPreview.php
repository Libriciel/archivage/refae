<?php

namespace App\ResultSet;

use App\Entity\RuleType;
use App\Service\ArrayToEntity;
use Override;

class RuleTypeResultSetPreview extends RuleTypeResultSet
{
    public string $id = 'rule_type_import_preview';

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
        ];
    }

    #[Override]
    public function fields(): array
    {
        return [
            "identifier",
            "name",
            "description",
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [];
    }

    public function setDataFromArray(array $data): void
    {
        $this->data = array_map(
            fn(array $d) => $this->mapResults()(ArrayToEntity::arrayToEntity($d, RuleType::class)),
            $data
        );
    }
}
