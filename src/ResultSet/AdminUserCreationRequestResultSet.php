<?php

namespace App\ResultSet;

use App\Entity\UserCreationRequest;
use App\Form\Type\EqualType;
use App\Form\Type\FavoriteType;
use App\Form\Type\WildcardType;
use App\Repository\TableRepository;
use App\Repository\UserCreationRequestRepository;
use App\Router\UrlTenantRouter;
use App\Service\LoggedUser;
use App\Service\Permission;
use Override;
use Symfony\Component\HttpFoundation\RequestStack;

class AdminUserCreationRequestResultSet extends AbstractResultSet implements ResultSetInterface
{
    use EntityResultSetTrait;

    public function __construct(
        TableRepository $table,
        RequestStack $requestStack,
        private readonly Permission $permission,
        private readonly UserCreationRequestRepository $userCreationRequestRepository,
        private readonly LoggedUser $loggedUser,
        private readonly UrlTenantRouter $router,
    ) {
        $this->repository = $userCreationRequestRepository;
        parent::__construct($table, $requestStack);
    }

    public function mapResults(array $groups = ['index', 'action']): callable
    {
        return fn(UserCreationRequest $user) => $this->normalize($user, $groups);
    }

    #[Override]
    public function fields(): array
    {
        $base = $this->tableFieldsFromEntity();
        return [
            'username' => $base['username'],
            'name' => $base['name'],
            'email' => $base['email'],
            'ip' => $base['ip'],
            'created' => $base['created'],
            'tenantNames' => [
                'label' => "Tenants",
                'callback' => 'TableHelper.array',
            ],
        ];
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-user-pen" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('admin_user_add', ['user_creation_request' => '{0}']),
                'data-title' => $title = "Ajouter l'utilisateur {1} à partir de la demande",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_user_add'),
                'params' => ['id', 'commonName'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-user-large-slash text-danger" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl(
                    'admin_user_creation_request_deny',
                    [
                        'userCreationRequest' => '{0}',
                    ]
                ),
                'data-title' => $title = "Rédiger un email de refus pour {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_user_creation_request_deny'),
                'params' => ['id', 'commonName'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer la demande de création de l'utilisateur {1} ?",
                'data-url' => $this->router->tableUrl('admin_user_delete_creation_request', ['username' => '{1}']),
                'data-title' => $title = "Supprimer la demande de création de {2}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_user_delete_creation_request'),
                'params' => ['id', 'username', 'commonName'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [
            'id' => [
                'label' => "ID de la demande",
                'type' => EqualType::class,
                'query' => EqualType::query('id'),
            ],
            'username' => [
                'label' => "Identifiant de connexion",
                'type' => WildcardType::class,
                'query' => WildcardType::query('username'),
            ],
            'name' => [
                'label' => "Nom",
                'type' => WildcardType::class,
                'query' => WildcardType::query('name'),
            ],
            'email' => [
                'label' => "E-mail",
                'type' => WildcardType::class,
                'query' => WildcardType::query('email'),
            ],
            'favorite' => [
                'label' => "Favoris seulement",
                'type' => FavoriteType::class,
                'query' => $this->queryFavorite(),
            ],
        ];
    }
}
