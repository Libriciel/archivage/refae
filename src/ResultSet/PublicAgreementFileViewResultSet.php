<?php

namespace App\ResultSet;

use Exception;
use Override;

class PublicAgreementFileViewResultSet extends AgreementFileViewResultSet
{
    #[Override]
    public function actions(): array
    {
        if ($this->agreement === null) {
            throw new Exception('Agreement should be set before rendering data');
        }

        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-download" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'href' => $this->router->tableUrl(
                    'public_agreement_file_download',
                    ['agreementFile' => '{1}']
                ),
                'data-title' => $title = "Télécharger {0}",
                'title' => $title,
                'aria-label' => $title,
                'params' => ['fileName', 'id'],
            ],
        ];
    }
}
