<?php

namespace App\ResultSet;

use App\Entity\ManagementRule;
use App\Form\Type\BooleanType;
use App\Form\Type\FavoriteType;
use App\Form\Type\LabelsType;
use App\Form\Type\StateType;
use App\Form\Type\WildcardType;
use App\Repository\ManagementRuleRepository;
use App\Repository\RuleTypeRepository;
use App\Repository\TableRepository;
use App\Router\UrlTenantRouter;
use App\Service\Permission;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ManagementRuleResultSet extends AbstractResultSet implements CsvExportEntityResultSetInterface
{
    use EntityResultSetTrait;

    protected ?string $tenantUrl = null;

    public function __construct(
        ManagementRuleRepository $managementRuleRepository,
        TableRepository $table,
        RequestStack $requestStack,
        protected readonly Permission $permission,
        protected readonly NormalizerInterface $normalizer,
        protected readonly UrlTenantRouter $router,
        protected readonly LabelsType $labelsType,
        protected readonly RuleTypeRepository $ruleTypeRepository,
    ) {
        $this->repository = $managementRuleRepository;
        parent::__construct($table, $requestStack);
    }

    protected function getDataQuery(): QueryBuilder
    {
        return $this->repository->queryOnlyLastVersionForTenant($this->getTenantUrl());
    }

    public function mapResults(array $groups = ['index', 'action']): callable
    {
        return fn(ManagementRule $managementRule) => $this->normalize($managementRule, $groups);
    }

    #[Override]
    public function fields(): array
    {
        return $this->tableFieldsFromEntity() + [
            'labels' => [
                'label' => "Étiquettes",
                'callback' => '(v, data) => RefaeLabel.callbackMultipleTableData(data.labels)',
            ]
        ];
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-eye" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('app_management_rule_view', ['managementRule' => '{0}']),
                'data-title' => $title = "Visualiser {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_management_rule_view'),
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('app_management_rule_edit', ['managementRule' => '{0}']),
                'data-title' => $title = "Modifier {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_management_rule_edit'),
                'displayEval' => 'data[{index}].editable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-globe" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-ajax-method' => 'POST',
                'data-url' => $this->router->tableUrl('app_management_rule_publish', ['managementRule' => '{0}']),
                'confirm' => "Êtes-vous sûr de vouloir publier cette règle de gestion ?",
                'data-title' => $title = "Publier {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_management_rule_publish'),
                'displayEval' => 'data[{index}].publishable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-code-fork" aria-hidden="true"></i>',
                'data-action' => "Versionner",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('app_management_rule_new_version', ['managementRule' => '{0}']),
                'data-title' => $title = "Créer une nouvelle version de {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_management_rule_new_version'),
                'displayEval' => 'data[{index}].versionable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-times-circle-o" aria-hidden="true"></i>',
                'data-action' => "Révoquer",
                'data-toggle' => 'tooltip',
                'data-ajax-method' => 'POST',
                'data-url' => $this->router->tableUrl('app_management_rule_revoke', ['managementRule' => '{0}']),
                'confirm' => "Êtes-vous sûr de vouloir révoquer cette règle de gestion ?",
                'data-title' => $title = "Révoquer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_management_rule_revoke'),
                'displayEval' => 'data[{index}].revokable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-check-circle-o" aria-hidden="true"></i>',
                'data-action' => "Restaurer",
                'data-toggle' => 'tooltip',
                'data-ajax-method' => 'POST',
                'data-url' => $this->router->tableUrl('app_management_rule_restore', ['managementRule' => '{0}']),
                'confirm' => "Êtes-vous sûr de vouloir restaurer cette règle de gestion ?",
                'data-title' => $title = "Restaurer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_management_rule_restore'),
                'displayEval' => 'data[{index}].recoverable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-toggle-off" aria-hidden="true"></i>',
                'data-action' => "Activer",
                'data-toggle' => 'tooltip',
                'data-ajax-method' => 'POST',
                'data-url' => $this->router->tableUrl('app_management_rule_activate', ['managementRule' => '{0}']),
                'confirm' => "Êtes-vous sûr de vouloir activer cette règle de gestion ?",
                'data-title' => $title = "Activer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_management_rule_activate'),
                'displayEval' => 'data[{index}].activatable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-toggle-on" aria-hidden="true"></i>',
                'data-action' => "Désactiver",
                'data-toggle' => 'tooltip',
                'data-ajax-method' => 'POST',
                'data-url' => $this->router->tableUrl('app_management_rule_deactivate', ['managementRule' => '{0}']),
                'confirm' => "Êtes-vous sûr de vouloir désactiver cette règle de gestion ?",
                'data-title' => $title = "Désactiver {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_management_rule_deactivate'),
                'displayEval' => 'data[{index}].deactivatable',
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'href' => $this->router->tableUrl(
                    'app_management_rule_file_index',
                    ['tenant_url' => $this->getTenantUrl(), 'managementRule' => '{0}']
                ),
                'label' => '<i class="fa fa-folder-o" aria-hidden="true"></i>',
                'data-action' => "Gérer les fichiers",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-title' => $title = "Gérer les fichiers de {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_management_rule_file_index'),
                'displayEval' => 'data[{index}].editable',
                'params' => ['id', 'name'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-action' => "Supprimer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer cette règle de gestion ?",
                'data-url' => $this->router->tableUrl('app_management_rule_delete', ['managementRule' => '{0}']),
                'data-title' => $title = "Supprimer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_management_rule_delete'),
                'displayEval' => 'data[{index}].deletable',
                'params' => ['id', 'name'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [
            'name' => [
                'label' => "Nom",
                'type' => WildcardType::class,
                'query' => WildcardType::query('name'),
            ],
            'identifier' => [
                'label' => "Identifiant",
                'type' => WildcardType::class,
                'query' => WildcardType::query('identifier'),
            ],
            'labels' => $this->labelsType->getOptions() + [
                'label' => "Étiquettes",
                'type' => LabelsType::class,
                'query' => LabelsType::query($this->repository, 'labels'),
            ],
            'ruleTypeName' =>  [
                'label' => "Type de règle",
                'choice_value' => 'identifier',
                'choice_label' => 'name',
                'type' => ChoiceType::class,
                'placeholder' => "-- Choisir un type --",
                'required' => true,
                'attr' => [
                    'data-s2' => true,
                ],
                'query' => function (QueryBuilder $query, int $index, $value) {
                    $query
                        ->andWhere(sprintf('ruleType.identifier = :ruleType%d', $index))
                        ->setParameter(sprintf('ruleType%d', $index), $value);
                },
                'choices' => $this->ruleTypeRepository->queryOptions($this->getTenantUrl())
                    ->getQuery()
                    ->getResult()
            ],
            'stateTranslation' => [
                'label' => "État",
                'type' => StateType::class,
                'choices' => array_flip(ManagementRule::$stateTranslations),
                'query' => StateType::query(),
            ],
            'public' => [
                'label' => "Publique",
                'type' => BooleanType::class,
                'query' => BooleanType::query('public'),
            ],
            'favorite' => [
                'label' => "Favoris seulement",
                'type' => FavoriteType::class,
                'query' => $this->queryFavorite(),
            ],
        ];
    }

    #[Override]
    public function getCsvFileName(): string
    {
        return 'regles_gestion';
    }
}
