<?php

namespace App\ResultSet;

use App\Entity\Vocabulary;
use App\Service\ArrayToEntity;
use Override;

class VocabularyResultSetPreview extends VocabularyResultSet
{
    public string $id = 'vocabulary_import_preview';

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
        ];
    }

    #[Override]
    public function fields(): array
    {
        return [
            "Identifiant",
            "Nom",
            "Description",
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [];
    }

    public function setDataFromArray(array $data): void
    {
        $this->data = array_map(
            fn(array $d) => $this->mapResults()(ArrayToEntity::arrayToEntity($d, Vocabulary::class)),
            $data
        );
    }
}
