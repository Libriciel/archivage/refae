<?php

namespace App\ResultSet;

use App\Entity\Ldap;
use App\Form\Type\BooleanType;
use App\Form\Type\FavoriteType;
use App\Form\Type\FilterDateType;
use App\Form\Type\WildcardType;
use App\Repository\LdapRepository;
use App\Repository\TableRepository;
use App\Router\UrlTenantRouter;
use App\Service\Permission;
use Override;
use Symfony\Component\HttpFoundation\RequestStack;

class LdapResultSet extends AbstractResultSet implements EntityResultSetInterface
{
    use EntityResultSetTrait;

    public function __construct(
        LdapRepository $ldap,
        TableRepository $table,
        RequestStack $requestStack,
        private readonly Permission $permission,
        private readonly UrlTenantRouter $router,
    ) {
        $this->repository = $ldap;
        parent::__construct($table, $requestStack);
    }

    #[Override]
    public function fields(): array
    {
        return $this->tableFieldsFromEntity();
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-eye" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('admin_ldap_view', ['id' => '{0}']),
                'data-title' => $title = "Visualiser {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_ldap_view'),
                'params' => ['id', 'name'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('admin_ldap_edit', ['ldap' => '{0}']),
                'data-title' => $title = "Modifier {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_ldap_edit'),
                'params' => ['id', 'name'],
            ],
            [
                'href' => $this->router->tableUrl('admin_ldap_list_entries', ['id' => '{0}']),
                'label' => '<i class="fa fa-list" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-title' => $title = "Lister les entrées de {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_ldap_list_entries'),
                'displayEval' => 'data[{index}].ping === true',
                'params' => ['id', 'name'],
            ],

            [
                'type' => 'button',
                'class' => 'btn-link',
                'disabled' => true,
                'href' => '#',
                'label' => '<i class="fa fa-list" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-title' => $title = "Le Ldap {1} ne répond pas, veuillez vérifier la configuration",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_ldap_list_entries'),
                'displayEval' => 'data[{index}].ping === false',
                'params' => ['id', 'name'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-action' => "Supprimer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer ce ldap ?",
                'data-url' => $this->router->tableUrl('admin_ldap_delete', ['id' => '{0}']),
                'data-title' => $title = "Supprimer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('admin_ldap_delete'),
                'displayEval' => 'data[{index}].deletable',
                'params' => ['id', 'name'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [
            'name' => [
                'label' => "Nom",
                'type' => WildcardType::class,
                'query' => WildcardType::query('name'),
            ],
            'baseurl' => [
                'label' => "Base de l'url",
                'type' => WildcardType::class,
                'query' => WildcardType::query('baseurl'),
            ],
            'created' => [
                'label' => "Date de création",
                'type' => FilterDateType::class,
                'query' => FilterDateType::query('created'),
            ],
            'active' => [
                'label' => "Actif ?",
                'type' => BooleanType::class,
                'query' => BooleanType::query('active'),
            ],
            'favorite' => [
                'label' => "Favoris seulement",
                'type' => FavoriteType::class,
                'query' => $this->queryFavorite(),
            ],
        ];
    }

    public function mapResults(array $groups = ['index', 'action']): callable
    {
        return fn(Ldap $ldap) => $this->normalize($ldap, $groups);
    }
}
