<?php

namespace App\ResultSet;

interface EntityResultSetInterface extends ResultSetInterface
{
    /**
     * @param array $groups
     * @param bool  $useBlacklist
     * @return array
     */
    public function tableFieldsFromEntity(array $groups = ['index', 'action'], bool $useBlacklist = true): array;
}
