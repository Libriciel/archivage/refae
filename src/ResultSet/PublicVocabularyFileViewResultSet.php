<?php

namespace App\ResultSet;

use Exception;
use Override;

class PublicVocabularyFileViewResultSet extends VocabularyFileViewResultSet
{
    #[Override]
    public function actions(): array
    {
        if ($this->vocabulary === null) {
            throw new Exception('Vocabulary should be set before rendering data');
        }

        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-download" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'href' => $this->router->tableUrl(
                    'public_vocabulary_file_download',
                    ['vocabularyFile' => '{1}']
                ),
                'data-title' => $title = "Télécharger {0}",
                'title' => $title,
                'aria-label' => $title,
                'params' => ['fileName', 'id'],
            ],
        ];
    }
}
