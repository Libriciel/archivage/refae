<?php

namespace App\ResultSet;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class InvalidPageException extends NotFoundHttpException
{
}
