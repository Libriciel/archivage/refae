<?php

namespace App\ResultSet;

use App\Entity\Category;
use App\Entity\Tenant;
use App\Repository\DocumentRuleRepository;
use App\Repository\ResultSetRepositoryInterface;
use Doctrine\ORM\QueryBuilder;
use Override;

class PublicDocumentRuleResultSet extends DocumentRuleResultSet implements EntityResultSetInterface
{
    /**
     * @var DocumentRuleRepository
     */
    public ResultSetRepositoryInterface $repository;

    /**
     * @var Category[]
     */
    private array $categories;
    /**
     * @var Tenant|null
     */
    private ?Tenant $tenant = null;

    #[Override]
    public function initialize(...$args): void
    {
        parent::initialize($args);
        $this->categories = $args[0];
        $this->tenant = $args[1] ?? null;
    }

    #[Override]
    protected function getDataQuery(): QueryBuilder
    {
        return $this->repository->queryCategoriesPublic($this->categories, $this->tenant);
    }

    public function mapResults(array $groups = ['index', 'action']): callable
    {
        $groups = $this->tenant
            ? $groups
            : ['index', 'action', 'index_public'];

        return parent::mapResults($groups);
    }

    #[Override]
    public function fields(): array
    {
        $groups = $this->tenant
            ? ['index', 'action']
            : ['index', 'action', 'index_public'];

        $fields = $this->tableFieldsFromEntity($groups) + [
            'labels' => [
                'label' => "Étiquettes",
                'callback' => '(v, data) => RefaeLabel.callbackMultipleTableData(data.labels)',
            ]
        ];
        unset($fields['stateTranslation']);
        unset($fields['public']);

        if (!$this->tenant) {
            $fields['tenantName'] = [
                'label' => "Tenant",
                'display' => false,
            ];
            $fields['identifier']['display'] = false;
            $fields['version']['display'] = false;
            $fields['labels']['display'] = false;
        }

        return $fields;
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-eye" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('public_document_rule_view', ['documentRule' => '{0}']),
                'data-title' => $title = "Visualiser {1}",
                'title' => $title,
                'aria-label' => $title,
                'params' => ['id', 'identifier'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        $filters = parent::filters();
        unset($filters['stateTranslation']);
        if (empty($this->tenant)) {
            unset($filters['public']);
        }
        return $filters;
    }
}
