<?php

namespace App\ResultSet;

use App\Entity\Label;
use App\Form\Type\FavoriteType;
use App\Form\Type\WildcardType;
use App\Repository\LabelRepository;
use App\Repository\TableRepository;
use App\Router\UrlTenantRouter;
use App\Service\Permission;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Component\HttpFoundation\RequestStack;

class LabelResultSet extends AbstractResultSet implements CsvExportEntityResultSetInterface
{
    use EntityResultSetTrait;

    protected ?string $tenantUrl = null;

    public function __construct(
        LabelRepository $label,
        TableRepository $table,
        RequestStack $requestStack,
        private readonly Permission $permission,
        private readonly UrlTenantRouter $router,
    ) {
        $this->repository = $label;
        parent::__construct($table, $requestStack);
    }

    protected function getDataQuery(): QueryBuilder
    {
        return $this->repository->queryForTenant($this->getTenantUrl());
    }

    public function mapResults(array $groups = ['index', 'action']): callable
    {
        return fn(Label $label) => $this->normalize($label, $groups);
    }

    #[Override]
    public function fields(): array
    {
        $fields = $this->tableFieldsFromEntity();

        $fields['color'] = [
            'label' => "Couleur",
            'callback' => 'RefaeLabel.colorDiv',
            'display' => false,
        ];
        $fields['label'] = [
            'label' => "Étiquette",
            'callback' => 'RefaeLabel.callbackTableData',
        ];

        return $fields;
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-eye" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('app_label_view', ['id' => '{0}']),
                'data-title' => $title = "Visualiser {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_label_view'),
                'params' => ['id', 'name'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl('app_label_edit', ['label' => '{0}']),
                'data-title' => $title = "Modifier {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_label_edit'),
                'params' => ['id', 'name'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-action' => "Supprimer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer cette étiquette ?",
                'data-url' => $this->router->tableUrl('app_label_delete', ['id' => '{0}']),
                'data-title' => $title = "Supprimer {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_label_delete'),
                'displayEval' => 'data[{index}].deletable',
                'params' => ['id', 'name'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [
            'name' => [
                'label' => "Nom",
                'type' => WildcardType::class,
                'query' => WildcardType::query('name'),
            ],
            'favorite' => [
                'label' => "Favoris seulement",
                'type' => FavoriteType::class,
                'query' => $this->queryFavorite(),
            ],
        ];
    }

    #[Override]
    public function getCsvFileName(): string
    {
        return 'etiquettes';
    }
}
