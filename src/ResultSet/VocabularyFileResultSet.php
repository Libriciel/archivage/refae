<?php

namespace App\ResultSet;

use App\Entity\VocabularyFile;
use App\Entity\Vocabulary;
use App\Repository\VocabularyFileRepository;
use App\Repository\TableRepository;
use App\Router\UrlTenantRouter;
use App\Service\Permission;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Override;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class VocabularyFileResultSet extends AbstractResultSet implements EntityResultSetInterface
{
    use EntityResultSetTrait;

    protected ?Vocabulary $vocabulary = null;

    public function __construct(
        VocabularyFileRepository $repository,
        TableRepository $table,
        RequestStack $requestStack,
        protected readonly Permission $permission,
        protected readonly UrlTenantRouter $router,
        protected readonly NormalizerInterface $normalizer,
    ) {
        $this->repository = $repository;
        parent::__construct($table, $requestStack);
    }

    #[Override]
    public function initialize(...$args): void
    {
        if (!isset($args[0]) || !$args[0] instanceof Vocabulary) {
            throw new BadRequestException();
        }
        $this->vocabulary = $args[0];
    }

    protected function getDataQuery(): QueryBuilder
    {
        return $this->repository->queryByVocabulary($this->vocabulary);
    }

    public function mapResults(array $groups = ['index', 'action']): callable
    {
        return fn(VocabularyFile $fv) => $this->normalize($fv, $groups);
    }

    #[Override]
    public function fields(): array
    {
        return $this->tableFieldsFromEntity();
    }

    #[Override]
    public function params(): array
    {
        return [
            'identifier' => 'id',
            'favorites' => true,
        ];
    }

    #[Override]
    public function actions(): array
    {
        if ($this->vocabulary === null) {
            throw new Exception('Vocabulary should be set before rendering data');
        }

        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl(
                    'app_vocabulary_file_edit',
                    ['tenant_url' => $this->getTenantUrl(), 'vocabularyFile' => '{0}', 'vocabulary' => '{1}']
                ),
                'data-title' => $title = "Modifier {2}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_vocabulary_file_edit'),
                'displayEval' => $this->vocabulary->getEditable() ? 'true' : 'false',
                'params' => ['id', 'vocabularyId', 'fileName'],
            ],
            $this->getDownloadAction(),
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
                'data-action' => "Supprimer",
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-delete' => 'ajax',
                'data-confirm' => "Êtes-vous sûr de vouloir supprimer ce fichier ?",
                'data-url' => $this->router->tableUrl(
                    'app_vocabulary_file_delete',
                    ['tenant_url' => $this->getTenantUrl(), 'vocabularyFile' => '{0}', 'vocabulary' => '{1}']
                ),
                'data-title' => $title = "Supprimer {2}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_vocabulary_file_delete'),
                'displayEval' => $this->vocabulary->getEditable() ? 'true' : 'false',
                'params' => ['id', 'vocabularyId', 'fileName'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        return [];
    }

    public function setVocabulary(Vocabulary $vocabulary): void
    {
        $this->vocabulary = $vocabulary;
    }

    protected function getDownloadAction(): array
    {
        return [
            'type' => 'button',
            'class' => 'btn-link',
            'label' => '<i class="fa fa-download" aria-hidden="true"></i>',
            'data-toggle' => 'tooltip',
            'data-table' => sprintf('#%s', $this->id()),
            'href' => $this->router->tableUrl(
                'app_vocabulary_file_download',
                ['tenant_url' => $this->getTenantUrl(), 'vocabularyFile' => '{1}']
            ),
            'data-title' => $title = "Télécharger {0}",
            'title' => $title,
            'aria-label' => $title,
            'display' => $this->permission->userCanAccessRoute('app_vocabulary_file_download'),
            'params' => ['fileName', 'id'],
        ];
    }
}
