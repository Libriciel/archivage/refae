<?php

namespace App\ResultSet;

use App\Repository\ResultSetRepositoryInterface;
use App\Repository\VocabularyRepository;
use Doctrine\ORM\QueryBuilder;
use Override;

class VocabularyPublishedResultSet extends VocabularyResultSet implements ResultSetInterface
{
    public VocabularyRepository|ResultSetRepositoryInterface $repository;

    #[Override]
    protected function getDataQuery(): QueryBuilder
    {
        return $this->repository->queryPublished($this->getTenantUrl());
    }

    #[Override]
    public function fields(): array
    {
        $fields = parent::fields();
        unset($fields['stateTranslation']);
        return $fields;
    }

    #[Override]
    public function actions(): array
    {
        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-eye" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-modal' => 'ajax',
                'data-url' => $this->router->tableUrl(
                    'app_vocabulary_view',
                    ['tenant_url' => $this->getTenantUrl(), 'vocabulary' => '{0}']
                ),
                'data-title' => $title = "Visualiser {1}",
                'title' => $title,
                'aria-label' => $title,
                'display' => $this->permission->userCanAccessRoute('app_vocabulary_view'),
                'params' => ['id', 'identifier'],
            ],
            [
                'type' => 'button',
                'class' => 'btn-link',
                'href' => $this->router->tableUrl(
                    'app_term',
                    ['vocabulary' => '{0}']
                ),
                'label' => '<i class="fa fa-book-bookmark" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'data-title' => $title = "Liste des Termes de {1}",
                'title' => $title,
                'aria-label' => $title,
                'params' => ['id', 'identifier'],
            ],
        ];
    }

    #[Override]
    public function filters(): array
    {
        $filters = parent::filters();
        unset($filters['stateTranslation']);
        return $filters;
    }
}
