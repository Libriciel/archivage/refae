<?php

namespace App\ResultSet;

use Exception;
use Override;

class PublicServiceLevelFileViewResultSet extends ServiceLevelFileViewResultSet
{
    #[Override]
    public function actions(): array
    {
        if ($this->serviceLevel === null) {
            throw new Exception('ServiceLevel should be set before rendering data');
        }

        return [
            [
                'type' => 'button',
                'class' => 'btn-link',
                'label' => '<i class="fa fa-download" aria-hidden="true"></i>',
                'data-toggle' => 'tooltip',
                'data-table' => sprintf('#%s', $this->id()),
                'href' => $this->router->tableUrl(
                    'public_service_level_file_download',
                    ['serviceLevelFile' => '{1}']
                ),
                'data-title' => $title = "Télécharger {0}",
                'title' => $title,
                'aria-label' => $title,
                'params' => ['fileName', 'id'],
            ],
        ];
    }
}
