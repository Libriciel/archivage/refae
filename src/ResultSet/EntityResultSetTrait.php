<?php

namespace App\ResultSet;

use App\Entity\Attribute;
use App\Repository\ResultSetRepositoryInterface;
use App\Service\AttributeExtractor;
use DateTimeInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\QueryBuilder;
use Exception;
use ReflectionAttribute;
use ReflectionMethod;
use ReflectionProperty;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\String\UnicodeString;

/**
 * @mixin AbstractResultSet
 */
trait EntityResultSetTrait
{
    public ResultSetRepositoryInterface $repository;
    public ?array $fields = null;
    public ?int $count = null;

    public function data(array $groups = ['index', 'action']): array
    {
        if ($this->data === null) {
            $limit = $this->memory()->max_per_page ?: (int)($_ENV['TABLE_PAGE_LIMIT'] ?? 50);
            $offset = $limit * $this->getPage() - $limit;

            if ($this->getPage() > 1 && $this->pageCount() < $this->getPage()) {
                throw new InvalidPageException();
            }
            $query = $this->getDataQuery();

            $this->appendFilters($query);
            $this->appendSort($query);

            $data = $query
                ->setMaxResults($limit)
                ->setFirstResult($offset)
                ->getQuery()
                ->getResult();

            if (method_exists($this, 'mapResults')) {
                $data = array_map($this->mapResults($groups), $data);
            }
            $this->data = $data;
        }

        return $this->data;
    }

    public function count(): int
    {
        if ($this->count === null) {
            $query = $this->getDataQuery();
            if ($this->appendFilters(...)) {
                $this->appendFilters($query);
            }
            $this->count = $this->repository->filterCount($query);
        }
        return $this->count;
    }

    public function appendFilters(QueryBuilder $query): void
    {
        $filters = $this->filters();
        $request = $this->requestStack->getCurrentRequest();
        foreach ($request->query as $field => $values) {
            if (!isset($filters[$field])) {
                continue;
            }
            foreach ((array)$values as $index => $value) {
                $filters[$field]['query']($query, $index, $value);
            }
        }
    }

    protected function getDataQuery(): QueryBuilder
    {
        return $this->repository->getResultsetQuery($this->appendFilters(...));
    }

    public function tableFieldsFromEntity(array $groups = ['index', 'action'], bool $useBlacklist = true): array
    {
        if ($this->fields) {
            return $this->fields;
        }
        $this->fields = [];
        $extractor = new AttributeExtractor($this->repository->getClassName());
        $extractor->getAnyHaving(
            Attribute\ResultSet::class,
            function (
                ReflectionProperty|ReflectionMethod $reflection,
                ReflectionAttribute $attr
            ) use (
                $extractor,
                $useBlacklist,
                $groups
            ) {
                /** @var ReflectionAttribute|null $groupAttribute */
                $groupAttribute = $reflection->getAttributes(Groups::class)[0]?->getArguments();
                if ($groupAttribute && !array_intersect($groups, $groupAttribute[0])) {
                    return;
                }

                $normalized = $extractor->extractNormalisedAttributeArgs($attr);
                if ($useBlacklist && $normalized['blacklist']) {
                    return;
                }
                $fieldname = $reflection->getName();
                if ($reflection instanceof ReflectionMethod && preg_match('/^(?:get|is)(.*)$/', $fieldname, $m)) {
                    $str = new UnicodeString($m[1]);
                    $fieldname = lcfirst($str->camel());
                }
                $this->fields[$fieldname] = [
                    'label' => mb_strtoupper(mb_substr($normalized['translation'], 0, 1))
                        . mb_substr($normalized['translation'], 1),
                ];
                if ($normalized['display'] === false) {
                    $this->fields[$fieldname]['display'] = false;
                }
                if ($reflection instanceof ReflectionProperty && empty($normalized['type'])) {
                    if ($reflection->getType()?->getName() === 'bool') {
                        $this->fields[$fieldname]['type'] = 'boolean';
                    }
                    if ($reflection->getType()?->getName() === DateTimeInterface::class) {
                        $this->fields[$fieldname]['type'] = 'datetime';
                    }
                }
                if ($normalized['type']) {
                    $this->fields[$fieldname]['type'] = $normalized['type'];
                }
                if ($normalized['order']) {
                    $this->fields[$fieldname]['order'] = $normalized['order'];
                } elseif ($reflection instanceof ReflectionProperty) {
                    $this->fields[$fieldname]['order'] = $fieldname;
                }
            }
        );
        return $this->fields;
    }

    public static function normalize(object $object, array $groups): array
    {
        $extractor = new AttributeExtractor($object);
        $results = [];
        $extractor->getPropertiesHaving(
            Groups::class,
            function (ReflectionProperty $property, ReflectionAttribute $attribute) use (&$results, $groups, $object) {
                if (array_intersect($attribute->getArguments()[0], $groups)) {
                    $results[$property->getName()] = $property->getValue($object);
                }
            }
        );
        $extractor->getMethodsHaving(
            Groups::class,
            function (
                ReflectionMethod $method,
                ReflectionAttribute $attribute
            ) use (
                &$results,
                $groups,
                $object,
                $extractor
            ) {
                if (array_intersect($attribute->getArguments()[0], $groups)) {
                    $fieldname = static::getterNameToFieldname($method, $extractor);
                    $results[$fieldname] = $object->{$method->getName()}();
                }
            }
        );
        return array_map(
            function ($e) {
                if ($e instanceof Collection) {
                    $e = $e->toArray();
                }
                if ($e instanceof DateTimeInterface) {
                    $e = $e->format(DATE_RFC3339);
                }
                return $e;
            },
            $results
        );
    }

    protected static function getterNameToFieldname(ReflectionMethod $method, AttributeExtractor $extractor): string
    {
        $getterName = $method->getName();
        if (preg_match('/^(?:get|is|has)(.*)$/', $getterName, $m)) {
            $camelCaseAttribute = lcfirst($m[1]);
            $str = new UnicodeString($m[1]);
            $snakeCaseAttribute = (string)$str->snake();
            $properties = array_keys($extractor->getPropertiesHaving(ORM\Column::class));
            if (in_array($snakeCaseAttribute, $properties)) {
                return $snakeCaseAttribute;
            } else {
                return $camelCaseAttribute;
            }
        }
        throw new Exception(
            "Pas de champ trouvé pour le getter $getterName, veuillez ajouter un SerializedName"
        );
    }

    protected function appendSort(QueryBuilder $queryBuilder): void
    {
        $request = $this->requestStack->getCurrentRequest();
        $sort = $request->query->get('sort');
        $direction = $request->query->get('direction');
        if ($sort) {
            if (preg_match('/[^\w.]/', $sort)) {
                throw new BadRequestHttpException();
            }
            if (!str_contains($sort, '.')) {
                $alias = $queryBuilder->getAllAliases()[0];
                $sort = "$alias.$sort";
            }
            $direction = $direction ?? 'asc';
            if (!in_array($direction, ['asc', 'desc'])) {
                throw new BadRequestHttpException();
            }
            $queryBuilder->orderBy($sort, strtoupper($direction));
        }
    }
}
