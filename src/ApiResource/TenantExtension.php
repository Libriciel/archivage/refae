<?php

namespace App\ApiResource;

use ApiPlatform\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use App\Entity\Tenant;
use App\Entity\User;
use App\Entity\VersionableEntityInterface;
use App\Service\AttributeExtractor;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpKernel\Exception\HttpException;

readonly class TenantExtension implements QueryCollectionExtensionInterface
{
    public function __construct(
        private Security $security,
    ) {
    }

    #[Override]
    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        Operation $operation = null,
        array $context = []
    ): void {
        $extractor = new AttributeExtractor($resourceClass);
        $tenantProperty = null;
        foreach ($extractor->getPropertiesHaving(ManyToOne::class) as $manyToOne) {
            if ($manyToOne->getType()?->getName() === Tenant::class) {
                $tenantProperty = $manyToOne->getName();
            }
        }

        $user = $this->security->getUser();
        if (!$tenantProperty || !$user instanceof User || !$user->getActiveTenant()) {
            throw new HttpException(403);
        }
        $alias = $queryBuilder->getAllAliases()[0];
        $queryBuilder
            ->andWhere(sprintf('%s.%s = :tenant', $alias, $tenantProperty))
            ->setParameter('tenant', (string)$user->getActiveTenant()->getId())
        ;
        $this->applyStatePublished($queryBuilder, $resourceClass);
    }

    private function applyStatePublished(
        QueryBuilder $queryBuilder,
        string $resourceClass,
    ): void {
        if (in_array(VersionableEntityInterface::class, class_implements($resourceClass))) {
            $alias = $queryBuilder->getAllAliases()[0];
            $queryBuilder
                ->andWhere(sprintf('%s.state = :state', $alias))
                ->setParameter('state', VersionableEntityInterface::S_PUBLISHED)
            ;
        }
    }
}
