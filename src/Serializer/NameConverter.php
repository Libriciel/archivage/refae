<?php

namespace App\Serializer;

use Symfony\Component\Serializer\NameConverter\AdvancedNameConverterInterface;

class NameConverter implements AdvancedNameConverterInterface
{
    public static $static = [];

    public function normalize(
        string $propertyName,
        ?string $class = null,
        ?string $format = null,
        array $context = []
    ): string {
        return $propertyName;
    }

    public function denormalize(
        string $propertyName,
        ?string $class = null,
        ?string $format = null,
        array $context = []
    ): string {
        return $propertyName;
    }
}
