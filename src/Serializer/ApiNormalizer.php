<?php

namespace App\Serializer;

use App\Entity\Activity;
use App\Entity\Agreement;
use App\Entity\Authority;
use App\Entity\DocumentRule;
use App\Entity\Label;
use App\Entity\ManagementRule;
use App\Entity\Profile;
use App\Entity\ServiceLevel;
use App\Entity\Term;
use App\Entity\Vocabulary;
use App\Service\AttributeExtractor;
use ReflectionAttribute;
use ReflectionMethod;
use ReflectionProperty;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerInterface;

final class ApiNormalizer implements NormalizerInterface, DenormalizerInterface, SerializerAwareInterface
{
    private NormalizerInterface $decorated;
    private static $translationCache = [];

    public function __construct(NormalizerInterface $decorated)
    {
        if (!$decorated instanceof DenormalizerInterface) {
            throw new \InvalidArgumentException(
                sprintf('The decorated normalizer must implement the %s.', DenormalizerInterface::class)
            );
        }

        $this->decorated = $decorated;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return $this->decorated->supportsNormalization($data, $format);
    }

    public function normalize(
        $object,
        $format = null,
        array $context = []
    ): float|int|bool|\ArrayObject|array|string|null {
        $data = $this->decorated->normalize($object, $format, $context);

        $class = get_class($object);
        $this->extractTranslations($class);

        $newData = [];
        foreach ($data as $key => $value) {
            if (isset(self::$translationCache[$class][$key])) {
                $newData[self::$translationCache[$class][$key]] = $value;
            } else {
                $newData[$key] = $value;
            }
        }

        return $newData;
    }

    public function supportsDenormalization($data, $type, $format = null, array $context = []): bool
    {
        return $this->decorated->supportsDenormalization($data, $type, $format);
    }

    public function denormalize($data, string $type, string $format = null, array $context = []): mixed
    {
        return $this->decorated->denormalize($data, $type, $format, $context);
    }

    public function setSerializer(SerializerInterface $serializer): void
    {
        if ($this->decorated instanceof SerializerAwareInterface) {
            $this->decorated->setSerializer($serializer);
        }
    }

    public function getSupportedTypes(?string $format): array
    {
        return [
            Activity::class => true,
            Agreement::class => true,
            Authority::class => true,
            DocumentRule::class => true,
            Label::class => true,
            ManagementRule::class => true,
            Profile::class => true,
            ServiceLevel::class => true,
            Term::class => true,
            Vocabulary::class => true,
        ];
    }

    protected function extractTranslations(string $class): void
    {
        if (isset(self::$translationCache[$class])) {
            return;
        }

        $translations = [];
        $extractor = new AttributeExtractor($class);
        $extractor->getAnyHaving(
            SerializedName::class,
            function (
                ReflectionProperty|ReflectionMethod $reflection,
                ReflectionAttribute $attr
            ) use (&$translations) {
                $attributes = $reflection->getAttributes();

                /** @var ReflectionAttribute $attribute */
                $attribute = current(
                    array_filter(
                        $attributes,
                        fn(ReflectionAttribute $attribute) => $attribute->getName() === Groups::class
                    )
                );

                if (in_array('api', $attribute->getArguments()[0])) {
                    /** @var ReflectionAttribute $attribute */
                    $name = $attr->getArguments()[0];
                    $newName = $reflection->getName();
                    $newName = substr($newName, 0, 3) === 'get'
                        ? lcfirst(substr($newName, 3, strlen($newName)))
                        : $newName;

                    $translations[$name] = $newName;
                }
            }
        );
        self::$translationCache[$class] = $translations;
    }
}
