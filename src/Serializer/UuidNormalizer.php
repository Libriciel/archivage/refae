<?php

namespace App\Serializer;

use Override;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class UuidNormalizer implements NormalizerInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;

    #[Override]
    public function normalize($object, string $format = null, array $context = []): string
    {
        return (string)$object;
    }

    #[Override]
    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        return $data instanceof UuidInterface;
    }

    #[Override]
    public function getSupportedTypes(?string $format): array
    {
        return [
            UuidInterface::class => true,
        ];
    }
}
