<?php

namespace App\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Id\AbstractIdGenerator;
use Override;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class UuidGenerator extends AbstractIdGenerator
{
    #[Override]
    public function generateId(EntityManagerInterface $em, $entity): UuidInterface
    {
        return Uuid::uuid7();
    }
}
