<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use App\Doctrine\UuidGenerator;
use App\Repository\TenantRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use JsonSerializable;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: TenantRepository::class)]
#[UniqueEntity(['name'], "Ce nom est déjà utilisé")]
#[UniqueEntity(['baseurl'], "Ce baseurl est déjà utilisé")]
class Tenant implements
    JsonSerializable,
    ViewEntityInterface,
    ActivitySubjectEntityInterface,
    DeletableEntityInterface,
    TranslatedTransitionsInterface
{
    use ArrayEntityTrait;

    public const string S_AVAILABLE = 'available';
    public const string S_CLOSED = 'closed';

    public const string T_OPEN = 'open';
    public const string T_CLOSE = 'close';

    public static $initialState = self::S_AVAILABLE;

    /**
     * Url interdites
     */
    public const string FORBIDDEN_URLS = '/^(_.*|admin|api|public)$|\W+/';

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Attribute\ResultSet("Id", display: false)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['index'])]
    #[ApiProperty(identifier: false)]
    private ?LazyUuidFromString $id = null;

    #[ORM\Column(length: 255, unique: true)]
    #[Assert\NotBlank]
    #[Attribute\ResultSet("Nom")]
    #[Groups(['index', 'view'])]
    #[SerializedName("Nom")]
    private ?string $name = null;

    #[ORM\Column(length: 255, unique: true)]
    #[Assert\Regex(
        pattern: self::FORBIDDEN_URLS,
        message: "Le baseurl ne peut pas prendre cette valeur",
        match: false,
    )]
    #[Groups(['index', 'view', 'api'])]
    #[Attribute\ResultSet("Base de l'url")]
    #[SerializedName("Base de l'url")]
    #[ApiProperty(identifier: true)]
    private ?string $baseurl = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['index', 'view'])]
    #[Attribute\ResultSet("Description", display: false)]
    #[SerializedName("Description")]
    private ?string $description = null;

    #[ORM\Column]
    #[Groups(['index', 'view'])]
    #[Attribute\ResultSet("Actif ?")]
    #[SerializedName("Actif ?")]
    private ?bool $active = true;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Attribute\ResultSet("Date de création")]
    #[Groups(['index', 'view'])]
    #[SerializedName("Date de création")]
    private ?DateTimeInterface $created;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'tenants')]
    private Collection $users;

    #[ORM\OneToMany(mappedBy: 'tenant', targetEntity: Role::class, orphanRemoval: true)]
    private Collection $roles;

    #[ORM\OneToMany(mappedBy: 'tenant', targetEntity: AccessUser::class, orphanRemoval: true)]
    private Collection $accessUsers;

    #[ORM\OneToMany(mappedBy: 'tenant', targetEntity: Privilege::class, orphanRemoval: true)]
    private Collection $privileges;

    #[ORM\OneToMany(mappedBy: 'tenant', targetEntity: PrivilegeUser::class, orphanRemoval: true)]
    private Collection $privilegeUsers;

    #[ORM\ManyToMany(targetEntity: Ldap::class, mappedBy: 'tenants')]
    private Collection $ldaps;

    #[ORM\ManyToMany(targetEntity: Openid::class, mappedBy: 'tenants')]
    private Collection $openids;

    #[ORM\OneToMany(mappedBy: 'tenant', targetEntity: Activity::class, orphanRemoval: true)]
    #[ORM\OrderBy(['id' => 'ASC'])]
    private Collection $activities;

    #[ORM\OneToMany(mappedBy: 'tenant', targetEntity: LabelGroup::class, orphanRemoval: true)]
    private Collection $labelGroups;

    #[ORM\OneToMany(mappedBy: 'tenant', targetEntity: Label::class, orphanRemoval: true)]
    private Collection $labels;

    #[ORM\OneToMany(mappedBy: 'tenant', targetEntity: Profile::class, orphanRemoval: true)]
    private Collection $profiles;

    #[ORM\OneToMany(mappedBy: 'tenant', targetEntity: Agreement::class, orphanRemoval: true)]
    private Collection $agreements;

    #[ORM\OneToMany(mappedBy: 'tenant', targetEntity: Authority::class, orphanRemoval: true)]
    private Collection $authorities;

    #[ORM\OneToMany(mappedBy: 'tenant', targetEntity: Vocabulary::class, orphanRemoval: true)]
    private Collection $vocabularies;

    #[ORM\OneToMany(mappedBy: 'tenant', targetEntity: ApiToken::class, orphanRemoval: true)]
    private Collection $apiTokens;

    #[ORM\OneToMany(mappedBy: 'tenant', targetEntity: Category::class, orphanRemoval: true)]
    private Collection $categories;

    #[ORM\OneToMany(mappedBy: 'tenant', targetEntity: ServiceLevel::class, orphanRemoval: true)]
    private Collection $serviceLevels;

    #[ORM\OneToMany(mappedBy: 'tenant', targetEntity: DocumentRule::class, orphanRemoval: true)]
    private Collection $documentRules;

    #[ORM\OneToMany(mappedBy: 'tenant', targetEntity: UserCreationRequest::class, orphanRemoval: true)]
    private Collection $userCreationRequests;

    #[ORM\OneToOne(inversedBy: 'tenant', cascade: ['persist', 'remove'])]
    private ?TenantConfiguration $configuration = null;

    #[ORM\OneToMany(mappedBy: 'scope', targetEntity: Activity::class, orphanRemoval: true)]
    private Collection $scopedActivities;

    #[ORM\OneToMany(mappedBy: 'tenant', targetEntity: ManagementRule::class, orphanRemoval: true)]
    private Collection $managementRules;

    #[ORM\OneToMany(mappedBy: 'tenant', targetEntity: RuleType::class, cascade: ['all'], orphanRemoval: true,)]
    private Collection $ruleTypes;

    #[ORM\OneToMany(mappedBy: 'tenant', targetEntity: DocumentRuleImportSession::class, orphanRemoval: true)]
    private Collection $documentRuleImportSessions;

    #[ORM\OneToOne(mappedBy: 'tenant', cascade: ['persist', 'remove'])]
    private ?RgpdInfo $rgpdInfo = null;

    public function __construct()
    {
        $this->created = new DateTime();
        $this->users = new ArrayCollection();
        $this->roles = new ArrayCollection();
        $this->accessUsers = new ArrayCollection();
        $this->privileges = new ArrayCollection();
        $this->privilegeUsers = new ArrayCollection();
        $this->ldaps = new ArrayCollection();
        $this->openids = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->labelGroups = new ArrayCollection();
        $this->labels = new ArrayCollection();
        $this->profiles = new ArrayCollection();
        $this->agreements = new ArrayCollection();
        $this->authorities = new ArrayCollection();
        $this->vocabularies = new ArrayCollection();
        $this->apiTokens = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->serviceLevels = new ArrayCollection();
        $this->documentRules = new ArrayCollection();
        $this->userCreationRequests = new ArrayCollection();
        $this->scopedActivities = new ArrayCollection();
        $this->managementRules = new ArrayCollection();
        $this->ruleTypes = new ArrayCollection();
        $this->documentRuleImportSessions = new ArrayCollection();
    }

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getBaseurl(): ?string
    {
        return $this->baseurl;
    }

    public function setBaseurl(string $baseurl): static
    {
        $this->baseurl = $baseurl;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): static
    {
        $this->active = $active;
        return $this;
    }

    public function getCreated(): ?DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(DateTimeInterface $created): static
    {
        $this->created = $created;
        return $this;
    }

    #[Override]
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'baseurl' => $this->getBaseurl(),
            'description' => $this->getDescription(),
            'active' => $this->isActive(),
            'created' => $this->getCreated()->format(DATE_RFC3339),
        ];
    }

    #[Override]
    public function viewData(): array
    {
        return [
            "Nom" => 'name',
            "Base de l'url" => 'baseurl',
            "Description" => 'description',
            "Actif ?" => 'active',
            "Date de création" => 'created',
        ];
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): static
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->addTenant($this);
        }
        return $this;
    }

    public function removeUser(User $user): static
    {
        if ($this->users->removeElement($user)) {
            $user->removeTenant($this);
        }
        return $this;
    }

    /**
     * @return Collection<int, Role>
     */
    public function getRoles(): Collection
    {
        return $this->roles;
    }

    public function addRole(Role $role): static
    {
        if (!$this->roles->contains($role)) {
            $this->roles->add($role);
            $role->setTenant($this);
        }
        return $this;
    }

    public function removeRole(Role $role): static
    {
        if ($this->roles->removeElement($role)) {
            if ($role->getTenant() === $this) {
                $role->setTenant(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, AccessUser>
     */
    public function getAccessUsers(): Collection
    {
        return $this->accessUsers;
    }

    public function addAccessUser(AccessUser $accessUser): static
    {
        if (!$this->accessUsers->contains($accessUser)) {
            $this->accessUsers->add($accessUser);
            $accessUser->setTenant($this);
        }
        return $this;
    }

    public function removeAccessUser(AccessUser $accessUser): static
    {
        if ($this->accessUsers->removeElement($accessUser)) {
            if ($accessUser->getTenant() === $this) {
                $accessUser->setTenant(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, Privilege>
     */
    public function getPrivileges(): Collection
    {
        return $this->privileges;
    }

    public function addPrivilege(Privilege $privilege): static
    {
        if (!$this->privileges->contains($privilege)) {
            $this->privileges->add($privilege);
            $privilege->setTenant($this);
        }
        return $this;
    }

    public function removePrivilege(Privilege $privilege): static
    {
        if ($this->privileges->removeElement($privilege)) {
            if ($privilege->getTenant() === $this) {
                $privilege->setTenant(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, PrivilegeUser>
     */
    public function getPrivilegeUsers(): Collection
    {
        return $this->privilegeUsers;
    }

    public function addPrivilegeUser(PrivilegeUser $privilegeUser): static
    {
        if (!$this->privilegeUsers->contains($privilegeUser)) {
            $this->privilegeUsers->add($privilegeUser);
            $privilegeUser->setTenant($this);
        }
        return $this;
    }

    public function removePrivilegeUser(PrivilegeUser $privilegeUser): static
    {
        if ($this->privilegeUsers->removeElement($privilegeUser)) {
            if ($privilegeUser->getTenant() === $this) {
                $privilegeUser->setTenant(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, Ldap>
     */
    public function getLdaps(): Collection
    {
        return $this->ldaps;
    }

    public function addLdap(Ldap $ldap): static
    {
        if (!$this->ldaps->contains($ldap)) {
            $this->ldaps->add($ldap);
            $ldap->addTenant($this);
        }
        return $this;
    }

    public function removeLdap(Ldap $ldap): static
    {
        if ($this->ldaps->removeElement($ldap)) {
            $ldap->removeTenant($this);
        }
        return $this;
    }

    /**
     * @return Collection<int, Openid>
     */
    public function getOpenids(): Collection
    {
        return $this->openids;
    }

    public function addOpenid(Openid $openid): static
    {
        if (!$this->openids->contains($openid)) {
            $this->openids->add($openid);
            $openid->addTenant($this);
        }
        return $this;
    }

    public function removeOpenid(Openid $openid): static
    {
        if ($this->openids->removeElement($openid)) {
            $openid->removeTenant($this);
        }
        return $this;
    }

    #[Override]
    public function getActivityName(): string
    {
        return sprintf("le service %s", $this->name);
    }

    /**
     * @return Collection<int, Activity>
     */
    #[Override]
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    #[Override]
    public function addActivity(Activity $activity): static
    {
        if (!$this->activities->contains($activity)) {
            $this->activities->add($activity);
            $activity->setTenant($this);
        }
        return $this;
    }

    public function removeActivity(Activity $activity): static
    {
        if ($this->activities->removeElement($activity)) {
            if ($activity->getTenant() === $this) {
                $activity->setTenant(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, LabelGroup>
     */
    public function getLabelGroups(): Collection
    {
        return $this->labelGroups;
    }

    public function addLabelGroup(LabelGroup $labelGroup): static
    {
        if (!$this->labelGroups->contains($labelGroup)) {
            $this->labelGroups->add($labelGroup);
            $labelGroup->setTenant($this);
        }
        return $this;
    }

    public function removeLabelGroup(LabelGroup $labelGroup): static
    {
        if ($this->labelGroups->removeElement($labelGroup)) {
            if ($labelGroup->getTenant() === $this) {
                $labelGroup->setTenant(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, Label>
     */
    public function getLabels(): Collection
    {
        return $this->labels;
    }

    public function addLabel(Label $label): static
    {
        if (!$this->labels->contains($label)) {
            $this->labels->add($label);
            $label->setTenant($this);
        }
        return $this;
    }

    public function removeLabel(Label $label): static
    {
        if ($this->labels->removeElement($label)) {
            if ($label->getTenant() === $this) {
                $label->setTenant(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, Profile>
     */
    public function getProfiles(): Collection
    {
        return $this->profiles;
    }

    public function addProfile(Profile $profile): static
    {
        if (!$this->profiles->contains($profile)) {
            $this->profiles->add($profile);
            $profile->setTenant($this);
        }
        return $this;
    }

    public function removeProfile(Profile $profile): static
    {
        if ($this->profiles->removeElement($profile)) {
            if ($profile->getTenant() === $this) {
                $profile->setTenant(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, Agreement>
     */
    public function getAgreements(): Collection
    {
        return $this->agreements;
    }

    public function addAgreement(Agreement $agreement): static
    {
        if (!$this->agreements->contains($agreement)) {
            $this->agreements->add($agreement);
            $agreement->setTenant($this);
        }
        return $this;
    }

    public function removeAgreement(Agreement $agreement): static
    {
        if ($this->agreements->removeElement($agreement)) {
            if ($agreement->getTenant() === $this) {
                $agreement->setTenant(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, Authority>
     */
    public function getAuthorities(): Collection
    {
        return $this->authorities;
    }

    public function addAuthority(Authority $authority): static
    {
        if (!$this->authorities->contains($authority)) {
            $this->authorities->add($authority);
            $authority->setTenant($this);
        }
        return $this;
    }

    public function removeAuthority(Authority $authority): static
    {
        if ($this->authorities->removeElement($authority)) {
            if ($authority->getTenant() === $this) {
                $authority->setTenant(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, Vocabulary>
     */
    public function getVocabularies(): Collection
    {
        return $this->vocabularies;
    }

    public function addVocabulary(Vocabulary $vocabulary): static
    {
        if (!$this->vocabularies->contains($vocabulary)) {
            $this->vocabularies->add($vocabulary);
            $vocabulary->setTenant($this);
        }
        return $this;
    }

    public function removeVocabulary(Vocabulary $vocabulary): static
    {
        if ($this->vocabularies->removeElement($vocabulary)) {
            if ($vocabulary->getTenant() === $this) {
                $vocabulary->setTenant(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, ServiceLevel>
     */
    public function getServiceLevels(): Collection
    {
        return $this->serviceLevels;
    }

    public function addServiceLevel(ServiceLevel $serviceLevel): static
    {
        if (!$this->serviceLevels->contains($serviceLevel)) {
            $this->serviceLevels->add($serviceLevel);
            $serviceLevel->setTenant($this);
        }
        return $this;
    }

    public function removeServiceLevel(ServiceLevel $serviceLevel): static
    {
        if ($this->serviceLevels->removeElement($serviceLevel)) {
            if ($serviceLevel->getTenant() === $this) {
                $serviceLevel->setTenant(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, DocumentRule>
     */
    public function getDocumentRules(): Collection
    {
        return $this->documentRules;
    }

    public function addDocumentRule(DocumentRule $documentRule): static
    {
        if (!$this->DocumentRules->contains($documentRule)) {
            $this->DocumentRules->add($documentRule);
            $documentRule->setTenant($this);
        }
        return $this;
    }

    public function removeDocumentRule(DocumentRule $documentRule): static
    {
        if ($this->documentRules->removeElement($documentRule)) {
            if ($documentRule->getTenant() === $this) {
                $documentRule->setTenant(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, ApiToken>
     */
    public function getApiTokens(): Collection
    {
        return $this->apiTokens;
    }

    public function addApiToken(ApiToken $apiToken): static
    {
        if (!$this->apiTokens->contains($apiToken)) {
            $this->apiTokens->add($apiToken);
            $apiToken->setTenant($this);
        }

        return $this;
    }

    public function removeApiToken(ApiToken $apiToken): static
    {
        if ($this->apiTokens->removeElement($apiToken)) {
            // set the owning side to null (unless already changed)
            if ($apiToken->getTenant() === $this) {
                $apiToken->setTenant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Category>
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): static
    {
        if (!$this->categories->contains($category)) {
            $this->categories->add($category);
            $category->setTenant($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): static
    {
        if ($this->categories->removeElement($category)) {
            // set the owning side to null (unless already changed)
            if ($category->getTenant() === $this) {
                $category->setTenant(null);
            }
        }

        return $this;
    }

    #[Groups(['action'])]
    #[SerializedName('deletable')]
    #[Override]
    public function getDeletable(): bool
    {
        return $this->active === false;
    }

    #[Groups(['action'])]
    #[SerializedName('closable')]
    public function getClosable(): bool
    {
        return $this->active === true;
    }

    #[Groups(['action'])]
    #[SerializedName('openable')]
    public function getOpenable(): bool
    {
        return $this->active === false;
    }

    /**
     * @return Collection<int, UserCreationRequest>
     */
    public function getUserCreationRequests(): Collection
    {
        return $this->userCreationRequests;
    }

    public function addUserCreationRequest(UserCreationRequest $userCreationRequest): static
    {
        if (!$this->userCreationRequests->contains($userCreationRequest)) {
            $this->userCreationRequests->add($userCreationRequest);
            $userCreationRequest->setTenant($this);
        }

        return $this;
    }

    public function removeUserCreationRequest(UserCreationRequest $userCreationRequest): static
    {
        if ($this->userCreationRequests->removeElement($userCreationRequest)) {
            // set the owning side to null (unless already changed)
            if ($userCreationRequest->getTenant() === $this) {
                $userCreationRequest->setTenant(null);
            }
        }

        return $this;
    }

    public function getConfiguration(): ?TenantConfiguration
    {
        return $this->configuration;
    }

    public function setConfiguration(?TenantConfiguration $configuration): static
    {
        $this->configuration = $configuration;

        return $this;
    }

    /**
     * @return Collection<int, Activity>
     */
    public function getScopedActivities(): Collection
    {
        return $this->scopedActivities;
    }

    public function addScopedActivity(Activity $scopedActivity): static
    {
        if (!$this->scopedActivities->contains($scopedActivity)) {
            $this->scopedActivities->add($scopedActivity);
            $scopedActivity->setScope($this);
        }

        return $this;
    }

    public function removeScopedActivity(Activity $scopedActivity): static
    {
        if ($this->scopedActivities->removeElement($scopedActivity)) {
            // set the owning side to null (unless already changed)
            if ($scopedActivity->getScope() === $this) {
                $scopedActivity->setScope(null);
            }
        }

        return $this;
    }

    #[Override]
    public function getActivityScope(): ?Tenant
    {
        return null;
    }

    /**
     * @return Collection<int, ManagementRule>
     */
    public function getManagementRules(): Collection
    {
        return $this->managementRules;
    }

    public function addManagementRule(ManagementRule $managementRule): static
    {
        if (!$this->managementRules->contains($managementRule)) {
            $this->managementRules->add($managementRule);
            $managementRule->setTenant($this);
        }

        return $this;
    }

    public function removeManagementRule(ManagementRule $managementRule): static
    {
        if ($this->managementRules->removeElement($managementRule)) {
            // set the owning side to null (unless already changed)
            if ($managementRule->getTenant() === $this) {
                $managementRule->setTenant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, RuleType>
     */
    public function getRuleTypes(): Collection
    {
        return $this->ruleTypes;
    }

    public function addRuleType(RuleType $ruleType): static
    {
        if (!$this->ruleTypes->contains($ruleType)) {
            $this->ruleTypes->add($ruleType);
            $ruleType->setTenant($this);
        }

        return $this;
    }

    public function removeRuleType(RuleType $ruleType): static
    {
        if ($this->ruleTypes->removeElement($ruleType)) {
            // set the owning side to null (unless already changed)
            if ($ruleType->getTenant() === $this) {
                $ruleType->setTenant(null);
            }
        }

        return $this;
    }

    public function getInitialState(): string
    {
        return static::$initialState;
    }

    public function getTransitions(): array
    {
        return [
            self::T_OPEN => [
                self::S_CLOSED => self::S_AVAILABLE,
            ],
            self::T_CLOSE => [
                self::S_AVAILABLE => self::S_CLOSED,
            ],
        ];
    }

    #[Override]
    public function getStateTranslations(): array
    {
        return [
            self::S_AVAILABLE => "Disponnible",
            self::S_CLOSED => "Fermé",
        ];
    }

    #[Override]
    public function getTransitionTranslations(): array
    {
        return [
            self::T_OPEN => "ouvrir",
            self::T_CLOSE => "fermer",
        ];
    }

    public function getState(): string
    {
        return $this->active ? self::S_AVAILABLE : self::S_CLOSED;
    }

    /**
     * @return Collection<int, DocumentRuleImportSession>
     */
    public function getDocumentRuleImportSessions(): Collection
    {
        return $this->documentRuleImportSessions;
    }

    public function addDocumentRuleImportSession(DocumentRuleImportSession $documentRuleImportSession): static
    {
        if (!$this->documentRuleImportSessions->contains($documentRuleImportSession)) {
            $this->documentRuleImportSessions->add($documentRuleImportSession);
            $documentRuleImportSession->setTenant($this);
        }

        return $this;
    }

    public function removeDocumentRuleImportSession(DocumentRuleImportSession $documentRuleImportSession): static
    {
        if ($this->documentRuleImportSessions->removeElement($documentRuleImportSession)) {
            // set the owning side to null (unless already changed)
            if ($documentRuleImportSession->getTenant() === $this) {
                $documentRuleImportSession->setTenant(null);
            }
        }

        return $this;
    }

    public function getRgpdInfo(): ?RgpdInfo
    {
        return $this->rgpdInfo;
    }

    public function setRgpdInfo(?RgpdInfo $rgpdInfo): static
    {
        // unset the owning side of the relation if necessary
        if ($rgpdInfo === null && $this->rgpdInfo !== null) {
            $this->rgpdInfo->setTenant(null);
        }

        // set the owning side of the relation if necessary
        if ($rgpdInfo !== null && $rgpdInfo->getTenant() !== $this) {
            $rgpdInfo->setTenant($this);
        }

        $this->rgpdInfo = $rgpdInfo;

        return $this;
    }
}
