<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Link;
use ApiPlatform\OpenApi\Model;
use App\ApiPlatform\LabelFilter;
use App\Doctrine\UuidGenerator;
use App\Entity\Attribute\ViewSection;
use App\Repository\AgreementRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

#[ORM\Entity(repositoryClass: AgreementRepository::class)]
#[UniqueEntity(['tenant', 'identifier', 'version'], "Cet accord de versement existe déjà")]
#[ApiResource(
    operations: [
        new GetCollection(
            uriTemplate: '/{tenant_url}/agreement-published',
            uriVariables: [],
            routeName: 'api_agreement_published_list',
            openapi: new Model\Operation(
                summary: "Liste des accords de versement publiés",
                description: "Liste des accords de versement publiés",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
        new Get(
            uriTemplate: '/{tenant_url}/agreement-published/{id}',
            routeName: 'api_agreement_published_id',
            openapi: new Model\Operation(
                summary: "Récupère un accord de versement publié",
                description: "Récupère un accord de versement publié",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                    new Model\Parameter(
                        name: 'id',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
        new Get(
            uriTemplate: '/{tenant_url}/agreement-by-identifier/{identifier}',
            routeName: 'api_agreement_published_identifier',
            openapi: new Model\Operation(
                summary: "Récupère un accord de versement par identifiant",
                description: "Récupère un accord de versement par identifiant",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                    new Model\Parameter(
                        name: 'identifier',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
    ],
    normalizationContext: [
        'groups' => ['api'],
    ]
)]
#[ApiFilter(SearchFilter::class, properties: ['identifier'])]
#[ApiFilter(LabelFilter::class, properties: ['labels'])]
class Agreement implements
    ActivitySubjectEntityInterface,
    EditableEntityInterface,
    DeletableEntityInterface,
    HaveLabelsEntityInterface,
    HaveFilesInterface,
    MeilisearchIndexedInterface,
    OneTenantIntegrityEntityInterface,
    PublishableEntityInterface,
    StateMachineEntityInterface,
    TranslatedTransitionsInterface
{
    use ArrayEntityTrait;
    use HaveLabelsEntityTrait;
    use VersionableEntityTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['api', 'meilisearch', 'index'])]
    #[ApiProperty(identifier: false)]
    private ?LazyUuidFromString $id = null;

    #[ORM\ManyToOne(targetEntity: Tenant::class, inversedBy: 'agreements')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Tenant $tenant = null;

    #[ORM\Column(length: 255)]
    #[Attribute\ResultSet("Identifiant")]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Identifiant")]
    #[ViewSection('main')]
    private ?string $identifier = null;

    #[ORM\Column(length: 255)]
    #[Attribute\ResultSet("Nom")]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Nom")]
    #[ViewSection('main')]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Attribute\ResultSet("Description", blacklist: true)]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Description")]
    #[ViewSection('main')]
    private ?string $description = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    #[Attribute\ResultSet("Date de début de validité", type: 'date')]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Date de début de validité")]
    #[ViewSection('main')]
    private ?DateTimeInterface $validityBegin = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    #[Attribute\ResultSet("Date de fin de validité", type: 'date')]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Date de fin de validité")]
    #[ViewSection('main')]
    private ?DateTimeInterface $validityEnd = null;

    #[ORM\Column]
    #[Attribute\ResultSet("Public", display: false)]
    #[Groups(['api', 'index', 'meilisearch', 'csv'])]
    #[SerializedName("Public")]
    private ?bool $public = null;

    #[ORM\OneToMany(mappedBy: 'agreement', targetEntity: AgreementFile::class, orphanRemoval: true)]
    #[Groups(['api'])]
    private Collection $agreementFiles;

    #[ORM\ManyToMany(targetEntity: Label::class, inversedBy: 'agreements', fetch: 'EAGER')]
    #[Groups(['api', 'index'])]
    private Collection $labels;

    #[ORM\OneToMany(mappedBy: 'agreement', targetEntity: Activity::class, orphanRemoval: true)]
    #[ORM\OrderBy(['id' => 'ASC'])]
    #[Groups(['api'])]
    private Collection $activities;

    #[Link(toProperty: 'baseurl')]
    public ?string $baseurl;

    #[ORM\OneToMany(
        mappedBy: 'agreement',
        targetEntity: AgreementProfileIdentifier::class,
        cascade: ['all'],
        orphanRemoval: true,
    )]
    private Collection $profileIdentifiers;

    #[ORM\OneToMany(
        mappedBy: 'agreement',
        targetEntity: AgreementArchivalAgencyIdentifier::class,
        cascade: ['all'],
        orphanRemoval: true,
    )]
    private Collection $archivalAgencyIdentifiers;

    #[ORM\OneToMany(
        mappedBy: 'agreement',
        targetEntity: AgreementOriginatingAgencyIdentifier::class,
        cascade: ['all'],
        orphanRemoval: true,
    )]
    private Collection $originatingAgencyIdentifiers;

    #[ORM\OneToMany(
        mappedBy: 'agreement',
        targetEntity: AgreementTransferringAgencyIdentifier::class,
        cascade: ['all'],
        orphanRemoval: true,
    )]
    private Collection $transferringAgencyIdentifiers;

    public function __construct()
    {
        $this->agreementFiles = new ArrayCollection();
        $this->labels = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->profileIdentifiers = new ArrayCollection();
        $this->archivalAgencyIdentifiers = new ArrayCollection();
        $this->transferringAgencyIdentifiers = new ArrayCollection();
        $this->originatingAgencyIdentifiers = new ArrayCollection();
        $this->baseurl = $this->getTenantUrl();
    }

    #[Override]
    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    #[Override]
    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    #[Groups(['index_public'])]
    #[Attribute\ResultSet("Tenant")]
    #[SerializedName("Tenant")]
    public function getTenantName(): string
    {
        return $this->getTenant()->getName();
    }

    #[Override]
    public function setTenant(?Tenant $tenant): static
    {
        $this->tenant = $tenant;
        return $this;
    }

    #[Override]
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): static
    {
        $this->identifier = $identifier;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    public function getValidityBegin(): ?DateTimeInterface
    {
        return $this->validityBegin;
    }

    public function setValidityBegin(?DateTimeInterface $validityBegin): static
    {
        $this->validityBegin = $validityBegin;
        return $this;
    }

    public function getValidityEnd(): ?DateTimeInterface
    {
        return $this->validityEnd;
    }

    public function setValidityEnd(?DateTimeInterface $validityEnd): static
    {
        $this->validityEnd = $validityEnd;
        return $this;
    }

    #[Override]
    public function isPublic(): bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): static
    {
        $this->public = $public;
        return $this;
    }

    /**
     * @return Collection<int, AgreementFile>
     */
    public function getAgreementFiles(): Collection
    {
        return $this->agreementFiles;
    }

    public function addAgreementFiles(AgreementFile $file): static
    {
        if (!$this->agreementFiles->contains($file)) {
            $this->agreementFiles->add($file);
            $file->setAgreement($this);
        }
        return $this;
    }

    public function removeAgreementFile(AgreementFile $file): static
    {
        if ($this->agreementFiles->removeElement($file)) {
            if ($file->getAgreement() === $this) {
                $file->setAgreement(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, Activity>
     */
    #[Override]
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    #[Override]
    public function addActivity(Activity $activity): static
    {
        if (!$this->activities->contains($activity)) {
            $this->activities->add($activity);
            $activity->setAgreement($this);
        }
        return $this;
    }

    public function removeActivity(Activity $activity): static
    {
        if ($this->activities->removeElement($activity)) {
            if ($activity->getAgreement() === $this) {
                $activity->setAgreement(null);
            }
        }
        return $this;
    }

    #[Override]
    public function getActivityName(): string
    {
        return sprintf("l'accord de versement %s", $this->name);
    }

    #[Override]
    #[Groups(['meilisearch'])]
    #[Attribute\ResultSet("Baseurl du tenant")]
    public function getTenantUrl(): ?string
    {
        return $this->getTenant()?->getBaseurl();
    }

    #[Attribute\ResultSet("Fichiers")]
    #[SerializedName("Fichiers")]
    #[Groups(['csv'])]
    public function getFilesForCsv(): string
    {
        return implode(
            "\n",
            array_map(
                fn(AgreementFile $e) => $e->getFileName(),
                $this->getAgreementFiles()->toArray()
            )
        );
    }

    /**
     * @return AgreementProfileIdentifier[]
     */
    public function getProfileIdentifiers(): array
    {
        return $this->profileIdentifiers->toArray();
    }

    /**
     * @param AgreementProfileIdentifier[] $profileIdentifiers
     * @return $this
     */
    public function setProfileIdentifiers(array $profileIdentifiers): static
    {
        $this->profileIdentifiers = new ArrayCollection();
        foreach ($profileIdentifiers as $profileIdentifier) {
            $this->addProfileIdentifier($profileIdentifier);
        }
        return $this;
    }

    public function addProfileIdentifier(AgreementProfileIdentifier $profileIdentifier): static
    {
        if (!$this->profileIdentifiers->contains($profileIdentifier)) {
            $this->profileIdentifiers->add($profileIdentifier);
            $profileIdentifier->setAgreement($this);
        }
        return $this;
    }

    public function removeProfileIdentifier(AgreementProfileIdentifier $profileIdentifier): static
    {
        $this->profileIdentifiers->removeElement($profileIdentifier);
        return $this;
    }

    #[Attribute\ResultSet("Profils d'archives", display: false)]
    #[Groups(['index', 'meilisearch', 'view'])]
    #[SerializedName("Profils d'archives")]
    #[ViewSection('main')]
    public function getProfilesAsString(): string
    {
        return implode(
            ', ',
            $this->getProfiles()
        );
    }

    #[Groups(['api'])]
    public function getProfiles()
    {
        return array_map(
            fn(AgreementProfileIdentifier $p) => $p->getIdentifier(),
            $this->getProfileIdentifiers()
        );
    }

    #[Groups(['csv'])]
    #[Attribute\ResultSet("Profils d'archives")]
    public function getProfilesForCsv()
    {
        return implode(
            "\n",
            array_map(
                fn(AgreementProfileIdentifier $p) => $p->getIdentifier(),
                $this->getProfileIdentifiers()
            )
        );
    }

    /**
     * @return AgreementArchivalAgencyIdentifier[]
     */
    public function getArchivalAgencyIdentifiers(): array
    {
        return $this->archivalAgencyIdentifiers->toArray();
    }

    /**
     * @param AgreementArchivalAgencyIdentifier[] $archivalAgencyIdentifiers
     * @return $this
     */
    public function setArchivalAgencyIdentifiers(array $archivalAgencyIdentifiers): static
    {
        $this->archivalAgencyIdentifiers = new ArrayCollection();
        foreach ($archivalAgencyIdentifiers as $archivalAgencyIdentifier) {
            $this->addArchivalAgencyIdentifier($archivalAgencyIdentifier);
        }
        return $this;
    }

    public function addArchivalAgencyIdentifier(AgreementArchivalAgencyIdentifier $archivalAgencyIdentifier): static
    {
        if (!$this->archivalAgencyIdentifiers->contains($archivalAgencyIdentifier)) {
            $this->archivalAgencyIdentifiers->add($archivalAgencyIdentifier);
            $archivalAgencyIdentifier->setAgreement($this);
        }
        return $this;
    }

    public function removeArchivalAgencyIdentifier(AgreementArchivalAgencyIdentifier $archivalAgencyIdentifier): static
    {
        $this->archivalAgencyIdentifiers->removeElement($archivalAgencyIdentifier);
        return $this;
    }

    #[Attribute\ResultSet("Services d'archives", display: false)]
    #[Groups(['index', 'meilisearch', 'view'])]
    #[SerializedName("Services d'archives")]
    #[ViewSection('main')]
    public function getArchivalAgenciesAsString(): string
    {
        return implode(', ', $this->getArchivalAgencies());
    }

    #[Groups(['api'])]
    public function getArchivalAgencies(): array
    {
        return array_map(
            fn(AgreementArchivalAgencyIdentifier $p) => $p->getIdentifier(),
            $this->getArchivalAgencyIdentifiers()
        );
    }

    #[Attribute\ResultSet("Services d'archives")]
    #[Groups(['csv'])]
    public function getArchivalAgenciesForCsv()
    {
        return implode(
            "\n",
            array_map(
                fn(AgreementArchivalAgencyIdentifier $p) => $p->getIdentifier(),
                $this->getArchivalAgencyIdentifiers()
            )
        );
    }

    /**
    * @return AgreementOriginatingAgencyIdentifier[]
    */
    public function getOriginatingAgencyIdentifiers(): array
    {
        return $this->originatingAgencyIdentifiers->toArray();
    }

    /**
     * @param AgreementOriginatingAgencyIdentifier[] $originatingAgencyIdentifiers
     * @return $this
     */
    public function setOriginatingAgencyIdentifiers(array $originatingAgencyIdentifiers): static
    {
        $this->originatingAgencyIdentifiers = new ArrayCollection();
        foreach ($originatingAgencyIdentifiers as $originatingAgencyIdentifier) {
            $this->addOriginatingAgencyIdentifier($originatingAgencyIdentifier);
        }
        return $this;
    }

    public function addOriginatingAgencyIdentifier(
        AgreementOriginatingAgencyIdentifier $originatingAgencyIdentifier
    ): static {
        if (!$this->originatingAgencyIdentifiers->contains($originatingAgencyIdentifier)) {
            $this->originatingAgencyIdentifiers->add($originatingAgencyIdentifier);
            $originatingAgencyIdentifier->setAgreement($this);
        }
        return $this;
    }

    public function removeOriginatingAgencyIdentifier(
        AgreementOriginatingAgencyIdentifier $originatingAgencyIdentifier
    ): static {
        $this->originatingAgencyIdentifiers->removeElement($originatingAgencyIdentifier);
        return $this;
    }

    #[Attribute\ResultSet("Services producteurs", display: false)]
    #[Groups(['index', 'meilisearch', 'view'])]
    #[SerializedName("Services producteur")]
    #[ViewSection('main')]
    public function getOriginatingAgenciesAsString(): string
    {
        return implode(', ', $this->getOriginatingAgencies());
    }

    #[Groups(['api'])]
    public function getOriginatingAgencies(): array
    {
        return array_map(
            fn(AgreementOriginatingAgencyIdentifier $p) => $p->getIdentifier(),
            $this->getOriginatingAgencyIdentifiers()
        );
    }

    #[Attribute\ResultSet("Services producteurs")]
    #[Groups(['csv'])]
    public function getOriginatingAgenciesForCsv()
    {
        return implode(
            "\n",
            array_map(
                fn(AgreementOriginatingAgencyIdentifier $p) => $p->getIdentifier(),
                $this->getOriginatingAgencyIdentifiers()
            )
        );
    }

    /**
     * @return AgreementTransferringAgencyIdentifier[]
     */
    public function getTransferringAgencyIdentifiers(): array
    {
        return $this->transferringAgencyIdentifiers->toArray();
    }

    /**
     * @param AgreementTransferringAgencyIdentifier[] $transferringAgencyIdentifiers
     * @return $this
     */
    public function setTransferringAgencyIdentifiers(array $transferringAgencyIdentifiers): static
    {
        $this->transferringAgencyIdentifiers = new ArrayCollection();
        foreach ($transferringAgencyIdentifiers as $transferringAgencyIdentifier) {
            $this->addTransferringAgencyIdentifier($transferringAgencyIdentifier);
        }
        return $this;
    }

    public function addTransferringAgencyIdentifier(
        AgreementTransferringAgencyIdentifier $transferringAgencyIdentifier
    ): static {
        if (!$this->transferringAgencyIdentifiers->contains($transferringAgencyIdentifier)) {
            $this->transferringAgencyIdentifiers->add($transferringAgencyIdentifier);
            $transferringAgencyIdentifier->setAgreement($this);
        }
        return $this;
    }

    public function removeTransferringAgencyIdentifier(
        AgreementTransferringAgencyIdentifier $transferringAgencyIdentifier
    ): static {
        $this->transferringAgencyIdentifiers->removeElement($transferringAgencyIdentifier);
        return $this;
    }

    #[Attribute\ResultSet("Services versant", display: false)]
    #[Groups(['index', 'meilisearch', 'view'])]
    #[SerializedName("Services versant")]
    #[ViewSection('main')]
    public function getTransferringAgenciesAsString(): string
    {
        return implode(', ', $this->getTransferringAgencies());
    }

    #[Groups(['api'])]
    public function getTransferringAgencies(): array
    {
        return array_map(
            fn(AgreementTransferringAgencyIdentifier $p) => $p->getIdentifier(),
            $this->getTransferringAgencyIdentifiers()
        );
    }

    #[Attribute\ResultSet("Services versants")]
    #[Groups(['csv'])]
    public function getTransferringAgenciesForCsv()
    {
        return implode(
            "\n",
            array_map(
                fn(AgreementTransferringAgencyIdentifier $p) => $p->getIdentifier(),
                $this->getTransferringAgencyIdentifiers()
            )
        );
    }

    #[Override]
    public function getActivityScope(): ?Tenant
    {
        return $this->tenant;
    }

    #[Override]
    public function getFiles(): array
    {
        return $this->getAgreementFiles()->toArray();
    }

    #[Attribute\ResultSet("Public")]
    #[Groups(['view'])]
    #[SerializedName("Public")]
    #[ViewSection('other')]
    public function getReadablePublic(): string
    {
        return $this->public ? "Oui" : "Non";
    }
}
