<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Entity\Attribute\ViewSection;
use App\Repository\AccessUserRepository;
use App\Validator\LdapIdentifierExists;
use App\Validator\SameTenantOrNone;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

#[ORM\Entity(repositoryClass: AccessUserRepository::class)]
#[SameTenantOrNone(Role::class)]
#[UniqueEntity(['user', 'tenant'], "Cet utilisateur a déjà un rôle sur ce tenant")]
#[UniqueEntity(['tenant', 'ldap', 'ldapUsername'], "Cet identifiant LDAP est déjà lié à un utilisateur")]
#[LdapIdentifierExists]
class AccessUser
{
    public const string LOGIN_TYPE_REFAE = 'refae';
    public const string LOGIN_TYPE_OPENID = 'openid';
    public const string LOGIN_TYPE_LDAP = 'ldap';

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['index'])]
    private ?LazyUuidFromString $id = null;

    #[ORM\ManyToOne(inversedBy: 'accessUsers')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\ManyToOne(inversedBy: 'accessUsers')]
    #[ORM\JoinColumn(nullable: true)]
    private ?Tenant $tenant = null;

    #[ORM\ManyToOne(inversedBy: 'accessUsers')]
    private ?Openid $openid = null;

    #[ORM\ManyToOne(inversedBy: 'accessUsers')]
    private ?Ldap $ldap = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Attribute\ResultSet("Identifiant de connexion LDAP", display: false)]
    #[Groups(['index', 'view'])]
    #[ViewSection('main')]
    private ?string $ldapUsername = null;

    #[ORM\ManyToOne(inversedBy: 'accessUsers')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Role $role = null;

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function getUserCommonname(): ?string
    {
        return $this->user?->getCommonName();
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;
        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    #[Attribute\ResultSet("Tenant")]
    #[Groups(['index', 'view'])]
    #[ViewSection('main')]
    public function getTenantName(): string
    {
        return $this->getTenant()?->getName() ?? '';
    }

    public function setTenant(?Tenant $tenant): static
    {
        $this->tenant = $tenant;
        return $this;
    }

    public function getOpenid(): ?Openid
    {
        return $this->openid;
    }

    public function setOpenid(?Openid $openid): static
    {
        $this->openid = $openid;
        return $this;
    }

    public function getLdap(): ?Ldap
    {
        return $this->ldap;
    }

    public function setLdap(?Ldap $ldap): static
    {
        $this->ldap = $ldap;
        return $this;
    }

    public function getLdapUsername(): ?string
    {
        return $this->ldapUsername;
    }

    public function setLdapUsername(?string $ldapUsername): static
    {
        $this->ldapUsername = $ldapUsername;
        return $this;
    }

    public function getRole(): ?Role
    {
        return $this->role;
    }

    public function setRole(?Role $role): static
    {
        $this->role = $role;
        return $this;
    }

    public function getLoginType(): string
    {
        if ($this->getLdap()) {
            $loginType = self::LOGIN_TYPE_LDAP;
        } elseif ($this->getOpenid()) {
            $loginType = self::LOGIN_TYPE_OPENID;
        } else {
            $loginType = self::LOGIN_TYPE_REFAE;
        }
        return $loginType;
    }

    #[Groups(['index'])]
    #[SerializedName('Méthode de connexion')]
    #[ViewSection('other')]
    #[Attribute\ResultSet("méthode de connexion")]
    public function getLoginTypeName(): ?string
    {
        return match ($this->getLoginType()) {
            self::LOGIN_TYPE_REFAE => 'Refae',
            self::LOGIN_TYPE_LDAP => sprintf(
                'LDAP: %s {user: %s}',
                $this->getLdap()?->getName() ?? '',
                $this->getLdapUsername(),
            ),
            self::LOGIN_TYPE_OPENID => sprintf(
                'Openid: %s {url:%s}',
                $this->getOpenid()?->getName() ?? '',
                $this->getOpenid()?->getLoginUrl() ?? '',
            ),
            default => null,
        };
    }

    #[Groups(['action'])]
    public function getTenantBaseUrl(): ?string
    {
        return $this->getTenant()?->getBaseurl();
    }

    #[Groups(['action'])]
    public function getUserUsername(): ?string
    {
        return $this->getUser()?->getUsername();
    }

    #[Groups(['index'])]
    #[Attribute\ResultSet("rôle")]
    public function getRoleName(): ?string
    {
        return $this->getRole()?->getCommonName();
    }
}
