<?php

namespace App\Entity\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_PROPERTY)]
class IgnoreActivity
{
    public const string ALL = 'all';
    public const string VALUE = 'value';

    public function __construct(public string $type = self::ALL)
    {
    }
}
