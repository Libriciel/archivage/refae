<?php

namespace App\Entity\Attribute;

use Attribute;

#[Attribute]
class Activity
{
    public function __construct(public ?string $label = null)
    {
    }
}
