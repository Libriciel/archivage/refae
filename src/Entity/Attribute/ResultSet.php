<?php

namespace App\Entity\Attribute;

use Attribute;

#[Attribute]
class ResultSet
{
    /**
     * @param string      $translation nom de la colonne du tableau de résultats
     * @param string|null $type type de champ (ex: boolean, date, etc...)
     * @param bool        $display est affiché par défaut ?
     * @param bool        $blacklist ne sera pas disponnible dans la liste des champs
     * @param string|null $order sur quel champ appliquer un "order by"
     */
    public function __construct(
        public string $translation,
        public ?string $type = null,
        public bool $display = true,
        public bool $blacklist = false,
        public ?string $order = null,
    ) {
    }
}
