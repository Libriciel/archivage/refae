<?php

namespace App\Entity\Attribute;

use Attribute;

#[Attribute]
class ViewSection
{
    /**
     * @param string|null $section Nom de la section : 'main', 'other' ou custom
     * @param int         $order   Position au sein de la section
     */
    public function __construct(public ?string $section = null, public int $order = INF)
    {
    }
}
