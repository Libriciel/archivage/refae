<?php

namespace App\Entity;

interface EditableEntityInterface
{
    public function getEditable(): bool;
}
