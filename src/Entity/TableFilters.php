<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Repository\TableFiltersRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: TableFiltersRepository::class)]
#[UniqueEntity(['table', 'name'], "Un filtre avec ce nom existe déjà")]
class TableFilters
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    private ?LazyUuidFromString $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $filters = null;

    #[ORM\ManyToOne(inversedBy: 'filters')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Table $table = null;

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getFilters(): ?string
    {
        return $this->filters;
    }

    public function setFilters(string $filters): static
    {
        $this->filters = $filters;

        return $this;
    }

    public function getTable(): ?Table
    {
        return $this->table;
    }

    public function setTable(?Table $table): static
    {
        $this->table = $table;

        return $this;
    }
}
