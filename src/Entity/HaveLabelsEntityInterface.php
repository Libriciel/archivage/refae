<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;

interface HaveLabelsEntityInterface
{
    public function getLabels(): Collection;
    public function addLabel(Label $label): static;
    public function removeLabel(Label $label): static;
}
