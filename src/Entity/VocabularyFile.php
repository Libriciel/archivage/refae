<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Repository\VocabularyFileRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Override;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

#[UniqueEntity(['vocabulary', 'file'], "Ce vocabulaire contrôlé possède déjà ce fichier")]
#[ORM\Entity(repositoryClass: VocabularyFileRepository::class)]
class VocabularyFile implements OneTenantIntegrityEntityInterface
{
    public const string OTHER = 'other';
    public const array TYPE_VALUES = [self::OTHER];

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['index'])]
    private ?string $id = null;

    #[ORM\ManyToOne(cascade: ['persist', 'remove'], inversedBy: 'vocabularyFiles')]
    #[ORM\JoinColumn(nullable: false)]
    private ?File $file = null;

    #[ORM\ManyToOne(inversedBy: 'vocabularyFiles')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Vocabulary $vocabulary;

    #[ORM\Column(length: 255)]
    #[Assert\Choice(choices: VocabularyFile::TYPE_VALUES, message: 'Choose a valid type.')]
    private ?string $type = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['index'])]
    private ?string $description = null;

    public function __construct(Vocabulary $vocabulary = null)
    {
        $this->vocabulary = $vocabulary;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(?File $file): static
    {
        $this->file = $file;
        return $this;
    }

    public function getVocabulary(): ?Vocabulary
    {
        return $this->vocabulary;
    }

    public function setVocabulary(?Vocabulary $vocabulary): static
    {
        $this->vocabulary = $vocabulary;
        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public static function getTypeTranslations()
    {
        return [
            self::OTHER => "Autre"
        ];
    }

    #[Groups(['index'])]
    public function getTypeTrad(): string
    {
        return static::getTypeTranslations()[$this->type];
    }

    public function setType(string $type): static
    {
        $this->type = $type;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    #[Groups(['index'])]
    #[Attribute\ResultSet("Nom")]
    #[SerializedName("Nom")]
    public function getFileName(): string
    {
        return $this->getFile()?->getName() ?? '';
    }

    #[Groups(['index'])]
    #[Attribute\ResultSet("Type MIME")]
    #[SerializedName("Mime")]
    public function getFileMime(): string
    {
        return $this->getFile()?->getMime() ?? '';
    }

    #[Groups(['index'])]
    #[Attribute\ResultSet("Taille")]
    #[SerializedName("Taille")]
    public function getFileSize(): string
    {
        return $this->getFile()?->getReadableSize() ?? '';
    }

    #[Groups(['index'])]
    #[Attribute\ResultSet("Algorithme")]
    public function getFileHashAlgo(): string
    {
        return $this->getFile()?->getHashAlgo() ?? '';
    }

    #[Groups(['index', 'view'])]
    #[Attribute\ResultSet("Hash", display: false)]
    public function getFileHash(): string
    {
        return $this->getFile()?->getHash() ?? '';
    }

    #[Groups(['index'])]
    public function getVocabularyId(): string
    {
        return $this->getVocabulary()?->getId() ?? '';
    }

    #[Groups(['index'])]
    public function getFileId(): string
    {
        return $this->getFile()?->getId() ?? '';
    }

    #[Override]
    public function getTenant(): ?Tenant
    {
        return $this->getVocabulary()?->getTenant();
    }
}
