<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\OpenApi\Model;
use App\ApiPlatform\LabelFilter;
use App\Doctrine\UuidGenerator;
use App\Entity\Attribute\ViewSection;
use App\Repository\CategoryRepository;
use App\Repository\DocumentRuleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: DocumentRuleRepository::class)]
#[UniqueEntity(['tenant', 'identifier', 'version'], "Cette règle documentaire existe déjà")]
#[ApiResource(
    operations: [
        new GetCollection(
            uriTemplate: '/{tenant_url}/document-rule-published',
            uriVariables: [],
            routeName: 'api_document_rule_published_list',
            openapi: new Model\Operation(
                summary: "Liste des règles documentaires publiées",
                description: "Liste des règles documentaires publiées",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
        new Get(
            uriTemplate: '/{tenant_url}/document-rule-published/{id}',
            routeName: 'api_document_rule_published_id',
            openapi: new Model\Operation(
                summary: "Récupère une règle documentaire publiée",
                description: "Récupère une règle documentaire publiée",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                    new Model\Parameter(
                        name: 'id',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
        new Get(
            uriTemplate: '/{tenant_url}/document-rule-by-identifier/{identifier}',
            routeName: 'api_document_rule_published_identifier',
            openapi: new Model\Operation(
                summary: "Récupère une règle documentaire par identifiant",
                description: "Récupère une règle documentaire par identifiant",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                    new Model\Parameter(
                        name: 'identifier',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
    ],
    normalizationContext: [
        'groups' => ['api'],
    ]
)]
#[ApiFilter(SearchFilter::class, properties: ['identifier'])]
#[ApiFilter(LabelFilter::class, properties: ['labels'])]
class DocumentRule implements
    ActivitySubjectEntityInterface,
    EditableEntityInterface,
    DeletableEntityInterface,
    HaveFilesInterface,
    HaveLabelsEntityInterface,
    MeilisearchIndexedInterface,
    OneTenantIntegrityEntityInterface,
    PublishableEntityInterface,
    StateMachineEntityInterface
{
    use ArrayEntityTrait;
    use HaveLabelsEntityTrait;
    use VersionableEntityTrait;

    public const string ELECTRONIC = 'electronic';
    public const string PAPER = 'paper';
    public const string MIXED = 'mixed';

    public const array DOCUMENT_SUPPORT_VALUES = [self::ELECTRONIC, self::PAPER, self::MIXED];

    public const string DESTROY = 'destroy';
    public const string KEEP = 'keep';
    public const string SORT = 'sort';

    public const array APPRASAIL_RULE_FINAL_ACTION_VALUES = [self::DESTROY, self::KEEP, self::SORT];

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['api', 'meilisearch', 'index'])]
    private ?LazyUuidFromString $id = null;

    #[ORM\ManyToOne(targetEntity: Tenant::class, inversedBy: 'documentRules')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Tenant $tenant = null;

    #[ORM\Column(length: 255)]
    #[Attribute\ResultSet("Identifiant")]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Identifiant")]
    #[ViewSection('main')]
    private ?string $identifier = null;

    #[ORM\Column(length: 255)]
    #[Attribute\ResultSet("Nom")]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Nom")]
    #[ViewSection('main')]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Attribute\ResultSet("Observations", display: false)]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Observations")]
    #[ViewSection('main')]
    private ?string $observation = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Attribute\ResultSet("Texte réglementaire", display: false)]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Texte réglementaire")]
    #[ViewSection('main')]
    private ?string $regulatoryText = null;

    #[ORM\Column(length: 255)]
    #[Assert\Choice(choices: DocumentRule::DOCUMENT_SUPPORT_VALUES, message: 'Choose a valid type.')]
    #[Groups(['api'])]
    #[SerializedName("Support des documents")]
    private ?string $documentSupport = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['index', 'api', 'meilisearch', 'view', 'csv'])]
    #[Attribute\ResultSet("Formats fichiers", display: false)]
    #[SerializedName("Formats fichiers")]
    #[ViewSection('other')]
    private ?string $literalFileFormat = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Attribute\ResultSet("Localisation des documents", display: false)]
    #[Groups(['api', 'meilisearch', 'index', 'view', 'csv'])]
    #[SerializedName("Localisation des documents")]
    #[ViewSection('other')]
    private ?string $documentLocation = null;

    #[ORM\ManyToOne(targetEntity: ManagementRule::class, inversedBy: 'storageDocumentRules')]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['api'])]
    #[SerializedName("Règle de la durée d'utilité courante")]
    private ?ManagementRule $storageRule = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['api', 'meilisearch', 'view'])]
    #[Attribute\ResultSet("Date littérale de départ de calcul de la DUC", blacklist: true)]
    #[SerializedName("Date littérale de départ de calcul de la DUC")]
    #[ViewSection("Durée d'utilité courante (DUC)", 2)]
    private ?string $storageRuleLiteralStartDate = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Attribute\ResultSet("Note sur la durée d'utilité courante", blacklist: true)]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Note sur la DUC")]
    #[ViewSection("Durée d'utilité courante (DUC)", 3)]
    private ?string $storageRuleNote = null;

    #[ORM\ManyToOne(targetEntity: ManagementRule::class, inversedBy: 'appraisalDocumentRules')]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['api'])]
    #[SerializedName("Règle de la durée d'utilité administrative")]
    private ?ManagementRule $appraisalRule = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['api', 'meilisearch', 'view'])]
    #[Attribute\ResultSet("Départ calcul DUA", blacklist: true)]
    #[SerializedName("Départ calcul DUA")]
    #[ViewSection("Durée d'utilité administrative (DUA)", 2)]
    private ?string $appraisalRuleLiteralStartDate = null;

    #[ORM\Column(length: 255, nullable: false)]
    #[Assert\Choice(choices: DocumentRule::APPRASAIL_RULE_FINAL_ACTION_VALUES, message: 'Choose a valid type.')]
    #[Groups(['api'])]
    #[SerializedName("Sort final à l'issue de la DUA")]
    private ?string $appraisalRuleFinalAction = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Attribute\ResultSet("Note sur la durée d'utilité administrative", blacklist: true)]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Note sur la DUA")]
    #[ViewSection("Durée d'utilité administrative (DUA)", 4)]
    private ?string $appraisalRuleNote = null;

    #[ORM\ManyToOne(targetEntity: ManagementRule::class, inversedBy: 'accessDocumentRules')]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['api'])]
    #[SerializedName("Règle de la communicabilité")]
    private ?ManagementRule $accessRule = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['api', 'meilisearch', 'view', 'csv'])]
    #[Attribute\ResultSet("Date calcul de la communicabilité", display: false, blacklist: true)]
    #[SerializedName("Départ de calcul de la communicabilité")]
    #[ViewSection("Communicabilité", 2)]
    private ?string $accessRuleLiteralStarDate = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Attribute\ResultSet("Note sur la communicabilité", blacklist: true)]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Note sur la communicabilité")]
    #[ViewSection("Communicabilité", 3)]
    private ?string $accessRuleNote = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['api', 'meilisearch', 'view', 'csv'])]
    #[Attribute\ResultSet("Métadonnées techniques", blacklist: true)]
    #[SerializedName("Métadonnées techniques")]
    #[ViewSection('other')]
    private ?string $technicalMetadata = null;

    #[ORM\Column]
    #[Attribute\ResultSet("Publique", blacklist: true)]
    #[Groups(['api', 'index', 'meilisearch', 'csv'])]
    #[SerializedName("public")]
    private ?bool $public = null;

    #[ORM\OneToMany(mappedBy: 'documentRule', targetEntity: DocumentRuleFile::class, orphanRemoval: true)]
    private Collection $documentRuleFiles;

    #[ORM\ManyToMany(targetEntity: Label::class, inversedBy: 'documentRules', fetch: 'EAGER')]
    #[Groups(['api', 'index'])]
    private Collection $labels;

    #[ORM\OneToMany(mappedBy: 'documentRule', targetEntity: Activity::class, orphanRemoval: true)]
    #[ORM\OrderBy(['id' => 'ASC'])]
    #[Groups(['api'])]
    private Collection $activities;

    #[ORM\ManyToMany(targetEntity: Category::class, inversedBy: 'documentRules', fetch: 'EAGER')]
    #[Groups(['api', 'index'])]
    private Collection $categories;

    #[ORM\ManyToOne(inversedBy: 'documentRules')]
    #[Groups(['api'])]
    private ?Profile $Profile = null;

    public function __construct()
    {
        $this->labels = new ArrayCollection();
        $this->documentRuleFiles = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    #[Override]
    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    #[Override]
    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    #[Groups(['csv', 'index_public'])]
    #[Attribute\ResultSet("Tenant")]
    #[SerializedName("Tenant")]
    public function getTenantName(): string
    {
        return $this->getTenant()->getName();
    }

    #[Override]
    public function setTenant(?Tenant $tenant): static
    {
        $this->tenant = $tenant;
        return $this;
    }

    #[Override]
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): static
    {
        $this->identifier = $identifier;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getObservation(): ?string
    {
        return $this->observation;
    }

    public function setObservation(?string $observation): static
    {
        $this->observation = $observation;
        return $this;
    }

    public function getRegulatoryText(): ?string
    {
        return $this->regulatoryText;
    }

    public function setRegulatoryText(?string $regulatoryText): static
    {
        $this->regulatoryText = $regulatoryText;
        return $this;
    }

    public function getDocumentSupport(): ?string
    {
        return $this->documentSupport;
    }

    public function setDocumentSupport(?string $documentSupport): static
    {
        $this->documentSupport = $documentSupport;
        return $this;
    }

    public static function getDocumentSupportTranslations()
    {
        return [
            self::ELECTRONIC => "Électronique",
            self::PAPER => "Papier",
            self::MIXED => "Mixte",
        ];
    }

    #[Groups(['index', 'meilisearch', 'csv', 'view'])]
    #[Attribute\ResultSet("Support des documents")]
    #[SerializedName("Support des documents")]
    #[ViewSection('other')]
    public function getDocumentSupportTrad(): string
    {
        return static::getDocumentSupportTranslations()[$this->documentSupport];
    }

    public function getLiteralFileFormat(): ?string
    {
        return $this->literalFileFormat;
    }

    public function setLiteralFileFormat(?string $literalFileFormat): static
    {
        $this->literalFileFormat = $literalFileFormat;
        return $this;
    }

    public function getDocumentLocation(): ?string
    {
        return $this->documentLocation;
    }

    public function setDocumentLocation(?string $documentLocation): static
    {
        $this->documentLocation = $documentLocation;
        return $this;
    }

    public function getStorageRule(): ?ManagementRule
    {
        return $this->storageRule;
    }

    public function setStorageRule(?ManagementRule $storageRule): static
    {
        $this->storageRule = $storageRule;
        return $this;
    }

    public function getStorageRuleLiteralStartDate(): ?string
    {
        return $this->storageRuleLiteralStartDate;
    }

    public function setStorageRuleLiteralStartDate(?string $storageRuleLiteralStartDate): static
    {
        $this->storageRuleLiteralStartDate = $storageRuleLiteralStartDate;
        return $this;
    }

    public function getStorageRuleNote(): ?string
    {
        return $this->storageRuleNote;
    }

    public function setStorageRuleNote(?string $storageRuleNote): DocumentRule
    {
        $this->storageRuleNote = $storageRuleNote;
        return $this;
    }

    public function getAppraisalRule(): ?ManagementRule
    {
        return $this->appraisalRule;
    }

    public function setAppraisalRule(?ManagementRule $appraisalRule): static
    {
        $this->appraisalRule = $appraisalRule;
        return $this;
    }

    public function getAppraisalRuleLiteralStartDate(): ?string
    {
        return $this->appraisalRuleLiteralStartDate;
    }

    public function setAppraisalRuleLiteralStartDate(?string $appraisalRuleLiteralStartDate): static
    {
        $this->appraisalRuleLiteralStartDate = $appraisalRuleLiteralStartDate;
        return $this;
    }

    public function getAppraisalRuleFinalAction(): ?string
    {
        return $this->appraisalRuleFinalAction;
    }

    public function setAppraisalRuleFinalAction(?string $appraisalRuleFinalAction): static
    {
        $this->appraisalRuleFinalAction = $appraisalRuleFinalAction;
        return $this;
    }

    public static function getAppraisalRuleFinalActionTranslations()
    {
        return [
            self::DESTROY => "Détruire",
            self::KEEP => "Conserver",
            self::SORT => "Trier",
        ];
    }

    #[Groups(['index', 'csv', 'meilisearch', 'view'])]
    #[Attribute\ResultSet("Sort final")]
    #[SerializedName("Sort final à l'issue de la DUA")]
    #[ViewSection("Durée d'utilité administrative (DUA)", 3)]
    public function getAppraisalRuleFinalActionTrad(): string
    {
        return static::getAppraisalRuleFinalActionTranslations()[$this->appraisalRuleFinalAction];
    }

    public function getAppraisalRuleNote(): ?string
    {
        return $this->appraisalRuleNote;
    }

    public function setAppraisalRuleNote(?string $appraisalRuleNote): DocumentRule
    {
        $this->appraisalRuleNote = $appraisalRuleNote;
        return $this;
    }

    public function getAccessRule(): ?ManagementRule
    {
        return $this->accessRule;
    }

    public function setAccessRule(?ManagementRule $accessRule): static
    {
        $this->accessRule = $accessRule;
        return $this;
    }

    public function getAccessRuleLiteralStarDate(): ?string
    {
        return $this->accessRuleLiteralStarDate;
    }

    public function setAccessRuleLiteralStarDate(?string $accessRuleLiteralStarDate): static
    {
        $this->accessRuleLiteralStarDate = $accessRuleLiteralStarDate;
        return $this;
    }

    public function getAccessRuleNote(): ?string
    {
        return $this->accessRuleNote;
    }

    public function setAccessRuleNote(?string $accessRuleNote): DocumentRule
    {
        $this->appraisalRuleNote = $accessRuleNote;
        return $this;
    }

    public function getTechnicalMetadata(): ?string
    {
        return $this->technicalMetadata;
    }

    public function setTechnicalMetadata(?string $technicalMetadata): static
    {
        $this->technicalMetadata = $technicalMetadata;
        return $this;
    }

    #[Override]
    public function isPublic(): bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): static
    {
        $this->public = $public;
        return $this;
    }

    /**
     * @return Collection<int, Category>
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): static
    {
        if (!$this->categories->contains($category)) {
            $this->categories->add($category);
        }
        return $this;
    }

    public function removeCategory(Category $category): static
    {
        $this->categories->removeElement($category);
        return $this;
    }

    /**
     * @return Collection<int, DocumentRuleFile>
     */
    public function getDocumentRuleFiles(): Collection
    {
        return $this->documentRuleFiles;
    }

    #[Override]
    public function getFiles(): array
    {
        return $this->getDocumentRuleFiles()->toArray();
    }

    #[Attribute\ResultSet("Fichiers")]
    #[Groups(['csv'])]
    #[SerializedName("Schémas")]
    public function getFilesForCsv(): string
    {
        return implode(
            "\n",
            array_map(
                fn(DocumentRuleFile $e) => $e->getFileName(),
                $this->getDocumentRuleFiles()->toArray()
            )
        );
    }

    public function addDocumentRuleFile(DocumentRuleFile $documentRuleFile): static
    {
        if (!$this->documentRuleFiles->contains($documentRuleFile)) {
            $this->documentRuleFiles->add($documentRuleFile);
            $documentRuleFile->setDocumentRule($this);
        }
        return $this;
    }

    public function removeDocumentRuleFile(DocumentRuleFile $documentRuleFile): static
    {
        if ($this->documentRuleFiles->removeElement($documentRuleFile)) {
            if ($documentRuleFile->getDocumentRule() === $this) {
                $documentRuleFile->setDocumentRule(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, Activity>
     */
    #[Override]
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    #[Override]
    public function addActivity(Activity $activity): static
    {
        if (!$this->activities->contains($activity)) {
            $this->activities->add($activity);
            $activity->setDocumentRule($this);
        }
        return $this;
    }

    public function removeActivity(Activity $activity): static
    {
        if ($this->activities->removeElement($activity)) {
            if ($activity->getDocumentRule() === $this) {
                $activity->setDocumentRule(null);
            }
        }
        return $this;
    }

    #[Override]
    public function getActivityName(): string
    {
        return sprintf("la règle documentaire %s", $this->name);
    }

    #[Override]
    #[Groups(['meilisearch'])]
    #[Attribute\ResultSet("Baseurl du tenant")]
    public function getTenantUrl(): ?string
    {
        return $this->getTenant()?->getBaseurl();
    }

    #[Attribute\ResultSet("Associée à")]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Associée à")]
    #[ViewSection('main')]
    public function getCategoriesPath(): array
    {
        return array_map(
            fn(Category $c) => $c->getPath(),
            $this->getCategories()->toArray()
        );
    }

    #[Attribute\ResultSet("Règle de la durée d'utilité courante", display: false)]
    #[Groups(['index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Règle de la DUC")]
    #[ViewSection("Durée d'utilité courante (DUC)", 1)]
    public function getDuc(): ?string
    {
        return $this->getStorageRule()?->getName();
    }

    #[Attribute\ResultSet("Règle de la durée d'utilité administrative", display: false)]
    #[Groups(['index', 'view', 'meilisearch', 'csv'])]
    #[SerializedName("Règle de la DUA")]
    #[ViewSection("Durée d'utilité administrative (DUA)", 1)]
    public function getDua(): ?string
    {
        return $this->getAppraisalRule()?->getName();
    }

    #[Attribute\ResultSet("Règle de la communicabilité", display: false)]
    #[Groups(['index', 'view', 'meilisearch', 'csv'])]
    #[SerializedName("Règle de la communicabilité")]
    #[ViewSection("Communicabilité", 1)]
    public function getCommunicability(): ?string
    {
        return $this->getAccessRule()?->getName();
    }

    #[Override]
    public function getActivityScope(): ?Tenant
    {
        return $this->tenant;
    }

    public function getProfile(): ?Profile
    {
        return $this->Profile;
    }

    #[Groups(['view', 'csv'])]
    #[ViewSection(section: 'other', order: 2)]
    #[Attribute\ResultSet("Identifiant profil")]
    #[SerializedName("Identifiant profil")]
    public function getProfileIdentifier(): ?string
    {
        return $this->getProfile()?->getIdentifier();
    }

    #[Groups(['view', 'csv'])]
    #[ViewSection('other', order: 1)]
    #[Attribute\ResultSet("Nom profil")]
    #[SerializedName("Nom profil")]
    public function getProfileName(): ?string
    {
        return $this->getProfile()?->getName();
    }

    public function setProfile(?Profile $Profile): static
    {
        $this->Profile = $Profile;
        return $this;
    }

    public function initializeCategories(CategoryRepository $categoryRepository, string $separator = ' ⮕ '): static
    {
        foreach ($this->getCategories() as $category) {
            $category->setPath($categoryRepository->getPathAsString($category, ['separator' => $separator]));
        }
        return $this;
    }

    #[Attribute\ResultSet("Publique")]
    #[Groups(['index', 'view'])]
    #[SerializedName("Publique")]
    #[ViewSection('other')]
    public function getReadablePublic(): string
    {
        return $this->public ? "Oui" : "Non";
    }
}
