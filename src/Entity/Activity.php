<?php

/** @noinspection PhpInternalEntityUsedInspection */

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Repository\ActivityRepository;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Proxy\InternalProxy;
use Exception;
use IntlDateFormatter;
use Locale;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use ReflectionAttribute;
use ReflectionClass;
use ReflectionProperty;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

#[ORM\Entity(repositoryClass: ActivityRepository::class)]
class Activity
{
    public const string ACTION_CREATE = 'créé';
    public const string ACTION_ADD = 'ajouté';
    public const string ACTION_EDIT = 'modifié';
    public const string ACTION_DELETE = 'supprimé';
    public const string ACTION_VERSION = 'versionné';
    public const string ACTION_IMPORT = 'importé';

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[ORM\Column(type: 'uuid', unique: true)]
    private ?LazyUuidFromString $id = null;

    #[ORM\Column(length: 255)]
    private ?string $foreign_model = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['api'])]
    private ?DateTimeInterface $created = null;

    #[ORM\ManyToOne(inversedBy: 'created_activities')]
    private ?User $created_user = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $user_deleted_name = null;

    #[ORM\Column(length: 255)]
    #[Groups(['api'])]
    private ?string $action = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['api'])]
    private ?string $subject = null;

    #[ActivityAttribute(Tenant::class)]
    #[ORM\ManyToOne(inversedBy: 'activities')]
    private ?Tenant $tenant = null;

    #[ActivityAttribute(User::class)]
    #[ORM\ManyToOne(inversedBy: 'activities')]
    private ?User $user = null;

    #[ActivityAttribute(LabelGroup::class)]
    #[ORM\ManyToOne(inversedBy: 'activities')]
    private ?LabelGroup $labelGroup = null;

    #[ActivityAttribute(Label::class)]
    #[ORM\ManyToOne(inversedBy: 'activities')]
    private ?Label $label = null;

    #[ActivityAttribute(Profile::class)]
    #[ORM\ManyToOne(inversedBy: 'activities')]
    private ?Profile $profile = null;

    #[ActivityAttribute(Agreement::class)]
    #[ORM\ManyToOne(inversedBy: 'activities')]
    private ?Agreement $agreement = null;

    #[ActivityAttribute(Authority::class)]
    #[ORM\ManyToOne(inversedBy: 'activities')]
    private ?Authority $authority = null;

    #[ActivityAttribute(Vocabulary::class)]
    #[ORM\ManyToOne(inversedBy: 'activities')]
    private ?Vocabulary $vocabulary = null;

    #[ActivityAttribute(Category::class)]
    #[ORM\ManyToOne(inversedBy: 'activities')]
    private ?Category $category = null;

    #[ActivityAttribute(ServiceLevel::class)]
    #[ORM\ManyToOne(inversedBy: 'activities')]
    private ?ServiceLevel $serviceLevel = null;

    #[ActivityAttribute(DocumentRule::class)]
    #[ORM\ManyToOne(inversedBy: 'activities')]
    private ?DocumentRule $documentRule = null;

    #[ORM\ManyToOne(inversedBy: 'scopedActivities')]
    private ?Tenant $scope = null;

    #[ActivityAttribute(ManagementRule::class)]
    #[ORM\ManyToOne(inversedBy: 'activities')]
    private ?ManagementRule $managementRule = null;

    #[ActivityAttribute(RuleType::class)]
    #[ORM\ManyToOne(inversedBy: 'activities')]
    private ?RuleType $ruleType = null;

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function getForeignModel(): ?string
    {
        return $this->foreign_model;
    }

    public function setForeignModel(string $foreign_model): static
    {
        $this->foreign_model = $foreign_model;
        return $this;
    }

    public function getCreated(): ?DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(DateTimeInterface $created): static
    {
        $this->created = $created;
        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->foreign_model = User::class;
        $this->user = $user;
        return $this;
    }

    public function getUserDeletedName(): ?string
    {
        return $this->user_deleted_name;
    }

    public function setUserDeletedName(?string $user_deleted_name): static
    {
        $this->user_deleted_name = $user_deleted_name;
        return $this;
    }

    public function getAction(): ?string
    {
        return $this->action;
    }

    public function setAction(string $action): static
    {
        $this->action = $action;
        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): static
    {
        $this->subject = $subject;
        return $this;
    }

    public function setActivityEntity(ActivitySubjectEntityInterface $activitySubjectEntity): static
    {
        $reflection = new ReflectionClass($this);
        $targetClassname = $activitySubjectEntity::class;
        $refl = new ReflectionClass($activitySubjectEntity);
        if ($refl->implementsInterface(InternalProxy::class)) {
            $targetClassname = $refl->getParentClass()->getName();
        }
        foreach ($reflection->getProperties(ReflectionProperty::IS_PRIVATE) as $reflectionProperty) {
            $attributes = $reflectionProperty->getAttributes(ActivityAttribute::class);
            if (!$attributes) {
                continue;
            }
            $attribute = current($attributes);
            if (!$attribute instanceof ReflectionAttribute) {
                continue;
            }
            $classname = current($attribute->getArguments());
            if ($classname === $targetClassname) {
                $this->setForeignModel($targetClassname);
                $this->{$reflectionProperty->getName()} = $activitySubjectEntity;
                $activitySubjectEntity->addActivity($this);
                return $this;
            }
        }
        throw new Exception("$targetClassname not defined in Activity ActivityAttribute");
    }

    public function getActivityEntity(): ActivitySubjectEntityInterface
    {
        $reflection = new ReflectionClass($this);
        foreach ($reflection->getProperties(ReflectionProperty::IS_PRIVATE) as $reflectionProperty) {
            $attributes = $reflectionProperty->getAttributes(ActivityAttribute::class);
            if (!$attributes) {
                continue;
            }
            $attribute = current($attributes);
            if (!$attribute instanceof ReflectionAttribute) {
                continue;
            }
            $classname = current($attribute->getArguments());
            if ($classname === $this->foreign_model) {
                return $this->{$reflectionProperty->getName()};
            }
        }
        throw new Exception('ActivityEntity not found');
    }

    #[Groups(['api'])]
    public function getAuthor(): string
    {
        return $this->getCreatedUser()?->getUsername() ?: $this->user_deleted_name;
    }

    public function getOutcome(): string
    {
        $author = $this->getAuthor();
        $subject = $this->getSubject();
        switch ($this->action) {
            case 'create':
                $action = self::ACTION_CREATE;
                break;
            case 'add':
                $action = self::ACTION_ADD;
                break;
            case 'edit':
                $action = self::ACTION_EDIT;
                break;
            case 'delete':
                $action = self::ACTION_DELETE;
                break;
            case 'new-version':
                $action = self::ACTION_VERSION;
                break;
            case 'import':
                $action = self::ACTION_IMPORT;
                break;
            default:
                $entity = $this->getActivityEntity();
                // note: bug sur condition sur TranslatedTransitionsInterface :
                // l'IDE indique "Potentially polymorphic call"
                if (method_exists($entity, 'getTransitionTranslations')) {
                    $action = $entity->getTransitionTranslations()[$this->action] ?? self::ACTION_EDIT;
                } else {
                    $action = self::ACTION_EDIT;
                }
                $subject = $entity->getActivityName();
        }

        $formatterDate = new IntlDateFormatter(
            Locale::getDefault(),
            IntlDateFormatter::SHORT,
            IntlDateFormatter::NONE,
        );

        $date = $formatterDate->format($this->created);
        $time = $this->created->format('H\hi');

        // ex: <Martin Dupuis> a <ajouté> <le service FRMP (service marchés publics)>
        // le <16/11/2023> à <11h50>
        return sprintf(
            "{author:%s} a {action:%s} {subject:%s} le {date:%s} à {time:%s}",
            $author,
            $action,
            $subject,
            $date,
            $time,
        );
    }

    #[SerializedName('outcome')]
    #[Groups(['api'])]
    public function getTextOutcome()
    {
        $outcome = $this->getOutcome();
        return preg_replace('/\{\w+:([^}]+)}/', '$1', $outcome);
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): static
    {
        $this->foreign_model = Tenant::class;
        $this->tenant = $tenant;
        return $this;
    }

    public function getCreatedUser(): ?User
    {
        return $this->created_user;
    }

    public function setCreatedUser(?User $created_user): static
    {
        $this->created_user = $created_user;
        return $this;
    }

    public function getLabelGroup(): ?LabelGroup
    {
        return $this->labelGroup;
    }

    public function setLabelGroup(?LabelGroup $labelGroup): static
    {
        $this->labelGroup = $labelGroup;
        return $this;
    }

    public function getLabel(): ?Label
    {
        return $this->label;
    }

    public function setLabel(?Label $label): static
    {
        $this->label = $label;
        return $this;
    }

    public function getAgreement(): ?Agreement
    {
        return $this->agreement;
    }

    public function setAgreement(?Agreement $agreement): static
    {
        $this->agreement = $agreement;
        return $this;
    }

    public function getAuthority(): ?Authority
    {
        return $this->authority;
    }

    public function setAuthority(?Authority $authority): static
    {
        $this->authority = $authority;
        return $this;
    }

    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function setProfile(?Profile $profile): static
    {
        $this->profile = $profile;
        return $this;
    }

    public function getVocabulary(): ?Vocabulary
    {
        return $this->vocabulary;
    }

    public function setVocabulary(?Vocabulary $vocabulary): static
    {
        $this->vocabulary = $vocabulary;
        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): static
    {
        $this->category = $category;
        return $this;
    }

    public function getServiceLevel(): ?ServiceLevel
    {
        return $this->serviceLevel;
    }

    public function setServiceLevel(?ServiceLevel $serviceLevel): static
    {
        $this->serviceLevel = $serviceLevel;
        return $this;
    }

    public function getDocumentRule(): ?DocumentRule
    {
        return $this->documentRule;
    }

    public function setDocumentRule(?DocumentRule $documentRule): static
    {
        $this->documentRule = $documentRule;
        return $this;
    }

    public function getScope(): ?Tenant
    {
        return $this->scope;
    }

    public function setScope(?Tenant $scope): static
    {
        $this->scope = $scope;

        return $this;
    }

    public function getModelName(): ?string
    {
        return preg_match('/^App\\\Entity\\\(.*)/', (string) $this->foreign_model, $m)
            ? $m[1]
            : null
        ;
    }

    public function getManagementRule(): ?ManagementRule
    {
        return $this->managementRule;
    }

    public function setManagementRule(?ManagementRule $managementRule): static
    {
        $this->managementRule = $managementRule;

        return $this;
    }

    public function getRuleType(): ?RuleType
    {
        return $this->ruleType;
    }

    public function setRuleType(?RuleType $ruleType): static
    {
        $this->ruleType = $ruleType;

        return $this;
    }
}
