<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Repository\CronExecutionRepository;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Ramsey\Uuid\Lazy\LazyUuidFromString;

#[ORM\Entity(repositoryClass: CronExecutionRepository::class)]
class CronExecution
{
    public const string S_SUCCESS = 'success';
    public const string S_WARNING = 'warning';
    public const string S_ERROR = 'error';
    public const string S_ABORTED = 'aborted';
    public const string S_RUNNING = 'running';
    public const string S_CANCELED = 'canceled';

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    private ?LazyUuidFromString $id = null;

    #[ORM\ManyToOne(inversedBy: 'cronExecutions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Cron $cron = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?DateTimeInterface $dateBegin = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $dateEnd = null;

    #[ORM\Column(length: 255)]
    private ?string $state = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $report = null;

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function getCron(): ?Cron
    {
        return $this->cron;
    }

    public function setCron(?Cron $cron): static
    {
        $this->cron = $cron;

        return $this;
    }

    public function getDateBegin(): ?DateTimeInterface
    {
        return $this->dateBegin;
    }

    public function setDateBegin(DateTimeInterface $dateBegin): static
    {
        $this->dateBegin = $dateBegin;

        return $this;
    }

    public function getDateEnd(): ?DateTimeInterface
    {
        return $this->dateEnd;
    }

    public function setDateEnd(?DateTimeInterface $dateEnd): static
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): static
    {
        $this->state = $state;

        return $this;
    }

    public function getReport(): ?string
    {
        return $this->report;
    }

    public function setReport(?string $report): static
    {
        $this->report = $report;

        return $this;
    }
}
