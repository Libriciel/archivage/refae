<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;

interface VersionableEntityInterface
{
    public const string S_EDITING = 'editing';
    public const string S_PUBLISHED = 'published';
    public const string S_DEPRECATED = 'deprecated';
    public const string S_DEACTIVATED = 'deactivated';
    public const string S_REVOKED = 'revoked';

    public const string T_PUBLISH = 'publish';
    public const string T_DEACTIVATE = 'deactivate';
    public const string T_ACTIVATE = 'activate';
    public const string T_DEPRECIATE = 'depreciate';
    public const string T_REVOKE = 'revoke';
    public const string T_RECOVER = 'recover';

    public function getDeletable(): bool;
    public function getEditable(): bool;
    public function getVersionable(): bool;
    public function getPublishable(): bool;
    public function getDeactivatable(): bool;
    public function getActivatable(): bool;
    public function getRevokable(): bool;
    public function getState(): ?string;
    public function setState(string $state): static;
    public function getVersion(): ?int;
    public function setVersion(int $version): static;
    public function getInitialState(): string;
    public function getTransitions(): array;
    public function activityBypassAdd(): bool;
    public function setPreviousVersions(array $previousVersions): static;
    public function getPreviousVersions(): array;
    public function getStateTranslation(): string;
    public function getStateTranslations(): array;
    public function getTransitionTranslations(): array;
    public function getTenant(): ?Tenant;
    public function setTenant(?Tenant $tenant): static;
    public function getLabels(): Collection;
    public function addLabel(Label $label): static;
    public function getIdentifier(): ?string;
}
