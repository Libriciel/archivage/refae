<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Repository\AgreementProfileIdentifierRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: AgreementProfileIdentifierRepository::class)]
#[UniqueEntity(['tenant', 'identifier'], "Ce profilIdentifier existe déjà")]
class AgreementOriginatingAgencyIdentifier
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    private ?LazyUuidFromString $id = null;

    #[ORM\Column(length: 255)]
    private ?string $identifier = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\ManyToOne(targetEntity: Agreement::class, inversedBy: 'originatingAgencyIdentifiers')]
    #[ORM\JoinColumn(nullable: true)]
    private ?Agreement $agreement = null;

    private ?bool $originalExists = true;

    private ?bool $originalIsPublished = true;

    public function __construct(Authority|AgreementOriginatingAgencyIdentifier|null $authority = null)
    {
        if ($authority) {
            $this->setIdentifier($authority->getIdentifier());
            $this->setName($authority->getName());
        }
    }

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): static
    {
        $this->identifier = $identifier;
        return $this;
    }

    public function getName(): string
    {
        return $this->originalName ?? $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getAgreement(): Agreement
    {
        return $this->agreement;
    }

    public function setAgreement(Agreement $agreement): static
    {
        $this->agreement = $agreement;
        return $this;
    }

    public function getOriginalExists(): bool
    {
        return $this->originalExists;
    }

    public function setOriginalExists(bool $originalExists): static
    {
        $this->originalExists = $originalExists;
        return $this;
    }

    public function getOriginalIsPublished(): bool
    {
        return $this->originalIsPublished;
    }

    public function setOriginalIsPublished(bool $originalIsPublished): static
    {
        $this->originalIsPublished = $originalIsPublished;
        return $this;
    }

    public function export(): array
    {
        return [
            'identifier' => $this->getIdentifier(),
            'name' => $this->getName(),
        ];
    }

    public function getNameForSelect(): string
    {
        return sprintf('%s - %s', $this->getIdentifier(), $this->getName());
    }
}
