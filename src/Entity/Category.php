<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Entity\Attribute\ViewSection;
use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Gedmo\Mapping\Annotation as Gedmo;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Stringable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Serializer\Attribute\Ignore;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: CategoryRepository::class)]
#[Gedmo\Tree(type: 'nested')]
#[UniqueEntity(['tenant', 'name'], "Cette catégorie existe déjà")]
class Category implements
    ActivitySubjectEntityInterface,
    TreeEntityInterface,
    OneTenantIntegrityEntityInterface,
    EditableEntityInterface,
    DeletableEntityInterface,
    Stringable
{
    use ArrayEntityTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['api', 'meilisearch', 'index'])]
    private ?LazyUuidFromString $id = null;

    #[ORM\ManyToOne(fetch: 'EAGER', inversedBy: 'categories')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Tenant $tenant = null;

    #[ORM\ManyToOne(targetEntity: self::class, fetch: 'EAGER', inversedBy: 'children')]
    #[Gedmo\TreeParent]
    #[Ignore]
    private ?self $parent = null;

    #[ORM\ManyToOne(targetEntity: self::class)]
    #[Gedmo\TreeRoot]
    private ?self $root = null;

    #[Gedmo\TreeLevel]
    #[ORM\Column(name: 'lvl', type: Types::INTEGER)]
    private ?int $level = 0;

    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: self::class, fetch: 'EAGER')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    #[Ignore]
    private Collection $children;

    #[ORM\Column(nullable: true)]
    #[Gedmo\TreeLeft]
    private ?int $lft = null;

    #[ORM\Column(nullable: true)]
    #[Gedmo\TreeRight]
    private ?int $rght = null;

    #[Attribute\ResultSet("Nom")]
    #[ORM\Column(length: 255)]
    #[Assert\Regex(pattern: '/(⮕|>)/', message: "Ne peut contenir le caractère > ou ⮕", match: false)]
    #[Groups(['index', 'csv'])]
    #[ViewSection('main')]
    #[SerializedName("Nom")]
    private ?string $name = null;

    #[Attribute\ResultSet("Description", blacklist: true)]
    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['index', 'csv'])]
    #[ViewSection('main')]
    #[SerializedName("Description")]
    private ?string $description = null;

    private ?string $path = null;

    #[ORM\OneToMany(mappedBy: 'category', targetEntity: Activity::class, orphanRemoval: true)]
    private Collection $activities;

    #[ORM\ManyToMany(targetEntity: DocumentRule::class, mappedBy: 'categories')]
    #[Ignore]
    private Collection $documentRules;

    #[ORM\OneToMany(mappedBy: 'lastCategory', targetEntity: DocumentRuleImportSession::class)]
    private Collection $documentRuleImportSessions;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->documentRules = new ArrayCollection();
        $this->documentRuleImportSessions = new ArrayCollection();
    }

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    #[Override]
    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): static
    {
        $this->tenant = $tenant;

        return $this;
    }

    #[Override]
    public function getParent(): ?self
    {
        return $this->parent;
    }

    #[Override]
    public function setParent(self|TreeEntityInterface|null $parent): static
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    #[Override]
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addCategory(self $category): static
    {
        if (!$this->children->contains($category)) {
            $this->children->add($category);
            $category->setParent($this);
        }

        return $this;
    }

    public function removeCategory(self $category): static
    {
        if ($this->children->removeElement($category)) {
            // set the owning side to null (unless already changed)
            if ($category->getParent() === $this) {
                $category->setParent(null);
            }
        }

        return $this;
    }

    #[Override]
    public function getLft(): ?int
    {
        return $this->lft;
    }

    #[Override]
    public function setLft(?string $lft): static
    {
        $this->lft = $lft;

        return $this;
    }

    #[Override]
    public function getRght(): ?int
    {
        return $this->rght;
    }

    #[Override]
    public function setRght(?string $rght): static
    {
        $this->rght = $rght;

        return $this;
    }

    #[Override]
    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    #[Override]
    public function getRoot(): ?self
    {
        return $this->root;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    #[Attribute\ResultSet("Parent")]
    #[Groups(['index'])]
    #[SerializedName("Parent")]
    public function getParentName(): ?string
    {
        return $this->getParent()?->getName();
    }

    #[Groups(['action'])]
    #[Override]
    public function getDeletable(): bool
    {
        return $this->rght === ($this->lft + 1); // TODO return false si $this->document_rules->count() > 0
    }

    #[Override]
    public function setPath(string $path): ?self
    {
        $this->path = $path;

        return $this;
    }

    #[Override]
    #[Attribute\ResultSet("Chemin")]
    #[Groups(['index', 'csv'])]
    #[ViewSection('main')]
    #[SerializedName("Chemin")]
    public function getPath(): ?string
    {
        return $this->path;
    }

    #[Override]
    public function __toString(): string
    {
        return (string) $this->name;
    }

    #[Override]
    public function getEditable(): bool
    {
        return true;
    }

    /**
     * @return Collection<int, DocumentRule>
     */
    public function getDocumentRules(): Collection
    {
        return $this->documentRules;
    }

    public function addDocumentRule(DocumentRule $documentRule): static
    {
        if (!$this->documentRules->contains($documentRule)) {
            $this->documentRules->add($documentRule);
            $documentRule->addCategory($this);
        }
        return $this;
    }

    public function removeDocumentRule(DocumentRule $documentRule): static
    {
        if ($this->documentRules->removeElement($documentRule)) {
            $documentRule->removeCategory($this);
        }
        return $this;
    }

    /**
     * @return Collection<int, Activity>
     */
    #[Override]
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    #[Override]
    public function addActivity(Activity $activity): static
    {
        if (!$this->activities->contains($activity)) {
            $this->activities->add($activity);
            $activity->setCategory($this);
        }

        return $this;
    }

    public function removeActivity(Activity $activity): static
    {
        if ($this->activities->removeElement($activity)) {
            // set the owning side to null (unless already changed)
            if ($activity->getCategory() === $this) {
                $activity->setCategory(null);
            }
        }

        return $this;
    }

    #[Override]
    public function getActivityName(): string
    {
        return sprintf("la catégorie %s", $this->name);
    }

    #[Override]
    public function getActivityScope(): ?Tenant
    {
        return $this->tenant;
    }

    /**
     * @return Collection<int, DocumentRuleImportSession>
     */
    public function getDocumentRuleImportSessions(): Collection
    {
        return $this->documentRuleImportSessions;
    }

    public function addDocumentRuleImportSession(DocumentRuleImportSession $documentRuleImportSession): static
    {
        if (!$this->documentRuleImportSessions->contains($documentRuleImportSession)) {
            $this->documentRuleImportSessions->add($documentRuleImportSession);
            $documentRuleImportSession->setLastCategory($this);
        }

        return $this;
    }

    public function removeDocumentRuleImportSession(DocumentRuleImportSession $documentRuleImportSession): static
    {
        if ($this->documentRuleImportSessions->removeElement($documentRuleImportSession)) {
            // set the owning side to null (unless already changed)
            if ($documentRuleImportSession->getLastCategory() === $this) {
                $documentRuleImportSession->setLastCategory(null);
            }
        }

        return $this;
    }
}
