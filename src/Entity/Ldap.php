<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Entity\Attribute\ResultSet;
use App\Repository\LdapRepository;
use App\Service\Encryption;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use ErrorException;
use Exception;
use JsonSerializable;
use LdapRecord\Auth\Guard;
use LdapRecord\Container as LdapContainer;
use LdapRecord\Connection as LdapConnection;
use LdapRecord\Models\Model;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Serializer\Attribute\Ignore;
use Throwable;

#[ORM\Entity(repositoryClass: LdapRepository::class)]
class Ldap implements JsonSerializable, ViewEntityInterface, TenantsIntegrityEntityInterface
{
    use ArrayEntityTrait;

    public const array LDAP_OPTS = [
        'DEREF',
        'SIZELIMIT',
        'TIMELIMIT',
        'ERROR_NUMBER',
        'RESTART',
        'HOST_NAME',
        'ERROR_STRING',
        'DIAGNOSTIC_MESSAGE',
        'MATCHED_DN',
        'SERVER_CONTROLS',
        'CLIENT_CONTROLS',
        'X_KEEPALIVE_IDLE',
        'X_KEEPALIVE_PROBES',
        'X_KEEPALIVE_INTERVAL',
        'X_TLS_CACERTDIR',
        'X_TLS_CACERTFILE',
        'X_TLS_CERTFILE',
        'X_TLS_CIPHER_SUITE',
        'X_TLS_CRLCHECK',
        'X_TLS_CRLFILE',
        'X_TLS_DHFILE',
        'X_TLS_KEYFILE',
        'X_TLS_PROTOCOL_MIN',
        'X_TLS_RANDOM_FILE',
        'X_TLS_REQUIRE_CERT',
    ];
    public const array LDAP_OPTS_INT = [
        'DEREF',
        'SIZELIMIT',
        'TIMELIMIT',
        'ERROR_NUMBER',
        'RESTART',
        'HOST_NAME',
        'ERROR_STRING',
        'DIAGNOSTIC_MESSAGE',
        'MATCHED_DN',
        'SERVER_CONTROLS',
        'CLIENT_CONTROLS',
        'X_KEEPALIVE_IDLE',
        'X_KEEPALIVE_PROBES',
        'X_KEEPALIVE_INTERVAL',
        'X_TLS_PROTOCOL_MIN',
        'X_TLS_REQUIRE_CERT',
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Attribute\ResultSet("Id", blacklist: true)]
    #[Groups(['index'])]
    private ?LazyUuidFromString $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['index', 'view'])]
    #[SerializedName('Nom')]
    #[ResultSet('Nom')]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Groups(['index', 'view'])]
    private ?string $host = null;

    #[ORM\Column]
    #[Groups(['index', 'view'])]
    private ?int $port = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['index', 'view'])]
    private ?string $user_query_login = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['index', 'view'])]
    private ?string $user_query_password = null;

    #[ORM\Column(length: 255)]
    #[Groups(['index', 'view'])]
    private ?string $ldap_root_search = null;

    #[ORM\Column(length: 65535, nullable: true)]
    #[Groups(['index', 'view'])]
    private ?string $ldap_users_filter = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['index', 'view'])]
    private ?string $account_prefix = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['index', 'view'])]
    private ?string $account_suffix = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['index', 'view'])]
    private ?string $description = null;

    #[ORM\Column(nullable: true)]
    private ?bool $use_proxy = null;

    #[ORM\Column(nullable: true)]
    private ?bool $use_ssl = null;

    #[ORM\Column(nullable: true)]
    private ?bool $use_tls = null;

    #[ORM\Column(length: 255)]
    #[Groups(['index', 'view'])]
    private ?string $schema = null;

    #[ORM\Column]
    private ?bool $follow_referrals = null;

    #[ORM\Column]
    #[Groups(['index', 'view'])]
    private ?int $version = null;

    #[ORM\Column]
    #[Groups(['index', 'view'])]
    private ?int $timeout = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['index', 'view'])]
    private ?string $custom_options = '[]';

    #[ORM\ManyToMany(targetEntity: Tenant::class, inversedBy: 'ldaps')]
    private Collection $tenants;

    #[ORM\Column(length: 512)]
    #[SerializedName("Nom de l'attribut utilisé pour se logger au LDAP")]
    #[Groups(['index', 'view', 'attribute'])]
    private ?string $user_login_attribute = null;

    #[ORM\Column(length: 512, nullable: true)]
    #[Groups(['index', 'view', 'attribute'])]
    #[SerializedName("Nom de l'attribut utilisé pour se logger à Refae")]
    private ?string $user_username_attribute = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['index', 'view', 'attribute'])]
    #[SerializedName("Attribut utilisé pour le champ nom")]
    private ?string $user_name_attribute = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['index', 'view', 'attribute'])]
    #[SerializedName("Attribut utilisé pour le champ mail")]
    private ?string $user_mail_attribute = null;

    #[Groups(['index', 'view'])]
    private int $countResults = 0;

    #[ORM\OneToMany(mappedBy: 'ldap', targetEntity: AccessUser::class, orphanRemoval: true)]
    private Collection $accessUsers;

    #[Ignore]
    private ?LdapConnection $provider = null;

    #[Ignore]
    private ?Guard $auth = null;

    #[Ignore]
    private ?string $test_username = null;

    #[Ignore]
    private ?string $test_password = null;

    public function __construct()
    {
        $this->tenants = new ArrayCollection();
        $this->accessUsers = new ArrayCollection();
    }

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getHost(): ?string
    {
        return $this->host;
    }

    public function setHost(string $host): static
    {
        $this->host = $host;
        return $this;
    }

    public function getPort(): ?int
    {
        return $this->port;
    }

    public function setPort(int $port): static
    {
        $this->port = $port;
        return $this;
    }

    public function getUserQueryLogin(): ?string
    {
        return $this->user_query_login;
    }

    public function setUserQueryLogin(?string $user_query_login): static
    {
        $this->user_query_login = $user_query_login;
        return $this;
    }

    public function getUserQueryPassword(): ?string
    {
        $encryption = new Encryption();
        return $encryption->decrypt($this->user_query_password ?? '');
    }

    public function setUserQueryPassword(?string $user_query_password): static
    {
        if ($user_query_password) {
            $encryption = new Encryption();
            $this->user_query_password = $encryption->encrypt($user_query_password);
        }
        return $this;
    }

    public function getLdapRootSearch(): ?string
    {
        return $this->ldap_root_search;
    }

    public function setLdapRootSearch(string $ldap_root_search): static
    {
        $this->ldap_root_search = $ldap_root_search;
        return $this;
    }

    public function getUserLoginAttribute(): ?string
    {
        return $this->user_login_attribute;
    }

    public function setUserLoginAttribute(string $user_login_attribute): static
    {
        $this->user_login_attribute = $user_login_attribute;
        return $this;
    }

    public function getLdapUsersFilter(): ?string
    {
        return $this->ldap_users_filter;
    }

    public function setLdapUsersFilter(?string $ldap_users_filter): static
    {
        $this->ldap_users_filter = $ldap_users_filter;
        return $this;
    }

    public function getAccountPrefix(): ?string
    {
        return $this->account_prefix;
    }

    public function setAccountPrefix(?string $account_prefix): static
    {
        $this->account_prefix = $account_prefix;
        return $this;
    }

    public function getAccountSuffix(): ?string
    {
        return $this->account_suffix;
    }

    public function setAccountSuffix(?string $account_suffix): static
    {
        $this->account_suffix = $account_suffix;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function isUseProxy(): ?bool
    {
        return $this->use_proxy;
    }

    public function setUseProxy(?bool $use_proxy): static
    {
        $this->use_proxy = $use_proxy;

        return $this;
    }

    public function isUseSsl(): ?bool
    {
        return $this->use_ssl;
    }

    public function setUseSsl(?bool $use_ssl): static
    {
        $this->use_ssl = $use_ssl;

        return $this;
    }

    public function isUseTls(): ?bool
    {
        return $this->use_tls;
    }

    public function setUseTls(?bool $use_tls): static
    {
        $this->use_tls = $use_tls;

        return $this;
    }

    public function getUserNameAttribute(): ?string
    {
        return $this->user_name_attribute;
    }

    public function setUserNameAttribute(?string $user_name_attribute): static
    {
        $this->user_name_attribute = $user_name_attribute;

        return $this;
    }

    public function getUserMailAttribute(): ?string
    {
        return $this->user_mail_attribute;
    }

    public function setUserMailAttribute(?string $user_mail_attribute): static
    {
        $this->user_mail_attribute = $user_mail_attribute;

        return $this;
    }

    public function getSchema(): ?string
    {
        return $this->schema;
    }

    public function setSchema(string $schema): static
    {
        $this->schema = $schema;

        return $this;
    }

    public function isFollowReferrals(): ?bool
    {
        return $this->follow_referrals;
    }

    public function setFollowReferrals(bool $follow_referrals): static
    {
        $this->follow_referrals = $follow_referrals;

        return $this;
    }

    public function getVersion(): ?int
    {
        return $this->version;
    }

    public function setVersion(int $version): static
    {
        $this->version = $version;

        return $this;
    }

    public function getTimeout(): ?int
    {
        return $this->timeout;
    }

    public function setTimeout(int $timeout): static
    {
        $this->timeout = $timeout;

        return $this;
    }

    public function getCustomOptions(): ?string
    {
        return $this->custom_options;
    }

    public function setCustomOptions(string $custom_options): static
    {
        $this->custom_options = $custom_options;

        return $this;
    }

    public function getUserUsernameAttribute(): ?string
    {
        return $this->user_username_attribute;
    }

    public function setUserUsernameAttribute(?string $user_username_attribute): static
    {
        $this->user_username_attribute = $user_username_attribute;

        return $this;
    }

    /**
     * @return Collection<int, Tenant>
     */
    #[Override]
    public function getTenants(): Collection
    {
        return $this->tenants;
    }

    public function addTenant(Tenant $tenant): static
    {
        if (!$this->tenants->contains($tenant)) {
            $this->tenants->add($tenant);
        }

        return $this;
    }

    public function removeTenant(Tenant $tenant): static
    {
        $this->tenants->removeElement($tenant);

        return $this;
    }

    /**
     * Donne la config pour initialiser un ldap via Adldap2
     * @return array
     */
    public function getLdapConfig(): array
    {
        $opts = [];
        $meta = $this->getCustomOptions()
            ? json_decode($this->getCustomOptions(), true)
            : [];
        foreach ($meta ?? [] as $option => $val) {
            if (defined('LDAP_OPT_' . $option)) {
                $opts[constant('LDAP_OPT_' . $option)]
                    = in_array($option, Ldap::LDAP_OPTS_INT) ? (int)$val : $val;
            }
        }

        return [
            // Mandatory Configuration Options
            'hosts' => [$this->getHost()],
            'base_dn' => $this->getLdapRootSearch(),
            'username' => $this->getUserQueryLogin(),
            'password' => $this->getUserQueryPassword(),

            // Optional Configuration Options
            'port' => (int)$this->getPort(),
            'follow_referrals' => (bool)$this->isFollowReferrals(),
            'use_ssl' => (bool)$this->isUseSsl(),
            'use_tls' => (bool)$this->isUseTls(),
            'version' => $this->getVersion(),
            'timeout' => $this->getTimeout(),

            // Custom LDAP Options
            'options' => $opts,
        ];
    }

    /**
     * Connexion au LDAP
     * @return LdapConnection|null
     */
    public function connect(): ?LdapConnection
    {
        if (empty($this->provider)) {
            try {
                set_error_handler(
                    function ($errno, $errstr, $errfile, $errline): never {
                        throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
                    }
                );
                $connection = new LdapConnection($this->getLdapConfig());
                $connection->connect();
                $this->provider = $connection;
                // ajoute un dispatcher à $connection
                $container = new LdapContainer();
                $container->addConnection($connection);
            } catch (Throwable) {
                return null;
            } finally {
                restore_error_handler();
            }
        }
        return $this->provider;
    }

    /**
     * Vérifie la connexion au LDAP
     */
    public function ping(): bool
    {
        return (bool)$this->connect();
    }

    #[Groups(['action'])]
    #[SerializedName('deletable')]
    public function getPing()
    {
        return $this->ping();
    }

    public function getRandomEntry(): array
    {
        $search = $this->connect()?->query();
        if (!$search) {
            return ["Echec de connexion, vérifier le paramétrage" => ''];
        }
        $arr = $search->paginate(100);
        unset($arr['count']);
        $entry = $arr[array_rand($arr)];
        $attrs = [];
        foreach ($entry as $key => $values) {
            if (
                !is_array($values)
                || !is_string($values[0])
                || !json_encode($values[0])
                || str_contains((string) $key, 'password')
            ) {
                continue;
            }
            unset($values['count']);
            $attrs[$key] = implode(', ', $values);
        }
        return $attrs;
    }

    public function getCount(): int
    {
        $provider = $this->connect();
        $search = $provider->query();

        $filter = $this->getLdapUsersFilter();
        $search->rawFilter($filter);
        $paginator = $search->paginate(100);
        return is_array($paginator) ? count($paginator) : $paginator->count();
    }

    public function getFiltered(): array
    {
        $provider = $this->connect();
        $search = $provider->query();

        $filter = $this->getLdapUsersFilter();
        $search->rawFilter($filter);

        $arr = $search->paginate(100);
        unset($arr['count']);
        $entry = $arr[array_rand($arr)];

        return [
            "utilisé pour se logger au LDAP\t" => $entry[$this->getUserLoginAttribute()][0] ?? null,
            "utilisé pour se logger à refae\t" => $entry[$this->getUserUsernameAttribute()][0] ?? null,
            "utilisé pour le champ nom\t\t" => $entry[$this->getUserNameAttribute()][0] ?? null,
            "utilisé pour le champ mail\t\t" => $entry[$this->getUserMailAttribute()][0] ?? null,
        ];
    }

    /**
     * Authentifie au LDAP
     * @return Guard|null
     */
    public function auth(): ?Guard
    {
        if (empty($this->auth)) {
            $connect = $this->connect();
            if ($connect) {
                $this->auth = $connect->auth();
            } else {
                return null;
            }
        }
        return $this->auth;
    }

    /**
     * Action de login
     * Renvoi l'entrée LDAP ou le succès de la vérification du password
     * @return Model|bool
     * @throws Exception
     */
    public function log(string $login, string $password, bool $bindAsUser = true)
    {
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline): never {
                throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
            }
        );
        $passwordOk = false;
        try {
            if (!$this->auth()) {
                return false;
            }
            $passwordOk = $this->auth()
                ->attempt($this->getAffixedLogin($login), $password, $bindAsUser);
        } finally {
            restore_error_handler();
        }
        return $passwordOk
            ? ($this
                ->connect()
                ->query()
                ->where($this->getUserLoginAttribute(), '=', $login)->first() ?: true)
            : false;
    }

    #[Override]
    public function viewData(): array
    {
        return [
            "Nom" => 'name',
            "Description" => 'description',
            "Schéma de connexion" => 'schema',
            "Tenants liés" => function (Ldap $ldap) {
                $ul = '<ul>';
                /** @var Tenant $tenant */
                foreach ($ldap->getTenants() as $tenant) {
                    $name = htmlentities((string) $tenant->getName());
                    $ul .= "<li>$name</li>\n";
                }
                $ul .= '</ul>';
                return $ul;
            },
            "Version" => 'version',
            "Hôte" => 'host',
            "Port" => 'port',
            "Proxy ?" => 'use_proxy',
            "SSL ?" => 'use_ssl',
            "TLS ?" => 'use_tls',
            "Suivre redirection ?" => 'follow_referrals',
            "Timeout" => 'timeout',
            "Options supplémentaire" => 'custom_options|json_decode',
            "Query login" => 'user_query_login',
            "Root search" => 'ldap_root_search',
            "Filtre utilisateur" => 'ldap_users_filter',
            "Préfixe de login" => 'account_prefix',
            "Suffix de login" => 'account_suffix',
            "Attribut utilisé pour se logger sur le LDAP" => 'user_login_attribute',
            "Attribut utilisé pour se logger sur refae" => 'user_username_attribute',
            "Attribut utilisé pour nommer l'utilisateur" => 'user_name_attribute',
            "Attribut utilisé pour le mail de l'utilisateur" => 'user_mail_attribute',
        ];
    }

    public function getSelectCustomOptions(): array
    {
        return array_combine(
            self::LDAP_OPTS,
            self::LDAP_OPTS,
        );
    }

    public function getSelectCustomOptionsIntegers(): array
    {
        return self::LDAP_OPTS_INT;
    }

    public function getTestUsername(): ?string
    {
        return $this->test_username;
    }

    public function getTestPassword(): ?string
    {
        return $this->test_password;
    }

    public function setTestUsername(string $testUsername): static
    {
        $this->test_username = $testUsername;
        return $this;
    }

    public function setTestPassword(string $testPassword): static
    {
        $this->test_password = $testPassword;
        return $this;
    }

    /**
     * Pagination ldap
     * @param int      $page
     * @param int|null $limit
     * @param array    $filters
     * @return array
     * @throws ErrorException
     */
    public function paginateLdap(int $page = 1, ?int $limit = null, array $filters = []): array
    {
        $limit ??= $_ENV['TABLE_PAGE_LIMIT'] ?? 50;
        $data = [];
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline): never {
                throw new ErrorException(
                    $errstr,
                    0,
                    $errno,
                    $errfile,
                    $errline
                );
            }
        );
        try {
            $provider = $this->connect();
            $search = $provider->query();
            $search->orderBy($this->getUserLoginAttribute());

            $filter = $this->getLdapUsersFilter();
            $search->rawFilter($filter);

            foreach ($filters as $field => $values) {
                if (!is_array($values)) {
                    continue;
                }
                foreach ($values as $value) {
                    $search->where($field, 'contains', $value);
                }
            }

            $paginator = $search->paginate($limit, $page - 1);

            /** @var array $entry */
            foreach ($paginator as $entry) {
                $d = [
                    'conf-login' => $entry[$this->getUserLoginAttribute()][0] ?? 'null',
                    'conf-username' => $entry[$this->getUserUsernameAttribute()][0] ?? 'null',
                    'conf-name' => $entry[$this->getUserNameAttribute()][0] ?? null,
                    'conf-email' => $entry[$this->getUserMailAttribute()][0] ?? null,
                ];
                foreach ($entry as $attr => $values) {
                    $value = $values[0] ?? null;
                    if (!is_string($value)) {
                        continue;
                    }
                    $value = json_encode($value) ? $value : null; //filtre les binaires
                    if ($value) {
                        $d[$attr] = $value;
                    } else {
                        unset($d[$attr]);
                    }
                }
                $data[] = $d;
            }
            $this->countResults = count($paginator);
        } catch (Throwable) {
        }
        restore_error_handler();
        return $data;
    }

    public function getCountResults(): int
    {
        return $this->countResults;
    }

    /**
     * Pagination ldap
     * @param Ldap  $ldap
     * @param int   $page
     * @param array $filters
     * @return array
     * @throws ErrorException
     */
    public function findEntry(string $login): array
    {
        $data = [];
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline): never {
                throw new ErrorException(
                    $errstr,
                    0,
                    $errno,
                    $errfile,
                    $errline
                );
            }
        );
        try {
            $provider = $this->connect();
            $search = $provider->query();
            $search->orderBy($this->getUserLoginAttribute());

            $filter = $this->getLdapUsersFilter();
            $search->rawFilter($filter);
            $search->where(
                $this->getUserUsernameAttribute(),
                'contains',
                $login
            );
            /** @var array $ldapUser */
            $ldapUser = $search->firstOrFail();
            foreach ($ldapUser as $key => $values) {
                if (is_string($key)) {
                    $data[$key] = !str_contains($key, 'password')
                        ? $values
                        : ['********'];
                }
            }
        } catch (Throwable) {
        }
        unset($data['count']);

        foreach ($data as &$values) {
            $values = (array)$values;
            unset($values['count']);
            foreach ($values as &$v) {
                if ($this->binaryIsImage($v)) {
                    $v = '<img alt="binary_data"'
                        . ' style="max-width: 100%; max-height: 250px;"'
                        . ' src="data:image/png;base64, ' . base64_encode((string) $v) . '">';
                } elseif (strlen((string) $v) > 1024) {
                    $v = '<i>** binary_data **</i>';
                } else {
                    $v = htmlentities((string) $v);
                }
            }
        }
        unset($values, $v);
        restore_error_handler();
        return array_map(fn($v) => implode("<br>\n", $v), $data);
    }

    private function binaryIsImage(string $binary): bool
    {
        $signatures = [
            'jpeg' => "\xFF\xD8\xFF",
            'gif' => "GIF",
            'png' => "\x89\x50\x4e\x47\x0d\x0a\x1a\x0a",
            'bmp' => "BM",
            'psd' => "8BPS",
            'swf' => "FWS",
        ];
        foreach ($signatures as $signature) {
            if (str_starts_with($binary, $signature)) {
                return true;
            }
        }
        return false;
    }

    #[Override]
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
        ];
    }

    /**
     * @return Collection<int, AccessUser>
     */
    public function getAccessUsers(): Collection
    {
        return $this->accessUsers;
    }

    public function addAccessUser(AccessUser $accessUser): static
    {
        if (!$this->accessUsers->contains($accessUser)) {
            $this->accessUsers->add($accessUser);
            $accessUser->setLdap($this);
        }

        return $this;
    }

    public function removeAccessUser(AccessUser $accessUser): static
    {
        if ($this->accessUsers->removeElement($accessUser)) {
            // set the owning side to null (unless already changed)
            if ($accessUser->getLdap() === $this) {
                $accessUser->setLdap(null);
            }
        }

        return $this;
    }

    public function getTenantJsonBaseurls()
    {
        $baseUrls = $this->getTenants()
            ->map(fn (Tenant $tenant) => $tenant->getBaseurl())
            ->toArray()
        ;
        return json_encode($baseUrls);
    }

    #[Groups(['action'])]
    public function getDeletable(): bool
    {
        return $this->getAccessUsers()->count() === 0;
    }

    /**
     * Note: il ne faut pas inclure $this->provider
     * @return array
     */
    public function __serialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'host' => $this->host,
            'port' => $this->port,
            'user_query_login' => $this->user_query_login,
            'user_query_password' => $this->user_query_password,
            'ldap_root_search' => $this->ldap_root_search,
            'ldap_users_filter' => $this->ldap_users_filter,
            'account_prefix' => $this->account_prefix,
            'account_suffix' => $this->account_suffix,
            'description' => $this->description,
            'use_proxy' => $this->use_proxy,
            'use_ssl' => $this->use_ssl,
            'use_tls' => $this->use_tls,
            'schema' => $this->schema,
            'follow_referrals' => $this->follow_referrals,
            'version' => $this->version,
            'timeout' => $this->timeout,
            'custom_options' => $this->custom_options,
            'tenants' => $this->tenants,
            'user_login_attribute' => $this->user_login_attribute,
            'user_username_attribute' => $this->user_username_attribute,
            'user_name_attribute' => $this->user_name_attribute,
            'user_mail_attribute' => $this->user_mail_attribute,
            'countResults' => $this->countResults,
            'accessUsers' => $this->accessUsers,
        ];
    }

    public function getAffixedLogin(string $username): ?string
    {
        return $this->getAccountPrefix()
            . $username
            . $this->getAccountSuffix();
    }

    public function getlistAvailableAttributes(): array
    {
        $provider = $this->connect();
        $search = $provider->query();

        $filter = $this->getLdapUsersFilter();
        $search->rawFilter($filter);
        $paginator = $search->paginate(1);
        $data = $paginator[0] ?? [];
        $keys = [];
        foreach ($data as $key => $values) {
            if (
                !is_array($values)
                || !is_string($values[0])
                || !json_encode($values[0])
                || str_contains((string) $key, 'password')
            ) {
                continue;
            }
            $keys[] = $key;
        }
        return $keys;
    }
}
