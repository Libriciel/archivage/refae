<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\OpenApi\Model;
use App\ApiPlatform\LabelFilter;
use App\Doctrine\UuidGenerator;
use App\Entity\Attribute\ViewSection;
use App\Repository\ServiceLevelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

#[ORM\Entity(repositoryClass: ServiceLevelRepository::class)]
#[UniqueEntity(['tenant', 'identifier', 'version'], "Ce niveau de service existe déjà")]
#[ApiResource(
    operations: [
        new GetCollection(
            uriTemplate: '/{tenant_url}/service-level-published',
            uriVariables: [],
            routeName: 'api_service_level_published_list',
            openapi: new Model\Operation(
                summary: "Liste des niveaux de service publiées",
                description: "Liste des niveaux de service publiées",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
        new Get(
            uriTemplate: '/{tenant_url}/service-level-published/{id}',
            routeName: 'api_service_level_published_id',
            openapi: new Model\Operation(
                summary: "Récupère un niveau de service publié",
                description: "Récupère un niveau de service publié",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                    new Model\Parameter(
                        name: 'id',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
        new Get(
            uriTemplate: '/{tenant_url}/service-level-by-identifier/{identifier}',
            routeName: 'api_service_level_published_identifier',
            openapi: new Model\Operation(
                summary: "Récupère un niveau de service par identifiant",
                description: "Récupère un niveau de service par identifiant",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                    new Model\Parameter(
                        name: 'identifier',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
    ],
    normalizationContext: [
        'groups' => ['api'],
    ]
)]
#[ApiFilter(SearchFilter::class, properties: ['identifier'])]
#[ApiFilter(LabelFilter::class, properties: ['labels'])]
class ServiceLevel implements
    ActivitySubjectEntityInterface,
    EditableEntityInterface,
    DeletableEntityInterface,
    HaveFilesInterface,
    HaveLabelsEntityInterface,
    MeilisearchIndexedInterface,
    OneTenantIntegrityEntityInterface,
    PublishableEntityInterface,
    StateMachineEntityInterface,
    TranslatedTransitionsInterface
{
    use ArrayEntityTrait;
    use HaveLabelsEntityTrait;
    use VersionableEntityTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['api', 'meilisearch', 'index'])]
    private ?LazyUuidFromString $id = null;

    #[ORM\ManyToOne(targetEntity: Tenant::class, inversedBy: 'serviceLevels')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Tenant $tenant = null;

    #[ORM\Column(length: 255)]
    #[Attribute\ResultSet("Identifiant")]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Identifiant")]
    #[ViewSection('main')]
    private ?string $identifier = null;

    #[ORM\Column(length: 255)]
    #[Attribute\ResultSet("Nom")]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Nom")]
    #[ViewSection('main')]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Attribute\ResultSet("Description", blacklist: true)]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Description")]
    #[ViewSection('main')]
    private ?string $description = null;

    #[ORM\Column]
    #[Attribute\ResultSet("Public", display: false)]
    #[Groups(['api', 'index', 'meilisearch', 'csv'])]
    #[SerializedName("Public")]
    private ?bool $public = null;

    #[ORM\OneToMany(mappedBy: 'serviceLevel', targetEntity: ServiceLevelFile::class, orphanRemoval: true)]
    private Collection $serviceLevelFiles;

    #[ORM\ManyToMany(targetEntity: Label::class, inversedBy: 'serviceLevels', fetch: 'EAGER')]
    #[Groups(['api', 'index'])]
    private Collection $labels;

    #[ORM\OneToMany(mappedBy: 'serviceLevel', targetEntity: Activity::class, orphanRemoval: true)]
    #[ORM\OrderBy(['id' => 'ASC'])]
    #[Groups(['api'])]
    private Collection $activities;

    public function __construct()
    {
        $this->labels = new ArrayCollection();
        $this->serviceLevelFiles = new ArrayCollection();
        $this->activities = new ArrayCollection();
    }

    #[Override]
    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    #[Override]
    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    #[Groups(['index_public'])]
    #[Attribute\ResultSet("Tenant")]
    #[SerializedName("Tenant")]
    public function getTenantName(): string
    {
        return $this->getTenant()->getName();
    }

    #[Override]
    public function setTenant(?Tenant $tenant): static
    {
        $this->tenant = $tenant;
        return $this;
    }

    #[Override]
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): static
    {
        $this->identifier = $identifier;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    #[Override]
    public function isPublic(): bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): static
    {
        $this->public = $public;
        return $this;
    }

    /**
     * @return Collection<int, ServiceLevelFile>
     */
    public function getServiceLevelFiles(): Collection
    {
        return $this->serviceLevelFiles;
    }

    public function addServiceLevelFile(ServiceLevelFile $serviceLevelFile): static
    {
        if (!$this->serviceLevelFiles->contains($serviceLevelFile)) {
            $this->serviceLevelFiles->add($serviceLevelFile);
            $serviceLevelFile->setServiceLevel($this);
        }
        return $this;
    }

    public function removeServiceLevelFile(ServiceLevelFile $serviceLevelFile): static
    {
        if ($this->serviceLevelFiles->removeElement($serviceLevelFile)) {
            if ($serviceLevelFile->getServiceLevel() === $this) {
                $serviceLevelFile->setServiceLevel(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, Activity>
     */
    #[Override]
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    #[Override]
    public function addActivity(Activity $activity): static
    {
        if (!$this->activities->contains($activity)) {
            $this->activities->add($activity);
            $activity->setServiceLevel($this);
        }
        return $this;
    }

    public function removeActivity(Activity $activity): static
    {
        if ($this->activities->removeElement($activity)) {
            if ($activity->getServiceLevel() === $this) {
                $activity->setServiceLevel(null);
            }
        }
        return $this;
    }

    #[Override]
    public function getActivityName(): string
    {
        return sprintf("le niveau de service %s", $this->name);
    }

    #[Override]
    #[Groups(['meilisearch', 'api'])]
    #[Attribute\ResultSet("Baseurl du tenant")]
    public function getTenantUrl(): ?string
    {
        return $this->getTenant()?->getBaseurl();
    }

    #[Override]
    public function getFiles(): array
    {
        return $this->getServiceLevelFiles()->toArray();
    }

    #[Attribute\ResultSet("Fichiers")]
    #[Groups(['csv'])]
    public function getFilesForCsv(): string
    {
        return implode(
            "\n",
            array_map(
                fn(ServiceLevelFile $e) => $e->getFileName(),
                $this->getFiles()
            )
        );
    }

    #[Override]
    public function getActivityScope(): ?Tenant
    {
        return $this->tenant;
    }

    #[Attribute\ResultSet("Public")]
    #[Groups(['view'])]
    #[SerializedName("Public")]
    #[ViewSection('other')]
    public function getReadablePublic(): string
    {
        return $this->public ? "Oui" : "Non";
    }
}
