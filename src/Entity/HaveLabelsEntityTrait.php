<?php

namespace App\Entity;

use App\Entity\Attribute\ViewSection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

trait HaveLabelsEntityTrait
{
    /**
     * @return Collection<int, Label>
     */
    #[Groups(['api'])]
    public function getLabels(): Collection
    {
        return $this->labels;
    }

    public function addLabel(Label $label): static
    {
        if (!$this->labels->contains($label)) {
            $this->labels->add($label);
        }
        return $this;
    }

    public function removeLabel(Label $label): static
    {
        $this->labels->removeElement($label);
        return $this;
    }

    #[Groups(['meilisearch'])]
    #[Attribute\ResultSet("Étiquettes")]
    public function getFlattenLabels(): array
    {
        return array_map(
            fn(Label $label) => $label->getConcatLabelGroupName(),
            $this->getLabels()->toArray()
        );
    }

    #[Groups(['view'])]
    #[ViewSection('other')]
    #[SerializedName("Étiquettes")]
    public function getHtmlLabels()
    {
        $dataLabels = htmlspecialchars(json_encode($this->labels->toArray()));
        return '<div data-render-labels="' . $dataLabels . '"></div>';
    }

    #[Groups(['csv'])]
    #[Attribute\ResultSet("Étiquettes")]
    public function getLabelNamesStringForCsv(): string
    {
        return implode(
            "\n",
            array_map(
                fn(Label $f) => $f->getName(),
                $this->getLabels()->toArray()
            )
        );
    }
}
