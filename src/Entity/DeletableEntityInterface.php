<?php

namespace App\Entity;

interface DeletableEntityInterface
{
    public function getDeletable(): bool;
}
