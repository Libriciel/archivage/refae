<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Repository\DocumentRuleImportSessionRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: DocumentRuleImportSessionRepository::class)]
class DocumentRuleImportSession
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['api', 'index', 'view'])]
    private ?LazyUuidFromString $id = null;

    #[ORM\ManyToOne(inversedBy: 'documentRuleImportSessions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\ManyToOne(inversedBy: 'documentRuleImportSessions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Tenant $tenant = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $column_types = null;

    #[ORM\OneToOne(inversedBy: 'documentRuleImportSession', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?File $csvFile = null;

    #[ORM\Column]
    private ?int $lastSeekOffset = null;

    #[ORM\ManyToOne(inversedBy: 'documentRuleImportSessions')]
    #[ORM\JoinColumn(onDelete: "SET NULL")]
    private ?Category $lastCategory = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private DateTimeInterface $created;

    #[ORM\Column(nullable: true)]
    private ?int $lastCategoryLevel = null;

    #[ORM\Column(nullable: true)]
    private ?int $lastRow = null;

    public function __construct()
    {
        $this->created = new DateTime();
    }

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): static
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getColumnTypes(): ?string
    {
        return $this->column_types;
    }

    public function setColumnTypes(string $column_types): static
    {
        $this->column_types = $column_types;

        return $this;
    }

    public function getCsvFile(): ?File
    {
        return $this->csvFile;
    }

    public function setCsvFile(File $csvFile): static
    {
        $this->csvFile = $csvFile;

        return $this;
    }

    public function getLastSeekOffset(): ?int
    {
        return $this->lastSeekOffset;
    }

    public function setLastSeekOffset(int $lastSeekOffset): static
    {
        $this->lastSeekOffset = $lastSeekOffset;

        return $this;
    }

    public function getLastCategory(): ?Category
    {
        return $this->lastCategory;
    }

    public function setLastCategory(?Category $lastCategory): static
    {
        $this->lastCategory = $lastCategory;

        return $this;
    }

    public function getCreated(): ?DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(DateTimeInterface $created): static
    {
        $this->created = $created;

        return $this;
    }

    public function getLastCategoryLevel(): ?int
    {
        return $this->lastCategoryLevel;
    }

    public function setLastCategoryLevel(?int $lastCategoryLevel): static
    {
        $this->lastCategoryLevel = $lastCategoryLevel;

        return $this;
    }

    public function getLastRow(): ?int
    {
        return $this->lastRow;
    }

    public function setLastRow(?int $lastRow): static
    {
        $this->lastRow = $lastRow;

        return $this;
    }
}
