<?php

namespace App\Entity;

interface OneTenantOrNoneIntegrityEntityInterface
{
    public function getTenant(): ?Tenant;
}
