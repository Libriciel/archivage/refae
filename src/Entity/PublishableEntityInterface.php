<?php

namespace App\Entity;

interface PublishableEntityInterface extends VersionableEntityInterface
{
    public function isPublic(): bool;
}
