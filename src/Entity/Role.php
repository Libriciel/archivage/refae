<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Repository\RoleRepository;
use App\Service\Permission;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: RoleRepository::class)]
class Role
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    private ?LazyUuidFromString $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\ManyToOne(targetEntity: Tenant::class, inversedBy: 'roles')]
    #[ORM\JoinColumn(nullable: true)]
    private ?Tenant $tenant = null;

    #[ORM\OneToMany(mappedBy: 'role', targetEntity: AccessUser::class, orphanRemoval: true)]
    private Collection $accessUsers;

    #[ORM\ManyToMany(targetEntity: Privilege::class, mappedBy: 'roles')]
    private Collection $privileges;

    public function __construct()
    {
        $this->accessUsers = new ArrayCollection();
        $this->privileges = new ArrayCollection();
    }

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): static
    {
        $this->tenant = $tenant;

        return $this;
    }

    /**
     * @return Collection<int, AccessUser>
     */
    public function getAccessUsers(): Collection
    {
        return $this->accessUsers;
    }

    public function addAccessUser(AccessUser $accessUser): static
    {
        if (!$this->accessUsers->contains($accessUser)) {
            $this->accessUsers->add($accessUser);
            $accessUser->setRole($this);
        }
        return $this;
    }

    public function removeAccessUser(AccessUser $accessUser): static
    {
        if ($this->accessUsers->removeElement($accessUser)) {
            if ($accessUser->getRole() === $this) {
                $accessUser->setRole(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, Privilege>
     */
    public function getPrivileges(): Collection
    {
        return $this->privileges;
    }

    public function addPrivilege(Privilege $privilege): static
    {
        if (!$this->privileges->contains($privilege)) {
            $this->privileges->add($privilege);
            $privilege->addRole($this);
        }

        return $this;
    }

    public function removePrivilege(Privilege $privilege): static
    {
        if ($this->privileges->removeElement($privilege)) {
            $privilege->removeRole($this);
        }

        return $this;
    }

    #[Groups(['session'])]
    public function getCommonName(): ?string
    {
        if ($this->getTenant() === null) {
            $name = $this->name;
            return Permission::$commonNames['roles'][$name] ?? $name;
        }

        return $this->name;
    }
}
