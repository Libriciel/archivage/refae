<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;

interface TenantsIntegrityEntityInterface
{
    /**
     * @return Collection<int, Tenant>
     */
    public function getTenants(): Collection;
}
