<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Repository\CronRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Ramsey\Uuid\Lazy\LazyUuidFromString;

#[ORM\Entity(repositoryClass: CronRepository::class)]
class Cron
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    private ?LazyUuidFromString $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;

    #[ORM\Column(length: 255)]
    private ?string $service = null;

    #[ORM\Column(length: 255)]
    private ?string $frequency = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?DateTimeInterface $next = null;

    #[ORM\OneToMany(mappedBy: 'cron', targetEntity: CronExecution::class, orphanRemoval: true)]
    private Collection $cronExecutions;

    public function __construct()
    {
        $this->cronExecutions = new ArrayCollection();
    }

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getService(): ?string
    {
        return $this->service;
    }

    public function setService(string $service): static
    {
        $this->service = $service;

        return $this;
    }

    public function getFrequency(): ?string
    {
        return $this->frequency;
    }

    public function setFrequency(string $frequency): static
    {
        $this->frequency = $frequency;

        return $this;
    }

    public function getNext(): ?DateTimeInterface
    {
        return $this->next;
    }

    public function setNext(DateTimeInterface $next): static
    {
        $this->next = $next;

        return $this;
    }

    /**
     * @return Collection<int, CronExecution>
     */
    public function getCronExecutions(): Collection
    {
        return $this->cronExecutions;
    }

    public function addCronExecution(CronExecution $cronExecution): static
    {
        if (!$this->cronExecutions->contains($cronExecution)) {
            $this->cronExecutions->add($cronExecution);
            $cronExecution->setCron($this);
        }

        return $this;
    }

    public function removeCronExecution(CronExecution $cronExecution): static
    {
        if ($this->cronExecutions->removeElement($cronExecution)) {
            // set the owning side to null (unless already changed)
            if ($cronExecution->getCron() === $this) {
                $cronExecution->setCron(null);
            }
        }

        return $this;
    }
}
