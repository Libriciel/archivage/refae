<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Entity\Attribute\ViewSection;
use App\Repository\UserCreationRequestRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: UserCreationRequestRepository::class)]
class UserCreationRequest implements OneTenantIntegrityEntityInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['index', 'index_admin'])]
    private ?LazyUuidFromString $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Groups(['index', 'index_admin', 'view'])]
    #[ViewSection('main')]
    #[SerializedName("Identifiant de connexion")]
    #[Attribute\ResultSet("Identifiant de connexion")]
    private ?string $username = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['index', 'index_admin', 'view'])]
    #[ViewSection('main')]
    #[SerializedName("Nom")]
    #[Attribute\ResultSet("Nom")]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Assert\Email]
    #[Groups(['index', 'index_admin', 'view'])]
    #[ViewSection('main')]
    #[SerializedName("E-mail")]
    #[Attribute\ResultSet("e-mail")]
    private ?string $email = null;

    #[ORM\Column(length: 255)]
    #[Groups(['index', 'index_admin', 'view'])]
    #[ViewSection('main')]
    #[SerializedName("Ip")]
    #[Attribute\ResultSet("ip")]
    private ?string $ip = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['index', 'index_admin', 'view'])]
    #[SerializedName("Date de création")]
    #[Attribute\ResultSet("date de création")]
    private DateTimeInterface $created;

    #[ORM\ManyToOne(inversedBy: 'userCreationRequests')]
    private ?Tenant $tenant = null;

    #[ORM\Column(length: 255)]
    private string $emailToken;

    #[ORM\Column]
    private ?bool $emailVerified = false;

    public function __construct()
    {
        $this->emailToken = bin2hex(random_bytes(32));
        $this->created = new DateTime();
    }

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): static
    {
        $this->username = $username;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): static
    {
        $this->ip = $ip;

        return $this;
    }

    public function getCreated(): ?DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(DateTimeInterface $created): static
    {
        $this->created = $created;

        return $this;
    }

    #[Override]
    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): static
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getEmailToken(): ?string
    {
        return $this->emailToken;
    }

    public function setEmailToken(string $emailToken): static
    {
        $this->emailToken = $emailToken;

        return $this;
    }

    public function isEmailVerified(): ?bool
    {
        return $this->emailVerified;
    }

    public function setEmailVerified(bool $emailVerified): static
    {
        $this->emailVerified = $emailVerified;

        return $this;
    }

    #[Groups(['index_admin'])]
    public function getTenantName(): string
    {
        return $this->getTenant()?->getName() ?? '';
    }

    #[Groups(['action'])]
    public function getCommonName(): ?string
    {
        return $this->getName() ?: $this->getUsername();
    }
}
