<?php

namespace App\Entity;

interface HaveFilesInterface
{
    public function getFiles(): array;
}
