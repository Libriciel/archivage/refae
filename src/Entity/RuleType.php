<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Entity\Attribute\ViewSection;
use App\Repository\RuleTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

#[ORM\Entity(repositoryClass: RuleTypeRepository::class)]
#[UniqueEntity(
    ['identifier', 'tenant'],
    "Ce type de règle de gestion existe déjà",
    repositoryMethod: 'getByIdentifier'
)]
class RuleType implements
    ActivitySubjectEntityInterface,
    EditableEntityInterface,
    DeletableEntityInterface,
    OneTenantOrNoneIntegrityEntityInterface
{
    use ArrayEntityTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['api', 'meilisearch', 'index'])]
    private ?LazyUuidFromString $id = null;

    #[ORM\ManyToOne(inversedBy: 'ruleTypes')]
    #[ORM\JoinColumn(nullable: true)]
    private ?Tenant $tenant = null;

    #[ORM\Column(length: 255)]
    #[Attribute\ResultSet("Identifiant")]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Identifiant")]
    #[ViewSection('main')]
    private ?string $identifier = null;

    #[ORM\Column(length: 255)]
    #[Attribute\ResultSet("Nom")]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Nom")]
    #[ViewSection('main')]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Attribute\ResultSet("description", blacklist: true)]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Description")]
    #[ViewSection('main')]
    private ?string $description = null;

    #[ORM\OneToMany(mappedBy: 'ruleType', targetEntity: ManagementRule::class, orphanRemoval: true)]
    private Collection $managementRules;

    #[ORM\OneToMany(mappedBy: 'ruleType', targetEntity: Activity::class, orphanRemoval: true)]
    #[ORM\OrderBy(['id' => 'ASC'])]
    private Collection $activities;

    public function __construct()
    {
        $this->managementRules = new ArrayCollection();
        $this->activities = new ArrayCollection();
    }

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    #[Override]
    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): static
    {
        $this->tenant = $tenant;
        return $this;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): static
    {
        $this->identifier = $identifier;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Collection<int, ManagementRule>
     */
    public function getManagementRules(): Collection
    {
        return $this->managementRules;
    }

    public function addManagementRule(ManagementRule $managementRule): static
    {
        if (!$this->managementRules->contains($managementRule)) {
            $this->managementRules->add($managementRule);
            $managementRule->setRuleType($this);
        }
        return $this;
    }

    public function removeManagementRule(ManagementRule $managementRule): static
    {
        if ($this->managementRules->removeElement($managementRule)) {
            // set the owning side to null (unless already changed)
            if ($managementRule->getRuleType() === $this) {
                $managementRule->setRuleType(null);
            }
        }
        return $this;
    }

    #[Groups(['action'])]
    #[Override]
    public function getEditable(): bool
    {
        return $this->getTenant() !== null;
    }

    #[Groups(['action'])]
    #[Override]
    public function getDeletable(): bool
    {
        return $this->getTenant() !== null
            && $this->getManagementRules()->count() === 0;
    }

    #[Attribute\ResultSet("Est global ?", type: 'bool', display: false)]
    #[Groups(['api', 'index', 'view'])]
    #[SerializedName("Est global ?")]
    #[ViewSection('other')]
    public function isGlobal(): bool
    {
        return $this->getTenant() === null;
    }

    /**
     * @return Collection<int, Activity>
     */
    #[Override]
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    #[Override]
    public function addActivity(Activity $activity): static
    {
        if (!$this->activities->contains($activity)) {
            $this->activities->add($activity);
            $activity->setRuleType($this);
        }
        return $this;
    }

    public function removeActivity(Activity $activity): static
    {
        if ($this->activities->removeElement($activity)) {
            // set the owning side to null (unless already changed)
            if ($activity->getRuleType() === $this) {
                $activity->setRuleType(null);
            }
        }
        return $this;
    }

    #[Override]
    public function getActivityName(): string
    {
        return sprintf("le type de règle %s", $this->name);
    }

    #[Override]
    public function getActivityScope(): ?Tenant
    {
        return $this->tenant;
    }
}
