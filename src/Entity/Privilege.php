<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Repository\PrivilegeRepository;
use App\Service\Permission;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Ramsey\Uuid\Lazy\LazyUuidFromString;

#[ORM\Entity(repositoryClass: PrivilegeRepository::class)]
class Privilege
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    private ?LazyUuidFromString $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\ManyToOne(inversedBy: 'privileges')]
    private ?Tenant $tenant = null;

    #[ORM\ManyToMany(targetEntity: Role::class, inversedBy: 'privileges')]
    private Collection $roles;

    #[ORM\OneToMany(mappedBy: 'privilege', targetEntity: PermissionPrivilege::class, orphanRemoval: true)]
    private Collection $permissionPrivileges;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'privilegeUsers')]
    private ?self $privilege = null;

    #[ORM\OneToMany(mappedBy: 'privilege', targetEntity: self::class)]
    private Collection $privilegeUsers;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
        $this->permissionPrivileges = new ArrayCollection();
        $this->privilegeUsers = new ArrayCollection();
    }

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): static
    {
        $this->tenant = $tenant;

        return $this;
    }

    /**
     * @return Collection<int, Role>
     */
    public function getRole(): Collection
    {
        return $this->roles;
    }

    public function addRole(Role $role): static
    {
        if (!$this->roles->contains($role)) {
            $this->roles->add($role);
        }

        return $this;
    }

    public function removeRole(Role $role): static
    {
        $this->roles->removeElement($role);

        return $this;
    }

    /**
     * @return Collection<int, PermissionPrivilege>
     */
    public function getPermissionPrivileges(): Collection
    {
        return $this->permissionPrivileges;
    }

    public function addPermissionPrivilege(PermissionPrivilege $permissionPrivilege): static
    {
        if (!$this->permissionPrivileges->contains($permissionPrivilege)) {
            $this->permissionPrivileges->add($permissionPrivilege);
            $permissionPrivilege->setPrivilege($this);
        }
        return $this;
    }

    public function removePermissionPrivilege(PermissionPrivilege $permissionPrivilege): static
    {
        if ($this->permissionPrivileges->removeElement($permissionPrivilege)) {
            if ($permissionPrivilege->getPrivilege() === $this) {
                $permissionPrivilege->setPrivilege(null);
            }
        }
        return $this;
    }

    public function getPrivilege(): ?self
    {
        return $this->privilege;
    }

    public function setPrivilege(?self $privilege): static
    {
        $this->privilege = $privilege;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getPrivilegeUsers(): Collection
    {
        return $this->privilegeUsers;
    }

    public function addPrivilegeUser(self $privilegeUser): static
    {
        if (!$this->privilegeUsers->contains($privilegeUser)) {
            $this->privilegeUsers->add($privilegeUser);
            $privilegeUser->setPrivilege($this);
        }
        return $this;
    }

    public function removePrivilegeUser(self $privilegeUser): static
    {
        if ($this->privilegeUsers->removeElement($privilegeUser)) {
            if ($privilegeUser->getPrivilege() === $this) {
                $privilegeUser->setPrivilege(null);
            }
        }
        return $this;
    }

    public function getCommonName(): ?string
    {
        $name = $this->name;
        return Permission::$commonNames['privileges'][$name] ?? $name;
    }
}
