<?php

namespace App\Entity;

use Attribute;

#[Attribute]
readonly class ActivityAttribute
{
    public function __construct(public string $classname)
    {
    }
}
