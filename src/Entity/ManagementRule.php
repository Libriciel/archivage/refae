<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\OpenApi\Model;
use App\ApiPlatform\LabelFilter;
use App\ApiPlatform\RuleTypeFilter;
use App\Doctrine\UuidGenerator;
use App\Entity\Attribute\ViewSection;
use App\Repository\ManagementRuleRepository;
use App\Validator\SameTenantOrNone;
use DateInterval;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ManagementRuleRepository::class)]
#[UniqueEntity(['tenant', 'identifier', 'version'], "Cette règle de gestion existe déjà")]
#[ApiResource(
    operations: [
        new GetCollection(
            uriTemplate: '/{tenant_url}/management-rule-published',
            uriVariables: [],
            routeName: 'api_management_rule_published_list',
            openapi: new Model\Operation(
                summary: "Liste des règles de gestion publiées",
                description: "Liste des règles de gestion publiées",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
        new Get(
            uriTemplate: '/{tenant_url}/management-rule-published/{id}',
            routeName: 'api_management_rule_published_id',
            openapi: new Model\Operation(
                summary: "Récupère une règle de gestion publiée",
                description: "Récupère une règles de gestion publiée",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                    new Model\Parameter(
                        name: 'id',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
        new Get(
            uriTemplate: '/{tenant_url}/management-rule-by-identifier/{identifier}',
            routeName: 'api_management_rule_published_identifier',
            openapi: new Model\Operation(
                summary: "Récupère une règle de gestion par identifiant",
                description: "Récupère une règle de gestion par identifiant",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                    new Model\Parameter(
                        name: 'identifier',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
    ],
    normalizationContext: [
        'groups' => ['api'],
    ]
)]
#[SameTenantOrNone(RuleType::class)]
#[ApiFilter(SearchFilter::class, properties: ['identifier'])]
#[ApiFilter(LabelFilter::class, properties: ['labels'])]
#[ApiFilter(RuleTypeFilter::class, properties: ['ruleType'])]
class ManagementRule implements
    ActivitySubjectEntityInterface,
    EditableEntityInterface,
    DeletableEntityInterface,
    HaveFilesInterface,
    HaveLabelsEntityInterface,
    MeilisearchIndexedInterface,
    OneTenantIntegrityEntityInterface,
    PublishableEntityInterface,
    StateMachineEntityInterface
{
    use ArrayEntityTrait;
    use HaveLabelsEntityTrait;
    use VersionableEntityTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['api', 'meilisearch', 'index'])]
    private ?LazyUuidFromString $id = null;

    #[ORM\ManyToOne(targetEntity: Tenant::class, inversedBy: 'managementRules')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Tenant $tenant = null;

    #[ORM\OneToMany(mappedBy: 'managementRule', targetEntity: ManagementRuleFile::class, orphanRemoval: true)]
    private Collection $managementRuleFiles;

    #[ORM\ManyToMany(targetEntity: Label::class, inversedBy: 'managementRules')]
    #[Groups(['api', 'index'])]
    private Collection $labels;

    #[ORM\OneToMany(mappedBy: 'managementRule', targetEntity: Activity::class, orphanRemoval: true)]
    #[Groups(['api'])]
    private Collection $activities;

    #[ORM\ManyToOne(inversedBy: 'managementRules')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['api'])]
    private ?RuleType $ruleType = null;

    #[ORM\Column(length: 255)]
    #[Attribute\ResultSet("Identifiant")]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Identifiant")]
    #[ViewSection('main')]
    private ?string $identifier = null;

    #[ORM\Column(length: 255)]
    #[Attribute\ResultSet("Nom")]
    #[SerializedName("Nom")]
    #[ViewSection('main')]
    #[Groups(['api', 'index', 'meilisearch', 'csv'])]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Attribute\ResultSet("Description", blacklist: true)]
    #[SerializedName("Description")]
    #[ViewSection('main')]
    #[Groups(['api', 'index', 'meilisearch', 'csv'])]
    private ?string $description = null;

    #[ORM\Column(length: 255)]
    #[Assert\Regex(pattern: '/^P\d+(Y(\d+M)?(\d+D)?|M(\d+D)?|D)$/', message: "Erreur de syntaxe")]
    #[Groups(['api', 'view', 'csv'])]
    #[SerializedName("Durée en xsd:duration")]
    #[ViewSection('other')]
    private ?string $duration = null;

    #[ORM\Column]
    #[Attribute\ResultSet("Publique", display: false)]
    #[Groups(['api', 'index', 'meilisearch', 'csv'])]
    #[SerializedName("Publique")]
    private ?bool $public = null;

    #[ORM\OneToMany(mappedBy: 'storageRule', targetEntity: DocumentRule::class, orphanRemoval: true)]
    private Collection $storageDocumentRules;

    #[ORM\OneToMany(mappedBy: 'appraisalRule', targetEntity: DocumentRule::class, orphanRemoval: true)]
    private Collection $appraisalDocumentRules;

    #[ORM\OneToMany(mappedBy: 'accessRule', targetEntity: DocumentRule::class, orphanRemoval: true)]
    private Collection $accessDocumentRules;

    public function __construct()
    {
        $this->labels = new ArrayCollection();
        $this->managementRuleFiles = new ArrayCollection();
        $this->storageDocumentRules = new ArrayCollection();
        $this->appraisalDocumentRules = new ArrayCollection();
        $this->accessDocumentRules = new ArrayCollection();
        $this->activities = new ArrayCollection();
    }

    #[Override]
    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    #[Override]
    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    #[Groups(['index_public'])]
    #[Attribute\ResultSet("Tenant")]
    #[SerializedName("Tenant")]
    public function getTenantName(): string
    {
        return $this->getTenant()->getName();
    }

    #[Override]
    public function setTenant(?Tenant $tenant): static
    {
        $this->tenant = $tenant;
        return $this;
    }

    /**
     * @return Collection<int, ManagementRuleFile>
     */
    public function getManagementRuleFiles(): Collection
    {
        return $this->managementRuleFiles;
    }

    public function addManagementRuleFile(ManagementRuleFile $managementRuleFile): static
    {
        if (!$this->managementRuleFiles->contains($managementRuleFile)) {
            $this->managementRuleFiles->add($managementRuleFile);
            $managementRuleFile->setManagementRule($this);
        }
        return $this;
    }

    public function removeManagementRuleFile(ManagementRuleFile $managementRuleFile): static
    {
        if ($this->managementRuleFiles->removeElement($managementRuleFile)) {
            if ($managementRuleFile->getManagementRule() === $this) {
                $managementRuleFile->setManagementRule(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, DocumentRule>
     */
    public function getStorageDocumentRules(): Collection
    {
        return $this->storageDocumentRules;
    }

    public function addStorageDocumentRule(DocumentRule $storageDocumentRule): static
    {
        if (!$this->storageDocumentRules->contains($storageDocumentRule)) {
            $this->storageDocumentRules->add($storageDocumentRule);
            $storageDocumentRule->setStorageRule($this);
        }
        return $this;
    }

    public function removeStorageDocumentRule(DocumentRule $storageDocumentRule): static
    {
        if ($this->storageDocumentRules->removeElement($storageDocumentRule)) {
            if ($storageDocumentRule->getStorageRule() === $this) {
                $storageDocumentRule->setStorageRule(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, DocumentRule>
     */
    public function getAppraisaisalDocumentRules(): Collection
    {
        return $this->appraisalDocumentRules;
    }

    public function addAppraisaisalDocumentRule(DocumentRule $appraisalDocumentRule): static
    {
        if (!$this->appraisalDocumentRules->contains($appraisalDocumentRule)) {
            $this->appraisalDocumentRules->add($appraisalDocumentRule);
            $appraisalDocumentRule->setAppraisalRule($this);
        }
        return $this;
    }

    public function removeAppraisaisalDocumentRule(DocumentRule $appraisalDocumentRule): static
    {
        if ($this->appraisalDocumentRules->removeElement($appraisalDocumentRule)) {
            if ($appraisalDocumentRule->getAppraisalRule() === $this) {
                $appraisalDocumentRule->setAppraisalRule(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, DocumentRule>
     */
    public function getAccessDocumentRules(): Collection
    {
        return $this->accessDocumentRules;
    }

    public function addAccessDocumentRule(DocumentRule $accessDocumentRule): static
    {
        if (!$this->accessDocumentRules->contains($accessDocumentRule)) {
            $this->accessDocumentRules->add($accessDocumentRule);
            $accessDocumentRule->setAccessRule($this);
        }
        return $this;
    }

    public function removeAccessDocumentRule(DocumentRule $accessDocumentRule): static
    {
        if ($this->accessDocumentRules->removeElement($accessDocumentRule)) {
            if ($accessDocumentRule->getAccessRule() === $this) {
                $accessDocumentRule->setAccessRule(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, Activity>
     */
    #[Override]
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    #[Override]
    public function addActivity(Activity $activity): static
    {
        if (!$this->activities->contains($activity)) {
            $this->activities->add($activity);
            $activity->setManagementRule($this);
        }

        return $this;
    }

    public function removeActivity(Activity $activity): static
    {
        if ($this->activities->removeElement($activity)) {
            // set the owning side to null (unless already changed)
            if ($activity->getManagementRule() === $this) {
                $activity->setManagementRule(null);
            }
        }

        return $this;
    }

    public function getRuleType(): ?RuleType
    {
        return $this->ruleType;
    }

    #[Attribute\ResultSet("Type de règle")]
    #[Groups(['index', 'csv'])]
    #[SerializedName("Type de règle")]
    public function getRuleTypeName(): string
    {
        return $this->getRuleType()?->getName() ?? '';
    }

    #[Groups(['api'])]
    #[SerializedName("ruleType")]
    public function getRuleTypeIdentifier(): string
    {
        return $this->getRuleType()?->getIdentifier() ?? '';
    }

    public function setRuleType(?RuleType $ruleType): static
    {
        $this->ruleType = $ruleType;
        return $this;
    }

    #[Override]
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): static
    {
        $this->identifier = $identifier;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    public function getDuration(): string
    {
        return $this->duration;
    }

    #[Attribute\ResultSet("Durée")]
    #[Groups(['index', 'meilisearch', 'csv'])]
    #[SerializedName("Durée")]
    public function getHumanizedDuration(): string
    {
        $date = new DateInterval($this->getDuration());
        $str = '';
        if ($date->y) {
            $str .= sprintf($date->y > 1 ? "%d ans" : "%d an", $date->y);
        }
        if ($date->m) {
            $str .= sprintf(" %d mois", $date->m);
        }
        if ($date->d) {
            $str .= sprintf($date->d > 1 ? " %d jours" : " %d jour", $date->d);
        }
        return $str ?: '0 an';
    }

    public function setDuration(string $duration): static
    {
        $this->duration = $duration;
        return $this;
    }

    #[Override]
    public function isPublic(): bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): static
    {
        $this->public = $public;
        return $this;
    }

    #[Override]
    #[Groups(['meilisearch'])]
    #[Attribute\ResultSet("Baseurl du tenant")]
    public function getTenantUrl(): ?string
    {
        return $this->getTenant()?->getBaseurl();
    }

    #[Attribute\ResultSet("Fichiers")]
    #[Groups(['csv'])]
    public function getFilesForCsv(): string
    {
        return implode(
            "\n",
            array_map(
                fn(ManagementRuleFile $e) => $e->getFileName(),
                $this->getManagementRuleFiles()->toArray()
            )
        );
    }

    #[Override]
    public function getActivityName(): string
    {
        return sprintf("la règle de gestion %s", $this->name);
    }

    #[Override]
    public function getActivityScope(): ?Tenant
    {
        return $this->tenant;
    }

    #[Override]
    public function getFiles(): array
    {
        return $this->getManagementRuleFiles()->toArray();
    }

    #[Attribute\ResultSet("Publique")]
    #[Groups(['view'])]
    #[SerializedName("Publique")]
    #[ViewSection('other')]
    public function getReadablePublic(): string
    {
        return $this->public ? "Oui" : "Non";
    }
}
