<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Post;
use ApiPlatform\OpenApi\Model;
use App\Doctrine\UuidGenerator;
use App\Repository\ApiTokenRepository;
use ArrayObject;
use DateTime;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ApiTokenRepository::class)]
#[ApiResource(
    operations: [
        new Post(
            routeName: 'api_token_create',
            openapi: new Model\Operation(
                summary: "Crée un jeton d'authentification",
                description: "Crée un jeton d'authentification",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                    new Model\Parameter(
                        name: 'Authorization',
                        in: 'header',
                        description: 'Authorization header (optionnel - à la place du formulaire)',
                        required: false,
                        schema: [
                            'type' => 'string'
                        ],
                    ),
                ],
                requestBody: new Model\RequestBody(
                    content: new ArrayObject([
                        'multipart/form-data' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'username' => [
                                        'type' => 'string',
                                    ],
                                    'password' => [
                                        'type' => 'string',
                                        'format' => 'password',
                                    ],
                                ],
                            ],
                        ],
                    ])
                )
            ),
        ),
        new Delete(
            uriVariables: [],
            routeName: 'api_token_delete',
            openapi: new Model\Operation(
                summary: "Supprime le jeton d'authentification",
                description: "Supprime le jeton d'authentification",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
    ],
    normalizationContext: [
        'groups' => ['api'],
    ]
)]
class ApiToken
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[ORM\Column(type: 'uuid', unique: true)]
    private ?string $id = null;

    #[ORM\ManyToOne(inversedBy: 'apiTokens')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\ManyToOne(inversedBy: 'apiTokens')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Tenant $tenant = null;

    #[ORM\Column(length: 255)]
    private ?string $token;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?DateTimeInterface $lastAccess;

    public function __construct()
    {
        $this->token = bin2hex(random_bytes(32));
        $this->lastAccess = new DateTime();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): static
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): static
    {
        $this->token = $token;

        return $this;
    }

    public function getLastAccess(): ?DateTimeInterface
    {
        return $this->lastAccess;
    }

    public function setLastAccess(DateTimeInterface $lastAccess): static
    {
        $this->lastAccess = $lastAccess;

        return $this;
    }
}
