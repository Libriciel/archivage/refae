<?php

namespace App\Entity;

use App\Entity\Attribute\ViewSection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

trait VersionableEntityTrait
{
    public static $initialState = VersionableEntityInterface::S_EDITING;

    public static $stateTranslations = [
            VersionableEntityInterface::S_EDITING => "En cours d'édition",
            VersionableEntityInterface::S_PUBLISHED => "Publié",
            VersionableEntityInterface::S_DEPRECATED => "Déprécié",
            VersionableEntityInterface::S_DEACTIVATED => "Désactivé",
            VersionableEntityInterface::S_REVOKED => "Révoqué",
        ];

    #[ORM\Column(length: 255)]
    #[Assert\Choice(
        choices: [
            VersionableEntityInterface::S_EDITING,
            VersionableEntityInterface::S_PUBLISHED,
            VersionableEntityInterface::S_DEPRECATED,
            VersionableEntityInterface::S_DEACTIVATED,
            VersionableEntityInterface::S_REVOKED,
        ],
        message: 'Choose a valid state.'
    )]
    #[Attribute\IgnoreActivity]
    #[Groups(['api', 'index'])]
    private ?string $state = null;

    #[ORM\Column]
    #[Attribute\ResultSet("Version")]
    #[Groups(['api', 'index', 'meilisearch', 'csv'])]
    private ?int $version = null;
    #[Attribute\ResultSet("Versions précédentes")]
    #[Groups(['api'])]
    private ?array $previousVersions = null;

    #[Groups(['action'])]
    #[SerializedName('deletable')]
    public function getDeletable(): bool
    {
        return $this->state === VersionableEntityInterface::S_EDITING;
    }

    #[Groups(['action'])]
    #[SerializedName('editable')]
    public function getEditable(): bool
    {
        return $this->state === VersionableEntityInterface::S_EDITING;
    }

    #[Groups(['action'])]
    #[SerializedName('versionable')]
    public function getVersionable(): bool
    {
        return in_array(
            $this->state,
            [VersionableEntityInterface::S_PUBLISHED, VersionableEntityInterface::S_DEACTIVATED]
        );
    }

    #[Groups(['action'])]
    #[SerializedName('publishable')]
    public function getPublishable(): bool
    {
        return $this->state === VersionableEntityInterface::S_EDITING;
    }

    #[Groups(['action'])]
    #[SerializedName('deactivatable')]
    public function getDeactivatable(): bool
    {
        return $this->state === VersionableEntityInterface::S_PUBLISHED;
    }

    #[Groups(['action'])]
    #[SerializedName('activable')]
    public function getActivatable(): bool
    {
        return $this->state === VersionableEntityInterface::S_DEACTIVATED;
    }

    #[Groups(['action'])]
    #[SerializedName('revokable')]
    public function getRevokable(): bool
    {
        return in_array(
            $this->state,
            [VersionableEntityInterface::S_PUBLISHED, VersionableEntityInterface::S_DEACTIVATED]
        );
    }

    #[Groups(['action'])]
    #[SerializedName('recoverable')]
    public function getRecoverable(): bool
    {
        return $this->state === VersionableEntityInterface::S_REVOKED;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): static
    {
        $this->state = $state;
        return $this;
    }

    public function getVersion(): ?int
    {
        return $this->version;
    }

    public function setVersion(int $version): static
    {
        $this->version = $version;
        return $this;
    }

    public function getInitialState(): string
    {
        return static::$initialState;
    }

    public function getTransitions(): array
    {
        return [
            VersionableEntityInterface::T_PUBLISH => [
                VersionableEntityInterface::S_EDITING => VersionableEntityInterface::S_PUBLISHED,
            ],
            VersionableEntityInterface::T_DEPRECIATE => [
                VersionableEntityInterface::S_PUBLISHED => VersionableEntityInterface::S_DEPRECATED,
                VersionableEntityInterface::S_DEACTIVATED => VersionableEntityInterface::S_DEPRECATED,
            ],
            VersionableEntityInterface::T_DEACTIVATE => [
                VersionableEntityInterface::S_PUBLISHED => VersionableEntityInterface::S_DEACTIVATED,
            ],
            VersionableEntityInterface::T_ACTIVATE => [
                VersionableEntityInterface::S_DEACTIVATED => VersionableEntityInterface::S_PUBLISHED,
            ],
            VersionableEntityInterface::T_REVOKE => [
                VersionableEntityInterface::S_DEACTIVATED => VersionableEntityInterface::S_REVOKED,
                VersionableEntityInterface::S_PUBLISHED => VersionableEntityInterface::S_REVOKED,
            ],
            VersionableEntityInterface::T_RECOVER => [
                VersionableEntityInterface::S_REVOKED => VersionableEntityInterface::S_PUBLISHED,
            ],
        ];
    }

    public function activityBypassAdd(): bool
    {
        return $this->getVersion() > 1;
    }

    public function setPreviousVersions(array $previousVersions): static
    {
        $this->previousVersions = $previousVersions;
        return $this;
    }

    public function getPreviousVersions(): array
    {
        return $this->previousVersions;
    }

    #[Attribute\ResultSet("État")]
    #[Groups(['index', 'meilisearch', 'view'])]
    #[SerializedName("État")]
    #[ViewSection('other')]
    public function getStateTranslation(): string
    {
        return $this->getStateTranslations()[$this->state] ?? '';
    }

    public function getStateTranslations(): array
    {
        return static::$stateTranslations;
    }

    public function getTransitionTranslations(): array
    {
        return [
            VersionableEntityInterface::T_PUBLISH => "publié",
            VersionableEntityInterface::T_ACTIVATE => "activé",
            VersionableEntityInterface::T_DEACTIVATE => "désactivé",
            VersionableEntityInterface::T_REVOKE => "révoqué",
            VersionableEntityInterface::T_DEPRECIATE => "déprécié",
            VersionableEntityInterface::T_RECOVER => "restauré",
        ];
    }
}
