<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Entity\Attribute\ViewSection;
use App\Repository\LabelGroupRepository;
use App\Service\AttributeExtractor;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use JsonSerializable;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Serializer\Attribute\Ignore;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: LabelGroupRepository::class)]
#[UniqueEntity(['tenant', 'name'], "Ce groupe existe déjà")]
class LabelGroup implements
    JsonSerializable,
    ViewEntityInterface,
    ActivitySubjectEntityInterface
{
    use ArrayEntityTrait;

    public const string BANNED_CHARSET = Label::BANNED_CHARSET;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['action'])]
    private ?LazyUuidFromString $id = null;

    #[ORM\ManyToOne(inversedBy: 'labelGroups')]
    #[ORM\JoinColumn(nullable: false)]
    #[Ignore]
    private ?Tenant $tenant = null;

    #[Assert\Regex(pattern: '/[~:\/!?,;]/', message: "Caractères interdits: ~:\/!?,;", match: false)]
    #[ORM\Column(length: 255)]
    #[Attribute\ResultSet("Nom")]
    #[Groups(['api', 'index', 'view', 'csv'])]
    #[SerializedName("Nom")]
    #[ViewSection('main')]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Attribute\Activity(label: "description")]
    #[Attribute\ResultSet("Description", display: false)]
    #[Groups(['api', 'index', 'view', 'csv'])]
    #[SerializedName("Description")]
    #[ViewSection('main')]
    private ?string $description = null;

    #[ORM\OneToMany(mappedBy: 'labelGroup', targetEntity: Activity::class, orphanRemoval: true)]
    #[Ignore]
    private Collection $activities;

    #[ORM\OneToMany(mappedBy: 'labelGroup', targetEntity: Label::class)]
    #[Ignore]
    private Collection $labels;

    public function __construct()
    {
        $this->activities = new ArrayCollection();
        $this->labels = new ArrayCollection();
    }

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): static
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    #[Override]
    public function getActivityName(): string
    {
        return sprintf("le groupe %s", $this->name);
    }

    #[Override]
    public function viewData(): array
    {
        return [
            "Nom" => 'name',
            "Description" => 'description',
        ];
    }

    #[Override]
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
        ];
    }

    /**
     * @return Collection<int, Activity>
     */
    #[Override]
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    #[Override]
    public function addActivity(Activity $activity): static
    {
        if (!$this->activities->contains($activity)) {
            $this->activities->add($activity);
            $activity->setLabelGroup($this);
        }
        return $this;
    }

    public function removeActivity(Activity $activity): static
    {
        if ($this->activities->removeElement($activity)) {
            if ($activity->getLabelGroup() === $this) {
                $activity->setLabelGroup(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, Label>
     */
    public function getLabels(): Collection
    {
        return $this->labels;
    }

    public function addLabel(Label $label): static
    {
        if (!$this->labels->contains($label)) {
            $this->labels->add($label);
            $label->setLabelGroup($this);
        }
        return $this;
    }

    public function removeLabel(Label $label): static
    {
        if ($this->labels->removeElement($label)) {
            if ($label->getLabelGroup() === $this) {
                $label->setLabelGroup(null);
            }
        }
        return $this;
    }

    public function export(): array
    {
        $extractor = new AttributeExtractor(LabelGroup::class);
        $properties = $extractor->getPropertiesHaving(ORM\Column::class);
        return $extractor->normalizeEntityByProperties($properties, $this);
    }

    #[Override]
    public function getActivityScope(): ?Tenant
    {
        return $this->tenant;
    }

    #[Groups(['action'])]
    public function getDeletable(): bool
    {
        return $this->getLabels()->count() === 0;
    }
}
