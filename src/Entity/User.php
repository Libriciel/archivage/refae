<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\OpenApi\Model;
use ApiPlatform\Metadata\GetCollection;
use App\Doctrine\UuidGenerator;
use App\Entity\Attribute\IgnoreActivity;
use App\Entity\Attribute\ViewSection;
use App\Repository\UserRepository;
use App\Validator\UniqueUser;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use IntlDateFormatter;
use Locale;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Serializer\Attribute\Ignore;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[UniqueEntity(['username'], "Identifiant de connexion déjà utilisé")]
#[UniqueUser]
#[ApiResource(
    operations: [
        new GetCollection( // Get oblige un parametre Id
            routeName: 'api_whoami',
            openapi: new Model\Operation(
                responses: [
                    '200' => [
                        'content' => [
                            'application/json' => [
                                'example' => [
                                    'username' => 'webservice',
                                    'name' => 'Webservice requêteur',
                                    'email' => 'webservice@refae.fr',
                                    'tenant' => 'myTenant',
                                    'app.version' => '2.X.X',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: "Informations sur l'utilisateur connecté",
                description: "Informations sur l'utilisateur connecté",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
    ],
)]
class User implements
    UserInterface,
    PasswordAuthenticatedUserInterface,
    DeletableEntityInterface,
    ViewEntityInterface,
    ActivitySubjectEntityInterface,
    TenantsIntegrityEntityInterface
{
    use ArrayEntityTrait;

    public const string DTO_TENANT = 'tenant';

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['index', 'index_admin'])]
    private ?LazyUuidFromString $id = null;

    #[ORM\Column(length: 255, unique: true)]
    #[Assert\NotBlank]
    #[Groups(['index', 'index_admin', 'view', 'csv', 'admin_csv'])]
    #[ViewSection('main')]
    #[SerializedName("Identifiant de connexion")]
    #[Attribute\ResultSet("Identifiant de connexion")]
    private ?string $username = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['index', 'index_admin', 'view', 'csv', 'admin_csv'])]
    #[ViewSection('main')]
    #[SerializedName("Nom")]
    #[Attribute\ResultSet("Nom")]
    private ?string $name = null;

    /**
     * @var string|null The hashed password
     */
    #[ORM\Column(length: 255, nullable: true)]
    #[Ignore]
    #[Attribute\IgnoreActivity(IgnoreActivity::VALUE)]
    private ?string $password = null;
    #[Ignore]
    private ?string $passwordConfirm = null;

    #[ORM\Column(length: 255)]
    #[Assert\Email]
    #[Groups(['index', 'index_admin', 'view', 'csv', 'admin_csv'])]
    #[ViewSection('main')]
    #[SerializedName("E-mail")]
    #[Attribute\ResultSet("e-mail")]
    private ?string $email = null;

    #[ORM\ManyToMany(targetEntity: Tenant::class, mappedBy: 'users')]
    #[Ignore]
    private Collection $tenants;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: PrivilegeUser::class, orphanRemoval: true)]
    #[Ignore]
    private Collection $privilegeUsers;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Activity::class, orphanRemoval: true)]
    #[Ignore]
    private Collection $activities;

    #[ORM\OneToMany(mappedBy: 'created_user', targetEntity: Activity::class)]
    #[Ignore]
    private Collection $created_activities;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: ApiToken::class, orphanRemoval: true)]
    #[Ignore]
    private Collection $apiTokens;

    /**
     * Défini et utilisé par le service LoggedUser et les champs virtuels:
     * - getTenant($request) (set la valeur)
     * - getActiveTenant() / setActiveTenant()
     * - plein d'autres champs virtuels (en lecture seule)
     * @var Tenant|null
     */
    #[Ignore]
    private ?Tenant $activeTenant = null;

    /**
     * Champ virtuel pour le formulaire
     * @var string|null
     */
    private ?string $loginType = null;

    #[ORM\OneToMany(
        mappedBy: 'user',
        targetEntity: AccessUser::class,
        cascade: ['persist', 'remove'],
        orphanRemoval: true,
    )]
    #[Ignore]
    private Collection $accessUsers;

    #[ORM\Column]
    private ?bool $notifyUserRegistration = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: UserPasswordRenewToken::class, orphanRemoval: true)]
    private Collection $userPasswordRenewTokens;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Notify::class, orphanRemoval: true)]
    private Collection $notifications;
    /**
     * @var Tenant[]
     */
    private array $tenantList;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: DocumentRuleImportSession::class, orphanRemoval: true)]
    private Collection $documentRuleImportSessions;

    private ?string $context = null;

    #[ORM\Column]
    private ?bool $userDefinedPassword = null;

    public function __construct()
    {
        $this->tenants = new ArrayCollection();
        $this->privilegeUsers = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->created_activities = new ArrayCollection();
        $this->apiTokens = new ArrayCollection();
        $this->accessUsers = new ArrayCollection();
        $this->userPasswordRenewTokens = new ArrayCollection();
        $this->notifications = new ArrayCollection();
        $this->documentRuleImportSessions = new ArrayCollection();
    }

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function setId(LazyUuidFromString $id)
    {
        $this->id = $id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): static
    {
        $this->username = $username;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;
        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    #[Override]
    public function getUserIdentifier(): string
    {
        return (string)$this->username;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    #[Override]
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;
        return $this;
    }

    public function getPasswordConfirm(): ?string
    {
        return $this->passwordConfirm;
    }

    public function setPasswordConfirm(string $passwordConfirm): static
    {
        $this->passwordConfirm = $passwordConfirm;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): static
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @see UserInterface
     */
    #[Override]
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * roles symfony, laisser vide
     * @return array|string[]
     */
    #[Override]
    public function getRoles(): array
    {
        return [];
    }

    /**
     * @return Collection<int, Tenant>
     */
    #[Override]
    public function getTenants(): Collection
    {
        $tenantsArray = $this->tenants->toArray();

        usort($tenantsArray, fn(Tenant $a, Tenant $b) => strcmp((string) $a->getName(), (string) $b->getName()));

        return new ArrayCollection($tenantsArray);
    }

    /**
     * Tri des tenants qui peuvent apparaître dans la barre utilisateur :
     *      -  actif
     *      - l'user y a un access
     */
    public function getAccessibleTenants(): Collection
    {
        return $this->getAccessUsers()
            ->map(fn (AccessUser $accessUser): ?Tenant => $accessUser->getTenant())
            ->filter(
                fn (?Tenant $tenant): bool => (bool)$tenant?->isActive()
            );
    }

    public function getTenant(Request $request): ?Tenant
    {
        $baseurl = $request->get('tenant_url');
        if (!$baseurl) {
            return null;
        }
        if ($this->getActiveTenant()?->getBaseurl() === $baseurl) {
            return $this->getActiveTenant();
        }
        return $this->setActiveTenantFromRequest($request);
    }

    public function setActiveTenantFromRequest(Request $request): ?Tenant
    {
        $baseurl = $request->get('tenant_url');
        foreach ($this->getTenants() as $tenant) {
            if ($tenant->getBaseurl() === $baseurl) {
                $this->setActiveTenant($tenant);
                return $tenant;
            }
        }
        return null;
    }

    public function addTenant(Tenant $tenant): static
    {
        if (!$this->tenants->contains($tenant)) {
            $this->tenants->add($tenant);
            $tenant->addUser($this);
        }

        return $this;
    }

    public function removeTenant(Tenant $tenant): static
    {
        $this->tenants->removeElement($tenant);
        $tenant->removeUser($this);

        return $this;
    }

    #[Groups(['action'])]
    #[SerializedName('availableTenants')]
    public function hasAvailableTenants(): bool
    {
        $accessTenants = array_filter(
            $this->getAccessUsers()->toArray(),
            fn(AccessUser $a): bool => (bool)$a->getTenant()
        );
        return count($accessTenants) < count($this->getTenants());
    }

    /**
     * @return Collection<int, AccessUser>
     */
    public function getAccessUsers(): Collection
    {
        return $this->accessUsers;
    }

    public function addAccessUser(AccessUser $accessUser): static
    {
        if (!$this->accessUsers->contains($accessUser)) {
            $this->accessUsers->add($accessUser);
            $accessUser->setUser($this);
        }
        return $this;
    }

    public function removeAccessUser(AccessUser $accessUser): static
    {
        if ($this->accessUsers->removeElement($accessUser)) {
            if ($accessUser->getUser() === $this) {
                $accessUser->setUser(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, PrivilegeUser>
     */
    public function getPrivilegeUsers(): Collection
    {
        return $this->privilegeUsers;
    }

    public function addPrivilegeUser(PrivilegeUser $privilegeUser): static
    {
        if (!$this->privilegeUsers->contains($privilegeUser)) {
            $this->privilegeUsers->add($privilegeUser);
            $privilegeUser->setUser($this);
        }
        return $this;
    }

    public function removePrivilegeUser(PrivilegeUser $privilegeUser): static
    {
        if ($this->privilegeUsers->removeElement($privilegeUser)) {
            if ($privilegeUser->getUser() === $this) {
                $privilegeUser->setUser(null);
            }
        }
        return $this;
    }

    #[Groups(['view', 'view_admin'])]
    #[SerializedName('Ldaps')]
    #[ViewSection('other')]
    public function getLdaps(): array
    {
        $activeTenant = $this->getActiveTenant();
        $filterActiveTenant = fn(AccessUser $accessUser) => $accessUser->getLdap()
            && (!$activeTenant || $accessUser->getTenant() === $activeTenant)
        ;
        return $this->accessUsers
            ->filter($filterActiveTenant)
            ->map(
                function (AccessUser $accessUser) {
                    $ldap = $accessUser->getLdap()?->getName() ?? '';
                    $tenant = $accessUser->getTenant()?->getName() ?? '';
                    return $tenant
                        ? sprintf('%s (Tenant : %s)', $ldap, $tenant)
                        : sprintf('%s (Administrateur technique)', $ldap);
                }
            )
            ->toArray()
        ;
    }

    #[Groups(['view', 'view_admin'])]
    #[SerializedName('Openids')]
    #[ViewSection('other')]
    public function getOpenids(): array
    {
        $activeTenant = $this->getActiveTenant();
        $filterActiveTenant = fn(AccessUser $accessUser) => $accessUser->getOpenid()
            && (!$activeTenant || $accessUser->getTenant() === $activeTenant)
        ;
        return $this->accessUsers
            ->filter($filterActiveTenant)
            ->map(
                function (AccessUser $accessUser) {
                    $openid = $accessUser->getOpenid()?->getName() ?? '';
                    $tenant = $accessUser->getTenant()?->getName() ?? '';
                    return $tenant
                        ? sprintf('%s (Tenant : %s)', $openid, $tenant)
                        : sprintf('%s (Administrateur technique)', $openid);
                }
            )
            ->toArray()
        ;
    }

    #[Override]
    public function viewData(): array
    {
        return [
            'Username' => 'username',
            'E-mail' => 'email',
        ];
    }

    #[Groups(['index', 'index_admin', 'view'])]
    #[SerializedName("Créé le")]
    #[ViewSection('main')]
    #[Attribute\ResultSet("date de création", display: false)]
    public function getCreatedDateTime(): string
    {
        $uuid = new LazyUuidFromString($this->id);
        $hexTimestampMilliseconds = $uuid->getFields()->getTimestamp()->toString();
        $timestampMilliseconds = $uuid->getNumberConverter()->fromHex($hexTimestampMilliseconds);
        $timestamp = number_format($timestampMilliseconds / 1000, 6, '.', '');

        $formatter = new IntlDateFormatter(
            Locale::getDefault(),
            IntlDateFormatter::LONG,
            IntlDateFormatter::MEDIUM
        );
        return  $formatter->format(DateTime::createFromFormat('U.u', $timestamp));
    }

    /**
     * @return Collection<int, Activity>
     */
    #[Override]
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    #[Override]
    public function addActivity(Activity $activity): static
    {
        if (!$this->activities->contains($activity)) {
            $this->activities->add($activity);
            $activity->setUser($this);
        }
        return $this;
    }

    public function removeActivity(Activity $activity): static
    {
        if ($this->activities->removeElement($activity)) {
            if ($activity->getUser() === $this) {
                $activity->setUser(null);
            }
        }
        return $this;
    }

    #[Override]
    public function getActivityName(): string
    {
        return sprintf("l'utilisateur %s", $this->username);
    }

    /**
     * @return Collection<int, Activity>
     */
    public function getCreatedActivities(): Collection
    {
        return $this->created_activities;
    }

    public function addCreatedActivity(Activity $createdActivity): static
    {
        if (!$this->created_activities->contains($createdActivity)) {
            $this->created_activities->add($createdActivity);
            $createdActivity->setCreatedUser($this);
        }
        return $this;
    }

    public function removeCreatedActivity(Activity $createdActivity): static
    {
        if ($this->created_activities->removeElement($createdActivity)) {
            if ($createdActivity->getCreatedUser() === $this) {
                $createdActivity->setCreatedUser(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, ApiToken>
     */
    public function getApiTokens(): Collection
    {
        return $this->apiTokens;
    }

    public function addApiToken(ApiToken $apiToken): static
    {
        if (!$this->apiTokens->contains($apiToken)) {
            $this->apiTokens->add($apiToken);
            $apiToken->setUser($this);
        }

        return $this;
    }

    public function removeApiToken(ApiToken $apiToken): static
    {
        if ($this->apiTokens->removeElement($apiToken)) {
            // set the owning side to null (unless already changed)
            if ($apiToken->getUser() === $this) {
                $apiToken->setUser(null);
            }
        }

        return $this;
    }

    public function setActiveTenant(Tenant $tenant)
    {
        $this->activeTenant = $tenant;

        return $this;
    }

    public function getActiveTenant(): ?Tenant
    {
        return $this->activeTenant;
    }

    #[Groups(['action'])]
    #[SerializedName('deletable')]
    #[Override]
    public function getDeletable(): bool
    {
        if ($this->context === 'admin_tech' && $this->getAdminTechAccessUser()) {
            return $this->getAccessUsers()->count() === 1;
        }

        if ($this->getActiveTenant()) {
            return $this->getAccessUsers()->count() === 1
                && $this->getAccessUsers()->first()->getTenant() === $this->getActiveTenant();
        } else {
            return $this->getAccessUsers()->count() === 0;
        }
    }

    #[Groups(['action'])]
    #[SerializedName('addableAccess')]
    public function getAddableAccess(): bool
    {
        return $this->getAccessUser() === null;
    }

    #[Groups(['action'])]
    #[SerializedName('deletableAccess')]
    public function getDeletableAccess(): bool
    {
        return (bool)$this->getAccessUser();
    }

    public function getLoginType(): ?string
    {
        if (!$this->loginType) {
            $accessUser = $this->getAccessUser();
            if (!$accessUser) {
                return null;
            }
            $this->loginType = $accessUser->getLoginType();
        }
        return $this->loginType;
    }

    public function setLoginType(string $loginType): static
    {
        $this->loginType = $loginType;

        return $this;
    }

    public function getLdap(): ?Ldap
    {
        return $this->getAccessUser()?->getLdap();
    }

    public function getOpenid(): ?Openid
    {
        return $this->getAccessUser()?->getOpenid();
    }

    public function getRole(): ?Role
    {
        return $this->getAccessUser()?->getRole();
    }

    #[Groups(['index', 'csv'])]
    #[SerializedName("Rôle")]
    #[ViewSection('other')]
    #[Attribute\ResultSet("Rôle")]
    public function getRoleName(): ?string
    {
        return $this->getAccessUser()?->getRoleName();
    }

    public function getAdminTechAccessUser(): ?AccessUser
    {
        $adminTechAccessUser = $this->getAccessUsers()
            ->filter(fn(AccessUser $accessUser) => $accessUser->getTenant() === null)
            ->first()
        ;
        return $adminTechAccessUser !== false ? $adminTechAccessUser : null;
    }

    public function getAccessUser(): ?AccessUser
    {
        if (!$this->getActiveTenant()) {
            return null;
        }
        $activeTenantAccessUser = $this->getAccessUsers()
            ->filter(fn(AccessUser $accessUser) => $accessUser->getTenant() === $this->getActiveTenant())
            ->first()
        ;
        return $activeTenantAccessUser !== false ? $activeTenantAccessUser : null;
    }

    #[Groups(['index', 'index_admin', 'csv'])]
    #[SerializedName("Méthode de connexion")]
    #[ViewSection('other')]
    #[Attribute\ResultSet("méthode de connexion")]
    public function getLoginTypeName(): ?string
    {
        if ($this->context === 'admin_tech' && $this->getAdminTechAccessUser()) {
            $accessUser = $this->getAdminTechAccessUser();
        } else {
            $accessUser = $this->getAccessUser();
        }
        return $accessUser?->getLoginTypeName();
    }

    #[Groups(['index_admin'])]
    public function getTenantNames(): array
    {
        return $this->getTenants()
            ->map(fn(Tenant $tenant): string => $tenant->getName() ?? '')
            ->toArray()
        ;
    }

    public function isNotifyUserRegistration(): ?bool
    {
        return $this->notifyUserRegistration;
    }

    public function setNotifyUserRegistration(bool $notifyUserRegistration): static
    {
        $this->notifyUserRegistration = $notifyUserRegistration;

        return $this;
    }

    /**
     * @return Collection<int, UserPasswordRenewToken>
     */
    public function getUserPasswordRenewTokens(): Collection
    {
        return $this->userPasswordRenewTokens;
    }

    public function addUserPasswordRenewToken(UserPasswordRenewToken $userPasswordRenewToken): static
    {
        if (!$this->userPasswordRenewTokens->contains($userPasswordRenewToken)) {
            $this->userPasswordRenewTokens->add($userPasswordRenewToken);
            $userPasswordRenewToken->setUser($this);
        }

        return $this;
    }

    public function removeUserPasswordRenewToken(UserPasswordRenewToken $userPasswordRenewToken): static
    {
        if ($this->userPasswordRenewTokens->removeElement($userPasswordRenewToken)) {
            // set the owning side to null (unless already changed)
            if ($userPasswordRenewToken->getUser() === $this) {
                $userPasswordRenewToken->setUser(null);
            }
        }

        return $this;
    }

    #[Groups(['action'])]
    public function getCommonName(): ?string
    {
        return $this->getName() ?: $this->getUsername();
    }

    #[Override]
    public function getActivityScope(): ?Tenant
    {
        if ($this->getTenants()->count() === 1) {
            return $this->getTenants()->first();
        }
        return $this->getActiveTenant();
    }

    /**
     * @return Collection<int, Notify>
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function getNotificationsJson(): string
    {
        $notifications = $this->notifications->map(
            fn (Notify $notify) => [
                'id' => (string)$notify->getId(),
                'text' => $notify->getText(),
                'created' => $notify->getCreated()->format(DATE_RFC3339),
                'css_class' => $notify->getCssClass(),
            ]
        );
        return $notifications->count() > 0 ? json_encode($notifications->toArray()) : '';
    }

    public function addNotification(Notify $notification): static
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications->add($notification);
            $notification->setUser($this);
        }
        return $this;
    }

    public function removeNotification(Notify $notification): static
    {
        if ($this->notifications->removeElement($notification)) {
            // set the owning side to null (unless already changed)
            if ($notification->getUser() === $this) {
                $notification->setUser(null);
            }
        }
        return $this;
    }

    #[Groups(['index_admin', 'admin_csv'])]
    #[Attribute\ResultSet("Accès")]
    public function getAccesses(): array
    {
        return array_values(
            $this->getAccessUsers()
                ->filter(fn(AccessUser $access): bool => $access->getTenant() !== null)
                ->map(
                    fn(AccessUser $access): string => htmlentities(
                        sprintf(
                            '%s: role %s',
                            $access->getTenant()->getBaseurl(),
                            $access->getRole()?->getCommonName()
                        )
                    )
                )
                ->toArray()
        );
    }

    #[Groups(['index_admin', 'admin_csv', 'view_admin'])]
    #[Attribute\ResultSet("Administrateur technique", type: 'boolean')]
    #[SerializedName("Administrateur technique")]
    #[ViewSection('other')]
    public function isTechnicalAdmin(): bool
    {
        foreach ($this->getAccessUsers() as $access) {
            if ($access->getTenant() === null) {
                return true;
            }
        }
        return false;
    }

    #[Groups(['action'])]
    public function hasAdminAccess(): bool
    {
        foreach ($this->getAccessUsers() as $accessUser) {
            if (!$accessUser->getTenant()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return Collection<int, DocumentRuleImportSession>
     */
    public function getDocumentRuleImportSessions(): Collection
    {
        return $this->documentRuleImportSessions;
    }

    public function addDocumentRuleImportSession(DocumentRuleImportSession $documentRuleImportSession): static
    {
        if (!$this->documentRuleImportSessions->contains($documentRuleImportSession)) {
            $this->documentRuleImportSessions->add($documentRuleImportSession);
            $documentRuleImportSession->setUser($this);
        }

        return $this;
    }

    public function removeDocumentRuleImportSession(DocumentRuleImportSession $documentRuleImportSession): static
    {
        if ($this->documentRuleImportSessions->removeElement($documentRuleImportSession)) {
            // set the owning side to null (unless already changed)
            if ($documentRuleImportSession->getUser() === $this) {
                $documentRuleImportSession->setUser(null);
            }
        }

        return $this;
    }

    public function setContext(string $context): static
    {
        $this->context = $context;

        return $this;
    }

    public function isUserDefinedPassword(): ?bool
    {
        return $this->userDefinedPassword;
    }

    public function setUserDefinedPassword(bool $userDefinedPassword): static
    {
        $this->userDefinedPassword = $userDefinedPassword;

        return $this;
    }
}
