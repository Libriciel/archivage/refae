<?php

namespace App\Entity;

interface StateMachineEntityInterface
{
    public function getState(): ?string;
    public function setState(string $state): static;
    public function getInitialState(): string;
    public function getTransitions(): array;
}
