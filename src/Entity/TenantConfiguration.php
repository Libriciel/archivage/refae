<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Repository\TenantConfigurationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Ramsey\Uuid\Lazy\LazyUuidFromString;

#[ORM\Entity(repositoryClass: TenantConfigurationRepository::class)]
class TenantConfiguration
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    private ?LazyUuidFromString $id = null;

    #[ORM\OneToOne(inversedBy: 'tenantLogoMain', cascade: ['persist', 'remove'])]
    private ?File $logoMain = null;

    #[ORM\OneToOne(inversedBy: 'tenantLogoLogin', cascade: ['persist', 'remove'])]
    private ?File $logoLogin = null;

    #[ORM\Column(nullable: true)]
    private ?int $logoMainHeight = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $mailPrefix = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $mailSignature = null;

    #[ORM\OneToOne(mappedBy: 'configuration', cascade: ['persist', 'remove'])]
    private ?Tenant $tenant = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $logoLoginBgColor = null;

    #[ORM\Column(type: Types::BIGINT, nullable: true)]
    private ?int $maxFilesize = null;

    #[ORM\Column(nullable: true)]
    private ?int $fileRetentionCount = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $logoLoginText = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $logoLoginTextStyle = null;

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function getLogoMain(): ?File
    {
        return $this->logoMain;
    }

    public function setLogoMain(?File $logoMain): static
    {
        $this->logoMain = $logoMain;

        return $this;
    }

    public function getLogoLogin(): ?File
    {
        return $this->logoLogin;
    }

    public function setLogoLogin(?File $logoLogin): static
    {
        $this->logoLogin = $logoLogin;

        return $this;
    }

    public function getLogoMainHeight(): ?int
    {
        return $this->logoMainHeight;
    }

    public function setLogoMainHeight(?int $logoMainHeight): static
    {
        $this->logoMainHeight = $logoMainHeight;

        return $this;
    }

    public function getMailPrefix(): ?string
    {
        return $this->mailPrefix;
    }

    public function setMailPrefix(?string $mailPrefix): static
    {
        $this->mailPrefix = $mailPrefix;

        return $this;
    }

    public function getMailSignature(): ?string
    {
        return $this->mailSignature;
    }

    public function setMailSignature(?string $mailSignature): static
    {
        $this->mailSignature = $mailSignature;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): static
    {
        // unset the owning side of the relation if necessary
        if ($tenant === null && $this->tenant !== null) {
            $this->tenant->setConfiguration(null);
        }

        // set the owning side of the relation if necessary
        if ($tenant !== null && $tenant->getConfiguration() !== $this) {
            $tenant->setConfiguration($this);
        }

        $this->tenant = $tenant;

        return $this;
    }

    public function getLogoLoginBgColor(): ?string
    {
        return $this->logoLoginBgColor;
    }

    public function setLogoLoginBgColor(?string $logoLoginBgColor): static
    {
        $this->logoLoginBgColor = $logoLoginBgColor;

        return $this;
    }

    public function getMaxFilesize(): ?int
    {
        return $this->maxFilesize;
    }

    public function setMaxFilesize(?int $maxFilesize): static
    {
        $this->maxFilesize = $maxFilesize;

        return $this;
    }

    public function getFileRetentionCount(): ?int
    {
        return $this->fileRetentionCount;
    }

    public function setFileRetentionCount(?int $fileRetentionCount): static
    {
        $this->fileRetentionCount = $fileRetentionCount;

        return $this;
    }

    public function getLogoLoginText(): ?string
    {
        return $this->logoLoginText;
    }

    public function setLogoLoginText(?string $logoLoginText): static
    {
        $this->logoLoginText = $logoLoginText;

        return $this;
    }

    public function getLogoLoginTextStyle(): ?string
    {
        return $this->logoLoginTextStyle;
    }

    public function setLogoLoginTextStyle(?string $logoLoginTextStyle): static
    {
        $this->logoLoginTextStyle = $logoLoginTextStyle;

        return $this;
    }
}
