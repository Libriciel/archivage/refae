<?php

namespace App\Entity;

use ArrayAccess;

interface ViewEntityInterface extends ArrayAccess
{
    /**
     * Donne la liste des champs ainsi que leur valeur
     * exemple: [
     *      "Nom" => "name",
     *      "Dates extrêmes" => '{oldest_date|default("[nd]")} - {latest_date|default("[nd]")}',
     *      "Communicabilité" => fn($entity) => $entity->getId(),
     * ]
     * @return array
     */
    public function viewData(): array;
}
