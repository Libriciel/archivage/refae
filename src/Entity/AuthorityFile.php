<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Repository\AuthorityFileRepository;
use App\Service\AttributeExtractor;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: AuthorityFileRepository::class)]
class AuthorityFile implements OneTenantIntegrityEntityInterface
{
    public const string TYPE_EACCPF_2010 = 'eac-cpf-2010';
    public const string TYPE_EACCPF_V20 = 'eac-cpf-v20';
    public const string TYPE_OTHER = 'other';
    public const array TYPE_VALUES = [self::TYPE_EACCPF_2010, self::TYPE_EACCPF_V20, self::TYPE_OTHER];

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['api', 'index', 'view'])]
    private ?LazyUuidFromString $id = null;

    #[ORM\ManyToOne(inversedBy: 'authorityFiles')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Authority $authority;

    #[ORM\ManyToOne(cascade: ['persist', 'remove'], inversedBy: 'authorityFiles')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['api'])]
    private ?File $file = null;

    #[ORM\Column(length: 255)]

    #[Assert\Choice(choices: AuthorityFile::TYPE_VALUES, message: 'Choose a valid type.')]
    #[Groups(['api'])]
    private ?string $type = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['api', 'index', 'view'])]
    #[Attribute\ResultSet("Description", display: false)]
    private ?string $description = null;

    public function __construct(Authority $authority = null)
    {
        $this->authority = $authority;
    }

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function getAuthority(): ?Authority
    {
        return $this->authority;
    }

    public function setAuthority(?Authority $authority): static
    {
        $this->authority = $authority;

        return $this;
    }

    #[Groups(['index', 'view'])]
    public function getAuthorityId(): string
    {
        return $this->authority->getId();
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(?File $file): static
    {
        $this->file = $file;
        return $this;
    }

    #[Groups(['index', 'view'])]
    public function getFileId(): string
    {
        return $this->getFile()?->getId() ?? '';
    }

    #[Groups(['index', 'view'])]
    #[Attribute\ResultSet("Nom")]
    #[SerializedName("Nom")]
    public function getFileName(): string
    {
        return $this->getFile()?->getName() ?? '';
    }

    #[Groups(['index', 'view'])]
    #[Attribute\ResultSet("Type MIME")]
    #[SerializedName("Mime")]
    public function getFileMime(): string
    {
        return $this->getFile()?->getMime() ?? '';
    }

    #[Groups(['index'])]
    #[Attribute\ResultSet("Taille")]
    #[SerializedName("Taille")]
    public function getFileSize(): string
    {
        return $this->getFile()?->getReadableSize() ?? '';
    }

    #[Groups(['index', 'view'])]
    #[Attribute\ResultSet("Algorithme")]
    public function getFileHashAlgo(): string
    {
        return $this->getFile()?->getHashAlgo() ?? '';
    }

    #[Groups(['index', 'view'])]
    #[Attribute\ResultSet("Hash", display: false)]
    public function getFileHash(): string
    {
        return $this->getFile()?->getHash() ?? '';
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function export(): array
    {
        $extractor = new AttributeExtractor(AuthorityFile::class);
        $properties = $extractor->getPropertiesHaving(ORM\Column::class);
        $normalized = $extractor->normalizeEntityByProperties($properties, $this);
        $normalized['file'] = $this->getFile()?->export();
        return $normalized;
    }

    public static function getTypeTranslations()
    {
        return [
            self::TYPE_EACCPF_2010 => "eac-cpf 2010",
            self::TYPE_EACCPF_V20 => "eac-cpf v2.0",
            self::TYPE_OTHER => "Autre",
        ];
    }

    #[Groups(['index', 'view'])]
    #[Attribute\ResultSet("Type")]
    public function getTypeTrad(): string
    {
        return static::getTypeTranslations()[$this->type];
    }

    #[Override]
    public function getTenant(): ?Tenant
    {
        return $this->getAuthority()?->getTenant();
    }
}
