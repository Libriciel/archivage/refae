<?php

namespace App\Entity;

/**
 * Permet d'ignorer l'inscription d'une entrée activity sous certaines conditions lors de l'ajout
 */
interface ActivityBypassAddEntityInterface
{
    public function activityBypassAdd(): bool;
}
