<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\OpenApi\Model;
use App\ApiPlatform\LabelFilter;
use App\Doctrine\UuidGenerator;
use App\Entity\Attribute\ViewSection;
use App\Repository\AuthorityRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use IntlDateFormatter;
use Locale;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

#[ORM\Entity(repositoryClass: AuthorityRepository::class)]
#[UniqueEntity(['tenant', 'identifier', 'version'], "Cette notice existe déjà")]
#[ApiResource(
    operations: [
        new GetCollection(
            uriTemplate: '/{tenant_url}/authority-published',
            uriVariables: [],
            routeName: 'api_authority_published_list',
            openapi: new Model\Operation(
                summary: "Liste des notices d'autorité publiées",
                description: "Liste des notices d'autorité publiées",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
        new Get(
            uriTemplate: '/{tenant_url}/authority-published/{id}',
            routeName: 'api_authority_published_id',
            openapi: new Model\Operation(
                summary: "Récupère une notice d'autorité publiée",
                description: "Récupère une notice d'autorité publiée",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                    new Model\Parameter(
                        name: 'id',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
        new Get(
            uriTemplate: '/{tenant_url}/authority-by-identifier/{identifier}',
            routeName: 'api_authority_published_identifier',
            openapi: new Model\Operation(
                summary: "Récupère une notice d'autorité par identifiant",
                description: "Récupère une notice d'autorité par identifiant",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                    new Model\Parameter(
                        name: 'identifier',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
    ],
    normalizationContext: [
        'groups' => ['api'],
    ]
)]
#[ApiFilter(SearchFilter::class, properties: ['identifier'])]
#[ApiFilter(LabelFilter::class, properties: ['labels'])]
class Authority implements
    ActivitySubjectEntityInterface,
    EditableEntityInterface,
    DeletableEntityInterface,
    HaveFilesInterface,
    HaveLabelsEntityInterface,
    MeilisearchIndexedInterface,
    OneTenantIntegrityEntityInterface,
    PublishableEntityInterface,
    StateMachineEntityInterface,
    ViewEntityInterface,
    TranslatedTransitionsInterface
{
    use ArrayEntityTrait;
    use HaveLabelsEntityTrait;
    use VersionableEntityTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['api', 'meilisearch', 'index'])]
    private ?LazyUuidFromString $id = null;

    #[ORM\ManyToOne(targetEntity: Tenant::class, inversedBy: 'authorities')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Tenant $tenant = null;

    #[ORM\Column(length: 255)]
    #[Attribute\ResultSet("Identifiant")]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Identifiant")]
    #[ViewSection('main')]
    private ?string $identifier = null;

    #[ORM\Column(length: 255)]
    #[Attribute\ResultSet("Nom")]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Nom")]
    #[ViewSection('main')]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Attribute\ResultSet("description", blacklist: true)]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Description")]
    #[ViewSection('main')]
    private ?string $description = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Attribute\ResultSet("état de la notice", display: false)]
    #[Groups(['api', 'index', 'meilisearch', 'view'])]
    #[SerializedName("État de la notice")]
    #[ViewSection('main')]
    private ?string $maintenance_status = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    #[Attribute\ResultSet("Date de début d'existence", type: 'date')]
    #[Groups(['api', 'index', 'meilisearch', 'view'])]
    #[SerializedName("Date de début d'existence")]
    #[ViewSection('main')]
    private ?DateTimeInterface $exist_dates_from = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    #[Attribute\ResultSet("Date de fin d'existence", type: 'date')]
    #[Groups(['api', 'index', 'meilisearch', 'view'])]
    #[SerializedName("Date de fin d'existence")]
    #[ViewSection('main')]
    private ?DateTimeInterface $exist_dates_to = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Attribute\ResultSet("type d’entité", display: false)]
    #[Groups(['api', 'index', 'meilisearch', 'view'])]
    #[SerializedName("Type d'entité")]
    #[ViewSection('main')]
    private ?string $entity_type = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Attribute\ResultSet("statut juridique", blacklist: true)]
    #[Groups(['api', 'index', 'meilisearch', 'view'])]
    #[SerializedName("Statut juridique")]
    #[ViewSection('main')]
    private ?string $legal_status = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Attribute\ResultSet("adresse", blacklist: true)]
    #[Groups(['api', 'index', 'meilisearch', 'view'])]
    #[SerializedName("Adresse")]
    #[ViewSection('main')]
    private ?string $place = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Attribute\ResultSet("fonction", blacklist: true)]
    #[Groups(['api', 'index', 'meilisearch', 'view'])]
    #[SerializedName("Fonction")]
    #[ViewSection('main')]
    private ?string $function = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Attribute\ResultSet("activité", blacklist: true)]
    #[Groups(['api', 'index', 'meilisearch', 'view'])]
    #[SerializedName("Activité")]
    #[ViewSection('main')]
    private ?string $occupation = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Attribute\ResultSet("texte de référence", blacklist: true)]
    #[Groups(['api', 'index', 'meilisearch', 'view'])]
    #[SerializedName("Texte de référence")]
    #[ViewSection('main')]
    private ?string $mandate = null;

    #[ORM\Column]
    #[Attribute\ResultSet("Publique", display: false)]
    #[Groups(['api', 'index', 'meilisearch', 'csv'])]
    #[SerializedName("Publique")]
    private ?bool $public = null;

    #[ORM\OneToMany(mappedBy: 'authority', targetEntity: AuthorityFile::class, orphanRemoval: true)]
    #[Groups(['api'])]
    private Collection $authorityFiles;

    #[ORM\ManyToMany(targetEntity: Label::class, inversedBy: 'authorities', fetch: 'EAGER')]
    #[Groups(['api', 'index'])]
    private Collection $labels;

    #[ORM\OneToMany(mappedBy: 'authority', targetEntity: Activity::class, orphanRemoval: true)]
    #[ORM\OrderBy(['id' => 'ASC'])]
    #[Groups(['api'])]
    private Collection $activities;

    public function __construct()
    {
        $this->authorityFiles = new ArrayCollection();
        $this->labels = new ArrayCollection();
        $this->activities = new ArrayCollection();
    }

    #[Override]
    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    #[Override]
    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    #[Groups(['index_public'])]
    #[Attribute\ResultSet("Tenant")]
    #[SerializedName("Tenant")]
    public function getTenantName(): string
    {
        return $this->getTenant()->getName();
    }

    #[Override]
    public function setTenant(?Tenant $tenant): static
    {
        $this->tenant = $tenant;
        return $this;
    }

    #[Override]
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): static
    {
        $this->identifier = $identifier;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    public function getMaintenanceStatus(): ?string
    {
        return $this->maintenance_status;
    }

    public function setMaintenanceStatus(?string $maintenance_status): static
    {
        $this->maintenance_status = $maintenance_status;
        return $this;
    }

    public function getExistDatesFrom(): ?DateTimeInterface
    {
        return $this->exist_dates_from;
    }

    public function setExistDatesFrom(?DateTimeInterface $exist_dates_from): static
    {
        $this->exist_dates_from = $exist_dates_from;
        return $this;
    }

    public function getExistDatesTo(): ?DateTimeInterface
    {
        return $this->exist_dates_to;
    }

    public function setExistDatesTo(?DateTimeInterface $exist_dates_to): static
    {
        $this->exist_dates_to = $exist_dates_to;
        return $this;
    }

    public function getEntityType(): ?string
    {
        return $this->entity_type;
    }

    public function setEntityType(?string $entity_type): static
    {
        $this->entity_type = $entity_type;
        return $this;
    }

    public function getLegalStatus(): ?string
    {
        return $this->legal_status;
    }

    public function setLegalStatus(?string $legal_status): static
    {
        $this->legal_status = $legal_status;
        return $this;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(?string $place): static
    {
        $this->place = $place;
        return $this;
    }

    public function getFunction(): ?string
    {
        return $this->function;
    }

    public function setFunction(?string $function): static
    {
        $this->function = $function;
        return $this;
    }

    public function getOccupation(): ?string
    {
        return $this->occupation;
    }

    public function setOccupation(?string $occupation): static
    {
        $this->occupation = $occupation;
        return $this;
    }

    public function getMandate(): ?string
    {
        return $this->mandate;
    }

    public function setMandate(?string $mandate): static
    {
        $this->mandate = $mandate;
        return $this;
    }

    #[Override]
    public function isPublic(): bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): static
    {
        $this->public = $public;
        return $this;
    }

    /**
     * @return Collection<int, AuthorityFile>
     */
    public function getAuthorityFiles(): Collection
    {
        return $this->authorityFiles;
    }

    public function addAuthorityFile(AuthorityFile $file): static
    {
        if (!$this->authorityFiles->contains($file)) {
            $this->authorityFiles->add($file);
            $file->setAuthority($this);
        }

        return $this;
    }

    public function removeAuthorityFile(AuthorityFile $file): static
    {
        if ($this->authorityFiles->removeElement($file)) {
            if ($file->getAuthority() === $this) {
                $file->setAuthority(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, Activity>
     */
    #[Override]
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    #[Override]
    public function addActivity(Activity $activity): static
    {
        if (!$this->activities->contains($activity)) {
            $this->activities->add($activity);
            $activity->setAuthority($this);
        }

        return $this;
    }

    public function removeActivity(Activity $activity): static
    {
        if ($this->activities->removeElement($activity)) {
            if ($activity->getAuthority() === $this) {
                $activity->setAuthority(null);
            }
        }
        return $this;
    }

    #[Override]
    public function viewData(): array
    {
        return [
            "Identifiant" => 'identifier',
            "Nom" => 'name',
            "Description" => 'description',
            "Etat de la notice" => 'maintenance_status',
            "Date de début d'existence" => fn() => $this->formatDate(
                $this->getExistDatesFrom()
            ),
            "Date de fin d'existence" => fn() => $this->formatDate(
                $this->getExistDatesTo()
            ),
            "Type d’entité" => 'entity_type',
            "Statut juridique" => 'legal_status',
            "Adresse" => 'place',
            "Fonction" => 'function',
            "Activité" => 'occupation',
            "Texte de référence" => 'mandate',
        ];
    }

    public function formatDate(
        ?DateTimeInterface $date,
        $formatDate = IntlDateFormatter::LONG,
        $formatTime = IntlDateFormatter::NONE,
        $locale = null
    ): string {
        if (!$date) {
            return '';
        }
        $formatter = new IntlDateFormatter(
            $locale ?: Locale::getDefault(),
            $formatDate,
            $formatTime
        );

        return $formatter->format($date);
    }

    #[Override]
    public function getActivityName(): string
    {
        return sprintf("la notice d'autorité %s", $this->name);
    }

    #[Override]
    #[Groups(['meilisearch'])]
    #[Attribute\ResultSet("Baseurl du tenant")]
    public function getTenantUrl(): ?string
    {
        return $this->getTenant()?->getBaseurl();
    }

    #[Attribute\ResultSet("Fichiers liés")]
    #[SerializedName("Fichiers liés")]
    #[Groups(['index', 'view', 'csv'])]
    public function getFilesForCsv(): string
    {
        return implode(
            "\n",
            array_map(
                fn(AuthorityFile $e) => $e->getFileName(),
                $this->getAuthorityFiles()->toArray()
            )
        );
    }

    #[Override]
    public function getFiles(): array
    {
        return $this->getAuthorityFiles()->toArray();
    }

    #[Override]
    public function getActivityScope(): ?Tenant
    {
        return $this->tenant;
    }

    #[Attribute\ResultSet("Publique")]
    #[Groups(['view'])]
    #[SerializedName("Publique")]
    #[ViewSection('other')]
    public function getReadablePublic(): string
    {
        return $this->public ? "Oui" : "Non";
    }
}
