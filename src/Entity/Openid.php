<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Repository\OpenidRepository;
use App\Security\OpenIDConnectClient;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use JsonSerializable;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: OpenidRepository::class)]
#[UniqueEntity(['name'], "Cet Openid existe déjà")]
class Openid implements JsonSerializable, ViewEntityInterface
{
    use ArrayEntityTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Attribute\ResultSet("Id", display: false)]
    #[Groups(['index'])]
    private ?LazyUuidFromString $id = null;

    #[ORM\Column(length: 255, unique: true)]
    #[Assert\NotBlank]
    #[Attribute\ResultSet("Nom")]
    #[Groups(['index'])]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Attribute\ResultSet("Url de base")]
    #[Groups(['index'])]
    private ?string $url = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Attribute\ResultSet("Id client")]
    #[Groups(['index'])]
    private ?string $client_id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Attribute\ResultSet("Secret client", blacklist: true)]
    #[Groups(['index'])]
    private ?string $client_secret = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Attribute\ResultSet("Champ utilisé pour le login", display: false)]
    #[Groups(['index'])]
    private ?string $username_field = null;

    #[ORM\ManyToMany(targetEntity: Tenant::class, inversedBy: 'openids')]
    private Collection $tenants;

    #[ORM\OneToMany(
        mappedBy: 'openid',
        targetEntity: OpenidScope::class,
        cascade: ['persist'],
        fetch: 'EAGER',
        orphanRemoval: true
    )]
    private Collection $scopes;

    #[ORM\OneToMany(mappedBy: 'openid', targetEntity: AccessUser::class, fetch: 'EAGER')]
    private Collection $accessUsers;

    #[ORM\Column(length: 255)]
    private ?string $identifier = null;

    private ?string $loginUrl = null;

    public function __construct()
    {
        $this->tenants = new ArrayCollection();
        $this->scopes = new ArrayCollection();
        $this->accessUsers = new ArrayCollection();
    }

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function getClientId(): ?string
    {
        return $this->client_id;
    }

    public function setClientId(string $client_id): static
    {
        $this->client_id = $client_id;

        return $this;
    }

    public function getClientSecret(): ?string
    {
        return $this->client_secret;
    }

    public function setClientSecret(?string $client_secret): static
    {
        $this->client_secret = $client_secret;

        return $this;
    }

    public function getUsernameField(): ?string
    {
        return $this->username_field;
    }

    public function setUsernameField(string $username_field): static
    {
        $this->username_field = $username_field;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): static
    {
        $this->url = $url;

        return $this;
    }

    public function getOpenidClient(): OpenIDConnectClient
    {
        $oidc = new OpenIDConnectClient(
            $this->getUrl(),
            $this->getClientId(),
            $this->getClientSecret(),
        );
        foreach ($this->getScopes() as $scope) {
            $oidc->addScope($scope->getName());
        }
        return $oidc;
    }

    /**
     * @return Collection<int, Tenant>
     */
    public function getTenants(): Collection
    {
        return $this->tenants;
    }

    public function addTenant(Tenant $tenant): static
    {
        if (!$this->tenants->contains($tenant)) {
            $this->tenants->add($tenant);
        }

        return $this;
    }

    public function removeTenant(Tenant $tenant): static
    {
        $this->tenants->removeElement($tenant);

        return $this;
    }

    /**
     * @return Collection<int, OpenidScope>
     */
    public function getScopes(): Collection
    {
        return $this->scopes;
    }

    public function addScope(OpenidScope $scope): static
    {
        if (!$this->scopes->contains($scope)) {
            $this->scopes->add($scope);
            $scope->setOpenid($this);
        }
        return $this;
    }

    public function removeScope(OpenidScope $scope): static
    {
        if ($this->scopes->removeElement($scope)) {
            if ($scope->getOpenid() === $this) {
                $scope->setOpenid(null);
            }
        }
        return $this;
    }

    #[Override]
    public function viewData(): array
    {
        return [
            "Nom" => 'name',
            "Url" => 'url',
            "Id client" => 'client_id',
            "Champ utilisé pour identifier l'utilisateur" => 'username_field',
            "Scopes" => function (Openid $openid) {
                $ul = '<ul>';
                /** @var OpenidScope $scope */
                foreach ($openid->getScopes() as $scope) {
                    $name = htmlentities((string) $scope->getName());
                    $ul .= "<li>$name</li>\n";
                }
                $ul .= '</ul>';
                return $ul;
            },
        ];
    }

    #[Override]
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'url' => $this->getUrl(),
            'client_id' => $this->getClientId(),
            'username_field' => $this->getUsernameField(),
            'scopes' => array_map(
                fn(OpenidScope $s) => $s->jsonSerialize(),
                $this->getScopes()?->toArray() ?: []
            ),
        ];
    }

    /**
     * @return Collection<int, AccessUser>
     */
    public function getAccessUsers(): Collection
    {
        return $this->accessUsers;
    }

    public function addAccessUser(AccessUser $accessUser): static
    {
        if (!$this->accessUsers->contains($accessUser)) {
            $this->accessUsers->add($accessUser);
            $accessUser->setOpenid($this);
        }

        return $this;
    }

    public function removeAccessUser(AccessUser $accessUser): static
    {
        if ($this->accessUsers->removeElement($accessUser)) {
            // set the owning side to null (unless already changed)
            if ($accessUser->getOpenid() === $this) {
                $accessUser->setOpenid(null);
            }
        }

        return $this;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): static
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getLoginUrl(): ?string
    {
        return $this->loginUrl;
    }

    public function setLoginUrl(string $loginUrl): static
    {
        $this->loginUrl = $loginUrl;

        return $this;
    }

    public function getTenantJsonBaseurls()
    {
        $baseUrls = $this->getTenants()
            ->map(fn (Tenant $tenant) => $tenant->getBaseurl())
            ->toArray()
        ;
        return json_encode($baseUrls);
    }

    #[Groups(['action'])]
    public function getDeletable(): bool
    {
        return $this->getAccessUsers()->count() === 0;
    }
}
