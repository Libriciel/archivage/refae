<?php

namespace App\Entity;

use Ramsey\Uuid\Lazy\LazyUuidFromString;

interface MeilisearchIndexedInterface
{
    public function getId(): ?LazyUuidFromString;
    public function getTenantUrl(): ?string;
    public function isPublic(): ?bool;
    public function getState(): ?string;
    public function getFlattenLabels(): ?array;
    public function getVersion(): ?int;
    public function getIdentifier(): ?string;
}
