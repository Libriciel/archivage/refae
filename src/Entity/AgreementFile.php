<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Repository\AgreementFileRepository;
use App\Service\AttributeExtractor;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: AgreementFileRepository::class)]
class AgreementFile implements OneTenantIntegrityEntityInterface
{
    public const string TYPE_OTHER = 'other';
    public const array TYPE_VALUES = [self::TYPE_OTHER];

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['api', 'index', 'view'])]
    private ?LazyUuidFromString $id = null;

    #[ORM\ManyToOne(inversedBy: 'agreementFiles')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Agreement $agreement;

    #[ORM\ManyToOne(cascade: ['persist', 'remove'], inversedBy: 'agreementFiles')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['api'])]
    private ?File $file = null;

    #[ORM\Column(length: 255)]

    #[Assert\Choice(choices: AgreementFile::TYPE_VALUES, message: 'Choose a valid type.')]
    #[Groups(['api'])]
    private ?string $type = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['api', 'index', 'view'])]
    #[Attribute\ResultSet("Description", display: false)]
    private ?string $description = null;

    public function __construct(Agreement $agreement = null)
    {
        $this->agreement = $agreement;
    }

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function getAgreement(): ?Agreement
    {
        return $this->agreement;
    }

    public function setAgreement(?Agreement $agreement): static
    {
        $this->agreement = $agreement;

        return $this;
    }

    #[Groups(['action'])]
    public function getAgreementId(): string
    {
        return $this->agreement?->getId() ?? '';
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(?File $file): static
    {
        $this->file = $file;
        return $this;
    }

    #[Groups(['index', 'view'])]
    public function getFileId(): string
    {
        return $this->getFile()?->getId() ?? '';
    }

    #[Groups(['index', 'view'])]
    #[Attribute\ResultSet("Nom")]
    #[SerializedName("Nom")]
    public function getFileName(): string
    {
        return $this->getFile()?->getName() ?? '';
    }

    #[Groups(['index', 'view'])]
    #[Attribute\ResultSet("Type MIME")]
    #[SerializedName("Mime")]
    public function getFileMime(): string
    {
        return $this->getFile()?->getMime() ?? '';
    }

    #[Groups(['index'])]
    #[Attribute\ResultSet("Taille")]
    #[SerializedName("Taille")]
    public function getFileSize(): string
    {
        return $this->getFile()?->getReadableSize() ?? '';
    }

    #[Groups(['index', 'view'])]
    #[Attribute\ResultSet("Algorithme")]
    public function getFileHashAlgo(): string
    {
        return $this->getFile()?->getHashAlgo() ?? '';
    }

    #[Groups(['index', 'view'])]
    #[Attribute\ResultSet("Hash", display: false)]
    public function getFileHash(): string
    {
        return $this->getFile()?->getHash() ?? '';
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function export(): array
    {
        $extractor = new AttributeExtractor(AgreementFile::class);
        $properties = $extractor->getPropertiesHaving(ORM\Column::class);
        $normalized = $extractor->normalizeEntityByProperties($properties, $this);
        $normalized['file'] = $this->getFile()?->export();
        return $normalized;
    }

    public static function getTypeTranslations()
    {
        return [
            self::TYPE_OTHER => "Autre",
        ];
    }

    #[Groups(['index'])]
    public function getTypeTrad(): string
    {
        return static::getTypeTranslations()[$this->type];
    }

    #[Override]
    public function getTenant(): ?Tenant
    {
        return $this->getAgreement()?->getTenant();
    }
}
