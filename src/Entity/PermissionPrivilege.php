<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Repository\PermissionPrivilegeRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Ramsey\Uuid\Lazy\LazyUuidFromString;

#[ORM\Entity(repositoryClass: PermissionPrivilegeRepository::class)]
class PermissionPrivilege
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    private ?LazyUuidFromString $id = null;

    #[ORM\ManyToOne(inversedBy: 'permissionPrivileges')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Privilege $privilege = null;

    #[ORM\Column(length: 255)]
    private ?string $permissionClassname = null;

    #[ORM\Column(length: 255)]
    private ?string $action = null;

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function getPrivilege(): ?Privilege
    {
        return $this->privilege;
    }

    public function setPrivilege(?Privilege $privilege): static
    {
        $this->privilege = $privilege;

        return $this;
    }

    public function getPermissionClassname(): ?string
    {
        return $this->permissionClassname;
    }

    public function setPermissionClassname(string $permissionClassname): static
    {
        $this->permissionClassname = $permissionClassname;

        return $this;
    }

    public function getAction(): ?string
    {
        return $this->action;
    }

    public function setAction(string $action): static
    {
        $this->action = $action;

        return $this;
    }
}
