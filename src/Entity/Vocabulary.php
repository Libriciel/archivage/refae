<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\OpenApi\Model;
use App\ApiPlatform\LabelFilter;
use App\Doctrine\UuidGenerator;
use App\Entity\Attribute\ViewSection;
use App\Repository\VocabularyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

#[ORM\Entity(repositoryClass: VocabularyRepository::class)]
#[UniqueEntity([
    'tenant',
    'identifier',
    'version',
], "Ce vocabulaire contrôlé existe déjà")]
#[ApiResource(
    operations: [
        new GetCollection(
            uriTemplate: '/{tenant_url}/vocabulary-published',
            uriVariables: [],
            routeName: 'api_vocabulary_published_list',
            openapi: new Model\Operation(
                summary: "Liste des vocabulaires contrôlés publiés",
                description: "Liste des vocabulaires contrôlés publiés",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
        new Get(
            uriTemplate: '/{tenant_url}/vocabulary-published/{id}',
            routeName: 'api_vocabulary_published_id',
            openapi: new Model\Operation(
                summary: "Récupère un vocabulaire contrôlé publié",
                description: "Récupère un vocabulaire contrôlé publié",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                    new Model\Parameter(
                        name: 'id',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
        new Get(
            uriTemplate: '/{tenant_url}/vocabulary-by-identifier/{identifier}',
            routeName: 'api_vocabulary_published_identifier',
            openapi: new Model\Operation(
                summary: "Récupère un vocabulaire contrôlé par identifiant",
                description: "Récupère un vocabulaire contrôlé par identifiant",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                    new Model\Parameter(
                        name: 'identifier',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
    ],
    normalizationContext: [
        'groups' => ['api'],
    ]
)]
#[ApiFilter(SearchFilter::class, properties: ['identifier'])]
#[ApiFilter(LabelFilter::class, properties: ['labels'])]
class Vocabulary implements
    ActivityBypassAddEntityInterface,
    ActivitySubjectEntityInterface,
    EditableEntityInterface,
    DeletableEntityInterface,
    HaveFilesInterface,
    HaveLabelsEntityInterface,
    MeilisearchIndexedInterface,
    OneTenantIntegrityEntityInterface,
    PublishableEntityInterface,
    StateMachineEntityInterface,
    TranslatedTransitionsInterface
{
    use ArrayEntityTrait;
    use HaveLabelsEntityTrait;
    use VersionableEntityTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['api', 'action', 'meilisearch'])]
    private ?LazyUuidFromString $id = null;

    #[ORM\ManyToOne(targetEntity: Tenant::class, inversedBy: 'vocabularies')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Tenant $tenant = null;

    #[ORM\ManyToMany(targetEntity: Label::class, inversedBy: 'vocabularies', fetch: 'EAGER')]
    #[Groups(['api', 'index'])]
    private Collection $labels;

    #[ORM\OneToMany(mappedBy: 'vocabulary', targetEntity: Activity::class, orphanRemoval: true)]
    #[Groups(['api'])]
    private Collection $activities;

    #[ORM\Column(length: 255)]
    #[Attribute\ResultSet("Identifiant")]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Identifiant")]
    #[ViewSection('main')]
    private ?string $identifier = null;

    #[ORM\Column(length: 255)]
    #[Attribute\ResultSet("Nom")]
    #[SerializedName("Nom")]
    #[ViewSection('main')]
    #[Groups(['api', 'index', 'meilisearch', 'csv'])]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Attribute\ResultSet("Description", blacklist: true)]
    #[SerializedName("Description")]
    #[ViewSection('main')]
    #[Groups(['api', 'index', 'meilisearch', 'csv'])]
    private ?string $description = null;

    #[ORM\Column]
    #[Attribute\ResultSet("Public", display: false)]
    #[SerializedName("Public")]
    #[Groups(['api', 'index', 'meilisearch', 'csv'])]
    private ?bool $public = null;

    #[ORM\OneToMany(mappedBy: 'vocabulary', targetEntity: VocabularyFile::class, orphanRemoval: true)]
    private Collection $vocabularyFiles;

    #[ORM\OneToMany(mappedBy: 'vocabulary', targetEntity: Term::class, orphanRemoval: true)]
    #[Groups(['api'])]
    private Collection $terms;

    public function __construct()
    {
        $this->labels = new ArrayCollection();
        $this->vocabularyFiles = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->terms = new ArrayCollection();
    }

    #[Override]
    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    #[Override]
    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    #[Groups(['index_public'])]
    #[Attribute\ResultSet("Tenant")]
    #[SerializedName("Tenant")]
    public function getTenantName(): string
    {
        return $this->getTenant()->getName();
    }

    #[Override]
    public function setTenant(?Tenant $tenant): static
    {
        $this->tenant = $tenant;
        return $this;
    }

    /**
     * @return Collection<int, VocabularyFile>
     */
    public function getVocabularyFiles(): Collection
    {
        return $this->vocabularyFiles;
    }

    #[Attribute\ResultSet("Fichiers")]
    #[SerializedName("Fichiers")]
    #[Groups(['csv'])]
    public function getFilesForCsv(): string
    {
        return implode(
            "\n",
            array_map(
                fn(VocabularyFile $e) => $e->getFileName(),
                $this->getVocabularyFiles()->toArray()
            )
        );
    }

    #[Override]
    public function getFiles(): array
    {
        return $this->getVocabularyFiles()->toArray();
    }

    public function addVocabularyFile(VocabularyFile $vocabularyFile): static
    {
        if (!$this->vocabularyFiles->contains($vocabularyFile)) {
            $this->vocabularyFiles->add($vocabularyFile);
            $vocabularyFile->setVocabulary($this);
        }
        return $this;
    }

    public function removeVocabularyFile(VocabularyFile $vocabularyFile): static
    {
        if ($this->vocabularyFiles->removeElement($vocabularyFile)) {
            if ($vocabularyFile->getVocabulary() === $this) {
                $vocabularyFile->setVocabulary(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, Activity>
     */
    #[Override]
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    #[Override]
    public function addActivity(Activity $activity): static
    {
        if (!$this->activities->contains($activity)) {
            $this->activities->add($activity);
            $activity->setVocabulary($this);
        }
        return $this;
    }

    public function removeActivity(Activity $activity): static
    {
        if ($this->activities->removeElement($activity)) {
            if ($activity->getVocabulary() === $this) {
                $activity->setVocabulary(null);
            }
        }
        return $this;
    }

    #[Override]
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): static
    {
        $this->identifier = $identifier;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    #[Override]
    public function isPublic(): bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): static
    {
        $this->public = $public;
        return $this;
    }

    #[Override]
    public function getActivityName(): string
    {
        return sprintf("le vocabulaire contrôlé %s", $this->name);
    }

    /**
     * @return Collection<int, Term>
     */
    public function getTerms(): Collection
    {
        return $this->terms;
    }

    public function addTerm(Term $term): static
    {
        if (!$this->terms->contains($term)) {
            $this->terms->add($term);
            $term->setVocabulary($this);
        }
        return $this;
    }

    public function removeTerm(Term $term): static
    {
        if ($this->terms->removeElement($term)) {
            if ($term->getVocabulary() === $this) {
                $term->setVocabulary(null);
            }
        }
        return $this;
    }

    #[Override]
    #[Groups(['meilisearch'])]
    #[Attribute\ResultSet("Baseurl du tenant")]
    public function getTenantUrl(): ?string
    {
        return $this->getTenant()?->getBaseurl();
    }

    #[Groups(['index', 'view', 'csv'])]
    #[Attribute\ResultSet("Nombre de termes")]
    #[SerializedName("Nombre de termes")]
    public function getTermsCount(): int
    {
        return count($this->terms);
    }

    #[Override]
    public function getActivityScope(): ?Tenant
    {
        return $this->tenant;
    }

    #[Attribute\ResultSet("Public")]
    #[Groups(['view'])]
    #[SerializedName("Public")]
    #[ViewSection('other')]
    public function getReadablePublic(): string
    {
        return $this->public ? "Oui" : "Non";
    }
}
