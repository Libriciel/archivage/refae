<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Doctrine\UuidGenerator;
use App\Entity\Attribute\ViewSection;
use App\Repository\TermRepository;
use App\Service\AttributeExtractor;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

#[ApiResource]
#[ORM\Entity(repositoryClass: TermRepository::class)]
#[UniqueEntity(['vocabulary', 'name'], "Ce terme existe déjà")]
class Term implements
    EditableEntityInterface,
    DeletableEntityInterface,
    OneTenantIntegrityEntityInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['action', 'api'])]
    private ?LazyUuidFromString $id = null;

    #[ORM\ManyToOne(inversedBy: 'terms')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Vocabulary $vocabulary;

    #[ORM\Column(length: 255)]

    #[Groups(['index', 'view', 'api'])]
    #[Attribute\ResultSet("Terme")]
    #[SerializedName("Nom")]
    #[ViewSection('main')]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Attribute\ResultSet("Identifiant dans un référentiel")]
    #[Groups(['index', 'view', 'api'])]
    #[SerializedName("Identifiant")]
    #[ViewSection('main')]
    private ?string $identifier = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Attribute\ResultSet("Nom du référentiel")]
    #[Groups(['index', 'view', 'api'])]
    #[SerializedName("Référentiel")]
    #[ViewSection('main')]
    private ?string $directoryName = null;

    public function __construct(Vocabulary $vocabulary = null)
    {
        $this->vocabulary = $vocabulary;
    }

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function getVocabulary(): ?Vocabulary
    {
        return $this->vocabulary;
    }

    #[Groups(['action'])]
    public function getVocabularyId(): ?string
    {
        return $this->vocabulary->getId();
    }

    public function setVocabulary(?Vocabulary $vocabulary): static
    {
        $this->vocabulary = $vocabulary;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(?string $identifier): static
    {
        $this->identifier = $identifier;
        return $this;
    }

    public function getDirectoryName(): ?string
    {
        return $this->directoryName;
    }

    public function setDirectoryName(?string $directoryName): static
    {
        $this->directoryName = $directoryName;
        return $this;
    }

    #[Override]
    public function getEditable(): bool
    {
        return $this->getVocabulary()?->getEditable() ?? false;
    }

    #[Override]
    public function getDeletable(): bool
    {
        return $this->getVocabulary()?->getEditable() ?? false;
    }

    #[Override]
    public function getTenant(): ?Tenant
    {
        return $this->getVocabulary()?->getTenant();
    }

    public function export(): array
    {
        $extractor = new AttributeExtractor(Term::class);
        $properties = $extractor->getPropertiesHaving(ORM\Column::class);
        return $extractor->normalizeEntityByProperties($properties, $this);
    }
}
