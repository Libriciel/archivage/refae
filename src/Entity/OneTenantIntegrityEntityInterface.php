<?php

namespace App\Entity;

interface OneTenantIntegrityEntityInterface
{
    public function getTenant(): ?Tenant;
}
