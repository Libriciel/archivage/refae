<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Repository\PrivilegeUserRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Ramsey\Uuid\Lazy\LazyUuidFromString;

#[ORM\Entity(repositoryClass: PrivilegeUserRepository::class)]
class PrivilegeUser
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    private ?LazyUuidFromString $id = null;

    #[ORM\ManyToOne(inversedBy: 'privilegeUsers')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Privilege $privilege = null;

    #[ORM\ManyToOne(inversedBy: 'privilegeUsers')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Tenant $tenant = null;

    #[ORM\ManyToOne(inversedBy: 'privilegeUsers')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\Column]
    private ?bool $is_granted = null;

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function getPrivilege(): ?Privilege
    {
        return $this->privilege;
    }

    public function setPrivilege(?Privilege $privilege): static
    {
        $this->privilege = $privilege;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): static
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function isIsGranted(): ?bool
    {
        return $this->is_granted;
    }

    public function setIsGranted(bool $is_granted): static
    {
        $this->is_granted = $is_granted;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }
}
