<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\OpenApi\Model;
use App\Doctrine\UuidGenerator;
use App\Entity\Attribute\ViewSection;
use App\Repository\LabelRepository;
use App\Service\AttributeExtractor;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Stringable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Serializer\Attribute\Ignore;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [
        new GetCollection(
            uriTemplate: '/{tenant_url}/label-list',
            formats: ['json'],
            uriVariables: [],
            routeName: 'api_label_list',
            openapi: new Model\Operation(
                responses: [
                    '200' => [
                        'content' => [
                            'application/json' => [
                                'example' => [
                                    'group1:label1',
                                    'group1:label2',
                                    'group2:label1',
                                    'no_group_label1',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: "Liste des étiquettes sous la forme utilisable en filtre",
                description: "Liste des étiquettes sous la forme utilisable en filtre",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
            paginationEnabled: false,
            security: 'is_granted("acl/Label/view")',
            name: 'List labels',
        ),
    ],
    normalizationContext: [
        'groups' => ['api'],
    ]
)]
#[ORM\Entity(repositoryClass: LabelRepository::class)]
#[UniqueEntity(['tenant', 'labelGroup', 'name'], "Cette étiquette existe déjà", ignoreNull: false)]
class Label implements
    JsonSerializable,
    ViewEntityInterface,
    ActivitySubjectEntityInterface,
    OneTenantIntegrityEntityInterface,
    DeletableEntityInterface,
    Stringable
{
    use ArrayEntityTrait;

    public const string BANNED_CHARSET = '~:\/!?,;';

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[Groups(['action', 'api'])]
    private ?LazyUuidFromString $id = null;

    #[ORM\ManyToOne(inversedBy: 'labels')]
    #[ORM\JoinColumn(nullable: false)]
    #[Ignore]
    private ?Tenant $tenant = null;

    #[ORM\ManyToOne(fetch: 'EAGER', inversedBy: 'labels')]
    #[Attribute\Activity(label: "groupe")]
    #[Groups(['index'])]
    private ?LabelGroup $labelGroup = null;

    /**
     * @Assert\Regex(pattern="/[]/", match=false, message="Caractères interdits: ~:\/!?,;")
     */
    #[ORM\Column(length: 255)]
    #[Attribute\Activity]
    #[Attribute\ResultSet("Nom")]
    #[Groups(['api', 'index', 'view', 'csv'])]
    #[SerializedName("Nom")]
    #[ViewSection('main')]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Attribute\Activity(label: "description")]
    #[Attribute\ResultSet("Description", display: false)]
    #[Groups(['api', 'index', 'view', 'csv'])]
    #[SerializedName("Description")]
    #[ViewSection('main')]
    private ?string $description = null;

    #[ORM\OneToMany(mappedBy: 'label', targetEntity: Activity::class, orphanRemoval: true)]
    #[ORM\OrderBy(['id' => 'ASC'])]
    #[Ignore]
    private Collection $activities;

    #[ORM\ManyToMany(targetEntity: Profile::class, mappedBy: 'labels')]
    #[Ignore]
    private Collection $profiles;

    #[ORM\ManyToMany(targetEntity: Agreement::class, mappedBy: 'labels')]
    #[Ignore]
    private Collection $agreements;

    #[ORM\ManyToMany(targetEntity: Authority::class, mappedBy: 'labels')]
    #[Ignore]
    private Collection $authorities;

    #[ORM\ManyToMany(targetEntity: Vocabulary::class, mappedBy: 'labels')]
    #[Ignore]
    private Collection $vocabularies;

    #[ORM\ManyToMany(targetEntity: ServiceLevel::class, mappedBy: 'labels')]
    #[Ignore]
    private Collection $serviceLevels;

    #[ORM\ManyToMany(targetEntity: DocumentRule::class, mappedBy: 'labels')]
    #[Ignore]
    private Collection $documentRules;

    #[ORM\Column(length: 255, nullable: true)]
    #[Attribute\ResultSet("couleur")]
    #[Attribute\Activity]
    #[Groups(['index', 'view'])]
    private ?string $color = null;

    #[ORM\ManyToMany(targetEntity: ManagementRule::class, mappedBy: 'labels')]
    private Collection $managementRules;

    public function __construct()
    {
        $this->agreements = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->profiles = new ArrayCollection();
        $this->authorities = new ArrayCollection();
        $this->vocabularies = new ArrayCollection();
        $this->serviceLevels = new ArrayCollection();
        $this->documentRules = new ArrayCollection();
        $this->managementRules = new ArrayCollection();
    }

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    #[Override]
    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): static
    {
        $this->tenant = $tenant;
        return $this;
    }

    public function getLabelGroup(): ?LabelGroup
    {
        return $this->labelGroup;
    }

    #[Attribute\ResultSet("Groupe")]
    #[Groups(['api', 'index', 'view', 'csv'])]
    #[SerializedName("Groupe")]
    #[ViewSection('main')]
    public function getLabelGroupName(): ?string
    {
        return $this->labelGroup?->getName();
    }

    public function getLabelGroupDescription(): ?string
    {
        return $this->labelGroup?->getDescription();
    }

    public function setLabelGroup(?LabelGroup $labelGroup): static
    {
        $this->labelGroup = $labelGroup;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Collection<int, Activity>
     */
    #[Override]
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    #[Override]
    public function addActivity(Activity $activity): static
    {
        if (!$this->activities->contains($activity)) {
            $this->activities->add($activity);
            $activity->setLabel($this);
        }
        return $this;
    }

    public function removeActivity(Activity $activity): static
    {
        if ($this->activities->removeElement($activity)) {
            if ($activity->getLabel() === $this) {
                $activity->setLabel(null);
            }
        }
        return $this;
    }

    #[Override]
    public function getActivityName(): string
    {
        return sprintf("l'étiquette %s", $this->name);
    }

    #[Override]
    public function viewData(): array
    {
        return [
            "Nom" => 'name',
            "Description" => 'description',
            "Groupe" => 'labelGroup.name',
        ];
    }

    #[Override]
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'labelGroup' => $this->getLabelGroup()?->jsonSerialize(),
            'color' => $this->getColor(),
        ];
    }

    /**
     * @return Collection<int, Profile>
     */
    public function getProfiles(): Collection
    {
        return $this->profiles;
    }

    public function addProfile(Profile $profile): static
    {
        if (!$this->profiles->contains($profile)) {
            $this->profiles->add($profile);
            $profile->addLabel($this);
        }
        return $this;
    }

    public function removeProfile(Profile $profile): static
    {
        if ($this->profiles->removeElement($profile)) {
            $profile->removeLabel($this);
        }
        return $this;
    }

    /**
     * @return Collection<int, Agreement>
     */
    public function getAgreements(): Collection
    {
        return $this->agreements;
    }

    public function addAgreement(Agreement $agreement): static
    {
        if (!$this->agreements->contains($agreement)) {
            $this->agreements->add($agreement);
            $agreement->addLabel($this);
        }
        return $this;
    }

    public function removeAgreement(Agreement $agreement): static
    {
        if ($this->agreements->removeElement($agreement)) {
            $agreement->removeLabel($this);
        }
        return $this;
    }

    /**
     * @return Collection<int, Authority>
     */
    public function getAuthorities(): Collection
    {
        return $this->authorities;
    }

    public function addAuthority(Authority $authority): static
    {
        if (!$this->authorities->contains($authority)) {
            $this->authorities->add($authority);
            $authority->addLabel($this);
        }
        return $this;
    }

    public function removeAuthority(Authority $authority): static
    {
        if ($this->authorities->removeElement($authority)) {
            $authority->removeLabel($this);
        }
        return $this;
    }

    /**
     * @return Collection<int, ServiceLevel>
     */
    public function getServiceLevels(): Collection
    {
        return $this->serviceLevels;
    }

    public function addServiceLevel(ServiceLevel $serviceLevel): static
    {
        if (!$this->serviceLevels->contains($serviceLevel)) {
            $this->serviceLevels->add($serviceLevel);
            $serviceLevel->addLabel($this);
        }
        return $this;
    }

    public function removeServiceLevel(ServiceLevel $serviceLevel): static
    {
        if ($this->serviceLevels->removeElement($serviceLevel)) {
            $serviceLevel->removeLabel($this);
        }
        return $this;
    }

    /**
     * @return Collection<int, DocumentRule>
     */
    public function getDocumentRules(): Collection
    {
        return $this->documentRules;
    }

    public function addDocumentRule(DocumentRule $documentRule): static
    {
        if (!$this->documentRules->contains($documentRule)) {
            $this->documentRules->add($documentRule);
            $documentRule->addLabel($this);
        }
        return $this;
    }

    public function removeDocumentRule(DocumentRule $documentRule): static
    {
        if ($this->documentRules->removeElement($documentRule)) {
            $documentRule->removeLabel($this);
        }
        return $this;
    }

    public function export(): array
    {
        $extractor = new AttributeExtractor(Label::class);
        $properties = $extractor->getPropertiesHaving(ORM\Column::class);
        $normalized = $extractor->normalizeEntityByProperties($properties, $this);
        $normalized['group'] = $this->getLabelGroup()?->export();
        return $normalized;
    }

    public function getConcatLabelGroupName(string $joinChar = ':'): string
    {
        $groupName = $this->getLabelGroupName();
        if ($groupName) {
            return $groupName . $joinChar . $this->getName();
        }
        return $this->getName();
    }

    /**
     * @return Collection<int, Vocabulary>
     */
    public function getVocabularies(): Collection
    {
        return $this->vocabularies;
    }

    public function addVocabulary(Vocabulary $vocabulary): static
    {
        if (!$this->vocabularies->contains($vocabulary)) {
            $this->vocabularies->add($vocabulary);
            $vocabulary->addLabel($this);
        }

        return $this;
    }

    public function removeVocabulary(Vocabulary $vocabulary): static
    {
        if ($this->vocabularies->removeElement($vocabulary)) {
            $vocabulary->removeLabel($this);
        }

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): static
    {
        $this->color = $color;

        return $this;
    }

    #[Override]
    public function __toString(): string
    {
        return $this->getConcatLabelGroupName();
    }

    #[Groups(['action'])]
    #[Override]
    public function getDeletable(): bool
    {
        return $this->getAgreements()->count() === 0
            && $this->getAuthorities()->count() === 0
            && $this->getProfiles()->count() === 0
            && $this->getServiceLevels()->count() === 0
            && $this->getVocabularies()->count() === 0;
    }

    #[Override]
    public function getActivityScope(): ?Tenant
    {
        return $this->tenant;
    }

    /**
     * @return Collection<int, ManagementRule>
     */
    public function getManagementRules(): Collection
    {
        return $this->managementRules;
    }

    public function addManagementRule(ManagementRule $managementRule): static
    {
        if (!$this->managementRules->contains($managementRule)) {
            $this->managementRules->add($managementRule);
            $managementRule->addLabel($this);
        }

        return $this;
    }

    public function removeManagementRule(ManagementRule $managementRule): static
    {
        if ($this->managementRules->removeElement($managementRule)) {
            $managementRule->removeLabel($this);
        }

        return $this;
    }
}
