<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Repository\ProfileFileRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

#[UniqueEntity(['profile', 'file'], "Ce profil possède déjà ce fichier")]
#[ORM\Entity(repositoryClass: ProfileFileRepository::class)]
class ProfileFile implements OneTenantIntegrityEntityInterface
{
    public const string SCHEMA = 'schema';
    public const string OTHER = 'other';
    public const array TYPE_VALUES = [self::SCHEMA, self::OTHER];

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['api', 'index'])]
    private ?LazyUuidFromString $id = null;

    #[ORM\ManyToOne(cascade: ['persist', 'remove'], inversedBy: 'profileFiles')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['api'])]
    private ?File $file = null;

    #[ORM\ManyToOne(inversedBy: 'profileFiles')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Profile $profile;

    #[ORM\Column(length: 255)]
    #[Assert\Choice(choices: ProfileFile::TYPE_VALUES, message: 'Choose a valid type.')]
    #[Groups(['api'])]
    private ?string $type = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['api', 'index'])]
    private ?string $description = null;

    public function __construct(Profile $profile = null)
    {
        $this->profile = $profile;
    }

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(?File $file): static
    {
        $this->file = $file;
        return $this;
    }

    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function setProfile(?Profile $profile): static
    {
        $this->profile = $profile;
        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public static function getTypeTranslations()
    {
        return [
            self::SCHEMA => "Schéma",
            self::OTHER => "Autre"
        ];
    }


    #[Groups(['api', 'index', 'view'])]
    #[Attribute\ResultSet("Type")]
    #[SerializedName("Type")]
    public function getTypeTrad(): string
    {
        return static::getTypeTranslations()[$this->type];
    }

    public function setType(string $type): static
    {
        $this->type = $type;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    #[Groups(['index'])]
    #[Attribute\ResultSet("Nom")]
    #[SerializedName("Nom")]
    public function getFileName(): string
    {
        return $this->getFile()?->getName() ?? '';
    }

    #[Groups(['index'])]
    #[Attribute\ResultSet("Type MIME")]
    #[SerializedName("Mime")]
    public function getFileMime(): string
    {
        return $this->getFile()?->getMime() ?? '';
    }

    #[Groups(['index'])]
    #[Attribute\ResultSet("Taille")]
    #[SerializedName("Taille")]
    public function getFileSize(): string
    {
        return $this->getFile()?->getReadableSize() ?? '';
    }

    #[Groups(['index', 'view'])]
    #[Attribute\ResultSet("Algorithme")]
    public function getFileHashAlgo(): string
    {
        return $this->getFile()?->getHashAlgo() ?? '';
    }

    #[Groups(['index', 'view'])]
    #[Attribute\ResultSet("Hash", display: false)]
    public function getFileHash(): string
    {
        return $this->getFile()?->getHash() ?? '';
    }

    #[Override]
    public function getTenant(): ?Tenant
    {
        return $this->getProfile()?->getTenant();
    }

    #[Groups(['action'])]
    public function getProfileId(): string
    {
        return $this->getProfile()?->getId() ?? '';
    }
}
