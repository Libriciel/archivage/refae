<?php

namespace App\Entity;

use ArrayAccess;
use Doctrine\Common\Collections\Collection;

interface ActivitySubjectEntityInterface extends ArrayAccess
{
    public function getActivityName(): string;
    public function addActivity(Activity $activity): static;
    public function getActivities(): Collection;
    public function getActivityScope(): ?Tenant;
}
