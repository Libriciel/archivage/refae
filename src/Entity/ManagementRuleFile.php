<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Repository\ManagementRuleFileRepository;
use App\Service\AttributeExtractor;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Override;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

#[UniqueEntity(['managementRule', 'file'], "Cette règle de gestion possède déjà ce fichier")]
#[ORM\Entity(repositoryClass: ManagementRuleFileRepository::class)]
class ManagementRuleFile implements OneTenantIntegrityEntityInterface
{
    public const string TYPE_OTHER = 'other';
    public const array TYPE_VALUES = [self::TYPE_OTHER];

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['index'])]
    private ?string $id = null;

    #[ORM\ManyToOne(cascade: ['persist', 'remove'], inversedBy: 'managementRuleFiles')]
    #[ORM\JoinColumn(nullable: false)]
    private ?File $file = null;

    #[ORM\ManyToOne(inversedBy: 'managementRuleFiles')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ManagementRule $managementRule;

    #[ORM\Column(length: 255)]
    #[Assert\Choice(choices: ManagementRuleFile::TYPE_VALUES, message: 'Choose a valid type.')]
    private ?string $type = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['index'])]
    private ?string $description = null;

    public function __construct(ManagementRule $managementRule = null)
    {
        $this->managementRule = $managementRule;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(?File $file): static
    {
        $this->file = $file;
        return $this;
    }

    public function getManagementRule(): ?ManagementRule
    {
        return $this->managementRule;
    }

    public function setManagementRule(?ManagementRule $managementRule): static
    {
        $this->managementRule = $managementRule;
        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public static function getTypeTranslations()
    {
        return [
            self::TYPE_OTHER => "Autre",
        ];
    }

    #[Groups(['index'])]
    public function getTypeTrad(): string
    {
        return static::getTypeTranslations()[$this->type];
    }

    public function setType(string $type): static
    {
        $this->type = $type;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    #[Groups(['index'])]
    #[Attribute\ResultSet("Nom")]
    #[SerializedName("Nom")]
    public function getFileName(): string
    {
        return $this->getFile()?->getName() ?? '';
    }

    #[Groups(['index'])]
    #[Attribute\ResultSet("Type MIME")]
    #[SerializedName("Mime")]
    public function getFileMime(): string
    {
        return $this->getFile()?->getMime() ?? '';
    }

    #[Groups(['index'])]
    #[Attribute\ResultSet("Taille")]
    #[SerializedName("Taille")]
    public function getFileSize(): string
    {
        return $this->getFile()?->getReadableSize() ?? '';
    }

    #[Groups(['index'])]
    #[Attribute\ResultSet("Algorithme")]
    public function getFileHashAlgo(): string
    {
        return $this->getFile()?->getHashAlgo() ?? '';
    }

    #[Groups(['index', 'view'])]
    #[Attribute\ResultSet("Hash", display: false)]
    public function getFileHash(): string
    {
        return $this->getFile()?->getHash() ?? '';
    }

    #[Groups(['index'])]
    public function getManagementRuleId(): string
    {
        return $this->getManagementRule()?->getId() ?? '';
    }

    #[Groups(['index'])]
    public function getFileId(): string
    {
        return $this->getFile()?->getId() ?? '';
    }

    public function export(): array
    {
        $extractor = new AttributeExtractor(ManagementRuleFile::class);
        $properties = $extractor->getPropertiesHaving(ORM\Column::class);
        $normalized = $extractor->normalizeEntityByProperties($properties, $this);
        $normalized['file'] = $this->getFile()?->export();
        return $normalized;
    }

    #[Override]
    public function getTenant(): ?Tenant
    {
        return $this->getManagementRule()?->getTenant();
    }
}
