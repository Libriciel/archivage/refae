<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Repository\FileRepository;
use App\Service\AttributeExtractor;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Exception;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\Ignore;

#[ORM\Entity(repositoryClass: FileRepository::class)]
class File implements OneTenantIntegrityEntityInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['api'])]
    private ?LazyUuidFromString $id = null;

    #[ORM\Column(length: 512)]
    #[Groups(['api'])]
    private ?string $name = null;

    #[ORM\Column(type: Types::BIGINT)]
    #[Groups(['api'])]
    private ?string $size = null;

    #[ORM\Column(length: 255, options: ['default' => 'sha256'])]
    #[Groups(['api'])]
    private string $hash_algo = 'sha256';

    #[ORM\Column(length: 255)]
    #[Groups(['api'])]
    private ?string $hash = null;

    #[ORM\Column(length: 255)]
    #[Groups(['api'])]
    private ?string $mime = null;

    /**
     * @var resource|null
     */
    #[ORM\Column(type: Types::BINARY)]
    #[Groups(['api_file'])]
    private $content = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['api'])]
    private ?DateTimeInterface $created;

    #[ORM\OneToMany(mappedBy: 'file', targetEntity: ProfileFile::class, orphanRemoval: true)]
    private Collection $profileFiles;

    #[ORM\OneToMany(mappedBy: 'file', targetEntity: AgreementFile::class, orphanRemoval: true)]
    private Collection $agreementFiles;

    #[ORM\OneToMany(mappedBy: 'file', targetEntity: AuthorityFile::class, orphanRemoval: true)]
    private Collection $authorityFiles;

    #[ORM\OneToMany(mappedBy: 'file', targetEntity: ServiceLevelFile::class, orphanRemoval: true)]
    private Collection $serviceLevelFiles;

    #[ORM\OneToMany(mappedBy: 'file', targetEntity: VocabularyFile::class, orphanRemoval: true)]
    private Collection $vocabularyFiles;

    #[ORM\OneToMany(mappedBy: 'file', targetEntity: DocumentRuleFile::class, orphanRemoval: true)]
    private Collection $documentRuleFiles;

    #[ORM\OneToMany(mappedBy: 'file', targetEntity: ManagementRuleFile::class, orphanRemoval: true)]
    private Collection $managementRuleFiles;

    #[ORM\OneToOne(mappedBy: 'logoMain', cascade: ['persist', 'remove'])]
    private ?TenantConfiguration $tenantLogoMain = null;

    #[ORM\OneToOne(mappedBy: 'logoLogin', cascade: ['persist', 'remove'])]
    private ?TenantConfiguration $tenantLogoLogin = null;

    #[ORM\OneToOne(mappedBy: 'csvFile', cascade: ['persist', 'remove'])]
    private ?DocumentRuleImportSession $documentRuleImportSession = null;

    public function __construct()
    {
        $this->created = new DateTime();
        $this->profileFiles = new ArrayCollection();
        $this->agreementFiles = new ArrayCollection();
        $this->authorityFiles = new ArrayCollection();
        $this->serviceLevelFiles = new ArrayCollection();
        $this->vocabularyFiles = new ArrayCollection();
        $this->documentRuleFiles = new ArrayCollection();
        $this->managementRuleFiles = new ArrayCollection();
    }

    #[Groups(['index', 'view'])]
    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    #[Groups(['index', 'view'])]
    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getSize(): ?int
    {
        return $this->size;
    }

    public function setSize(int $size): static
    {
        $this->size = $size;
        return $this;
    }

    #[Groups(['index', 'view'])]
    #[SerializedName("Taille")]
    public function getReadableSize(): string
    {
        $bytes = $this->getSize() ?? 0;
        $i = floor(log($bytes) / log(1024));
        $sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        return sprintf('%.02F', $bytes / 1024 ** $i) * 1 . ' ' . $sizes[$i];
    }

    public function getHashAlgo(): ?string
    {
        return $this->hash_algo;
    }

    public function setHashAlgo(string $hash_algo): static
    {
        $this->hash_algo = $hash_algo;
        return $this;
    }

    #[Groups(['index', 'view'])]
    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function setHash(string $hash): static
    {
        $this->hash = $hash;
        return $this;
    }

    #[Groups(['index', 'view'])]
    public function getMime(): ?string
    {
        return $this->mime;
    }

    public function setMime(string $mime): static
    {
        $this->mime = $mime;
        return $this;
    }

    #[Ignore]
    public function getContent()
    {
        if (is_resource($this->content)) {
            rewind($this->content);
        }
        return $this->content;
    }

    public function setContent($content): static
    {
        $this->content = $content;
        return $this;
    }

    #[Groups(['index', 'view'])]
    public function getCreated(): ?DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(DateTimeInterface $created): static
    {
        $this->created = $created;
        return $this;
    }

    #[Groups(['view'])]
    public function getHashIntegrity(): bool
    {
        $currentHash = hash((string) $this->getHashAlgo(), stream_get_contents($this->getContent()));
        return $this->getHash() === $currentHash;
    }

    /**
     * @return Collection<int, ProfileFile>
     */
    public function getProfileFiles(): Collection
    {
        return $this->profileFiles;
    }

    public function addProfileFile(ProfileFile $profileFile): static
    {
        if (!$this->profileFiles->contains($profileFile)) {
            $this->profileFiles->add($profileFile);
            $profileFile->setFile($this);
        }
        return $this;
    }

    public function removeProfileFile(ProfileFile $profileFile): static
    {
        if ($this->profileFiles->removeElement($profileFile)) {
            if ($profileFile->getFile() === $this) {
                $profileFile->setFile(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, AgreementFile>
     */
    public function getAgreementFiles(): Collection
    {
        return $this->agreementFiles;
    }

    public function addAgreementFile(AgreementFile $agreementFile): static
    {
        if (!$this->agreementFiles->contains($agreementFile)) {
            $this->agreementFiles->add($agreementFile);
            $agreementFile->setFile($this);
        }
        return $this;
    }

    public function removeAgreementFile(AgreementFile $agreementFile): static
    {
        if ($this->agreementFiles->removeElement($agreementFile)) {
            if ($agreementFile->getFile() === $this) {
                $agreementFile->setFile(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, AuthorityFile>
     */
    public function getAuthorityFiles(): Collection
    {
        return $this->authorityFiles;
    }

    public function addAuthorityFile(AuthorityFile $authorityFile): static
    {
        if (!$this->authorityFiles->contains($authorityFile)) {
            $this->authorityFiles->add($authorityFile);
            $authorityFile->setFile($this);
        }
        return $this;
    }

    public function removeAuthorityFile(AuthorityFile $authorityFile): static
    {
        if ($this->authorityFiles->removeElement($authorityFile)) {
            if ($authorityFile->getFile() === $this) {
                $authorityFile->setFile(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, ServiceLevelFile>
     */
    public function getServiceLevelFiles(): Collection
    {
        return $this->serviceLevelFiles;
    }

    public function addServiceLevelFile(ServiceLevelFile $serviceLevelFile): static
    {
        if (!$this->serviceLevelFiles->contains($serviceLevelFile)) {
            $this->serviceLevelFiles->add($serviceLevelFile);
            $serviceLevelFile->setFile($this);
        }
        return $this;
    }

    public function removeServiceLevelFile(ServiceLevelFile $serviceLevelFile): static
    {
        if ($this->serviceLevelFiles->removeElement($serviceLevelFile)) {
            if ($serviceLevelFile->getFile() === $this) {
                $serviceLevelFile->setFile(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, VocabularyFile>
     */
    public function getVocabularyFiles(): Collection
    {
        return $this->vocabularyFiles;
    }

    public function addVocabularyFile(VocabularyFile $vocabularyFile): static
    {
        if (!$this->vocabularyFiles->contains($vocabularyFile)) {
            $this->vocabularyFiles->add($vocabularyFile);
            $vocabularyFile->setFile($this);
        }
        return $this;
    }

    public function removeVocabularyFile(VocabularyFile $vocabularyFile): static
    {
        if ($this->vocabularyFiles->removeElement($vocabularyFile)) {
            if ($vocabularyFile->getFile() === $this) {
                $vocabularyFile->setFile(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, DocumentRuleFile>
     */
    public function getDocumentRuleFiles(): Collection
    {
        return $this->documentRuleFiles;
    }

    public function addDocumentRuleFile(DocumentRuleFile $documentRuleFile): static
    {
        if (!$this->documentRuleFiles->contains($documentRuleFile)) {
            $this->documentRuleFiles->add($documentRuleFile);
            $documentRuleFile->setFile($this);
        }
        return $this;
    }

    public function removeDocumentRuleFile(DocumentRuleFile $documentRuleFile): static
    {
        if ($this->documentRuleFiles->removeElement($documentRuleFile)) {
            if ($documentRuleFile->getFile() === $this) {
                $documentRuleFile->setFile(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, ManagementRuleFile>
     */
    public function getManagementRuleFiles(): Collection
    {
        return $this->managementRuleFiles;
    }

    public function addManagementRuleFile(ManagementRuleFile $managementRuleFile): static
    {
        if (!$this->managementRuleFiles->contains($managementRuleFile)) {
            $this->managementRuleFiles->add($managementRuleFile);
            $managementRuleFile->setFile($this);
        }
        return $this;
    }

    public function removeManagementRuleFile(ManagementRuleFile $managementRuleFile): static
    {
        if ($this->managementRuleFiles->removeElement($managementRuleFile)) {
            if ($managementRuleFile->getFile() === $this) {
                $managementRuleFile->setFile(null);
            }
        }
        return $this;
    }

    #[Override]
    public function getTenant(): ?Tenant
    {
        if ($this->getProfileFiles()->count()) {
            $profileFile = $this->getProfileFiles()->current();
            return $profileFile->getProfile()?->getTenant();
        }
        if ($this->getAgreementFiles()->count()) {
            $agreementFile = $this->getAgreementFiles()->current();
            return $agreementFile->getAgreement()?->getTenant();
        }
        if ($this->getAuthorityFiles()->count()) {
            $authorityFile = $this->getAuthorityFiles()->current();
            return $authorityFile->getAuthority()?->getTenant();
        }
        if ($this->getServiceLevelFiles()->count()) {
            $serviceLevelFile = $this->getServiceLevelFiles()->current();
            return $serviceLevelFile->getServiceLevel()?->getTenant();
        }
        if ($this->getVocabularyFiles()->count()) {
            $vocabularyFile = $this->getVocabularyFiles()->current();
            return $vocabularyFile->getVocabulary()?->getTenant();
        }
        throw new Exception(sprintf('file %s does not have linked tenant entity', $this->getId()));
    }

    public function export(): array
    {
        $extractor = new AttributeExtractor(File::class);
        $properties = $extractor->getPropertiesHaving(ORM\Column::class);
        unset($properties['content']);
        return $extractor->normalizeEntityByProperties($properties, $this);
    }

    public function getTenantLogoMain(): ?TenantConfiguration
    {
        return $this->tenantLogoMain;
    }

    public function setTenantLogoMain(?TenantConfiguration $tenantLogoMain): static
    {
        // unset the owning side of the relation if necessary
        if ($tenantLogoMain === null && $this->tenantLogoMain !== null) {
            $this->tenantLogoMain->setLogoMain(null);
        }

        // set the owning side of the relation if necessary
        if ($tenantLogoMain !== null && $tenantLogoMain->getLogoMain() !== $this) {
            $tenantLogoMain->setLogoMain($this);
        }

        $this->tenantLogoMain = $tenantLogoMain;

        return $this;
    }

    public function getTenantLogoLogin(): ?TenantConfiguration
    {
        return $this->tenantLogoLogin;
    }

    public function setTenantLogoLogin(?TenantConfiguration $tenantLogoLogin): static
    {
        // unset the owning side of the relation if necessary
        if ($tenantLogoLogin === null && $this->tenantLogoLogin !== null) {
            $this->tenantLogoLogin->setLogoLogin(null);
        }

        // set the owning side of the relation if necessary
        if ($tenantLogoLogin !== null && $tenantLogoLogin->getLogoLogin() !== $this) {
            $tenantLogoLogin->setLogoLogin($this);
        }

        $this->tenantLogoLogin = $tenantLogoLogin;

        return $this;
    }

    public function getDocumentRuleImportSession(): ?DocumentRuleImportSession
    {
        return $this->documentRuleImportSession;
    }

    public function setDocumentRuleImportSession(DocumentRuleImportSession $documentRuleImportSession): static
    {
        // set the owning side of the relation if necessary
        if ($documentRuleImportSession->getCsvFile() !== $this) {
            $documentRuleImportSession->setCsvFile($this);
        }

        $this->documentRuleImportSession = $documentRuleImportSession;

        return $this;
    }
}
