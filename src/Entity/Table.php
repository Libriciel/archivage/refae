<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Repository\TableRepository;
use App\ResultSet\Table\Memory as TableMemory;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Ramsey\Uuid\Lazy\LazyUuidFromString;

#[ORM\Entity(repositoryClass: TableRepository::class)]
#[ORM\Table(name: '`table`')]
class Table
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    private ?LazyUuidFromString $id = null;

    #[ORM\Column(length: 255)]
    private ?string $dom_id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $active_line = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $favorites = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $columns = null;

    #[ORM\Column(nullable: true)]
    private ?int $max_per_page = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $user_id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $guest_token = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $guest_last_access = null;

    #[ORM\OneToMany(mappedBy: 'table', targetEntity: TableFilters::class, orphanRemoval: true)]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private Collection $filters;

    public function __construct()
    {
        $this->filters = new ArrayCollection();
    }

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    /**
     * @fixme ? utile lors de la sauvegarde des filtres ?
     * @param $id
     * @return $this
     */
    public function setId($id): static
    {
        if (is_string($id)) {
            $id = new LazyUuidFromString($id);
        }
        $this->id = $id;

        return $this;
    }

    public function getUserId(): ?string
    {
        return $this->user_id;
    }

    public function setUserId(string $user_id): static
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getDomId(): ?string
    {
        return $this->dom_id;
    }

    public function setDomId(string $dom_id): static
    {
        $this->dom_id = $dom_id;

        return $this;
    }

    public function getActiveLine(): ?string
    {
        return $this->active_line;
    }

    public function setActiveLine(?string $active_line): static
    {
        $this->active_line = $active_line;

        return $this;
    }

    public function getFavorites(): ?string
    {
        return $this->favorites;
    }

    public function setFavorites(?string $favorites): static
    {
        $this->favorites = $favorites;

        return $this;
    }

    public function getColumns(): ?string
    {
        return $this->columns;
    }

    public function setColumns(?string $columns): static
    {
        $this->columns = $columns;

        return $this;
    }

    public function getMaxPerPage(): ?int
    {
        return $this->max_per_page ?: ($_ENV['TABLE_PAGE_LIMIT'] ?? 50);
    }

    public function setMaxPerPage(?int $max_per_page): static
    {
        $this->max_per_page = $max_per_page;

        return $this;
    }

    public function getGuestToken(): ?string
    {
        return $this->guest_token;
    }

    public function setGuestToken(?string $guest_token): static
    {
        $this->guest_token = $guest_token;

        return $this;
    }

    public function getGuestLastAccess(): ?DateTimeInterface
    {
        return $this->guest_last_access;
    }

    public function setGuestLastAccess(?DateTimeInterface $guest_last_access): static
    {
        $this->guest_last_access = $guest_last_access;

        return $this;
    }

    public function getMemory(): TableMemory
    {
        $memory = new TableMemory();
        $memory->activeLine = $this->getActiveLine();
        if ($favorites = $this->getFavorites()) {
            $memory->favorites = explode(',', $favorites);
        }
        if ($columns = $this->getColumns()) {
            $memory->columns = explode(',', $columns);
        }
        $memory->max_per_page = $this->getMaxPerPage();
        return $memory;
    }

    /**
     * @return Collection<int, TableFilters>
     */
    public function getFilters(): Collection
    {
        return $this->filters;
    }

    public function addFilter(TableFilters $filter): static
    {
        if (!$this->filters->contains($filter)) {
            $this->filters->add($filter);
            $filter->setTable($this);
        }
        return $this;
    }

    public function removeFilter(TableFilters $filter): static
    {
        if ($this->filters->removeElement($filter)) {
            if ($filter->getTable() === $this) {
                $filter->setTable(null);
            }
        }
        return $this;
    }
}
