<?php

namespace App\Entity;

interface TranslatedTransitionsInterface
{
    public function getStateTranslations(): array;
    public function getTransitionTranslations(): array;
}
