<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\OpenApi\Model;
use App\ApiPlatform\LabelFilter;
use App\Doctrine\UuidGenerator;
use App\Entity\Attribute\ViewSection;
use App\Repository\ProfileRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

#[ORM\Entity(repositoryClass: ProfileRepository::class)]
#[UniqueEntity(['tenant', 'identifier', 'version'], "Ce profil existe déjà")]
#[ApiResource(
    operations: [
        new GetCollection(
            uriTemplate: '/{tenant_url}/profile-published',
            uriVariables: [],
            routeName: 'api_profile_published_list',
            openapi: new Model\Operation(
                summary: "Liste des profils d'archives publiés",
                description: "Liste des profils d'archives publiés",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
        new Get(
            uriTemplate: '/{tenant_url}/profile-published/{id}',
            routeName: 'api_profile_published_id',
            openapi: new Model\Operation(
                summary: "Récupère un profil d'archives publié",
                description: "Récupère un profil d'archives publié",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                    new Model\Parameter(
                        name: 'id',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
        new Get(
            uriTemplate: '/{tenant_url}/profile-by-identifier/{identifier}',
            routeName: 'api_profile_published_identifier',
            openapi: new Model\Operation(
                summary: "Récupère un profil d'archives par identifiant",
                description: "Récupère un profil d'archives par identifiant",
                parameters: [
                    new Model\Parameter(
                        name: 'tenant_url',
                        in: 'path',
                        required: true,
                    ),
                    new Model\Parameter(
                        name: 'identifier',
                        in: 'path',
                        required: true,
                    ),
                ],
            ),
        ),
    ],
    normalizationContext: [
        'groups' => ['api'],
    ]
)]
#[ApiFilter(SearchFilter::class, properties: ['identifier'])]
#[ApiFilter(LabelFilter::class, properties: ['labels'])]
class Profile implements
    ActivityBypassAddEntityInterface,
    ActivitySubjectEntityInterface,
    EditableEntityInterface,
    DeletableEntityInterface,
    HaveFilesInterface,
    HaveLabelsEntityInterface,
    MeilisearchIndexedInterface,
    OneTenantIntegrityEntityInterface,
    PublishableEntityInterface,
    StateMachineEntityInterface,
    TranslatedTransitionsInterface
{
    use ArrayEntityTrait;
    use HaveLabelsEntityTrait;
    use VersionableEntityTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    #[Groups(['api', 'index', 'meilisearch'])]
    private ?LazyUuidFromString $id = null;

    #[ORM\ManyToOne(targetEntity: Tenant::class, inversedBy: 'profiles')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Tenant $tenant = null;

    #[ORM\Column(length: 255)]
    #[Attribute\ResultSet("Identifiant")]
    #[Groups(['api', 'index', 'meilisearch', 'view', 'csv'])]
    #[SerializedName("Identifiant")]
    #[ViewSection('main')]
    private ?string $identifier = null;

    #[ORM\Column(length: 255)]
    #[Attribute\ResultSet("Nom")]
    #[SerializedName("Nom")]
    #[ViewSection('main')]
    #[Groups(['api', 'index', 'meilisearch', 'csv'])]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Attribute\ResultSet("Description", blacklist: true)]
    #[SerializedName("Description")]
    #[ViewSection('main')]
    #[Groups(['api', 'index', 'meilisearch', 'csv'])]
    private ?string $description = null;

    #[ORM\Column]
    #[Attribute\ResultSet("Public", display: false)]
    #[SerializedName("Public")]
    #[Groups(['api', 'index', 'meilisearch', 'csv'])]
    private bool $public = false;

    #[ORM\OneToMany(mappedBy: 'profile', targetEntity: Activity::class, orphanRemoval: true)]
    #[Groups(['api'])]
    private Collection $activities;

    #[ORM\OneToMany(mappedBy: 'profile', targetEntity: ProfileFile::class, orphanRemoval: true)]
    #[Groups(['api'])]
    private Collection $profileFiles;

    #[ORM\ManyToMany(targetEntity: Label::class, inversedBy: 'profiles', fetch: 'EAGER')]
    #[Groups(['api', 'index'])]
    private Collection $labels;

    #[ORM\OneToMany(mappedBy: 'Profile', targetEntity: DocumentRule::class)]
    private Collection $documentRules;

    public function __construct()
    {
        $this->activities = new ArrayCollection();
        $this->profileFiles = new ArrayCollection();
        $this->labels = new ArrayCollection();
        $this->documentRules = new ArrayCollection();
    }

    #[Override]
    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    #[Override]
    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    #[Groups(['index_public'])]
    #[Attribute\ResultSet("Tenant")]
    #[SerializedName("Tenant")]
    public function getTenantName(): string
    {
        return $this->getTenant()->getName();
    }

    #[Override]
    public function setTenant(?Tenant $tenant): static
    {
        $this->tenant = $tenant;
        return $this;
    }

    #[Override]
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): static
    {
        $this->identifier = $identifier;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    #[Override]
    public function isPublic(): bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): static
    {
        $this->public = $public;
        return $this;
    }

    #[Override]
    public function getActivityName(): string
    {
        return sprintf("le profil %s", $this->name);
    }

    /**
     * @return Collection<int, Activity>
     */
    #[Override]
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    #[Override]
    public function addActivity(Activity $activity): static
    {
        if (!$this->activities->contains($activity)) {
            $this->activities->add($activity);
            $activity->setProfile($this);
        }
        return $this;
    }

    public function removeActivity(Activity $activity): static
    {
        if ($this->activities->removeElement($activity)) {
            if ($activity->getProfile() === $this) {
                $activity->setProfile(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, ProfileFile>
     */
    public function getProfileFiles(): Collection
    {
        return $this->profileFiles;
    }

    public function addProfileFile(ProfileFile $profileFile): static
    {
        if (!$this->profileFiles->contains($profileFile)) {
            $this->profileFiles->add($profileFile);
            $profileFile->setProfile($this);
        }
        return $this;
    }

    public function removeProfileFile(ProfileFile $profileFile): static
    {
        if ($this->profileFiles->removeElement($profileFile)) {
            if ($profileFile->getProfile() === $this) {
                $profileFile->setProfile(null);
            }
        }
        return $this;
    }

    #[Groups(['index', 'view'])]
    #[Attribute\ResultSet("Schémas")]
    #[SerializedName("Schémas")]
    #[ViewSection('other')]
    public function getSchemaNamesString(): string
    {
        return implode(', ', $this->getSchemaNames());
    }

    public function getSchemaNames()
    {
        return array_map(
            fn(ProfileFile $f) => $f->getFile()?->getName() ?? '',
            array_filter(
                $this->getProfileFiles()->toArray(),
                fn(ProfileFile $f) => $f->getType() === ProfileFile::SCHEMA
            )
        );
    }

    #[Groups(['csv'])]
    #[Attribute\ResultSet("Schémas")]
    public function getProfilesForCsv()
    {
        return implode("\n", $this->getSchemaNames());
    }

    #[Override]
    #[Groups(['meilisearch'])]
    #[Attribute\ResultSet("Baseurl du tenant")]
    public function getTenantUrl(): ?string
    {
        return $this->getTenant()?->getBaseurl();
    }

    #[Override]
    public function getFiles(): array
    {
        return $this->getProfileFiles()->toArray();
    }

    #[Override]
    public function getActivityScope(): ?Tenant
    {
        return $this->tenant;
    }

    /**
     * @return Collection<int, DocumentRule>
     */
    public function getDocumentRules(): Collection
    {
        return $this->documentRules;
    }

    public function addDocumentRule(DocumentRule $documentRule): static
    {
        if (!$this->documentRules->contains($documentRule)) {
            $this->documentRules->add($documentRule);
            $documentRule->setProfile($this);
        }

        return $this;
    }

    public function removeDocumentRule(DocumentRule $documentRule): static
    {
        if ($this->documentRules->removeElement($documentRule)) {
            // set the owning side to null (unless already changed)
            if ($documentRule->getProfile() === $this) {
                $documentRule->setProfile(null);
            }
        }

        return $this;
    }

    #[Attribute\ResultSet("Public")]
    #[Groups(['view'])]
    #[SerializedName("Public")]
    #[ViewSection('other')]
    public function getReadablePublic(): string
    {
        return $this->public ? "Oui" : "Non";
    }
}
