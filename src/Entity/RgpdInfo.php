<?php

namespace App\Entity;

use App\Doctrine\UuidGenerator;
use App\Repository\RgpdInfoRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;

#[ORM\Entity(repositoryClass: RgpdInfoRepository::class)]
class RgpdInfo implements OneTenantOrNoneIntegrityEntityInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(UuidGenerator::class)]
    #[Column(type: 'uuid', unique: true)]
    private ?LazyUuidFromString $id = null;

    #[ORM\OneToOne(inversedBy: 'rgpdInfo', cascade: ['persist', 'remove'])]
    private ?Tenant $tenant = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $orgName = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $orgAddress = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $orgSiret = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $orgApe = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $orgPhone = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $orgEmail = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $ceoName = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $ceoFunction = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $dpoName = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $dpoEmail = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $comment = null;

    public function getId(): ?LazyUuidFromString
    {
        return $this->id;
    }

    #[Override]
    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): static
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getOrgName(): ?string
    {
        return $this->orgName;
    }

    public function setOrgName(?string $orgName): static
    {
        $this->orgName = $orgName;

        return $this;
    }

    public function getOrgAddress(): ?string
    {
        return $this->orgAddress;
    }

    public function setOrgAddress(?string $orgAddress): static
    {
        $this->orgAddress = $orgAddress;

        return $this;
    }

    public function getOrgSiret(): ?string
    {
        return $this->orgSiret;
    }

    public function setOrgSiret(?string $orgSiret): static
    {
        $this->orgSiret = $orgSiret;

        return $this;
    }

    public function getOrgApe(): ?string
    {
        return $this->orgApe;
    }

    public function setOrgApe(?string $orgApe): static
    {
        $this->orgApe = $orgApe;

        return $this;
    }

    public function getOrgPhone(): ?string
    {
        return $this->orgPhone;
    }

    public function setOrgPhone(?string $orgPhone): static
    {
        $this->orgPhone = $orgPhone;

        return $this;
    }

    public function getOrgEmail(): ?string
    {
        return $this->orgEmail;
    }

    public function setOrgEmail(?string $orgEmail): static
    {
        $this->orgEmail = $orgEmail;

        return $this;
    }

    public function getCeoName(): ?string
    {
        return $this->ceoName;
    }

    public function setCeoName(?string $ceoName): static
    {
        $this->ceoName = $ceoName;

        return $this;
    }

    public function getCeoFunction(): ?string
    {
        return $this->ceoFunction;
    }

    public function setCeoFunction(?string $ceoFunction): static
    {
        $this->ceoFunction = $ceoFunction;

        return $this;
    }

    public function getDpoName(): ?string
    {
        return $this->dpoName;
    }

    public function setDpoName(?string $dpoName): static
    {
        $this->dpoName = $dpoName;

        return $this;
    }

    public function getDpoEmail(): ?string
    {
        return $this->dpoEmail;
    }

    public function setDpoEmail(?string $dpoEmail): static
    {
        $this->dpoEmail = $dpoEmail;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): static
    {
        $this->comment = $comment;

        return $this;
    }
}
