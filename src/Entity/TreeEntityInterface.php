<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;

interface TreeEntityInterface
{
    public function getParent(): ?self;
    public function setParent(?self $parent): static;
    public function getLft(): ?int;
    public function setLft(?string $lft): static;
    public function getRght(): ?int;
    public function setRght(?string $rght): static;
    public function getChildren(): Collection;
    public function setPath(string $path): ?self;
    public function getPath(): ?string;
    public function getRoot(): ?self;
    public function getName(): ?string;
}
