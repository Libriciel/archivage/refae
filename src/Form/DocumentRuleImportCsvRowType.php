<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\DocumentRule;
use App\Entity\DocumentRuleImportSession;
use App\Entity\RuleType;
use App\Entity\VersionableEntityInterface;
use App\Repository\CategoryRepository;
use App\Repository\ManagementRuleRepository;
use App\Repository\RuleTypeRepository;
use App\Service\LoggedUser;
use App\Service\Manager\DocumentRuleManager;
use App\Service\Translit;
use Override;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentRuleImportCsvRowType extends AbstractType
{
    /**
     * @var RuleType[]
     */
    private array $ruleTypes = [];
    private ?DocumentRule $documentRule = null;

    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly CategoryRepository $categoryRepository,
        private readonly DocumentRuleManager $documentRuleManager,
        private readonly LoggedUser $loggedUser,
        private readonly ManagementRuleRepository $managementRuleRepository,
        private readonly RuleTypeRepository $ruleTypeRepository,
    ) {
    }

    /**
     * @param FormBuilderInterface|FormBuilder $builder
     * @param array $options
     * @return void
     */
    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var DocumentRuleImportSession $importSession */
        $importSession = $options['importSession'];
        $types = json_decode((string) $importSession->getColumnTypes(), true);

        $request = $this->requestStack->getCurrentRequest();
        $tenantUrl = $request->get('tenant_url');
        $this->documentRule = $this->documentRuleManager->newDocumentRule($this->loggedUser->tenant());
        $this->documentRule->setState(VersionableEntityInterface::S_PUBLISHED);
        $this->documentRule->setPublic(true);

        foreach ($this->ruleTypeRepository->findBy(['tenant' => null]) as $ruleType) {
            $this->ruleTypes[$ruleType->getIdentifier()] = $ruleType;
        }

        $identified = [];
        $missings = [];
        $label = null;
        $level = null;
        foreach ($options['rawData'] as $i => $value) {
            if (!$value) {
                continue;
            }
            $value = trim((string) $value, " \n\r\t\v\0.-");
            $this->identifyDefaults(
                $types["col-$i"] ?? '',
                $value,
                $identified,
                $missings,
            );
        }
        if (isset($identified['label'])) {
            $label = $identified['label'];
        }
        if (isset($identified['final_action'])) {
            $finalAction = $identified['final_action'];
        }
        if (isset($identified['level'])) {
            $level = $identified['level'];
        } elseif (isset($identified['extracted_level'])) {
            $level = $identified['extracted_level'];
        }
        if (empty($identified['type'])) {
            $identified['type'] = 'category';
        }
        $parentCategory = $importSession->getLastCategory();
        $lastLevel = $importSession->getLastCategoryLevel();
        if ($lastLevel !== null) {
            $identified['level'] = $lastLevel;
        }
        if ($parentCategory && $lastLevel && isset($level)) {
            while ($lastLevel >= $level && $parentCategory) {
                $parentCategory = $parentCategory->getParent();
                $lastLevel--;
            }
            $identified['level'] = $level;
        }
        $identified['parent_category'] = $parentCategory;
        if ($parentCategory) {
            $this->documentRule->addCategory($parentCategory);
        }
        $builder->setData([
            'identified' => $identified,
            'missings' => $missings,
            'documentRule' => $this->documentRule,
        ]);

        $builder->add('level', HiddenType::class, [
            'data' => $level ?? 0,
        ]);
        $builder->add('type', ChoiceType::class, [
            'label' => "Catégorie ou Règle de gestion ?",
            'choices' => [
                "Catégorie" => 'category',
                "Règle documentaire" => 'document_rule',
            ],
            'data' => isset($finalAction) ? 'document_rule' : 'category',
        ]);
        // findOptions défini $category->getPath() -> utiliser avant choice_label
        $categories = $this->categoryRepository->findOptions($tenantUrl);
        $builder
            ->add('category_name', null, [
                'label' => "Nom de la catégorie",
                'help' => "(ne peut contenir le caractère > ou ⮕)",
                'data' => $label ?? null,
            ])
            ->add('category_description', null, [
                'label' => "Description de la catégorie",
                'required' => false,
            ])
            ->add('category_parent', EntityType::class, [
                'label' => "Catégorie parente de la catégorie",
                'class' => Category::class,
                'choice_label' => fn (Category $category) => $category->getPath(),
                'placeholder' => "-- Choisir un parent --",
                'required' => false,
                'attr' => [
                    'data-s2' => true,
                ],
                'choices' => $categories,
                'data' => $parentCategory,
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'importSession' => null,
            'rawData' => [],
        ]);
    }

    private function identifyDefaults(
        string $default,
        string $value,
        array &$identified,
        array &$missings,
    ): void {
        switch ($default) {
            case 'access_rule_comment':
                $identified['access_rule_comment'] = $value;
                $this->documentRule->setAccessRuleNote($value);
                break;
            case 'access_rule_duration':
                $identified['access_rule_duration'] = $value;
                $accessRule = $this->managementRuleRepository->findOneBy([
                    'tenant' => $this->loggedUser->tenant(),
                    'name' => $value,
                    'ruleType' => $this->ruleTypes['AccessRule'],
                    'state' => VersionableEntityInterface::S_PUBLISHED,
                ]);
                if ($accessRule) {
                    $this->documentRule->setAccessRule($accessRule);
                } else {
                    $missings['access_rule'] = $value;
                }
                break;
            case 'comment':
                $identified['comment'] = $value;
                $this->documentRule->setObservation($value);
                break;
            case 'document_support':
                $identified['document_support'] = $value;
                switch (Translit::asciiToLower($value)) {
                    case 'electronique':
                    case 'numerique':
                        $this->documentRule->setDocumentSupport(DocumentRule::ELECTRONIC);
                        break;
                    case 'papier':
                        $this->documentRule->setDocumentSupport(DocumentRule::PAPER);
                        break;
                    case 'electronique et papier':
                    case 'numerique et papier':
                    case 'papier et numerique':
                    case 'papier et electronique':
                        $this->documentRule->setDocumentSupport(DocumentRule::MIXED);
                        break;
                }
                break;
            case 'dua_comment':
                $identified['dua_comment'] = $value;
                $this->documentRule->setAppraisalRuleNote($value);
                break;
            case 'dua_duration':
                $identified['dua_duration'] = $value;
                $identified['type'] = 'document_rule';
                $appraisalRule = $this->managementRuleRepository->findOneBy([
                    'tenant' => $this->loggedUser->tenant(),
                    'name' => $value,
                    'ruleType' => $this->ruleTypes['AppraisalRule'],
                    'state' => VersionableEntityInterface::S_PUBLISHED,
                ]);
                if ($appraisalRule) {
                    $this->documentRule->setAppraisalRule($appraisalRule);
                } else {
                    $missings['appraisal_rule'] = $value;
                }
                break;
            case 'final_action':
                $identified['final_action'] = $value;
                switch (Translit::asciiToLower($value)) {
                    case 'eliminer':
                    case 'destruction':
                    case 'detruire':
                        $this->documentRule->setAppraisalRuleFinalAction(DocumentRule::DESTROY);
                        break;
                    case 'conserver':
                        $this->documentRule->setAppraisalRuleFinalAction(DocumentRule::KEEP);
                        break;
                    case 'tri selectif':
                    case 'trier':
                        $this->documentRule->setAppraisalRuleFinalAction(DocumentRule::SORT);
                        break;
                }
                break;
            case 'final_action_comment':
                $identified['final_action_comment'] = $value;
                $this->documentRule->setStorageRuleNote($value);
                break;
            case 'label':
                // Récupère le niveau lorsque la valeur est préfixé par un num.
                // ex: "5. Marché publics" séparé en "5. " et "Marché publics", "5. " est trim en "5"
                if (preg_match('/^[\d. -]+/', $value, $m) && empty($identified['level'])) {
                    $level = trim($m[0], " \n\r\t\v\0.-");
                    $value = substr($value, strlen($m[0]));
                    // transforme un niveau sous la forme 4.7.8 en 8
                    if (preg_match('/\d+$/', $level, $m2) && $m2[0] !== $level) {
                        $level = $m2[0];
                    }
                    $identified['extracted_level'] = (int)$level;
                }
                $identified['label'] = $value;
                $this->documentRule->setName($value);
                $this->documentRule->setIdentifier($value);
                break;
            case 'level':
                $identified['level'] = (int)$value;
                break;
            case 'technical_metadata':
                $identified['technical_metadata'] = $value;
                $this->documentRule->setTechnicalMetadata($value);
                break;
        }
    }
}
