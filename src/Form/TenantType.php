<?php

namespace App\Form;

use App\Entity\Tenant;
use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TenantType extends AbstractType
{
    public string $url;

    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'label' => "Nom"
            ])
            ->add('baseurl', null, [
                'label' => "Base de l'url",
                'help' => "Pas de caractères spéciaux, pas de tiret bas (underscore) en premier,"
                    . " et les valeurs 'admin', 'api' ou 'public' sont interdites"
            ])
            ->add('description', null, [
                'label' => "Description"
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Tenant::class,
        ]);
    }
}
