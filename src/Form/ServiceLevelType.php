<?php

namespace App\Form;

use App\Entity\Label;
use App\Entity\ServiceLevel;
use App\Repository\LabelRepository;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ServiceLevelType extends AbstractType
{
    public function __construct(
        private readonly RequestStack $requestStack,
    ) {
    }

    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $request = $this->requestStack->getCurrentRequest();
        $tenantUrl = $request->get('tenant_url');
        $builder
            ->add('identifier', null, [
                'label' => "Identifiant",
                'disabled' => $builder->getData()?->getVersion() !== 1,
            ])
            ->add('name', null, [
                'label' => "Nom",
            ])
            ->add('description', null, [
                'label' => "Description",
            ])
            ->add('public', ChoiceType::class, [
                'label' => "Public",
                'choices' => [
                    "Oui" => true,
                    "Non" => false,
                ],
                'expanded' => true,
            ])
            ->add('labels', EntityType::class, [
                'label' => "Étiquettes",
                'class' => Label::class,
                'choice_label' => fn(Label $label) => $label->getName(),
                'multiple' => true,
                'placeholder' => "-- Choisir un label --",
                'required' => false,
                'choice_attr' => fn(Label $label) => ['data-s2-label' => json_encode($label)],
                'attr' => [
                    'data-s2' => true,
                    'data-s2-options' => '{"templateResult": "RefaeLabel.templateResult", ' .
                        '"templateSelection": "RefaeLabel.templateSelection"}',
                ],
                'query_builder' => fn(LabelRepository $labelRepository): QueryBuilder
                    => $labelRepository->queryOptions($tenantUrl),
                'group_by' => fn(Label $label) => $label->getLabelGroupName() ?: '-- sans groupe --',
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ServiceLevel::class,
        ]);
    }
}
