<?php

namespace App\Form;

use App\Entity\DocumentRuleFile;
use App\Service\SessionFiles;
use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentRuleFileType extends AbstractType
{
    public string $url;

    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly RouterInterface $router,
        private readonly SessionFiles $sessionFiles,
    ) {
    }

    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', ChoiceType::class, [
                'label' => "Type du fichier",
                'choices' => array_flip(DocumentRuleFile::getTypeTranslations()),
            ])
            ->add('description', null, [
                'label' => "Description",
            ])
        ;

        if (!$options['withFileUpload'] ?? true) {
            return;
        }
        $attrs = [
            'data-uploader' => $this->router->generate('public_upload'),
        ];
        $request = $this->requestStack->getCurrentRequest();
        $session_tmpfile_uuid = $request->get('document_rule_file')['file'] ?? null;
        if ($session_tmpfile_uuid) {
            $uri = $this->sessionFiles->getUri($session_tmpfile_uuid);
            $attrs['data-value'] = json_encode([
                'name' => basename((string) $uri),
                'size' => filesize($uri),
                'uuid' => $session_tmpfile_uuid,
            ]);
        }
        $builder
            ->add('file', FileType::class, [
                'label' => "Fichier à ajouter",
                'attr' => $attrs,
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DocumentRuleFile::class,
            'withFileUpload' => true,
        ]);
    }
}
