<?php

namespace App\Form\Type;

use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType as CoreTextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EqualType extends AbstractType
{
    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'required' => false,
        ]);
    }

    #[Override]
    public function getParent(): string
    {
        return CoreTextType::class;
    }

    public static function query(string $fieldname, string $alias = null)
    {
        return function (QueryBuilder $query, int $index, $value) use ($fieldname, $alias) {
            if (!$alias) {
                $alias = $query->getAllAliases()[0];
            }
            $query
                ->andWhere(sprintf('%s.%s = :f%s%d', $alias, $fieldname, $fieldname, $index))
                ->setParameter(sprintf('f%s%d', $fieldname, $index), $value);
        };
    }
}
