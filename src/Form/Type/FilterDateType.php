<?php

namespace App\Form\Type;

use DateInterval;
use DateTime;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FilterDateType extends AbstractType
{
    public const array AVAILABLE_OPERATORS = [
        '=', '<=', '>=', '<', '>', 'today', 'week', 'month', 'year', 'P7D',
        'P1M', 'P1Y',
    ];

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'required' => false,
            'widget' => 'single_text',
            'attr' => ['class' => 'with-select'],
        ]);
    }

    #[Override]
    public function getParent(): string
    {
        return DateType::class;
    }

    public static function query(string $fieldname, string $alias = null)
    {
        return function (QueryBuilder $query, int $index, $value) use ($fieldname, $alias) {
            if (!$alias) {
                $alias = $query->getAllAliases()[0];
            }
            [$operator, $value] = explode('~', $value);
            if (!in_array($operator, self::AVAILABLE_OPERATORS)) {
                throw new BadRequestHttpException();
            }
            $field = sprintf('%s.%s', $alias, $fieldname);

            if (preg_match('/^PT?\d+[HDMY]$/', $operator)) {
                $dateMin = (new DateTime())->sub(new DateInterval($operator));
                $query->andWhere(
                    sprintf(
                        '%s between :f%smin%d and CURRENT_DATE()',
                        $field,
                        $fieldname,
                        $index
                    )
                );
                $query->setParameter(sprintf('f%smin%d', $fieldname, $index), $dateMin);
            } elseif ($operator === 'today') {
                $query->andWhere(sprintf('date(%s) = date(CURRENT_DATE())', $field));
            } elseif (in_array($operator, ['week', 'month', 'year'])) {
                if ($operator === 'week') {
                    $dateMin = (new DateTime())->modify('monday this week');
                    $dateMax = (new DateTime())->modify('sunday this week');
                } elseif ($operator === 'month') {
                    $dateMin = (new DateTime())->modify('first day of this month');
                    $dateMax = (new DateTime())->modify('last day of this month');
                } else {
                    $dateMin = (new DateTime())->modify('first day of january');
                    $dateMax = (new DateTime())->modify('last day of december');
                }
                $query->andWhere(
                    sprintf(
                        '%s between :f%smin%d and :f%smax%d',
                        $field,
                        $fieldname,
                        $index,
                        $fieldname,
                        $index
                    )
                );
                $query->setParameter(sprintf('f%smin%d', $fieldname, $index), $dateMin);
                $query->setParameter(sprintf('f%smax%d', $fieldname, $index), $dateMax);
            } else {
                $query->andWhere(
                    sprintf(
                        'date(%s) %s date(:f%s%d)',
                        $field,
                        $operator,
                        $fieldname,
                        $index
                    )
                );
                $query->setParameter(sprintf('f%s%d', $fieldname, $index), new DateTime($value));
            }
        };
    }
}
