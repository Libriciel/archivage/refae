<?php

namespace App\Form\Type;

use App\Validator\PasswordComplexity;
use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PasswordStrengthType extends AbstractType
{
    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'constraints' => new PasswordComplexity(),
        ]);
    }

    #[Override]
    public function getParent(): string
    {
        return PasswordType::class;
    }
}
