<?php

namespace App\Form\Type;

use App\Entity\Label;
use App\Repository\LabelRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\LazyServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LabelsType extends AbstractType
{
    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly LabelRepository $labelRepository,
    ) {
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults($this->getOptions());
    }

    public function getOptions(): array
    {
        $request = $this->requestStack->getCurrentRequest();
        $tenantUrl = $request->get('tenant_url');
        $options = [
            'label' => "Étiquettes",
            'class' => Label::class,
            'choice_value' => fn(Label $label) => $label->getConcatLabelGroupName('~'),
            'choice_label' => fn(Label $label) => (string)$label->getName(),
            'multiple' => true,
            'placeholder' => "-- Choisir un label --",
            'required' => false,
            'choice_attr' => fn(Label $label) => ['data-s2-label' => json_encode($label)],
            'attr' => [
                'data-s2' => true,
                'data-s2-options' => '{"templateResult": "RefaeLabel.templateResult", ' .
                    '"templateSelection": "RefaeLabel.templateSelection"}',
            ],
            'group_by' => fn(Label $label) => $label->getLabelGroupName() ?: '-- sans groupe --',
        ];
        /** @var Label[] $labels */
        $labels = $this->labelRepository->queryOptions($tenantUrl)->getQuery()->getResult();
        foreach ($labels as $label) {
            $options['choices'][] = $label;
        }
        return $options;
    }

    #[Override]
    public function getParent(): string
    {
        return ChoiceType::class;
    }

    public static function query(LazyServiceEntityRepository $repository, string $joinFieldname, string $alias = null)
    {
        return function (QueryBuilder $query, int $index, array $values) use ($repository, $joinFieldname, $alias) {
            if (!$alias) {
                $alias = $query->getAllAliases()[0];
            }
            foreach ($values as $i => $value) {
                $group = null;
                if (str_contains($value, '~')) {
                    [$group, $name] = explode('~', $value);
                }
                $subname = sprintf('sub_%d_%d', $index, $i);
                $subnameLabel = sprintf('%s_%s', $subname, $joinFieldname);
                $subquery = $repository->createQueryBuilder($subname)
                    ->select(sprintf('%s.id', $subname))
                    ->innerJoin(sprintf('%s.%s', $subname, $joinFieldname), $subnameLabel)
                    ->andWhere(sprintf('%s.name = :%s_name', $subnameLabel, $subname))
                ;
                if ($group) {
                    $subnameLabelGroup = sprintf('%s_%s_label_group', $subname, $joinFieldname);
                    $subquery
                        ->innerJoin(sprintf('%s.labelGroup', $subnameLabel), $subnameLabelGroup)
                        ->andWhere(sprintf('%s.name = :%s_name', $subnameLabelGroup, $subnameLabelGroup))
                    ;
                }
                $query
                    ->andWhere(sprintf('%s.id IN (%s)', $alias, $subquery->getDQL()))
                    ->setParameter(sprintf('%s_name', $subname), $name ?? $value)
                ;
                if ($group) {
                    $query->setParameter(sprintf('%s_name', $subnameLabelGroup), $group);
                }
            }
        };
    }
}
