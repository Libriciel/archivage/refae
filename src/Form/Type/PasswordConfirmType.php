<?php

namespace App\Form\Type;

use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PasswordConfirmType extends AbstractType
{
    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'mapped' => false,
            'required' => true,
            'type' => PasswordStrengthType::class,
            'invalid_message' => "Les mots de passe ne sont pas identiques",
            'first_options' => [
                'label' => "Mot de passe",
            ],
            'second_options' => [
                'label' => "Confirmer le mot de passe",
            ],
        ]);
    }

    #[Override]
    public function getParent(): string
    {
        return RepeatedType::class;
    }
}
