<?php

namespace App\Form\Type;

use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FavoriteType extends AbstractType
{
    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'required' => false,
            'attr' => [
                'readonly' => 'readonly',
                'onclick' => 'return false;',
                'class' => 'with-icon',
            ],
            'label_attr' => ['class' => 'as-star-o'],
            'data' => true,
        ]);
    }

    #[Override]
    public function getParent(): string
    {
        return CheckboxType::class;
    }
}
