<?php

namespace App\Form\Type;

use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WildcardType extends AbstractType
{
    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'required' => false,
            'help' => "? = n'importe quel caractère ; * = n'importe quelle chaîne de caractères",
        ]);
    }

    #[Override]
    public function getParent(): string
    {
        return TextType::class;
    }

    public static function query(string $fieldname, string $alias = null)
    {
        return function (QueryBuilder $query, int $index, $value) use ($fieldname, $alias) {
            if (!$alias) {
                $alias = $query->getAllAliases()[0];
            }
            $value = str_replace(['?', '*'], ['_', '%'], $value);
            $query
                ->andWhere(sprintf('%s.%s like :f%s%d', $alias, $fieldname, $fieldname, $index))
                ->setParameter(sprintf('f%s%d', $fieldname, $index), $value);
        };
    }
}
