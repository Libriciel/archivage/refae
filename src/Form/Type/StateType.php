<?php

namespace App\Form\Type;

use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StateType extends AbstractType
{
    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'choices' => []
        ]);
    }

    #[Override]
    public function getParent(): string
    {
        return ChoiceType::class;
    }

    public static function query()
    {
        return function (QueryBuilder $query, int $index, $value) {
            $alias = $query->getAllAliases()[0];
            $query
                ->andWhere(sprintf('%s.state = :state%d', $alias, $index))
                ->setParameter(sprintf('state%d', $index), $value);
        };
    }
}
