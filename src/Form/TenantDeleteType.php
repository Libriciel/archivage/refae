<?php

namespace App\Form;

use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TenantDeleteType extends AbstractType
{
    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $baseurl = $options['baseurl'];
        $builder
            ->add('confirm', TextType::class, [
                'label' => "Confirmer la suppression en écrivant `$baseurl`",
                'attr' => ['pattern' => preg_quote((string) $baseurl)],
            ])
            ->add('delete_users', ChoiceType::class, [
                'label' => "Supprimer également les utilisateurs liés uniquement à ce tenant",
                'choices' => [
                    "Oui" => true,
                    "Non" => false,
                ],
                'expanded' => true,
            ])
            ->add('delete_ldaps', ChoiceType::class, [
                'label' => "Supprimer également les LDAPs liés uniquement à ce tenant",
                'choices' => [
                    "Oui" => true,
                    "Non" => false,
                ],
                'expanded' => true,
            ])
            ->add('delete_openids', ChoiceType::class, [
                'label' => "Supprimer également les OpenIds liés uniquement à ce tenant",
                'choices' => [
                    "Oui" => true,
                    "Non" => false,
                ],
                'expanded' => true,
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'baseurl' => null,
        ]);
    }
}
