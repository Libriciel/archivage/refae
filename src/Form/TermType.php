<?php

namespace App\Form;

use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class TermType extends AbstractType
{
    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'label' => "Nom du terme"
            ])
            ->add('identifier', null, [
                'label' => "Identifiant du terme dans un référentiel",
            ])
            ->add('directoryName', null, [
                'label' => "Nom ou identifiant du référentiel"
            ])
        ;
    }
}
