<?php

namespace App\Form;

use App\Entity\LabelGroup;
use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LabelGroupType extends AbstractType
{
    public string $url;

    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'label' => "Nom du groupe d'étiquettes",
                'attr' => ['title' => "Caractères interdits: " . LabelGroup::BANNED_CHARSET],
            ])
            ->add('description', null, [
                'label' => "Description"
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => LabelGroup::class,
        ]);
    }
}
