<?php

namespace App\Form;

use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentRuleImportCsvColumnsType extends AbstractType
{
    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $i = 0;

        $defaults = [
            'access_rule_comment' => null,
            'access_rule_duration' => null,
            'comment' => null,
            'document_support' => null,
            'dua_comment' => null,
            'dua_duration' => null,
            'final_action' => null,
            'final_action_comment' => null,
            'label' => null,
            'level' => null,
            'technical_metadata' => null,
        ];

        foreach ($options['columns'] as $label => $translit) {
            if (!$label) {
                continue;
            }
            $this->setDefaults($label, $translit, $defaults);
        }
        foreach ($options['columns'] as $label => $translit) {
            if (!$label) {
                $i++;
                continue;
            }
            $params = [
                'label' => "Colonne: $label",
                'required' => false,
                'placeholder' => '-- Choisir un type de champ pour cette colonne --',
                'choices' => [
                    "Commentaire" => 'comment',
                    "Durée de la DUA" => 'dua_duration',
                    "Délai de communicabilité" => 'access_rule_duration',
                    "Niveau / profondeur de la catégorie" => 'level',
                    "Nom de la catégorie / de la règle documentaire" => 'label',
                    "Note sur la DUA" => 'dua_comment',
                    "Note sur la communicabilité" => 'access_rule_comment',
                    "Note sur le sort final" => 'final_action_comment',
                    "Sort final" => 'final_action',
                    "Support de conservation" => 'document_support',
                    "Métadonnées techniques" => 'technical_metadata',
                ],
            ];
            foreach ($defaults as $type => $selectedLabel) {
                if ($selectedLabel === $label) {
                    $params['data'] = $type;
                    break;
                }
            }
            if (empty($defaults['label']) && empty($params['data'])) {
                $defaults['label'] = $label;
                $params['data'] = 'label';
            }
            $builder->add('col-' . $i++, ChoiceType::class, $params);
        }
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'columns' => [],
        ]);
    }

    private function setDefaults(int|string $label, mixed $translit, array &$defaults)
    {
        $extractor = [
            'access_rule_duration' => fn ($v) => str_contains((string) $v, 'communicabilite'),
            'comment' => fn ($v) => preg_match('/commentaire|description|observation/', (string) $v),
            'document_support' => fn ($v) => str_contains((string) $v, 'support'),
            'dua_comment' => fn ($v) => str_contains((string) $v, 'consigne'),
            'dua_duration' => fn ($v) => str_contains((string) $v, 'dua'),
            'final_action' => fn ($v) => str_contains((string) $v, 'sort final'),
            'label' => fn ($v) => preg_match('/dossier|document/', (string) $v),
            'level' => fn ($v) => preg_match('/niveau|^lv|^n°/', (string) $v),
            'technical_metadata' => fn ($v) => preg_match('/meta|donnee/', (string) $v),
        ];
        foreach (array_keys($defaults) as $key) {
            if (!empty($defaults[$key]) || !isset($extractor[$key])) {
                continue;
            }
            if ($extractor[$key]($translit)) {
                $defaults[$key] = $label;
            }
        }
    }
}
