<?php

namespace App\Form;

use App\Entity\Label;
use App\Entity\Vocabulary;
use App\Repository\LabelRepository;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VocabularyType extends AbstractType
{
    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('identifier', null, [
                'label' => "Identifiant",
                'disabled' => $builder->getData()?->getVersion() !== 1,
            ])
            ->add('name', null, [
                'label' => "Nom"
            ])
            ->add('description', null, [
                'label' => "Description"
            ])
            ->add('public', ChoiceType::class, [
                'label' => "Public",
                'choices' => [
                    "Oui" => true,
                    "Non" => false,
                ],
                'expanded' => true,
            ])
            ->add('labels', EntityType::class, [
                'label' => "Étiquettes",
                'class' => Label::class,
                'choice_label' => function (Label $label) {
                    $groupname = $label->getLabelGroupName();
                    return ($groupname ? $groupname . ':' : '') . $label->getName();
                },
                'multiple' => true,
                'placeholder' => "-- Choisir un label --",
                'required' => false,
                'attr' => ['data-s2' => true],
                'query_builder' => fn(LabelRepository $labelRepository): QueryBuilder
                    => $labelRepository->queryOptions($options['tenant_url']),
                'group_by' => fn(Label $label) => $label->getLabelGroupName() ?: '-- sans groupe --',
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Vocabulary::class,
            'tenant_url' => null,
        ]);
        $resolver->setAllowedTypes('tenant_url', 'string');
    }
}
