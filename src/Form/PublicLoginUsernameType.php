<?php

namespace App\Form;

use App\Repository\UserRepository;
use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class PublicLoginUsernameType extends AbstractType
{
    public function __construct(
        private readonly UserRepository $userRepository,
    ) {
    }

    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', null, [
                'label' => "Identifiant",
                'constraints' => new Callback($this->validateUsername(...))
            ])
        ;
    }

    public function validateUsername(string $username, ExecutionContextInterface $context): void
    {
        $user = $this->userRepository->findOneByUsername($username);
        if (!$user) {
            $context->buildViolation("L'utilisateur n'existe pas")
                ->atPath('username')
                ->addViolation();
        }
    }
}
