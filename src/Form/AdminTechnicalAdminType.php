<?php

namespace App\Form;

use App\Entity\AccessUser;
use App\Entity\User;
use App\Form\Type\BooleanType;
use App\Repository\RoleRepository;
use App\Service\Permission;
use App\Validator\LdapIdentifierExists;
use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminTechnicalAdminType extends AbstractType
{
    public function __construct(
        private readonly RoleRepository $roleRepository,
    ) {
    }

    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var User $user */
        $user = $builder->getData();
        $accessUser = $user->getAdminTechAccessUser();
        if (!$accessUser) {
            $accessUser = new AccessUser();
            $role = $this->roleRepository->findOneDefault(Permission::ROLE_TECHNICAL_ADMIN);
            $accessUser->setRole($role);
            $user->addAccessUser($accessUser);
        } else {
            $user->setLoginType($accessUser->getLoginType());
        }

        $builder
            ->add('username', TextType::class, [
                'label' => "Identifiant de connexion",
            ])
            ->add('name', TextType::class, [
                'label' => "Nom",
                'required' => false,
            ])
            ->add('email', EmailType::class, [
                'label' => "E-mail",
                'help' => "Le lien d'initialisation du mot de passe sera envoyé"
                    . " par e-mail dans le cas d'une connexion type refae",
            ])
            ->add('loginType', ChoiceType::class, [
                'label' => "Méthode de connexion",
                'choices' => [
                    'Refae' => AccessUser::LOGIN_TYPE_REFAE,
                    'LDAP' => AccessUser::LOGIN_TYPE_LDAP,
                    'Openid' => AccessUser::LOGIN_TYPE_OPENID,
                ],
                'attr' => ['data-s2' => true, 'required' => true],
                'placeholder' => "-- Choisir une méthode de connexion --",
            ])
            ->add('accessUser', AdminTechAccessUserType::class, [
                'label' => false,
                'data' => $accessUser,
                'mapped' => false,
                'constraints' => [new LdapIdentifierExists()],
            ])
            ->add('notifyUserRegistration', BooleanType::class, [
                'label' => "Recevoir les demandes de création d'utilisateur",
                'expanded' => true,
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
