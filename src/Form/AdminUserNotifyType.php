<?php

namespace App\Form;

use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class AdminUserNotifyType extends AbstractType
{
    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('text', TextareaType::class, [
            'label' => "Message",
            'attr' => [
                'data-tinymce' => 'true',
            ],
        ]);
        $builder->add('cssClass', ChoiceType::class, [
            'label' => "Couleur de notification",
            'choices' => [
                "bleu" => 'alert-info',
                "vert" => 'alert-success',
                "rouge" => 'alert-danger',
                "jaune" => 'alert-warning',
            ],
        ]);
    }
}
