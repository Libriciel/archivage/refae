<?php

namespace App\Form;

use App\Entity\Label;
use App\Entity\ManagementRule;
use App\Entity\RuleType;
use App\Repository\LabelRepository;
use App\Repository\RuleTypeRepository;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ManagementRuleType extends AbstractType
{
    public function __construct(
        private readonly RequestStack $requestStack,
    ) {
    }

    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $request = $this->requestStack->getCurrentRequest();
        $tenantUrl = $request->get('tenant_url');
        $builder
            ->add('autopublish', HiddenType::class, [
                'data' => $request->get('autopublish'),
                'mapped' => false,
            ])
            ->add('identifier', null, [
                'label' => "Identifiant",
                'disabled' => $builder->getData()?->getVersion() !== 1,
            ])
            ->add('name', null, [
                'label' => "Nom",
            ])
            ->add('description', null, [
                'label' => "Description",
            ])
            ->add('duration', null, [
                'label' => "Durée de la règle",
                'help' => "Sous la forme PxYxMxD (xY = x années, xM = x mois, xD = x jours)",
            ])
            ->add('public', ChoiceType::class, [
                'label' => "Publique",
                'choices' => [
                    "Oui" => true,
                    "Non" => false,
                ],
                'expanded' => true,
            ])
            ->add('ruleType', EntityType::class, [
                'label' => "Type",
                'class' => RuleType::class,
                'choice_label' => 'name',
                'placeholder' => "-- Choisir un type --",
                'required' => true,
                'query_builder' => fn(RuleTypeRepository $ruleTypeRepository): QueryBuilder
                    => $ruleTypeRepository->queryOptions($tenantUrl),
            ])
            ->add('labels', EntityType::class, [
                'label' => "Étiquettes",
                'class' => Label::class,
                'choice_label' => fn(Label $label): string => $label->getName(),
                'multiple' => true,
                'placeholder' => "-- Choisir un label --",
                'required' => false,
                'choice_attr' => fn (Label $label) => ['data-s2-label' => json_encode($label)],
                'attr' => [
                    'data-s2' => true,
                    'data-s2-options' => '{"templateResult": "RefaeLabel.templateResult", ' .
                        '"templateSelection": "RefaeLabel.templateSelection"}',
                ],
                'query_builder' => fn(LabelRepository $labelRepository): QueryBuilder
                    => $labelRepository->queryOptions($tenantUrl),
                'group_by' => fn(Label $label) => $label->getLabelGroupName() ?: '-- sans groupe --',
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ManagementRule::class,
        ]);
    }
}
