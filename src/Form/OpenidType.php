<?php

namespace App\Form;

use App\Entity\Openid;
use App\Entity\Tenant;
use App\Repository\TenantRepository;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class OpenidType extends AbstractType
{
    public string $url;

    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'label' => "Nom du connecteur OpenID",
            ])
            ->add('identifier', null, [
                'label' => "Identifiant du connecteur (définit l'URL de connexion)",
            ])
            ->add('tenants', EntityType::class, [
                'class' => Tenant::class,
                'multiple' => true,
                'required' => false,
                'choice_label' => 'name',
                'attr' => ['data-s2' => true],
                'query_builder' => fn(TenantRepository $tenantRepository): QueryBuilder
                    => $tenantRepository->createQueryBuilder('r')->orderBy('r.name'),
            ])
            ->add('url', UrlType::class, [
                'label' => "Url de base",
                'attr' => [
                    'placeholder' => 'http://localhost:8080/realms/master',
                ],
            ])
            ->add('client_id', null, [
                'label' => "ID Client",
                'attr' => [
                    'placeholder' => 'refae',
                ],
            ])
            ->add('client_secret', PasswordType::class, [
                'label' => "Secret client",
                'required' => false,
                'help' => "Laisser vide pour ne pas modifier"
            ])
            ->add('username_field', null, [
                'label' => "Champ utilisé pour identifier l'utilisateur",
                'attr' => [
                    'placeholder' => 'preferred_username',
                ],
            ])
            ->add('scopes', CollectionType::class, [
                'entry_type' => OpenidScopeType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Count(min: 1, minMessage: "Il doit y avoir au moins 1 scope"),
                    new Assert\Unique(),
                ],
                'prototype' => true,
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Openid::class,
        ]);
    }
}
