<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Label;
use App\Entity\DocumentRule;
use App\Entity\ManagementRule;
use App\Entity\Profile;
use App\Repository\CategoryRepository;
use App\Repository\LabelRepository;
use App\Repository\ManagementRuleRepository;
use App\Repository\ProfileRepository;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentRuleType extends AbstractType
{
    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly CategoryRepository $categoryRepository,
    ) {
    }

    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $request = $this->requestStack->getCurrentRequest();
        $tenantUrl = $request->get('tenant_url');

        // findOptions défini $category->getPath() -> utiliser avant choice_label
        $categories = $this->categoryRepository->findOptions($tenantUrl);
        $builder
            ->add('identifier', null, [
                'label' => "Identifiant",
                'disabled' => $builder->getData()?->getVersion() !== 1,
            ])
            ->add('name', null, [
                'label' => "Nom",
            ])
            ->add('observation', null, [
                'label' => "Observations",
            ])
            ->add('regulatoryText', null, [
                'label' => "Texte réglementaire",
            ])
            ->add('documentSupport', ChoiceType::class, [
                'label' => "Support des documents",
                'choices' => array_flip(DocumentRule::getDocumentSupportTranslations()),
                'placeholder' => "-- Choisir le support des documents --",
            ])
            ->add('literalFileFormat', null, [
                'label' => "Formats littéraux des fichiers",
            ])
            ->add('documentLocation', null, [
                'label' => "Localisation des documents",
            ])
            ->add('profile', EntityType::class, [
                'label' => "Profil d'archives",
                'class' => Profile::class,
                'choice_label' => fn(Profile $profile) => $profile->getName(),
                'placeholder' => "-- Choisir un profil --",
                'required' => false,
                'query_builder' => fn(ProfileRepository $profilRepository): QueryBuilder
                    => $profilRepository->queryOptions($tenantUrl),
            ])
            ->add('storageRule', EntityType::class, [
                'label' => "Règle de la DUC",
                'class' => ManagementRule::class,
                'choice_label' => fn(ManagementRule $managementRule): string => $managementRule->getName(),
                'multiple' => false,
                'placeholder' => "-- Choisir une règle de DUC --",
                'required' => false,
                'choice_attr' => fn(ManagementRule $managementRule)
                    => ['data-s2-title' => $managementRule->getDescription()],
                'attr' => ['data-s2' => true],
                'query_builder' => fn(ManagementRuleRepository $managementRuleRepository): QueryBuilder
                    => $managementRuleRepository->queryStorageRuleOptions($tenantUrl),
            ])
            ->add('storageRuleLiteralStartDate', null, [
                'label' => "Date littérale de départ de calcul de la DUC",
            ])
            ->add('storageRuleNote', null, [
                'label' => "Note sur la DUC",
            ])
            ->add('appraisalRule', EntityType::class, [
                'label' => "Règle de la DUA",
                'class' => ManagementRule::class,
                'choice_label' => fn(ManagementRule $managementRule): string => $managementRule->getName(),
                'multiple' => false,
                'placeholder' => "-- Choisir une règle de gestion --",
                'required' => false,
                'choice_attr' => fn(ManagementRule $managementRule)
                    => ['data-s2-title' => $managementRule->getDescription()],
                'attr' => ['data-s2' => true],
                'query_builder' => fn(ManagementRuleRepository $managementRuleRepository): QueryBuilder
                    => $managementRuleRepository->queryAppraisalOptions($tenantUrl),
            ])
            ->add('appraisalRuleLiteralStartDate', null, [
                'label' => "Date littérale de départ de calcul de la DUA",
            ])
            ->add('appraisalRuleFinalAction', ChoiceType::class, [
                'label' => "Sort final à l'issue de la DUA",
                'choices' => array_flip(DocumentRule::getAppraisalRuleFinalActionTranslations()),
                'placeholder' => "-- Choisir un sort final --",
            ])
            ->add('appraisalRuleNote', null, [
                'label' => "Note sur la DUA",
            ])
            ->add('accessRule', EntityType::class, [
                'label' => "Règle de la communicabilité",
                'class' => ManagementRule::class,
                'choice_label' => fn(ManagementRule $managementRule): string => $managementRule->getName(),
                'multiple' => false,
                'placeholder' => "-- Choisir une règle de communicabilité",
                'required' => false,
                'choice_attr' => fn(ManagementRule $managementRule)
                    => ['data-s2-title' => $managementRule->getDescription()],
                'attr' => ['data-s2' => true],
                'query_builder' => fn(ManagementRuleRepository $managementRuleRepository): QueryBuilder
                    => $managementRuleRepository->queryAccessOptions($tenantUrl),
            ])
            ->add('accessRuleLiteralStarDate', null, [
                'label' => "Date littérale de départ de calcul de la communicabilité",
            ])
            ->add('accessRuleNote', null, [
                'label' => "Note sur la communicabilité",
            ])
            ->add('technicalMetadata', null, [
                'label' => "Métadonnées techniques",
            ])
            ->add('public', ChoiceType::class, [
                'label' => "Publique",
                'choices' => [
                    "Oui" => true,
                    "Non" => false,
                ],
                'expanded' => true,
            ])
            ->add('labels', EntityType::class, [
                'label' => "Étiquettes",
                'class' => Label::class,
                'choice_label' => fn(Label $label) => $label->getName(),
                'multiple' => true,
                'placeholder' => "-- Choisir un label --",
                'required' => false,
                'choice_attr' => fn(Label $label) => ['data-s2-label' => json_encode($label)],
                'attr' => [
                    'data-s2' => true,
                    'data-s2-options' => '{"templateResult": "RefaeLabel.templateResult", ' .
                        '"templateSelection": "RefaeLabel.templateSelection"}',
                ],
                'query_builder' => fn(LabelRepository $labelRepository): QueryBuilder
                    => $labelRepository->queryOptions($tenantUrl),
                'group_by' => fn(Label $label) => $label->getLabelGroupName() ?: '-- sans groupe --',
            ])
            ->add('categories', EntityType::class, [
                'label' => "Catégories",
                'class' => Category::class,
                'choice_label' => fn (Category $category) => $category->getPath(),
                'multiple' => true,
                'placeholder' => "-- Choisir une catégorie --",
                'required' => false,
                'attr' => ['data-s2' => true],
                'choices' => $categories,
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DocumentRule::class,
        ]);
    }
}
