<?php

namespace App\Form;

use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\RouterInterface;

class MeilisearchType extends AbstractType
{
    public function __construct(
        private readonly RouterInterface $router,
    ) {
    }

    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('index', HiddenType::class)
            ->add('tenant', HiddenType::class)
            ->add('public', HiddenType::class)
            ->add('published', HiddenType::class)
            ->add('search', null, [
                'label' => "Moteur de recherche",
                'required' => false,
                'label_attr' => [
                    'class' => 'sr-only',
                ],
                'row_attr' => [
                    'class' => 'search-engine',
                ],
                'attr' => [
                    'placeholder' => "Rechercher...",
                    'data-search-engine' => $this->router->generate('public_search'),
                ],
            ])
        ;
    }
}
