<?php

namespace App\Form;

use App\Entity\Agreement;
use App\Entity\AgreementArchivalAgencyIdentifier;
use App\Entity\AgreementOriginatingAgencyIdentifier;
use App\Entity\AgreementProfileIdentifier;
use App\Entity\AgreementTransferringAgencyIdentifier;
use App\Entity\Authority;
use App\Entity\Label;
use App\Entity\Profile;
use App\Repository\AgreementArchivalAgencyIdentifierRepository;
use App\Repository\AgreementOriginatingAgencyIdentifierRepository;
use App\Repository\AgreementProfileIdentifierRepository;
use App\Repository\AgreementTransferringAgencyIdentifierRepository;
use App\Repository\AuthorityRepository;
use App\Repository\LabelRepository;
use App\Repository\ProfileRepository;
use Doctrine\ORM\QueryBuilder;
use Override;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AgreementType extends AbstractType
{
    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly ProfileRepository $profileRepo,
        private readonly AuthorityRepository $authorityRepo,
        private readonly AgreementArchivalAgencyIdentifierRepository $agreementArchivalAgencyIdentifierRepo,
        private readonly AgreementOriginatingAgencyIdentifierRepository $agreementOriginatingAgencyIdentifierRepo,
        private readonly AgreementTransferringAgencyIdentifierRepository $agreementTransferringAgencyIdentifierRepo,
        private readonly AgreementProfileIdentifierRepository $agreementProfileIdentifierRepo,
    ) {
    }

    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $request = $this->requestStack->getCurrentRequest();
        $tenantUrl = $request->get('tenant_url');
        $builder
            ->add('identifier', null, [
                'label' => "Identifiant",
                'disabled' => $builder->getData()?->getVersion() !== 1,
            ])
            ->add('name', null, [
                'label' => "Nom",
            ])
            ->add('description', null, [
                'label' => "Description",
            ])
            ->add('validityBegin', DateType::class, [
                'label' => "Date de début de validité",
                'required' => false,
                'widget' => 'single_text',
            ])
            ->add('validityEnd', DateType::class, [
                'label' => "Date de fin de validité",
                'required' => false,
                'widget' => 'single_text',
            ])
            ->add('public', ChoiceType::class, [
                'label' => "Public",
                'choices' => [
                    "Oui" => true,
                    "Non" => false,
                ],
                'expanded' => true,
            ])
            ->add('profileIdentifiers', EntityType::class, [
                'label' => "Profils d'archives",
                'class' => AgreementProfileIdentifier::class,
                'choice_value' => 'identifier',
                'choice_label' => fn(AgreementProfileIdentifier $p): string =>
                    $p->getNameForSelect() . ($p->getOriginalExists() ? '' : " (n'existe pas dans l'application)")
                    . ($p->getOriginalIsPublished() ? '' : " (n'est pas publié)"),
                'choice_attr' => fn(AgreementProfileIdentifier $p): array => $p->getOriginalIsPublished()
                    ? ($p->getOriginalExists() ? [] : ['data-s2-class' => 'category-unexistant'])
                    : ['data-s2-class' => 'category-unpublished'],
                'multiple' => true,
                'placeholder' => "-- Choisir un profil --",
                'required' => false,
                'attr' => [
                    'data-s2' => true,
                ],
                'mapped' => true,
                'choices' => $this->getProfileOptions($options['tenant_url'], $options['agreement_id']),
            ])
            ->add('archivalAgencyIdentifiers', EntityType::class, [
                'label' => "Services d'Archives",
                'class' => AgreementArchivalAgencyIdentifier::class,
                'choice_value' => 'identifier',
                'choice_label' => fn(AgreementArchivalAgencyIdentifier $p): string =>
                    $p->getNameForSelect() . ($p->getOriginalExists() ? '' : " (n'existe pas dans l'application)")
                    . ($p->getOriginalIsPublished() ? '' : " (n'est pas publié)"),
                'choice_attr' => fn(AgreementArchivalAgencyIdentifier $p): array => $p->getOriginalIsPublished()
                    ? ($p->getOriginalExists() ? [] : ['data-s2-class' => 'category-unexistant'])
                    : ['data-s2-class' => 'category-unpublished'],
                'multiple' => true,
                'placeholder' => "-- Choisir un service d'archive --",
                'required' => false,
                'attr' => [
                    'data-s2' => true,
                ],
                'mapped' => true,
                'choices' => $this->getArchivalOptions($options['tenant_url'], $options['agreement_id']),
            ])
            ->add('originatingAgencyIdentifiers', EntityType::class, [
                'label' => "Services producteurs",
                'class' => AgreementOriginatingAgencyIdentifier::class,
                'choice_value' => 'identifier',
                'choice_label' => fn(AgreementOriginatingAgencyIdentifier $p): string =>
                    $p->getNameForSelect() . ($p->getOriginalExists() ? '' : " (n'existe pas dans l'application)")
                    . ($p->getOriginalIsPublished() ? '' : " (n'est pas publié)"),
                'choice_attr' => fn(AgreementOriginatingAgencyIdentifier $p): array => $p->getOriginalIsPublished()
                    ? ($p->getOriginalExists() ? [] : ['data-s2-class' => 'category-unexistant'])
                    : ['data-s2-class' => 'category-unpublished'],
                'multiple' => true,
                'placeholder' => "-- Choisir un service producteur --",
                'required' => false,
                'attr' => [
                    'data-s2' => true,
                ],
                'mapped' => true,
                'choices' => $this->getOriginatingOptions($options['tenant_url'], $options['agreement_id']),
            ])
            ->add('transferringAgencyIdentifiers', EntityType::class, [
                'label' => "Services versants",
                'class' => AgreementTransferringAgencyIdentifier::class,
                'choice_value' => 'identifier',
                'choice_label' => fn(AgreementTransferringAgencyIdentifier $p): string =>
                    $p->getNameForSelect() . ($p->getOriginalExists() ? '' : " (n'existe pas dans l'application)")
                    . ($p->getOriginalIsPublished() ? '' : " (n'est pas publié)"),
                'choice_attr' => fn(AgreementTransferringAgencyIdentifier $p): array => $p->getOriginalIsPublished()
                    ? ($p->getOriginalExists() ? [] : ['data-s2-class' => 'category-unexistant'])
                    : ['data-s2-class' => 'category-unpublished'],
                'multiple' => true,
                'placeholder' => "-- Choisir un service versant --",
                'required' => false,
                'attr' => [
                    'data-s2' => true,
                ],
                'mapped' => true,
                'choices' => $this->getTransferringOptions($options['tenant_url'], $options['agreement_id']),
            ])
            ->add('labels', EntityType::class, [
                'label' => "Étiquettes",
                'class' => Label::class,
                'choice_label' => fn(Label $label): string => $label->getName(),
                'multiple' => true,
                'placeholder' => "-- Choisir un label --",
                'required' => false,
                'choice_attr' => fn(Label $label): array => ['data-s2-label' => json_encode($label)],
                'attr' => [
                    'data-s2' => true,
                    'data-s2-options' => '{"templateResult": "RefaeLabel.templateResult", ' .
                        '"templateSelection": "RefaeLabel.templateSelection"}',
                ],
                'query_builder' => fn(LabelRepository $labelRepository): QueryBuilder
                    => $labelRepository->queryOptions($tenantUrl),
                'group_by' => fn(Label $label): string => $label->getLabelGroupName() ?: '-- sans groupe --',
            ])
        ;
    }

    protected function getProfileOptions(string $tenantUrl, ?string $agreementId = null): array
    {
        $profileIdentifiers = $agreementId
            ? $this->agreementProfileIdentifierRepo->findForAgreement($agreementId)
            : [];

        $profiles = $this->profileRepo->queryPublished($tenantUrl, withDeactivated: false);

        // on filtre pour ne pas récupérer ceux qui existent déjà
        if ($profileIdentifiers) {
            $ids = array_map(fn($p): string => $p->getIdentifier(), $profileIdentifiers);
            $profiles
                ->andWhere($this->profileRepo->getAlias() . '.identifier NOT IN (:identifiers)')
                ->setParameter('identifiers', $ids);
        }

        $convertedProfiles = array_map(
            fn(Profile $p): AgreementProfileIdentifier => new AgreementProfileIdentifier($p),
            $profiles->getQuery()->getResult()
        );

        return [...$convertedProfiles, ...$profileIdentifiers];
    }

    protected function getArchivalOptions(string $tenantUrl, ?string $agreementId = null): array
    {
        $archivalIdentifiers = $agreementId
            ? $this->agreementArchivalAgencyIdentifierRepo->findForAgreement($agreementId)
            : [];

        $authorities = $this->authorityRepo->queryPublished($tenantUrl, withDeactivated: false);

        // on filtre pour ne pas récupérer ceux qui existent déjà
        if ($archivalIdentifiers) {
            $ids = array_map(fn($p): string => $p->getIdentifier(), $archivalIdentifiers);
            $authorities
                ->andWhere($this->authorityRepo->getAlias() . '.identifier NOT IN (:identifiers)')
                ->setParameter('identifiers', $ids);
        }

        $convertedArchivals = array_map(
            fn(Authority $a): AgreementArchivalAgencyIdentifier => new AgreementArchivalAgencyIdentifier($a),
            $authorities->getQuery()->getResult()
        );

        return [...$convertedArchivals, ...$archivalIdentifiers];
    }

    protected function getOriginatingOptions(string $tenantUrl, ?string $agreementId = null): array
    {
        $originatingIdentifiers = $agreementId
            ? $this->agreementOriginatingAgencyIdentifierRepo->findForAgreement($agreementId)
            : [];

        $authorities = $this->authorityRepo->queryPublished($tenantUrl, withDeactivated: false);

        // on filtre pour ne pas récupérer ceux qui existent déjà
        if ($originatingIdentifiers) {
            $ids = array_map(fn($p): string => $p->getIdentifier(), $originatingIdentifiers);
            $authorities
                ->andWhere($this->authorityRepo->getAlias() . '.identifier NOT IN (:identifiers)')
                ->setParameter('identifiers', $ids);
        }

        $convertedOriginatings = array_map(
            fn(Authority $a): AgreementOriginatingAgencyIdentifier => new AgreementOriginatingAgencyIdentifier($a),
            $authorities->getQuery()->getResult()
        );

        return [...$convertedOriginatings, ...$originatingIdentifiers];
    }

    protected function getTransferringOptions(string $tenantUrl, ?string $agreementId = null): array
    {
        $transferringIdentifiers = $agreementId
            ? $this->agreementTransferringAgencyIdentifierRepo->findForAgreement($agreementId)
            : [];

        $authorities = $this->authorityRepo->queryPublished($tenantUrl, withDeactivated: false);

        // on filtre pour ne pas récupérer ceux qui existent déjà
        if ($transferringIdentifiers) {
            $ids = array_map(fn($p): string => $p->getIdentifier(), $transferringIdentifiers);
            $authorities
                ->andWhere($this->authorityRepo->getAlias() . '.identifier NOT IN (:identifiers)')
                ->setParameter('identifiers', $ids);
        }

        $convertedTransferrings = array_map(
            fn(Authority $a): AgreementTransferringAgencyIdentifier => new AgreementTransferringAgencyIdentifier($a),
            $authorities->getQuery()->getResult()
        );

        return [...$convertedTransferrings, ...$transferringIdentifiers];
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Agreement::class,
            'tenant_url' => null,
            'agreement_id' => null,
        ]);
        $resolver->setAllowedTypes('tenant_url', 'string');
        $resolver->setAllowedTypes('agreement_id', ['null', LazyUuidFromString::class]);
    }
}
