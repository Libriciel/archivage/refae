<?php

namespace App\Form;

use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class UserCreationRequestRefusalType extends AbstractType
{
    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('reason', TextareaType::class, [
            'label' => "Motif du refus de création de l'utilisateur",
            'help' => "Sera affiché dans l'e-mail de l'utilisateur",
            'attr' => ['data-tinymce' => 'true']
        ]);
    }
}
