<?php

namespace App\Form;

use App\Entity\AccessUser;
use App\Entity\Ldap;
use App\Entity\Openid;
use App\Entity\User;
use App\Repository\LdapRepository;
use App\Repository\OpenidRepository;
use App\Repository\UserRepository;
use App\Validator\LdapResponds;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\Expr;
use Override;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminTechPromoteAccessUserType extends AbstractType
{
    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('user', EntityType::class, [
                'label' => "Utilisateur",
                'class' => User::class,
                'choice_label' => 'username',
                'attr' => ['data-s2' => true],
                'row_attr' => ['class' => 'type-user'],
                'placeholder' => "-- Sélectionner un utilisateur --",
                'query_builder' => fn(UserRepository $userRepository): QueryBuilder
                    => $userRepository->createQueryBuilder('u')
                        ->leftJoin('u.accessUsers', 'a', Expr\Join::WITH, 'a.tenant IS NULL')
                        ->andWhere('a.id IS NULL')
                        ->orderBy('u.username'),
            ])
            ->add('loginType', ChoiceType::class, [
                'label' => "Méthode de connexion",
                'choices' => [
                    'Refae' => AccessUser::LOGIN_TYPE_REFAE,
                    'LDAP' => AccessUser::LOGIN_TYPE_LDAP,
                    'Openid' => AccessUser::LOGIN_TYPE_OPENID,
                ],
                'attr' => ['data-s2' => true, 'required' => true],
                'placeholder' => "-- Choisir une méthode de connexion --",
                'mapped' => false,
            ])
            ->add('openid', EntityType::class, [
                'label' => "Connexion via Openid",
                'class' => Openid::class,
                'choice_label' => 'name',
                'attr' => ['data-s2' => true],
                'row_attr' => ['class' => 'type-openid'],
                'query_builder' => fn(OpenidRepository $openidRepository): QueryBuilder
                    => $openidRepository->createQueryBuilder('o')
                        ->orderBy('o.name'),
            ])
            ->add('ldap', EntityType::class, [
                'label' => "Connexion via LDAP",
                'class' => Ldap::class,
                'choice_label' => 'name',
                'choice_attr' => fn (Ldap $ldap) => [
                    'data-ldap-map-username' => $ldap->getUserUsernameAttribute(),
                    'data-ldap-map-login' => $ldap->getUserLoginAttribute(),
                ],
                'attr' => ['data-s2' => true, 'required' => true],
                'row_attr' => ['class' => 'type-ldap'],
                'query_builder' => fn(LdapRepository $ldapRepository): QueryBuilder
                    => $ldapRepository->createQueryBuilder('l')
                        ->orderBy('l.name'),
                'constraints' => [new LdapResponds()],
            ])
            ->add('ldapUsername', null, [
                'label' => "Identifiant de connexion sur le LDAP",
                'attr' => ['required' => true],
                'row_attr' => ['class' => 'type-ldap required'],
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AccessUser::class,
        ]);
    }
}
