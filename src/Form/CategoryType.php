<?php

namespace App\Form;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Override;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly CategoryRepository $categoryRepository,
    ) {
    }

    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $request = $this->requestStack->getCurrentRequest();
        $tenantUrl = $request->get('tenant_url');
        /** @var Category $category */
        $category = $builder->getData();

        // findOptions défini $category->getPath() -> utiliser avant choice_label
        $categories = $this->categoryRepository->findOptions($tenantUrl, $category->getId() ? $category : null);
        $builder
            ->add('name', null, [
                'label' => "Nom",
                'help' => "(ne peut contenir le caractère > ou ⮕)",
            ])
            ->add('description', null, [
                'label' => "Description",
                'required' => false,
            ])
            ->add('parent', EntityType::class, [
                'label' => "Catégorie parente",
                'class' => Category::class,
                'choice_value' => fn (?Category $category) => $category?->getId(),
                'choice_label' => fn (?Category $category) => $category?->getPath(),
                'placeholder' => "-- Choisir un parent --",
                'required' => false,
                'attr' => [
                    'data-s2' => true,
                ],
                'choices' => $categories,
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Category::class,
        ]);
    }
}
