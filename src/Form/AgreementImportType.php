<?php

namespace App\Form;

use App\Service\Manager\AgreementManager;
use App\Service\SessionFiles;
use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

class AgreementImportType extends AbstractType
{
    public string $url;

    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly RouterInterface $router,
        private readonly SessionFiles $sessionFiles,
    ) {
    }

    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $attrs = [
            'data-uploader' => $this->router->generate('public_upload'),
            'accept' => sprintf('.%s.refae', AgreementManager::getFileName()),
        ];
        $request = $this->requestStack->getCurrentRequest();
        $session_tmpfile_uuid = $request->get('agreement_import')['file'] ?? null;
        if ($session_tmpfile_uuid) {
            $uri = $this->sessionFiles->getUri($session_tmpfile_uuid);
            $attrs['data-value'] = json_encode([
                'name' => basename((string) $uri),
                'size' => filesize($uri),
                'uuid' => $session_tmpfile_uuid,
            ]);
        }
        $builder
            ->add('file', FileType::class, [
                'label' => "Fichier d'export d'accord de versement",
                'attr' => $attrs,
            ])
        ;
    }
}
