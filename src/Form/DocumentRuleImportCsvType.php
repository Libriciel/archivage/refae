<?php

namespace App\Form;

use App\Service\SessionFiles;
use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

class DocumentRuleImportCsvType extends AbstractType
{
    public string $url;

    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly RouterInterface $router,
        private readonly SessionFiles $sessionFiles,
    ) {
    }

    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $attrs = [
            'data-uploader' => $this->router->generate('public_upload'),
            'accept' => '.csv',
        ];
        $request = $this->requestStack->getCurrentRequest();
        $session_tmpfile_uuid = $request->get('document_rule_import_csv')['file'] ?? null;
        if ($session_tmpfile_uuid) {
            $uri = $this->sessionFiles->getUri($session_tmpfile_uuid);
            $attrs['data-value'] = json_encode([
                'name' => basename((string) $uri),
                'size' => filesize($uri),
                'uuid' => $session_tmpfile_uuid,
            ]);
        }
        $builder
            ->add('file', FileType::class, [
                'label' => "Liste de règles documentaires au format CSV",
                'attr' => $attrs,
            ])
        ;
    }
}
