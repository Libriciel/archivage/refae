<?php

namespace App\Form;

use App\Entity\AccessUser;
use App\Entity\Tenant;
use App\Entity\User;
use App\Repository\TenantRepository;
use Override;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminUserType extends AbstractType
{
    public function __construct(private readonly TenantRepository $tenantRepository)
    {
    }

    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var User $user */
        $user = $builder->getData();
        $access = array_map(
            fn(AccessUser $access): ?Tenant => $access->getTenant(),
            $user->getAccessUsers()->toArray()
        );

        $tenants = $this->tenantRepository->getActiveTenants();

        // ajout des tenants désactivé déjà assignés à l'user
        foreach ($user->getTenants()->toArray() as $t) {
            if (!in_array($t, $tenants)) {
                $tenants[] = $t;
            }
        }

        // verrouillage des tenant pour lequel l'user a un acces
        $choiceAttr = array_map(
            fn(Tenant $t): array => ['locked' => in_array($t, $access)],
            $tenants
        );

        $builder
            ->add('username', TextType::class, [
                'label' => "Identifiant de connexion",
            ])
            ->add('name', TextType::class, [
                'label' => "Nom",
                'required' => false,
            ])
            ->add('email', EmailType::class, [
                'label' => "E-mail",
                'help' => "Le lien d'initialisation du mot de passe sera envoyé"
                    . " par e-mail dans le cas d'une connexion type refae",
            ])
            ->add('tenants', EntityType::class, [
                'label' => "Lié aux tenants",
                'class' => Tenant::class,
                'multiple' => true,
                'required' => false,
                'choice_label' => 'name',
                'attr' => ['data-s2' => true],
                'choices' => $tenants,
                'choice_attr' => $choiceAttr,
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
