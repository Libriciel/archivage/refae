<?php

namespace App\Form;

use App\Entity\TableFilters;
use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TableFiltersType extends AbstractType
{
    public string $url;

    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('table', HiddenType::class, [
                'property_path' => 'table.id',
            ])
            ->add('filters', HiddenType::class)
            ->add('name', null, [
                'label' => "Nom de la sauvegarde",
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TableFilters::class,
        ]);
    }
}
