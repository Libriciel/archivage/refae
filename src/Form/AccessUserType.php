<?php

namespace App\Form;

use App\Entity\AccessUser;
use App\Entity\Ldap;
use App\Entity\Openid;
use App\Entity\Role;
use App\Repository\LdapRepository;
use App\Repository\OpenidRepository;
use App\Repository\RoleRepository;
use App\Service\LoggedUser;
use App\Service\Permission;
use App\Validator\LdapResponds;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccessUserType extends AbstractType
{
    public function __construct(
        private readonly LoggedUser $loggedUser,
    ) {
    }

    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var AccessUser $accessUser */
        $accessUser = $builder->getData();
        $builder
            ->add('loginType', ChoiceType::class, [
                'label' => "Méthode de connexion",
                'choices' => [
                    'Refae' => AccessUser::LOGIN_TYPE_REFAE,
                    'LDAP' => AccessUser::LOGIN_TYPE_LDAP,
                    'Openid' => AccessUser::LOGIN_TYPE_OPENID,
                ],
                'attr' => ['data-s2' => true],
                'mapped' => false,
            ])
            ->add('role', EntityType::class, [
                'label' => "Rôle de l'utilisateur",
                'class' => Role::class,
                'choice_label' => fn (Role $role) => $role->getCommonName(),
                'choice_attr' => fn (Role $role) => ['data-role' => $role->getName()],
                'attr' => ['data-s2' => true, 'required' => true],
                'query_builder' => fn(RoleRepository $roleRepository): QueryBuilder
                    => $roleRepository->createQueryBuilder('r')
                        ->leftJoin('r.tenant', 't')
                        ->andWhere('t.id IS NULL OR t.id = :tenant')
                        ->setParameter('tenant', $this->loggedUser->tenant())
                        ->andWhere('r.name != :adminTech')
                        ->setParameter('adminTech', Permission::ROLE_TECHNICAL_ADMIN)
                        ->orderBy('r.name'),
            ])
            ->add('openid', EntityType::class, [
                'label' => "Connexion via Openid",
                'class' => Openid::class,
                'choice_label' => 'name',
                'attr' => ['data-s2' => true],
                'row_attr' => ['class' => 'type-openid'],
                'query_builder' => fn(OpenidRepository $openidRepository): QueryBuilder
                    => $openidRepository->createQueryBuilder('o')
                        ->innerJoin('o.tenants', 't')
                        ->andWhere('t.id = :tenants')
                        ->setParameter('tenants', $this->loggedUser->tenant())
                        ->orderBy('o.name'),
            ])
            ->add('ldap', EntityType::class, [
                'label' => "Connexion via LDAP",
                'class' => Ldap::class,
                'choice_label' => 'name',
                'attr' => ['data-s2' => true, 'required' => true],
                'choice_attr' => fn (Ldap $ldap) => [
                    'data-ldap-map-username' => $ldap->getUserUsernameAttribute(),
                    'data-ldap-map-login' => $ldap->getUserLoginAttribute(),
                ],
                'row_attr' => ['class' => 'type-ldap'],
                'query_builder' => fn(LdapRepository $ldapRepository): QueryBuilder
                    => $ldapRepository->createQueryBuilder('l')
                        ->innerJoin('l.tenants', 't')
                        ->andWhere('t.id = :tenants')
                        ->setParameter('tenants', $this->loggedUser->tenant())
                        ->orderBy('l.name'),
                'constraints' => [new LdapResponds()],
            ])
            ->add('ldapUsername', null, [
                'label' => "Identifiant de connexion sur le LDAP",
                'attr' => ['required' => true, 'data-username' => $accessUser?->getUser()?->getUsername()],
                'row_attr' => ['class' => 'type-ldap required'],
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AccessUser::class,
        ]);
    }
}
