<?php

namespace App\Form;

use App\Entity\RgpdInfo;
use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminRgpdInfoType extends AbstractType
{
    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('orgName', TextareaType::class, [
                'label' => "Raison sociale",
                'required' => false,
            ])
            ->add('orgAddress', TextareaType::class, [
                'label' => "Adresse",
                'required' => false,
            ])
            ->add('orgSiret', TextType::class, [
                'label' => "Numéro SIRET",
                'required' => false,
            ])
            ->add('orgApe', TextType::class, [
                'label' => "Code APE",
                'required' => false,
            ])
            ->add('orgPhone', TextType::class, [
                'label' => "N° de téléphone",
                'required' => false,
            ])
            ->add('orgEmail', EmailType::class, [
                'label' => "E-mail",
                'required' => false,
            ])
            ->add('ceoName', TextType::class, [
                'label' => "Nom du représentant",
                'required' => false,
            ])
            ->add('ceoFunction', TextType::class, [
                'label' => "Qualité/fonction du représentant",
                'required' => false,
            ])
            ->add('comment', TextareaType::class, [
                'label' => "Mentions supplémentaires",
                'required' => false,
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RgpdInfo::class,
        ]);
    }
}
