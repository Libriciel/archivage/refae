<?php

namespace App\Form;

use App\Entity\Label;
use App\Entity\LabelGroup;
use App\Repository\LabelGroupRepository;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LabelType extends AbstractType
{
    public string $url;

    public function __construct(
        private readonly RequestStack $requestStack,
    ) {
    }

    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $request = $this->requestStack->getCurrentRequest();
        $tenantUrl = $request->get('tenant_url');
        $builder
            ->add('name', null, [
                'label' => "Nom de l'étiquette",
                'attr' => [
                    'title' => "Caractères interdits: " . Label::BANNED_CHARSET,
                    'pattern' => sprintf('((?![%s]).)*', Label::BANNED_CHARSET),
                ],
            ])
            ->add('labelGroup', EntityType::class, [
                'label' => "Groupe d'étiquettes",
                'class' => LabelGroup::class,
                'choice_label' => 'name',
                'placeholder' => "-- Choisir un groupe --",
                'required' => false,
                'query_builder' => fn(LabelGroupRepository $labelGroupRepository): QueryBuilder
                => $labelGroupRepository->queryOptions($tenantUrl),
            ])
            ->add('color', ColorType::class, [
                'label' => "Couleur de l'étiquette",
            ])
            ->add('description', null, [
                'label' => "Description"
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Label::class,
        ]);
    }
}
