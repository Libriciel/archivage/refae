<?php

namespace App\Form;

use App\Entity\AccessUser;
use App\Entity\Ldap;
use App\Entity\Openid;
use App\Entity\Role;
use App\Entity\Tenant;
use App\Repository\LdapRepository;
use App\Repository\OpenidRepository;
use App\Repository\RoleRepository;
use App\Repository\TenantRepository;
use App\Service\Permission;
use App\Validator\LdapResponds;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminAccessUserType extends AbstractType
{
    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var AccessUser $accessUser */
        $accessUser = $builder->getData();
        $user = $accessUser->getUser();

        $builder
            ->add('tenant', EntityType::class, [
                'label' => "Lié au tenant",
                'class' => Tenant::class,
                'placeholder' => "-- Choisir un tenant --",
                'choice_label' => 'name',
                'choice_attr' => fn (Tenant $tenant)
                    => ['data-baseurl' => $tenant->getBaseurl()],
                'attr' => ['data-s2' => true, 'required' => true],
                'query_builder' => fn(TenantRepository $tenantRepository): QueryBuilder
                    => $tenantRepository->queryForAccessUserAdd($user),
            ])
            ->add('role', EntityType::class, [
                'label' => "Rôle de l'utilisateur",
                'class' => Role::class,
                'placeholder' => "-- Choisir un rôle --",
                'choice_label' => fn (Role $role) => $role->getCommonName(),
                'choice_attr' => fn (Role $role)
                    => ['data-tenant' => $role->getTenant()?->getBaseurl()],
                'attr' => ['data-s2' => true, 'required' => true],
                'query_builder' => fn (RoleRepository $roleRepository): QueryBuilder
                    => $roleRepository->createQueryBuilder('r')
                        ->leftJoin('r.tenant', 't')
                        ->andWhere('r.name != :adminTech')
                        ->setParameter('adminTech', Permission::ROLE_TECHNICAL_ADMIN)
                        ->orderBy('r.name'),
            ])
            ->add('loginType', ChoiceType::class, [
                'label' => "Méthode de connexion",
                'choices' => [
                    'Refae' => AccessUser::LOGIN_TYPE_REFAE,
                    'LDAP' => AccessUser::LOGIN_TYPE_LDAP,
                    'Openid' => AccessUser::LOGIN_TYPE_OPENID,
                ],
                'mapped' => false,
                'attr' => ['data-s2' => true, 'required' => true],
            ])
            ->add('openid', EntityType::class, [
                'label' => "Connexion via Openid",
                'class' => Openid::class,
                'placeholder' => "-- Choisir un Openid --",
                'choice_label' => 'name',
                'choice_attr' => fn (Openid $openid)
                    => ['data-tenants' => $openid->getTenantJsonBaseurls()],
                'attr' => ['data-s2' => true],
                'row_attr' => ['class' => 'type-openid'],
                'query_builder' => fn (OpenidRepository $openidRepository): QueryBuilder
                    => $openidRepository->createQueryBuilder('o')
                        ->innerJoin('o.tenants', 't')
                        ->orderBy('o.name'),
            ])
            ->add('ldap', EntityType::class, [
                'label' => "Connexion via LDAP",
                'class' => Ldap::class,
                'placeholder' => "-- Choisir un LDAP --",
                'choice_label' => 'name',
                'choice_attr' => fn (Ldap $ldap) => [
                        'data-tenants' => $ldap->getTenantJsonBaseurls(),
                        'data-ldap-map-username' => $ldap->getUserUsernameAttribute(),
                        'data-ldap-map-login' => $ldap->getUserLoginAttribute(),
                ],
                'attr' => ['data-s2' => true, 'required' => true],
                'row_attr' => ['class' => 'type-ldap'],
                'query_builder' => fn (LdapRepository $ldapRepository): QueryBuilder
                    => $ldapRepository->createQueryBuilder('l')
                        ->innerJoin('l.tenants', 't')
                        ->orderBy('l.name'),
                'constraints' => [new LdapResponds()],
            ])
            ->add('ldapUsername', null, [
                'label' => "Identifiant de connexion sur le LDAP",
                'attr' => ['required' => true],
                'row_attr' => ['class' => 'type-ldap required'],
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AccessUser::class,
        ]);
    }
}
