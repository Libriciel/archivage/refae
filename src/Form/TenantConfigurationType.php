<?php

namespace App\Form;

use App\Entity\TenantConfiguration;
use App\Service\SessionFiles;
use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;

class TenantConfigurationType extends AbstractType
{
    public string $url;

    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly RouterInterface $router,
        private readonly SessionFiles $sessionFiles,
    ) {
    }

    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $attrs = [
            'data-uploader' => $this->router->generate('public_upload'),
            'accept' => 'image/*',
        ];
        $attrsLogoMain = $attrs;
        $attrsLogoLogin = $attrs;

        /** @var TenantConfiguration $tenantConfiguration */
        $tenantConfiguration = $builder->getData();
        $logoMain = $tenantConfiguration->getLogoMain();
        $logoLogin = $tenantConfiguration->getLogoLogin();

        if ($logoMain) {
            $attrsLogoMain['data-value'] = json_encode([
                'name' => $logoMain->getName(),
                'size' => $logoMain->getSize(),
                'uuid' => (string)$logoMain->getId(),
                'deleteUrl' => $options['deleteLogoMain'],
            ]);
        }
        if ($logoLogin) {
            $attrsLogoLogin['data-value'] = json_encode([
                'name' => $logoLogin->getName(),
                'size' => $logoLogin->getSize(),
                'uuid' => (string)$logoLogin->getId(),
                'deleteUrl' => $options['deleteLogoLogin'],
            ]);
        }

        $request = $this->requestStack->getCurrentRequest();
        $session_tmpfile_uuid_main = $request->get('tenant_configuration')['logoMain'] ?? null;
        if ($this->sessionFiles->exists($session_tmpfile_uuid_main)) {
            $uri = $this->sessionFiles->getUri($session_tmpfile_uuid_main);
            $attrsLogoMain['data-value'] = json_encode([
                'name' => basename((string) $uri),
                'size' => filesize($uri),
                'uuid' => $session_tmpfile_uuid_main,
            ]);
        }
        $session_tmpfile_uuid_login = $request->get('tenant_configuration')['logoLogin'] ?? null;
        if ($this->sessionFiles->exists($session_tmpfile_uuid_login)) {
            $uri = $this->sessionFiles->getUri($session_tmpfile_uuid_login);
            $attrsLogoLogin['data-value'] = json_encode([
                'name' => basename((string) $uri),
                'size' => filesize($uri),
                'uuid' => $session_tmpfile_uuid_login,
            ]);
        }
        $builder
            ->add('logoMainHandler', FileType::class, [
                'label' => "Logo affiché sur toutes les pages",
                'attr' => $attrsLogoMain,
                'help' => "hauteur conseillée: 50px à 75px",
                'required' => false,
                'mapped' => false,
            ])
            ->add('logoMainHeight', IntegerType::class, [
                'label' => "Hauteur du logo en pixels",
                'attr' => ['placeholder' => 75],
                'help' => "hauteur conseillée: 50px à 75px",
                'required' => false,
            ])
            ->add('logoLoginHandler', FileType::class, [
                'label' => "Logo affiché sur la page de connexion",
                'attr' => $attrsLogoLogin,
                'help' => "Taille conseillée supérieure à 300 px x 300 px " .
                    "(taille automatiquement attribuée selon l'écran de l'utilisateur)",
                'required' => false,
                'mapped' => false,
            ])
            ->add('logoLoginBgColor', ColorType::class, [
                'label' => "Couleur d'arrière-plan du logo sur la page de connexion",
                'required' => false,
            ])
            ->add('logoLoginText', null, [
                'label' => "Texte sous le logo sur la page de connexion",
                'required' => false,
            ])
            ->add('logoLoginTextStyle', null, [
                'label' => "Style CSS du texte",
                'required' => false,
            ])
            ->add('maxFilesize', IntegerType::class, [
                'label' => "Taille maximum des fichiers des référentiels",
                'attr' => ['placeholder' => 1048576, 'min' => 10240, 'max' => 9007199254739968, 'step' => 1024],
                'help' => "Valeur par défaut: 1048576 (1 Mo)",
                'required' => false,
            ])
            ->add('fileRetentionCount', IntegerType::class, [
                'label' => "Nombre de versions pendant lesquelles les fichiers sont conservés",
                'attr' => ['placeholder' => 3, 'min' => 1],
                'help' => "Valeur par défaut: 3",
                'required' => false,
            ])
            ->add('mailPrefix', null, [
                'label' => "Préfixe utilisé dans les e-mails",
                'attr' => ['placeholder' => '[refae]'],
                'required' => false,
            ])
            ->add('mailSignature', TextareaType::class, [
                'label' => "Signature des e-mails",
                'attr' => ['data-tinymce' => 'true'],
                'required' => false,
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'deleteLogoMain' => '',
            'deleteLogoLogin' => '',
        ]);
    }
}
