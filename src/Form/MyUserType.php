<?php

namespace App\Form;

use App\Entity\AccessUser;
use App\Entity\User;
use App\Form\Type\BooleanType;
use App\Form\Type\PasswordConfirmType;
use App\Service\Permission;
use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MyUserType extends AbstractType
{
    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var User $user */
        $user = $builder->getData();
        $hasFonctionnalAdminRole = $user->getAccessUsers()
            ->filter(
                fn(AccessUser $a)
                    => in_array(
                        $a->getRole()?->getName(),
                        [Permission::ROLE_FUNCTIONAL_ADMIN, Permission::ROLE_TECHNICAL_ADMIN]
                    )
            )
            ->count() !== 0;
        $builder
            ->add('username', TextType::class, [
                'label' => "Identifiant de connexion",
                'disabled' => true,
            ])
            ->add('name', TextType::class, [
                'label' => "Nom",
                'required' => false,
            ])
            ->add('email', EmailType::class, [
                'label' => "E-mail",
                'help' => "Le lien d'initialisation du mot de passe sera envoyé"
                    . " par e-mail dans le cas d'une connexion type refae",
            ])
            ->add('password', PasswordConfirmType::class, [
                'required' => false,
                'first_options' => [
                    'label' => "Mot de passe",
                    'help' => "Inutile en cas de connexion via LDAP ou Openid. " .
                        "Laisser vide pour conserver le mot de passe actuel.",
                ],
            ])
        ;
        if ($hasFonctionnalAdminRole) {
            $builder->add('notifyUserRegistration', BooleanType::class, [
                'label' => "Recevoir les demandes de création d'utilisateur",
                'expanded' => true,
            ]);
        }
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
