<?php

namespace App\Form;

use App\Entity\Ldap;
use App\Entity\Tenant;
use App\Repository\TenantRepository;
use Doctrine\ORM\QueryBuilder;
use Override;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LdapType extends AbstractType
{
    public string $url;

    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', HiddenType::class, ['mapped' => false])
            ->add('name', null, [
                'label' => "Nom du LDAP",
            ])
            ->add('description', null, [
                'label' => "Description",
            ])
            ->add('tenants', EntityType::class, [
                'class' => Tenant::class,
                'multiple' => true,
                'required' => false,
                'choice_label' => 'name',
                'attr' => ['data-s2' => true],
                'query_builder' => fn(TenantRepository $tenantRepository): QueryBuilder
                    => $tenantRepository->createQueryBuilder('r')->orderBy('r.name'),
            ])
            ->add('schema', ChoiceType::class, [ // obsolète
                'label' => "Schema",
                'choices' => [
                    "Active Directory" => 'Adldap\Schemas\ActiveDirectory',
                    "Open LDAP" => 'Adldap\Schemas\OpenLDAP',
                    "Free IPA" => 'Adldap\Schemas\FreeIPA',
                ],
            ])
            ->add('version', ChoiceType::class, [
                'label' => "Version LDAP",
                'choices' => [
                    "LDAP version 2" => 2,
                    "LDAP version 3" => 3,
                ],
            ])
            ->add('host', null, [
                'label' => "Hôte du serveur",
            ])
            ->add('port', NumberType::class, [
                'label' => "Port du serveur",
                'attr' => [
                    'min' => 1,
                    'max' => 65535,
                ],
            ])
            ->add('use_proxy', null, [
                'label' => "Utiliser le proxy",
            ])
            ->add('use_ssl', null, [
                'label' => "SSL",
            ])
            ->add('use_tls', null, [
                'label' => "TLS",
            ])
            ->add('timeout', NumberType::class, [
                'label' => "Timeout",
                'attr' => [
                    'min' => 1,
                    'max' => 30,
                ],
            ])
            ->add('custom_options', HiddenType::class)
            ->add('follow_referrals', null, [
                'label' => "Suivre les références LDAP",
            ])
            ->add('user_query_login', null, [
                'label' => "Identifiant de connexion pour les recherches",
                'attr' => [
                    'placeholder' => "admin@refae.fr",
                ],
            ])
            ->add('user_query_password', PasswordType::class, [
                'label' => "Mot de passe pour les recherches",
                'required' => false,
                'help' => "Laisser vide pour ne pas modifier"
            ])
            ->add('ldap_root_search', null, [
                'label' => "DN de base",
                'attr' => [
                    'placeholder' => "dc=refae,dc=fr",
                ],
            ])
            ->add('ldap_users_filter', null, [
                'label' => "Filtre supplémentaire de recherche",
                'attr' => [
                    'placeholder' => "(memberOf=refae)",
                ],
                'help' => "Il est conseillé de limiter la recherche aux utilisateurs "
                    . "et de placer des parentèses autours de la condition. ex: "
                    . "(objectClass=inetOrgPerson)"
            ])
            ->add('account_prefix', null, [
                'label' => "Prefix pour le login",
                'attr' => [
                    'placeholder' => "uid=",
                ],
            ])
            ->add('account_suffix', null, [
                'label' => "Suffix pour le login",
                'attr' => [
                    'placeholder' => "@refae.fr",
                ],
            ])
            ->add('test_username', null, [
                'label' => "Identifiant (hors mapping, celui utilisé pour la connexion LDAP)",
                'required' => false,
            ])
            ->add('test_password', PasswordType::class, [
                'label' => "Mot de passe",
                'required' => false,
            ])
            ->add('user_login_attribute', null, [
                'label' => "Nom de l'attribut utilisé pour se logger au LDAP",
                'attr' => [
                    'placeholder' => "sAMAccountName",
                ],
            ])
            ->add('user_username_attribute', null, [
                'label' => "Nom de l'attribut utilisé pour se logger à refae",
                'attr' => [
                    'placeholder' => "uid",
                ],
            ])
            ->add('user_name_attribute', null, [
                'label' => "Attribut utilisé pour le champ nom",
                'attr' => [
                    'placeholder' => "displayname",
                ],
            ])
            ->add('user_mail_attribute', null, [
                'label' => "Attribut utilisé pour le champ mail",
                'attr' => [
                    'placeholder' => "mail",
                ],
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Ldap::class,
        ]);
    }
}
