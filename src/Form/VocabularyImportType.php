<?php

namespace App\Form;

use App\Service\Manager\VocabularyManager;
use App\Service\SessionFiles;
use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

class VocabularyImportType extends AbstractType
{
    public string $url;

    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly RouterInterface $router,
        private readonly SessionFiles $sessionFiles,
    ) {
    }

    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $attrs = [
            'data-uploader' => $this->router->generate('public_upload'),
            'accept' => sprintf('.%s.refae', VocabularyManager::getFileName()),
        ];
        $request = $this->requestStack->getCurrentRequest();
        $session_tmpfile_uuid = $request->get('vocabulary_import')['file'] ?? null;
        if ($session_tmpfile_uuid) {
            $uri = $this->sessionFiles->getUri($session_tmpfile_uuid);
            $attrs['data-value'] = json_encode([
                'name' => basename((string) $uri),
                'size' => filesize($uri),
                'uuid' => $session_tmpfile_uuid,
            ]);
        }
        $builder
            ->add('file', FileType::class, [
                'label' => "Fichier d'export de vocabulaires contrôlés",
                'attr' => $attrs,
            ]);
    }
}
