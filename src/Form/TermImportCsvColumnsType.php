<?php

namespace App\Form;

use Override;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TermImportCsvColumnsType extends AbstractType
{
    #[Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if ($options['type'] !== 'csv') {
            return;
        }
        unset($options['columns']['id']);
        $columns = array_keys($options['columns']);
        $colomnNames = array_combine($columns, $columns);
        $builder
            ->add('name', ChoiceType::class, [
                'label' => "Colonne pour le nom du terme",
                'choices' => $colomnNames,
                'attr' => ['data-s2' => true],
                'data' => 'A',
            ])
            ->add('identifier', ChoiceType::class, [
                'label' => "Colonne pour l'identifiant du terme dans un référentiel",
                'required' => false,
                'choices' => $colomnNames,
                'attr' => ['data-s2' => true],
                'data' => 'B',
            ])
            ->add('directoryName', ChoiceType::class, [
                'label' => "Colonne pour le nom ou l'identifiant du référentiel",
                'required' => false,
                'choices' => $colomnNames,
                'attr' => ['data-s2' => true],
            ])
            ->add('firstLine', IntegerType::class, [
                'label' => "Numéro de ligne du 1er terme",
                'attr' => [
                    'min' => 1,
                ],
                'data' => 1,
            ])
        ;
    }

    #[Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'columns' => [],
            'type' => 'csv',
        ]);
    }
}
