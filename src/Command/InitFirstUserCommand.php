<?php

namespace App\Command;

use App\Entity\AccessUser;
use App\Entity\User;
use App\Entity\UserPasswordRenewToken;
use App\Repository\RoleRepository;
use App\Repository\TenantRepository;
use App\Repository\UserRepository;
use App\Service\Permission;
use Doctrine\ORM\EntityManagerInterface;
use Override;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

#[AsCommand(
    name: 'app:init:first-user',
    description: "Ajoute le 1er admin fonctionnel sur le 1er tenant",
)]
class InitFirstUserCommand extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly UserRepository $userRepository,
        private readonly RoleRepository $roleRepository,
        private readonly TenantRepository $tenantRepository,
        private readonly RouterInterface $router,
        private readonly MailerInterface $mailer,
    ) {
        parent::__construct();
        $context = $this->router->getContext();
        $context->setScheme('https');
        if ($_ENV['HTTP_HOST'] ?? null) {
            $context->setScheme($_ENV['HTTP_PROTOCOL'] ?? 'https');
            $context->setHost($_ENV['HTTP_HOST']);
            if (isset($_ENV['HTTP_PORT'])) {
                if (($_ENV['HTTP_PROTOCOL'] ?? 'https') === 'https') {
                    $context->setHttpsPort((int)ltrim(((string) ($_ENV['HTTP_PORT'] ?: '443')), ':'));
                } else {
                    $context->setHttpPort((int)ltrim(((string) ($_ENV['HTTP_PORT'] ?: '80')), ':'));
                }
            }
            $context->setBaseUrl($_ENV['HTTP_BASEURL'] ?? '');
        }
    }

    #[Override]
    protected function configure(): void
    {
        $this
            ->addArgument('username', InputArgument::REQUIRED, 'username')
            ->addArgument('name', InputArgument::REQUIRED, 'name')
            ->addArgument('email', InputArgument::REQUIRED, 'email')
        ;
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $username = $input->getArgument('username');
        $name = $input->getArgument('username');
        $email = $input->getArgument('email');

        if (!$username || !$email) {
            $output->writeln("Argument obligatoire manquant");
        }

        $count = $this->tenantRepository->countAll();
        if ($count === 0) {
            $output->writeln("Aucun tenant");
            return Command::FAILURE;
        }
        if ($count > 1) {
            $output->writeln("Plus d'un tenant existe");
            return Command::FAILURE;
        }

        $tenant = $this->tenantRepository->findOneBy([]);

        if ($this->userRepository->countByTenant($tenant) > 0) {
            $output->writeln("User already exists");
            return Command::FAILURE;
        }

        $user = $this->userRepository->findOneByUsername($username);
        $sendMail = false;
        if (!$user) {
            $user = new User();
            $user->setUsername($username);
            $user->setName($name);
            $user->setEmail($email);
            $user->setNotifyUserRegistration(true);
            $user->setUserDefinedPassword(false);
            $sendMail = true;
        }
        $user->addTenant($tenant);

        $this->entityManager->persist($user);

        $role = $this->roleRepository->findOneDefault(Permission::ROLE_FUNCTIONAL_ADMIN);
        if (!$role) {
            $output->writeln("Role not found");
            return Command::FAILURE;
        }

        $accessUser = new AccessUser();
        $accessUser->setRole($role);
        $accessUser->setUser($user);
        $accessUser->setTenant($tenant);
        $this->entityManager->persist($accessUser);
        $this->entityManager->flush();

        if ($sendMail) {
            $userPasswordRenewToken = new UserPasswordRenewToken();
            $userPasswordRenewToken->setUser($user);
            $this->entityManager->persist($userPasswordRenewToken);
            $this->entityManager->flush();

            $url =  $this->router->generate(
                'public_user_select_password',
                ['token' => $userPasswordRenewToken->getToken()],
                UrlGeneratorInterface::ABSOLUTE_URL,
            );
            $urlAlt =  $this->router->generate(
                'app_tenant_login_username',
                ['tenant_url' => $tenant->getBaseurl(), 'username' => $user->getUsername()],
                UrlGeneratorInterface::ABSOLUTE_URL,
            );
            $mailPrefix = '[refae]';
            $email = (new TemplatedEmail())
                ->from($_ENV['MAILER_FROM'] ?? 'refae@test.fr')
                ->to($user->getEmail())
                ->subject($mailPrefix . 'Création de votre compte')
                ->htmlTemplate('email/user-created-ask-password.html.twig')
                ->context([
                    'user' => $user,
                    'url' => $url,
                    'url_alt' => $urlAlt,
                    'tenant' => $tenant,
                    'signature' => null,
                ])
            ;
            $this->mailer->send($email);
        }

        return Command::SUCCESS;
    }
}
