<?php

namespace App\Command;

use App\Entity\AccessUser;
use App\Entity\User;
use App\Entity\UserPasswordRenewToken;
use App\Repository\RoleRepository;
use App\Repository\UserRepository;
use App\Service\Permission;
use Doctrine\ORM\EntityManagerInterface;
use Override;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

#[AsCommand(
    name: 'app:init:first-admin',
    description: "Ajoute le 1er admin technique",
)]
class InitFirstAdminCommand extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly UserRepository $userRepository,
        private readonly RoleRepository $roleRepository,
        private readonly RouterInterface $router,
        private readonly MailerInterface $mailer,
    ) {
        parent::__construct();
        $context = $this->router->getContext();
        $context->setScheme('https');
        if ($_ENV['HTTP_HOST'] ?? null) {
            $context->setScheme($_ENV['HTTP_PROTOCOL'] ?? 'https');
            $context->setHost($_ENV['HTTP_HOST']);
            if (isset($_ENV['HTTP_PORT'])) {
                if (($_ENV['HTTP_PROTOCOL'] ?? 'https') === 'https') {
                    $context->setHttpsPort((int)ltrim(((string) ($_ENV['HTTP_PORT'] ?: '443')), ':'));
                } else {
                    $context->setHttpPort((int)ltrim(((string) ($_ENV['HTTP_PORT'] ?: '80')), ':'));
                }
            }
            $context->setBaseUrl($_ENV['HTTP_BASEURL'] ?? '');
        }
    }

    #[Override]
    protected function configure(): void
    {
        $this
            ->addArgument('username', InputArgument::REQUIRED, 'username')
            ->addArgument('name', InputArgument::REQUIRED, 'name')
            ->addArgument('email', InputArgument::REQUIRED, 'email')
        ;
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $username = $input->getArgument('username');
        $name = $input->getArgument('username');
        $email = $input->getArgument('email');

        if (!$username || !$email) {
            $output->writeln("Argument obligatoire manquant");
        }
        if ($this->userRepository->findBy([])) {
            $output->writeln("User already exists");
            return Command::FAILURE;
        }

        $user = new User();
        $user->setUsername($username);
        $user->setName($name);
        $user->setEmail($email);
        $user->setNotifyUserRegistration(true);
        $user->setUserDefinedPassword(false);

        $this->entityManager->persist($user);

        $role = $this->roleRepository->findOneDefault(Permission::ROLE_TECHNICAL_ADMIN);
        if (!$role) {
            $output->writeln("Role not found");
            return Command::FAILURE;
        }

        $accessUser = new AccessUser();
        $accessUser->setRole($role);
        $accessUser->setUser($user);
        $this->entityManager->persist($accessUser);

        $userPasswordRenewToken = new UserPasswordRenewToken();
        $userPasswordRenewToken->setUser($user);
        $this->entityManager->persist($userPasswordRenewToken);
        $this->entityManager->flush();

        $url = $this->router->generate(
            'admin_technical_admin_select_password',
            ['token' => $userPasswordRenewToken->getToken()],
            UrlGeneratorInterface::ABSOLUTE_URL,
        );
        $urlAlt = $this->router->generate(
            'public_login_username',
            ['username' => $user->getUsername()],
            UrlGeneratorInterface::ABSOLUTE_URL,
        );
        $mailPrefix = '[refae]';
        $email = (new TemplatedEmail())
            ->from($_ENV['MAILER_FROM'] ?? 'refae@test.fr')
            ->to($user->getEmail())
            ->subject($mailPrefix . 'Création de votre compte')
            ->htmlTemplate('email/technical-admin-created-ask-password.html.twig')
            ->context([
                'user' => $user,
                'url' => $url,
                'url_alt' => $urlAlt,
                'choosePassword' => true,
            ])
        ;
        $this->mailer->send($email);

        return Command::SUCCESS;
    }
}
