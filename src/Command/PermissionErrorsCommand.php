<?php

namespace App\Command;

use App\Repository\PermissionPrivilegeRepository;
use App\Repository\PrivilegeRepository;
use App\Service\Permission;
use Override;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:permission:errors',
    description: "Donne la liste des permissions incohérentes",
)]
class PermissionErrorsCommand extends Command
{
    public function __construct(
        private readonly Permission $permission,
        private readonly PermissionPrivilegeRepository $permissionPrivilegeRepository,
        private readonly PrivilegeRepository $privilegeRepository,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $error = false;
        foreach ($this->permissionPrivilegeRepository->findUseless() as $permissionPrivilege) {
            $output->writeln(
                sprintf(
                    "La permission acl/%s/%s lié au privilège %s n'est lié à aucun role",
                    $permissionPrivilege->getPermissionClassname(),
                    $permissionPrivilege->getAction(),
                    $permissionPrivilege->getPrivilege()->getName(),
                )
            );
            $error = true;
        }

        foreach ($this->permission->routePermissions() as $classname => $actions) {
            foreach ($actions as $action) {
                $permissionPrivilege = $this->permissionPrivilegeRepository->findOneBy(
                    [
                        'permissionClassname' => $classname,
                        'action' => $action,
                    ]
                );
                if (!$permissionPrivilege) {
                    $output->writeln(
                        sprintf(
                            "La permission acl/%s/%s n'a pas de privilège lié",
                            $classname,
                            $action,
                        )
                    );
                    $error = true;
                }
            }
        }

        foreach ($this->privilegeRepository->findRoleLess() as $privilege) {
            $output->writeln(
                sprintf(
                    "Le privilège %s n'est pas lié à un rôle",
                    $privilege->getName(),
                )
            );
            $error = true;
        }

        if (!$error) {
            $output->writeln("Aucune erreur n'a été détectée");
        }
        return $error ? Command::FAILURE : Command::SUCCESS;
    }
}
