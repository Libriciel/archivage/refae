<?php

namespace App\Command;

use App\Entity\MeilisearchIndexedInterface;
use App\Repository\VersionableEntityRepositoryInterface;
use App\Service\MeilisearchInterface;
use Doctrine\ORM\EntityManagerInterface;
use Override;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:meilisearch:index',
    description: "Ajoute toutes les entités dans meilisearch",
)]
class MeilisearchIndexCommand extends Command
{
    public function __construct(
        private readonly MeilisearchInterface $meilisearch,
        private readonly EntityManagerInterface $entityManager,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $metas = $this->entityManager->getMetadataFactory()->getAllMetadata();
        sort($metas);
        foreach ($metas as $meta) {
            if (in_array(MeilisearchIndexedInterface::class, class_implements($meta->getName()))) {
                $indexName = $this->meilisearch->classnameToIndexname($meta->getName());
                $output->writeln("index: $indexName");
                $this->meilisearch->client->deleteIndex($indexName);
                $this->fillIndex($meta->getName());
            }
        }

        return Command::SUCCESS;
    }

    private function fillIndex(string $classname)
    {
        $repository = $this->entityManager->getRepository($classname);
        if ($repository instanceof VersionableEntityRepositoryInterface) {
            $query = $repository->findBy([], ['version' => 'ASC']);
        } else {
            $query = $repository->findAll();
        }
        foreach ($query as $entity) {
            $this->meilisearch->add($entity);
        }
    }
}
