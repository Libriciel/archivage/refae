<?php

namespace App\Command;

use App\Cron;
use App\Entity\Cron as CronEntity;
use App\Entity\CronExecution;
use App\Kernel;
use App\Repository\CronRepository;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Override;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

#[AsCommand(
    name: 'app:run:cron',
    description: 'Lancement des crons',
)]
class CronCommand extends Command
{
    private ?CronExecution $currentCronExecution = null;

    public function __construct(
        private readonly CronRepository $cronRepository,
        private readonly Kernel $kernel,
        private readonly EntityManagerInterface $entityManager,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        register_shutdown_function([$this, 'handleShutdown']);

        /** @var CronEntity[] $crons */
        $crons = $this->cronRepository->findNextScheduled();
        foreach ($crons as $cron) {
            $cronExecution = new CronExecution();
            $cronExecution
                ->setState(CronExecution::S_RUNNING)
                ->setCron($cron)
                ->setDateBegin(new DateTime())
            ;
            /** @var Cron\CronInterface $service */
            $service = $this->kernel->getContainer()->get($cron->getService());
            $interval = new DateInterval($cron->getFrequency());
            $this->currentCronExecution = $cronExecution;
            $this->entityManager->persist($cronExecution);
            $this->entityManager->flush();
            try {
                $output->writeln(
                    date('Y-m-d H:i:s - ') . "Lancement de la tâche " . $cron->getName()
                );
                $service->work($input, $output, $cronExecution);
                $this->currentCronExecution->setState(CronExecution::S_SUCCESS);
                $now = new DateTime();
                while ($cron->getNext() < $now) {
                    $current = $cron->getNext();
                    $next = (clone $current)->add($interval);
                    $cron->setNext($next);
                    if ($current == $next) {
                        throw new Exception(
                            "Erreur lors de l'attribution de la prochaine date d'execution"
                        );
                    }
                }
                $output->writeln(
                    date('Y-m-d H:i:s - ') . "Tâche terminée avec succès\n\n"
                );
            } catch (Throwable $e) {
                $output->writeln(
                    date('Y-m-d H:i:s - ') . $e->getMessage() . "\n" . $e->getTraceAsString()
                );
                $this->currentCronExecution->setReport(
                    $this->currentCronExecution->getReport()
                    . "\n" . date('Y-m-d H:i:s - ') . "Erreur: " . $e->getMessage()
                    . "\n" . $e->getTraceAsString()
                );
                $this->currentCronExecution->setState(CronExecution::S_ERROR);
            }
            $this->currentCronExecution->setDateEnd(new DateTime());
            $this->entityManager->persist($cron);
            $this->entityManager->persist($this->currentCronExecution);
            $this->entityManager->flush();
        }

        return Command::SUCCESS;
    }

    private function handleShutdown()
    {
        if ($this->currentCronExecution?->getState() === CronExecution::S_RUNNING) {
            $this->currentCronExecution->setState(CronExecution::S_ABORTED);
            $this->currentCronExecution->setReport(
                $this->currentCronExecution->getReport()
                . "\n" . date('Y-m-d H:i:s - ') . "La tâche a été arrêtée de manière inattendue"
            );
            $this->currentCronExecution->setDateEnd(new DateTime());
            $this->entityManager->persist($this->currentCronExecution);
            $this->entityManager->flush();
        }
    }
}
