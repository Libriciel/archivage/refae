<?php

namespace App\Command;

use App\Repository\UserRepository;
use App\Service\Manager\UserManager;
use Override;
use RuntimeException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:set:user-password',
    description: "Défini le mot de passe refae d'un utilisateur",
)]
class SetUserPasswordCommand extends Command
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly UserManager $userManager,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function configure(): void
    {
        $this
            ->addArgument('username', InputArgument::OPTIONAL, 'username')
            ->addArgument('password', InputArgument::OPTIONAL, 'password')
        ;
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');

        $question = new Question("Username?\n", false);
        $question->setTrimmable(true);
        $question->setValidator(function (string $answer): string {
            // TODO: ajout tenant car l'unicité sera sur le coupe tenant_id / username
            if (!$this->userRepository->findOneBy(['username' => $answer])) {
                throw new RuntimeException('User not found');
            }
            return $answer;
        });

        // pour valider si valeur fourni en arg
        if ($username = $input->getArgument('username')) {
            $question->getValidator()($username);
        }
        while (is_null($username) || $username === '') {
            $username = $helper->ask($input, $output, $question);
        }
        $user = $this->userRepository->findOneBy(['username' => $username]);

        $password = $input->getArgument('password');
        $question = new Question("Password?\n", false);
        $question->setHidden(true);
        $question->setHiddenFallback(false);
        while (is_null($password) || $password === '') {
            $password = $helper->ask($input, $output, $question);
        }

        $this->userManager->edit($user, $password);
        $io->success(sprintf("Mot de passe de l'utilisateur: %s modifié avec succès", $user->getUsername()));
        return Command::SUCCESS;
    }
}
