<?php

namespace App\Command;

use App\Entity\Category;
use App\Entity\DocumentRule;
use App\Entity\DocumentRuleFile;
use App\Entity\File;
use App\Entity\ManagementRule;
use App\Entity\Profile;
use App\Entity\Tenant;
use App\Entity\VersionableEntityInterface;
use App\Repository\CategoryRepository;
use App\Repository\DocumentRuleRepository;
use App\Repository\ManagementRuleRepository;
use App\Repository\ProfileRepository;
use App\Repository\TenantRepository;
use App\Service\Manager\ActivityManager;
use Doctrine\ORM\EntityManagerInterface;
use Override;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

#[AsCommand(
    name: 'app:import-refae-1-1',
    description: "Importe les données d'un export de base refae 1",
)]
class ImportRefae1Command extends Command
{
    private InputInterface $input;
    private ?Tenant $tenant = null;
    /**
     * @var Category[] [id_export => entity]
     */
    private array $categories;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly TenantRepository $tenantRepository,
        private readonly ManagementRuleRepository $managementRuleRepository,
        private readonly ProfileRepository $profileRepository,
        private readonly CategoryRepository $categoryRepository,
        private readonly DocumentRuleRepository $documentRuleRepository,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function configure(): void
    {
        $this
            ->addArgument(
                name: 'tenant',
                mode: InputArgument::REQUIRED,
                description: "Baseurl du tenant sur lequel importer le fichier",
            )
            ->addArgument(
                name: 'filename',
                mode: InputArgument::REQUIRED,
                description: "Fichier cible",
            )
            ->addOption(
                name: 'public',
                mode: InputOption::VALUE_NONE,
                description: "les documents importés seront public",
            )
        ;
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $output1 = $output;
        $io = new SymfonyStyle($input, $output);
        $this->tenant = $this->tenantRepository->findOneByBaseurl($input->getArgument('tenant'));
        if (!$this->tenant) {
            $io->error("Le tenant {$input->getArgument('tenant')} n'a pas été trouvé");
            return Command::FAILURE;
        }

        $export = json_decode(file_get_contents($input->getArgument('filename')), true);
        if (!$export) {
            $io->error("Le fichier indiqué n'est pas un fichier json valide");
            return Command::FAILURE;
        }
        if (!isset($export['categories']) || !isset($export['categories_documents']) || !isset($export['documents'])) {
            $io->error("Le fichier n'est pas un export refae 1 valide");
            return Command::FAILURE;
        }
        $this->entityManager->beginTransaction();
        try {
            $countCategories = 0;
            $this->importCategories($export['categories'], null, $countCategories);
            $output1->writeln("Import de $countCategories catégories.");
            $countDocuments = 0;
            $this->importDocuments($export['documents'], $countDocuments);
            $output1->writeln("Import de $countDocuments règles documentaires.");
            $this->entityManager->flush();
            $this->entityManager->commit();
        } catch (ImportRefae1Exception $e) {
            $this->entityManager->rollback();
            $io->error($e->getMessage());
            $output1->writeln("rollback effectué");
            return Command::FAILURE;
        } catch (Throwable $e) {
            $this->entityManager->rollback();
            throw $e;
        }
        return Command::SUCCESS;
    }

    /**
     * @param array         $exportedCategories
     * @param Category|null $parent
     * @param int|null      $count
     * @return void
     */
    private function importCategories(array $exportedCategories, ?Category $parent = null, int &$count = null): void
    {
        foreach ($exportedCategories as $exportedCategory) {
            $path = $parent ? $parent->getPath() . ' ⮕ ' . $exportedCategory['name'] : $exportedCategory['name'];
            $categories = $this->categoryRepository->findByPath(explode(' ⮕ ', (string) $path), $this->tenant);
            $category = $categories ? $categories[0] : null;
            if (!$category) {
                $category = new Category();
                $category->setTenant($this->tenant);
                $category->setParent($parent);
                $category->setName($exportedCategory['name']);
                $category->setDescription($exportedCategory['description']);
                $activity = ActivityManager::newActivity($category, 'import', 'import_refae1');
                $this->entityManager->persist($activity);
                $this->entityManager->persist($category);
                $count++;
            }
            $this->categories[$exportedCategory['id']] = $category;
            $category->setPath($path);
            $this->importCategories($exportedCategory['children'], $category, $count);
        }
    }

    /**
     * @param array $export
     * @return void
     * @throws ImportRefae1Exception
     */
    private function checkRequiredFields(array $export): void
    {
        $required = [
            'identifiant',
            'name',
            'support',
            'sort_final',
            'dua',
            'communicabilite',
        ];
        foreach ($required as $key) {
            if (!isset($export[$key])) {
                throw new ImportRefae1Exception(
                    "Un champ obligatoire est vide dans documents : $key"
                );
            }
        }
    }

    /**
     * @param array $exportedDocuments
     * @param int   $count
     * @return void
     * @throws ImportRefae1Exception
     */
    private function importDocuments(array $exportedDocuments, int &$count): void
    {
        foreach ($exportedDocuments as $exportedDocument) {
            $this->checkRequiredFields($exportedDocument);
            $documents = $this->documentRuleRepository
                ->findByIdentifiersForTenant($this->tenant, [$exportedDocument['identifiant']]);
            if ($documents) {
                continue;
            }
            $document = new DocumentRule();
            $document->setTenant($this->tenant);
            $document->setName($exportedDocument['name']);
            switch ($exportedDocument['support']) {
                case 'P':
                    $document->setDocumentSupport(DocumentRule::PAPER);
                    break;
                case 'E':
                    $document->setDocumentSupport(DocumentRule::ELECTRONIC);
                    break;
                case 'P et E':
                    $document->setDocumentSupport(DocumentRule::MIXED);
                    break;
            }
            $document->setLiteralFileFormat($exportedDocument['file_format']);
            $document->setAppraisalRule($this->getAppraisalRule($exportedDocument['dua']));
            $document->setAccessRule($this->getAccessRule($exportedDocument['communicabilite']));
            switch ($exportedDocument['sort_final']) {
                case 'D':
                    $document->setAppraisalRuleFinalAction(DocumentRule::DESTROY);
                    break;
                case 'C':
                    $document->setAppraisalRuleFinalAction(DocumentRule::KEEP);
                    break;
                case 'T':
                    $document->setAppraisalRuleFinalAction(DocumentRule::SORT);
                    break;
            }
            $document->setObservation($exportedDocument['description']);
            $document->setIdentifier($exportedDocument['identifiant']);
            if ($exportedDocument['duc']) {
                $document->setStorageRule($this->getStorageRule($exportedDocument['duc']));
            }
            $document->setAppraisalRuleLiteralStartDate($exportedDocument['depart_calcul_dua']);
            $document->setStorageRuleLiteralStartDate($exportedDocument['depart_calcul_duc']);
            if ($exportedDocument['profil_archivage']) {
                $document->setProfile($this->getProfile($exportedDocument['profil_archivage']));
            }
            $document->setAccessRuleLiteralStarDate($exportedDocument['depart_calcul_com']);
            $document->setDocumentLocation($exportedDocument['localisation']);
            $document->setTechnicalMetadata($exportedDocument['app_meta']);
            if ($exportedDocument['file']) {
                $document->addDocumentRuleFile($this->getFile($exportedDocument['file']));
            }

            $document->setPublic($this->input->getOption('public'));
            $document->setState(VersionableEntityInterface::S_PUBLISHED);
            $document->setVersion(1);

            foreach ($exportedDocument['categories'] as $category) {
                if (isset($this->categories[$category['id']])) {
                    $document->addCategory($this->categories[$category['id']]);
                }
            }

            $activity = ActivityManager::newActivity($document, 'import', 'import_refae1');
            $this->entityManager->persist($activity);
            $this->entityManager->persist($document);
            $count++;
        }
    }

    /**
     * @param string $dua
     * @return ManagementRule
     * @throws ImportRefae1Exception
     */
    private function getAccessRule(string $communicabilite): ManagementRule
    {
        $accessRule = $this->managementRuleRepository
            ->findOneAccessByTenantIdentifier($this->tenant, $communicabilite)
        ;
        if (!$accessRule) {
            throw new ImportRefae1Exception(
                "Code de restriction d'accès $communicabilite non défini" .
                " dans les règles de gestion. Ajouter la règle de gestion de type" .
                " \"Gestion de la communicabilité\" avant un nouvel import."
            );
        }
        return $accessRule;
    }

    /**
     * @param string $dua
     * @return ManagementRule
     * @throws ImportRefae1Exception
     */
    private function getAppraisalRule(string $dua): ManagementRule
    {
        $appraisalRule = $this->managementRuleRepository
            ->findOneAppraisalByTenantDuration($this->tenant, (int)$dua)
        ;
        if (!$appraisalRule) {
            throw new ImportRefae1Exception(
                "Aucune règle de durée d'utilité administrative de durée" .
                " $dua définie dans les règles de gestion. Ajouter la règle de" .
                " gestion de type \"Gestion de la durée d’utilité administrative\"" .
                " avant un nouvel import."
            );
        }
        return $appraisalRule;
    }

    /**
     * @param string $duc
     * @return ManagementRule
     * @throws ImportRefae1Exception
     */
    private function getStorageRule(string $duc): ManagementRule
    {
        $appraisalRule = $this->managementRuleRepository
            ->findOneStorageByTenantDuration($this->tenant, (int)$duc)
        ;
        if (!$appraisalRule) {
            throw new ImportRefae1Exception(
                "Aucune règle de durée d'utilité courante de durée" .
                " $duc définie dans les règles de gestion. Ajouter la règle de" .
                " gestion de type \"Gestion de la durée d’utilité courante\"" .
                " avant un nouvel import."
            );
        }
        return $appraisalRule;
    }

    private function getProfile(string $profil): Profile
    {
        $profile = $this->profileRepository
            ->findOneByTenantIdentifier($this->tenant, $profil)
        ;
        if (!$profile) {
            throw new ImportRefae1Exception(
                "Profil d'archive $profil non défini dans les règles de" .
                " gestion. Ajouter le profil d'archive et le nommer \"$profil\"" .
                " avant un nouvel import."
            );
        }
        return $profile;
    }

    private function getFile(array $fileExport): DocumentRuleFile
    {
        $tmpFile = tmpfile();
        fwrite($tmpFile, base64_decode((string) $fileExport['content']));
        rewind($tmpFile);
        $mime = mime_content_type($tmpFile);
        fclose($tmpFile);
        $file = new File();
        $file
            ->setName(basename((string) $fileExport['filename']))
            ->setContent(base64_decode((string) $fileExport['content']))
            ->setHash($fileExport['hash'])
            ->setHashAlgo($fileExport['hash_algo'])
            ->setMime($mime)
            ->setSize($fileExport['size'])
        ;
        $documentFile = new DocumentRuleFile();
        $documentFile->setFile($file);
        $documentFile->setType(DocumentRuleFile::TYPE_SCHEMA);
        $this->entityManager->persist($file);
        $this->entityManager->persist($documentFile);
        return $documentFile;
    }
}
