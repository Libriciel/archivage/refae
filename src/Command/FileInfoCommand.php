<?php

namespace App\Command;

use App\Entity\File;
use App\Repository\FileRepository;
use Override;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Serializer\SerializerInterface;

#[AsCommand(
    name: 'app:file:info',
    description: 'Informations sur un fichier en base',
)]
class FileInfoCommand extends Command
{
    public function __construct(
        private readonly FileRepository $fileRepository,
        private readonly SerializerInterface $serializer,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function configure(): void
    {
        $this->addArgument('id', InputArgument::REQUIRED, 'Id du fichier en base');
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $id = $input->getArgument('id');

        $file = $this->fileRepository->find($id);
        if (!$file) {
            $io->error(sprintf("File %s not found", $id));
            return Command::FAILURE;
        }

        $display = [File::class];
        foreach ($this->serializer->normalize($file, context: ['groups' => ['view']]) as $attr => $value) {
            $display[] = [$attr => $value];
        }

        $io->definitionList(...$display);

        return Command::SUCCESS;
    }
}
