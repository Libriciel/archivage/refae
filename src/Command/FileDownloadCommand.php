<?php

namespace App\Command;

use App\Repository\FileRepository;
use Override;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

#[AsCommand(
    name: 'app:file:download',
    description: 'Récupère un fichier depuis la base de donnée',
)]
class FileDownloadCommand extends Command
{
    public function __construct(
        private readonly FileRepository $fileRepository,
        private readonly ContainerBagInterface $params,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function configure(): void
    {
        $this->addArgument('id', InputArgument::REQUIRED, 'Id du fichier en base');
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $id = $input->getArgument('id');
        $path = $this->params->get('app.paths.tmp');

        $file = $this->fileRepository->find($id);
        if (!$file) {
            $io->error(sprintf("File %s not found", $id));
            return Command::FAILURE;
        }

        if (!is_dir($path)) {
            $io->error('Destination not found');
            return Command::FAILURE;
        }

        if (!is_writable($path)) {
            $io->error('Destination not writable');
            return Command::FAILURE;
        }

        if (!$file->getHashIntegrity()) {
            /** @var QuestionHelper $helper */
            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion(
                "Hash incohérent, continuer ? [y/n]\n",
                false,
                '/^(y|o)/i'
            );
            if (!$helper->ask($input, $output, $question)) {
                return Command::SUCCESS;
            }
        }

        file_put_contents(
            $path . DIRECTORY_SEPARATOR . $file->getName(),
            $file->getContent()
        );

        return Command::SUCCESS;
    }
}
