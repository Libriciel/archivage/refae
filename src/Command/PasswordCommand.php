<?php

namespace App\Command;

use App\Entity\UserPasswordRenewToken;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Override;
use RuntimeException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

#[AsCommand(
    name: 'app:password',
    description: 'Creation d\'un utilisateur',
)]
class PasswordCommand extends Command
{
    private QuestionHelper $question;
    private InputInterface $input;
    private OutputInterface $output;

    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly EntityManagerInterface $entityManager,
        private readonly RouterInterface $router,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function configure(): void
    {
        $this
            ->addArgument('username', InputArgument::OPTIONAL, 'username')
        ;
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;
        $this->question = $this->getHelper('question');

        $username = $this->askUsername();
        $user = $this->userRepository->findOneBy(['username' => $username]);
        if (!$user) {
            throw new Exception('User not found', 404);
        }

        $userPasswordRenewToken = new UserPasswordRenewToken();
        $userPasswordRenewToken->setUser($user);
        $this->entityManager->persist($userPasswordRenewToken);
        $this->entityManager->flush();

        $context = $this->router->getContext();
        $context->setScheme('https');
        if ($_ENV['HTTP_HOST'] ?? null) {
            $context->setScheme($_ENV['HTTP_PROTOCOL'] ?? 'https');
            $context->setHost($_ENV['HTTP_HOST']);
            if (isset($_ENV['HTTP_PORT'])) {
                if (($_ENV['HTTP_PROTOCOL'] ?? 'https') === 'https') {
                    $context->setHttpsPort((int)ltrim(((string) ($_ENV['HTTP_PORT'] ?: '443')), ':'));
                } else {
                    $context->setHttpPort((int)ltrim(((string) ($_ENV['HTTP_PORT'] ?: '80')), ':'));
                }
            }
            $context->setBaseUrl($_ENV['HTTP_BASEURL'] ?? '');
        }

        $url = $this->router->generate(
            'public_user_select_password',
            ['token' => $userPasswordRenewToken->getToken()],
            UrlGeneratorInterface::ABSOLUTE_URL,
        );
        $output->writeln($url);

        return Command::SUCCESS;
    }

    private function askUsername()
    {
        $question = new Question("-------------------\n", false);
        $question->setTrimmable(true);
        $question->setValidator(function (string $answer): string {
            if (!$this->userRepository->findOneBy(['username' => $answer])) {
                throw new RuntimeException('This user does not exists');
            }
            return $answer;
        });

        // pour valider si valeur fourni en arg
        if ($username = $this->input->getArgument('username')) {
            $question->getValidator()($username);
        } else {
            $this->output->writeln("Choisir parmis les utilisateurs:");
            foreach ($this->userRepository->findBy([], ['username' => 'ASC']) as $user) {
                $this->output->writeln($user->getUsername());
            }
        }
        while (is_null($username) || $username === '') {
            $username = $this->question->ask($this->input, $this->output, $question);
        }
        return $username;
    }
}
