<?php

namespace App\Command;

use App\Service\MeilisearchInterface;
use Meilisearch\Endpoints\Indexes;
use Override;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:meilisearch:purge',
    description: "Supprime tout dans meilisearch",
)]
class MeilisearchPurgeCommand extends Command
{
    public function __construct(
        private readonly MeilisearchInterface $meilisearch,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $indexes = $this->meilisearch->client->getIndexes();
        /** @var  $index Indexes */
        foreach ($indexes as $index) {
            $output->writeln("deleted: " . $index->getUid());
            $this->meilisearch->client->deleteIndex($index->getUid());
        }
        return Command::SUCCESS;
    }
}
