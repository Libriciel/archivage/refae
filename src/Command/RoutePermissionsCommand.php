<?php

namespace App\Command;

use App\Service\Permission;
use Override;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:route:permissions',
    description: "Donne la liste des permissions utilisés dans les controlleurs",
)]
class RoutePermissionsCommand extends Command
{
    public function __construct(
        private readonly Permission $permission,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $out = [];
        foreach ($this->permission->routePermissions() as $classname => $actions) {
            foreach ($actions as $action) {
                $out[] = "$classname/$action";
            }
        }
        sort($out);
        $output->writeln(implode(PHP_EOL, $out));
        return Command::SUCCESS;
    }
}
