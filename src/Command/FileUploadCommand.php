<?php

namespace App\Command;

use App\Repository\FileRepository;
use App\Service\Manager\FileManager;
use Doctrine\ORM\EntityManagerInterface;
use Override;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:file:upload',
    description: 'Envoi un fichier en base de donnée',
)]
class FileUploadCommand extends Command
{
    public function __construct(
        private readonly FileRepository $fileRepository,
        private readonly EntityManagerInterface $entityManager,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function configure(): void
    {
        $this
            ->addArgument('path', InputArgument::REQUIRED, 'Chemin du fichier à sauver')
            ->addArgument('id', InputArgument::OPTIONAL, 'Id du fichier à remplacer')
            ->addOption(
                'algo',
                '-a',
                InputOption::VALUE_OPTIONAL,
                "Choix de l'algorithme de hashage",
                'sha256' // TODO: valeur en conf ?
            )
        ;
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $path = $input->getArgument('path');

        if (!file_exists($path)) {
            $io->error('File not found');
            return Command::FAILURE;
        }

        if (!is_readable($path)) {
            $io->error('File not readable');
            return Command::FAILURE;
        }

        $file = null;
        if ($id = $input->getArgument('id')) {
            $file = $this->fileRepository->find($id);
            if (!$file) {
                $io->error(sprintf("File with id %s not found", $id));
                return Command::FAILURE;
            }
        }

        $algo = $input->getOption('algo');
        if (!in_array($algo, hash_algos())) {
            $io->error(sprintf("Algorithme %s non supporté", $algo));
            return Command::FAILURE;
        }

        $file = FileManager::setDataFromPath($path, $file, $algo);

        $this->entityManager->persist($file);
        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
