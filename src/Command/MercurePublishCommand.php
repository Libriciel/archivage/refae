<?php

namespace App\Command;

use Override;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;

#[AsCommand(
    name: 'app:mercure:publish',
    description: "Envoi un message via mercure",
)]
class MercurePublishCommand extends Command
{
    public function __construct(
        private readonly HubInterface $hub,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function configure(): void
    {
        $this
            ->addArgument(
                'topic',
                InputArgument::REQUIRED,
                "Nom du topic ex: ping",
            )
            ->addArgument(
                'data',
                InputArgument::REQUIRED,
                "Data à envoyer (au format json)",
            )
        ;
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $update = new Update(
            $input->getArgument('topic'),
            $input->getArgument('data'),
            true,
        );
        $output->writeln($this->hub->publish($update));
        return Command::SUCCESS;
    }
}
