<?php

namespace App\Command;

use App\Entity\PrivilegeUser;
use App\Entity\Tenant;
use App\Repository\PrivilegeRepository;
use App\Repository\PrivilegeUserRepository;
use App\Repository\TenantRepository;
use App\Repository\UserRepository;
use App\Service\Permission;
use Doctrine\ORM\EntityManagerInterface;
use Override;
use RuntimeException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:user:set-privilege',
    description: "Affectation d'un privilege à un utilisateur pour un tenant",
)]
class SetUserPrivilegeCommand extends Command
{
    public const string METHOD_GRANT = 'grant';
    public const string METHOD_DENY = 'deny';
    public const string METHOD_UNSET = 'unset';

    private QuestionHelper $question;
    private InputInterface $input;
    private OutputInterface $output;

    public function __construct(
        private readonly Permission $permission,
        private readonly UserRepository $userRepository,
        private readonly TenantRepository $tenantRepository,
        private readonly PrivilegeRepository $privilegeRepository,
        private readonly PrivilegeUserRepository $privilegeUserRepository,
        private readonly EntityManagerInterface $entityManager,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function configure(): void
    {
        $this
            ->addArgument(
                'method',
                InputArgument::REQUIRED,
                "grant: Ajoute un privilege à un utilisateur\n"
                    . "deny: Interdit un privilege à un utilisateur\n"
                    . "unset: Supprime une exception de droit\n",
                null,
                [self::METHOD_GRANT, self::METHOD_DENY, self::METHOD_UNSET]
            )
            ->addOption(
                'username',
                null,
                InputOption::VALUE_OPTIONAL,
                "username de l'utilisateur (headless)"
            )
            ->addOption(
                'tenant',
                null,
                InputOption::VALUE_OPTIONAL,
                "baseurl du tenant (headless)"
            )
            ->addOption(
                'privilege',
                null,
                InputOption::VALUE_OPTIONAL,
                "id du privilege (headless)"
            )
        ;
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;
        $io = new SymfonyStyle($input, $output);
        $this->question = $this->getHelper('question');

        $this->permission->privilege();
        $this->permission->permissionRole();

        $username = $this->askUsername();
        $user = $this->userRepository->findOneBy(['username' => $username]);

        $this->output->writeln("");
        $tenantBaseurl = $this->askTenant();
        $tenant = $this->tenantRepository->findOneBy(['baseurl' => $tenantBaseurl]);

        $this->output->writeln("");
        $privilegeId = $this->askPrivilege($tenant);
        $privilege = $this->privilegeRepository->findOneBy(['id' => $privilegeId]);
        $privilegeUser = $this->privilegeUserRepository->findOneBy(
            [
                'user' => $user,
                'tenant' => $tenant,
                'privilege' => $privilege,
            ]
        );
        if (!$privilegeUser) {
            $privilegeUser = new PrivilegeUser();
            $privilegeUser->setUser($user);
            $privilegeUser->setTenant($tenant);
            $privilegeUser->setPrivilege($privilege);
        }
        switch ($this->input->getArgument('method')) {
            case self::METHOD_GRANT:
                $privilegeUser->setIsGranted(true);
                $this->entityManager->persist($privilegeUser);
                $message = sprintf(
                    "Privilege (%s) ajouté sur l'utilisateur "
                    . "(%s) pour le tenant %s",
                    $privilege->getCommonName(),
                    $user->getUsername(),
                    $tenant->getBaseurl(),
                );
                break;
            case self::METHOD_DENY:
                $privilegeUser->setIsGranted(false);
                $this->entityManager->persist($privilegeUser);
                $message = sprintf(
                    "Privilege (%s) interdit sur l'utilisateur"
                    . " (%s) pour le tenant %s",
                    $privilege->getCommonName(),
                    $user->getUsername(),
                    $tenant->getBaseurl(),
                );
                break;
            case self::METHOD_UNSET:
            default:
                $this->entityManager->remove($privilegeUser);
                $message = sprintf(
                    "Suppression de l'exception de droit de Privilege (%s)"
                    . " sur l'utilisateur (%s) pour le tenant %s",
                    $privilege->getCommonName(),
                    $user->getUsername(),
                    $tenant->getBaseurl(),
                );
                break;
        }
        $this->entityManager->flush();
        $io->success($message);

        return Command::SUCCESS;
    }

    private function askUsername()
    {
        $question = new Question("-------------------\n", false);
        $question->setTrimmable(true);
        $question->setValidator(function (string $answer): string {
            if (!$this->userRepository->findOneBy(['username' => $answer])) {
                throw new RuntimeException('This user does not exists');
            }
            return $answer;
        });

        // pour valider si valeur fourni en arg
        if ($username = $this->input->getOption('username')) {
            $question->getValidator()($username);
        } else {
            $this->output->writeln("Choisir parmis les utilisateurs:");
            foreach ($this->userRepository->findBy([], ['username' => 'ASC']) as $user) {
                $this->output->writeln($user->getUsername());
            }
        }
        while (is_null($username) || $username === '') {
            $username = $this->question->ask($this->input, $this->output, $question);
        }
        return $username;
    }

    private function askTenant()
    {
        $question = new Question("-------------------\n", false);
        $question->setTrimmable(true);
        $question->setValidator(function (string $answer): string {
            if (!$this->tenantRepository->findOneBy(['baseurl' => $answer])) {
                throw new RuntimeException('This tenant does not exists');
            }
            return $answer;
        });

        // pour valider si valeur fourni en arg
        if ($tenantBaseurl = $this->input->getOption('tenant')) {
            $question->getValidator()($tenantBaseurl);
        } else {
            $this->output->writeln("Choisir parmis les tenants:");
            foreach ($this->tenantRepository->findBy([], ['baseurl' => 'ASC']) as $tenant) {
                $this->output->writeln($tenant->getBaseurl());
            }
        }
        while (is_null($tenantBaseurl) || $tenantBaseurl === '') {
            $tenantBaseurl = $this->question->ask($this->input, $this->output, $question);
        }
        return $tenantBaseurl;
    }

    private function askPrivilege(Tenant $tenant)
    {
        $question = new Question("-------------------\n", false);
        $question->setTrimmable(true);
        $question->setValidator(function (string $answer): string {
            if (!$this->privilegeRepository->findOneBy(['id' => $answer])) {
                throw new RuntimeException('This privilege does not exists');
            }
            return $answer;
        });

        // pour valider si valeur fourni en arg
        if ($privilegeName = $this->input->getOption('privilege')) {
            $question->getValidator()($privilegeName);
        } else {
            $this->output->writeln("Choisir parmis les privileges:");
            foreach ($this->privilegeRepository->findBy([], ['name' => 'ASC']) as $privilege) {
                if (
                    $privilege->getTenant() === null
                    || $privilege->getTenant()->getBaseurl() === $tenant->getBaseurl()
                ) {
                    $privilegeCommonName = $privilege->getCommonName();
                    $this->output->writeln(sprintf('%s : %s', $privilege->getId(), $privilegeCommonName));
                }
            }
        }
        while (is_null($privilegeName) || $privilegeName === '') {
            $privilegeName = $this->question->ask($this->input, $this->output, $question);
        }
        return $privilegeName;
    }
}
