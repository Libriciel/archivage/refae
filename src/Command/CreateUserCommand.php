<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\Manager\UserManager;
use Override;
use RuntimeException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[AsCommand(
    name: 'app:create:user',
    description: "Creation d'un utilisateur",
)]
class CreateUserCommand extends Command
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly ValidatorInterface $validator,
        private readonly UserManager $userManager,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function configure(): void
    {
        $this
            ->addArgument('username', InputArgument::OPTIONAL, 'username')
            ->addArgument('email', InputArgument::OPTIONAL, 'email')
            ->addArgument('password', InputArgument::OPTIONAL, 'password')
            ->addOption('admin', null, InputOption::VALUE_OPTIONAL, 'Donne le rôle d\'administrateur technique', false)
        ;
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');

        $question = new Question("Username?\n", false);
        $question->setTrimmable(true);
        $question->setValidator(function (string $answer): string {
            if ($this->userRepository->findOneBy(['username' => $answer])) {
                throw new RuntimeException('A user with the same username allready exists');
            }
            return $answer;
        });

        // pour valider si valeur fourni en arg
        if ($username = $input->getArgument('username')) {
            $question->getValidator()($username);
        }
        while (is_null($username) || $username === '') {
            $username = $helper->ask($input, $output, $question);
        }

        $question = new Question("E-mail?\n", false);
        $question->setTrimmable(true);
        $email = $input->getArgument('email');
        while (is_null($email) || $email === '') {
            $email = $helper->ask($input, $output, $question);
        }

        $password = $input->getArgument('password');
        $question = new Question("Password?\n", false);
        $question->setHidden(true);
        $question->setHiddenFallback(false);
        while (is_null($password) || $password === '') {
            $password = $helper->ask($input, $output, $question);
        }

        $user = new User();
        $user
            ->setUsername($username)
            ->setEmail($email)
            ->setNotifyUserRegistration(true)
            ->setUserDefinedPassword(true)
        ;

        $errors = $this->validator->validate($user);
        if (count($errors)) {
            $io->error((string)$errors);
            return Command::FAILURE;
        }

        $admin = $input->getOption('admin') !== false;
        $success = $this->userManager->add($user, $password, admin: $admin);
        if (!$success) {
            $io->error(sprintf("Erreur lors de la sauvegarde de l'utilisateur %s", $username));
            return Command::FAILURE;
        }

        $io->success(sprintf("User créé avec l'id : %s", $user->getId()));
        return Command::SUCCESS;
    }
}
