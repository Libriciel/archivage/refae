<?php

namespace App\Command;

use App\Entity\VersionableEntityInterface;
use App\Service\MeilisearchInterface;
use Override;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\VarDumper\VarDumper;

#[AsCommand(
    name: 'app:search',
    description: "Recherche dans meilisearch",
)]
class SearchCommand extends Command
{
    public function __construct(
        private readonly MeilisearchInterface $meilisearch,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function configure(): void
    {
        $this
            ->addArgument(
                'index',
                InputArgument::REQUIRED,
                "Nom de l'index (entity) ex: Authority",
            )
            ->addArgument(
                'string',
                InputArgument::REQUIRED,
                "Texte à rechercher",
            )
            ->addOption('public-only')
            ->addOption('last-version-only')
        ;
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $index = $this->meilisearch->getIndex($input->getArgument('index'));
        $searchParams = [];
        if ($input->getOption('public-only')) {
            $searchParams['filter'] = [
                'public = true',
                'state = ' . VersionableEntityInterface::S_PUBLISHED,
            ];
        }
        if ($input->getOption('last-version-only')) {
            $searchParams['filter'] = [
                'last_version = true',
            ];
        }
        $searchParams['sort'] = ['version:desc'];
        $searchParams['showMatchesPosition'] = true;
        VarDumper::dump($index->search($input->getArgument('string'), $searchParams));
        return Command::SUCCESS;
    }
}
