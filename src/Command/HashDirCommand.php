<?php

namespace App\Command;

use Exception;
use Override;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:hash-dir',
    description: "Donne une liste de fichiers avec leurs hashes
example: bin/console app:hash-dir -r src/ config/ --filter *.bak",
)]
class HashDirCommand extends Command
{
    private InputInterface $input;
    private OutputInterface $output;
    private string $out;
    private bool $noOutput = false;
    /**
     * @var false|resource
     */
    private $outputFile;

    public function __construct()
    {
        parent::__construct();
    }

    #[Override]
    protected function configure(): void
    {
        $this
            ->addArgument(
                name: 'target',
                mode: InputArgument::OPTIONAL | InputArgument::IS_ARRAY,
                description: "Fichiers/Dossiers à hasher",
            )
            ->addOption(
                name: 'basedir',
                mode: InputOption::VALUE_REQUIRED,
                description: "Dossier racine",
                default: getcwd(),
            )
            ->addOption(
                name: 'filter',
                shortcut: 'f',
                mode: InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY,
                description: "Filtres le nom de fichiers correspondant (cumulable)
                 exemple: bin/cake hash-dir path/to/my/dir --filter
                 *.foo --filter Bar*",
            )
            ->addOption(
                'hash-algo',
                null,
                InputOption::VALUE_OPTIONAL,
                "Algorithme de hashage utilisé",
                'sha256',
                hash_algos(),
            )
            ->addOption(
                name: 'output',
                shortcut: 'o',
                mode: InputOption::VALUE_OPTIONAL,
                description: "Fichier cible",
            )
            ->addOption(
                name: 'recursive',
                shortcut: 'r',
                mode: InputOption::VALUE_NONE,
                description: "Liste les fichiers de façon récursive",
            )
        ;
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;

        if ($input->getOption('output')) {
            $outputFile = $input->getOption('basedir') . '/' . $input->getOption('output');
            if (!is_writable(dirname($outputFile))) {
                throw new Exception(dirname($outputFile) . ' IS NOT WRITABLE');
            }
            $this->outputFile = fopen($outputFile, 'w');
        }
        $dirs = $input->getArgument('target');
        sort($dirs);
        $this->displayHeader($dirs);
        $trim = strlen($input->getOption('basedir') . '/');
        foreach ($dirs as $dir) {
            $d = $input->getOption('basedir') . '/' . trim((string) $dir, '/');
            if (is_file($d)) {
                $this->outputFile($d, $trim);
            } elseif (is_dir($d)) {
                $this->outputDir($d, $trim);
            } else {
                throw new Exception('404 DIR NOT FOUND: {' . $d . '}');
            }
        }
        if ($this->outputFile && isset($outputFile)) {
            fclose($this->outputFile);
            if (is_file($outputFile)) {
                $md5sum = hash_file('sha256', $outputFile);
                $sha256 = hex2bin($md5sum);
                file_put_contents(
                    $outputFile . '.sha256',
                    $sha256
                );
                $this->output->writeln($md5sum);
            }
        }
        return Command::SUCCESS;
    }

    /**
     * Entête du fichier
     * @param string[] $dirs
     */
    private function displayHeader(array $dirs)
    {
        $this->writeOutput('####################################');

        $str = $dirs;
        $params = [
            'recursive',
            'filter',
            'hash-algo',
        ];
        $options = [];
        foreach ($params as $key) {
            /** @var InputOption $values */
            $values = $this->input->getOption($key);
            $options[$key] = $values;
            if (is_array($values) && $values) {
                foreach ($values as $value) {
                    $str[] = '--' . $key . ' ' . addcslashes($value, ' *');
                }
            } elseif (is_string($values)) {
                $str[] = '--' . $key . ' ' . addcslashes($values, ' *');
            } elseif ($values === true) {
                $str[] = '--' . $key;
            }
        }

        $pass = [
            'args' => $this->input->getArgument('target'),
            'options' => $options,
        ];
        rsort($params);

        $this->writeOutput(implode(' ', $str));
        $this->writeOutput(base64_encode(serialize($pass)));
        $this->writeOutput('####################################', 2);
    }

    /**
     * Affichage conditionnel de la sortie
     */
    private function writeOutput(string $out, int $nl = 1)
    {
        $out = $out . str_repeat(PHP_EOL, $nl);
        if ($this->noOutput) {
            $this->out .= $out;
        } elseif ($this->input->getOption('output')) {
            fwrite($this->outputFile, $out);
        } else {
            $this->output->write($out);
        }
    }

    /**
     * Affiche le résultat, de façon récursive ou pas
     */
    private function outputDir(string $dir, int $ltrimAmount)
    {
        foreach (glob($dir . '/*') as $filename) {
            if (is_dir($filename)) {
                if ($this->input->getOption('recursive')) {
                    $this->outputDir($filename, $ltrimAmount);
                }
            } else {
                $this->outputFile($filename, $ltrimAmount);
            }
        }
    }

    /**
     * Affiche un fichier (fichier -> hash)
     */
    private function outputFile(string $filename, int $ltrimAmount)
    {
        $truncatedFilename = substr($filename, $ltrimAmount);
        $filter = false;
        foreach ($this->input->getOption('filter') ?: [] as $f) {
            if (fnmatch($f, $truncatedFilename)) {
                $filter = true;
                break;
            }
        }
        if (!$filter) {
            $this->writeOutput(
                $truncatedFilename
                . ':' . hash_file($this->input->getOption('hash-algo'), $filename)
            );
        }
    }
}
