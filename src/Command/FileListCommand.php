<?php

namespace App\Command;

use App\Repository\FileRepository;
use Override;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Serializer\SerializerInterface;

#[AsCommand(
    name: 'app:file:list',
    description: 'Liste les fichiers en base de donnée, les filtres supportent les wildvards % et _',
)]
class FileListCommand extends Command
{
    public function __construct(
        private readonly FileRepository $fileRepository,
        private readonly SerializerInterface $serializer,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function configure(): void
    {
        $this
            ->addOption('name', null, InputOption::VALUE_OPTIONAL, 'Filtre par name')
            ->addOption('mime', null, InputOption::VALUE_OPTIONAL, 'Filtre par mime')
            ->addOption('hash', null, InputOption::VALUE_OPTIONAL, 'Filtre par hash')
            ->addOption('algo', null, InputOption::VALUE_OPTIONAL, 'Filtre par hash_algo')
        ;
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $query = $this->fileRepository->createQueryBuilder('f');
        if ($name = $input->getOption('name')) {
            $query->andWhere('f.name LIKE :name')
                ->setParameter('name', $name);
        }
        if ($mime = $input->getOption('mime')) {
            $query->andWhere('f.mime LIKE :mime')
                ->setParameter('mime', $mime);
        }
        if ($hash = $input->getOption('hash')) {
            $query->andWhere('f.hash LIKE :hash')
                ->setParameter('hash', $hash);
        }
        if ($hash_algo = $input->getOption('algo')) {
            $query->andWhere('f.hash_algo LIKE :hash_algo')
                ->setParameter('hash_algo', $hash_algo);
        }

        $results = $query
            ->orderBy('f.id', 'ASC')
            ->getQuery()
            ->getResult();

        $display = [];
        foreach ($results as $file) {
            foreach ($this->serializer->normalize($file, context: ['groups' => ['index']]) as $attr => $val) {
                $display[] = [$attr => $val];
            }
            $display[] = new TableSeparator();
        }

        $display[] = sprintf("Nombre de fichiers trouvés : %d", count($results));
        $io->definitionList(...$display);

        return Command::SUCCESS;
    }
}
