<?php

namespace App\Command;

use App\Entity\OneTenantIntegrityEntityInterface;
use App\Entity\OneTenantOrNoneIntegrityEntityInterface;
use App\Entity\Tenant;
use App\Entity\TenantsIntegrityEntityInterface;
use App\Serializer\UuidNormalizer;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Doctrine\ORM\Mapping as ORM;
use Override;
use ReflectionClass;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AttributeLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\String\UnicodeString;

#[AsCommand(
    name: 'app:database:table',
    description: "Affiche le contenu d'une table",
    aliases: ['app:db:table', 'db:table'],
)]
class DatabaseTableCommand extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function configure(): void
    {
        $this
            ->addArgument('entity', InputArgument::REQUIRED, "Nom de l'entité")
            ->addArgument('fields', InputArgument::IS_ARRAY, "Champs")
            ->addOption('group', 'g', InputOption::VALUE_OPTIONAL, "Groupe de serialization")
            ->addOption(
                'where',
                null,
                InputOption::VALUE_OPTIONAL,
                "Injection SQL dans le where",
                "1 = 1"
            )
        ;
    }

    #[Override]
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $classname = 'App\\Entity\\' . $input->getArgument('entity');
        if (!class_exists($classname)) {
            throw new Exception("classname $classname does not exists");
        }
        $reflectionClass = new ReflectionClass($classname);
        $typeFields = [
            ORM\Column::class,
            ORM\ManyToOne::class,
            ORM\ManyToMany::class,
            ORM\OneToMany::class,
        ];
        $fields = [];
        foreach ($reflectionClass->getProperties() as $property) {
            $field = ['groups' => []];
            foreach ($property->getAttributes() as $attribute) {
                if (in_array($attribute->getName(), $typeFields)) {
                    $field['type'] = $attribute->getName();
                } elseif ($attribute->getName() === Groups::class) {
                    $field['groups'] = $attribute->getArguments()[0];
                } elseif ($attribute->getName() === SerializedName::class) {
                    $field['serializedName'] = $attribute->getArguments()[0];
                }
            }
            if (
                isset($field['type'])
                && (empty($input->getArgument('fields'))
                    || in_array($property->getName(), $input->getArgument('fields')))
                && (empty($input->getOption('group'))
                    || in_array($input->getOption('group'), $field['groups']))
            ) {
                $fields[$property->getName()] = $field;
            }
        }
        $classMetadataFactory = new ClassMetadataFactory(new AttributeLoader());
        $normalizers = [
            new UuidNormalizer(),
            new ObjectNormalizer($classMetadataFactory),
        ];
        $serializer = new Serializer($normalizers);

        $headers = array_keys($fields);
        if (
            !isset($fields['tenants'])
            && $reflectionClass->implementsInterface(TenantsIntegrityEntityInterface::class)
        ) {
            $headers[] = 'tenants';
        }
        if (
            !isset($fields['tenant']) && (
                $reflectionClass->implementsInterface(OneTenantIntegrityEntityInterface::class)
                || $reflectionClass->implementsInterface(OneTenantOrNoneIntegrityEntityInterface::class)
            )
        ) {
            $headers[] = 'tenant';
        }

        $table = new Table($output);
        $table->setHeaders($headers);
        $repository = $this->entityManager->getRepository($classname);
        $count = $repository->createQueryBuilder('a')
            ->select('COUNT(a.id)')
            ->andWhere($input->getOption('where'))
            ->getQuery()
            ->getSingleScalarResult()
        ;
        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');

        for ($i = 0; $i < $count; $i += 10) {
            $rawData = $repository->createQueryBuilder('a')
                ->andWhere($input->getOption('where'))
                ->addOrderBy('a.id')
                ->setFirstResult($i)
                ->setMaxResults(10)
                ->getQuery()
                ->getResult()
            ;
            $rows = [];
            foreach ($rawData as $entity) {
                $row = [];
                $normalized = $serializer->normalize($entity, context: ['groups' => ['index', 'meilisearch']]);
                foreach ($normalized as $field => $value) {
                    if (is_array($value)) {
                        $normalized[$field] = json_encode($value, JSON_UNESCAPED_UNICODE);
                    } else {
                        $normalized[$field] = (string)$value;
                    }
                }
                foreach ($fields as $field => $params) {
                    $row[$field] = $this->extractData($field, $params, $entity, $normalized) ?: '␀';
                }
                if (empty($row['tenants']) && $entity instanceof TenantsIntegrityEntityInterface) {
                    $row['tenants'] = implode(
                        "\n",
                        array_map(fn(Tenant $t) => $t->getBaseurl(), $entity->getTenants()->toArray())
                    );
                } elseif (
                    empty($row['tenant']) && (
                        $entity instanceof OneTenantIntegrityEntityInterface
                        || $entity instanceof OneTenantOrNoneIntegrityEntityInterface
                    )
                ) {
                    $row['tenant'] = $entity->getTenant()?->getBaseurl();
                }
                $rows[] = array_values($row);
            }
            $table->setRows($rows);
            $table->render();
            $table = new Table($output);
            $table->setHeaders(array_keys($fields));

            if ($i + 10 < $count) {
                $question = new Question("Afficher la suite ?\n", 'y');
                if ($helper->ask($input, $output, $question) === 'n') {
                    break;
                }
            }
        }

        return Command::SUCCESS;
    }

    private function extractData(string $field, $params, $entity, $normalized): mixed
    {
        $result = '';
        if ($field === 'id') {
            return $entity->getId();
        }
        if ($params['type'] === ORM\Column::class) {
            $result = $normalized[$field] ?? '␀';
        } elseif ($params['type'] === ORM\ManyToOne::class) {
            $camel = ucfirst((new UnicodeString($field))->camel());
            $getter = 'get' . $camel;
            $subentity = null;
            if ($entity && method_exists($entity, $getter)) {
                $subentity = $entity->$getter();
            }
            if ($subentity && method_exists($subentity, 'getUsername')) {
                $result = $subentity->getUsername();
            } elseif ($subentity && method_exists($subentity, 'getName')) {
                $result = $subentity->getName();
            } elseif ($subentity && method_exists($subentity, 'getId')) {
                $result = $subentity->getId();
            } else {
                $result = $normalized[$field] ?? '␀';
            }
        } elseif (in_array($params['type'], [ORM\ManyToMany::class, ORM\OneToMany::class])) {
            $camel = ucfirst((new UnicodeString($field))->camel());
            $getter = 'get' . $camel;
            $collection = null;
            if ($entity && method_exists($entity, $getter)) {
                $collection = $entity->$getter();
            }
            $sub = [];
            foreach ((array)$collection as $subentity) {
                if (is_object($subentity) && method_exists($subentity, 'getName')) {
                    $sub[] = $subentity->getName();
                } elseif (is_array($subentity) && array_key_exists('name', $subentity)) {
                    $sub[] = $subentity['name'];
                } elseif (is_object($subentity) && method_exists($subentity, 'getId')) {
                    $sub[] = $subentity->getId();
                } elseif (is_array($subentity) && array_key_exists('id', $subentity)) {
                    $sub[] = $subentity['id'];
                } else {
                    $sub[] = $normalized[$field] ?? '␀';
                }
            }
            if ($sub) {
                $result = implode("\n", $sub);
            }
        } else {
            $result = 'not-implemented-yet';
        }
        return $result;
    }
}
