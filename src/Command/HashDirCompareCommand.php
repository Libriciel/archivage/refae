<?php

namespace App\Command;

use Exception;
use Override;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\StreamOutput;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:hash-dir:compare',
    description: "Compare l'état actuel par rapport à un fichier",
)]
class HashDirCompareCommand extends Command
{
    private OutputInterface $output;

    public function __construct(
        private readonly HashDirCommand $hashDirCommand,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function configure(): void
    {
        $this
            ->addArgument(
                name: 'filename',
                mode: InputArgument::OPTIONAL,
                description: "Fichier cible",
                default: dirname(__DIR__, 2) . '/resources/hashes.txt'
            )
            ->addOption(
                name: 'basedir',
                mode: InputOption::VALUE_OPTIONAL,
                description: "Dossier racine",
                default: getcwd(),
            )
        ;
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->output = $output;
        $io = new SymfonyStyle($input, $output);
        $filename = realpath($input->getArgument('filename'));

        if (!is_file($filename)) {
            throw new Exception('file not found: ' . $filename);
        }

        // parse le fichier
        $file = file($filename, FILE_IGNORE_NEW_LINES);

        // appel de hash_dir pour obtenir la liste des fichiers actuel avec leurs hashs
        $out = $this->getHashsList($file);

        $missing = [];
        $diff = [];
        $added = [];
        $c = count($file);
        for ($i = 5; $i < $c; $i++) {
            if (!strpos($file[$i], ':') || str_contains($out, $file[$i])) {
                continue;
            }
            [$filename, $hash] = explode(':', $file[$i]);
            $pos = strpos($out, $filename . ':');
            if (!$pos) {
                $missing[] = $filename;
            } else {
                $nextNL = strpos($out, PHP_EOL, $pos + 1);
                $diff[$filename] = [
                    'expected' => $hash,
                    'actual' => substr(
                        substr(
                            $out,
                            $pos,
                            $nextNL - $pos
                        ),
                        strlen($filename) + 1
                    )
                ];
            }
        }
        $new = implode(PHP_EOL, $file);
        foreach (explode(PHP_EOL, $out) as $out) {
            if (!strpos($out, ':') || str_contains($new, $out)) {
                continue;
            }
            [$filename] = explode(':', $out);
            if (!strpos($new, $filename . ':')) {
                $added[] = $filename;
            }
        }

        if (empty($missing) && empty($diff) && empty($added)) {
            $io->success('OK');
        } else {
            if (!empty($missing)) {
                $io->warning("Les fichiers suivants sont manquants:");
                foreach ($missing as $m) {
                    $this->output->writeln('    ' . $m);
                }
                $this->hr();
            }
            if (!empty($diff)) {
                $io->warning("Les fichiers suivants sont différents (<hash_attendu>:<hash_actuel>):");
                foreach ($diff as $filename => $hashes) {
                    $this->output->writeln(
                        '    ' . $filename . ' (' . $hashes['expected'] . ':' . $hashes['actual'] . ')'
                    );
                }
                $this->hr();
            }
            if (!empty($added)) {
                $io->warning("Les fichiers suivants sont en trop:");
                foreach ($added as $m) {
                    $this->output->writeln('    ' . $m);
                }
                $this->hr();
            }
            throw new Exception("Une différence a été détectée");
        }
        return Command::SUCCESS;
    }

    /**
     * Inscrit dans $this->noOutput la liste des fichiers et leurs hashes
     * @return void
     * @throws Exception
     */
    private function getHashsList(array $file): string
    {
        // extraction des paramètres de la commande hash_dir
        $pass = unserialize(base64_decode(trim($file[2] ?? '')));
        if (!$pass) {
            throw new Exception('unable to read params');
        }
        $args = array_merge(['shift_me'], $pass['args']);
        foreach ($pass['options'] as $option => $value) {
            if ($value) {
                $args[] = '--' . $option;
                if ($value !== true) {
                    $args[] = $value;
                }
            }
        }
        $subCommandInput = new ArgvInput($args, $this->hashDirCommand->getDefinition());
        $subCommandOutput = new StreamOutput($subCommandFHandle = tmpfile());
        $this->hashDirCommand->execute($subCommandInput, $subCommandOutput);

        rewind($subCommandFHandle);
        $content = stream_get_contents($subCommandFHandle);
        fclose($subCommandFHandle);
        return $content;
    }

    private function hr(int $newlines = 0, int $width = 79): void
    {
        $this->output->writeln(str_repeat(PHP_EOL, $newlines));
        $this->output->writeln(str_repeat('-', $width));
        $this->output->writeln(str_repeat(PHP_EOL, $newlines));
    }
}
