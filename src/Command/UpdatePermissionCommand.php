<?php

namespace App\Command;

use App\Service\Permission;
use Override;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:update-permission',
    description: 'Add a short description for your command',
)]
class UpdatePermissionCommand extends Command
{
    public function __construct(
        private readonly Permission $permission,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->permission->privilege();
        $this->permission->permissionRole();

        $io->success('done.');

        return Command::SUCCESS;
    }
}
