<?php

namespace App\Command;

use App\Entity\Tenant;
use App\Repository\TenantRepository;
use Doctrine\ORM\EntityManagerInterface;
use Override;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:init:first-tenant',
    description: "Ajoute le 1er tenant",
)]
class InitFirstTenantCommand extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly TenantRepository $tenantRepository,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function configure(): void
    {
        $this
            ->addArgument('name', InputArgument::REQUIRED, 'name')
            ->addArgument('baseurl', InputArgument::REQUIRED, 'baseurl')
        ;
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $name = $input->getArgument('name');
        $baseurl = $input->getArgument('baseurl');

        if (!$name || !$baseurl) {
            $output->writeln("Argument obligatoire manquant");
        }
        if ($this->tenantRepository->findBy([])) {
            $output->writeln("Tenant already exists");
            return Command::FAILURE;
        }

        $tenant = new Tenant();
        $tenant->setName($name);
        $tenant->setBaseurl($baseurl);
        $tenant->setActive(true);

        $this->entityManager->persist($tenant);
        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
