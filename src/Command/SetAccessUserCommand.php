<?php

namespace App\Command;

use App\Entity\AccessUser;
use App\Entity\Tenant;
use App\Repository\AccessUserRepository;
use App\Repository\RoleRepository;
use App\Repository\TenantRepository;
use App\Repository\UserRepository;
use App\Service\Permission;
use Doctrine\ORM\EntityManagerInterface;
use Override;
use RuntimeException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:user:set-role',
    description: "Affectation d'un role à un utilisateur pour un tenant",
)]
class SetAccessUserCommand extends Command
{
    private QuestionHelper $question;
    private InputInterface $input;
    private OutputInterface $output;

    public function __construct(
        private readonly Permission $permission,
        private readonly UserRepository $userRepository,
        private readonly TenantRepository $tenantRepository,
        private readonly RoleRepository $roleRepository,
        private readonly AccessUserRepository $accessUserRepository,
        private readonly EntityManagerInterface $entityManager,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function configure(): void
    {
        $this
            ->addArgument(
                'method',
                InputArgument::OPTIONAL,
                "add: Ajoute un lien\ndelete: Supprime un lien\noverride: remplace un lien\n",
                'add',
                ['add', 'delete', 'override']
            )
            ->addOption(
                'username',
                null,
                InputOption::VALUE_OPTIONAL,
                "username de l'utilisateur (headless)"
            )
            ->addOption(
                'tenant',
                null,
                InputOption::VALUE_OPTIONAL,
                "baseurl du tenant (headless)"
            )
            ->addOption(
                'role',
                null,
                InputOption::VALUE_OPTIONAL,
                "id du role (headless)"
            )
        ;
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;
        $io = new SymfonyStyle($input, $output);
        $this->question = $this->getHelper('question');

        $this->permission->privilege();
        $this->permission->permissionRole();

        $username = $this->askUsername();
        $user = $this->userRepository->findOneBy(['username' => $username]);

        $this->output->writeln("");
        $tenantBaseurl = $this->askTenant();
        $tenant = $tenantBaseurl !== 'null'
            ? $this->tenantRepository->findOneBy(['baseurl' => $tenantBaseurl])
            : null;

        $accessUser = $this->accessUserRepository->findOneBy(
            [
                'user' => $user,
                'tenant' => $tenant,
            ]
        );
        $method = $this->input->getArgument('method');
        $delete = $method === 'delete';
        $override = $method === 'override';
        if ($accessUser && !$delete && !$override) {
            $roleCommonName = $accessUser->getRole()->getCommonName();
            $io->error(
                sprintf(
                    "Cet utilisateur (%s) est déjà lié a ce tenant (%s) avec le role %s",
                    $user->getUsername(),
                    $tenant?->getName() ?? '<null>',
                    $roleCommonName,
                )
            );
            return Command::FAILURE;
        } elseif ($delete) {
            if ($accessUser) {
                $user->removeAccessUser($accessUser);
                $this->entityManager->persist($user);
                $this->entityManager->flush();
                $io->success(
                    sprintf(
                        "L'utilisateur %s n'est désormais plus lié au tenant %s",
                        $user->getUsername(),
                        $tenant?->getName() ?? '<null>',
                    )
                );
                return Command::SUCCESS;
            } else {
                $io->error(
                    sprintf(
                        "L'utilisateur %s n'est pas lié au tenant %s",
                        $user->getUsername(),
                        $tenant?->getName() ?? '<null>',
                    )
                );
                return Command::FAILURE;
            }
        }

        $this->output->writeln("");
        $roleId = $this->askRole($tenant);
        $role = $this->roleRepository->findOneBy(['id' => $roleId]);

        $accessUser = $this->accessUserRepository->findOneBy(
            [
                'user' => $user,
                'tenant' => $tenant,
            ]
        );
        if ($accessUser && $override) {
            $user->removeAccessUser($accessUser);
            $accessUser = null;
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }
        if (!$accessUser) {
            $accessUser = new AccessUser();
            $accessUser->setUser($user);
            $accessUser->setRole($role);
            $accessUser->setTenant($tenant);
            if ($tenant) {
                $user->addTenant($tenant);
                $tenant->addUser($user);
                $this->entityManager->persist($tenant);
            }
            $this->entityManager->persist($accessUser);
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $roleCommonName = $accessUser->getRole()->getCommonName();
            $io->success(
                sprintf(
                    "L'utilisateur %s est désormais lié au tenant %s avec le role %s",
                    $user->getUsername(),
                    $tenant?->getName() ?? '<null>',
                    $roleCommonName,
                )
            );
        }

        return Command::SUCCESS;
    }

    private function askUsername()
    {
        $question = new Question("-------------------\n", false);
        $question->setTrimmable(true);
        $question->setValidator(function (string $answer): string {
            if (!$this->userRepository->findOneBy(['username' => $answer])) {
                throw new RuntimeException('This user does not exists');
            }
            return $answer;
        });

        // pour valider si valeur fourni en arg
        if ($username = $this->input->getOption('username')) {
            $question->getValidator()($username);
        } else {
            $this->output->writeln("Choisir parmis les utilisateurs:");
            foreach ($this->userRepository->findBy([], ['username' => 'ASC']) as $user) {
                $this->output->writeln($user->getUsername());
            }
        }
        while (is_null($username) || $username === '') {
            $username = $this->question->ask($this->input, $this->output, $question);
        }
        return $username;
    }

    private function askTenant()
    {
        $question = new Question("-------------------\n", false);
        $question->setTrimmable(true);
        $question->setValidator(function (string $answer): string {
            if ($answer !== 'null' && !$this->tenantRepository->findOneBy(['baseurl' => $answer])) {
                throw new RuntimeException('This tenant does not exists');
            }
            return $answer;
        });

        // pour valider si valeur fourni en arg
        if ($tenantBaseurl = $this->input->getOption('tenant')) {
            $question->getValidator()($tenantBaseurl);
        } else {
            $this->output->writeln("Choisir parmis les tenants:");
            foreach ($this->tenantRepository->findBy([], ['baseurl' => 'ASC']) as $tenant) {
                $this->output->writeln($tenant->getBaseurl());
            }
            $this->output->writeln('null');
        }
        while (is_null($tenantBaseurl) || $tenantBaseurl === '') {
            $tenantBaseurl = $this->question->ask($this->input, $this->output, $question);
        }
        return $tenantBaseurl;
    }

    private function askRole(?Tenant $tenant)
    {
        $question = new Question("-------------------\n", false);
        $question->setTrimmable(true);
        $question->setValidator(function (string $answer): string {
            if (!$this->roleRepository->findOneBy(['id' => $answer])) {
                throw new RuntimeException('This role does not exists');
            }
            return $answer;
        });

        // pour valider si valeur fourni en arg
        if ($roleName = $this->input->getOption('role')) {
            $question->getValidator()($roleName);
        } else {
            $this->output->writeln("Choisir parmis les roles:");
            foreach ($this->roleRepository->findBy([], ['name' => 'ASC']) as $role) {
                if (
                    $role->getTenant() === null
                    || $role->getTenant()->getBaseurl() === $tenant->getBaseurl()
                ) {
                    $roleCommonName = $role->getCommonName();
                    $this->output->writeln(sprintf('%s : %s', $role->getId(), $roleCommonName));
                }
            }
        }
        while (is_null($roleName) || $roleName === '') {
            $roleName = $this->question->ask($this->input, $this->output, $question);
        }
        return $roleName;
    }
}
