<?php

namespace App\Security;

use App\Repository\ApiTokenRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Override;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CustomCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;

class ApiBearerAuthenticator extends AbstractAuthenticator
{
    public function __construct(
        private readonly ApiTokenRepository $apiTokenRepository,
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    #[Override]
    public function supports(Request $request): ?bool
    {
        return $request->attributes->get('_firewall_context')
            === 'security.firewall.map.context.api'
            && $request->headers->get('authorization')
            && $request->attributes->get('_route') !== 'api_token_create';
    }

    #[Override]
    public function authenticate(
        Request $request,
    ): Passport {
        [$type, $token] = explode(' ', $request->headers->get('authorization') . ' ');
        $username = $user = '';
        if (strtolower($type) === 'bearer' && $token) {
            $this->apiTokenRepository->deleteExpired();
            $apiToken = $this->apiTokenRepository->findBytoken($token);
            if ($apiToken) {
                $user = $apiToken->getUser();
                $user->setActiveTenant($apiToken->getTenant());
                $username = $user->getUsername();
                $apiToken->setLastAccess(new DateTime());
                $this->entityManager->persist($apiToken);
                $this->entityManager->flush();
            }
        }
        return new Passport(
            new UserBadge($username, fn() => $user),
            new CustomCredentials(fn() => (bool)$user, [])
        );
    }

    #[Override]
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    #[Override]
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return new Response(
            "The provided token is incorrect or expired.",
            Response::HTTP_UNAUTHORIZED
        );
    }
}
