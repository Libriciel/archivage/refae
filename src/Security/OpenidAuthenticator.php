<?php

namespace App\Security;

use App\Entity\AccessUser;
use App\Entity\User;
use App\Repository\OpenidRepository;
use App\Repository\UserRepository;
use Override;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CustomCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\SecurityRequestAttributes;

class OpenidAuthenticator extends AbstractAuthenticator
{
    private ?User $user = null;

    public function __construct(
        private readonly OpenidRepository $openidRepository,
        private readonly UserRepository $userRepository,
        private readonly RouterInterface $router,
    ) {
    }

    #[Override]
    public function supports(Request $request): ?bool
    {
        return in_array(
            $request->attributes->get('_route'),
            ['app_tenant_openid_redirect', 'public_openid_redirect']
        );
    }

    #[Override]
    public function authenticate(
        Request $request,
    ): Passport {
        $openid = $this->openidRepository->findOneBy(['id' => $request->get('openid')]);
        $oidc = $openid->getOpenidClient();
        $tenant_url = $request->get('tenant_url');

        $session = $request->getSession();
        if (!$session->isStarted()) {
            $session->start();
        }
        $tenantSessionId = sprintf('tenant.%s', $tenant_url ?: 'public');
        $tenantData = $session->get($tenantSessionId);
        if ($tenantData) {
            $oidc->setAccessToken($tenantData['access_token']);
        }
        $oidc->authenticate();
        $username = $oidc->requestUserInfo($openid->getUsernameField());
        if ($tenant_url) {
            $this->user = $this->userRepository->findOneByUsernameTenant($username, $tenant_url);
        } else {
            $this->user = $this->userRepository->findOneByUsername($username);
            $access = $this->user?->getAdminTechAccessUser();
            if ($access?->getOpenid() !== $openid) {
                $this->user = null;
            }
        }

        $session->set(
            $tenantSessionId,
            [
                'baseurl' => $tenant_url ?: 'public',
                'username' => $username,
                'connection' => AccessUser::LOGIN_TYPE_OPENID,
                'openid' => $openid->getId(),
                'access_token' => $oidc->getAccessToken(),
                'id_token' => $oidc->getIdToken(),
            ]
        );
        $session->save();
        return new Passport(
            new UserBadge($username, $this->selectUser(...)),
            new CustomCredentials(fn() => true, null)
        );
    }

    public function selectUser(): User
    {
        if (!$this->user) {
            throw new UserNotFoundException();
        }
        return $this->user;
    }

    #[Override]
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        $tenant_url = $request->get('tenant_url');
        if (!$tenant_url) {
            return new RedirectResponse(
                $this->router->generate('admin_home')
            );
        }
        return new RedirectResponse(
            $this->router->generate('app_home')
        );
    }

    #[Override]
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $request->getSession()->set(SecurityRequestAttributes::AUTHENTICATION_ERROR, $exception);
        return new RedirectResponse(
            $this->router->generate('app_tenant_login')
        );
    }
}
