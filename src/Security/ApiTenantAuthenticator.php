<?php

namespace App\Security;

use App\Entity\ApiToken;
use Override;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\RateLimiter\Exception\RateLimitExceededException;
use Symfony\Component\RateLimiter\LimiterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CustomCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;

class ApiTenantAuthenticator extends TenantAuthenticator
{
    private LimiterInterface $limiter;

    #[Override]
    public function supports(Request $request): ?bool
    {
        $authorizationToLower = strtolower($request->headers->get('Authorization'));
        $get = $request->isMethod('get') && str_contains($authorizationToLower, 'basic');
        return ($get || $request->isMethod('post'))
            && $request->attributes->get('_route') === 'api_token_create';
    }

    #[Override]
    public function authenticate(
        Request $request,
    ): Passport {
        $username = $request->get('username');
        $password = $request->get('password');
        $tenant_url = $this->tenant_url ?: $request->get('tenant_url');

        if (!$username && $request->headers->get('Authorization')) {
            [$type, $b64] = explode(' ', $request->headers->get('Authorization'));
            if (strtolower($type) !== 'basic') {
                throw new AuthenticationException('only basic work here', 403);
            }
            [$username, $password] = explode(':', base64_decode($b64));
        }

        $this->limiter = $this->loginLimiter->create($username);
        $limit = $this->limiter->consume();
        if (!$limit->isAccepted()) {
            $e = new RateLimitExceededException($limit);
            throw new AuthenticationException($e->getMessage(), 429, $e);
        }

        $this->user = $this->user ?: $this->userRepository->findOneByUsernameTenant($username, $tenant_url);
        if (!$this->link) {
            $this->link = $this->user
                ? $this->accessUserRepository->findOneByUsernameTenant($username, $tenant_url)
                : null;
        }
        if ($this->link?->getLdap()) {
            $credentials = new CustomCredentials(
                $this->checkLdapUserCredentials(...),
                [$this->link, $password]
            );
        } else {
            $credentials = new PasswordCredentials($password);
        }

        return new Passport(
            new UserBadge($username, $this->selectUser(...)),
            $credentials
        );
    }

    #[Override]
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        $this->limiter->reset();
        $apiToken = new ApiToken();
        $apiToken->setTenant($this->user->getTenant($request));
        $apiToken->setUser($this->user);

        $this->entityManager->persist($apiToken);
        $this->entityManager->flush();

        return new Response(
            $apiToken->getToken(),
            Response::HTTP_CREATED,
            ['Authorization' => sprintf('Bearer %s', $apiToken->getToken())],
        );
    }

    #[Override]
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $prevException = $exception->getPrevious();
        if ($prevException instanceof RateLimitExceededException) {
            $limit = $prevException->getRateLimit();
            return new Response(
                "Vous avez dépassé le nombre maximal ({$limit->getLimit()})" .
                " de tentatives de connexion autorisées en 15 minutes",
                $exception->getCode()
            );
        }
        return new Response(
            "The provided username or password is incorrect.",
            Response::HTTP_UNAUTHORIZED
        );
    }
}
