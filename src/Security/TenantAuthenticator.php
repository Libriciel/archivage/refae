<?php

namespace App\Security;

use App\Entity\AccessUser;
use App\Entity\User;
use App\Repository\AccessUserRepository;
use App\Repository\TenantRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Override;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\RateLimiter\Exception\RateLimitExceededException;
use Symfony\Component\RateLimiter\LimiterInterface;
use Symfony\Component\RateLimiter\RateLimiterFactory;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CustomCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\SecurityRequestAttributes;

class TenantAuthenticator extends AbstractAuthenticator
{
    public ?User $user = null;
    public ?AccessUser $link = null;
    public ?string $username = null;
    public ?string $tenant_url = null;
    private LimiterInterface $limiter;

    public function __construct(
        protected readonly UserRepository $userRepository,
        protected readonly AccessUserRepository $accessUserRepository,
        protected readonly EntityManagerInterface $entityManager,
        protected readonly RouterInterface $router,
        protected readonly RateLimiterFactory $loginLimiter,
        protected readonly TenantRepository $tenantRepository,
    ) {
    }

    #[Override]
    public function supports(Request $request): ?bool
    {
        return $request->isMethod('post')
            && $request->attributes->get('_route') === 'app_tenant_login_username';
    }

    #[Override]
    public function authenticate(
        Request $request,
    ): Passport {
        $username = $this->username ?: $request->get('username');
        $password = $request->get('login_password')['password'];
        $tenant_url = $this->tenant_url ?: $request->get('tenant_url');

        $this->limiter = $this->loginLimiter->create($username);
        $limit = $this->limiter->consume();
        if (!$limit->isAccepted()) {
            $e = new RateLimitExceededException($limit);
            throw new AuthenticationException($e->getMessage(), 429, $e);
        }

        $tenant = $this->tenantRepository->findOneByBaseurl($tenant_url);
        if (!$tenant->isActive()) {
            throw new AuthenticationException('Closed tenant');
        }

        $this->user = $this->user ?: $this->userRepository->findOneByUsernameTenant($username, $tenant_url);
        if (!$this->link) {
            $this->link = $this->user
                ? $this->accessUserRepository->findOneByUsernameTenant($username, $tenant_url)
                : null;
        }
        if ($this->link?->getLdap()) {
            $credentials = new CustomCredentials(
                $this->checkLdapUserCredentials(...),
                [$this->link, $password]
            );
        } else {
            $credentials = new PasswordCredentials($password);
        }

        return new Passport(
            new UserBadge($username, $this->selectUser(...)),
            $credentials
        );
    }

    public function selectUser(): User
    {
        if (!$this->user) {
            return new User();
        }
        return $this->user;
    }

    public function checkLdapUserCredentials(array $credentials)
    {
        /** @var AccessUser $link */
        [$link, $password] = $credentials;
        $ldap = $link->getLdap();
        $ldapConnection = $ldap?->log($link->getLdapUsername(), $password);
        if ($ldapConnection) {
            return true;
        }
        return false;
    }

    #[Override]
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        if (isset($this->limiter)) {
            $this->limiter->reset();
        }
        $username = $this->username ?: $request->get('username');
        $tenant_url = $request->get('tenant_url');
        $tenantSessionId = sprintf('tenant.%s', $tenant_url);
        $session = $request->getSession();

        if ($this->link?->getLdap()) {
            $session->set(
                $tenantSessionId,
                [
                    'baseurl' => $tenant_url,
                    'username' => $username,
                    'connection' => AccessUser::LOGIN_TYPE_LDAP,
                    'ldap' => $this->link->getLdap()->getId(),
                ]
            );
        } else {
            $session->set(
                $tenantSessionId,
                [
                    'baseurl' => $tenant_url,
                    'username' => $username,
                    'connection' => AccessUser::LOGIN_TYPE_REFAE,
                ]
            );
        }
        return new RedirectResponse(
            $this->router->generate('app_home', ['tenant_url' => $this->tenant_url])
        );
    }

    #[Override]
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $prevException = $exception->getPrevious();
        if ($prevException instanceof RateLimitExceededException) {
            $limit = $prevException->getRateLimit();
            $exception = new BadCredentialsException(
                "Vous avez dépassé le nombre maximal ({$limit->getLimit()})" .
                " de tentatives de connexion autorisées en 15 minutes",
                $exception->getCode(),
                $exception
            );
        } else {
            $exception = new BadCredentialsException(
                "Identifiant de connexion ou mot de passe invalide",
                $exception->getCode(),
                $exception
            );
        }
        $request->getSession()->set(SecurityRequestAttributes::AUTHENTICATION_ERROR, $exception);
        return new RedirectResponse(
            $this->router->generate('app_tenant_login_username', [
                'tenant_url' => $this->tenant_url,
                'username' => $this->username ?: $request->get('username'),
            ])
        );
    }
}
