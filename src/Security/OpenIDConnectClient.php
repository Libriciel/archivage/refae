<?php

namespace App\Security;

use Jumbojett\OpenIDConnectClient as JumbojettOpenIDConnectClient;
use Override;

class OpenIDConnectClient extends JumbojettOpenIDConnectClient
{
    public ?string $redirectEndpoint = null;

    /**
     * @param string $url
     */
    #[Override]
    public function redirect($url)
    {
        $this->redirectEndpoint = $url;
    }
}
