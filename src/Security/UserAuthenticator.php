<?php

namespace App\Security;

use App\Entity\AccessUser;
use App\Entity\User;
use App\Repository\AccessUserRepository;
use App\Repository\UserRepository;
use App\Router\UrlTenantRouter;
use Doctrine\ORM\EntityManagerInterface;
use Override;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\RateLimiter\Exception\RateLimitExceededException;
use Symfony\Component\RateLimiter\LimiterInterface;
use Symfony\Component\RateLimiter\RateLimiterFactory;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CustomCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\SecurityRequestAttributes;

/**
 * Permet de logger un utilisateur avec
 * $security->login($user, UserAuthenticator::class);
 * (login sans tenant)
 */
class UserAuthenticator extends AbstractAuthenticator
{
    public ?User $user = null;
    public ?AccessUser $link = null;
    private LimiterInterface $limiter;

    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly UrlTenantRouter $router,
        private readonly RateLimiterFactory $loginLimiter,
        protected readonly AccessUserRepository $accessUserRepository,
        protected readonly EntityManagerInterface $entityManager,
    ) {
    }

    #[Override]
    public function supports(Request $request): ?bool
    {
        return $request->isMethod('post')
            && $request->attributes->get('_route') === 'public_login_username';
    }

    #[Override]
    public function authenticate(
        Request $request,
    ): Passport {
        $username = $request->get('username');
        $password = $request->get('login_password')['password'];
        $this->user = $this->userRepository->findOneByUsername($username);

        $accesses = $this->user?->getAccessUsers() ?: [];
        if ($accesses && $accesses->count() === 1) {
            $this->link = $accesses->first();
        } else {
            $adminAccess = null;
            $ldapAccesses = [];
            $otherAccesses = [];
            // si tous les accès sont sur le même ldap, on l'utilise pour logger
            // si l'utilisateur est un admin, on l'utilise cet accès en priorité
            // sinon, on log en type refae
            foreach ($accesses as $access) {
                if ($access->getTenant() === null) {
                    $adminAccess = $access;
                } elseif ($ldap = $access->getLdap()) {
                    $ldapAccesses[(string)$ldap->getId()] = $ldap;
                } else {
                    $otherAccesses[$access->getLoginType()] = true;
                }
            }
            if ($adminAccess) {
                $this->link = $adminAccess;
            } elseif (empty($otherAccesses) && count($ldapAccesses) === 1) {
                $this->link = $ldapAccesses[0];
            } else {
                $this->link = null;
            }
        }

        if ($this->link?->getLdap()) {
            $credentials = new CustomCredentials(
                $this->checkLdapUserCredentials(...),
                [$this->link, $password]
            );
        } else {
            $credentials = new PasswordCredentials($password);
        }

        $this->limiter = $this->loginLimiter->create($username);
        $limit = $this->limiter->consume();
        if (!$limit->isAccepted()) {
            $e = new RateLimitExceededException($limit);
            throw new AuthenticationException($e->getMessage(), 429, $e);
        }

        return new Passport(
            new UserBadge($username, $this->selectUser(...)),
            $credentials
        );
    }

    public function checkLdapUserCredentials(array $credentials)
    {
        /** @var AccessUser $link */
        [$link, $password] = $credentials;
        $ldap = $link->getLdap();
        $ldapConnection = $ldap?->log($link->getLdapUsername(), $password);
        if ($ldapConnection) {
            return true;
        }
        return false;
    }

    public function selectUser(): User
    {
        if (!$this->user) {
            return new User();
        }
        return $this->user;
    }

    #[Override]
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        $this->limiter->reset();
        $username = $request->get('username');
        $session = $request->getSession();
        if ($this->link?->getLdap()) {
            if ($this->link?->getTenant()) {
                $tenant_url = $this->link?->getTenant()->getBaseurl();
                $tenantSessionId = sprintf('tenant.%s', $tenant_url);
                $session = $request->getSession();
                $session->set(
                    $tenantSessionId,
                    [
                        'baseurl' => $tenant_url,
                        'username' => $username,
                        'connection' => AccessUser::LOGIN_TYPE_LDAP,
                        'ldap' => $this->link->getLdap()->getId(),
                    ]
                );
            }
            $session->set(
                'tenant.public',
                [
                    'baseurl' => 'public',
                    'username' => $username,
                    'connection' => AccessUser::LOGIN_TYPE_LDAP,
                    'ldap' => $this->link->getLdap()->getId(),
                ]
            );
        } else {
            $session->set(
                'tenant.public',
                [
                    'baseurl' => 'public',
                    'username' => $username,
                    'connection' => AccessUser::LOGIN_TYPE_REFAE,
                ]
            );
        }

        $lastTenant = null;
        $tenantCount = 0;
        foreach ($this->user->getAccessUsers() as $accessUser) {
            if ($accessUser->getTenant()?->isActive()) {
                $tenantCount++;
                $lastTenant = $accessUser->getTenant();
            } else {
                return new RedirectResponse(
                    $this->router->generate('admin_home')
                );
            }
        }
        if ($tenantCount === 1) {
            return new RedirectResponse(
                $this->router->generate('app_home', ['tenant_url' => $lastTenant->getBaseurl()])
            );
        }
        return new RedirectResponse(
            $this->router->generate('public_home')
        );
    }

    #[Override]
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $prevException = $exception->getPrevious();
        if ($prevException instanceof RateLimitExceededException) {
            $limit = $prevException->getRateLimit();
            $exception = new BadCredentialsException(
                "Vous avez dépassé le nombre maximal ({$limit->getLimit()})" .
                " de tentatives de connexion autorisées en 15 minutes",
                $exception->getCode(),
                $exception
            );
        } else {
            $exception = new BadCredentialsException(
                "Identifiant de connexion ou mot de passe invalide",
                $exception->getCode(),
                $exception
            );
        }

        $request->getSession()->set(SecurityRequestAttributes::AUTHENTICATION_ERROR, $exception);
        return new RedirectResponse(
            $this->router->generate('public_login_username', [
                'username' => $request->get('username'),
            ])
        );
    }
}
