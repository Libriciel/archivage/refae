<?php

namespace App\Security\Voter;

use App\Entity\OneTenantIntegrityEntityInterface;
use App\Entity\OneTenantOrNoneIntegrityEntityInterface;
use App\Entity\TenantsIntegrityEntityInterface;
use App\Repository\TenantRepository;
use Exception;
use Override;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Voter lié à l'appartenance de l'entity au tenant demandé dans l'url (cas tenant_id)
 */
class BelongsToTenantVoter extends Voter
{
    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly TenantRepository $tenantRepository,
    ) {
    }

    #[Override]
    protected function supports(string $attribute, mixed $subject): bool
    {
        return $attribute === BelongsToTenantVoter::class;
    }

    #[Override]
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $tenantUrl = $this->requestStack->getCurrentRequest()->get('tenant_url');

        if ($subject instanceof OneTenantIntegrityEntityInterface) {
            return $tenantUrl === $subject->getTenant()->getBaseurl();
        }
        if ($subject instanceof OneTenantOrNoneIntegrityEntityInterface) {
            $subjectBaseUrl = $subject->getTenant()?->getBaseurl();
            return $subjectBaseUrl === null || $tenantUrl === $subjectBaseUrl;
        }
        if ($subject instanceof TenantsIntegrityEntityInterface) {
            $tenant = $this->tenantRepository->findOneByBaseurl($tenantUrl);
            return in_array($tenant, $subject->getTenants()->toArray());
        }

        throw new Exception(
            sprintf(
                '"%s" should implements OneTenantIntegrityEntityInterface, OneTenantOrNoneIntegrityEntityInterface'
                    . ' or TenantsIntegrityEntityInterface to be used with BelongsTenantVoter',
                is_object($subject) ? $subject::class : (string)$subject
            )
        );
    }
}
