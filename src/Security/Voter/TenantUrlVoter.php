<?php

namespace App\Security\Voter;

use App\Entity\User;
use Override;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Voter lié à l'appartenance de l'user au tenant demandé dans l'url
 */
class TenantUrlVoter extends Voter
{
    #[Override]
    protected function supports(string $attribute, mixed $subject): bool
    {
        return $attribute === TenantUrlVoter::class;
    }

    #[Override]
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        foreach ($user->getTenants() as $tenant) {
            if ($tenant->isActive() && $tenant->getBaseurl() === $subject) {
                return true;
            }
        }

        return false;
    }
}
