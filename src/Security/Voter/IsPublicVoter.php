<?php

namespace App\Security\Voter;

use App\Entity\PublishableEntityInterface;
use App\Entity\VersionableEntityInterface;
use Exception;
use Override;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Voter pour les parties publiques : l'entité est publiée et publique
 */
class IsPublicVoter extends Voter
{
    #[Override]
    protected function supports(string $attribute, mixed $subject): bool
    {
        return $attribute === IsPublicVoter::class;
    }

    #[Override]
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        if (!$subject instanceof PublishableEntityInterface) {
            throw new Exception(
                sprintf(
                    '"%s" should implements PublishableEntityInterface to be used with IsPublicVoter',
                    is_object($subject) ? $subject::class : (string)$subject
                )
            );
        }

        return $subject->isPublic() && $subject->getState() === VersionableEntityInterface::S_PUBLISHED;
    }
}
