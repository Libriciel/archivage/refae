<?php

namespace App\Security\Voter;

use App\Entity\EditableEntityInterface;
use Exception;
use Override;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class EntityIsEditableVoter extends Voter
{
    #[Override]
    protected function supports(string $attribute, mixed $subject): bool
    {
        return $attribute === EntityIsEditableVoter::class;
    }

    #[Override]
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        if (!$subject instanceof EditableEntityInterface) {
            throw new Exception(
                sprintf(
                    '"%s" should implements EditableEntityInterface to be used with EntityIsEditableVoter',
                    is_object($subject) ? $subject::class : (string)$subject
                )
            );
        }

        return $subject->getEditable();
    }
}
