<?php

namespace App\Security\Voter;

use App\Entity\DeletableEntityInterface;
use App\Entity\User;
use App\Service\LoggedUser;
use Exception;
use Override;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class EntityIsDeletableVoter extends Voter
{
    public function __construct(
        protected readonly LoggedUser $loggedUser,
    ) {
    }

    #[Override]
    protected function supports(string $attribute, mixed $subject): bool
    {
        return $attribute === EntityIsDeletableVoter::class;
    }

    #[Override]
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        if (!$subject instanceof DeletableEntityInterface) {
            throw new Exception(
                sprintf(
                    '"%s" should implements DeletableEntityInterface to be used with EntityIsDeletableVoter',
                    is_object($subject) ? $subject::class : (string)$subject
                )
            );
        }

        // Cas particulier pour User
        if ($subject instanceof User) {
            // on ne peut pas se supprimer soit même
            if ($this->loggedUser->user()->getId() === $subject->getId()) {
                return false;
            }

            // User a besoin de contexte pour déterminer son getDeletable
            if ($this->loggedUser->tenant()) {
                $subject->setActiveTenant($this->loggedUser->tenant());
            }
        }

        return $subject->getDeletable();
    }
}
