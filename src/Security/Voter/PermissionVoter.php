<?php

namespace App\Security\Voter;

use App\Entity\User;
use App\Repository\TenantRepository;
use App\Service\Permission;
use Override;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

readonly class PermissionVoter implements VoterInterface
{
    public function __construct(
        private Permission $permission,
        private TenantRepository $tenantRepository,
        private RequestStack $requestStack,
    ) {
    }

    #[Override]
    public function vote(TokenInterface $token, mixed $subject, array $attributes): int
    {
        $vote = self::ACCESS_ABSTAIN;

        foreach ($attributes as $attribute) {
            if (preg_match('#^acl/(.*)/(.*)#', (string) $attribute, $m)) {
                [, $classname, $action] = $m;
                return $this->isGranted($token, $classname, $action);
            }
        }

        return $vote;
    }

    public function isGranted(TokenInterface $token, string $permissionClass, string $action)
    {
        $user = $token->getUser();
        if ($user instanceof User) {
            $request = $this->requestStack->getCurrentRequest();
            $tenantBaseurl = $request?->get('tenant_url') ?? null;
            $tenant = $tenantBaseurl ? $this->tenantRepository->findOneBy(['baseurl' => $tenantBaseurl]) : null;
            $tenant = $tenant ?: $user->getActiveTenant();
            if ($this->permission->userCanAccess($user, $tenant, $permissionClass, $action)) {
                return self::ACCESS_GRANTED;
            } else {
                return self::ACCESS_DENIED;
            }
        }

        return self::ACCESS_DENIED;
    }
}
