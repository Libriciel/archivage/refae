<?php

namespace App\DataFixtures;

use App\Cron;
use App\Entity\AccessUser;
use App\Entity\Activity;
use App\Entity\ActivitySubjectEntityInterface;
use App\Entity\Agreement;
use App\Entity\AgreementProfileIdentifier;
use App\Entity\Authority;
use App\Entity\AuthorityFile;
use App\Entity\Category;
use App\Entity\Cron as CronEntity;
use App\Entity\DocumentRule;
use App\Entity\ProfileFile;
use App\Entity\RgpdInfo;
use App\Entity\RuleType;
use App\Entity\VocabularyFile;
use App\Entity\Label;
use App\Entity\LabelGroup;
use App\Entity\Ldap;
use App\Entity\ManagementRule;
use App\Entity\Openid;
use App\Entity\Profile;
use App\Entity\Role;
use App\Entity\ServiceLevel;
use App\Entity\Tenant;
use App\Entity\Term;
use App\Entity\User;
use App\Entity\UserCreationRequest;
use App\Entity\VersionableEntityInterface;
use App\Entity\Vocabulary;
use App\Repository\RoleRepository;
use App\Service\Manager\FileManager;
use App\Service\Manager\TenantManager;
use App\Service\Permission;
use DateInterval;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory as FakerFactory;
use Faker\Generator as Faker;
use Faker\Provider\fr_FR\Company as FakerProviderCompany;
use Override;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AllRecetteFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    private readonly Faker $faker;

    public function __construct(
        private readonly UserPasswordHasherInterface $passwordHasher,
        private readonly Permission $permission,
        private readonly RoleRepository $roleRepository,
        private readonly TenantManager $tenantManager,
    ) {
        $this->faker = FakerFactory::create('fr_FR');
        $this->faker->seed(0x71228443);
        $provider = new FakerProviderCompany($this->faker);
        $this->faker->addProvider($provider);
    }

    #[Override]
    public function load(ObjectManager $manager): void
    {
        $this->permission->privilege();
        $this->permission->permissionRole();

        $user1 = new User();
        $user1->setUsername('recette1')
            ->setPassword($this->passwordHasher->hashPassword($user1, 'recette1recette1recette1'))
            ->setEmail('recette1@refae.fr')
            ->setNotifyUserRegistration(true)
            ->setUserDefinedPassword(true)
        ;
        $manager->persist($user1);

        $user2 = new User();
        $user2->setUsername('recette2')
            ->setPassword($this->passwordHasher->hashPassword($user2, 'recette2recette2recette2'))
            ->setEmail('recette2@refae.fr')
            ->setNotifyUserRegistration(true)
            ->setUserDefinedPassword(true)
        ;
        $manager->persist($user2);

        $ldap = new Ldap();
        $ldap
            ->setName('Futurama')
            ->setHost('ldap')
            ->setPort(10389)
            ->setUserQueryLogin('cn=admin,dc=planetexpress,dc=com')
            ->setUserQueryPassword('GoodNewsEveryone')
            ->setLdapRootSearch('ou=people,dc=planetexpress,dc=com')
            ->setUserLoginAttribute('cn')
            ->setLdapUsersFilter('(objectClass=inetOrgPerson)')
            ->setAccountPrefix('cn=')
            ->setAccountSuffix(',ou=people,dc=planetexpress,dc=com')
            ->setUseProxy(true)
            ->setUseSsl(false)
            ->setUseTls(false)
            ->setUserNameAttribute('cn')
            ->setUserMailAttribute('mail')
            ->setSchema('Adldap\Schemas\ActiveDirectory') // obsolète
            ->setFollowReferrals(false)
            ->setVersion(3)
            ->setTimeout(5)
            ->setCustomOptions('[]')
            ->setUserUsernameAttribute('uid')
        ;
        $manager->persist($ldap);

        $ldap2 = new Ldap();
        $ldap2
            ->setName('Futurama2')
            ->setHost('ldap')
            ->setPort(10389)
            ->setUserQueryLogin('cn=admin,dc=planetexpress,dc=com')
            ->setUserQueryPassword('GoodNewsEveryone')
            ->setLdapRootSearch('ou=people,dc=planetexpress,dc=com')
            ->setUserLoginAttribute('cn')
            ->setLdapUsersFilter('(objectClass=inetOrgPerson)')
            ->setAccountPrefix('cn=')
            ->setAccountSuffix(',ou=people,dc=planetexpress,dc=com')
            ->setUseProxy(true)
            ->setUseSsl(false)
            ->setUseTls(false)
            ->setUserNameAttribute('cn')
            ->setUserMailAttribute('mail')
            ->setSchema('Adldap\Schemas\ActiveDirectory') // obsolète
            ->setFollowReferrals(false)
            ->setVersion(3)
            ->setTimeout(5)
            ->setCustomOptions('[]')
            ->setUserUsernameAttribute('uid')
        ;
        $manager->persist($ldap2);

        $user3 = new User();
        $user3->setUsername('fry')
            ->setEmail('fry@refae.fr')
            ->setNotifyUserRegistration(true)
            ->setUserDefinedPassword(false);
        $manager->persist($user3);

        $tenant1 = new Tenant();
        $tenant1
            ->setName('Tenant n°1')
            ->setBaseurl('tenant1')
            ->setDescription('tenant 1');
        $tenant1->addUser($user1);
        $tenant1->addUser($user2);
        $tenant1->addUser($user3);
        $tenant1->addLdap($ldap);
        $manager->persist($tenant1);
        $this->tenantManager->initialize($tenant1, ['initAccessRules' => true]);

        $tenant2 = new Tenant();
        $tenant2
            ->setName('Tenant n°2')
            ->setBaseurl('tenant2')
            ->setDescription('tenant 2');
        $tenant2->addUser($user1);
        $tenant2->addUser($user3);
        $tenant2->addLdap($ldap2);
        $manager->persist($tenant2);
        $this->tenantManager->initialize($tenant2, ['initAccessRules' => true]);

        $tenant3 = new Tenant();
        $tenant3
            ->setName('tenant3')
            ->setBaseurl('tenant3')
            ->setDescription('tenant 3 sans user');
        $manager->persist($tenant3);
        $this->tenantManager->initialize($tenant3, ['initAccessRules' => true]);

        /** @var RoleRepository $roleRepository */
        $roleRepository = $manager->getRepository(Role::class);
        $roleFunctionalAdmin = $roleRepository->findOneBy(['name' => Permission::ROLE_FUNCTIONAL_ADMIN]);

        $accessUser = new AccessUser();
        $accessUser->setUser($user1)
            ->setRole($roleFunctionalAdmin)
            ->setTenant($tenant1);
        $manager->persist($accessUser);

        $accessUser = new AccessUser();
        $accessUser->setUser($user1)
            ->setRole($roleFunctionalAdmin)
            ->setTenant($tenant2);
        $manager->persist($accessUser);

        $accessUser = new AccessUser();
        $accessUser->setUser($user2)
            ->setRole($roleFunctionalAdmin)
            ->setTenant($tenant1);
        $manager->persist($accessUser);

        $roleTechnicalAdmin = $roleRepository->findOneBy(['name' => Permission::ROLE_TECHNICAL_ADMIN]);
        $accessUser = new AccessUser();
        $accessUser->setUser($user1)
            ->setRole($roleTechnicalAdmin);
        $manager->persist($accessUser);

        $ldapUser = new AccessUser();
        $ldapUser->setUser($user3);
        $ldapUser->setTenant($tenant1);
        $ldapUser->setRole($roleFunctionalAdmin);
        $ldapUser->setLdap($ldap);
        $ldapUser->setLdapUsername("Philip J. Fry");
        $manager->persist($ldapUser);

        $ldapUser2 = new AccessUser();
        $ldapUser2->setUser($user3);
        $ldapUser2->setTenant($tenant2);
        $ldapUser2->setRole($roleFunctionalAdmin);
        $ldapUser2->setLdap($ldap2);
        $ldapUser2->setLdapUsername("Philip J. Fry");
        $manager->persist($ldapUser2);

        $user4 = new User();
        $user4
            ->setUsername('openid')
            ->setEmail('openid@refae.fr')
            ->addTenant($tenant1)
            ->addTenant($tenant2)
            ->setNotifyUserRegistration(true)
            ->setUserDefinedPassword(false);
        $manager->persist($user4);

        $tenant1->addUser($user4);
        $manager->persist($tenant1);

        $tenant2->addUser($user4);
        $manager->persist($tenant2);

        $openid = new Openid();
        $openid->setName('Keycloak');
        $openid->setUrl('http://keycloak:8080/realms/master');
        $openid->setIdentifier('keycloak1');
        $openid->setUsernameField('preferred_username');
        $openid->setClientId('refae');
        $openid->setClientSecret('4ifYZia3OhVYNvqkmtSMe4CcQwtljdAS');
        $openid->addTenant($tenant1);
        $manager->persist($openid);
        $tenant1->addOpenid($openid);
        $manager->persist($tenant1);

        $openid2 = new Openid();
        $openid2->setName('Keycloak 2');
        $openid2->setUrl('http://keycloak:8080/realms/master');
        $openid2->setIdentifier('keycloak2');
        $openid2->setUsernameField('preferred_username');
        $openid2->setClientId('refae2');
        $openid2->setClientSecret('CP9iendiabNhoUvT8M1DVoaLcIE3x8Fq');
        $openid2->addTenant($tenant2);
        $manager->persist($openid2);
        $tenant2->addOpenid($openid2);
        $manager->persist($tenant2);

        $openidUser = new AccessUser();
        $openidUser->setUser($user4);
        $openidUser->setRole($roleFunctionalAdmin);
        $openidUser->setTenant($tenant1);
        $openidUser->setOpenid($openid);
        $manager->persist($openidUser);

        $openid2User = new AccessUser();
        $openid2User->setUser($user4);
        $openid2User->setRole($roleFunctionalAdmin);
        $openid2User->setTenant($tenant2);
        $openid2User->setOpenid($openid2);
        $manager->persist($openid2User);

        $labelGroup1Tenant1 = new LabelGroup();
        $labelGroup1Tenant1->setTenant($tenant1);
        $labelGroup1Tenant1->setName('recette');
        $manager->persist($labelGroup1Tenant1);

        $labelGroup2Tenant1 = new LabelGroup();
        $labelGroup2Tenant1->setTenant($tenant1);
        $labelGroup2Tenant1->setName('dev');
        $manager->persist($labelGroup2Tenant1);

        $labelGroup3Tenant1 = new LabelGroup();
        $labelGroup3Tenant1->setTenant($tenant1);
        $labelGroup3Tenant1->setName('Libriciel');
        $manager->persist($labelGroup3Tenant1);

        $labelGroup1Tenant2 = new LabelGroup();
        $labelGroup1Tenant2->setTenant($tenant2);
        $labelGroup1Tenant2->setName('Libriciel');
        $manager->persist($labelGroup1Tenant2);

        $labelsTenant1 = [];

        $label1 = new Label();
        $label1->setTenant($tenant1);
        $label1->setLabelGroup($labelGroup1Tenant1);
        $label1->setName('A tester');
        $label1->setColor('#5843ad');
        $manager->persist($label1);
        $labelsTenant1[] = $label1;

        $label2 = new Label();
        $label2->setTenant($tenant1);
        $label2->setLabelGroup($labelGroup1Tenant1);
        $label2->setName('Recette OK');
        $label2->setColor('#a295d6');
        $manager->persist($label2);
        $labelsTenant1[] = $label2;

        $label3 = new Label();
        $label3->setTenant($tenant1);
        $label3->setLabelGroup($labelGroup2Tenant1);
        $label3->setName('A faire');
        $label3->setColor('#5cb85c');
        $manager->persist($label3);
        $labelsTenant1[] = $label3;

        $label4 = new Label();
        $label4->setTenant($tenant1);
        $label4->setLabelGroup($labelGroup2Tenant1);
        $label4->setName('Fait');
        $label4->setColor('#a8d695');
        $manager->persist($label4);
        $labelsTenant1[] = $label4;

        $label5 = new Label();
        $label5->setTenant($tenant1);
        $label5->setLabelGroup(null);
        $label5->setName('cool');
        $label5->setColor('#598db9');
        $manager->persist($label5);
        $labelsTenant1[] = $label5;

        $logiciels = [
            'asalae' => '#61869e',
            'comélus' => '#207671',
            'idelibre' => '#0081c5',
            'iparapheur' => '#2d9ca1',
            'lsmessage' => '#42a1e8',
            'pastell' => '#4b508a',
            'packs pastell' => '#61669e',
            'refae' => '#4b708b',
            's²low' => '#222222',
            'versae' => '#799ab1',
            'webactes' => '#0c9142',
            'webdelib' => '#58a725',
            'webgfc' => '#5397a7',
        ];
        $labelsTenant2 = [];
        foreach ($logiciels as $logiciel => $color) {
            $labelLogiciel = new Label();
            $labelLogiciel->setTenant($tenant1);
            $labelLogiciel->setLabelGroup($labelGroup3Tenant1);
            $labelLogiciel->setName($logiciel);
            $labelLogiciel->setColor($color);
            $manager->persist($labelLogiciel);
            $labelsTenant1[] = $labelLogiciel;

            $labelLogiciel = new Label();
            $labelLogiciel->setTenant($tenant2);
            $labelLogiciel->setLabelGroup($labelGroup1Tenant2);
            $labelLogiciel->setName($logiciel);
            $labelLogiciel->setColor($color);
            $manager->persist($labelLogiciel);
            $labelsTenant2[] = $labelLogiciel;
        }

        $file = FileManager::setDataFromPath(__DIR__ . '/../../README.md');
        $manager->persist($file);

        $profile = new Profile();
        $profile
            ->setTenant($tenant1)
            ->setIdentifier('profil1')
            ->setName('profil1')
            ->setDescription('description 1')
            ->setPublic(true)
            ->setState(VersionableEntityInterface::S_PUBLISHED)
            ->setVersion(1)
        ;
        $manager->persist($profile);

        $profile = new Profile();
        $profile
            ->setTenant($tenant1)
            ->setIdentifier('profil1')
            ->setName('profil1')
            ->setDescription('description 1')
            ->setPublic(true)
            ->setState(VersionableEntityInterface::S_EDITING)
            ->setVersion(2)
            ->addLabel($label1)
        ;
        $manager->persist($profile);

        $file = FileManager::setDataFromPath(__DIR__ . '/resources/actes_v1.rng');
        $manager->persist($file);

        $profileFile = new ProfileFile();
        $profileFile
            ->setFile($file)
            ->setProfile($profile)
            ->setDescription('Fichier actes')
            ->setType(ProfileFile::SCHEMA)
        ;
        $manager->persist($profileFile);

        $profile = new Profile();
        $profile
            ->setTenant($tenant1)
            ->setIdentifier('profil2')
            ->setName('profil2')
            ->setDescription('description 2')
            ->setPublic(true)
            ->setState(VersionableEntityInterface::S_PUBLISHED)
            ->setVersion(1)
        ;
        $manager->persist($profile);

        $profile = new Profile();
        $profile
            ->setTenant($tenant1)
            ->setIdentifier('profil2')
            ->setName('profil2')
            ->setDescription('description 2')
            ->setPublic(true)
            ->setState(VersionableEntityInterface::S_EDITING)
            ->setVersion(2)
        ;
        $manager->persist($profile);

        $profile = new Profile();
        $profile
            ->setTenant($tenant2)
            ->setIdentifier('profil1')
            ->setName('profil1')
            ->setDescription('description 1')
            ->setPublic(true)
            ->setState(VersionableEntityInterface::S_EDITING)
            ->setVersion(1)
        ;
        $manager->persist($profile);

        $authority1 = new Authority();
        $authority1
            ->setTenant($tenant1)
            ->setIdentifier('authority1')
            ->setName('authority1')
            ->setDescription('description de authority1')
            ->setMaintenanceStatus('maintenance status de autority1')
            ->setExistDatesFrom(new DateTime('2000-01-01'))
            ->setExistDatesTo(new DateTime('2000-12-31'))
            ->setEntityType('entity_type de authority1')
            ->setLegalStatus('legal_status de authority1')
            ->setPlace('101 Rue de Authority1, 99999 Celeste, Monde')
            ->setFunction('function de authority1')
            ->setOccupation('occupation de authority1')
            ->setMandate('mandate de authority1')
            ->setPublic(true)
            ->setState(VersionableEntityInterface::S_DEPRECATED)
            ->setVersion(1)
            ->addLabel($label1)
        ;
        $authority2 = clone $authority1;
        $manager->persist($authority1);

        $authority2
            ->setDescription('description de authority2')
            ->setState(VersionableEntityInterface::S_PUBLISHED)
            ->setVersion(2);
        $manager->persist($authority2);

        $file2 = FileManager::setDataFromPath(__DIR__ . '/resources/actes_v1.xml');
        $manager->persist($file2);

        $authorityFile1 = new AuthorityFile();
        $authorityFile1
            ->setAuthority($authority2)
            ->setFile($file2)
            ->setType(AuthorityFile::TYPE_OTHER)
        ;
        $manager->persist($authorityFile1);

        $authority3 = new Authority();
        $authority3
            ->setTenant($tenant1)
            ->setIdentifier('authority3')
            ->setName('authority3')
            ->setPublic(true)
            ->setState(VersionableEntityInterface::S_EDITING)
            ->setVersion(1)
        ;
        $manager->persist($authority3);

        $file4 = FileManager::setDataFromPath(__DIR__ . '/resources/actes_v1.xml');
        $manager->persist($file4);

        $authorityFile2 = new AuthorityFile();
        $authorityFile2
            ->setAuthority($authority3)
            ->setFile($file4)
            ->setType(AuthorityFile::TYPE_OTHER)
        ;
        $manager->persist($authorityFile2);

        $vocabulary = new Vocabulary();
        $vocabulary
            ->setTenant($tenant1)
            ->setIdentifier('vocabulary1')
            ->setName('vocabulary1')
            ->setDescription('this is vocabulary 1')
            ->setPublic(true)
            ->setState(VersionableEntityInterface::S_PUBLISHED)
            ->setVersion(1)
            ->addLabel($label1)
        ;
        $manager->persist($vocabulary);

        $term = new Term();
        $term
            ->setVocabulary($vocabulary)
            ->setName('term1')
            ->setIdentifier('term1')
            ->setDirectoryName('termDirectory')
        ;
        $manager->persist($term);

        $file3 = FileManager::setDataFromPath(__DIR__ . '/resources/actes_v1.xml');
        $manager->persist($file3);

        $vocabularyFile = new VocabularyFile();
        $vocabularyFile
            ->setType(VocabularyFile::OTHER)
            ->setDescription('un fichier')
            ->setVocabulary($vocabulary)
            ->setFile($file3)
        ;
        $manager->persist($vocabularyFile);

        $agreement = new Agreement();
        $agreement
            ->setTenant($tenant1)
            ->setIdentifier('agreement1')
            ->setName('agreement1')
            ->setValidityBegin(new DateTime('2000-01-01'))
            ->setValidityEnd(new DateTime('2000-12-31'))
            ->setPublic(true)
            ->setState(VersionableEntityInterface::S_PUBLISHED)
            ->setVersion(1);
        $agreement
            ->addLabel($label1)
            ->addLabel($label2)
            ->addLabel($label3);
        $agreement->addProfileIdentifier((new AgreementProfileIdentifier($profile)));
        $agreement->addProfileIdentifier(
            (new AgreementProfileIdentifier())
                ->setIdentifier('cetIdentifiantNExistePas')
                ->setName('cetIdentifiantNExistePas')
        );
        $manager->persist($agreement);

        $categories = [
            'Commune' => [
                'Affaires scolaires' => [
                    'Administration' => true,
                    'Personnel' => true,
                ],
                'Tourisme' => [
                    'Office du tourisme' => true,
                    'Mise en valeur touristique' => true,
                    'Accueil touristique' => true,
                ],
            ],
            'Département' => [
                'Finances' => [
                    'Pièces comptables et financières' => true,
                ],
                'Ressources humaines' => true,
                'Culture' => true,
                'Cabinet' => true,
                'Urbanisme' => true,
            ],
            'Libriciel' => [
                'Pôle archivage' => [
                    'Asalae' => true,
                ],
                'Pôle administratif, Finances et Moyens Généraux' => [
                    'Parc automobile' => true,
                    'Ressources humaines et paie' => true,
                    'Contrats et facturation' => true,
                    'Design' => true,
                    'Sociétariat' => true,
                ],
                'Pôle Services Clients' => [
                    'Consultants' => true,
                ],
            ],
        ];
        $this->createCategories($categories, $manager, $labelsTenant1);
        $this->documentCount = 35; // refait au moins 20 document_rule (55 - 35)
        $this->createCategories(
            ['Libriciel' => $categories['Libriciel']],
            $manager,
            $labelsTenant2
        );

        $serviceLevel = (new ServiceLevel())
            ->setIdentifier('serviceLevel1')
            ->setName('serviceLevel1')
            ->setDescription('Ceci est un niveau de service')
            ->setTenant($tenant1)
            ->setPublic(true)
            ->setVersion(1)
            ->setState(VersionableEntityInterface::S_PUBLISHED)
            ->addLabel($label3)
        ;
        $manager->persist($serviceLevel);

        $userCreationRequest = new UserCreationRequest();
        $userCreationRequest
            ->setUsername('test_request')
            ->setName('test')
            ->setEmail('test_request@test.fr')
            ->setIp('127.0.0.1')
            ->setEmailVerified(true)
            ->setTenant($tenant1)
        ;
        $manager->persist($userCreationRequest);

        $storageRuleType = $this->getReference('StorageRule', RuleType::class);
        $appraisalRuleType = $this->getReference('AppraisalRule', RuleType::class);
        foreach ([$tenant1, $tenant2, $tenant3] as $tenant) {
            for ($i = 0; $i <= 100; $i++) {
                $duration = 'P' . $i . 'Y';
                $identifierDua = 'AP' . $duration;
                $identifierDuc = 'DUC' . $duration;
                $name = $i . ' an';
                if ($i > 1) {
                    $name .= 's';
                }
                $description = 'Règle de gestion ' . $name;
                $storageRule = (new ManagementRule())
                    ->setIdentifier($identifierDuc)
                    ->setName($name)
                    ->setDescription($description)
                    ->setRuleType($storageRuleType)
                    ->setDuration($duration)
                    ->setTenant($tenant)
                    ->setPublic(true)
                    ->setVersion(1)
                    ->setState(VersionableEntityInterface::S_PUBLISHED);
                $manager->persist($storageRule);

                $appraisalRule = (new ManagementRule())
                    ->setIdentifier($identifierDua)
                    ->setName($name)
                    ->setDescription($description)
                    ->setRuleType($appraisalRuleType)
                    ->setDuration($duration)
                    ->setTenant($tenant)
                    ->setPublic(true)
                    ->setVersion(1)
                    ->setState(VersionableEntityInterface::S_PUBLISHED);
                $manager->persist($appraisalRule);
            }
        }

        // génère un faux historique (affichage du /tenant/home)
        $day = new DateTime();
        $day->modify('- 7 days');
        $models = array_flip([
            'Agreement',
            'LabelGroup',
            'Label',
            'Profile',
            'Authority',
            'Vocabulary',
            'Category',
            'ServiceLevel',
        ]);
        for ($i = 0; $i < 6; $i++) {
            $day->modify('+ 1 day');
            $newDay = clone $day;
            if ($newDay->format('N') >= 6) {
                continue;
            }
            for ($j = 0; $j < random_int(1, 50); $j++) {
                $model = array_rand($models);
                $foreignModel = "App\\Entity\\$model";
                /** @var ServiceEntityRepository $repository */
                $repository = $manager->getRepository($foreignModel);
                $entity = $repository->findOneBy([]);
                if (!$entity instanceof ActivitySubjectEntityInterface) {
                    continue;
                }
                $activity = new Activity();
                $activity
                    ->setActivityEntity($entity)
                    ->setCreated($newDay)
                    ->setAction('create')
                    ->setSubject($entity->getActivityName() . ' pour remplir le graph du tableau de bord')
                    ->setForeignModel($foreignModel)
                    ->setScope($entity->getActivityScope())
                    ->setUserDeletedName('fixture')
                ;
                $manager->persist($activity);
            }
        }

        $cron = new CronEntity();
        $cron
            ->setName('Test cron')
            ->setDescription("Cron pour tester le fonctionnement des crons")
            ->setFrequency('PT1H')
            ->setNext(
                (new DateTime('02:00:00'))->sub(new DateInterval('P1D'))
            )
            ->setService(Cron\Test::class)
        ;
        $manager->persist($cron);

        $this->generateUserForEachRoles($manager, $tenant1);

        $adminRgpdInfo = new RgpdInfo();
        $adminRgpdInfo
            ->setOrgName(<<<TEXT
Libriciel SCOP
Éditeur de logiciels libres métiers pour les collectivités et administrations.
TEXT)
            ->setOrgAddress(<<<TEXT
140 rue Aglaonice de Thessalie
Castelnau-le-lez
34170
TEXT)
            ->setOrgSiret('491 011 698 00033')
            ->setOrgApe('6201Z - Programmation informatique')
            ->setOrgPhone('04 67 65 96 44')
            ->setOrgEmail('contact@libriciel.coop')
            ->setCeoName('Frédéric Losserand')
            ->setCeoFunction('PDG')
            ->setComment($this->faker->unique()->realText())
        ;
        $manager->persist($adminRgpdInfo);

        $rgpdInfo = new RgpdInfo();
        /** @noinspection PhpUndefinedMethodInspection */
        $rgpdInfo
            ->setTenant($tenant1)
            ->setOrgName($this->faker->unique()->company())
            ->setOrgAddress($this->faker->unique()->address())
            ->setOrgSiret($this->faker->unique()->siret())
            ->setOrgApe('6201Z')
            ->setOrgPhone($this->faker->unique()->phoneNumber())
            ->setOrgEmail($this->faker->unique()->email())
            ->setCeoName($this->faker->unique()->name())
            ->setCeoFunction("Directeur Général")
            ->setDpoName($this->faker->unique()->name())
            ->setDpoEmail($this->faker->unique()->email())
        ;
        $manager->persist($rgpdInfo);

        $manager->flush();
    }

    /**
     * @param array         $categories
     * @param ObjectManager $manager
     * @param Label[]       $labels
     * @param Category|null $parent
     * @return void
     */
    private function createCategories(
        array $categories,
        ObjectManager $manager,
        array $labels,
        ?Category $parent = null
    ) {
        $tenant = $labels[0]->getTenant();
        $leafCategories = [];
        foreach ($categories as $category => $value) {
            $newCategory = new Category();
            $newCategory->setTenant($tenant);
            $newCategory->setName($category);
            $newCategory->setDescription('');
            $newCategory->setParent($parent);
            $manager->persist($newCategory);
            if (is_array($value)) {
                $this->createCategories($value, $manager, $labels, $newCategory);
            } else {
                $leafCategories[] = $newCategory;
                $this->createDocumentRule($tenant, $manager, $newCategory, $labels);
            }
        }
        while ($this->documentCount < 55) {
            foreach ($leafCategories as $newCategory) {
                $this->createDocumentRule($tenant, $manager, $newCategory, $labels);
            }
        }
    }

    #[Override]
    public function getDependencies()
    {
        return [
            RuleTypeFixtures::class,
        ];
    }

    #[Override]
    public static function getGroups(): array
    {
        return ['recette', 'test'];
    }

    private int $documentCount = 0;
    private function createDocumentRule(
        Tenant $tenant,
        ObjectManager $manager,
        Category $newCategory,
        array $labels,
    ) {
        for ($i = 0; $i < $this->faker->numberBetween(-1, 16); $i++) {
            $this->documentCount++;
            $documentRule = new DocumentRule();
            $documentRule->setTenant($tenant);
            $documentRule->addCategory($newCategory);
            $prefix = strtoupper((string) $tenant->getBaseurl());
            $documentRule->setIdentifier(
                sprintf('%s_RULE%03d', $prefix, $this->documentCount)
            );
            $documentRule->setName($this->faker->unique()->text(20));
            $documentRule->setObservation($this->faker->unique()->realText());
            $documentRule->setRegulatoryText($this->faker->unique()->realText());
            $documentRule->setDocumentLocation($this->faker->address());
            $documentRule->setDocumentSupport($this->faker->randomElement(DocumentRule::DOCUMENT_SUPPORT_VALUES));
            $documentRule->setAppraisalRuleFinalAction(
                $this->faker->randomElement(DocumentRule::APPRASAIL_RULE_FINAL_ACTION_VALUES)
            );
            $documentRule->setPublic(true);
            $documentRule->setState(VersionableEntityInterface::S_PUBLISHED);
            $documentRule->setVersion(1);

            for ($j = 0; $j < $this->faker->numberBetween(-1, 3); $j++) {
                $documentRule->addLabel($this->faker->randomElement($labels));
            }

            $manager->persist($documentRule);
        }
    }

    private function generateUserForEachRoles(ObjectManager $manager, Tenant $tenant)
    {
        foreach (Permission::$commonNames['roles'] as $roleName => $commonName) {
            $user = new User();
            $user
                ->setUsername($roleName)
                ->setName($commonName)
                ->setPassword($this->passwordHasher->hashPassword($user, str_repeat((string) $roleName, 3)))
                ->setEmail($roleName . '@refae.fr')
                ->setNotifyUserRegistration(true)
                ->setUserDefinedPassword(true)
            ;
            $role = $this->roleRepository->findOneBy(['name' => $roleName]);
            $accessUser = new AccessUser();
            $accessUser
                ->setUser($user)
                ->setRole($role)
            ;
            if ($roleName !== Permission::ROLE_TECHNICAL_ADMIN) {
                $user->addTenant($tenant);
                $accessUser->setTenant($tenant);
            }
            $manager->persist($user);
            $manager->persist($accessUser);
        }
        $manager->flush();
    }
}
