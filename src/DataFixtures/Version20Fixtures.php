<?php

namespace App\DataFixtures;

use App\Entity\Version;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Override;

class Version20Fixtures extends Fixture implements FixtureGroupInterface
{
    #[Override]
    public function load(ObjectManager $manager): void
    {
        $version = new Version();

        if ($manager->getRepository($version::class)->count([])) {
            return;
        }

        $version->setSubject('refae');
        $version->setVersion('2.0.0');
        $manager->persist($version);
        $manager->flush();
    }

    // Ajouter des dépendences dans les futures versions

    #[Override]
    public static function getGroups(): array
    {
        return ['prod', 'recette', 'test'];
    }
}
