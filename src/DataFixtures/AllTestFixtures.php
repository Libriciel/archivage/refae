<?php

namespace App\DataFixtures;

use App\Entity\AccessUser;
use App\Entity\Activity;
use App\Entity\Authority;
use App\Entity\AuthorityFile;
use App\Entity\Category;
use App\Entity\DocumentRuleImportSession;
use App\Entity\Label;
use App\Entity\LabelGroup;
use App\Entity\Profile;
use App\Entity\Role;
use App\Entity\ServiceLevel;
use App\Entity\Tenant;
use App\Entity\Term;
use App\Entity\User;
use App\Entity\VersionableEntityInterface;
use App\Entity\Vocabulary;
use App\Repository\RoleRepository;
use App\Service\Manager\FileManager;
use App\Service\Permission;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Override;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AllTestFixtures extends Fixture implements FixtureGroupInterface
{
    public function __construct(
        private readonly UserPasswordHasherInterface $passwordHasher,
        private readonly Permission $permission,
    ) {
    }

    #[Override]
    public function load(ObjectManager $manager): void
    {
        $this->permission->privilege();
        $this->permission->permissionRole();

        $user1 = new User();
        $user1->setUsername('test_user1')
            ->setPassword($this->passwordHasher->hashPassword($user1, 'testtesttest'))
            ->setEmail('test1@refae.fr')
            ->setNotifyUserRegistration(true)
            ->setUserDefinedPassword(true)
        ;
        $manager->persist($user1);

        $user2 = new User();
        $user2->setUsername('test_user2')
            ->setPassword($this->passwordHasher->hashPassword($user2, 'testtesttest'))
            ->setEmail('recette2@refae.fr')
            ->setNotifyUserRegistration(true)
            ->setUserDefinedPassword(true)
        ;
        $manager->persist($user2);

        $tenant1 = new Tenant();
        $tenant1
            ->setName('test_tenant1')
            ->setBaseurl('test_tenant1')
            ->setDescription('tenant 1');
        $tenant1->addUser($user1);
        $tenant1->addUser($user2);
        $manager->persist($tenant1);

        $tenant2 = new Tenant();
        $tenant2
            ->setName('test_tenant2')
            ->setBaseurl('test_tenant2')
            ->setDescription('tenant 2');
        $tenant2->addUser($user1);
        $manager->persist($tenant2);

        $tenant3 = new Tenant();
        $tenant3
            ->setName('test_tenant3')
            ->setBaseurl('test_tenant3')
            ->setDescription('tenant 3 sans user');
        $manager->persist($tenant3);

        /** @var RoleRepository $roleRepository */
        $roleRepository = $manager->getRepository(Role::class);
        $role = $roleRepository->findOneBy(['name' => Permission::ROLE_FUNCTIONAL_ADMIN]);

        $accessUser = new AccessUser();
        $accessUser->setUser($user1)
            ->setRole($role)
            ->setTenant($tenant1);
        $manager->persist($accessUser);

        $role = $roleRepository->findOneBy(['name' => Permission::ROLE_FUNCTIONAL_ADMIN]);
        $accessUser = new AccessUser();
        $accessUser->setUser($user2)
            ->setRole($role)
            ->setTenant($tenant1);
        $manager->persist($accessUser);

        $role = $roleRepository->findOneBy(['name' => Permission::ROLE_TECHNICAL_ADMIN]);
        $accessUser = new AccessUser();
        $accessUser->setUser($user1)
            ->setRole($role);
        $manager->persist($accessUser);

        $activity = new Activity();
        $activity->setCreatedUser($user1);
        $activity->setAction('add');
        $activity->setCreated(new DateTime('2023-02-03T15:45:21.456'));
        $activity->setSubject($tenant1->getActivityName());
        $activity->setTenant($tenant1);
        $manager->persist($activity);

        $labelGroup = new LabelGroup();
        $labelGroup->setTenant($tenant1);
        $labelGroup->setName('labelGroup1');
        $manager->persist($labelGroup);

        $label = new Label();
        $label->setTenant($tenant1);
        $label->setLabelGroup($labelGroup);
        $label->setName('test');
        $manager->persist($label);

        $profile1 = new Profile();
        $profile1
            ->setTenant($tenant1)
            ->setIdentifier('profil1')
            ->setName('profil1')
            ->setPublic(true)
            ->setState(VersionableEntityInterface::S_PUBLISHED)
            ->setVersion(1)
        ;
        $manager->persist($profile1);

        $profile2 = new Profile();
        $profile2
            ->setTenant($tenant1)
            ->setIdentifier('profil1')
            ->setName('profil1')
            ->setPublic(true)
            ->setState($profile2->getInitialState())
            ->setVersion(2)
        ;
        $manager->persist($profile2);

        $authority1 = new Authority();
        $authority1
            ->setTenant($tenant1)
            ->setIdentifier('authority1')
            ->setName('authority1')
            ->setDescription('description de authority1')
            ->setMaintenanceStatus('maintenance status de autority1')
            ->setExistDatesFrom(new DateTime('2000-01-01'))
            ->setExistDatesTo(new DateTime('2000-12-31'))
            ->setEntityType('entity_type de authority1')
            ->setLegalStatus('legal_status de authority1')
            ->setPlace('101 Rue de Authority1, 99999 Celeste, Monde')
            ->setFunction('function de authority1')
            ->setOccupation('occupation de authority1')
            ->setMandate('mandate de authority1')
            ->setPublic(true)
            ->setState(VersionableEntityInterface::S_PUBLISHED)
            ->setVersion(1)
            ->addLabel($label)
        ;
        $authority2 = clone $authority1;
        $manager->persist($authority1);

        $file = FileManager::setDataFromPath(__DIR__ . '/../../tests/resources/example.txt');
        $manager->persist($file);

        $authorityFile = new AuthorityFile();
        $authorityFile
            ->setType(AuthorityFile::TYPE_OTHER)
            ->setFile($file)
            ->setAuthority($authority2);
        $manager->persist($authorityFile);

        $authority2
            ->setState(VersionableEntityInterface::S_EDITING)
            ->setVersion(2)
        ;
        $manager->persist($authority2);

        $vocabulary1 = new Vocabulary();
        $vocabulary1
            ->setTenant($tenant1)
            ->setIdentifier('vocabulary1')
            ->setName('vocabulary1')
            ->setDescription('this is vocabulary 1')
            ->setPublic(true)
            ->setState(VersionableEntityInterface::S_PUBLISHED)
            ->setVersion(1)
            ->addLabel($label)
        ;
        $vocabulary2 = clone $vocabulary1;
        $manager->persist($vocabulary1);

        $vocabulary2
            ->setState(VersionableEntityInterface::S_EDITING)
            ->setVersion(2)
        ;
        $manager->persist($vocabulary2);

        $term = new Term();
        $term
            ->setVocabulary($vocabulary2)
            ->setName('term1')
            ->setIdentifier('term1')
            ->setDirectoryName('termDirectory');
        $manager->persist($term);

        $categories = [
            'Commune' => [
                'Affaires scolaires' => [
                    'Administration' => true,
                    'Personnel' => true,
                ],
                'Tourisme' => [
                    'Office du tourisme' => true,
                    'Mise en valeur touristique' => true,
                    'Accueil touristique' => true,
                ],
            ],
            'Département' => [
                'Finances' => [
                    'Pièces comptables et financières' => true,
                ],
                'Ressources humaines' => true,
                'Culture' => true,
                'Cabinet' => true,
                'Urbanisme' => true,
            ],
            'Libriciel' => [
                'Pôle archivage' => [
                    'Asalae' => true,
                ],
                'Pôle administratif, Finances et Moyens Généraux' => [
                    'Parc automobile' => true,
                    'Ressources humaines et paie' => true,
                    'Contrats et facturation' => true,
                    'Design' => true,
                    'Sociétariat' => true,
                ],
                'Pôle Services Clients' => [
                    'Consultants' => true,
                ],
            ],
        ];
        $this->createCategories($categories, $tenant1, $manager);

        $serviceLevel1 = (new ServiceLevel())
            ->setIdentifier('serviceLevel1')
            ->setName('serviceLevel1')
            ->setDescription('Ceci est un niveau de service')
            ->setTenant($tenant1)
            ->setPublic(true)
            ->setVersion(1)
            ->setState(VersionableEntityInterface::S_PUBLISHED);
        $serviceLevel2 = clone $serviceLevel1;
        $manager->persist($serviceLevel1);

        $serviceLevel2
            ->setState(VersionableEntityInterface::S_EDITING)
            ->setVersion(2)
        ;
        $manager->persist($serviceLevel2);

        $filePath = __DIR__ . '/../../tests/resources/test_import.csv';
        $file = FileManager::setDataFromPath($filePath);
        $manager->persist($file);

        $columnTypes = json_encode([
            'col-0' => 'level',
            'col-1' => 'label',
            'col-2' => 'dua_duration',
            'col-3' => 'dua_comment',
            'col-4' => 'final_action',
            'col-5' => 'final_action_comment',
            'col-6' => 'document_support',
            'col-7' => 'access_rule_duration',
            'col-8' => 'access_rule_comment',
            'col-9' => 'comment',
        ]);
        $handle = fopen($filePath, 'r');
        fgets($handle);
        $documentRuleImportSession = new DocumentRuleImportSession();
        $documentRuleImportSession
            ->setTenant($tenant1)
            ->setUser($user1)
            ->setCsvFile($file)
            ->setColumnTypes($columnTypes)
            ->setLastSeekOffset(ftell($handle))
            ->setLastRow(2)
        ;
        fclose($handle);
        $manager->persist($documentRuleImportSession);

        $manager->flush();
    }

    #[Override]
    public static function getGroups(): array
    {
        return ['test'];
    }

    private function createCategories(
        array $categories,
        Tenant $tenant,
        ObjectManager $manager,
        ?Category $parent = null
    ) {
        foreach ($categories as $category => $value) {
            $newCategory = new Category();
            $newCategory->setTenant($tenant);
            $newCategory->setName($category);
            $newCategory->setDescription('');
            $newCategory->setParent($parent);
            $manager->persist($newCategory);
            if (is_array($value)) {
                $this->createCategories($value, $tenant, $manager, $newCategory);
            }
        }
    }
}
