<?php

namespace App\DataFixtures;

use App\Entity\RuleType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Override;

class RuleTypeFixtures extends Fixture implements FixtureGroupInterface
{
    public const array INITIAL_RULE_TYPES = [
        'StorageRule' => [
            'name' => "Gestion de la durée d’utilité courante",
            'description' => "Codes utilisés dans toutes les versions 2.x du SEDA",
        ],
        'AppraisalRule' => [
            'name' => "Gestion de la durée d’utilité administrative",
            'description' => "Codes utilisés dans toutes les versions du SEDA",
        ],
        'AccessRule' => [
            'name' => "Gestion de la communicabilité",
            'description' => "Codes utilisés dans toutes les versions du SEDA",
        ],
        'DisseminationRule' => [
            'name' => "Gestion de la diffusion",
            'description' => "Codes utilisés dans toutes les versions 2.x du SEDA",
        ],
        'ReuseRule' => [
            'name' => "Gestion de la réutilisation",
            'description' => "Codes utilisés dans toutes les versions 2.x du SEDA",
        ],
        'ClassificationRule' => [
            'name' => "Gestion de la classification",
            'description' => "Codes utilisés dans toutes les versions 2.x du SEDA",
        ],
        'HoldRule' => [
            'name' => "Gestion de la durée de gel des archives",
            'description' => "Codes utilisés dans toutes les versions 2.x du SEDA",
        ],
    ];

    #[Override]
    public function load(ObjectManager $manager): void
    {
        $ruleType = new RuleType();
        if ($manager->getRepository($ruleType::class)->count([])) {
            return;
        }
        foreach (self::INITIAL_RULE_TYPES as $identifier => $values) {
            $ruleType = (new RuleType())
                ->setIdentifier($identifier)
                ->setName($values['name'])
                ->setDescription($values['description']);
            $manager->persist($ruleType);
            $this->addReference($identifier, $ruleType);
        }
        $manager->flush();
    }

    #[Override]
    public static function getGroups(): array
    {
        return ['prod', 'recette', 'test'];
    }
}
