<?php

namespace App\Twig\Runtime;

use App\Entity\ActivitySubjectEntityInterface;
use App\Entity\Attribute\ViewSection;
use App\Entity\HaveFilesInterface;
use App\Entity\ViewEntityInterface;
use App\ResultSet\EntityResultSetInterface;
use App\Service\AttributeExtractor;
use DateTimeInterface;
use Exception;
use IntlDateFormatter;
use Locale;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Traversable;
use Twig\Environment;
use Twig\Extension\RuntimeExtensionInterface;
use Twig\Markup;

class ViewExtensionRuntime implements RuntimeExtensionInterface
{
    public const string DEFAULT_TABLE_TITLE = "Informations principales";

    public function __construct(private readonly Environment $twig, private readonly SerializerInterface $serializer)
    {
    }

    public function generate(ViewEntityInterface $entity, string $title = self::DEFAULT_TABLE_TITLE)
    {
        $template = $this->twig->load('view/table.html.twig');
        $content = [];
        foreach ($entity->viewData() as $th => $td) {
            $td = $this->completeTd($entity, $td);
            $content[] = sprintf(
                '<tr><th>%s</th><td>%s</td>',
                $th,
                $td
            );
        }
        $vars = ['title' => $title, 'content' => implode(PHP_EOL, $content)];
        return new Markup($template->render($vars), 'UTF-8');
    }

    public function generateFromNormalized(array $data, string $title = self::DEFAULT_TABLE_TITLE): Markup
    {
        $template = $this->twig->load('view/table.html.twig');
        $content = [];
        foreach ($data as $th => $td) {
            if (is_array($td)) {
                $td = $td
                    ? '<ul><li>' . implode('</li><li>', $td) . '</li></ul>'
                    : null;
            } elseif (is_callable($td)) {
                $td = $td($data);
            }
            $content[] = sprintf(
                '<tr><th>%s</th><td>%s</td>',
                $th,
                $td
            );
        }
        $vars = ['title' => $title, 'content' => implode(PHP_EOL, $content)];

        return new Markup($template->render($vars), 'UTF-8');
    }

    public function generateSmallTable($entity, ?EntityResultSetInterface $entityFileResultSet = null): Markup
    {
        $groups = [];
        $order = [];
        $properties = AttributeExtractor::getPropertiesForAttribute($entity, ViewSection::class);
        $serializedNames = array_map(
            fn (array $value): string => $value[0],
            AttributeExtractor::getPropertiesForAttribute($entity, SerializedName::class)
        );

        foreach ($properties as $property => $args) {
            if (!isset($serializedNames[$property])) {
                throw new Exception(
                    sprintf(
                        'Property "%s" does not have SerializedName attribute in class "%s".',
                        $property,
                        $entity::class
                    )
                );
            }
            // TODO: pour le moment gère juste les méthodes 'get***', prévoir is et has ?
            // cf. Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer()->extractAttributes()
            $groups[$args['section'] ?? $args[0]][] =
                method_exists($entity, $property) ? lcfirst(substr($property, 3)) : $property;
            $order[current($args)][$serializedNames[$property]] = $args['order'] ?? $args[1] ?? INF;
        }

        $contents = [];
        foreach ($groups as $name => $group) {
            $data = $this->serializer->normalize($entity, context: [AbstractNormalizer::ATTRIBUTES => $group]);
            uksort(
                $data,
                fn ($a, $b) => ($order[$name][$a] ?? INF) <=> ($order[$name][$b] ?? INF)
            );
            foreach ($data as $th => $td) {
                if (is_array($td)) {
                    $td = $td
                        ? '<ul><li>' . implode('</li><li>', $td) . '</li></ul>'
                        : null;
                }
                $contents[$name][] = sprintf(
                    '<tr><th>%s</th><td>%s</td>',
                    $th,
                    $td
                );
            }
        }

        $main = implode(PHP_EOL, $contents['main']);
        $other = isset($contents['other']) ? implode(PHP_EOL, $contents['other']) : null;
        unset($contents['main'], $contents['other']);

        $vars = [
            'main' => $main,
            'other' => $other,
            'undefinedSections' => array_map(fn (array $a): string => implode(PHP_EOL, $a), $contents),
            'files' => $entity instanceof HaveFilesInterface ? $entityFileResultSet : null,
            'activities' => $entity instanceof ActivitySubjectEntityInterface
                ? $entity->getActivities()
                : null,
        ];

        $template = $this->twig->load('view/small_table.html.twig');
        return new Markup($template->render($vars), 'UTF-8');
    }

    /**
     * Complète les valeurs du $td par le contenu de $entity
     * @param ViewEntityInterface $entity
     * @param string|callable     $td
     * @return string
     * @throws Exception
     */
    private function completeTd(ViewEntityInterface $entity, $td): string
    {
        if (is_callable($td)) {
            $td = $td($entity);
            if (!str_contains((string) $td, '{')) {
                $td = '{do_not_parse}' . $td;
            }
        } elseif ($td instanceof DateTimeInterface) {
            $td = $this->formatDate($td);
        }
        if (!str_contains((string) $td, '{do_not_parse}')) {
            if (str_contains((string) $td, '{n}')) {
                [$before, $after] = explode('{n}', (string) $td);
                $before = trim($before, '.');
                $after = trim($after, '.');
                $lis = [];
                foreach ($entity[$before] ?? [] as $n => $base) {
                    $path = "$before.$n.$after";
                    $parsed = trim($this->parseTd($entity, $path));
                    if ($parsed) {
                        $lis[] = sprintf('<li>%s</li>', $parsed);
                    }
                }
                $td = sprintf("<ul>%s</ul>\n", implode('', $lis));
            } else {
                $td = $td ? $this->parseTd($entity, $td) : '';
            }
        } else {
            $td = str_replace('{do_not_parse}', '', (string) $td);
        }
        return $td;
    }

    /**
     * Transforme un Foo.bar ou un {{ Foo.bar }} en valeur de l'entité
     * @param ViewEntityInterface $entity
     * @param string              $td
     * @return string
     * @throws Exception
     */
    private function parseTd(ViewEntityInterface $entity, string $td): string
    {
        if (!str_contains($td, '{')) {
            $td = '{' . $td . '}';
        }
        $offset = 0;
        $replaces = [];
        while (($pos = strpos($td, '{', $offset)) !== false) {
            $offset = strpos($td, '}', $pos);
            if ($offset === false) {
                throw new Exception('Missing closing brace');
            }
            $extract = substr($td, $pos + 1, $offset - ($pos + 1));
            $filters = array_map('trim', explode('|', $extract));
            $field = array_shift($filters);

            $ref = $entity;
            foreach (explode('.', $field) as $part) {
                if (!isset($ref[$part])) {
                    $ref = null;
                    break;
                }
                $ref = $ref[$part];
            }
            $value = $ref;
            if ($value instanceof DateTimeInterface) {
                $value = $this->formatDate($value);
            } elseif (is_bool($value)) {
                $value = $value ? "Oui" : "Non";
            } elseif ($value && !in_array('raw', $filters)) {
                $value = htmlspecialchars((string) $value);
            }
            foreach ($filters as $filter) {
                $value = $this->applyFilter($value, $filter);
            }
            $replaces['{' . $extract . '}'] = $this->flatten($value);
        }
        return str_replace(array_keys($replaces), array_values($replaces), $td);
    }

    /**
     * Applique un filtre. ex: ' test '|trim donnera 'test'
     * @return mixed
     */
    private function applyFilter(mixed $value, string $filter)
    {
        $argTokens = [
            T_LNUMBER, T_DNUMBER, T_CONSTANT_ENCAPSED_STRING
        ];
        $func = null;
        $args = [$value];
        foreach (token_get_all('<?php _' . $filter . '; ?>') as $token) {
            if (!$func && is_array($token) && $token[0] === T_STRING) {
                // retire le "_" ajouté au début par protection contre les mots clefs réservés (ex: default)
                $func = substr($token[1], 1);
            } elseif (is_array($token) && $token[0] === T_STRING) {
                switch (strtolower($token[1])) {
                    case 'true':
                        $args[] = true;
                        break;
                    case 'false':
                        $args[] = false;
                        break;
                    case 'null':
                        $args[] = null;
                        break;
                }
            } elseif (is_array($token) && in_array($token[0], $argTokens)) {
                $args[] = trim($token[1], '\'"');
            }
        }
        if (method_exists($this, $func)) {
            $value = call_user_func_array([$this, $func], $args);
        } elseif (function_exists($func)) {
            $value = call_user_func_array($func, $args);
        }
        return $value;
    }

    /**
     * Applati une valeur à multiple dimentions si nécéssaire
     * @param string|array|Traversable $value
     * @return string
     */
    private function flatten($value): string
    {
        if (!is_array($value) && !is_object($value)) {
            return (string)$value;
        }
        $output = '<ul class="flatten-values">';
        foreach ($value as $key => $val) {
            $output .= '<li class="li-row">';
            if (!is_array($val) && !is_object($val)) {
                if ($key === 'password') {
                    $output .= '<span class="key">password</span> <span class="value">*****</span>';
                } elseif (is_string($key)) {
                    $output .= '<span class="key">' . $key
                        . ':</span> <span class="value">' . htmlspecialchars((string) $val) . '</span>';
                } else {
                    $output .= htmlspecialchars((string) $val);
                }
            } else {
                $output .= $this->flatten($val);
            }
            $output .= '</li>';
        }
        $output .= '<ul>';
        return $output;
    }

    public function formatDate(
        DateTimeInterface $date,
        $formatDate = IntlDateFormatter::LONG,
        $formatTime = IntlDateFormatter::MEDIUM,
        $locale = null
    ): string {
        $formatter = new IntlDateFormatter(
            $locale ?: Locale::getDefault(),
            $formatDate,
            $formatTime
        );

        return $formatter->format($date);
    }
}
