<?php

namespace App\Twig\Runtime;

use App\Service\Permission;
use Twig\Extension\RuntimeExtensionInterface;

readonly class PermissionExtensionRuntime implements RuntimeExtensionInterface
{
    public function __construct(
        private Permission $permission,
    ) {
    }

    public function userCanAccess(string $route): bool
    {
        return $this->permission->userCanAccessRoute($route);
    }
}
