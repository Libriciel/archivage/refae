<?php

namespace App\Twig\Runtime;

use Symfony\Component\Mercure\HubRegistry;
use Twig\Environment;
use Twig\Extension\RuntimeExtensionInterface;
use Twig\Markup;

readonly class MercureRuntime implements RuntimeExtensionInterface
{
    public function __construct(
        private Environment $twig,
        private HubRegistry $hubRegistry,
    ) {
    }

    public function mercureSubscibe(array $topics)
    {
        $hubInstance = $this->hubRegistry->getHub();
        $tokenFactory = $hubInstance->getFactory();
        $token = $tokenFactory->create($topics);

        $template = $this->twig->load('mercure/subscribe.js.twig');
        $vars = [
            'token' => $token,
            'topics' => $topics,
        ];
        return new Markup($template->render($vars), 'UTF-8');
    }

    public function mercureCallback(array $topics, string $jsCallback)
    {
        $template = $this->twig->load('mercure/callback.js.twig');
        $vars = [
            'subscribe' => $this->mercureSubscibe($topics),
            'callback' => $jsCallback,
            'topics' => $topics,
        ];
        return new Markup($template->render($vars), 'UTF-8');
    }
}
