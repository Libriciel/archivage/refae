<?php

namespace App\Twig\Runtime;

use App\Entity\User;
use App\Repository\TenantRepository;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Twig\Extension\RuntimeExtensionInterface;

readonly class TenantRuntime implements RuntimeExtensionInterface
{
    private ?User $user;

    public function __construct(
        private RequestStack $requestStack,
        TokenStorageInterface $tokenStorage,
        private TenantRepository $tenants,
    ) {
        $this->user = $tokenStorage->getToken()?->getUser();
    }

    public function getCurrentTenant(?string $attribute = null): mixed
    {
        $request = $this->requestStack->getCurrentRequest();
        $baseurl = $request->get('tenant_url');
        if (!$baseurl) {
            return null;
        }
        $tenants = $this->user ? $this->user->getTenants() : $this->tenants->findAll();
        foreach ($tenants as $tenant) {
            if ($tenant->getBaseurl() === $baseurl) {
                if ($attribute) {
                    return $tenant[$attribute];
                }
                return $tenant;
            }
        }
        return null;
    }
}
