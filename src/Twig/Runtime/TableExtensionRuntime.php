<?php

namespace App\Twig\Runtime;

use App\Entity\Table;
use App\Entity\TableFilters;
use App\Form\TableFiltersType;
use App\Repository\TableRepository;
use App\ResultSet\ResultSetInterface;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;
use Twig\Extension\RuntimeExtensionInterface;
use Twig\Markup;

use function Symfony\Component\String\u;

readonly class TableExtensionRuntime implements RuntimeExtensionInterface
{
    public function __construct(
        private Environment $twig,
        private FormFactoryInterface $factory,
        private RequestStack $resquestStack,
        private RouterInterface $router,
        private TableRepository $tableRepository,
        private EntityManagerInterface $entityManager,
        private Security $security,
        private RequestStack $requestStack,
    ) {
    }

    public function generateFilters(ResultSetInterface $data)
    {
        $template = $this->twig->load('table/filters.html.twig');
        $request = $this->resquestStack->getCurrentRequest();
        $actionUrl = $request->getSchemeAndHttpHost()
            . $request->getBaseUrl()
            . $request->getPathInfo();
        $builder = $this->factory->createNamedBuilder(
            '',
            FormType::class,
            null,
            [
                'action' => $actionUrl,
            ]
        );

        $filters = $data->filters();
        $selectFilters = [];
        foreach ($filters as $query => $params) {
            $selectFilters[] = sprintf(
                '<option value="%s">%s</option>',
                $query,
                $params['label'] ?? $query
            );
            $type = $params['type'] ?? null;
            unset($params['type'], $params['query']);
            $builder->add($query, $type, $params);
        }
        $filterTemplatesForm = $builder->getForm()->createView();

        $currentFilters = [];
        $queryString = $request->getQueryString();
        if ($queryString) {
            foreach (explode('&', urldecode($queryString)) as $queryStr) {
                if (preg_match('/^(.*)\[\d+]=(.*)$/', $queryStr, $m) && isset($filters[$m[1]])) {
                    $currentFilters[] = $queryStr;
                }
            }
        }

        $table = $this->tableRepository->findByDomId($data->id());
        if (!$table) {
            $table = new Table();
            $table->setDomId($data->id());
            $user = $this->security->getUser();
            if ($user) {
                $table->setUserId($user->getUserIdentifier());
            } else {
                $request = $this->requestStack->getCurrentRequest();
                $cookieValue = $request->cookies->get('guest_uuid');
                $table->setGuestToken($cookieValue);
                $table->setGuestLastAccess(new DateTime());
            }
            $this->entityManager->persist($table);
            $this->entityManager->flush();
        }
        $tableFilters = new TableFilters();
        $tableFilters->setTable($table);
        $tableFilters->setFilters(implode('&', $currentFilters));
        $builder = $this->factory->create(
            TableFiltersType::class,
            $tableFilters,
            [
                'action' => $this->router->generate('public_save_filters'),
            ]
        );
        $saveFiltersForm = $builder->createView();

        $vars = [
            'result_set' => $data,
            'base64' => base64_encode(""),
            'options_select_filter' => implode(PHP_EOL, $selectFilters),
            'filter_templates_form' => $filterTemplatesForm,
            'current_query_filters' => $this->getCurrentQueryFilters($request),
            'save_filters_form' => $saveFiltersForm,
            'filters_saves' => $table->getFilters(),
        ];
        return new Markup($template->render($vars), 'UTF-8');
    }

    public function generateTable(ResultSetInterface $data): Markup
    {
        $request = $this->resquestStack->getCurrentRequest();
        $actionUrl = $request->getSchemeAndHttpHost()
            . $request->getBaseUrl()
            . $request->getPathInfo();

        $builder = $this->factory->createNamedBuilder('');
        foreach ($data->filters() as $query => $params) {
            $type = $params['type'] ?? null;
            unset($params['type'], $params['query']);
            $builder->add($query, $type, $params);
        }
        $filtersForm = $builder->getForm()->createView();

        $memory = $data->memory();
        $template = $this->twig->load('table/script.html.twig');
        $fields = $this->appendOrderToFields($data->fields());
        $vars = [
            'table_classes' => $data->options()['class'] ?? '',
            'table_id' => $data->id(),
            'js_object' => $this->getJsTableObject($data->id()),
            'json_data' => json_encode($this->dataToArray($data->data()), JSON_PRETTY_PRINT),
            'json_action' => json_encode($this->formatActions($data->actions()), JSON_PRETTY_PRINT),
            'json_params' => $this->getParamsJson($fields, $data->actions(), $data->params()),
            'json_memory' => (string)$memory,
            'filters_form' => $filtersForm,
            'fields' => $data->fields(),
            'action_url' => $actionUrl,
        ];
        return new Markup($template->render($vars), 'UTF-8');
    }

    /**
     * permet d'obtenir le nom de l'objet javascript selon l'id de la table
     * @param string $id
     * @return string
     */
    public function getJsTableObject(string $id): string
    {
        return u('generated_' . preg_replace('/\W/', '_', $id))->camel()->title();
    }

    /**
     * Assure une utilisation de la méthode toArray() sur toutes les profondeurs
     * sans modification de la structure de données
     */
    private function dataToArray(mixed $data)
    {
        foreach ($data as $key => $value) {
            if ($value instanceof DateTimeInterface) {
                $data[$key] = $value->format(DATE_RFC3339);
            } elseif (is_iterable($value)) {
                $data[$key] = $this->dataToArray($value);
            } elseif (is_resource($value)) {
                rewind($value);
                $data[$key] = stream_get_contents($value);
            }
        }
        return $data;
    }

    /**
     * Permet d'obtenir le json de paramètre à passer au javascript
     * @return string json
     */
    private function getParamsJson(array $fields, array $actions, array $params): string
    {
        $paramsJs = [];

        foreach ($fields as $fieldname => $fieldParams) {
            if (is_int($fieldname)) {
                $fieldname = $fieldParams;
                $fieldParams = [];
            }
            if (!isset($fieldParams['label'])) {
                $fieldParams['label'] = $fieldname;
            }
            $fieldParams = $this->fieldParams($fieldname, (array)$fieldParams);
            $paramsJs['thead'][0][] = $this->fieldValue($fieldname, $fieldParams);
        }

        if ($actions) {
            $paramsJs['thead'][0][] = [
                '_id' => 'actions',
                'label' => 'Actions',
                'title' => '',
                'link' => false,
                'type' => 'th',
                'data-view' => 'column',
                'display' => true,
                'colspan' => 1,
                'target' => 'actions',
                'thead' => $this->actionsToThead($actions),
                'callback' => null,
                'class' => 'action'
            ];
        }
        if (($params['favorites'] ?? false) === true) {
            $params['favorites'] = $this->filterFavoriteLink();
        }

        $paramsJs['tbody'] = $params;
        return json_encode($paramsJs, JSON_PRETTY_PRINT);
    }

    /**
     * Transforme les actions en json exploitable par le javascript
     * @param array $actions
     * @return array
     */
    private function actionsToThead(array $actions): array
    {
        $thead = [];
        foreach ($this->formatActions($actions) as $actionName => $action) {
            $display = !(($action['display'] ?? null) === false
                || ($action['display'] ?? null) === 'none');

            $thead[$actionName] = [
                '_id' => $actionName,
                'label' => false,
                'title' => ($action['title'] ?? null) ?: $actionName,
                'link' => false,
                'type' => 'th',
                'data-view' => 'default',
                'display' => $display,
                'colspan' => 1,
                'target' => $actionName,
                'thead' => null,
                'callback' => ($action['callback'] ?? null),
            ];
        }
        $thead['subactions'] = [
            '_id' => 'subactions',
            'type' => 'th',
            'data-view' => 'default',
            'target' => 'subactions',
        ];

        return $thead;
    }

    /**
     * Assure que les actions ont un identifiant
     * @param $actions
     * @return array
     */
    private function formatActions($actions): array
    {
        foreach ($actions as $key => $action) {
            if (is_numeric($key)) {
                $actionName = 'action_' . $key;
                $actions[$actionName] = $action;
                unset($actions[$key]);
            }
        }
        return $actions;
    }

    /**
     * Donne un champ avec tout ses paramètres
     *
     * '_id' => identifiant unique de l'objet
     * 'label' => Nom qui sera affiché à l'utilisateur
     * 'title' => attribut title de l'objet
     * 'link' => url qui sera inséré dans le 'th' pour le tri sur colonne
     * 'type' => 'th' ou 'td' pour la colonne dans le thead
     * 'data-view' => default, div ou column pour le basculement d'une vue à
     *                l'autre
     * 'display' => colonne affiché ou non par défaut
     * 'colspan' => attribut colspan de la colonne
     * 'target' => Pointeur vers la donnée dans "$results[index]"
     *              ex: Monmodel.field
     * 'thead' => Si la colonne en contient d'autres, les informations seront
     *            contenues ici
     * 'callback' => function javascript qui reçois la valeur de la colonne en
     *               paramètre et renvoi la valeur modifié
     * @param string $fieldname
     * @param array  $params
     * @return array
     */
    private function fieldValue(string $fieldname, array $params): array
    {
        $display = !(($params['display'] ?? null) === false
            || ($params['display'] ?? null) === 'none');
        $label = !isset($params['label']) ? $fieldname : $params['label'];

        $value = [
            '_id' => preg_replace('/\W/', '_', $fieldname),
            'label' => $label,
            'title' => ($params['title'] ?? ''),
            'link' => ($params['link'] ?? false),
            'type' => ($params['type'] ?? 'th'),
            'data-view' => ($params['data-view'] ?? 'default'),
            'display' => $display,
            'displayEval' => ($params['displayEval'] ?? null),
            'colspan' => ($params['colspan'] ?? 1),
            'target' => $params['target'] ?? $fieldname,
            'thead' => ($params['thead'] ?? null),
            'callback' => ($params['callback'] ?? null),
            'style' => ($params['style'] ?? null),
            'class' => ($params['class'] ?? null),
            'filter' => ($params['filter'] ?? null),
        ] + $params;

        if ($value['thead']) {
            $value['thead'] = $this->normalize((array)$value['thead']);
            foreach ($value['thead'] as $subfield => $subparams) {
                $value['thead'][$subfield]
                    = $this->fieldParams($subfield, (array)$subparams);
            }
        }

        return $value;
    }

    /**
     * Permet d'obtenir les paramètres d'un field
     * @param string $fieldname
     * @param array  $params
     * @return array
     */
    private function fieldParams(string $fieldname, array $params): array
    {
        if (!isset($params['label'])) {
            $params['label'] = $fieldname;
        }
        if (isset($params['type'])) {
            switch ($params['type']) {
                case 'date':
                    unset($params['type']);
                    $params['callback'] = 'TableHelper.date("LL")';
                    break;
                case 'datetime':
                    unset($params['type']);
                    $params['callback'] = 'TableHelper.date("LLL")';
                    break;
                case 'bool':
                case 'boolean':
                    unset($params['type']);
                    $params['callback'] = 'TableHelper.boolean("Oui", "Non")';
                    break;
            }
        }
        if (isset($params['thead'])) {
            $params['thead'] = $this->normalize($params['thead']);
            foreach ($params['thead'] as $subFieldname => $subParams) {
                $params['thead'][$subFieldname]
                    = $this->fieldValue($subFieldname, (array)$subParams);
            }
        }

        return $params;
    }

    /**
     * Normalizes an array, and converts it to a standard format.
     * @param array $data List to normalize
     * @return array
     */
    private function normalize(array $data): array
    {
        $keys = array_keys($data);
        $count = count($keys);
        $newList = [];
        for ($i = 0; $i < $count; $i++) {
            if (is_int($keys[$i])) {
                $newList[$data[$keys[$i]]] = null;
            } else {
                $newList[$keys[$i]] = $data[$keys[$i]];
            }
        }
        return $newList;
    }

    private function getCurrentQueryFilters(Request $request): array
    {
        $filters = [];

        // getQueryString() renvoi les paramètres dans le mauvais ordre
        $requestUri = $request->getRequestUri();
        $queryString = str_contains($requestUri, '?')
            ? explode('?', $requestUri)[1]
            : '';

        foreach (explode('&', urldecode($queryString)) as $str) {
            if (!$str) {
                continue;
            }
            [$name, $value] = explode('=', $str, 2);
            if (!preg_match('/^(\w+)\[(\d+)](\[])?$/', $name, $m)) {
                continue;
            }
            $nameIndex = sprintf('%s[%d]', $m[1], $m[2]);
            $multiple = isset($m[3]);
            if ($multiple) {
                $filters[$nameIndex]['field'] = $m[1] . '[]';
                $filters[$nameIndex]['value'][] = $value;
            } else {
                $filters[$nameIndex] = [
                    'field' => $m[1],
                    'value' => $value,
                ];
            }
        }
        return $filters;
    }

    private function appendOrderToFields(array $fields)
    {
        foreach ($fields as $fieldname => $params) {
            if (!empty($params['order'])) {
                $label = $params['label'] ?? $fieldname;
                $fields[$fieldname]['label'] = [
                    'label' => $label,
                    'sort' => $this->sortFieldLink($label, $params['order']),
                ];
            }
        }
        return $fields;
    }

    private function sortFieldLink(string $label, string $order)
    {
        $request = $this->resquestStack->getCurrentRequest();
        $route = $request->get('_route');
        $routeParams = $request->get('_route_params');
        $queryParams = $request->query->all();
        $currentSort = $queryParams['sort'] ?? null;
        $queryParams['direction'] ??= 'asc';
        if ($currentSort === $order) {
            if ($queryParams['direction'] === 'asc') {
                $template = '<a class="sort-asc" href="%s" ' .
                    'title="Trier par %s par ordre décroissant"' .
                    ' data-toggle="tooltip">' .
                    '<span class="sr-only">Trier par %s</span></a>'
                ;
                $queryParams['direction'] = 'desc';
            } else {
                $template = '<a class="sort-desc" href="%s" ' .
                    'title="Trier par %s par ordre croissant"' .
                    ' data-toggle="tooltip">' .
                    '<span class="sr-only">Trier par %s</span></a>'
                ;
                $queryParams['direction'] = 'asc';
            }
        } else {
            $template = '<a class="sortable" href="%s" ' .
                'title="Trier par %s par ordre croissant"' .
                ' data-toggle="tooltip">' .
                '<span class="sr-only">Trier par %s</span></a>'
            ;
            $queryParams['direction'] = 'asc';
        }
        $queryParams['sort'] = $order;
        $url = $this->router->generate($route, $routeParams + $queryParams);
        return sprintf($template, $url, $label, $label);
    }

    private function filterFavoriteLink()
    {
        $request = $this->resquestStack->getCurrentRequest();
        $route = $request->get('_route');
        $routeParams = $request->get('_route_params');
        $queryParams = $request->query->all();
        $template = '<a class="btn btn-link btn-filter" href="%s">' .
            '<i class="fa fa-filter fa-space" aria-hidden="true"></i>' .
            '<span class="sr-only">Filtrer par favoris</span></a>';
        $queryParams['favorite'][0] = '1';
        $url = $this->router->generate($route, $routeParams + $queryParams);
        return sprintf($template, $url);
    }
}
