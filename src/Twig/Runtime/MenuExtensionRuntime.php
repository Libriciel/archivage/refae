<?php

namespace App\Twig\Runtime;

use App\Service\LoggedUser;
use App\Service\Permission;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;
use Twig\Extension\RuntimeExtensionInterface;
use Twig\Markup;

class MenuExtensionRuntime implements RuntimeExtensionInterface
{
    private int $i = 0;
    private ?string $topItems = null;
    public function __construct(
        private readonly Environment $twig,
        private readonly Permission $permission,
        private readonly RouterInterface $router,
        private readonly RequestStack $requestStack,
        private readonly LoggedUser $loggedUser,
    ) {
    }

    public function hasTop(array $data): bool
    {
        if (!isset($this->topItems)) {
            $this->topItems = $this->renderTopItems($data);
        }
        return !empty($this->topItems);
    }

    public function buildTop(array $data)
    {
        $template = $this->twig->load('menu/topnav.html.twig');
        if (!isset($this->topItems)) {
            $this->topItems = $this->renderTopItems($data);
        }
        if (empty($this->topItems)) {
            return '';
        }
        return new Markup($template->render(['lis' => $this->topItems]), 'UTF-8');
    }

    private function renderTopItems(array $directories)
    {
        $request = $this->requestStack->getCurrentRequest();
        $tenantUrl = $request->get('tenant_url');
        $content = [];
        $templateItemgroup = $this->twig->load('menu/topitemgroup.html.twig');
        $templateItem = $this->twig->load('menu/topitem.html.twig');
        $nextIsSeparator = false;
        foreach ($directories as $name => $item) {
            if (is_numeric($name) && preg_match('/-+/', (string) $item)) {
                $nextIsSeparator = true;
                continue;
            }
            $icon = $item['icon'];
            $onclick = $item['onclick'] ?? null;
            $href = $item['href'] ?? null;
            $dataModal = $item['data-modal'] ?? null;
            $dataUrl = $item['data-url'] ?? null;
            $dataTitle = $item['data-title'] ?? null;
            unset(
                $item['icon'],
                $item['onclick'],
                $item['href'],
                $item['acl'],
                $item['data-modal'],
                $item['data-title'],
                $item['data-url']
            );
            if ($href) {
                if (
                    (!$tenantUrl && str_starts_with((string) $href, 'app_'))
                    || ($this->permission->userCanAccessRoute($href) === false)
                ) {
                    continue;
                }
                $href = $this->router->generate($href);
            }
            if ($dataUrl) {
                if (
                    (!$tenantUrl && str_starts_with((string) $dataUrl, 'app_'))
                    || ($this->permission->userCanAccessRoute($dataUrl) === false)
                ) {
                    continue;
                }
                $dataUrl = $this->router->generate($dataUrl);
            }
            if (empty($item)) {
                if ($nextIsSeparator) {
                    $nextIsSeparator = false;
                    if ($content) {
                        $content[] = '<li role="separator" class="divider"></li>';
                    }
                }
                $content[] = $templateItem->render(
                    [
                        'href' => $href,
                        'onclick' => $onclick,
                        'icon' => $icon,
                        'linkname' => $name,
                        'dataModal' => $dataModal,
                        'dataUrl' => $dataUrl,
                        'dataTitle' => $dataTitle,
                    ]
                );
            } else {
                $subcontent = $this->renderTopItems($item);
                if (empty($subcontent)) {
                    continue;
                }
                if ($nextIsSeparator) {
                    $nextIsSeparator = false;
                    $content[] = '<li role="separator" class="divider"></li>';
                }
                $content[] = $templateItemgroup->render(
                    [
                        'id_submenu' => sprintf('nav-submenu%d', $this->i++),
                        'icon' => $icon,
                        'groupname' => $name,
                        'content' => $subcontent,
                    ]
                );
            }
        }
        return implode(PHP_EOL, $content);
    }

    public function buildSide(array $data)
    {
        $template = $this->twig->load('menu/sidenav.html.twig');
        $lis = [];
        foreach ($data as $entity => $directories) {
            $wrap = $this->twig->load('menu/sidewrap.html.twig');
            $url = $directories['_url'];
            $active = $directories['_active'];
            unset($directories['_url'], $directories['_active']);
            $lis[] = $wrap->render(
                [
                    'id_submenu' => sprintf('nav-submenu%d', $this->i++),
                    'entity' => $entity,
                    'url' => $url,
                    'content' => $this->renderSideItems($directories),
                    'active' => $active,
                ]
            );
        }
        $vars = ['lis' => implode(PHP_EOL, $lis)];
        $tenant = $this->loggedUser->tenant();
        $vars['tenant'] = $tenant;
        if ($tenant && $tenant->getConfiguration()) {
            $vars['tenant_configuration'] = $tenant->getConfiguration();
        }

        return new Markup($template->render($vars), 'UTF-8');
    }

    private function renderSideItems(array $directories)
    {
        $content = [];
        $templateItemgroup = $this->twig->load('menu/sideitemgroup.html.twig');
        $templateItem = $this->twig->load('menu/sideitem.html.twig');
        foreach ($directories as $group => $item) {
            if (count($item) === 2 && isset($item['_url']) && isset($item['_active'])) {
                $active = $item['_active'];
                $item = $item['_url'];
            }
            if (is_array($item)) {
                $url = $item['_url'];
                $active = $item['_active'];
                unset($item['_url'], $item['_active']);
                $content[] = $templateItemgroup->render(
                    [
                        'id_submenu' => sprintf('nav-submenu%d', $this->i++),
                        'url' => $url,
                        'dirname' => $group,
                        'content' => $this->renderSideItems($item),
                        'active' => $active,
                    ]
                );
            } else {
                $content[] = $templateItem->render(
                    [
                        'url' => $item,
                        'dirname' => $group,
                        'active' => $active ?? false,
                    ]
                );
            }
        }
        return implode(PHP_EOL, $content);
    }
}
