<?php

namespace App\Twig\Extension;

use App\Twig\Runtime\ViewExtensionRuntime;
use Override;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class ViewExtension extends AbstractExtension
{
    #[Override]
    public function getFilters(): array
    {
        return [
            new TwigFilter('format_date', [ViewExtensionRuntime::class, 'formatDate']),
        ];
    }

    #[Override]
    public function getFunctions(): array
    {
        return [
            new TwigFunction('render_view_table_normalized', [ViewExtensionRuntime::class, 'generateFromNormalized']),
            new TwigFunction('render_view_table', [ViewExtensionRuntime::class, 'generate']),
            new TwigFunction('render_view_small_table', [ViewExtensionRuntime::class, 'generateSmallTable']),
        ];
    }
}
