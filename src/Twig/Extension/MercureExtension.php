<?php

namespace App\Twig\Extension;

use App\Twig\Runtime\MercureRuntime;
use Override;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class MercureExtension extends AbstractExtension
{
    #[Override]
    public function getFunctions(): array
    {
        return [
            new TwigFunction('mercure_subscribe', [MercureRuntime::class, 'mercureSubscibe']),
            new TwigFunction('mercure_callback', [MercureRuntime::class, 'mercureCallback']),
        ];
    }
}
