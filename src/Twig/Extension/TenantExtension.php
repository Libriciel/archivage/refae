<?php

namespace App\Twig\Extension;

use App\Twig\Runtime\TenantRuntime;
use Override;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TenantExtension extends AbstractExtension
{
    #[Override]
    public function getFunctions(): array
    {
        return [
            new TwigFunction('current_tenant', [TenantRuntime::class, 'getCurrentTenant']),
        ];
    }
}
