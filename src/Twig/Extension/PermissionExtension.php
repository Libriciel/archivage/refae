<?php

namespace App\Twig\Extension;

use App\Twig\Runtime\PermissionExtensionRuntime;
use Override;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class PermissionExtension extends AbstractExtension
{
    #[Override]
    public function getFunctions(): array
    {
        return [
            new TwigFunction('can_access', [PermissionExtensionRuntime::class, 'userCanAccess']),
        ];
    }
}
