<?php

namespace App\Twig\Extension;

use App\Twig\Runtime\TableExtensionRuntime;
use Override;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TableExtension extends AbstractExtension
{
    #[Override]
    public function getFunctions(): array
    {
        return [
            new TwigFunction('generate_filters', [TableExtensionRuntime::class, 'generateFilters']),
            new TwigFunction('generate_table', [TableExtensionRuntime::class, 'generateTable']),
        ];
    }
}
