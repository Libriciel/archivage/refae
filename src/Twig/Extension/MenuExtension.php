<?php

namespace App\Twig\Extension;

use App\Twig\Runtime\MenuExtensionRuntime;
use Override;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class MenuExtension extends AbstractExtension
{
    #[Override]
    public function getFunctions(): array
    {
        return [
            new TwigFunction('menu_has_top', [MenuExtensionRuntime::class, 'hasTop']),
            new TwigFunction('menu_build_top', [MenuExtensionRuntime::class, 'buildTop']),
            new TwigFunction('menu_build_side', [MenuExtensionRuntime::class, 'buildSide']),
        ];
    }
}
