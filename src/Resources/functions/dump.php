<?php

use Symfony\Component\VarDumper\Caster\ReflectionCaster;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;

if (!function_exists('dforcelog')) {
    /**
     * Exporte le debug dans le logs/debug.log (fonctionne même sans debug => true)
     * @param mixed ...$args debug les arguments
     * @return mixed 1er argument
     */
    function dforcelog(mixed ...$args)
    {
        $trace = debug_backtrace();
        $location = [
            'line' => $trace[0]['line'] ?? '',
            'file' => $trace[0]['file'] ?? '',
        ];

        $cloner = new VarCloner();
        $cloner->addCasters(ReflectionCaster::UNSET_CLOSURE_FILE_INFO);
        $dumper = new CliDumper();
        $template = <<<TEXT
%s
########## DEBUG ##########
%s
###########################

TEXT;
        $lineInfo = sprintf('%s (line %s)', $location['file'], $location['line']);
        foreach ($args as $arg) {
            $var = $cloner->cloneVar($arg);
            $exportedVar = $dumper->dump($var, true);

            $str = sprintf($template, $lineInfo, $exportedVar);
            file_put_contents(
                dirname(__DIR__, 3) . '/var/log/debug.log',
                $str,
                FILE_APPEND
            );
        }
        return current($args);
    }
}
