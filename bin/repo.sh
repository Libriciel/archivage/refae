#!/bin/bash
# shellcheck disable=SC2164

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "$DIR/.."
cd vendor/libriciel/asalae-assets || exit 1
pwd
git checkout master
git remote set-url composer git@gitlab.libriciel.fr:libriciel/pole-archivage/asalae/asalae-assets.git
git remote set-url origin git@gitlab.libriciel.fr:libriciel/pole-archivage/asalae/asalae-assets.git
git pull
