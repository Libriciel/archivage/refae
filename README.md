# Refae (référentiel archivage)
[![License](https://img.shields.io/badge/license-AGPL-blue)](https://www.gnu.org/licenses/agpl-3.0.txt)
[![build status](https://gitlab.libriciel.fr/libriciel/pole-archivage/refae/refae/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/libriciel/pole-archivage/refae/refae/pipelines/latest)
[![coverage report](https://gitlab.libriciel.fr/libriciel/pole-archivage/refae/refae/badges/master/coverage.svg)](https://asalae-master.dev.libriciel.eu/coverage/asalae2/index.html)

Dépendances:

| Projet                                                                                 | Build                                                                                                                                                                                                           | Coverage                                                                                                                                                                                         |
|----------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [asalae-assets](https://gitlab.libriciel.fr/Archivage/asalae/asalae-assets)            | [![build status](https://gitlab.libriciel.fr/Archivage/asalae/asalae-assets/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/Archivage/asalae/asalae-assets/pipelines/latest)                           | [![coverage report](https://gitlab.libriciel.fr/Archivage/asalae/asalae-assets/badges/master/coverage.svg)](https://asalae-master.dev.libriciel.eu/coverage/asalae-assets/index.html)            |

