# Changelog

## [2.0.0] - 07/06/2024

### Nouveautés

- multi-tenant : possibilité d'ajouter des tenants pour les projets de mutualisation
- utilisateurs : pouvant être liés à plusieurs tenants et pouvant s'authentifier en LDAP et OpenID
- rôles utilisateurs : administrateur fonctionnel, lecteur, webservice requêteur, administrateur technique
- notices d'autorité : déclaration des services versants et producteurs (référentiel archivage)
- accords de versement : accords de versements entre les services et le service d'Archives (référentiel archivage)
- profils d'archives : profils utilisés dans le SAE (référentiel archivage)
- vocabulaires contrôlés : liste des termes employés pour l'indexation des archives (référentiel archivage)
- niveaux de service : niveaux de service déclarés dans le SAE (référentiel archivage)
- règles de gestion du SEDA 2.0 : référentiel des règles de gestion à utiliser pour les transferts en SEDA 2.x (référentiel archivage)
- nouveaux attributs : caractère public ou privé des règles, étiquettes 
- fonctions d'export et d'import des tenants, des règles documentaires et des éléments du référentiel archivage
- API : accès en lecture aux règles documentaires et aux éléments du référentiel archivage au format API-PLATEFORM

### Évolutions

- règles documentaires : liens vers le référentiel archivage, export et import,
- règles documentaires : fonctions d'export et d'import
- règles documentaires : services gestionnaires, dossiers et sous-dossiers remplacés par les catégories
