#!/bin/bash

# initialisation d'un nouveau volume
if [ ! -f /etc/apache2/sites-available/refae.conf ]; then
    cp /data-entrypoint/refae.conf /etc/apache2/sites-available/refae.conf
    sed -i'' -E \
          -e "s/\%\{HTTP_HOST\}/${HTTP_HOST}/" \
          /etc/apache2/sites-available/refae.conf
fi
if [ ! -f /etc/apache2/ssl/server.crt ]; then
    cp /data-entrypoint/server.crt /etc/apache2/ssl/server.crt
fi
if [ ! -f /etc/apache2/ssl/server.key ]; then
    cp /data-entrypoint/server.key /etc/apache2/ssl/server.key
fi

# Copie des fichiers de l'image vers le volume
cd /var/www/refae/ || exit 1
xargs -I % sh -c 'rm -rf "/var/www/refae-volume/%" && cp -R "%" "/var/www/refae-volume/" 2>/dev/null' < files_to_override_on_init.txt
cd /var/www/refae-volume/ || exit 1
mkdir -p /var/www/refae-volume/tmp /var/www/refae-volume/var

ENV_FILE=/var/www/refae-volume/.env.local
if [ ! -f "${ENV_FILE}" ]; then
  KEY1=$(php -r 'echo base64_encode(openssl_random_pseudo_bytes(32));')
  KEY2=$(php -r 'echo base64_encode(openssl_random_pseudo_bytes(64));')
    echo "
API_MAX_PER_PAGE=\"${API_MAX_PER_PAGE}\"
DATABASE_URL=\"postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_DBPORT}/${POSTGRES_DB}?serverVersion=15&charset=utf8\"
DEBUG=\"${DEBUG}\"
DEFAULT_FILE_RETENTION_COUNT=\"${DEFAULT_FILE_RETENTION_COUNT}\"
DEFAULT_LOGIN_BACKGROUND_COLOR=\"${DEFAULT_LOGIN_BACKGROUND_COLOR}\"
DEFAULT_LOGIN_LOGO=\"${DEFAULT_LOGIN_LOGO}\"
DEFAULT_LOGIN_TEXT=\"${DEFAULT_LOGIN_TEXT}\"
DEFAULT_LOGIN_TEXT_STYLE=\"${DEFAULT_LOGIN_TEXT_STYLE}\"
DEFAULT_MAX_FILESIZE=\"${DEFAULT_MAX_FILESIZE}\"
ENC_KEY1=\"${KEY1}\"
ENC_KEY2=\"${KEY2}\"
HTTP_BASEURL=\"${HTTP_BASEURL:-}\"
HTTP_HOST=\"${HTTP_HOST}\"
HTTP_PORT=\"${HTTP_PORT:-}\"
HTTP_PROTOCOL=\"${HTTP_PROTOCOL:-}\"
IP_ENABLE_WHITELIST=\"${IP_ENABLE_WHITELIST}\"
IP_WHITELIST=\"${IP_WHITELIST}\"
MAILER_DSN=\"${MAILER_DSN}\"
MAILER_FROM=\"${MAILER_FROM}\"
MEILI_MASTER_KEY=\"${MEILI_MASTER_KEY}\"
MERCURE_JWT_SECRET=\"${MERCURE_JWT_SECRET}\"
MERCURE_PORT_3000=\"${MERCURE_PORT_3000}\"
MERCURE_PUBLIC_URL=\"${MERCURE_PUBLIC_URL}\"
MERCURE_TLS=\"${MERCURE_TLS}\"
MIN_PASSWORD_COMPLEXITY=\"${MIN_PASSWORD_COMPLEXITY}\"
RATE_LIMITER_ANONYMOUS_AMOUNT=\"${RATE_LIMITER_ANONYMOUS_AMOUNT}\"
RATE_LIMITER_ANONYMOUS_INTERVAL=\"${RATE_LIMITER_ANONYMOUS_INTERVAL}\"
RATE_LIMITER_ANONYMOUS_LIMIT=\"${RATE_LIMITER_ANONYMOUS_LIMIT}\"
RATE_LIMITER_AUTHENTICATED_AMOUNT=\"${RATE_LIMITER_AUTHENTICATED_AMOUNT}\"
RATE_LIMITER_AUTHENTICATED_INTERVAL=\"${RATE_LIMITER_AUTHENTICATED_INTERVAL}\"
RATE_LIMITER_AUTHENTICATED_LIMIT=\"${RATE_LIMITER_AUTHENTICATED_LIMIT}\"
RATE_LIMITER_LOGIN_AMOUNT=\"${RATE_LIMITER_LOGIN_AMOUNT}\"
RATE_LIMITER_LOGIN_INTERVAL=\"${RATE_LIMITER_LOGIN_INTERVAL}\"
RATE_LIMITER_LOGIN_LIMIT=\"${RATE_LIMITER_LOGIN_LIMIT}\"
TABLE_PAGE_LIMIT=\"${TABLE_PAGE_LIMIT}\"
TIMEZONE=\"${TZ}\"
" > "$ENV_FILE"
  chown "${APACHE_UID}:${APACHE_GID}" "${ENV_FILE}"
  chmod 600 "${ENV_FILE}"
  bin/console secret:generate

  # INITIALISATION BDD
  bin/console doc:mig:mig -n
  bin/console doc:fix:load --group prod --append -n
  bin/console app:update

  # INSTALLATION 1er admin tech
  if [[ ("${INIT_FIRST_ADMIN_TECH}" == "1" || "${INIT_FIRST_ADMIN_TECH}" == "true") && \
        -n "${INIT_FIRST_ADMIN_TECH_USERNAME}" && \
        -n "${INIT_FIRST_ADMIN_TECH_EMAIL}" ]]; then
      bin/console app:init:first-admin "${INIT_FIRST_ADMIN_TECH_USERNAME}" "${INIT_FIRST_ADMIN_TECH_NAME}" "${INIT_FIRST_ADMIN_TECH_EMAIL}"
  fi
  CAN_INIT_USER="0"
  if [[ ("${INIT_FIRST_TENANT}" == "1" || "${INIT_FIRST_TENANT}" == "true") && \
        -n "${INIT_FIRST_TENANT_NAME}" && \
        -n "${INIT_FIRST_TENANT_BASEURL}" ]]; then
      bin/console app:init:first-tenant "${INIT_FIRST_TENANT_NAME}" "${INIT_FIRST_TENANT_BASEURL}"
      CAN_INIT_USER="1"
  fi
  if [[ ("${INIT_FIRST_TENANT_USER}" == "1" || "${INIT_FIRST_TENANT_USER}" == "true") && \
        "${CAN_INIT_USER}" == "1" && \
        -n "${INIT_FIRST_TENANT_USER_USERNAME}" && \
        -n "${INIT_FIRST_TENANT_USER_EMAIL}" ]]; then
      bin/console app:init:first-user "${INIT_FIRST_TENANT_USER_USERNAME}" "${INIT_FIRST_TENANT_USER_NAME}" "${INIT_FIRST_TENANT_USER_EMAIL}"
      CAN_INIT_USER=1
  fi

else
  # Modifier DATABASE_URL et LOCK_DSN
  sed -i -E '
      s/^(DATABASE_URL="[^"]*)@db:([^"]*")$/\1@refae_db:\2/
      s/^(LOCK_DSN="[^"]*)@db:([^"]*")$/\1@refae_db:\2/
  ' .env.local
  # Modifier MAILER_DSN si sa valeur est exactement "smtp://mailer:1025"
  sed -i 's/^MAILER_DSN="smtp:\/\/mailer:1025"$/MAILER_DSN="smtp:\/\/refae_mailer:1025"/' .env.local

  declare -A ENV_VARS=(
    ["API_MAX_PER_PAGE"]="${API_MAX_PER_PAGE}"
    ["DEBUG"]="${DEBUG}"
    ["DEFAULT_FILE_RETENTION_COUNT"]="${DEFAULT_FILE_RETENTION_COUNT}"
    ["DEFAULT_LOGIN_BACKGROUND_COLOR"]="${DEFAULT_LOGIN_BACKGROUND_COLOR}"
    ["DEFAULT_LOGIN_LOGO"]="${DEFAULT_LOGIN_LOGO}"
    ["DEFAULT_LOGIN_TEXT"]="${DEFAULT_LOGIN_TEXT}"
    ["DEFAULT_LOGIN_TEXT_STYLE"]="${DEFAULT_LOGIN_TEXT_STYLE}"
    ["DEFAULT_MAX_FILESIZE"]="${DEFAULT_MAX_FILESIZE}"
    ["HTTP_BASEURL"]="${HTTP_BASEURL}"
    ["HTTP_HOST"]="${HTTP_HOST}"
    ["HTTP_PORT"]="${HTTP_PORT}"
    ["HTTP_PROTOCOL"]="${HTTP_PROTOCOL}"
    ["IP_ENABLE_WHITELIST"]="${IP_ENABLE_WHITELIST}"
    ["IP_WHITELIST"]="${IP_WHITELIST}"
    ["MAILER_DSN"]="${MAILER_DSN}"
    ["MAILER_FROM"]="${MAILER_FROM}"
    ["MEILI_MASTER_KEY"]="${MEILI_MASTER_KEY}"
    ["MERCURE_JWT_SECRET"]="${MERCURE_JWT_SECRET}"
    ["MERCURE_PUBLIC_URL"]="${MERCURE_PUBLIC_URL}"
    ["MIN_PASSWORD_COMPLEXITY"]="${MIN_PASSWORD_COMPLEXITY}"
    ["RATE_LIMITER_ANONYMOUS_AMOUNT"]="${RATE_LIMITER_ANONYMOUS_AMOUNT}"
    ["RATE_LIMITER_ANONYMOUS_INTERVAL"]="${RATE_LIMITER_ANONYMOUS_INTERVAL}"
    ["RATE_LIMITER_ANONYMOUS_LIMIT"]="${RATE_LIMITER_ANONYMOUS_LIMIT}"
    ["RATE_LIMITER_AUTHENTICATED_AMOUNT"]="${RATE_LIMITER_AUTHENTICATED_AMOUNT}"
    ["RATE_LIMITER_AUTHENTICATED_INTERVAL"]="${RATE_LIMITER_AUTHENTICATED_INTERVAL}"
    ["RATE_LIMITER_AUTHENTICATED_LIMIT"]="${RATE_LIMITER_AUTHENTICATED_LIMIT}"
    ["RATE_LIMITER_LOGIN_AMOUNT"]="${RATE_LIMITER_LOGIN_AMOUNT}"
    ["RATE_LIMITER_LOGIN_INTERVAL"]="${RATE_LIMITER_LOGIN_INTERVAL}"
    ["RATE_LIMITER_LOGIN_LIMIT"]="${RATE_LIMITER_LOGIN_LIMIT}"
    ["TABLE_PAGE_LIMIT"]="${TABLE_PAGE_LIMIT}"
    ["TIMEZONE"]="${TZ}"
  )

  for var_name in "${!ENV_VARS[@]}"; do
      if ! grep -q "^${var_name}=" "${ENV_FILE}"; then
          echo "${var_name}=${ENV_VARS[$var_name]}" >> "${ENV_FILE}"
      else
          sed -i'' -E -e "s~^${var_name}=.*~${var_name}=\"${ENV_VARS[$var_name]}\"~" "${ENV_FILE}"
      fi
  done

  echo "Les nouvelles valeurs des variables ont été mises à jour dans ${ENV_FILE}."
  bin/console doc:fix:load --group prod --append -n
  bin/console app:update
fi

chown "${APACHE_UID}:${APACHE_GID}" -R /etc/apache2/

rm /var/www/refae-volume/tmp/* -Rf
chown "${APACHE_UID}:${APACHE_GID}" /var/www/refae-volume/tmp/
rm /var/www/refae-volume/var/cache/prod/* -Rf
bin/console c:c
chown "${APACHE_UID}:${APACHE_GID}" /var/www/refae-volume/var/ -R

echo "initialization completed"
