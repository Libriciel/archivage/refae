--
-- PostgreSQL database dump
--

-- Dumped from database version 15.3 (Debian 15.3-1.pgdg120+1)
-- Dumped by pg_dump version 15.3 (Debian 15.3-1.pgdg120+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: admin_event_entity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.admin_event_entity (
    id character varying(36) NOT NULL,
    admin_event_time bigint,
    realm_id character varying(255),
    operation_type character varying(255),
    auth_realm_id character varying(255),
    auth_client_id character varying(255),
    auth_user_id character varying(255),
    ip_address character varying(255),
    resource_path character varying(2550),
    representation text,
    error character varying(255),
    resource_type character varying(64)
);


ALTER TABLE public.admin_event_entity OWNER TO postgres;

--
-- Name: associated_policy; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.associated_policy (
    policy_id character varying(36) NOT NULL,
    associated_policy_id character varying(36) NOT NULL
);


ALTER TABLE public.associated_policy OWNER TO postgres;

--
-- Name: authentication_execution; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.authentication_execution (
    id character varying(36) NOT NULL,
    alias character varying(255),
    authenticator character varying(36),
    realm_id character varying(36),
    flow_id character varying(36),
    requirement integer,
    priority integer,
    authenticator_flow boolean DEFAULT false NOT NULL,
    auth_flow_id character varying(36),
    auth_config character varying(36)
);


ALTER TABLE public.authentication_execution OWNER TO postgres;

--
-- Name: authentication_flow; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.authentication_flow (
    id character varying(36) NOT NULL,
    alias character varying(255),
    description character varying(255),
    realm_id character varying(36),
    provider_id character varying(36) DEFAULT 'basic-flow'::character varying NOT NULL,
    top_level boolean DEFAULT false NOT NULL,
    built_in boolean DEFAULT false NOT NULL
);


ALTER TABLE public.authentication_flow OWNER TO postgres;

--
-- Name: authenticator_config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.authenticator_config (
    id character varying(36) NOT NULL,
    alias character varying(255),
    realm_id character varying(36)
);


ALTER TABLE public.authenticator_config OWNER TO postgres;

--
-- Name: authenticator_config_entry; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.authenticator_config_entry (
    authenticator_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.authenticator_config_entry OWNER TO postgres;

--
-- Name: broker_link; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.broker_link (
    identity_provider character varying(255) NOT NULL,
    storage_provider_id character varying(255),
    realm_id character varying(36) NOT NULL,
    broker_user_id character varying(255),
    broker_username character varying(255),
    token text,
    user_id character varying(255) NOT NULL
);


ALTER TABLE public.broker_link OWNER TO postgres;

--
-- Name: client; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client (
    id character varying(36) NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    full_scope_allowed boolean DEFAULT false NOT NULL,
    client_id character varying(255),
    not_before integer,
    public_client boolean DEFAULT false NOT NULL,
    secret character varying(255),
    base_url character varying(255),
    bearer_only boolean DEFAULT false NOT NULL,
    management_url character varying(255),
    surrogate_auth_required boolean DEFAULT false NOT NULL,
    realm_id character varying(36),
    protocol character varying(255),
    node_rereg_timeout integer DEFAULT 0,
    frontchannel_logout boolean DEFAULT false NOT NULL,
    consent_required boolean DEFAULT false NOT NULL,
    name character varying(255),
    service_accounts_enabled boolean DEFAULT false NOT NULL,
    client_authenticator_type character varying(255),
    root_url character varying(255),
    description character varying(255),
    registration_token character varying(255),
    standard_flow_enabled boolean DEFAULT true NOT NULL,
    implicit_flow_enabled boolean DEFAULT false NOT NULL,
    direct_access_grants_enabled boolean DEFAULT false NOT NULL,
    always_display_in_console boolean DEFAULT false NOT NULL
);


ALTER TABLE public.client OWNER TO postgres;

--
-- Name: client_attributes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_attributes (
    client_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value text
);


ALTER TABLE public.client_attributes OWNER TO postgres;

--
-- Name: client_auth_flow_bindings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_auth_flow_bindings (
    client_id character varying(36) NOT NULL,
    flow_id character varying(36),
    binding_name character varying(255) NOT NULL
);


ALTER TABLE public.client_auth_flow_bindings OWNER TO postgres;

--
-- Name: client_initial_access; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_initial_access (
    id character varying(36) NOT NULL,
    realm_id character varying(36) NOT NULL,
    "timestamp" integer,
    expiration integer,
    count integer,
    remaining_count integer
);


ALTER TABLE public.client_initial_access OWNER TO postgres;

--
-- Name: client_node_registrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_node_registrations (
    client_id character varying(36) NOT NULL,
    value integer,
    name character varying(255) NOT NULL
);


ALTER TABLE public.client_node_registrations OWNER TO postgres;

--
-- Name: client_scope; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_scope (
    id character varying(36) NOT NULL,
    name character varying(255),
    realm_id character varying(36),
    description character varying(255),
    protocol character varying(255)
);


ALTER TABLE public.client_scope OWNER TO postgres;

--
-- Name: client_scope_attributes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_scope_attributes (
    scope_id character varying(36) NOT NULL,
    value character varying(2048),
    name character varying(255) NOT NULL
);


ALTER TABLE public.client_scope_attributes OWNER TO postgres;

--
-- Name: client_scope_client; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_scope_client (
    client_id character varying(255) NOT NULL,
    scope_id character varying(255) NOT NULL,
    default_scope boolean DEFAULT false NOT NULL
);


ALTER TABLE public.client_scope_client OWNER TO postgres;

--
-- Name: client_scope_role_mapping; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_scope_role_mapping (
    scope_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE public.client_scope_role_mapping OWNER TO postgres;

--
-- Name: client_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_session (
    id character varying(36) NOT NULL,
    client_id character varying(36),
    redirect_uri character varying(255),
    state character varying(255),
    "timestamp" integer,
    session_id character varying(36),
    auth_method character varying(255),
    realm_id character varying(255),
    auth_user_id character varying(36),
    current_action character varying(36)
);


ALTER TABLE public.client_session OWNER TO postgres;

--
-- Name: client_session_auth_status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_session_auth_status (
    authenticator character varying(36) NOT NULL,
    status integer,
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_auth_status OWNER TO postgres;

--
-- Name: client_session_note; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_session_note (
    name character varying(255) NOT NULL,
    value character varying(255),
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_note OWNER TO postgres;

--
-- Name: client_session_prot_mapper; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_session_prot_mapper (
    protocol_mapper_id character varying(36) NOT NULL,
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_prot_mapper OWNER TO postgres;

--
-- Name: client_session_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_session_role (
    role_id character varying(255) NOT NULL,
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_role OWNER TO postgres;

--
-- Name: client_user_session_note; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_user_session_note (
    name character varying(255) NOT NULL,
    value character varying(2048),
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_user_session_note OWNER TO postgres;

--
-- Name: component; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.component (
    id character varying(36) NOT NULL,
    name character varying(255),
    parent_id character varying(36),
    provider_id character varying(36),
    provider_type character varying(255),
    realm_id character varying(36),
    sub_type character varying(255)
);


ALTER TABLE public.component OWNER TO postgres;

--
-- Name: component_config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.component_config (
    id character varying(36) NOT NULL,
    component_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(4000)
);


ALTER TABLE public.component_config OWNER TO postgres;

--
-- Name: composite_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.composite_role (
    composite character varying(36) NOT NULL,
    child_role character varying(36) NOT NULL
);


ALTER TABLE public.composite_role OWNER TO postgres;

--
-- Name: credential; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.credential (
    id character varying(36) NOT NULL,
    salt bytea,
    type character varying(255),
    user_id character varying(36),
    created_date bigint,
    user_label character varying(255),
    secret_data text,
    credential_data text,
    priority integer
);


ALTER TABLE public.credential OWNER TO postgres;

--
-- Name: databasechangelog; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255),
    deployment_id character varying(10)
);


ALTER TABLE public.databasechangelog OWNER TO postgres;

--
-- Name: databasechangeloglock; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE public.databasechangeloglock OWNER TO postgres;

--
-- Name: default_client_scope; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.default_client_scope (
    realm_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL,
    default_scope boolean DEFAULT false NOT NULL
);


ALTER TABLE public.default_client_scope OWNER TO postgres;

--
-- Name: event_entity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.event_entity (
    id character varying(36) NOT NULL,
    client_id character varying(255),
    details_json character varying(2550),
    error character varying(255),
    ip_address character varying(255),
    realm_id character varying(255),
    session_id character varying(255),
    event_time bigint,
    type character varying(255),
    user_id character varying(255)
);


ALTER TABLE public.event_entity OWNER TO postgres;

--
-- Name: fed_user_attribute; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fed_user_attribute (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36),
    value character varying(2024)
);


ALTER TABLE public.fed_user_attribute OWNER TO postgres;

--
-- Name: fed_user_consent; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fed_user_consent (
    id character varying(36) NOT NULL,
    client_id character varying(255),
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36),
    created_date bigint,
    last_updated_date bigint,
    client_storage_provider character varying(36),
    external_client_id character varying(255)
);


ALTER TABLE public.fed_user_consent OWNER TO postgres;

--
-- Name: fed_user_consent_cl_scope; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fed_user_consent_cl_scope (
    user_consent_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL
);


ALTER TABLE public.fed_user_consent_cl_scope OWNER TO postgres;

--
-- Name: fed_user_credential; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fed_user_credential (
    id character varying(36) NOT NULL,
    salt bytea,
    type character varying(255),
    created_date bigint,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36),
    user_label character varying(255),
    secret_data text,
    credential_data text,
    priority integer
);


ALTER TABLE public.fed_user_credential OWNER TO postgres;

--
-- Name: fed_user_group_membership; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fed_user_group_membership (
    group_id character varying(36) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE public.fed_user_group_membership OWNER TO postgres;

--
-- Name: fed_user_required_action; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fed_user_required_action (
    required_action character varying(255) DEFAULT ' '::character varying NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE public.fed_user_required_action OWNER TO postgres;

--
-- Name: fed_user_role_mapping; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fed_user_role_mapping (
    role_id character varying(36) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE public.fed_user_role_mapping OWNER TO postgres;

--
-- Name: federated_identity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.federated_identity (
    identity_provider character varying(255) NOT NULL,
    realm_id character varying(36),
    federated_user_id character varying(255),
    federated_username character varying(255),
    token text,
    user_id character varying(36) NOT NULL
);


ALTER TABLE public.federated_identity OWNER TO postgres;

--
-- Name: federated_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.federated_user (
    id character varying(255) NOT NULL,
    storage_provider_id character varying(255),
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.federated_user OWNER TO postgres;

--
-- Name: group_attribute; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.group_attribute (
    id character varying(36) DEFAULT 'sybase-needs-something-here'::character varying NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255),
    group_id character varying(36) NOT NULL
);


ALTER TABLE public.group_attribute OWNER TO postgres;

--
-- Name: group_role_mapping; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.group_role_mapping (
    role_id character varying(36) NOT NULL,
    group_id character varying(36) NOT NULL
);


ALTER TABLE public.group_role_mapping OWNER TO postgres;

--
-- Name: identity_provider; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.identity_provider (
    internal_id character varying(36) NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    provider_alias character varying(255),
    provider_id character varying(255),
    store_token boolean DEFAULT false NOT NULL,
    authenticate_by_default boolean DEFAULT false NOT NULL,
    realm_id character varying(36),
    add_token_role boolean DEFAULT true NOT NULL,
    trust_email boolean DEFAULT false NOT NULL,
    first_broker_login_flow_id character varying(36),
    post_broker_login_flow_id character varying(36),
    provider_display_name character varying(255),
    link_only boolean DEFAULT false NOT NULL
);


ALTER TABLE public.identity_provider OWNER TO postgres;

--
-- Name: identity_provider_config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.identity_provider_config (
    identity_provider_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.identity_provider_config OWNER TO postgres;

--
-- Name: identity_provider_mapper; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.identity_provider_mapper (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    idp_alias character varying(255) NOT NULL,
    idp_mapper_name character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.identity_provider_mapper OWNER TO postgres;

--
-- Name: idp_mapper_config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.idp_mapper_config (
    idp_mapper_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.idp_mapper_config OWNER TO postgres;

--
-- Name: keycloak_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.keycloak_group (
    id character varying(36) NOT NULL,
    name character varying(255),
    parent_group character varying(36) NOT NULL,
    realm_id character varying(36)
);


ALTER TABLE public.keycloak_group OWNER TO postgres;

--
-- Name: keycloak_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.keycloak_role (
    id character varying(36) NOT NULL,
    client_realm_constraint character varying(255),
    client_role boolean DEFAULT false NOT NULL,
    description character varying(255),
    name character varying(255),
    realm_id character varying(255),
    client character varying(36),
    realm character varying(36)
);


ALTER TABLE public.keycloak_role OWNER TO postgres;

--
-- Name: migration_model; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migration_model (
    id character varying(36) NOT NULL,
    version character varying(36),
    update_time bigint DEFAULT 0 NOT NULL
);


ALTER TABLE public.migration_model OWNER TO postgres;

--
-- Name: offline_client_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.offline_client_session (
    user_session_id character varying(36) NOT NULL,
    client_id character varying(255) NOT NULL,
    offline_flag character varying(4) NOT NULL,
    "timestamp" integer,
    data text,
    client_storage_provider character varying(36) DEFAULT 'local'::character varying NOT NULL,
    external_client_id character varying(255) DEFAULT 'local'::character varying NOT NULL
);


ALTER TABLE public.offline_client_session OWNER TO postgres;

--
-- Name: offline_user_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.offline_user_session (
    user_session_id character varying(36) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    created_on integer NOT NULL,
    offline_flag character varying(4) NOT NULL,
    data text,
    last_session_refresh integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.offline_user_session OWNER TO postgres;

--
-- Name: policy_config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.policy_config (
    policy_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value text
);


ALTER TABLE public.policy_config OWNER TO postgres;

--
-- Name: protocol_mapper; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.protocol_mapper (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    protocol character varying(255) NOT NULL,
    protocol_mapper_name character varying(255) NOT NULL,
    client_id character varying(36),
    client_scope_id character varying(36)
);


ALTER TABLE public.protocol_mapper OWNER TO postgres;

--
-- Name: protocol_mapper_config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.protocol_mapper_config (
    protocol_mapper_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.protocol_mapper_config OWNER TO postgres;

--
-- Name: realm; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.realm (
    id character varying(36) NOT NULL,
    access_code_lifespan integer,
    user_action_lifespan integer,
    access_token_lifespan integer,
    account_theme character varying(255),
    admin_theme character varying(255),
    email_theme character varying(255),
    enabled boolean DEFAULT false NOT NULL,
    events_enabled boolean DEFAULT false NOT NULL,
    events_expiration bigint,
    login_theme character varying(255),
    name character varying(255),
    not_before integer,
    password_policy character varying(2550),
    registration_allowed boolean DEFAULT false NOT NULL,
    remember_me boolean DEFAULT false NOT NULL,
    reset_password_allowed boolean DEFAULT false NOT NULL,
    social boolean DEFAULT false NOT NULL,
    ssl_required character varying(255),
    sso_idle_timeout integer,
    sso_max_lifespan integer,
    update_profile_on_soc_login boolean DEFAULT false NOT NULL,
    verify_email boolean DEFAULT false NOT NULL,
    master_admin_client character varying(36),
    login_lifespan integer,
    internationalization_enabled boolean DEFAULT false NOT NULL,
    default_locale character varying(255),
    reg_email_as_username boolean DEFAULT false NOT NULL,
    admin_events_enabled boolean DEFAULT false NOT NULL,
    admin_events_details_enabled boolean DEFAULT false NOT NULL,
    edit_username_allowed boolean DEFAULT false NOT NULL,
    otp_policy_counter integer DEFAULT 0,
    otp_policy_window integer DEFAULT 1,
    otp_policy_period integer DEFAULT 30,
    otp_policy_digits integer DEFAULT 6,
    otp_policy_alg character varying(36) DEFAULT 'HmacSHA1'::character varying,
    otp_policy_type character varying(36) DEFAULT 'totp'::character varying,
    browser_flow character varying(36),
    registration_flow character varying(36),
    direct_grant_flow character varying(36),
    reset_credentials_flow character varying(36),
    client_auth_flow character varying(36),
    offline_session_idle_timeout integer DEFAULT 0,
    revoke_refresh_token boolean DEFAULT false NOT NULL,
    access_token_life_implicit integer DEFAULT 0,
    login_with_email_allowed boolean DEFAULT true NOT NULL,
    duplicate_emails_allowed boolean DEFAULT false NOT NULL,
    docker_auth_flow character varying(36),
    refresh_token_max_reuse integer DEFAULT 0,
    allow_user_managed_access boolean DEFAULT false NOT NULL,
    sso_max_lifespan_remember_me integer DEFAULT 0 NOT NULL,
    sso_idle_timeout_remember_me integer DEFAULT 0 NOT NULL,
    default_role character varying(255)
);


ALTER TABLE public.realm OWNER TO postgres;

--
-- Name: realm_attribute; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.realm_attribute (
    name character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    value text
);


ALTER TABLE public.realm_attribute OWNER TO postgres;

--
-- Name: realm_default_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.realm_default_groups (
    realm_id character varying(36) NOT NULL,
    group_id character varying(36) NOT NULL
);


ALTER TABLE public.realm_default_groups OWNER TO postgres;

--
-- Name: realm_enabled_event_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.realm_enabled_event_types (
    realm_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.realm_enabled_event_types OWNER TO postgres;

--
-- Name: realm_events_listeners; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.realm_events_listeners (
    realm_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.realm_events_listeners OWNER TO postgres;

--
-- Name: realm_localizations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.realm_localizations (
    realm_id character varying(255) NOT NULL,
    locale character varying(255) NOT NULL,
    texts text NOT NULL
);


ALTER TABLE public.realm_localizations OWNER TO postgres;

--
-- Name: realm_required_credential; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.realm_required_credential (
    type character varying(255) NOT NULL,
    form_label character varying(255),
    input boolean DEFAULT false NOT NULL,
    secret boolean DEFAULT false NOT NULL,
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.realm_required_credential OWNER TO postgres;

--
-- Name: realm_smtp_config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.realm_smtp_config (
    realm_id character varying(36) NOT NULL,
    value character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE public.realm_smtp_config OWNER TO postgres;

--
-- Name: realm_supported_locales; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.realm_supported_locales (
    realm_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.realm_supported_locales OWNER TO postgres;

--
-- Name: redirect_uris; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.redirect_uris (
    client_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.redirect_uris OWNER TO postgres;

--
-- Name: required_action_config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.required_action_config (
    required_action_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.required_action_config OWNER TO postgres;

--
-- Name: required_action_provider; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.required_action_provider (
    id character varying(36) NOT NULL,
    alias character varying(255),
    name character varying(255),
    realm_id character varying(36),
    enabled boolean DEFAULT false NOT NULL,
    default_action boolean DEFAULT false NOT NULL,
    provider_id character varying(255),
    priority integer
);


ALTER TABLE public.required_action_provider OWNER TO postgres;

--
-- Name: resource_attribute; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.resource_attribute (
    id character varying(36) DEFAULT 'sybase-needs-something-here'::character varying NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255),
    resource_id character varying(36) NOT NULL
);


ALTER TABLE public.resource_attribute OWNER TO postgres;

--
-- Name: resource_policy; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.resource_policy (
    resource_id character varying(36) NOT NULL,
    policy_id character varying(36) NOT NULL
);


ALTER TABLE public.resource_policy OWNER TO postgres;

--
-- Name: resource_scope; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.resource_scope (
    resource_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL
);


ALTER TABLE public.resource_scope OWNER TO postgres;

--
-- Name: resource_server; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.resource_server (
    id character varying(36) NOT NULL,
    allow_rs_remote_mgmt boolean DEFAULT false NOT NULL,
    policy_enforce_mode smallint NOT NULL,
    decision_strategy smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public.resource_server OWNER TO postgres;

--
-- Name: resource_server_perm_ticket; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.resource_server_perm_ticket (
    id character varying(36) NOT NULL,
    owner character varying(255) NOT NULL,
    requester character varying(255) NOT NULL,
    created_timestamp bigint NOT NULL,
    granted_timestamp bigint,
    resource_id character varying(36) NOT NULL,
    scope_id character varying(36),
    resource_server_id character varying(36) NOT NULL,
    policy_id character varying(36)
);


ALTER TABLE public.resource_server_perm_ticket OWNER TO postgres;

--
-- Name: resource_server_policy; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.resource_server_policy (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    type character varying(255) NOT NULL,
    decision_strategy smallint,
    logic smallint,
    resource_server_id character varying(36) NOT NULL,
    owner character varying(255)
);


ALTER TABLE public.resource_server_policy OWNER TO postgres;

--
-- Name: resource_server_resource; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.resource_server_resource (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    type character varying(255),
    icon_uri character varying(255),
    owner character varying(255) NOT NULL,
    resource_server_id character varying(36) NOT NULL,
    owner_managed_access boolean DEFAULT false NOT NULL,
    display_name character varying(255)
);


ALTER TABLE public.resource_server_resource OWNER TO postgres;

--
-- Name: resource_server_scope; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.resource_server_scope (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    icon_uri character varying(255),
    resource_server_id character varying(36) NOT NULL,
    display_name character varying(255)
);


ALTER TABLE public.resource_server_scope OWNER TO postgres;

--
-- Name: resource_uris; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.resource_uris (
    resource_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.resource_uris OWNER TO postgres;

--
-- Name: role_attribute; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role_attribute (
    id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255)
);


ALTER TABLE public.role_attribute OWNER TO postgres;

--
-- Name: scope_mapping; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.scope_mapping (
    client_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE public.scope_mapping OWNER TO postgres;

--
-- Name: scope_policy; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.scope_policy (
    scope_id character varying(36) NOT NULL,
    policy_id character varying(36) NOT NULL
);


ALTER TABLE public.scope_policy OWNER TO postgres;

--
-- Name: user_attribute; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_attribute (
    name character varying(255) NOT NULL,
    value character varying(255),
    user_id character varying(36) NOT NULL,
    id character varying(36) DEFAULT 'sybase-needs-something-here'::character varying NOT NULL
);


ALTER TABLE public.user_attribute OWNER TO postgres;

--
-- Name: user_consent; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_consent (
    id character varying(36) NOT NULL,
    client_id character varying(255),
    user_id character varying(36) NOT NULL,
    created_date bigint,
    last_updated_date bigint,
    client_storage_provider character varying(36),
    external_client_id character varying(255)
);


ALTER TABLE public.user_consent OWNER TO postgres;

--
-- Name: user_consent_client_scope; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_consent_client_scope (
    user_consent_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL
);


ALTER TABLE public.user_consent_client_scope OWNER TO postgres;

--
-- Name: user_entity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_entity (
    id character varying(36) NOT NULL,
    email character varying(255),
    email_constraint character varying(255),
    email_verified boolean DEFAULT false NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    federation_link character varying(255),
    first_name character varying(255),
    last_name character varying(255),
    realm_id character varying(255),
    username character varying(255),
    created_timestamp bigint,
    service_account_client_link character varying(255),
    not_before integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.user_entity OWNER TO postgres;

--
-- Name: user_federation_config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_federation_config (
    user_federation_provider_id character varying(36) NOT NULL,
    value character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE public.user_federation_config OWNER TO postgres;

--
-- Name: user_federation_mapper; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_federation_mapper (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    federation_provider_id character varying(36) NOT NULL,
    federation_mapper_type character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.user_federation_mapper OWNER TO postgres;

--
-- Name: user_federation_mapper_config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_federation_mapper_config (
    user_federation_mapper_id character varying(36) NOT NULL,
    value character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE public.user_federation_mapper_config OWNER TO postgres;

--
-- Name: user_federation_provider; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_federation_provider (
    id character varying(36) NOT NULL,
    changed_sync_period integer,
    display_name character varying(255),
    full_sync_period integer,
    last_sync integer,
    priority integer,
    provider_name character varying(255),
    realm_id character varying(36)
);


ALTER TABLE public.user_federation_provider OWNER TO postgres;

--
-- Name: user_group_membership; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_group_membership (
    group_id character varying(36) NOT NULL,
    user_id character varying(36) NOT NULL
);


ALTER TABLE public.user_group_membership OWNER TO postgres;

--
-- Name: user_required_action; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_required_action (
    user_id character varying(36) NOT NULL,
    required_action character varying(255) DEFAULT ' '::character varying NOT NULL
);


ALTER TABLE public.user_required_action OWNER TO postgres;

--
-- Name: user_role_mapping; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_role_mapping (
    role_id character varying(255) NOT NULL,
    user_id character varying(36) NOT NULL
);


ALTER TABLE public.user_role_mapping OWNER TO postgres;

--
-- Name: user_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_session (
    id character varying(36) NOT NULL,
    auth_method character varying(255),
    ip_address character varying(255),
    last_session_refresh integer,
    login_username character varying(255),
    realm_id character varying(255),
    remember_me boolean DEFAULT false NOT NULL,
    started integer,
    user_id character varying(255),
    user_session_state integer,
    broker_session_id character varying(255),
    broker_user_id character varying(255)
);


ALTER TABLE public.user_session OWNER TO postgres;

--
-- Name: user_session_note; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_session_note (
    user_session character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(2048)
);


ALTER TABLE public.user_session_note OWNER TO postgres;

--
-- Name: username_login_failure; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.username_login_failure (
    realm_id character varying(36) NOT NULL,
    username character varying(255) NOT NULL,
    failed_login_not_before integer,
    last_failure bigint,
    last_ip_failure character varying(255),
    num_failures integer
);


ALTER TABLE public.username_login_failure OWNER TO postgres;

--
-- Name: web_origins; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.web_origins (
    client_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.web_origins OWNER TO postgres;

--
-- Data for Name: admin_event_entity; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.admin_event_entity (id, admin_event_time, realm_id, operation_type, auth_realm_id, auth_client_id, auth_user_id, ip_address, resource_path, representation, error, resource_type) FROM stdin;
\.


--
-- Data for Name: associated_policy; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.associated_policy (policy_id, associated_policy_id) FROM stdin;
\.


--
-- Data for Name: authentication_execution; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.authentication_execution (id, alias, authenticator, realm_id, flow_id, requirement, priority, authenticator_flow, auth_flow_id, auth_config) FROM stdin;
6e1954c3-0afa-4b87-a685-fbfccd170c1d	\N	auth-cookie	b938aebe-888e-45fc-beec-404ca79e1824	964607dd-c0b1-4ecd-b354-f703d5290d81	2	10	f	\N	\N
b2ae1244-fa87-44ed-9e04-73c934b3a1c2	\N	auth-spnego	b938aebe-888e-45fc-beec-404ca79e1824	964607dd-c0b1-4ecd-b354-f703d5290d81	3	20	f	\N	\N
c1656474-d0db-460b-a69b-a585be09ac04	\N	identity-provider-redirector	b938aebe-888e-45fc-beec-404ca79e1824	964607dd-c0b1-4ecd-b354-f703d5290d81	2	25	f	\N	\N
ea678088-3bae-4b49-97e2-2beaf278e536	\N	\N	b938aebe-888e-45fc-beec-404ca79e1824	964607dd-c0b1-4ecd-b354-f703d5290d81	2	30	t	06b3240c-a48e-4f19-8be0-27c9951d67d7	\N
3b32211e-2c35-41d1-bab7-9997d1d89ad8	\N	auth-username-password-form	b938aebe-888e-45fc-beec-404ca79e1824	06b3240c-a48e-4f19-8be0-27c9951d67d7	0	10	f	\N	\N
9b2463a7-084a-48bb-b42d-6faa0b29206a	\N	\N	b938aebe-888e-45fc-beec-404ca79e1824	06b3240c-a48e-4f19-8be0-27c9951d67d7	1	20	t	ca454acd-f332-4794-a8da-7b6805cc6e23	\N
d7e2d72e-5e43-493a-b81e-7b7b938c1b13	\N	conditional-user-configured	b938aebe-888e-45fc-beec-404ca79e1824	ca454acd-f332-4794-a8da-7b6805cc6e23	0	10	f	\N	\N
df67e949-f43e-439d-b6ec-1d1c4d359812	\N	auth-otp-form	b938aebe-888e-45fc-beec-404ca79e1824	ca454acd-f332-4794-a8da-7b6805cc6e23	0	20	f	\N	\N
9f98c8bc-57e4-4a40-b08c-6edd1a6dcf04	\N	direct-grant-validate-username	b938aebe-888e-45fc-beec-404ca79e1824	91edf9d2-6feb-4248-bced-4ed9b81a40d0	0	10	f	\N	\N
f816cc42-fe02-4380-a7da-1133e94dfb17	\N	direct-grant-validate-password	b938aebe-888e-45fc-beec-404ca79e1824	91edf9d2-6feb-4248-bced-4ed9b81a40d0	0	20	f	\N	\N
8f7b7452-8ad5-4f23-b13f-2522de97f27a	\N	\N	b938aebe-888e-45fc-beec-404ca79e1824	91edf9d2-6feb-4248-bced-4ed9b81a40d0	1	30	t	0b47eccf-02d1-4c87-af3d-9ff76e8f331d	\N
bf9fdb3e-9fa0-442b-b86f-08f02b567710	\N	conditional-user-configured	b938aebe-888e-45fc-beec-404ca79e1824	0b47eccf-02d1-4c87-af3d-9ff76e8f331d	0	10	f	\N	\N
54019be0-4549-496f-84e4-9d8447e7a630	\N	direct-grant-validate-otp	b938aebe-888e-45fc-beec-404ca79e1824	0b47eccf-02d1-4c87-af3d-9ff76e8f331d	0	20	f	\N	\N
396b706f-3a14-4708-9cee-789a45dcefe0	\N	registration-page-form	b938aebe-888e-45fc-beec-404ca79e1824	8ac27ec6-bb8a-4e9f-a3dd-560c1ea6e68f	0	10	t	b156d1f3-9dd8-46bd-a71e-560cae85b7d1	\N
3a0abcb5-725f-4d3a-a229-bfb3ade31bfd	\N	registration-user-creation	b938aebe-888e-45fc-beec-404ca79e1824	b156d1f3-9dd8-46bd-a71e-560cae85b7d1	0	20	f	\N	\N
fe50b027-1131-455b-a5a2-a8b5e45eba0b	\N	registration-profile-action	b938aebe-888e-45fc-beec-404ca79e1824	b156d1f3-9dd8-46bd-a71e-560cae85b7d1	0	40	f	\N	\N
496a1718-9f6e-4879-aae0-ec27fb937f48	\N	registration-password-action	b938aebe-888e-45fc-beec-404ca79e1824	b156d1f3-9dd8-46bd-a71e-560cae85b7d1	0	50	f	\N	\N
eb3d1b3e-ffb7-492a-b4e3-b813cc8c87fb	\N	registration-recaptcha-action	b938aebe-888e-45fc-beec-404ca79e1824	b156d1f3-9dd8-46bd-a71e-560cae85b7d1	3	60	f	\N	\N
869f776d-f908-4271-81da-d4ebc8d265e8	\N	registration-terms-and-conditions	b938aebe-888e-45fc-beec-404ca79e1824	b156d1f3-9dd8-46bd-a71e-560cae85b7d1	3	70	f	\N	\N
4580fb4c-5f88-4c72-bede-a2c204b33252	\N	reset-credentials-choose-user	b938aebe-888e-45fc-beec-404ca79e1824	06c0a236-4076-4c9c-9185-bb239caaf827	0	10	f	\N	\N
335bebe2-8b87-4575-948a-1a06498b39ed	\N	reset-credential-email	b938aebe-888e-45fc-beec-404ca79e1824	06c0a236-4076-4c9c-9185-bb239caaf827	0	20	f	\N	\N
593e018d-941f-4522-937e-1871059485d7	\N	reset-password	b938aebe-888e-45fc-beec-404ca79e1824	06c0a236-4076-4c9c-9185-bb239caaf827	0	30	f	\N	\N
040dcf9d-a82a-43d5-9f5e-20fdf7d131c4	\N	\N	b938aebe-888e-45fc-beec-404ca79e1824	06c0a236-4076-4c9c-9185-bb239caaf827	1	40	t	24f03cde-a0be-4a7c-9311-e73e65012475	\N
c233daac-4c9e-4a8c-a6d8-5806ccfdc229	\N	conditional-user-configured	b938aebe-888e-45fc-beec-404ca79e1824	24f03cde-a0be-4a7c-9311-e73e65012475	0	10	f	\N	\N
71a4c4c9-201a-4854-a1bf-932ae5c06930	\N	reset-otp	b938aebe-888e-45fc-beec-404ca79e1824	24f03cde-a0be-4a7c-9311-e73e65012475	0	20	f	\N	\N
1442f0c5-cbac-4362-b9aa-2d13efd0b2f5	\N	client-secret	b938aebe-888e-45fc-beec-404ca79e1824	ede23736-e67d-4e88-9dfc-1458be14d32c	2	10	f	\N	\N
a439d5b0-93f5-4815-bafe-c713994ec59e	\N	client-jwt	b938aebe-888e-45fc-beec-404ca79e1824	ede23736-e67d-4e88-9dfc-1458be14d32c	2	20	f	\N	\N
7079f2e9-ccbe-41e5-a0e8-3a73a1746e26	\N	client-secret-jwt	b938aebe-888e-45fc-beec-404ca79e1824	ede23736-e67d-4e88-9dfc-1458be14d32c	2	30	f	\N	\N
a8dd62cb-5cc3-45ec-b76d-28512596be23	\N	client-x509	b938aebe-888e-45fc-beec-404ca79e1824	ede23736-e67d-4e88-9dfc-1458be14d32c	2	40	f	\N	\N
f1969c4c-e9f7-4ec0-bea8-01a7d8717a47	\N	idp-review-profile	b938aebe-888e-45fc-beec-404ca79e1824	dc25ad74-35eb-4ed0-8950-d3a56c3a674e	0	10	f	\N	43f85a2b-45ce-4ebc-ba38-b850df3960f4
e8231731-6abb-430f-aff6-03fee5277271	\N	\N	b938aebe-888e-45fc-beec-404ca79e1824	dc25ad74-35eb-4ed0-8950-d3a56c3a674e	0	20	t	52880802-5063-4e52-b0d4-a40414349747	\N
6e2234e4-9b9b-4c32-8743-324d2ea5417d	\N	idp-create-user-if-unique	b938aebe-888e-45fc-beec-404ca79e1824	52880802-5063-4e52-b0d4-a40414349747	2	10	f	\N	6bd2d826-d7a9-457a-932c-6959d8048cac
946654eb-3381-4726-b9fd-1212c40aa965	\N	\N	b938aebe-888e-45fc-beec-404ca79e1824	52880802-5063-4e52-b0d4-a40414349747	2	20	t	1367cad6-5f9e-4890-be63-32431c0d8d09	\N
daec2196-2a61-4c13-a6ac-15d522351909	\N	idp-confirm-link	b938aebe-888e-45fc-beec-404ca79e1824	1367cad6-5f9e-4890-be63-32431c0d8d09	0	10	f	\N	\N
09a2b1c1-1dbd-4355-a894-68bbc8c69214	\N	\N	b938aebe-888e-45fc-beec-404ca79e1824	1367cad6-5f9e-4890-be63-32431c0d8d09	0	20	t	fc252434-dd94-49a5-9248-c568e2098ae8	\N
7976ff13-6ea5-4451-be11-4be87f5d5e5d	\N	idp-email-verification	b938aebe-888e-45fc-beec-404ca79e1824	fc252434-dd94-49a5-9248-c568e2098ae8	2	10	f	\N	\N
78528c2d-f97e-4771-baf7-06eb1ba8fee4	\N	\N	b938aebe-888e-45fc-beec-404ca79e1824	fc252434-dd94-49a5-9248-c568e2098ae8	2	20	t	ebd76882-3d97-4704-bf8c-6c36cf12f298	\N
7ce557f7-2f16-4d5b-ad27-5ec38fce3ceb	\N	idp-username-password-form	b938aebe-888e-45fc-beec-404ca79e1824	ebd76882-3d97-4704-bf8c-6c36cf12f298	0	10	f	\N	\N
58abbf4d-ae2d-41a5-8b7c-5597e2e4f7df	\N	\N	b938aebe-888e-45fc-beec-404ca79e1824	ebd76882-3d97-4704-bf8c-6c36cf12f298	1	20	t	ad521088-07db-470a-a8c3-cd465e57b33d	\N
128dda78-e34e-45f4-a71f-08dfca1aab9f	\N	conditional-user-configured	b938aebe-888e-45fc-beec-404ca79e1824	ad521088-07db-470a-a8c3-cd465e57b33d	0	10	f	\N	\N
9448a740-aac3-4cc6-b8d2-8232f9690567	\N	auth-otp-form	b938aebe-888e-45fc-beec-404ca79e1824	ad521088-07db-470a-a8c3-cd465e57b33d	0	20	f	\N	\N
268e5429-f49a-466f-bb43-838263a5ec1d	\N	http-basic-authenticator	b938aebe-888e-45fc-beec-404ca79e1824	d6389a02-8de1-41c1-b393-c92fd8069136	0	10	f	\N	\N
e831736c-4d5f-43f6-b1ea-46af0de0ea2c	\N	docker-http-basic-authenticator	b938aebe-888e-45fc-beec-404ca79e1824	b59c7da7-dd4b-4a64-993e-58190f66df9d	0	10	f	\N	\N
\.


--
-- Data for Name: authentication_flow; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.authentication_flow (id, alias, description, realm_id, provider_id, top_level, built_in) FROM stdin;
964607dd-c0b1-4ecd-b354-f703d5290d81	browser	browser based authentication	b938aebe-888e-45fc-beec-404ca79e1824	basic-flow	t	t
06b3240c-a48e-4f19-8be0-27c9951d67d7	forms	Username, password, otp and other auth forms.	b938aebe-888e-45fc-beec-404ca79e1824	basic-flow	f	t
ca454acd-f332-4794-a8da-7b6805cc6e23	Browser - Conditional OTP	Flow to determine if the OTP is required for the authentication	b938aebe-888e-45fc-beec-404ca79e1824	basic-flow	f	t
91edf9d2-6feb-4248-bced-4ed9b81a40d0	direct grant	OpenID Connect Resource Owner Grant	b938aebe-888e-45fc-beec-404ca79e1824	basic-flow	t	t
0b47eccf-02d1-4c87-af3d-9ff76e8f331d	Direct Grant - Conditional OTP	Flow to determine if the OTP is required for the authentication	b938aebe-888e-45fc-beec-404ca79e1824	basic-flow	f	t
8ac27ec6-bb8a-4e9f-a3dd-560c1ea6e68f	registration	registration flow	b938aebe-888e-45fc-beec-404ca79e1824	basic-flow	t	t
b156d1f3-9dd8-46bd-a71e-560cae85b7d1	registration form	registration form	b938aebe-888e-45fc-beec-404ca79e1824	form-flow	f	t
06c0a236-4076-4c9c-9185-bb239caaf827	reset credentials	Reset credentials for a user if they forgot their password or something	b938aebe-888e-45fc-beec-404ca79e1824	basic-flow	t	t
24f03cde-a0be-4a7c-9311-e73e65012475	Reset - Conditional OTP	Flow to determine if the OTP should be reset or not. Set to REQUIRED to force.	b938aebe-888e-45fc-beec-404ca79e1824	basic-flow	f	t
ede23736-e67d-4e88-9dfc-1458be14d32c	clients	Base authentication for clients	b938aebe-888e-45fc-beec-404ca79e1824	client-flow	t	t
dc25ad74-35eb-4ed0-8950-d3a56c3a674e	first broker login	Actions taken after first broker login with identity provider account, which is not yet linked to any Keycloak account	b938aebe-888e-45fc-beec-404ca79e1824	basic-flow	t	t
52880802-5063-4e52-b0d4-a40414349747	User creation or linking	Flow for the existing/non-existing user alternatives	b938aebe-888e-45fc-beec-404ca79e1824	basic-flow	f	t
1367cad6-5f9e-4890-be63-32431c0d8d09	Handle Existing Account	Handle what to do if there is existing account with same email/username like authenticated identity provider	b938aebe-888e-45fc-beec-404ca79e1824	basic-flow	f	t
fc252434-dd94-49a5-9248-c568e2098ae8	Account verification options	Method with which to verity the existing account	b938aebe-888e-45fc-beec-404ca79e1824	basic-flow	f	t
ebd76882-3d97-4704-bf8c-6c36cf12f298	Verify Existing Account by Re-authentication	Reauthentication of existing account	b938aebe-888e-45fc-beec-404ca79e1824	basic-flow	f	t
ad521088-07db-470a-a8c3-cd465e57b33d	First broker login - Conditional OTP	Flow to determine if the OTP is required for the authentication	b938aebe-888e-45fc-beec-404ca79e1824	basic-flow	f	t
d6389a02-8de1-41c1-b393-c92fd8069136	saml ecp	SAML ECP Profile Authentication Flow	b938aebe-888e-45fc-beec-404ca79e1824	basic-flow	t	t
b59c7da7-dd4b-4a64-993e-58190f66df9d	docker auth	Used by Docker clients to authenticate against the IDP	b938aebe-888e-45fc-beec-404ca79e1824	basic-flow	t	t
\.


--
-- Data for Name: authenticator_config; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.authenticator_config (id, alias, realm_id) FROM stdin;
43f85a2b-45ce-4ebc-ba38-b850df3960f4	review profile config	b938aebe-888e-45fc-beec-404ca79e1824
6bd2d826-d7a9-457a-932c-6959d8048cac	create unique user config	b938aebe-888e-45fc-beec-404ca79e1824
\.


--
-- Data for Name: authenticator_config_entry; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.authenticator_config_entry (authenticator_id, value, name) FROM stdin;
43f85a2b-45ce-4ebc-ba38-b850df3960f4	missing	update.profile.on.first.login
6bd2d826-d7a9-457a-932c-6959d8048cac	false	require.password.update.after.registration
\.


--
-- Data for Name: broker_link; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.broker_link (identity_provider, storage_provider_id, realm_id, broker_user_id, broker_username, token, user_id) FROM stdin;
\.


--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client (id, enabled, full_scope_allowed, client_id, not_before, public_client, secret, base_url, bearer_only, management_url, surrogate_auth_required, realm_id, protocol, node_rereg_timeout, frontchannel_logout, consent_required, name, service_accounts_enabled, client_authenticator_type, root_url, description, registration_token, standard_flow_enabled, implicit_flow_enabled, direct_access_grants_enabled, always_display_in_console) FROM stdin;
3fc4f049-48fc-43cd-b350-7ec09377778d	t	f	master-realm	0	f	\N	\N	t	\N	f	b938aebe-888e-45fc-beec-404ca79e1824	\N	0	f	f	master Realm	f	client-secret	\N	\N	\N	t	f	f	f
086168c8-0c23-4897-b277-9ab41cd09256	t	f	account	0	t	\N	/realms/master/account/	f	\N	f	b938aebe-888e-45fc-beec-404ca79e1824	openid-connect	0	f	f	${client_account}	f	client-secret	${authBaseUrl}	\N	\N	t	f	f	f
95a16281-c129-4cc2-8f85-d1e225808260	t	f	account-console	0	t	\N	/realms/master/account/	f	\N	f	b938aebe-888e-45fc-beec-404ca79e1824	openid-connect	0	f	f	${client_account-console}	f	client-secret	${authBaseUrl}	\N	\N	t	f	f	f
f906b10c-d01d-4616-b7e0-f37c72fc5145	t	f	broker	0	f	\N	\N	t	\N	f	b938aebe-888e-45fc-beec-404ca79e1824	openid-connect	0	f	f	${client_broker}	f	client-secret	\N	\N	\N	t	f	f	f
cc4cf8f3-57b9-47c6-a489-0cd5e817d1fd	t	f	security-admin-console	0	t	\N	/admin/master/console/	f	\N	f	b938aebe-888e-45fc-beec-404ca79e1824	openid-connect	0	f	f	${client_security-admin-console}	f	client-secret	${authAdminUrl}	\N	\N	t	f	f	f
e6688b22-4a04-4a0f-bf2b-df8ba853f92f	t	f	admin-cli	0	t	\N	\N	f	\N	f	b938aebe-888e-45fc-beec-404ca79e1824	openid-connect	0	f	f	${client_admin-cli}	f	client-secret	\N	\N	\N	f	f	t	f
17139c9f-1d33-46cc-9402-7a4efd1761a3	t	t	refae	0	f	4ifYZia3OhVYNvqkmtSMe4CcQwtljdAS	https://refae	f	http://localhost:8080	f	b938aebe-888e-45fc-beec-404ca79e1824	openid-connect	-1	t	f		f	client-secret	http://localhost:8080		\N	t	f	t	f
e2247467-ce88-4f66-ad26-2f941c57481a	t	t	refae2	0	f	CP9iendiabNhoUvT8M1DVoaLcIE3x8Fq	https://refae	f	https://refae	f	b938aebe-888e-45fc-beec-404ca79e1824	openid-connect	-1	t	f		f	client-secret	https://refae		\N	t	f	t	f
\.


--
-- Data for Name: client_attributes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_attributes (client_id, name, value) FROM stdin;
086168c8-0c23-4897-b277-9ab41cd09256	post.logout.redirect.uris	+
95a16281-c129-4cc2-8f85-d1e225808260	post.logout.redirect.uris	+
95a16281-c129-4cc2-8f85-d1e225808260	pkce.code.challenge.method	S256
cc4cf8f3-57b9-47c6-a489-0cd5e817d1fd	post.logout.redirect.uris	+
cc4cf8f3-57b9-47c6-a489-0cd5e817d1fd	pkce.code.challenge.method	S256
17139c9f-1d33-46cc-9402-7a4efd1761a3	client.secret.creation.time	1700728950
17139c9f-1d33-46cc-9402-7a4efd1761a3	oauth2.device.authorization.grant.enabled	false
17139c9f-1d33-46cc-9402-7a4efd1761a3	oidc.ciba.grant.enabled	false
17139c9f-1d33-46cc-9402-7a4efd1761a3	post.logout.redirect.uris	https://refae/*
17139c9f-1d33-46cc-9402-7a4efd1761a3	backchannel.logout.session.required	true
17139c9f-1d33-46cc-9402-7a4efd1761a3	backchannel.logout.revoke.offline.tokens	false
e2247467-ce88-4f66-ad26-2f941c57481a	client.secret.creation.time	1700729298
e2247467-ce88-4f66-ad26-2f941c57481a	oauth2.device.authorization.grant.enabled	false
e2247467-ce88-4f66-ad26-2f941c57481a	oidc.ciba.grant.enabled	false
e2247467-ce88-4f66-ad26-2f941c57481a	post.logout.redirect.uris	https://refae/*
e2247467-ce88-4f66-ad26-2f941c57481a	backchannel.logout.session.required	true
e2247467-ce88-4f66-ad26-2f941c57481a	backchannel.logout.revoke.offline.tokens	false
\.


--
-- Data for Name: client_auth_flow_bindings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_auth_flow_bindings (client_id, flow_id, binding_name) FROM stdin;
\.


--
-- Data for Name: client_initial_access; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_initial_access (id, realm_id, "timestamp", expiration, count, remaining_count) FROM stdin;
\.


--
-- Data for Name: client_node_registrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_node_registrations (client_id, value, name) FROM stdin;
\.


--
-- Data for Name: client_scope; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_scope (id, name, realm_id, description, protocol) FROM stdin;
704d63d6-4d37-4dfb-a450-aebaf8eee9ce	offline_access	b938aebe-888e-45fc-beec-404ca79e1824	OpenID Connect built-in scope: offline_access	openid-connect
0d38c2d9-1d4f-4457-afaf-8cb043566ac8	role_list	b938aebe-888e-45fc-beec-404ca79e1824	SAML role list	saml
7cf26061-6cc3-4b06-882b-61162ac1fc78	profile	b938aebe-888e-45fc-beec-404ca79e1824	OpenID Connect built-in scope: profile	openid-connect
1383c0b9-83e2-47bf-945f-1098401335be	email	b938aebe-888e-45fc-beec-404ca79e1824	OpenID Connect built-in scope: email	openid-connect
e6bf5ba8-3429-4c01-90d8-172eca1b74f9	address	b938aebe-888e-45fc-beec-404ca79e1824	OpenID Connect built-in scope: address	openid-connect
4d97c3c5-9466-48c4-9a67-bc792e6642cd	phone	b938aebe-888e-45fc-beec-404ca79e1824	OpenID Connect built-in scope: phone	openid-connect
0e4dc652-e563-41a9-8040-ff79aaa4ccc5	roles	b938aebe-888e-45fc-beec-404ca79e1824	OpenID Connect scope for add user roles to the access token	openid-connect
2925672b-bc38-4bb2-ae97-872b361cd6e6	web-origins	b938aebe-888e-45fc-beec-404ca79e1824	OpenID Connect scope for add allowed web origins to the access token	openid-connect
767507df-0f87-4861-a374-3d1d11d9a1a6	microprofile-jwt	b938aebe-888e-45fc-beec-404ca79e1824	Microprofile - JWT built-in scope	openid-connect
42c14764-a445-4851-bdf9-f613cd73bdab	acr	b938aebe-888e-45fc-beec-404ca79e1824	OpenID Connect scope for add acr (authentication context class reference) to the token	openid-connect
\.


--
-- Data for Name: client_scope_attributes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_scope_attributes (scope_id, value, name) FROM stdin;
704d63d6-4d37-4dfb-a450-aebaf8eee9ce	true	display.on.consent.screen
704d63d6-4d37-4dfb-a450-aebaf8eee9ce	${offlineAccessScopeConsentText}	consent.screen.text
0d38c2d9-1d4f-4457-afaf-8cb043566ac8	true	display.on.consent.screen
0d38c2d9-1d4f-4457-afaf-8cb043566ac8	${samlRoleListScopeConsentText}	consent.screen.text
7cf26061-6cc3-4b06-882b-61162ac1fc78	true	display.on.consent.screen
7cf26061-6cc3-4b06-882b-61162ac1fc78	${profileScopeConsentText}	consent.screen.text
7cf26061-6cc3-4b06-882b-61162ac1fc78	true	include.in.token.scope
1383c0b9-83e2-47bf-945f-1098401335be	true	display.on.consent.screen
1383c0b9-83e2-47bf-945f-1098401335be	${emailScopeConsentText}	consent.screen.text
1383c0b9-83e2-47bf-945f-1098401335be	true	include.in.token.scope
e6bf5ba8-3429-4c01-90d8-172eca1b74f9	true	display.on.consent.screen
e6bf5ba8-3429-4c01-90d8-172eca1b74f9	${addressScopeConsentText}	consent.screen.text
e6bf5ba8-3429-4c01-90d8-172eca1b74f9	true	include.in.token.scope
4d97c3c5-9466-48c4-9a67-bc792e6642cd	true	display.on.consent.screen
4d97c3c5-9466-48c4-9a67-bc792e6642cd	${phoneScopeConsentText}	consent.screen.text
4d97c3c5-9466-48c4-9a67-bc792e6642cd	true	include.in.token.scope
0e4dc652-e563-41a9-8040-ff79aaa4ccc5	true	display.on.consent.screen
0e4dc652-e563-41a9-8040-ff79aaa4ccc5	${rolesScopeConsentText}	consent.screen.text
0e4dc652-e563-41a9-8040-ff79aaa4ccc5	false	include.in.token.scope
2925672b-bc38-4bb2-ae97-872b361cd6e6	false	display.on.consent.screen
2925672b-bc38-4bb2-ae97-872b361cd6e6		consent.screen.text
2925672b-bc38-4bb2-ae97-872b361cd6e6	false	include.in.token.scope
767507df-0f87-4861-a374-3d1d11d9a1a6	false	display.on.consent.screen
767507df-0f87-4861-a374-3d1d11d9a1a6	true	include.in.token.scope
42c14764-a445-4851-bdf9-f613cd73bdab	false	display.on.consent.screen
42c14764-a445-4851-bdf9-f613cd73bdab	false	include.in.token.scope
\.


--
-- Data for Name: client_scope_client; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_scope_client (client_id, scope_id, default_scope) FROM stdin;
086168c8-0c23-4897-b277-9ab41cd09256	2925672b-bc38-4bb2-ae97-872b361cd6e6	t
086168c8-0c23-4897-b277-9ab41cd09256	42c14764-a445-4851-bdf9-f613cd73bdab	t
086168c8-0c23-4897-b277-9ab41cd09256	7cf26061-6cc3-4b06-882b-61162ac1fc78	t
086168c8-0c23-4897-b277-9ab41cd09256	0e4dc652-e563-41a9-8040-ff79aaa4ccc5	t
086168c8-0c23-4897-b277-9ab41cd09256	1383c0b9-83e2-47bf-945f-1098401335be	t
086168c8-0c23-4897-b277-9ab41cd09256	4d97c3c5-9466-48c4-9a67-bc792e6642cd	f
086168c8-0c23-4897-b277-9ab41cd09256	767507df-0f87-4861-a374-3d1d11d9a1a6	f
086168c8-0c23-4897-b277-9ab41cd09256	e6bf5ba8-3429-4c01-90d8-172eca1b74f9	f
086168c8-0c23-4897-b277-9ab41cd09256	704d63d6-4d37-4dfb-a450-aebaf8eee9ce	f
95a16281-c129-4cc2-8f85-d1e225808260	2925672b-bc38-4bb2-ae97-872b361cd6e6	t
95a16281-c129-4cc2-8f85-d1e225808260	42c14764-a445-4851-bdf9-f613cd73bdab	t
95a16281-c129-4cc2-8f85-d1e225808260	7cf26061-6cc3-4b06-882b-61162ac1fc78	t
95a16281-c129-4cc2-8f85-d1e225808260	0e4dc652-e563-41a9-8040-ff79aaa4ccc5	t
95a16281-c129-4cc2-8f85-d1e225808260	1383c0b9-83e2-47bf-945f-1098401335be	t
95a16281-c129-4cc2-8f85-d1e225808260	4d97c3c5-9466-48c4-9a67-bc792e6642cd	f
95a16281-c129-4cc2-8f85-d1e225808260	767507df-0f87-4861-a374-3d1d11d9a1a6	f
95a16281-c129-4cc2-8f85-d1e225808260	e6bf5ba8-3429-4c01-90d8-172eca1b74f9	f
95a16281-c129-4cc2-8f85-d1e225808260	704d63d6-4d37-4dfb-a450-aebaf8eee9ce	f
e6688b22-4a04-4a0f-bf2b-df8ba853f92f	2925672b-bc38-4bb2-ae97-872b361cd6e6	t
e6688b22-4a04-4a0f-bf2b-df8ba853f92f	42c14764-a445-4851-bdf9-f613cd73bdab	t
e6688b22-4a04-4a0f-bf2b-df8ba853f92f	7cf26061-6cc3-4b06-882b-61162ac1fc78	t
e6688b22-4a04-4a0f-bf2b-df8ba853f92f	0e4dc652-e563-41a9-8040-ff79aaa4ccc5	t
e6688b22-4a04-4a0f-bf2b-df8ba853f92f	1383c0b9-83e2-47bf-945f-1098401335be	t
e6688b22-4a04-4a0f-bf2b-df8ba853f92f	4d97c3c5-9466-48c4-9a67-bc792e6642cd	f
e6688b22-4a04-4a0f-bf2b-df8ba853f92f	767507df-0f87-4861-a374-3d1d11d9a1a6	f
e6688b22-4a04-4a0f-bf2b-df8ba853f92f	e6bf5ba8-3429-4c01-90d8-172eca1b74f9	f
e6688b22-4a04-4a0f-bf2b-df8ba853f92f	704d63d6-4d37-4dfb-a450-aebaf8eee9ce	f
f906b10c-d01d-4616-b7e0-f37c72fc5145	2925672b-bc38-4bb2-ae97-872b361cd6e6	t
f906b10c-d01d-4616-b7e0-f37c72fc5145	42c14764-a445-4851-bdf9-f613cd73bdab	t
f906b10c-d01d-4616-b7e0-f37c72fc5145	7cf26061-6cc3-4b06-882b-61162ac1fc78	t
f906b10c-d01d-4616-b7e0-f37c72fc5145	0e4dc652-e563-41a9-8040-ff79aaa4ccc5	t
f906b10c-d01d-4616-b7e0-f37c72fc5145	1383c0b9-83e2-47bf-945f-1098401335be	t
f906b10c-d01d-4616-b7e0-f37c72fc5145	4d97c3c5-9466-48c4-9a67-bc792e6642cd	f
f906b10c-d01d-4616-b7e0-f37c72fc5145	767507df-0f87-4861-a374-3d1d11d9a1a6	f
f906b10c-d01d-4616-b7e0-f37c72fc5145	e6bf5ba8-3429-4c01-90d8-172eca1b74f9	f
f906b10c-d01d-4616-b7e0-f37c72fc5145	704d63d6-4d37-4dfb-a450-aebaf8eee9ce	f
3fc4f049-48fc-43cd-b350-7ec09377778d	2925672b-bc38-4bb2-ae97-872b361cd6e6	t
3fc4f049-48fc-43cd-b350-7ec09377778d	42c14764-a445-4851-bdf9-f613cd73bdab	t
3fc4f049-48fc-43cd-b350-7ec09377778d	7cf26061-6cc3-4b06-882b-61162ac1fc78	t
3fc4f049-48fc-43cd-b350-7ec09377778d	0e4dc652-e563-41a9-8040-ff79aaa4ccc5	t
3fc4f049-48fc-43cd-b350-7ec09377778d	1383c0b9-83e2-47bf-945f-1098401335be	t
3fc4f049-48fc-43cd-b350-7ec09377778d	4d97c3c5-9466-48c4-9a67-bc792e6642cd	f
3fc4f049-48fc-43cd-b350-7ec09377778d	767507df-0f87-4861-a374-3d1d11d9a1a6	f
3fc4f049-48fc-43cd-b350-7ec09377778d	e6bf5ba8-3429-4c01-90d8-172eca1b74f9	f
3fc4f049-48fc-43cd-b350-7ec09377778d	704d63d6-4d37-4dfb-a450-aebaf8eee9ce	f
cc4cf8f3-57b9-47c6-a489-0cd5e817d1fd	2925672b-bc38-4bb2-ae97-872b361cd6e6	t
cc4cf8f3-57b9-47c6-a489-0cd5e817d1fd	42c14764-a445-4851-bdf9-f613cd73bdab	t
cc4cf8f3-57b9-47c6-a489-0cd5e817d1fd	7cf26061-6cc3-4b06-882b-61162ac1fc78	t
cc4cf8f3-57b9-47c6-a489-0cd5e817d1fd	0e4dc652-e563-41a9-8040-ff79aaa4ccc5	t
cc4cf8f3-57b9-47c6-a489-0cd5e817d1fd	1383c0b9-83e2-47bf-945f-1098401335be	t
cc4cf8f3-57b9-47c6-a489-0cd5e817d1fd	4d97c3c5-9466-48c4-9a67-bc792e6642cd	f
cc4cf8f3-57b9-47c6-a489-0cd5e817d1fd	767507df-0f87-4861-a374-3d1d11d9a1a6	f
cc4cf8f3-57b9-47c6-a489-0cd5e817d1fd	e6bf5ba8-3429-4c01-90d8-172eca1b74f9	f
cc4cf8f3-57b9-47c6-a489-0cd5e817d1fd	704d63d6-4d37-4dfb-a450-aebaf8eee9ce	f
17139c9f-1d33-46cc-9402-7a4efd1761a3	2925672b-bc38-4bb2-ae97-872b361cd6e6	t
17139c9f-1d33-46cc-9402-7a4efd1761a3	42c14764-a445-4851-bdf9-f613cd73bdab	t
17139c9f-1d33-46cc-9402-7a4efd1761a3	7cf26061-6cc3-4b06-882b-61162ac1fc78	t
17139c9f-1d33-46cc-9402-7a4efd1761a3	0e4dc652-e563-41a9-8040-ff79aaa4ccc5	t
17139c9f-1d33-46cc-9402-7a4efd1761a3	1383c0b9-83e2-47bf-945f-1098401335be	t
17139c9f-1d33-46cc-9402-7a4efd1761a3	4d97c3c5-9466-48c4-9a67-bc792e6642cd	f
17139c9f-1d33-46cc-9402-7a4efd1761a3	767507df-0f87-4861-a374-3d1d11d9a1a6	f
17139c9f-1d33-46cc-9402-7a4efd1761a3	e6bf5ba8-3429-4c01-90d8-172eca1b74f9	f
17139c9f-1d33-46cc-9402-7a4efd1761a3	704d63d6-4d37-4dfb-a450-aebaf8eee9ce	f
e2247467-ce88-4f66-ad26-2f941c57481a	2925672b-bc38-4bb2-ae97-872b361cd6e6	t
e2247467-ce88-4f66-ad26-2f941c57481a	42c14764-a445-4851-bdf9-f613cd73bdab	t
e2247467-ce88-4f66-ad26-2f941c57481a	7cf26061-6cc3-4b06-882b-61162ac1fc78	t
e2247467-ce88-4f66-ad26-2f941c57481a	0e4dc652-e563-41a9-8040-ff79aaa4ccc5	t
e2247467-ce88-4f66-ad26-2f941c57481a	1383c0b9-83e2-47bf-945f-1098401335be	t
e2247467-ce88-4f66-ad26-2f941c57481a	4d97c3c5-9466-48c4-9a67-bc792e6642cd	f
e2247467-ce88-4f66-ad26-2f941c57481a	767507df-0f87-4861-a374-3d1d11d9a1a6	f
e2247467-ce88-4f66-ad26-2f941c57481a	e6bf5ba8-3429-4c01-90d8-172eca1b74f9	f
e2247467-ce88-4f66-ad26-2f941c57481a	704d63d6-4d37-4dfb-a450-aebaf8eee9ce	f
\.


--
-- Data for Name: client_scope_role_mapping; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_scope_role_mapping (scope_id, role_id) FROM stdin;
704d63d6-4d37-4dfb-a450-aebaf8eee9ce	c3cac550-d778-4944-9d50-9eacc13819d2
\.


--
-- Data for Name: client_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_session (id, client_id, redirect_uri, state, "timestamp", session_id, auth_method, realm_id, auth_user_id, current_action) FROM stdin;
\.


--
-- Data for Name: client_session_auth_status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_session_auth_status (authenticator, status, client_session) FROM stdin;
\.


--
-- Data for Name: client_session_note; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_session_note (name, value, client_session) FROM stdin;
\.


--
-- Data for Name: client_session_prot_mapper; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_session_prot_mapper (protocol_mapper_id, client_session) FROM stdin;
\.


--
-- Data for Name: client_session_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_session_role (role_id, client_session) FROM stdin;
\.


--
-- Data for Name: client_user_session_note; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_user_session_note (name, value, client_session) FROM stdin;
\.


--
-- Data for Name: component; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.component (id, name, parent_id, provider_id, provider_type, realm_id, sub_type) FROM stdin;
dbcfbb80-260f-4793-970f-b650e098291f	Trusted Hosts	b938aebe-888e-45fc-beec-404ca79e1824	trusted-hosts	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	b938aebe-888e-45fc-beec-404ca79e1824	anonymous
72acf2fe-79e6-44fa-be1c-e4aeb347dec6	Consent Required	b938aebe-888e-45fc-beec-404ca79e1824	consent-required	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	b938aebe-888e-45fc-beec-404ca79e1824	anonymous
7c84ad1a-6cfa-4190-9f3e-f4c71f983254	Full Scope Disabled	b938aebe-888e-45fc-beec-404ca79e1824	scope	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	b938aebe-888e-45fc-beec-404ca79e1824	anonymous
292f6563-7a02-407f-936d-74a3d726b44e	Max Clients Limit	b938aebe-888e-45fc-beec-404ca79e1824	max-clients	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	b938aebe-888e-45fc-beec-404ca79e1824	anonymous
92e71874-01af-4a95-a800-1a4688df3993	Allowed Protocol Mapper Types	b938aebe-888e-45fc-beec-404ca79e1824	allowed-protocol-mappers	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	b938aebe-888e-45fc-beec-404ca79e1824	anonymous
c3a0beb5-722f-418b-b090-4fec5ae9a349	Allowed Client Scopes	b938aebe-888e-45fc-beec-404ca79e1824	allowed-client-templates	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	b938aebe-888e-45fc-beec-404ca79e1824	anonymous
e6cc0a48-82b2-4732-9770-14bfcb0991c3	Allowed Protocol Mapper Types	b938aebe-888e-45fc-beec-404ca79e1824	allowed-protocol-mappers	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	b938aebe-888e-45fc-beec-404ca79e1824	authenticated
d60a8e30-4a4a-42e4-9c99-b46eaab2f732	Allowed Client Scopes	b938aebe-888e-45fc-beec-404ca79e1824	allowed-client-templates	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	b938aebe-888e-45fc-beec-404ca79e1824	authenticated
f9e8b836-141a-4a60-bee9-3f4bf900d4af	rsa-generated	b938aebe-888e-45fc-beec-404ca79e1824	rsa-generated	org.keycloak.keys.KeyProvider	b938aebe-888e-45fc-beec-404ca79e1824	\N
58eb6127-303c-4afb-9953-5ae5d4e5ec40	rsa-enc-generated	b938aebe-888e-45fc-beec-404ca79e1824	rsa-enc-generated	org.keycloak.keys.KeyProvider	b938aebe-888e-45fc-beec-404ca79e1824	\N
14f2ff40-d745-4b69-9d41-69983a3ecece	hmac-generated	b938aebe-888e-45fc-beec-404ca79e1824	hmac-generated	org.keycloak.keys.KeyProvider	b938aebe-888e-45fc-beec-404ca79e1824	\N
5a4fdace-3b77-477f-815f-38f019b73bcb	aes-generated	b938aebe-888e-45fc-beec-404ca79e1824	aes-generated	org.keycloak.keys.KeyProvider	b938aebe-888e-45fc-beec-404ca79e1824	\N
\.


--
-- Data for Name: component_config; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.component_config (id, component_id, name, value) FROM stdin;
6a79af31-c50e-41c9-bc0f-beadd3466276	292f6563-7a02-407f-936d-74a3d726b44e	max-clients	200
eb8758e4-955a-4af9-8bf0-41469f5f393c	92e71874-01af-4a95-a800-1a4688df3993	allowed-protocol-mapper-types	oidc-address-mapper
e3058022-78ac-4cfe-94e5-52a407b04507	92e71874-01af-4a95-a800-1a4688df3993	allowed-protocol-mapper-types	saml-role-list-mapper
b3a83384-8789-4212-880c-fa8caaefdb57	92e71874-01af-4a95-a800-1a4688df3993	allowed-protocol-mapper-types	oidc-full-name-mapper
fb47b36b-8a9a-4b8f-880c-e880ef44e773	92e71874-01af-4a95-a800-1a4688df3993	allowed-protocol-mapper-types	saml-user-attribute-mapper
29c7ca04-0454-4d67-b16b-ac8eac0df5a5	92e71874-01af-4a95-a800-1a4688df3993	allowed-protocol-mapper-types	oidc-sha256-pairwise-sub-mapper
d0764193-3dc5-4ece-86dd-da0e289e0f68	92e71874-01af-4a95-a800-1a4688df3993	allowed-protocol-mapper-types	oidc-usermodel-attribute-mapper
0f364414-d360-4fd2-90e2-63feea366991	92e71874-01af-4a95-a800-1a4688df3993	allowed-protocol-mapper-types	oidc-usermodel-property-mapper
e4afa33f-3817-4803-b94e-e03c9402409c	92e71874-01af-4a95-a800-1a4688df3993	allowed-protocol-mapper-types	saml-user-property-mapper
dcf772db-6fe0-4690-a600-4c39b9026282	c3a0beb5-722f-418b-b090-4fec5ae9a349	allow-default-scopes	true
ff063b84-2a7b-4d2c-bb7b-ed7becb64963	e6cc0a48-82b2-4732-9770-14bfcb0991c3	allowed-protocol-mapper-types	saml-user-property-mapper
53f1ccc1-e40b-404d-9ca0-65eea6387b25	e6cc0a48-82b2-4732-9770-14bfcb0991c3	allowed-protocol-mapper-types	oidc-address-mapper
2a851420-cf21-4a7b-a823-e13110f995f8	e6cc0a48-82b2-4732-9770-14bfcb0991c3	allowed-protocol-mapper-types	oidc-full-name-mapper
36b18e07-6bc8-4eb4-b290-99d5d5b81456	e6cc0a48-82b2-4732-9770-14bfcb0991c3	allowed-protocol-mapper-types	saml-role-list-mapper
c1db76d8-f450-45d9-9c82-394ad2117c52	e6cc0a48-82b2-4732-9770-14bfcb0991c3	allowed-protocol-mapper-types	oidc-sha256-pairwise-sub-mapper
ae670045-e5b9-411f-bc60-10ac4576434f	e6cc0a48-82b2-4732-9770-14bfcb0991c3	allowed-protocol-mapper-types	oidc-usermodel-property-mapper
04ebf3f7-42fd-470f-8031-f1f08c6133da	e6cc0a48-82b2-4732-9770-14bfcb0991c3	allowed-protocol-mapper-types	saml-user-attribute-mapper
cd12c931-e219-4cc7-a6bb-1b77145b9a9a	e6cc0a48-82b2-4732-9770-14bfcb0991c3	allowed-protocol-mapper-types	oidc-usermodel-attribute-mapper
28fc201a-4d07-4f9e-8c6d-5cb79e82e911	d60a8e30-4a4a-42e4-9c99-b46eaab2f732	allow-default-scopes	true
ea30b93f-edd6-4703-82c7-a7643908bf6b	dbcfbb80-260f-4793-970f-b650e098291f	host-sending-registration-request-must-match	true
043c90cf-59af-4fe3-af78-d97c016e3b7f	dbcfbb80-260f-4793-970f-b650e098291f	client-uris-must-match	true
82fa5848-4b8a-4fc1-aacd-5a2814a1861a	f9e8b836-141a-4a60-bee9-3f4bf900d4af	priority	100
80da49c3-40b7-4ab1-8ff7-9f308c38d051	f9e8b836-141a-4a60-bee9-3f4bf900d4af	privateKey	MIIEowIBAAKCAQEAq8fqEy2JcqrIwgVUik96C1QUfv4BBvPk+KVuCr/84GWRAsoI4iC3OTvNQZi3/Y6aKxsp2E3LuKMVxqPV5jU/bobeaI+pKTDrHFcIiJpiRIdT/jtCmOO5IJtsUHEGTRb+kTk0j2kxESxixKgYqJffXIBBnUNn3lAL2+ytpQLa39f4vjOFfE+yJPUSHXql/wHy+36ZweY/tA2uhl9ZZeJW0Lq79EmvQBvxhH3/RA+xQPffWFICWbTD9tZP/L5ewDZm2z052iUuBp1WB74iu1YeaMVkEAsMUu2dXz1RNzoXLJ2A9gU8CSsXWCgVnDrFY2FwNsTxEBfxmSZQJpP5FxIMawIDAQABAoIBAAW3sbgnBobP3b7CI/42hv65iF/HCeG2sc2qM+9yVHMOQ7VZZCeipHaTNvxn840iTJ9YAsc1pQI662P2hWK+2E2lejb2ESX91YAr+sIRNr7GglUumRjdpjEfpeCc4nN5Fcq092WOlW4s5rIE+6J2QPOjO+EwIn+TynvsyhrOmC+7F+Yu+NYrPsUugxjD4AkwDo+DVAe+y4jvYR960suSWrCrXTcraOxz8wGvprNa/7TDLL4ByOGOBEMoB5fTTYvlHhU+WSCi7BHjypS+rMXudJAf/U/JW4TE6Bnn5ohLNFXtAaVChxlXKQ62NcH6XtmaNFxDR6Q+b9W6DUijGF5SQwECgYEA1UYElyp4AAWOEZadHiWovC2mqkMvP6LIYsmXOl9MTmLvjXMD1RSB2kJWO3EoQWfdNw6aFgsylNgPlvZx4Qibvh/0E4GhlBFLF37H1Ek2RMfHBO5GtBpT9mvROyhY9ou6/0fQx1+USouvgGAXWLHcLZwF1t12NNW4SbGavbW/BhMCgYEAzjHmcIhxt7Swg73A9Rpu1naej66UMJuRuqiaKYqtFTfhVkQsLb8PM2OklwfoVFXzDEvE4oaoTueBPHvxuoZ4UINfP4BpEtJJRQtRgxuulFGYBabuNMOA+5bJS906ZzlzdOVr9rZPyjtYIvunZVeT5TW0AOdvL3GGtWXoNtUfi0kCgYEAu7LJq3i2793zsMsLMFMYc7UBr2s9lBXhof+lVrw0XfB4v9ZvkwJfXgVv+xSVxypv6FqzhZ8JPzIAVm5CvzhiSSu6FcjKuZOWsPDlUZG1b0cbgt1p4GwjZQZyuEvEMAe95ZoAxtb5nbneFW98XJT8IC/me7mgpNNLQ6BJfZkMq30CgYBj0HoZ7aMG59wu2fOHcxVGkkNtkHhCE3z3G9kM1Qm2ZMebSAMO3hdQrCKnzfckpDq0sBUOB1uAXscL7X/16FRjvUSla9bxeoRnfPQngIYBXII38jgLqLxOjg7Bv8Ue5QvEAa9CzbxQTGv8+09sWnnIkjeh3GbRU6Z/N2ay7HaJqQKBgGHQo162vlSKOiBFPe1WTe+kful5Rvb4/ntmzQvkkKob2hizLRjO4lYfzAB1lmzJNbSq+K/vTC/oJxfGePL9r80PNvVhf5Xt6eRqv7VvurUJhjZ+G+NyiZLozW+aAwG7+WpgOjxZpLt5ubrET+dzOqrDGWJRzVzdW++/y5M3DEf4
60a915e4-8e9c-46f8-9b44-825db8d0b52b	f9e8b836-141a-4a60-bee9-3f4bf900d4af	keyUse	SIG
0939a885-c107-4788-b55b-3b14b4b907ef	f9e8b836-141a-4a60-bee9-3f4bf900d4af	certificate	MIICmzCCAYMCBgGL+1RxYjANBgkqhkiG9w0BAQsFADARMQ8wDQYDVQQDDAZtYXN0ZXIwHhcNMjMxMTIzMDgzNjM3WhcNMzMxMTIzMDgzODE3WjARMQ8wDQYDVQQDDAZtYXN0ZXIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCrx+oTLYlyqsjCBVSKT3oLVBR+/gEG8+T4pW4Kv/zgZZECygjiILc5O81BmLf9jporGynYTcu4oxXGo9XmNT9uht5oj6kpMOscVwiImmJEh1P+O0KY47kgm2xQcQZNFv6ROTSPaTERLGLEqBiol99cgEGdQ2feUAvb7K2lAtrf1/i+M4V8T7Ik9RIdeqX/AfL7fpnB5j+0Da6GX1ll4lbQurv0Sa9AG/GEff9ED7FA999YUgJZtMP21k/8vl7ANmbbPTnaJS4GnVYHviK7Vh5oxWQQCwxS7Z1fPVE3OhcsnYD2BTwJKxdYKBWcOsVjYXA2xPEQF/GZJlAmk/kXEgxrAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAFXeX1uQ/IuUvMEiBlvZL0aQzt6s8Kp4aNv/BDX9kJ0gCoq0g+ifqNtTHq6MmLVk7uRaXYxsNMOKt9ZiUTaze+DiC4kQy2SedRyKXmSzAJG/dNa6xggwuKjVUA/YYOtAIT8XQjzZ74mvVrgn67YogYf5aFzksUcCXsxqt5a9FyH2nFjWMV5Su1/jH590mgHYVBzJG4JONXfe5bTZcA3Kfq11p15KV+Vu1unG1rFiyvwZwZkASkT7PQJnI49cKAekiVmDtnY+AE9v6a/DvtWt1JYnNb8oa7FW8EeV4ex5sWI5ZqIimoOpuP7egY208AL4A1aG0Qje0m0oQq0yPDAm6ic=
0b95d52a-2e5a-4add-b241-7b9913068a7b	14f2ff40-d745-4b69-9d41-69983a3ecece	algorithm	HS256
be18be4d-2e5e-4e32-b0d9-0dd546856714	14f2ff40-d745-4b69-9d41-69983a3ecece	secret	ou-MEYVylRWQOM_w_O5_ID80y7pabgx4WhmttbXc0sXCiCXRxyKkV6Rc9tDBaUlgQUVnjfmZLIde_6RHVbJw0g
4e02d4e6-0a96-433d-8752-f2cefe34487c	14f2ff40-d745-4b69-9d41-69983a3ecece	kid	e6a94d16-6d9d-4e97-a0fa-abe413f69a67
c2d0e142-3e4b-49e0-89bc-a2bea0a92b07	14f2ff40-d745-4b69-9d41-69983a3ecece	priority	100
be539bd3-7fd8-4923-b2ef-c8da43854f33	5a4fdace-3b77-477f-815f-38f019b73bcb	secret	reBiVBAuo9As7uuOYhldlQ
13c6b94f-37cf-44f8-9ba3-15e99ad56e18	5a4fdace-3b77-477f-815f-38f019b73bcb	kid	d203eaf0-2e77-4ce0-869e-fdb46c160fff
6b549350-cf64-416d-b4a6-0cea690509b6	5a4fdace-3b77-477f-815f-38f019b73bcb	priority	100
0bc36fc7-da88-44fe-94d0-e5f1c745d35b	58eb6127-303c-4afb-9953-5ae5d4e5ec40	certificate	MIICmzCCAYMCBgGL+1RyPDANBgkqhkiG9w0BAQsFADARMQ8wDQYDVQQDDAZtYXN0ZXIwHhcNMjMxMTIzMDgzNjM3WhcNMzMxMTIzMDgzODE3WjARMQ8wDQYDVQQDDAZtYXN0ZXIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCP7ockRTBrVBR29EV9ku8pRIwY2IF4d25LM8nDCImvkd4qlAFJgfSm9MJNrUsJ85J8NgdHZQo+6q2MMifKWh8TgDR4WTEMQZVSuc1ku2jMIOEFOmQ37fSUV4bwH+4ArQ7brWwlVaQLJY/y65EjwNsMCh7VWVeNhhixV25E7J3+OOBVFcA/kdwf45n4H7ikwUTACS8sZAXBGnsRAn/7pU4BSaLZShyLWuNsYPv0lY3iykdA751LYBg/Y/gycdm0tkMPDVjE/kvI9XHPXd2cH6duraUmoUzCXjQH/bHnTr5U+oe8P3un4JHFr9/ru/oDtYXR7RWMEJhi3bLYIU9N5+j5AgMBAAEwDQYJKoZIhvcNAQELBQADggEBABF+bTuBdiwyF7qpbB7YxZnJ4FY/oz/CKeZ0t0v88HjFM5DGIoCXvzwjwVdCw7FMUZlfKV1038ECjeRpdcqKWSSx0vQehCXPpza0X+5D2ssZ9twIPB4tfh7edI8518lw7jN8cW1z3LlTEkAeK9VRsnC/WGVQAWZYpAsc2yl7whW+4IlI4IHudEgod5E2+j3X2OPQZZG+ALOQ1E2RHvmAyHmVu/LiVmakttW1v7v7VIP0Bp+5456UQL4W1d62gm3ihEIMbDcGJp5LjWedTbbB6hcqNX7GHQpzIIfkNzflT/QvhQ5eExHyD9gc2ZBIAI0JhGlxv55mCe1vjPCa1vpYn0E=
86af650a-fd4f-41aa-b872-421cb74cb6bb	58eb6127-303c-4afb-9953-5ae5d4e5ec40	priority	100
c92aa713-ce4b-4d2d-98c7-3c95b6fa0b6e	58eb6127-303c-4afb-9953-5ae5d4e5ec40	privateKey	MIIEowIBAAKCAQEAj+6HJEUwa1QUdvRFfZLvKUSMGNiBeHduSzPJwwiJr5HeKpQBSYH0pvTCTa1LCfOSfDYHR2UKPuqtjDInylofE4A0eFkxDEGVUrnNZLtozCDhBTpkN+30lFeG8B/uAK0O261sJVWkCyWP8uuRI8DbDAoe1VlXjYYYsVduROyd/jjgVRXAP5HcH+OZ+B+4pMFEwAkvLGQFwRp7EQJ/+6VOAUmi2Uoci1rjbGD79JWN4spHQO+dS2AYP2P4MnHZtLZDDw1YxP5LyPVxz13dnB+nbq2lJqFMwl40B/2x506+VPqHvD97p+CRxa/f67v6A7WF0e0VjBCYYt2y2CFPTefo+QIDAQABAoIBAAvz6JgsAMXhASQRzBWOFrtsHGBR1oqtDGVFYzuFoX2JlRQfH2490xXsXJh7zDAC4Mbc68TTkMLB1XC/l4tX6Z3Cd1kJOygD6EUq9fIoGB9fmaBIBZuZcMbC8AtP84Ft3b6Di+gb0Rg14drwEikQcoBw9LeOCFXR7YoIszkZF1PNwH2TQzw++8UDcyO9wDSJCKePDe9bfuALLMhSwQvJ0v+MFnZEt8F4SsZPTyf7rzb2r9bhCgf3PeI3xp5P+yP8It+ZcWuXGdo0jl3M6wMhbl+oyo7l8nj0Qx6EjbHPLZA4T5tRydOds2eqsEheNb2JIUMRjDT14IW7bS6bDk1GZXsCgYEAwPWDgWT9wV9Ayfj0zPZ/DmqW4M4wbMs0HhijknHgNFQGO4I0PHZQmphaoYzPMpGFWudGMhVp0OvleMm6RdveDz5hEuuZpO/vfU8Hbvmx88eUCZzt9xMGcuN5g0eTMctkv3J56lZgkY+G3TnRz3Fujwk0h09fTf22Zf9luUu7VysCgYEAvvSGtPC5Bylgpb+UGSz9OSbSo3xnF6FBCRpafTATeW1V75cYOd4NKm/5BQroSOU6ubYqikdUVisU6CghXc2K50ZGS5YO8+FaaDIyCL/mZieZEXOFsPEW+xRNzBBeg+83ArPGaE5w+LEcbjIjc9gEFiwMhxBDD0mppPIfJQJybmsCgYBgZgd1zGNtiM3QWVzLTNY6+Is58iiFIZt+8aY+Uj/FRLj8X9cC4cTxBu44SSV2SPcLbAFIoiQEKOWyS22Q2v/8M5jBoAxZNzk5LBM1oO8DMBDIIKtNxSZynSwmpspllQxXDLTVmqOR9NdWjD1CWpP2R44Dj2a4iujl2gsS4D/OkwKBgQCuLBmmdp8+IgauiDTMVdXOyFN+nRznV8UbSXdaEnzg19J3i8ikF57Io4NujwlaXAOZmsfogrExO3CBZQEyLyXQV7AMh52x4030Fjy+8FJ1wQ6D9Wvu/ahpcZaYWCHtk0cI5vNE9q9ilyGIfaEKq/DmTF1M4I9aJWVgNxwjRL0dawKBgCcfoSUNWb2v0Yek2bD2no32u46mNw2oJRuCXvzFxhx1Imr1vUEy519zqEqoYQl1Im7YHW1e3rblyaj9vE6NT1Sf16CQ/l+xiDnCPM7903Su1Ir15Jkd6hzpcU8l3nLuXalP91EDXGm7H2OFGVGiMgbys3L/KAmRZKr1tLUEJczH
d509ed5b-5943-4762-aacc-c4ca8558e5b7	58eb6127-303c-4afb-9953-5ae5d4e5ec40	keyUse	ENC
23adb410-de40-4def-9607-3962767f698e	58eb6127-303c-4afb-9953-5ae5d4e5ec40	algorithm	RSA-OAEP
\.


--
-- Data for Name: composite_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.composite_role (composite, child_role) FROM stdin;
33ace452-b68c-4888-beae-20ddf10736ed	6f196d67-62de-4587-b241-3331eb8abfc8
33ace452-b68c-4888-beae-20ddf10736ed	4a6b248d-8a5a-48f8-b742-e55d1b431072
33ace452-b68c-4888-beae-20ddf10736ed	21ad198e-bc21-47cf-a5cf-7dcd3c1efbe7
33ace452-b68c-4888-beae-20ddf10736ed	36b9b617-ce5c-404d-b77e-fd4ded3c9394
33ace452-b68c-4888-beae-20ddf10736ed	b0d1e610-bda3-4afb-aa64-99f10401cc15
33ace452-b68c-4888-beae-20ddf10736ed	63ccc837-810f-4755-a190-88aed15d84c2
33ace452-b68c-4888-beae-20ddf10736ed	5cd999c3-9967-47c9-85e6-8a144184d386
33ace452-b68c-4888-beae-20ddf10736ed	c072b6fe-ccd9-4d33-b14c-363936acb52c
33ace452-b68c-4888-beae-20ddf10736ed	7c737e6b-6d83-4be5-90c0-35a5b1fd0d87
33ace452-b68c-4888-beae-20ddf10736ed	8fbba102-b122-4ea4-9443-2b46b0a6ed7b
33ace452-b68c-4888-beae-20ddf10736ed	4f0d577a-c474-4cb9-b968-99fb95644509
33ace452-b68c-4888-beae-20ddf10736ed	9b9310cd-def0-4293-aaa3-4f082035e67b
33ace452-b68c-4888-beae-20ddf10736ed	57f3c009-3760-4e4d-bf45-c118eae98195
33ace452-b68c-4888-beae-20ddf10736ed	eee67b59-b468-47cb-b55c-9337077ea17e
33ace452-b68c-4888-beae-20ddf10736ed	233b0ff1-abe9-43ee-9ac0-3edeab6de994
33ace452-b68c-4888-beae-20ddf10736ed	4df16dd6-8ff1-4143-a347-f246958db6c1
33ace452-b68c-4888-beae-20ddf10736ed	c2242c67-339c-43d0-95fd-10017ae2e9bb
33ace452-b68c-4888-beae-20ddf10736ed	4f5457dd-8b47-446f-bad1-8b78ca1af439
36b9b617-ce5c-404d-b77e-fd4ded3c9394	233b0ff1-abe9-43ee-9ac0-3edeab6de994
36b9b617-ce5c-404d-b77e-fd4ded3c9394	4f5457dd-8b47-446f-bad1-8b78ca1af439
52e01ede-b6ad-4985-ab21-0eee35fee7b6	4de23a1e-a374-4f67-8261-4e078bdb0e38
b0d1e610-bda3-4afb-aa64-99f10401cc15	4df16dd6-8ff1-4143-a347-f246958db6c1
52e01ede-b6ad-4985-ab21-0eee35fee7b6	bf294699-cdf9-4402-8510-c56d9af46f6d
bf294699-cdf9-4402-8510-c56d9af46f6d	f1dd8a34-eefc-477a-9221-7a181841ab90
4e0f73a8-ddf0-45b2-9197-9f50092ac04e	63f1f4c9-85aa-423a-ae26-cdca65096e7b
33ace452-b68c-4888-beae-20ddf10736ed	3c575119-b8f9-49d1-8c5b-1bf881e73463
52e01ede-b6ad-4985-ab21-0eee35fee7b6	c3cac550-d778-4944-9d50-9eacc13819d2
52e01ede-b6ad-4985-ab21-0eee35fee7b6	b69d7097-a6eb-4228-903b-7578b110d8a1
\.


--
-- Data for Name: credential; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.credential (id, salt, type, user_id, created_date, user_label, secret_data, credential_data, priority) FROM stdin;
f8eac13b-a25d-4eca-a897-ba8bd38e320d	\N	password	699cc64d-e041-49c5-8cd9-2829e7618431	1700728697970	\N	{"value":"LDg89uaCv7H2ezkH1+Xgsa+5OoqR+ignel9clbED6c8=","salt":"yvvclkI6srykgp4BR5/BiA==","additionalParameters":{}}	{"hashIterations":27500,"algorithm":"pbkdf2-sha256","additionalParameters":{}}	10
11fd74de-af30-47d0-9760-c89ca67df846	\N	password	154b4fe9-0f22-47d2-bca8-a0df8e9fa2a3	1700729069655	\N	{"value":"qCJUCGwX8yCg88+eioDM6EK7XwjdoIjFsfERhwq5Vzo=","salt":"OiKCXV/f7D9h9FQr+D5aAQ==","additionalParameters":{}}	{"hashIterations":27500,"algorithm":"pbkdf2-sha256","additionalParameters":{}}	10
\.


--
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) FROM stdin;
1.0.0.Final-KEYCLOAK-5461	sthorger@redhat.com	META-INF/jpa-changelog-1.0.0.Final.xml	2023-11-23 08:38:14.894184	1	EXECUTED	8:bda77d94bf90182a1e30c24f1c155ec7	createTable tableName=APPLICATION_DEFAULT_ROLES; createTable tableName=CLIENT; createTable tableName=CLIENT_SESSION; createTable tableName=CLIENT_SESSION_ROLE; createTable tableName=COMPOSITE_ROLE; createTable tableName=CREDENTIAL; createTable tab...		\N	4.20.0	\N	\N	0728694538
1.0.0.Final-KEYCLOAK-5461	sthorger@redhat.com	META-INF/db2-jpa-changelog-1.0.0.Final.xml	2023-11-23 08:38:14.90044	2	MARK_RAN	8:1ecb330f30986693d1cba9ab579fa219	createTable tableName=APPLICATION_DEFAULT_ROLES; createTable tableName=CLIENT; createTable tableName=CLIENT_SESSION; createTable tableName=CLIENT_SESSION_ROLE; createTable tableName=COMPOSITE_ROLE; createTable tableName=CREDENTIAL; createTable tab...		\N	4.20.0	\N	\N	0728694538
1.1.0.Beta1	sthorger@redhat.com	META-INF/jpa-changelog-1.1.0.Beta1.xml	2023-11-23 08:38:14.938584	3	EXECUTED	8:cb7ace19bc6d959f305605d255d4c843	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=CLIENT_ATTRIBUTES; createTable tableName=CLIENT_SESSION_NOTE; createTable tableName=APP_NODE_REGISTRATIONS; addColumn table...		\N	4.20.0	\N	\N	0728694538
1.1.0.Final	sthorger@redhat.com	META-INF/jpa-changelog-1.1.0.Final.xml	2023-11-23 08:38:14.942357	4	EXECUTED	8:80230013e961310e6872e871be424a63	renameColumn newColumnName=EVENT_TIME, oldColumnName=TIME, tableName=EVENT_ENTITY		\N	4.20.0	\N	\N	0728694538
1.2.0.Beta1	psilva@redhat.com	META-INF/jpa-changelog-1.2.0.Beta1.xml	2023-11-23 08:38:15.018994	5	EXECUTED	8:67f4c20929126adc0c8e9bf48279d244	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=PROTOCOL_MAPPER; createTable tableName=PROTOCOL_MAPPER_CONFIG; createTable tableName=...		\N	4.20.0	\N	\N	0728694538
1.2.0.Beta1	psilva@redhat.com	META-INF/db2-jpa-changelog-1.2.0.Beta1.xml	2023-11-23 08:38:15.020553	6	MARK_RAN	8:7311018b0b8179ce14628ab412bb6783	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=PROTOCOL_MAPPER; createTable tableName=PROTOCOL_MAPPER_CONFIG; createTable tableName=...		\N	4.20.0	\N	\N	0728694538
1.2.0.RC1	bburke@redhat.com	META-INF/jpa-changelog-1.2.0.CR1.xml	2023-11-23 08:38:15.069932	7	EXECUTED	8:037ba1216c3640f8785ee6b8e7c8e3c1	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=MIGRATION_MODEL; createTable tableName=IDENTITY_P...		\N	4.20.0	\N	\N	0728694538
1.2.0.RC1	bburke@redhat.com	META-INF/db2-jpa-changelog-1.2.0.CR1.xml	2023-11-23 08:38:15.072002	8	MARK_RAN	8:7fe6ffe4af4df289b3157de32c624263	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=MIGRATION_MODEL; createTable tableName=IDENTITY_P...		\N	4.20.0	\N	\N	0728694538
1.2.0.Final	keycloak	META-INF/jpa-changelog-1.2.0.Final.xml	2023-11-23 08:38:15.076519	9	EXECUTED	8:9c136bc3187083a98745c7d03bc8a303	update tableName=CLIENT; update tableName=CLIENT; update tableName=CLIENT		\N	4.20.0	\N	\N	0728694538
1.3.0	bburke@redhat.com	META-INF/jpa-changelog-1.3.0.xml	2023-11-23 08:38:15.132417	10	EXECUTED	8:b5f09474dca81fb56a97cf5b6553d331	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=ADMI...		\N	4.20.0	\N	\N	0728694538
1.4.0	bburke@redhat.com	META-INF/jpa-changelog-1.4.0.xml	2023-11-23 08:38:15.164229	11	EXECUTED	8:ca924f31bd2a3b219fdcfe78c82dacf4	delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...		\N	4.20.0	\N	\N	0728694538
1.4.0	bburke@redhat.com	META-INF/db2-jpa-changelog-1.4.0.xml	2023-11-23 08:38:15.165516	12	MARK_RAN	8:8acad7483e106416bcfa6f3b824a16cd	delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...		\N	4.20.0	\N	\N	0728694538
1.5.0	bburke@redhat.com	META-INF/jpa-changelog-1.5.0.xml	2023-11-23 08:38:15.176852	13	EXECUTED	8:9b1266d17f4f87c78226f5055408fd5e	delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...		\N	4.20.0	\N	\N	0728694538
1.6.1_from15	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2023-11-23 08:38:15.189374	14	EXECUTED	8:d80ec4ab6dbfe573550ff72396c7e910	addColumn tableName=REALM; addColumn tableName=KEYCLOAK_ROLE; addColumn tableName=CLIENT; createTable tableName=OFFLINE_USER_SESSION; createTable tableName=OFFLINE_CLIENT_SESSION; addPrimaryKey constraintName=CONSTRAINT_OFFL_US_SES_PK2, tableName=...		\N	4.20.0	\N	\N	0728694538
1.6.1_from16-pre	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2023-11-23 08:38:15.190838	15	MARK_RAN	8:d86eb172171e7c20b9c849b584d147b2	delete tableName=OFFLINE_CLIENT_SESSION; delete tableName=OFFLINE_USER_SESSION		\N	4.20.0	\N	\N	0728694538
1.6.1_from16	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2023-11-23 08:38:15.19238	16	MARK_RAN	8:5735f46f0fa60689deb0ecdc2a0dea22	dropPrimaryKey constraintName=CONSTRAINT_OFFLINE_US_SES_PK, tableName=OFFLINE_USER_SESSION; dropPrimaryKey constraintName=CONSTRAINT_OFFLINE_CL_SES_PK, tableName=OFFLINE_CLIENT_SESSION; addColumn tableName=OFFLINE_USER_SESSION; update tableName=OF...		\N	4.20.0	\N	\N	0728694538
1.6.1	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2023-11-23 08:38:15.194383	17	EXECUTED	8:d41d8cd98f00b204e9800998ecf8427e	empty		\N	4.20.0	\N	\N	0728694538
1.7.0	bburke@redhat.com	META-INF/jpa-changelog-1.7.0.xml	2023-11-23 08:38:15.218103	18	EXECUTED	8:5c1a8fd2014ac7fc43b90a700f117b23	createTable tableName=KEYCLOAK_GROUP; createTable tableName=GROUP_ROLE_MAPPING; createTable tableName=GROUP_ATTRIBUTE; createTable tableName=USER_GROUP_MEMBERSHIP; createTable tableName=REALM_DEFAULT_GROUPS; addColumn tableName=IDENTITY_PROVIDER; ...		\N	4.20.0	\N	\N	0728694538
1.8.0	mposolda@redhat.com	META-INF/jpa-changelog-1.8.0.xml	2023-11-23 08:38:15.246974	19	EXECUTED	8:1f6c2c2dfc362aff4ed75b3f0ef6b331	addColumn tableName=IDENTITY_PROVIDER; createTable tableName=CLIENT_TEMPLATE; createTable tableName=CLIENT_TEMPLATE_ATTRIBUTES; createTable tableName=TEMPLATE_SCOPE_MAPPING; dropNotNullConstraint columnName=CLIENT_ID, tableName=PROTOCOL_MAPPER; ad...		\N	4.20.0	\N	\N	0728694538
1.8.0-2	keycloak	META-INF/jpa-changelog-1.8.0.xml	2023-11-23 08:38:15.250572	20	EXECUTED	8:dee9246280915712591f83a127665107	dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; update tableName=CREDENTIAL		\N	4.20.0	\N	\N	0728694538
1.8.0	mposolda@redhat.com	META-INF/db2-jpa-changelog-1.8.0.xml	2023-11-23 08:38:15.251762	21	MARK_RAN	8:9eb2ee1fa8ad1c5e426421a6f8fdfa6a	addColumn tableName=IDENTITY_PROVIDER; createTable tableName=CLIENT_TEMPLATE; createTable tableName=CLIENT_TEMPLATE_ATTRIBUTES; createTable tableName=TEMPLATE_SCOPE_MAPPING; dropNotNullConstraint columnName=CLIENT_ID, tableName=PROTOCOL_MAPPER; ad...		\N	4.20.0	\N	\N	0728694538
1.8.0-2	keycloak	META-INF/db2-jpa-changelog-1.8.0.xml	2023-11-23 08:38:15.253717	22	MARK_RAN	8:dee9246280915712591f83a127665107	dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; update tableName=CREDENTIAL		\N	4.20.0	\N	\N	0728694538
1.9.0	mposolda@redhat.com	META-INF/jpa-changelog-1.9.0.xml	2023-11-23 08:38:15.269251	23	EXECUTED	8:d9fa18ffa355320395b86270680dd4fe	update tableName=REALM; update tableName=REALM; update tableName=REALM; update tableName=REALM; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=REALM; update tableName=REALM; customChange; dr...		\N	4.20.0	\N	\N	0728694538
1.9.1	keycloak	META-INF/jpa-changelog-1.9.1.xml	2023-11-23 08:38:15.274095	24	EXECUTED	8:90cff506fedb06141ffc1c71c4a1214c	modifyDataType columnName=PRIVATE_KEY, tableName=REALM; modifyDataType columnName=PUBLIC_KEY, tableName=REALM; modifyDataType columnName=CERTIFICATE, tableName=REALM		\N	4.20.0	\N	\N	0728694538
1.9.1	keycloak	META-INF/db2-jpa-changelog-1.9.1.xml	2023-11-23 08:38:15.275266	25	MARK_RAN	8:11a788aed4961d6d29c427c063af828c	modifyDataType columnName=PRIVATE_KEY, tableName=REALM; modifyDataType columnName=CERTIFICATE, tableName=REALM		\N	4.20.0	\N	\N	0728694538
1.9.2	keycloak	META-INF/jpa-changelog-1.9.2.xml	2023-11-23 08:38:15.293764	26	EXECUTED	8:a4218e51e1faf380518cce2af5d39b43	createIndex indexName=IDX_USER_EMAIL, tableName=USER_ENTITY; createIndex indexName=IDX_USER_ROLE_MAPPING, tableName=USER_ROLE_MAPPING; createIndex indexName=IDX_USER_GROUP_MAPPING, tableName=USER_GROUP_MEMBERSHIP; createIndex indexName=IDX_USER_CO...		\N	4.20.0	\N	\N	0728694538
authz-2.0.0	psilva@redhat.com	META-INF/jpa-changelog-authz-2.0.0.xml	2023-11-23 08:38:15.337163	27	EXECUTED	8:d9e9a1bfaa644da9952456050f07bbdc	createTable tableName=RESOURCE_SERVER; addPrimaryKey constraintName=CONSTRAINT_FARS, tableName=RESOURCE_SERVER; addUniqueConstraint constraintName=UK_AU8TT6T700S9V50BU18WS5HA6, tableName=RESOURCE_SERVER; createTable tableName=RESOURCE_SERVER_RESOU...		\N	4.20.0	\N	\N	0728694538
authz-2.5.1	psilva@redhat.com	META-INF/jpa-changelog-authz-2.5.1.xml	2023-11-23 08:38:15.33994	28	EXECUTED	8:d1bf991a6163c0acbfe664b615314505	update tableName=RESOURCE_SERVER_POLICY		\N	4.20.0	\N	\N	0728694538
2.1.0-KEYCLOAK-5461	bburke@redhat.com	META-INF/jpa-changelog-2.1.0.xml	2023-11-23 08:38:15.37908	29	EXECUTED	8:88a743a1e87ec5e30bf603da68058a8c	createTable tableName=BROKER_LINK; createTable tableName=FED_USER_ATTRIBUTE; createTable tableName=FED_USER_CONSENT; createTable tableName=FED_USER_CONSENT_ROLE; createTable tableName=FED_USER_CONSENT_PROT_MAPPER; createTable tableName=FED_USER_CR...		\N	4.20.0	\N	\N	0728694538
2.2.0	bburke@redhat.com	META-INF/jpa-changelog-2.2.0.xml	2023-11-23 08:38:15.388613	30	EXECUTED	8:c5517863c875d325dea463d00ec26d7a	addColumn tableName=ADMIN_EVENT_ENTITY; createTable tableName=CREDENTIAL_ATTRIBUTE; createTable tableName=FED_CREDENTIAL_ATTRIBUTE; modifyDataType columnName=VALUE, tableName=CREDENTIAL; addForeignKeyConstraint baseTableName=FED_CREDENTIAL_ATTRIBU...		\N	4.20.0	\N	\N	0728694538
2.3.0	bburke@redhat.com	META-INF/jpa-changelog-2.3.0.xml	2023-11-23 08:38:15.4043	31	EXECUTED	8:ada8b4833b74a498f376d7136bc7d327	createTable tableName=FEDERATED_USER; addPrimaryKey constraintName=CONSTR_FEDERATED_USER, tableName=FEDERATED_USER; dropDefaultValue columnName=TOTP, tableName=USER_ENTITY; dropColumn columnName=TOTP, tableName=USER_ENTITY; addColumn tableName=IDE...		\N	4.20.0	\N	\N	0728694538
2.4.0	bburke@redhat.com	META-INF/jpa-changelog-2.4.0.xml	2023-11-23 08:38:15.410034	32	EXECUTED	8:b9b73c8ea7299457f99fcbb825c263ba	customChange		\N	4.20.0	\N	\N	0728694538
2.5.0	bburke@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2023-11-23 08:38:15.416751	33	EXECUTED	8:07724333e625ccfcfc5adc63d57314f3	customChange; modifyDataType columnName=USER_ID, tableName=OFFLINE_USER_SESSION		\N	4.20.0	\N	\N	0728694538
2.5.0-unicode-oracle	hmlnarik@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2023-11-23 08:38:15.41815	34	MARK_RAN	8:8b6fd445958882efe55deb26fc541a7b	modifyDataType columnName=DESCRIPTION, tableName=AUTHENTICATION_FLOW; modifyDataType columnName=DESCRIPTION, tableName=CLIENT_TEMPLATE; modifyDataType columnName=DESCRIPTION, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=DESCRIPTION,...		\N	4.20.0	\N	\N	0728694538
2.5.0-unicode-other-dbs	hmlnarik@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2023-11-23 08:38:15.43656	35	EXECUTED	8:29b29cfebfd12600897680147277a9d7	modifyDataType columnName=DESCRIPTION, tableName=AUTHENTICATION_FLOW; modifyDataType columnName=DESCRIPTION, tableName=CLIENT_TEMPLATE; modifyDataType columnName=DESCRIPTION, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=DESCRIPTION,...		\N	4.20.0	\N	\N	0728694538
2.5.0-duplicate-email-support	slawomir@dabek.name	META-INF/jpa-changelog-2.5.0.xml	2023-11-23 08:38:15.440329	36	EXECUTED	8:73ad77ca8fd0410c7f9f15a471fa52bc	addColumn tableName=REALM		\N	4.20.0	\N	\N	0728694538
2.5.0-unique-group-names	hmlnarik@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2023-11-23 08:38:15.44476	37	EXECUTED	8:64f27a6fdcad57f6f9153210f2ec1bdb	addUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP		\N	4.20.0	\N	\N	0728694538
2.5.1	bburke@redhat.com	META-INF/jpa-changelog-2.5.1.xml	2023-11-23 08:38:15.447867	38	EXECUTED	8:27180251182e6c31846c2ddab4bc5781	addColumn tableName=FED_USER_CONSENT		\N	4.20.0	\N	\N	0728694538
3.0.0	bburke@redhat.com	META-INF/jpa-changelog-3.0.0.xml	2023-11-23 08:38:15.450585	39	EXECUTED	8:d56f201bfcfa7a1413eb3e9bc02978f9	addColumn tableName=IDENTITY_PROVIDER		\N	4.20.0	\N	\N	0728694538
3.2.0-fix	keycloak	META-INF/jpa-changelog-3.2.0.xml	2023-11-23 08:38:15.451717	40	MARK_RAN	8:91f5522bf6afdc2077dfab57fbd3455c	addNotNullConstraint columnName=REALM_ID, tableName=CLIENT_INITIAL_ACCESS		\N	4.20.0	\N	\N	0728694538
3.2.0-fix-with-keycloak-5416	keycloak	META-INF/jpa-changelog-3.2.0.xml	2023-11-23 08:38:15.453192	41	MARK_RAN	8:0f01b554f256c22caeb7d8aee3a1cdc8	dropIndex indexName=IDX_CLIENT_INIT_ACC_REALM, tableName=CLIENT_INITIAL_ACCESS; addNotNullConstraint columnName=REALM_ID, tableName=CLIENT_INITIAL_ACCESS; createIndex indexName=IDX_CLIENT_INIT_ACC_REALM, tableName=CLIENT_INITIAL_ACCESS		\N	4.20.0	\N	\N	0728694538
3.2.0-fix-offline-sessions	hmlnarik	META-INF/jpa-changelog-3.2.0.xml	2023-11-23 08:38:15.459227	42	EXECUTED	8:ab91cf9cee415867ade0e2df9651a947	customChange		\N	4.20.0	\N	\N	0728694538
3.2.0-fixed	keycloak	META-INF/jpa-changelog-3.2.0.xml	2023-11-23 08:38:15.529079	43	EXECUTED	8:ceac9b1889e97d602caf373eadb0d4b7	addColumn tableName=REALM; dropPrimaryKey constraintName=CONSTRAINT_OFFL_CL_SES_PK2, tableName=OFFLINE_CLIENT_SESSION; dropColumn columnName=CLIENT_SESSION_ID, tableName=OFFLINE_CLIENT_SESSION; addPrimaryKey constraintName=CONSTRAINT_OFFL_CL_SES_P...		\N	4.20.0	\N	\N	0728694538
3.3.0	keycloak	META-INF/jpa-changelog-3.3.0.xml	2023-11-23 08:38:15.531616	44	EXECUTED	8:84b986e628fe8f7fd8fd3c275c5259f2	addColumn tableName=USER_ENTITY		\N	4.20.0	\N	\N	0728694538
authz-3.4.0.CR1-resource-server-pk-change-part1	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2023-11-23 08:38:15.53417	45	EXECUTED	8:a164ae073c56ffdbc98a615493609a52	addColumn tableName=RESOURCE_SERVER_POLICY; addColumn tableName=RESOURCE_SERVER_RESOURCE; addColumn tableName=RESOURCE_SERVER_SCOPE		\N	4.20.0	\N	\N	0728694538
authz-3.4.0.CR1-resource-server-pk-change-part2-KEYCLOAK-6095	hmlnarik@redhat.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2023-11-23 08:38:15.540385	46	EXECUTED	8:70a2b4f1f4bd4dbf487114bdb1810e64	customChange		\N	4.20.0	\N	\N	0728694538
authz-3.4.0.CR1-resource-server-pk-change-part3-fixed	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2023-11-23 08:38:15.541441	47	MARK_RAN	8:7be68b71d2f5b94b8df2e824f2860fa2	dropIndex indexName=IDX_RES_SERV_POL_RES_SERV, tableName=RESOURCE_SERVER_POLICY; dropIndex indexName=IDX_RES_SRV_RES_RES_SRV, tableName=RESOURCE_SERVER_RESOURCE; dropIndex indexName=IDX_RES_SRV_SCOPE_RES_SRV, tableName=RESOURCE_SERVER_SCOPE		\N	4.20.0	\N	\N	0728694538
authz-3.4.0.CR1-resource-server-pk-change-part3-fixed-nodropindex	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2023-11-23 08:38:15.563192	48	EXECUTED	8:bab7c631093c3861d6cf6144cd944982	addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, tableName=RESOURCE_SERVER_POLICY; addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, tableName=RESOURCE_SERVER_RESOURCE; addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, ...		\N	4.20.0	\N	\N	0728694538
authn-3.4.0.CR1-refresh-token-max-reuse	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2023-11-23 08:38:15.566556	49	EXECUTED	8:fa809ac11877d74d76fe40869916daad	addColumn tableName=REALM		\N	4.20.0	\N	\N	0728694538
3.4.0	keycloak	META-INF/jpa-changelog-3.4.0.xml	2023-11-23 08:38:15.590564	50	EXECUTED	8:fac23540a40208f5f5e326f6ceb4d291	addPrimaryKey constraintName=CONSTRAINT_REALM_DEFAULT_ROLES, tableName=REALM_DEFAULT_ROLES; addPrimaryKey constraintName=CONSTRAINT_COMPOSITE_ROLE, tableName=COMPOSITE_ROLE; addPrimaryKey constraintName=CONSTR_REALM_DEFAULT_GROUPS, tableName=REALM...		\N	4.20.0	\N	\N	0728694538
3.4.0-KEYCLOAK-5230	hmlnarik@redhat.com	META-INF/jpa-changelog-3.4.0.xml	2023-11-23 08:38:15.606721	51	EXECUTED	8:2612d1b8a97e2b5588c346e817307593	createIndex indexName=IDX_FU_ATTRIBUTE, tableName=FED_USER_ATTRIBUTE; createIndex indexName=IDX_FU_CONSENT, tableName=FED_USER_CONSENT; createIndex indexName=IDX_FU_CONSENT_RU, tableName=FED_USER_CONSENT; createIndex indexName=IDX_FU_CREDENTIAL, t...		\N	4.20.0	\N	\N	0728694538
3.4.1	psilva@redhat.com	META-INF/jpa-changelog-3.4.1.xml	2023-11-23 08:38:15.609304	52	EXECUTED	8:9842f155c5db2206c88bcb5d1046e941	modifyDataType columnName=VALUE, tableName=CLIENT_ATTRIBUTES		\N	4.20.0	\N	\N	0728694538
3.4.2	keycloak	META-INF/jpa-changelog-3.4.2.xml	2023-11-23 08:38:15.610995	53	EXECUTED	8:2e12e06e45498406db72d5b3da5bbc76	update tableName=REALM		\N	4.20.0	\N	\N	0728694538
3.4.2-KEYCLOAK-5172	mkanis@redhat.com	META-INF/jpa-changelog-3.4.2.xml	2023-11-23 08:38:15.612723	54	EXECUTED	8:33560e7c7989250c40da3abdabdc75a4	update tableName=CLIENT		\N	4.20.0	\N	\N	0728694538
4.0.0-KEYCLOAK-6335	bburke@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2023-11-23 08:38:15.617142	55	EXECUTED	8:87a8d8542046817a9107c7eb9cbad1cd	createTable tableName=CLIENT_AUTH_FLOW_BINDINGS; addPrimaryKey constraintName=C_CLI_FLOW_BIND, tableName=CLIENT_AUTH_FLOW_BINDINGS		\N	4.20.0	\N	\N	0728694538
4.0.0-CLEANUP-UNUSED-TABLE	bburke@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2023-11-23 08:38:15.620796	56	EXECUTED	8:3ea08490a70215ed0088c273d776311e	dropTable tableName=CLIENT_IDENTITY_PROV_MAPPING		\N	4.20.0	\N	\N	0728694538
4.0.0-KEYCLOAK-6228	bburke@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2023-11-23 08:38:15.631493	57	EXECUTED	8:2d56697c8723d4592ab608ce14b6ed68	dropUniqueConstraint constraintName=UK_JKUWUVD56ONTGSUHOGM8UEWRT, tableName=USER_CONSENT; dropNotNullConstraint columnName=CLIENT_ID, tableName=USER_CONSENT; addColumn tableName=USER_CONSENT; addUniqueConstraint constraintName=UK_JKUWUVD56ONTGSUHO...		\N	4.20.0	\N	\N	0728694538
4.0.0-KEYCLOAK-5579-fixed	mposolda@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2023-11-23 08:38:15.681445	58	EXECUTED	8:3e423e249f6068ea2bbe48bf907f9d86	dropForeignKeyConstraint baseTableName=CLIENT_TEMPLATE_ATTRIBUTES, constraintName=FK_CL_TEMPL_ATTR_TEMPL; renameTable newTableName=CLIENT_SCOPE_ATTRIBUTES, oldTableName=CLIENT_TEMPLATE_ATTRIBUTES; renameColumn newColumnName=SCOPE_ID, oldColumnName...		\N	4.20.0	\N	\N	0728694538
authz-4.0.0.CR1	psilva@redhat.com	META-INF/jpa-changelog-authz-4.0.0.CR1.xml	2023-11-23 08:38:15.694793	59	EXECUTED	8:15cabee5e5df0ff099510a0fc03e4103	createTable tableName=RESOURCE_SERVER_PERM_TICKET; addPrimaryKey constraintName=CONSTRAINT_FAPMT, tableName=RESOURCE_SERVER_PERM_TICKET; addForeignKeyConstraint baseTableName=RESOURCE_SERVER_PERM_TICKET, constraintName=FK_FRSRHO213XCX4WNKOG82SSPMT...		\N	4.20.0	\N	\N	0728694538
authz-4.0.0.Beta3	psilva@redhat.com	META-INF/jpa-changelog-authz-4.0.0.Beta3.xml	2023-11-23 08:38:15.69788	60	EXECUTED	8:4b80200af916ac54d2ffbfc47918ab0e	addColumn tableName=RESOURCE_SERVER_POLICY; addColumn tableName=RESOURCE_SERVER_PERM_TICKET; addForeignKeyConstraint baseTableName=RESOURCE_SERVER_PERM_TICKET, constraintName=FK_FRSRPO2128CX4WNKOG82SSRFY, referencedTableName=RESOURCE_SERVER_POLICY		\N	4.20.0	\N	\N	0728694538
authz-4.2.0.Final	mhajas@redhat.com	META-INF/jpa-changelog-authz-4.2.0.Final.xml	2023-11-23 08:38:15.704321	61	EXECUTED	8:66564cd5e168045d52252c5027485bbb	createTable tableName=RESOURCE_URIS; addForeignKeyConstraint baseTableName=RESOURCE_URIS, constraintName=FK_RESOURCE_SERVER_URIS, referencedTableName=RESOURCE_SERVER_RESOURCE; customChange; dropColumn columnName=URI, tableName=RESOURCE_SERVER_RESO...		\N	4.20.0	\N	\N	0728694538
authz-4.2.0.Final-KEYCLOAK-9944	hmlnarik@redhat.com	META-INF/jpa-changelog-authz-4.2.0.Final.xml	2023-11-23 08:38:15.707355	62	EXECUTED	8:1c7064fafb030222be2bd16ccf690f6f	addPrimaryKey constraintName=CONSTRAINT_RESOUR_URIS_PK, tableName=RESOURCE_URIS		\N	4.20.0	\N	\N	0728694538
4.2.0-KEYCLOAK-6313	wadahiro@gmail.com	META-INF/jpa-changelog-4.2.0.xml	2023-11-23 08:38:15.709872	63	EXECUTED	8:2de18a0dce10cdda5c7e65c9b719b6e5	addColumn tableName=REQUIRED_ACTION_PROVIDER		\N	4.20.0	\N	\N	0728694538
4.3.0-KEYCLOAK-7984	wadahiro@gmail.com	META-INF/jpa-changelog-4.3.0.xml	2023-11-23 08:38:15.7116	64	EXECUTED	8:03e413dd182dcbd5c57e41c34d0ef682	update tableName=REQUIRED_ACTION_PROVIDER		\N	4.20.0	\N	\N	0728694538
4.6.0-KEYCLOAK-7950	psilva@redhat.com	META-INF/jpa-changelog-4.6.0.xml	2023-11-23 08:38:15.713522	65	EXECUTED	8:d27b42bb2571c18fbe3fe4e4fb7582a7	update tableName=RESOURCE_SERVER_RESOURCE		\N	4.20.0	\N	\N	0728694538
4.6.0-KEYCLOAK-8377	keycloak	META-INF/jpa-changelog-4.6.0.xml	2023-11-23 08:38:15.721258	66	EXECUTED	8:698baf84d9fd0027e9192717c2154fb8	createTable tableName=ROLE_ATTRIBUTE; addPrimaryKey constraintName=CONSTRAINT_ROLE_ATTRIBUTE_PK, tableName=ROLE_ATTRIBUTE; addForeignKeyConstraint baseTableName=ROLE_ATTRIBUTE, constraintName=FK_ROLE_ATTRIBUTE_ID, referencedTableName=KEYCLOAK_ROLE...		\N	4.20.0	\N	\N	0728694538
4.6.0-KEYCLOAK-8555	gideonray@gmail.com	META-INF/jpa-changelog-4.6.0.xml	2023-11-23 08:38:15.724569	67	EXECUTED	8:ced8822edf0f75ef26eb51582f9a821a	createIndex indexName=IDX_COMPONENT_PROVIDER_TYPE, tableName=COMPONENT		\N	4.20.0	\N	\N	0728694538
4.7.0-KEYCLOAK-1267	sguilhen@redhat.com	META-INF/jpa-changelog-4.7.0.xml	2023-11-23 08:38:15.72757	68	EXECUTED	8:f0abba004cf429e8afc43056df06487d	addColumn tableName=REALM		\N	4.20.0	\N	\N	0728694538
4.7.0-KEYCLOAK-7275	keycloak	META-INF/jpa-changelog-4.7.0.xml	2023-11-23 08:38:15.735897	69	EXECUTED	8:6662f8b0b611caa359fcf13bf63b4e24	renameColumn newColumnName=CREATED_ON, oldColumnName=LAST_SESSION_REFRESH, tableName=OFFLINE_USER_SESSION; addNotNullConstraint columnName=CREATED_ON, tableName=OFFLINE_USER_SESSION; addColumn tableName=OFFLINE_USER_SESSION; customChange; createIn...		\N	4.20.0	\N	\N	0728694538
4.8.0-KEYCLOAK-8835	sguilhen@redhat.com	META-INF/jpa-changelog-4.8.0.xml	2023-11-23 08:38:15.739114	70	EXECUTED	8:9e6b8009560f684250bdbdf97670d39e	addNotNullConstraint columnName=SSO_MAX_LIFESPAN_REMEMBER_ME, tableName=REALM; addNotNullConstraint columnName=SSO_IDLE_TIMEOUT_REMEMBER_ME, tableName=REALM		\N	4.20.0	\N	\N	0728694538
authz-7.0.0-KEYCLOAK-10443	psilva@redhat.com	META-INF/jpa-changelog-authz-7.0.0.xml	2023-11-23 08:38:15.741383	71	EXECUTED	8:4223f561f3b8dc655846562b57bb502e	addColumn tableName=RESOURCE_SERVER		\N	4.20.0	\N	\N	0728694538
8.0.0-adding-credential-columns	keycloak	META-INF/jpa-changelog-8.0.0.xml	2023-11-23 08:38:15.744639	72	EXECUTED	8:215a31c398b363ce383a2b301202f29e	addColumn tableName=CREDENTIAL; addColumn tableName=FED_USER_CREDENTIAL		\N	4.20.0	\N	\N	0728694538
8.0.0-updating-credential-data-not-oracle-fixed	keycloak	META-INF/jpa-changelog-8.0.0.xml	2023-11-23 08:38:15.748125	73	EXECUTED	8:83f7a671792ca98b3cbd3a1a34862d3d	update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL		\N	4.20.0	\N	\N	0728694538
8.0.0-updating-credential-data-oracle-fixed	keycloak	META-INF/jpa-changelog-8.0.0.xml	2023-11-23 08:38:15.749164	74	MARK_RAN	8:f58ad148698cf30707a6efbdf8061aa7	update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL		\N	4.20.0	\N	\N	0728694538
8.0.0-credential-cleanup-fixed	keycloak	META-INF/jpa-changelog-8.0.0.xml	2023-11-23 08:38:15.758202	75	EXECUTED	8:79e4fd6c6442980e58d52ffc3ee7b19c	dropDefaultValue columnName=COUNTER, tableName=CREDENTIAL; dropDefaultValue columnName=DIGITS, tableName=CREDENTIAL; dropDefaultValue columnName=PERIOD, tableName=CREDENTIAL; dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; dropColumn ...		\N	4.20.0	\N	\N	0728694538
8.0.0-resource-tag-support	keycloak	META-INF/jpa-changelog-8.0.0.xml	2023-11-23 08:38:15.762597	76	EXECUTED	8:87af6a1e6d241ca4b15801d1f86a297d	addColumn tableName=MIGRATION_MODEL; createIndex indexName=IDX_UPDATE_TIME, tableName=MIGRATION_MODEL		\N	4.20.0	\N	\N	0728694538
9.0.0-always-display-client	keycloak	META-INF/jpa-changelog-9.0.0.xml	2023-11-23 08:38:15.764956	77	EXECUTED	8:b44f8d9b7b6ea455305a6d72a200ed15	addColumn tableName=CLIENT		\N	4.20.0	\N	\N	0728694538
9.0.0-drop-constraints-for-column-increase	keycloak	META-INF/jpa-changelog-9.0.0.xml	2023-11-23 08:38:15.766012	78	MARK_RAN	8:2d8ed5aaaeffd0cb004c046b4a903ac5	dropUniqueConstraint constraintName=UK_FRSR6T700S9V50BU18WS5PMT, tableName=RESOURCE_SERVER_PERM_TICKET; dropUniqueConstraint constraintName=UK_FRSR6T700S9V50BU18WS5HA6, tableName=RESOURCE_SERVER_RESOURCE; dropPrimaryKey constraintName=CONSTRAINT_O...		\N	4.20.0	\N	\N	0728694538
9.0.0-increase-column-size-federated-fk	keycloak	META-INF/jpa-changelog-9.0.0.xml	2023-11-23 08:38:15.777305	79	EXECUTED	8:e290c01fcbc275326c511633f6e2acde	modifyDataType columnName=CLIENT_ID, tableName=FED_USER_CONSENT; modifyDataType columnName=CLIENT_REALM_CONSTRAINT, tableName=KEYCLOAK_ROLE; modifyDataType columnName=OWNER, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=CLIENT_ID, ta...		\N	4.20.0	\N	\N	0728694538
9.0.0-recreate-constraints-after-column-increase	keycloak	META-INF/jpa-changelog-9.0.0.xml	2023-11-23 08:38:15.778856	80	MARK_RAN	8:c9db8784c33cea210872ac2d805439f8	addNotNullConstraint columnName=CLIENT_ID, tableName=OFFLINE_CLIENT_SESSION; addNotNullConstraint columnName=OWNER, tableName=RESOURCE_SERVER_PERM_TICKET; addNotNullConstraint columnName=REQUESTER, tableName=RESOURCE_SERVER_PERM_TICKET; addNotNull...		\N	4.20.0	\N	\N	0728694538
9.0.1-add-index-to-client.client_id	keycloak	META-INF/jpa-changelog-9.0.1.xml	2023-11-23 08:38:15.783631	81	EXECUTED	8:95b676ce8fc546a1fcfb4c92fae4add5	createIndex indexName=IDX_CLIENT_ID, tableName=CLIENT		\N	4.20.0	\N	\N	0728694538
9.0.1-KEYCLOAK-12579-drop-constraints	keycloak	META-INF/jpa-changelog-9.0.1.xml	2023-11-23 08:38:15.784989	82	MARK_RAN	8:38a6b2a41f5651018b1aca93a41401e5	dropUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP		\N	4.20.0	\N	\N	0728694538
9.0.1-KEYCLOAK-12579-add-not-null-constraint	keycloak	META-INF/jpa-changelog-9.0.1.xml	2023-11-23 08:38:15.788582	83	EXECUTED	8:3fb99bcad86a0229783123ac52f7609c	addNotNullConstraint columnName=PARENT_GROUP, tableName=KEYCLOAK_GROUP		\N	4.20.0	\N	\N	0728694538
9.0.1-KEYCLOAK-12579-recreate-constraints	keycloak	META-INF/jpa-changelog-9.0.1.xml	2023-11-23 08:38:15.789878	84	MARK_RAN	8:64f27a6fdcad57f6f9153210f2ec1bdb	addUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP		\N	4.20.0	\N	\N	0728694538
9.0.1-add-index-to-events	keycloak	META-INF/jpa-changelog-9.0.1.xml	2023-11-23 08:38:15.794467	85	EXECUTED	8:ab4f863f39adafd4c862f7ec01890abc	createIndex indexName=IDX_EVENT_TIME, tableName=EVENT_ENTITY		\N	4.20.0	\N	\N	0728694538
map-remove-ri	keycloak	META-INF/jpa-changelog-11.0.0.xml	2023-11-23 08:38:15.798683	86	EXECUTED	8:13c419a0eb336e91ee3a3bf8fda6e2a7	dropForeignKeyConstraint baseTableName=REALM, constraintName=FK_TRAF444KK6QRKMS7N56AIWQ5Y; dropForeignKeyConstraint baseTableName=KEYCLOAK_ROLE, constraintName=FK_KJHO5LE2C0RAL09FL8CM9WFW9		\N	4.20.0	\N	\N	0728694538
map-remove-ri	keycloak	META-INF/jpa-changelog-12.0.0.xml	2023-11-23 08:38:15.803932	87	EXECUTED	8:e3fb1e698e0471487f51af1ed80fe3ac	dropForeignKeyConstraint baseTableName=REALM_DEFAULT_GROUPS, constraintName=FK_DEF_GROUPS_GROUP; dropForeignKeyConstraint baseTableName=REALM_DEFAULT_ROLES, constraintName=FK_H4WPD7W4HSOOLNI3H0SW7BTJE; dropForeignKeyConstraint baseTableName=CLIENT...		\N	4.20.0	\N	\N	0728694538
12.1.0-add-realm-localization-table	keycloak	META-INF/jpa-changelog-12.0.0.xml	2023-11-23 08:38:15.810168	88	EXECUTED	8:babadb686aab7b56562817e60bf0abd0	createTable tableName=REALM_LOCALIZATIONS; addPrimaryKey tableName=REALM_LOCALIZATIONS		\N	4.20.0	\N	\N	0728694538
default-roles	keycloak	META-INF/jpa-changelog-13.0.0.xml	2023-11-23 08:38:15.816573	89	EXECUTED	8:72d03345fda8e2f17093d08801947773	addColumn tableName=REALM; customChange		\N	4.20.0	\N	\N	0728694538
default-roles-cleanup	keycloak	META-INF/jpa-changelog-13.0.0.xml	2023-11-23 08:38:15.821352	90	EXECUTED	8:61c9233951bd96ffecd9ba75f7d978a4	dropTable tableName=REALM_DEFAULT_ROLES; dropTable tableName=CLIENT_DEFAULT_ROLES		\N	4.20.0	\N	\N	0728694538
13.0.0-KEYCLOAK-16844	keycloak	META-INF/jpa-changelog-13.0.0.xml	2023-11-23 08:38:15.82523	91	EXECUTED	8:ea82e6ad945cec250af6372767b25525	createIndex indexName=IDX_OFFLINE_USS_PRELOAD, tableName=OFFLINE_USER_SESSION		\N	4.20.0	\N	\N	0728694538
map-remove-ri-13.0.0	keycloak	META-INF/jpa-changelog-13.0.0.xml	2023-11-23 08:38:15.82988	92	EXECUTED	8:d3f4a33f41d960ddacd7e2ef30d126b3	dropForeignKeyConstraint baseTableName=DEFAULT_CLIENT_SCOPE, constraintName=FK_R_DEF_CLI_SCOPE_SCOPE; dropForeignKeyConstraint baseTableName=CLIENT_SCOPE_CLIENT, constraintName=FK_C_CLI_SCOPE_SCOPE; dropForeignKeyConstraint baseTableName=CLIENT_SC...		\N	4.20.0	\N	\N	0728694538
13.0.0-KEYCLOAK-17992-drop-constraints	keycloak	META-INF/jpa-changelog-13.0.0.xml	2023-11-23 08:38:15.831062	93	MARK_RAN	8:1284a27fbd049d65831cb6fc07c8a783	dropPrimaryKey constraintName=C_CLI_SCOPE_BIND, tableName=CLIENT_SCOPE_CLIENT; dropIndex indexName=IDX_CLSCOPE_CL, tableName=CLIENT_SCOPE_CLIENT; dropIndex indexName=IDX_CL_CLSCOPE, tableName=CLIENT_SCOPE_CLIENT		\N	4.20.0	\N	\N	0728694538
13.0.0-increase-column-size-federated	keycloak	META-INF/jpa-changelog-13.0.0.xml	2023-11-23 08:38:15.838062	94	EXECUTED	8:9d11b619db2ae27c25853b8a37cd0dea	modifyDataType columnName=CLIENT_ID, tableName=CLIENT_SCOPE_CLIENT; modifyDataType columnName=SCOPE_ID, tableName=CLIENT_SCOPE_CLIENT		\N	4.20.0	\N	\N	0728694538
13.0.0-KEYCLOAK-17992-recreate-constraints	keycloak	META-INF/jpa-changelog-13.0.0.xml	2023-11-23 08:38:15.839239	95	MARK_RAN	8:3002bb3997451bb9e8bac5c5cd8d6327	addNotNullConstraint columnName=CLIENT_ID, tableName=CLIENT_SCOPE_CLIENT; addNotNullConstraint columnName=SCOPE_ID, tableName=CLIENT_SCOPE_CLIENT; addPrimaryKey constraintName=C_CLI_SCOPE_BIND, tableName=CLIENT_SCOPE_CLIENT; createIndex indexName=...		\N	4.20.0	\N	\N	0728694538
json-string-accomodation-fixed	keycloak	META-INF/jpa-changelog-13.0.0.xml	2023-11-23 08:38:15.843154	96	EXECUTED	8:dfbee0d6237a23ef4ccbb7a4e063c163	addColumn tableName=REALM_ATTRIBUTE; update tableName=REALM_ATTRIBUTE; dropColumn columnName=VALUE, tableName=REALM_ATTRIBUTE; renameColumn newColumnName=VALUE, oldColumnName=VALUE_NEW, tableName=REALM_ATTRIBUTE		\N	4.20.0	\N	\N	0728694538
14.0.0-KEYCLOAK-11019	keycloak	META-INF/jpa-changelog-14.0.0.xml	2023-11-23 08:38:15.849188	97	EXECUTED	8:75f3e372df18d38c62734eebb986b960	createIndex indexName=IDX_OFFLINE_CSS_PRELOAD, tableName=OFFLINE_CLIENT_SESSION; createIndex indexName=IDX_OFFLINE_USS_BY_USER, tableName=OFFLINE_USER_SESSION; createIndex indexName=IDX_OFFLINE_USS_BY_USERSESS, tableName=OFFLINE_USER_SESSION		\N	4.20.0	\N	\N	0728694538
14.0.0-KEYCLOAK-18286	keycloak	META-INF/jpa-changelog-14.0.0.xml	2023-11-23 08:38:15.850557	98	MARK_RAN	8:7fee73eddf84a6035691512c85637eef	createIndex indexName=IDX_CLIENT_ATT_BY_NAME_VALUE, tableName=CLIENT_ATTRIBUTES		\N	4.20.0	\N	\N	0728694538
14.0.0-KEYCLOAK-18286-revert	keycloak	META-INF/jpa-changelog-14.0.0.xml	2023-11-23 08:38:15.859255	99	MARK_RAN	8:7a11134ab12820f999fbf3bb13c3adc8	dropIndex indexName=IDX_CLIENT_ATT_BY_NAME_VALUE, tableName=CLIENT_ATTRIBUTES		\N	4.20.0	\N	\N	0728694538
14.0.0-KEYCLOAK-18286-supported-dbs	keycloak	META-INF/jpa-changelog-14.0.0.xml	2023-11-23 08:38:15.863466	100	EXECUTED	8:c0f6eaac1f3be773ffe54cb5b8482b70	createIndex indexName=IDX_CLIENT_ATT_BY_NAME_VALUE, tableName=CLIENT_ATTRIBUTES		\N	4.20.0	\N	\N	0728694538
14.0.0-KEYCLOAK-18286-unsupported-dbs	keycloak	META-INF/jpa-changelog-14.0.0.xml	2023-11-23 08:38:15.86451	101	MARK_RAN	8:18186f0008b86e0f0f49b0c4d0e842ac	createIndex indexName=IDX_CLIENT_ATT_BY_NAME_VALUE, tableName=CLIENT_ATTRIBUTES		\N	4.20.0	\N	\N	0728694538
KEYCLOAK-17267-add-index-to-user-attributes	keycloak	META-INF/jpa-changelog-14.0.0.xml	2023-11-23 08:38:15.86772	102	EXECUTED	8:09c2780bcb23b310a7019d217dc7b433	createIndex indexName=IDX_USER_ATTRIBUTE_NAME, tableName=USER_ATTRIBUTE		\N	4.20.0	\N	\N	0728694538
KEYCLOAK-18146-add-saml-art-binding-identifier	keycloak	META-INF/jpa-changelog-14.0.0.xml	2023-11-23 08:38:15.871657	103	EXECUTED	8:276a44955eab693c970a42880197fff2	customChange		\N	4.20.0	\N	\N	0728694538
15.0.0-KEYCLOAK-18467	keycloak	META-INF/jpa-changelog-15.0.0.xml	2023-11-23 08:38:15.874843	104	EXECUTED	8:ba8ee3b694d043f2bfc1a1079d0760d7	addColumn tableName=REALM_LOCALIZATIONS; update tableName=REALM_LOCALIZATIONS; dropColumn columnName=TEXTS, tableName=REALM_LOCALIZATIONS; renameColumn newColumnName=TEXTS, oldColumnName=TEXTS_NEW, tableName=REALM_LOCALIZATIONS; addNotNullConstrai...		\N	4.20.0	\N	\N	0728694538
17.0.0-9562	keycloak	META-INF/jpa-changelog-17.0.0.xml	2023-11-23 08:38:15.87824	105	EXECUTED	8:5e06b1d75f5d17685485e610c2851b17	createIndex indexName=IDX_USER_SERVICE_ACCOUNT, tableName=USER_ENTITY		\N	4.20.0	\N	\N	0728694538
18.0.0-10625-IDX_ADMIN_EVENT_TIME	keycloak	META-INF/jpa-changelog-18.0.0.xml	2023-11-23 08:38:15.88121	106	EXECUTED	8:4b80546c1dc550ac552ee7b24a4ab7c0	createIndex indexName=IDX_ADMIN_EVENT_TIME, tableName=ADMIN_EVENT_ENTITY		\N	4.20.0	\N	\N	0728694538
19.0.0-10135	keycloak	META-INF/jpa-changelog-19.0.0.xml	2023-11-23 08:38:15.884515	107	EXECUTED	8:af510cd1bb2ab6339c45372f3e491696	customChange		\N	4.20.0	\N	\N	0728694538
20.0.0-12964-supported-dbs	keycloak	META-INF/jpa-changelog-20.0.0.xml	2023-11-23 08:38:15.887748	108	EXECUTED	8:05c99fc610845ef66ee812b7921af0ef	createIndex indexName=IDX_GROUP_ATT_BY_NAME_VALUE, tableName=GROUP_ATTRIBUTE		\N	4.20.0	\N	\N	0728694538
20.0.0-12964-unsupported-dbs	keycloak	META-INF/jpa-changelog-20.0.0.xml	2023-11-23 08:38:15.888825	109	MARK_RAN	8:314e803baf2f1ec315b3464e398b8247	createIndex indexName=IDX_GROUP_ATT_BY_NAME_VALUE, tableName=GROUP_ATTRIBUTE		\N	4.20.0	\N	\N	0728694538
client-attributes-string-accomodation-fixed	keycloak	META-INF/jpa-changelog-20.0.0.xml	2023-11-23 08:38:15.89249	110	EXECUTED	8:56e4677e7e12556f70b604c573840100	addColumn tableName=CLIENT_ATTRIBUTES; update tableName=CLIENT_ATTRIBUTES; dropColumn columnName=VALUE, tableName=CLIENT_ATTRIBUTES; renameColumn newColumnName=VALUE, oldColumnName=VALUE_NEW, tableName=CLIENT_ATTRIBUTES		\N	4.20.0	\N	\N	0728694538
21.0.2-17277	keycloak	META-INF/jpa-changelog-21.0.2.xml	2023-11-23 08:38:15.895786	111	EXECUTED	8:8806cb33d2a546ce770384bf98cf6eac	customChange		\N	4.20.0	\N	\N	0728694538
21.1.0-19404	keycloak	META-INF/jpa-changelog-21.1.0.xml	2023-11-23 08:38:15.910661	112	EXECUTED	8:fdb2924649d30555ab3a1744faba4928	modifyDataType columnName=DECISION_STRATEGY, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=LOGIC, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=POLICY_ENFORCE_MODE, tableName=RESOURCE_SERVER		\N	4.20.0	\N	\N	0728694538
21.1.0-19404-2	keycloak	META-INF/jpa-changelog-21.1.0.xml	2023-11-23 08:38:15.911828	113	MARK_RAN	8:1c96cc2b10903bd07a03670098d67fd6	addColumn tableName=RESOURCE_SERVER_POLICY; update tableName=RESOURCE_SERVER_POLICY; dropColumn columnName=DECISION_STRATEGY, tableName=RESOURCE_SERVER_POLICY; renameColumn newColumnName=DECISION_STRATEGY, oldColumnName=DECISION_STRATEGY_NEW, tabl...		\N	4.20.0	\N	\N	0728694538
22.0.0-17484	keycloak	META-INF/jpa-changelog-22.0.0.xml	2023-11-23 08:38:15.915672	114	EXECUTED	8:4c3d4e8b142a66fcdf21b89a4dd33301	customChange		\N	4.20.0	\N	\N	0728694538
\.


--
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
1	f	\N	\N
1000	f	\N	\N
1001	f	\N	\N
\.


--
-- Data for Name: default_client_scope; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.default_client_scope (realm_id, scope_id, default_scope) FROM stdin;
b938aebe-888e-45fc-beec-404ca79e1824	704d63d6-4d37-4dfb-a450-aebaf8eee9ce	f
b938aebe-888e-45fc-beec-404ca79e1824	0d38c2d9-1d4f-4457-afaf-8cb043566ac8	t
b938aebe-888e-45fc-beec-404ca79e1824	7cf26061-6cc3-4b06-882b-61162ac1fc78	t
b938aebe-888e-45fc-beec-404ca79e1824	1383c0b9-83e2-47bf-945f-1098401335be	t
b938aebe-888e-45fc-beec-404ca79e1824	e6bf5ba8-3429-4c01-90d8-172eca1b74f9	f
b938aebe-888e-45fc-beec-404ca79e1824	4d97c3c5-9466-48c4-9a67-bc792e6642cd	f
b938aebe-888e-45fc-beec-404ca79e1824	0e4dc652-e563-41a9-8040-ff79aaa4ccc5	t
b938aebe-888e-45fc-beec-404ca79e1824	2925672b-bc38-4bb2-ae97-872b361cd6e6	t
b938aebe-888e-45fc-beec-404ca79e1824	767507df-0f87-4861-a374-3d1d11d9a1a6	f
b938aebe-888e-45fc-beec-404ca79e1824	42c14764-a445-4851-bdf9-f613cd73bdab	t
\.


--
-- Data for Name: event_entity; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.event_entity (id, client_id, details_json, error, ip_address, realm_id, session_id, event_time, type, user_id) FROM stdin;
3c2f84bf-0af5-4cc0-8a02-1fc53ddfc6c0	refae	{"auth_method":"openid-connect","auth_type":"code","redirect_uri":"https://refae/tenant1/openid_redirect/018bf1f7-f0bc-71d8-9b26-f8dc14116ddb","code_id":"0b9d0fea-4d68-4cf0-8bdc-92d457472c2a","username":"openid"}	user_not_found	172.18.0.1	b938aebe-888e-45fc-beec-404ca79e1824	\N	1700728999415	LOGIN_ERROR	\N
4dff49fb-ea2d-4df1-91ed-e2f7d12e64f7	\N	{"code_id":"865a5e6a-81e2-4908-8965-cc9e5183ec3d"}	expired_code	172.18.0.1	b938aebe-888e-45fc-beec-404ca79e1824	\N	1700748526217	LOGOUT_ERROR	\N
3358e5b9-c9fa-4ae1-adc9-a5b1b90c5e82	\N	{"code_id":"865a5e6a-81e2-4908-8965-cc9e5183ec3d"}	session_expired	172.18.0.1	b938aebe-888e-45fc-beec-404ca79e1824	\N	1700748526244	LOGOUT_ERROR	\N
aab31dea-45e8-4591-9c5d-48b9c52c9c5b	refae	{"grant_type":"authorization_code","code_id":"1e2c4ddd-97ec-4cca-a9f6-a2c808f8ea1b","client_auth_method":"client-secret"}	invalid_code	172.18.0.4	b938aebe-888e-45fc-beec-404ca79e1824	1e2c4ddd-97ec-4cca-a9f6-a2c808f8ea1b	1700816010383	CODE_TO_TOKEN_ERROR	\N
\.


--
-- Data for Name: fed_user_attribute; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fed_user_attribute (id, name, user_id, realm_id, storage_provider_id, value) FROM stdin;
\.


--
-- Data for Name: fed_user_consent; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fed_user_consent (id, client_id, user_id, realm_id, storage_provider_id, created_date, last_updated_date, client_storage_provider, external_client_id) FROM stdin;
\.


--
-- Data for Name: fed_user_consent_cl_scope; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fed_user_consent_cl_scope (user_consent_id, scope_id) FROM stdin;
\.


--
-- Data for Name: fed_user_credential; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fed_user_credential (id, salt, type, created_date, user_id, realm_id, storage_provider_id, user_label, secret_data, credential_data, priority) FROM stdin;
\.


--
-- Data for Name: fed_user_group_membership; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fed_user_group_membership (group_id, user_id, realm_id, storage_provider_id) FROM stdin;
\.


--
-- Data for Name: fed_user_required_action; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fed_user_required_action (required_action, user_id, realm_id, storage_provider_id) FROM stdin;
\.


--
-- Data for Name: fed_user_role_mapping; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fed_user_role_mapping (role_id, user_id, realm_id, storage_provider_id) FROM stdin;
\.


--
-- Data for Name: federated_identity; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.federated_identity (identity_provider, realm_id, federated_user_id, federated_username, token, user_id) FROM stdin;
\.


--
-- Data for Name: federated_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.federated_user (id, storage_provider_id, realm_id) FROM stdin;
\.


--
-- Data for Name: group_attribute; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.group_attribute (id, name, value, group_id) FROM stdin;
\.


--
-- Data for Name: group_role_mapping; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.group_role_mapping (role_id, group_id) FROM stdin;
\.


--
-- Data for Name: identity_provider; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.identity_provider (internal_id, enabled, provider_alias, provider_id, store_token, authenticate_by_default, realm_id, add_token_role, trust_email, first_broker_login_flow_id, post_broker_login_flow_id, provider_display_name, link_only) FROM stdin;
\.


--
-- Data for Name: identity_provider_config; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.identity_provider_config (identity_provider_id, value, name) FROM stdin;
\.


--
-- Data for Name: identity_provider_mapper; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.identity_provider_mapper (id, name, idp_alias, idp_mapper_name, realm_id) FROM stdin;
\.


--
-- Data for Name: idp_mapper_config; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.idp_mapper_config (idp_mapper_id, value, name) FROM stdin;
\.


--
-- Data for Name: keycloak_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.keycloak_group (id, name, parent_group, realm_id) FROM stdin;
\.


--
-- Data for Name: keycloak_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.keycloak_role (id, client_realm_constraint, client_role, description, name, realm_id, client, realm) FROM stdin;
52e01ede-b6ad-4985-ab21-0eee35fee7b6	b938aebe-888e-45fc-beec-404ca79e1824	f	${role_default-roles}	default-roles-master	b938aebe-888e-45fc-beec-404ca79e1824	\N	\N
33ace452-b68c-4888-beae-20ddf10736ed	b938aebe-888e-45fc-beec-404ca79e1824	f	${role_admin}	admin	b938aebe-888e-45fc-beec-404ca79e1824	\N	\N
6f196d67-62de-4587-b241-3331eb8abfc8	b938aebe-888e-45fc-beec-404ca79e1824	f	${role_create-realm}	create-realm	b938aebe-888e-45fc-beec-404ca79e1824	\N	\N
4a6b248d-8a5a-48f8-b742-e55d1b431072	3fc4f049-48fc-43cd-b350-7ec09377778d	t	${role_create-client}	create-client	b938aebe-888e-45fc-beec-404ca79e1824	3fc4f049-48fc-43cd-b350-7ec09377778d	\N
21ad198e-bc21-47cf-a5cf-7dcd3c1efbe7	3fc4f049-48fc-43cd-b350-7ec09377778d	t	${role_view-realm}	view-realm	b938aebe-888e-45fc-beec-404ca79e1824	3fc4f049-48fc-43cd-b350-7ec09377778d	\N
36b9b617-ce5c-404d-b77e-fd4ded3c9394	3fc4f049-48fc-43cd-b350-7ec09377778d	t	${role_view-users}	view-users	b938aebe-888e-45fc-beec-404ca79e1824	3fc4f049-48fc-43cd-b350-7ec09377778d	\N
b0d1e610-bda3-4afb-aa64-99f10401cc15	3fc4f049-48fc-43cd-b350-7ec09377778d	t	${role_view-clients}	view-clients	b938aebe-888e-45fc-beec-404ca79e1824	3fc4f049-48fc-43cd-b350-7ec09377778d	\N
63ccc837-810f-4755-a190-88aed15d84c2	3fc4f049-48fc-43cd-b350-7ec09377778d	t	${role_view-events}	view-events	b938aebe-888e-45fc-beec-404ca79e1824	3fc4f049-48fc-43cd-b350-7ec09377778d	\N
5cd999c3-9967-47c9-85e6-8a144184d386	3fc4f049-48fc-43cd-b350-7ec09377778d	t	${role_view-identity-providers}	view-identity-providers	b938aebe-888e-45fc-beec-404ca79e1824	3fc4f049-48fc-43cd-b350-7ec09377778d	\N
c072b6fe-ccd9-4d33-b14c-363936acb52c	3fc4f049-48fc-43cd-b350-7ec09377778d	t	${role_view-authorization}	view-authorization	b938aebe-888e-45fc-beec-404ca79e1824	3fc4f049-48fc-43cd-b350-7ec09377778d	\N
7c737e6b-6d83-4be5-90c0-35a5b1fd0d87	3fc4f049-48fc-43cd-b350-7ec09377778d	t	${role_manage-realm}	manage-realm	b938aebe-888e-45fc-beec-404ca79e1824	3fc4f049-48fc-43cd-b350-7ec09377778d	\N
8fbba102-b122-4ea4-9443-2b46b0a6ed7b	3fc4f049-48fc-43cd-b350-7ec09377778d	t	${role_manage-users}	manage-users	b938aebe-888e-45fc-beec-404ca79e1824	3fc4f049-48fc-43cd-b350-7ec09377778d	\N
4f0d577a-c474-4cb9-b968-99fb95644509	3fc4f049-48fc-43cd-b350-7ec09377778d	t	${role_manage-clients}	manage-clients	b938aebe-888e-45fc-beec-404ca79e1824	3fc4f049-48fc-43cd-b350-7ec09377778d	\N
9b9310cd-def0-4293-aaa3-4f082035e67b	3fc4f049-48fc-43cd-b350-7ec09377778d	t	${role_manage-events}	manage-events	b938aebe-888e-45fc-beec-404ca79e1824	3fc4f049-48fc-43cd-b350-7ec09377778d	\N
57f3c009-3760-4e4d-bf45-c118eae98195	3fc4f049-48fc-43cd-b350-7ec09377778d	t	${role_manage-identity-providers}	manage-identity-providers	b938aebe-888e-45fc-beec-404ca79e1824	3fc4f049-48fc-43cd-b350-7ec09377778d	\N
eee67b59-b468-47cb-b55c-9337077ea17e	3fc4f049-48fc-43cd-b350-7ec09377778d	t	${role_manage-authorization}	manage-authorization	b938aebe-888e-45fc-beec-404ca79e1824	3fc4f049-48fc-43cd-b350-7ec09377778d	\N
233b0ff1-abe9-43ee-9ac0-3edeab6de994	3fc4f049-48fc-43cd-b350-7ec09377778d	t	${role_query-users}	query-users	b938aebe-888e-45fc-beec-404ca79e1824	3fc4f049-48fc-43cd-b350-7ec09377778d	\N
4df16dd6-8ff1-4143-a347-f246958db6c1	3fc4f049-48fc-43cd-b350-7ec09377778d	t	${role_query-clients}	query-clients	b938aebe-888e-45fc-beec-404ca79e1824	3fc4f049-48fc-43cd-b350-7ec09377778d	\N
c2242c67-339c-43d0-95fd-10017ae2e9bb	3fc4f049-48fc-43cd-b350-7ec09377778d	t	${role_query-realms}	query-realms	b938aebe-888e-45fc-beec-404ca79e1824	3fc4f049-48fc-43cd-b350-7ec09377778d	\N
4f5457dd-8b47-446f-bad1-8b78ca1af439	3fc4f049-48fc-43cd-b350-7ec09377778d	t	${role_query-groups}	query-groups	b938aebe-888e-45fc-beec-404ca79e1824	3fc4f049-48fc-43cd-b350-7ec09377778d	\N
4de23a1e-a374-4f67-8261-4e078bdb0e38	086168c8-0c23-4897-b277-9ab41cd09256	t	${role_view-profile}	view-profile	b938aebe-888e-45fc-beec-404ca79e1824	086168c8-0c23-4897-b277-9ab41cd09256	\N
bf294699-cdf9-4402-8510-c56d9af46f6d	086168c8-0c23-4897-b277-9ab41cd09256	t	${role_manage-account}	manage-account	b938aebe-888e-45fc-beec-404ca79e1824	086168c8-0c23-4897-b277-9ab41cd09256	\N
f1dd8a34-eefc-477a-9221-7a181841ab90	086168c8-0c23-4897-b277-9ab41cd09256	t	${role_manage-account-links}	manage-account-links	b938aebe-888e-45fc-beec-404ca79e1824	086168c8-0c23-4897-b277-9ab41cd09256	\N
b7ef18be-d069-4f16-967c-520123fef72a	086168c8-0c23-4897-b277-9ab41cd09256	t	${role_view-applications}	view-applications	b938aebe-888e-45fc-beec-404ca79e1824	086168c8-0c23-4897-b277-9ab41cd09256	\N
63f1f4c9-85aa-423a-ae26-cdca65096e7b	086168c8-0c23-4897-b277-9ab41cd09256	t	${role_view-consent}	view-consent	b938aebe-888e-45fc-beec-404ca79e1824	086168c8-0c23-4897-b277-9ab41cd09256	\N
4e0f73a8-ddf0-45b2-9197-9f50092ac04e	086168c8-0c23-4897-b277-9ab41cd09256	t	${role_manage-consent}	manage-consent	b938aebe-888e-45fc-beec-404ca79e1824	086168c8-0c23-4897-b277-9ab41cd09256	\N
6716b6b3-d7a9-49b2-bde7-37a00a7b5d1e	086168c8-0c23-4897-b277-9ab41cd09256	t	${role_view-groups}	view-groups	b938aebe-888e-45fc-beec-404ca79e1824	086168c8-0c23-4897-b277-9ab41cd09256	\N
6a84daa9-aa9e-4511-8bff-684400a97102	086168c8-0c23-4897-b277-9ab41cd09256	t	${role_delete-account}	delete-account	b938aebe-888e-45fc-beec-404ca79e1824	086168c8-0c23-4897-b277-9ab41cd09256	\N
216d8ae9-4f96-4f3d-8201-78c88d5aa917	f906b10c-d01d-4616-b7e0-f37c72fc5145	t	${role_read-token}	read-token	b938aebe-888e-45fc-beec-404ca79e1824	f906b10c-d01d-4616-b7e0-f37c72fc5145	\N
3c575119-b8f9-49d1-8c5b-1bf881e73463	3fc4f049-48fc-43cd-b350-7ec09377778d	t	${role_impersonation}	impersonation	b938aebe-888e-45fc-beec-404ca79e1824	3fc4f049-48fc-43cd-b350-7ec09377778d	\N
c3cac550-d778-4944-9d50-9eacc13819d2	b938aebe-888e-45fc-beec-404ca79e1824	f	${role_offline-access}	offline_access	b938aebe-888e-45fc-beec-404ca79e1824	\N	\N
b69d7097-a6eb-4228-903b-7578b110d8a1	b938aebe-888e-45fc-beec-404ca79e1824	f	${role_uma_authorization}	uma_authorization	b938aebe-888e-45fc-beec-404ca79e1824	\N	\N
\.


--
-- Data for Name: migration_model; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migration_model (id, version, update_time) FROM stdin;
yi55b	22.0.0	1700728696
\.


--
-- Data for Name: offline_client_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.offline_client_session (user_session_id, client_id, offline_flag, "timestamp", data, client_storage_provider, external_client_id) FROM stdin;
\.


--
-- Data for Name: offline_user_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.offline_user_session (user_session_id, user_id, realm_id, created_on, offline_flag, data, last_session_refresh) FROM stdin;
\.


--
-- Data for Name: policy_config; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.policy_config (policy_id, name, value) FROM stdin;
\.


--
-- Data for Name: protocol_mapper; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.protocol_mapper (id, name, protocol, protocol_mapper_name, client_id, client_scope_id) FROM stdin;
bf212ae8-f821-439c-bd9b-b974580e3c01	audience resolve	openid-connect	oidc-audience-resolve-mapper	95a16281-c129-4cc2-8f85-d1e225808260	\N
9119ca0c-f4ab-44a8-b431-c69635c9db18	locale	openid-connect	oidc-usermodel-attribute-mapper	cc4cf8f3-57b9-47c6-a489-0cd5e817d1fd	\N
d4c0b2bb-6734-4d1c-90e2-3bfd8cee9935	role list	saml	saml-role-list-mapper	\N	0d38c2d9-1d4f-4457-afaf-8cb043566ac8
03f12647-4784-4ec0-afe5-2415c3a074a6	full name	openid-connect	oidc-full-name-mapper	\N	7cf26061-6cc3-4b06-882b-61162ac1fc78
9e150ef5-3edd-44db-ae80-8ac08cbf99db	family name	openid-connect	oidc-usermodel-attribute-mapper	\N	7cf26061-6cc3-4b06-882b-61162ac1fc78
41b1e20e-180f-4f58-903c-07ccbde38205	given name	openid-connect	oidc-usermodel-attribute-mapper	\N	7cf26061-6cc3-4b06-882b-61162ac1fc78
010af3bd-59d1-4810-b387-fa620a84fa7a	middle name	openid-connect	oidc-usermodel-attribute-mapper	\N	7cf26061-6cc3-4b06-882b-61162ac1fc78
fe1f2387-cc55-474a-8129-ab1c099adb51	nickname	openid-connect	oidc-usermodel-attribute-mapper	\N	7cf26061-6cc3-4b06-882b-61162ac1fc78
ec845c98-0378-4f49-b7f3-0fca3ee36fce	username	openid-connect	oidc-usermodel-attribute-mapper	\N	7cf26061-6cc3-4b06-882b-61162ac1fc78
cf025e88-c155-42ce-bac5-500796017a8c	profile	openid-connect	oidc-usermodel-attribute-mapper	\N	7cf26061-6cc3-4b06-882b-61162ac1fc78
9d3ce2bb-57a9-4101-adf6-70fc3d44a4d7	picture	openid-connect	oidc-usermodel-attribute-mapper	\N	7cf26061-6cc3-4b06-882b-61162ac1fc78
5b94a6ee-b6a8-419b-9e8c-b2241efb1e01	website	openid-connect	oidc-usermodel-attribute-mapper	\N	7cf26061-6cc3-4b06-882b-61162ac1fc78
a00b4b4a-883f-4113-acab-5fae27d89e2e	gender	openid-connect	oidc-usermodel-attribute-mapper	\N	7cf26061-6cc3-4b06-882b-61162ac1fc78
2fba8aef-7b18-4b32-af6a-fe552a94f8bb	birthdate	openid-connect	oidc-usermodel-attribute-mapper	\N	7cf26061-6cc3-4b06-882b-61162ac1fc78
e793335f-8f05-4420-bc8d-e0311429d0e8	zoneinfo	openid-connect	oidc-usermodel-attribute-mapper	\N	7cf26061-6cc3-4b06-882b-61162ac1fc78
773c58c4-1fe2-4d45-a22d-98261947305d	locale	openid-connect	oidc-usermodel-attribute-mapper	\N	7cf26061-6cc3-4b06-882b-61162ac1fc78
bce7be70-01b0-40c6-addd-d842f54898af	updated at	openid-connect	oidc-usermodel-attribute-mapper	\N	7cf26061-6cc3-4b06-882b-61162ac1fc78
1766941c-c4cd-4be2-a739-cefac3545d72	email	openid-connect	oidc-usermodel-attribute-mapper	\N	1383c0b9-83e2-47bf-945f-1098401335be
0df2ba75-b30e-4e0b-a199-19b54af22793	email verified	openid-connect	oidc-usermodel-property-mapper	\N	1383c0b9-83e2-47bf-945f-1098401335be
98857a32-6221-4637-a37d-906d58168dd9	address	openid-connect	oidc-address-mapper	\N	e6bf5ba8-3429-4c01-90d8-172eca1b74f9
15317e5d-09ed-477a-a39a-7eaf85b6bc54	phone number	openid-connect	oidc-usermodel-attribute-mapper	\N	4d97c3c5-9466-48c4-9a67-bc792e6642cd
7355d1e6-1d7e-42c9-a7fe-2c33736a9cf8	phone number verified	openid-connect	oidc-usermodel-attribute-mapper	\N	4d97c3c5-9466-48c4-9a67-bc792e6642cd
35946389-41ae-4bcf-bd6b-78b2a575d0ce	realm roles	openid-connect	oidc-usermodel-realm-role-mapper	\N	0e4dc652-e563-41a9-8040-ff79aaa4ccc5
ede96522-37b7-4499-b820-034a86ab27e9	client roles	openid-connect	oidc-usermodel-client-role-mapper	\N	0e4dc652-e563-41a9-8040-ff79aaa4ccc5
f1924b68-0792-4ea4-8b16-d24ed3b7056e	audience resolve	openid-connect	oidc-audience-resolve-mapper	\N	0e4dc652-e563-41a9-8040-ff79aaa4ccc5
95e3bbd7-c9c9-4aa9-9223-ceae40597e2d	allowed web origins	openid-connect	oidc-allowed-origins-mapper	\N	2925672b-bc38-4bb2-ae97-872b361cd6e6
3630f51d-1761-4a85-b7e5-f22acf99d550	upn	openid-connect	oidc-usermodel-attribute-mapper	\N	767507df-0f87-4861-a374-3d1d11d9a1a6
d7621645-75d5-4eaa-b7f9-e8e5fa5d5ae6	groups	openid-connect	oidc-usermodel-realm-role-mapper	\N	767507df-0f87-4861-a374-3d1d11d9a1a6
3cff6120-cff4-42c8-b91a-80da208705cd	acr loa level	openid-connect	oidc-acr-mapper	\N	42c14764-a445-4851-bdf9-f613cd73bdab
\.


--
-- Data for Name: protocol_mapper_config; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.protocol_mapper_config (protocol_mapper_id, value, name) FROM stdin;
9119ca0c-f4ab-44a8-b431-c69635c9db18	true	userinfo.token.claim
9119ca0c-f4ab-44a8-b431-c69635c9db18	locale	user.attribute
9119ca0c-f4ab-44a8-b431-c69635c9db18	true	id.token.claim
9119ca0c-f4ab-44a8-b431-c69635c9db18	true	access.token.claim
9119ca0c-f4ab-44a8-b431-c69635c9db18	locale	claim.name
9119ca0c-f4ab-44a8-b431-c69635c9db18	String	jsonType.label
d4c0b2bb-6734-4d1c-90e2-3bfd8cee9935	false	single
d4c0b2bb-6734-4d1c-90e2-3bfd8cee9935	Basic	attribute.nameformat
d4c0b2bb-6734-4d1c-90e2-3bfd8cee9935	Role	attribute.name
010af3bd-59d1-4810-b387-fa620a84fa7a	true	userinfo.token.claim
010af3bd-59d1-4810-b387-fa620a84fa7a	middleName	user.attribute
010af3bd-59d1-4810-b387-fa620a84fa7a	true	id.token.claim
010af3bd-59d1-4810-b387-fa620a84fa7a	true	access.token.claim
010af3bd-59d1-4810-b387-fa620a84fa7a	middle_name	claim.name
010af3bd-59d1-4810-b387-fa620a84fa7a	String	jsonType.label
03f12647-4784-4ec0-afe5-2415c3a074a6	true	userinfo.token.claim
03f12647-4784-4ec0-afe5-2415c3a074a6	true	id.token.claim
03f12647-4784-4ec0-afe5-2415c3a074a6	true	access.token.claim
2fba8aef-7b18-4b32-af6a-fe552a94f8bb	true	userinfo.token.claim
2fba8aef-7b18-4b32-af6a-fe552a94f8bb	birthdate	user.attribute
2fba8aef-7b18-4b32-af6a-fe552a94f8bb	true	id.token.claim
2fba8aef-7b18-4b32-af6a-fe552a94f8bb	true	access.token.claim
2fba8aef-7b18-4b32-af6a-fe552a94f8bb	birthdate	claim.name
2fba8aef-7b18-4b32-af6a-fe552a94f8bb	String	jsonType.label
41b1e20e-180f-4f58-903c-07ccbde38205	true	userinfo.token.claim
41b1e20e-180f-4f58-903c-07ccbde38205	firstName	user.attribute
41b1e20e-180f-4f58-903c-07ccbde38205	true	id.token.claim
41b1e20e-180f-4f58-903c-07ccbde38205	true	access.token.claim
41b1e20e-180f-4f58-903c-07ccbde38205	given_name	claim.name
41b1e20e-180f-4f58-903c-07ccbde38205	String	jsonType.label
5b94a6ee-b6a8-419b-9e8c-b2241efb1e01	true	userinfo.token.claim
5b94a6ee-b6a8-419b-9e8c-b2241efb1e01	website	user.attribute
5b94a6ee-b6a8-419b-9e8c-b2241efb1e01	true	id.token.claim
5b94a6ee-b6a8-419b-9e8c-b2241efb1e01	true	access.token.claim
5b94a6ee-b6a8-419b-9e8c-b2241efb1e01	website	claim.name
5b94a6ee-b6a8-419b-9e8c-b2241efb1e01	String	jsonType.label
773c58c4-1fe2-4d45-a22d-98261947305d	true	userinfo.token.claim
773c58c4-1fe2-4d45-a22d-98261947305d	locale	user.attribute
773c58c4-1fe2-4d45-a22d-98261947305d	true	id.token.claim
773c58c4-1fe2-4d45-a22d-98261947305d	true	access.token.claim
773c58c4-1fe2-4d45-a22d-98261947305d	locale	claim.name
773c58c4-1fe2-4d45-a22d-98261947305d	String	jsonType.label
9d3ce2bb-57a9-4101-adf6-70fc3d44a4d7	true	userinfo.token.claim
9d3ce2bb-57a9-4101-adf6-70fc3d44a4d7	picture	user.attribute
9d3ce2bb-57a9-4101-adf6-70fc3d44a4d7	true	id.token.claim
9d3ce2bb-57a9-4101-adf6-70fc3d44a4d7	true	access.token.claim
9d3ce2bb-57a9-4101-adf6-70fc3d44a4d7	picture	claim.name
9d3ce2bb-57a9-4101-adf6-70fc3d44a4d7	String	jsonType.label
9e150ef5-3edd-44db-ae80-8ac08cbf99db	true	userinfo.token.claim
9e150ef5-3edd-44db-ae80-8ac08cbf99db	lastName	user.attribute
9e150ef5-3edd-44db-ae80-8ac08cbf99db	true	id.token.claim
9e150ef5-3edd-44db-ae80-8ac08cbf99db	true	access.token.claim
9e150ef5-3edd-44db-ae80-8ac08cbf99db	family_name	claim.name
9e150ef5-3edd-44db-ae80-8ac08cbf99db	String	jsonType.label
a00b4b4a-883f-4113-acab-5fae27d89e2e	true	userinfo.token.claim
a00b4b4a-883f-4113-acab-5fae27d89e2e	gender	user.attribute
a00b4b4a-883f-4113-acab-5fae27d89e2e	true	id.token.claim
a00b4b4a-883f-4113-acab-5fae27d89e2e	true	access.token.claim
a00b4b4a-883f-4113-acab-5fae27d89e2e	gender	claim.name
a00b4b4a-883f-4113-acab-5fae27d89e2e	String	jsonType.label
bce7be70-01b0-40c6-addd-d842f54898af	true	userinfo.token.claim
bce7be70-01b0-40c6-addd-d842f54898af	updatedAt	user.attribute
bce7be70-01b0-40c6-addd-d842f54898af	true	id.token.claim
bce7be70-01b0-40c6-addd-d842f54898af	true	access.token.claim
bce7be70-01b0-40c6-addd-d842f54898af	updated_at	claim.name
bce7be70-01b0-40c6-addd-d842f54898af	long	jsonType.label
cf025e88-c155-42ce-bac5-500796017a8c	true	userinfo.token.claim
cf025e88-c155-42ce-bac5-500796017a8c	profile	user.attribute
cf025e88-c155-42ce-bac5-500796017a8c	true	id.token.claim
cf025e88-c155-42ce-bac5-500796017a8c	true	access.token.claim
cf025e88-c155-42ce-bac5-500796017a8c	profile	claim.name
cf025e88-c155-42ce-bac5-500796017a8c	String	jsonType.label
e793335f-8f05-4420-bc8d-e0311429d0e8	true	userinfo.token.claim
e793335f-8f05-4420-bc8d-e0311429d0e8	zoneinfo	user.attribute
e793335f-8f05-4420-bc8d-e0311429d0e8	true	id.token.claim
e793335f-8f05-4420-bc8d-e0311429d0e8	true	access.token.claim
e793335f-8f05-4420-bc8d-e0311429d0e8	zoneinfo	claim.name
e793335f-8f05-4420-bc8d-e0311429d0e8	String	jsonType.label
ec845c98-0378-4f49-b7f3-0fca3ee36fce	true	userinfo.token.claim
ec845c98-0378-4f49-b7f3-0fca3ee36fce	username	user.attribute
ec845c98-0378-4f49-b7f3-0fca3ee36fce	true	id.token.claim
ec845c98-0378-4f49-b7f3-0fca3ee36fce	true	access.token.claim
ec845c98-0378-4f49-b7f3-0fca3ee36fce	preferred_username	claim.name
ec845c98-0378-4f49-b7f3-0fca3ee36fce	String	jsonType.label
fe1f2387-cc55-474a-8129-ab1c099adb51	true	userinfo.token.claim
fe1f2387-cc55-474a-8129-ab1c099adb51	nickname	user.attribute
fe1f2387-cc55-474a-8129-ab1c099adb51	true	id.token.claim
fe1f2387-cc55-474a-8129-ab1c099adb51	true	access.token.claim
fe1f2387-cc55-474a-8129-ab1c099adb51	nickname	claim.name
fe1f2387-cc55-474a-8129-ab1c099adb51	String	jsonType.label
0df2ba75-b30e-4e0b-a199-19b54af22793	true	userinfo.token.claim
0df2ba75-b30e-4e0b-a199-19b54af22793	emailVerified	user.attribute
0df2ba75-b30e-4e0b-a199-19b54af22793	true	id.token.claim
0df2ba75-b30e-4e0b-a199-19b54af22793	true	access.token.claim
0df2ba75-b30e-4e0b-a199-19b54af22793	email_verified	claim.name
0df2ba75-b30e-4e0b-a199-19b54af22793	boolean	jsonType.label
1766941c-c4cd-4be2-a739-cefac3545d72	true	userinfo.token.claim
1766941c-c4cd-4be2-a739-cefac3545d72	email	user.attribute
1766941c-c4cd-4be2-a739-cefac3545d72	true	id.token.claim
1766941c-c4cd-4be2-a739-cefac3545d72	true	access.token.claim
1766941c-c4cd-4be2-a739-cefac3545d72	email	claim.name
1766941c-c4cd-4be2-a739-cefac3545d72	String	jsonType.label
98857a32-6221-4637-a37d-906d58168dd9	formatted	user.attribute.formatted
98857a32-6221-4637-a37d-906d58168dd9	country	user.attribute.country
98857a32-6221-4637-a37d-906d58168dd9	postal_code	user.attribute.postal_code
98857a32-6221-4637-a37d-906d58168dd9	true	userinfo.token.claim
98857a32-6221-4637-a37d-906d58168dd9	street	user.attribute.street
98857a32-6221-4637-a37d-906d58168dd9	true	id.token.claim
98857a32-6221-4637-a37d-906d58168dd9	region	user.attribute.region
98857a32-6221-4637-a37d-906d58168dd9	true	access.token.claim
98857a32-6221-4637-a37d-906d58168dd9	locality	user.attribute.locality
15317e5d-09ed-477a-a39a-7eaf85b6bc54	true	userinfo.token.claim
15317e5d-09ed-477a-a39a-7eaf85b6bc54	phoneNumber	user.attribute
15317e5d-09ed-477a-a39a-7eaf85b6bc54	true	id.token.claim
15317e5d-09ed-477a-a39a-7eaf85b6bc54	true	access.token.claim
15317e5d-09ed-477a-a39a-7eaf85b6bc54	phone_number	claim.name
15317e5d-09ed-477a-a39a-7eaf85b6bc54	String	jsonType.label
7355d1e6-1d7e-42c9-a7fe-2c33736a9cf8	true	userinfo.token.claim
7355d1e6-1d7e-42c9-a7fe-2c33736a9cf8	phoneNumberVerified	user.attribute
7355d1e6-1d7e-42c9-a7fe-2c33736a9cf8	true	id.token.claim
7355d1e6-1d7e-42c9-a7fe-2c33736a9cf8	true	access.token.claim
7355d1e6-1d7e-42c9-a7fe-2c33736a9cf8	phone_number_verified	claim.name
7355d1e6-1d7e-42c9-a7fe-2c33736a9cf8	boolean	jsonType.label
35946389-41ae-4bcf-bd6b-78b2a575d0ce	true	multivalued
35946389-41ae-4bcf-bd6b-78b2a575d0ce	foo	user.attribute
35946389-41ae-4bcf-bd6b-78b2a575d0ce	true	access.token.claim
35946389-41ae-4bcf-bd6b-78b2a575d0ce	realm_access.roles	claim.name
35946389-41ae-4bcf-bd6b-78b2a575d0ce	String	jsonType.label
ede96522-37b7-4499-b820-034a86ab27e9	true	multivalued
ede96522-37b7-4499-b820-034a86ab27e9	foo	user.attribute
ede96522-37b7-4499-b820-034a86ab27e9	true	access.token.claim
ede96522-37b7-4499-b820-034a86ab27e9	resource_access.${client_id}.roles	claim.name
ede96522-37b7-4499-b820-034a86ab27e9	String	jsonType.label
3630f51d-1761-4a85-b7e5-f22acf99d550	true	userinfo.token.claim
3630f51d-1761-4a85-b7e5-f22acf99d550	username	user.attribute
3630f51d-1761-4a85-b7e5-f22acf99d550	true	id.token.claim
3630f51d-1761-4a85-b7e5-f22acf99d550	true	access.token.claim
3630f51d-1761-4a85-b7e5-f22acf99d550	upn	claim.name
3630f51d-1761-4a85-b7e5-f22acf99d550	String	jsonType.label
d7621645-75d5-4eaa-b7f9-e8e5fa5d5ae6	true	multivalued
d7621645-75d5-4eaa-b7f9-e8e5fa5d5ae6	foo	user.attribute
d7621645-75d5-4eaa-b7f9-e8e5fa5d5ae6	true	id.token.claim
d7621645-75d5-4eaa-b7f9-e8e5fa5d5ae6	true	access.token.claim
d7621645-75d5-4eaa-b7f9-e8e5fa5d5ae6	groups	claim.name
d7621645-75d5-4eaa-b7f9-e8e5fa5d5ae6	String	jsonType.label
3cff6120-cff4-42c8-b91a-80da208705cd	true	id.token.claim
3cff6120-cff4-42c8-b91a-80da208705cd	true	access.token.claim
\.


--
-- Data for Name: realm; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.realm (id, access_code_lifespan, user_action_lifespan, access_token_lifespan, account_theme, admin_theme, email_theme, enabled, events_enabled, events_expiration, login_theme, name, not_before, password_policy, registration_allowed, remember_me, reset_password_allowed, social, ssl_required, sso_idle_timeout, sso_max_lifespan, update_profile_on_soc_login, verify_email, master_admin_client, login_lifespan, internationalization_enabled, default_locale, reg_email_as_username, admin_events_enabled, admin_events_details_enabled, edit_username_allowed, otp_policy_counter, otp_policy_window, otp_policy_period, otp_policy_digits, otp_policy_alg, otp_policy_type, browser_flow, registration_flow, direct_grant_flow, reset_credentials_flow, client_auth_flow, offline_session_idle_timeout, revoke_refresh_token, access_token_life_implicit, login_with_email_allowed, duplicate_emails_allowed, docker_auth_flow, refresh_token_max_reuse, allow_user_managed_access, sso_max_lifespan_remember_me, sso_idle_timeout_remember_me, default_role) FROM stdin;
b938aebe-888e-45fc-beec-404ca79e1824	60	300	60	\N	\N	\N	t	f	0	\N	master	0	\N	f	f	f	f	EXTERNAL	1800	36000	f	f	3fc4f049-48fc-43cd-b350-7ec09377778d	1800	f	\N	f	f	f	f	0	1	30	6	HmacSHA1	totp	964607dd-c0b1-4ecd-b354-f703d5290d81	8ac27ec6-bb8a-4e9f-a3dd-560c1ea6e68f	91edf9d2-6feb-4248-bced-4ed9b81a40d0	06c0a236-4076-4c9c-9185-bb239caaf827	ede23736-e67d-4e88-9dfc-1458be14d32c	2592000	f	900	t	f	b59c7da7-dd4b-4a64-993e-58190f66df9d	0	f	0	0	52e01ede-b6ad-4985-ab21-0eee35fee7b6
\.


--
-- Data for Name: realm_attribute; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.realm_attribute (name, realm_id, value) FROM stdin;
_browser_header.contentSecurityPolicyReportOnly	b938aebe-888e-45fc-beec-404ca79e1824	
_browser_header.xContentTypeOptions	b938aebe-888e-45fc-beec-404ca79e1824	nosniff
_browser_header.referrerPolicy	b938aebe-888e-45fc-beec-404ca79e1824	no-referrer
_browser_header.xRobotsTag	b938aebe-888e-45fc-beec-404ca79e1824	none
_browser_header.xFrameOptions	b938aebe-888e-45fc-beec-404ca79e1824	SAMEORIGIN
_browser_header.contentSecurityPolicy	b938aebe-888e-45fc-beec-404ca79e1824	frame-src 'self'; frame-ancestors 'self'; object-src 'none';
_browser_header.xXSSProtection	b938aebe-888e-45fc-beec-404ca79e1824	1; mode=block
_browser_header.strictTransportSecurity	b938aebe-888e-45fc-beec-404ca79e1824	max-age=31536000; includeSubDomains
bruteForceProtected	b938aebe-888e-45fc-beec-404ca79e1824	false
permanentLockout	b938aebe-888e-45fc-beec-404ca79e1824	false
maxFailureWaitSeconds	b938aebe-888e-45fc-beec-404ca79e1824	900
minimumQuickLoginWaitSeconds	b938aebe-888e-45fc-beec-404ca79e1824	60
waitIncrementSeconds	b938aebe-888e-45fc-beec-404ca79e1824	60
quickLoginCheckMilliSeconds	b938aebe-888e-45fc-beec-404ca79e1824	1000
maxDeltaTimeSeconds	b938aebe-888e-45fc-beec-404ca79e1824	43200
failureFactor	b938aebe-888e-45fc-beec-404ca79e1824	30
realmReusableOtpCode	b938aebe-888e-45fc-beec-404ca79e1824	false
displayName	b938aebe-888e-45fc-beec-404ca79e1824	Keycloak
displayNameHtml	b938aebe-888e-45fc-beec-404ca79e1824	<div class="kc-logo-text"><span>Keycloak</span></div>
defaultSignatureAlgorithm	b938aebe-888e-45fc-beec-404ca79e1824	RS256
offlineSessionMaxLifespanEnabled	b938aebe-888e-45fc-beec-404ca79e1824	false
offlineSessionMaxLifespan	b938aebe-888e-45fc-beec-404ca79e1824	5184000
\.


--
-- Data for Name: realm_default_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.realm_default_groups (realm_id, group_id) FROM stdin;
\.


--
-- Data for Name: realm_enabled_event_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.realm_enabled_event_types (realm_id, value) FROM stdin;
\.


--
-- Data for Name: realm_events_listeners; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.realm_events_listeners (realm_id, value) FROM stdin;
b938aebe-888e-45fc-beec-404ca79e1824	jboss-logging
\.


--
-- Data for Name: realm_localizations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.realm_localizations (realm_id, locale, texts) FROM stdin;
\.


--
-- Data for Name: realm_required_credential; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.realm_required_credential (type, form_label, input, secret, realm_id) FROM stdin;
password	password	t	t	b938aebe-888e-45fc-beec-404ca79e1824
\.


--
-- Data for Name: realm_smtp_config; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.realm_smtp_config (realm_id, value, name) FROM stdin;
\.


--
-- Data for Name: realm_supported_locales; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.realm_supported_locales (realm_id, value) FROM stdin;
\.


--
-- Data for Name: redirect_uris; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.redirect_uris (client_id, value) FROM stdin;
086168c8-0c23-4897-b277-9ab41cd09256	/realms/master/account/*
95a16281-c129-4cc2-8f85-d1e225808260	/realms/master/account/*
cc4cf8f3-57b9-47c6-a489-0cd5e817d1fd	/admin/master/console/*
17139c9f-1d33-46cc-9402-7a4efd1761a3	https://refae/*
e2247467-ce88-4f66-ad26-2f941c57481a	https://refae/*
\.


--
-- Data for Name: required_action_config; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.required_action_config (required_action_id, value, name) FROM stdin;
\.


--
-- Data for Name: required_action_provider; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.required_action_provider (id, alias, name, realm_id, enabled, default_action, provider_id, priority) FROM stdin;
abab85f9-9e9f-42ba-a6bc-aabc38d5b7b4	VERIFY_EMAIL	Verify Email	b938aebe-888e-45fc-beec-404ca79e1824	t	f	VERIFY_EMAIL	50
72a90b0b-5796-4792-9f1a-2efcb69cbba6	UPDATE_PROFILE	Update Profile	b938aebe-888e-45fc-beec-404ca79e1824	t	f	UPDATE_PROFILE	40
ff4c1372-7dcc-4dd0-b4bd-b72cd8157171	CONFIGURE_TOTP	Configure OTP	b938aebe-888e-45fc-beec-404ca79e1824	t	f	CONFIGURE_TOTP	10
535323e1-58f1-4b8a-afd5-dfd69b42c0e1	UPDATE_PASSWORD	Update Password	b938aebe-888e-45fc-beec-404ca79e1824	t	f	UPDATE_PASSWORD	30
1aa4ea04-b502-48e2-899b-d284f4a86567	TERMS_AND_CONDITIONS	Terms and Conditions	b938aebe-888e-45fc-beec-404ca79e1824	f	f	TERMS_AND_CONDITIONS	20
cf8bcb79-13ba-4526-9fd1-3bdc4f12cfe7	delete_account	Delete Account	b938aebe-888e-45fc-beec-404ca79e1824	f	f	delete_account	60
62cf6a3f-30a4-41d6-830e-5f67bda20326	update_user_locale	Update User Locale	b938aebe-888e-45fc-beec-404ca79e1824	t	f	update_user_locale	1000
9372da59-249b-4083-988d-5eba632ba359	UPDATE_EMAIL	Update Email	b938aebe-888e-45fc-beec-404ca79e1824	t	f	UPDATE_EMAIL	70
d6913b9d-6d7a-442b-8303-19bafc039cb7	CONFIGURE_RECOVERY_AUTHN_CODES	Recovery Authentication Codes	b938aebe-888e-45fc-beec-404ca79e1824	t	f	CONFIGURE_RECOVERY_AUTHN_CODES	70
686285d0-b550-4f33-b84e-f45c4c2deb10	webauthn-register	Webauthn Register	b938aebe-888e-45fc-beec-404ca79e1824	t	f	webauthn-register	70
6b49ed62-c29e-4029-a6be-9c0d1fff93df	webauthn-register-passwordless	Webauthn Register Passwordless	b938aebe-888e-45fc-beec-404ca79e1824	t	f	webauthn-register-passwordless	80
\.


--
-- Data for Name: resource_attribute; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.resource_attribute (id, name, value, resource_id) FROM stdin;
\.


--
-- Data for Name: resource_policy; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.resource_policy (resource_id, policy_id) FROM stdin;
\.


--
-- Data for Name: resource_scope; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.resource_scope (resource_id, scope_id) FROM stdin;
\.


--
-- Data for Name: resource_server; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.resource_server (id, allow_rs_remote_mgmt, policy_enforce_mode, decision_strategy) FROM stdin;
\.


--
-- Data for Name: resource_server_perm_ticket; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.resource_server_perm_ticket (id, owner, requester, created_timestamp, granted_timestamp, resource_id, scope_id, resource_server_id, policy_id) FROM stdin;
\.


--
-- Data for Name: resource_server_policy; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.resource_server_policy (id, name, description, type, decision_strategy, logic, resource_server_id, owner) FROM stdin;
\.


--
-- Data for Name: resource_server_resource; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.resource_server_resource (id, name, type, icon_uri, owner, resource_server_id, owner_managed_access, display_name) FROM stdin;
\.


--
-- Data for Name: resource_server_scope; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.resource_server_scope (id, name, icon_uri, resource_server_id, display_name) FROM stdin;
\.


--
-- Data for Name: resource_uris; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.resource_uris (resource_id, value) FROM stdin;
\.


--
-- Data for Name: role_attribute; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.role_attribute (id, role_id, name, value) FROM stdin;
\.


--
-- Data for Name: scope_mapping; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.scope_mapping (client_id, role_id) FROM stdin;
95a16281-c129-4cc2-8f85-d1e225808260	6716b6b3-d7a9-49b2-bde7-37a00a7b5d1e
95a16281-c129-4cc2-8f85-d1e225808260	bf294699-cdf9-4402-8510-c56d9af46f6d
\.


--
-- Data for Name: scope_policy; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.scope_policy (scope_id, policy_id) FROM stdin;
\.


--
-- Data for Name: user_attribute; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_attribute (name, value, user_id, id) FROM stdin;
\.


--
-- Data for Name: user_consent; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_consent (id, client_id, user_id, created_date, last_updated_date, client_storage_provider, external_client_id) FROM stdin;
\.


--
-- Data for Name: user_consent_client_scope; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_consent_client_scope (user_consent_id, scope_id) FROM stdin;
\.


--
-- Data for Name: user_entity; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_entity (id, email, email_constraint, email_verified, enabled, federation_link, first_name, last_name, realm_id, username, created_timestamp, service_account_client_link, not_before) FROM stdin;
699cc64d-e041-49c5-8cd9-2829e7618431	\N	79e7b5c9-5da7-49ce-b27a-d251fc6f8bf5	f	t	\N	\N	\N	b938aebe-888e-45fc-beec-404ca79e1824	admin	1700728697829	\N	0
154b4fe9-0f22-47d2-bca8-a0df8e9fa2a3	\N	96e344c4-30eb-4eb0-8acc-7d28d8c6054a	t	t	\N			b938aebe-888e-45fc-beec-404ca79e1824	openid	1700729035818	\N	0
\.


--
-- Data for Name: user_federation_config; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_federation_config (user_federation_provider_id, value, name) FROM stdin;
\.


--
-- Data for Name: user_federation_mapper; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_federation_mapper (id, name, federation_provider_id, federation_mapper_type, realm_id) FROM stdin;
\.


--
-- Data for Name: user_federation_mapper_config; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_federation_mapper_config (user_federation_mapper_id, value, name) FROM stdin;
\.


--
-- Data for Name: user_federation_provider; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_federation_provider (id, changed_sync_period, display_name, full_sync_period, last_sync, priority, provider_name, realm_id) FROM stdin;
\.


--
-- Data for Name: user_group_membership; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_group_membership (group_id, user_id) FROM stdin;
\.


--
-- Data for Name: user_required_action; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_required_action (user_id, required_action) FROM stdin;
\.


--
-- Data for Name: user_role_mapping; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_role_mapping (role_id, user_id) FROM stdin;
52e01ede-b6ad-4985-ab21-0eee35fee7b6	699cc64d-e041-49c5-8cd9-2829e7618431
33ace452-b68c-4888-beae-20ddf10736ed	699cc64d-e041-49c5-8cd9-2829e7618431
52e01ede-b6ad-4985-ab21-0eee35fee7b6	154b4fe9-0f22-47d2-bca8-a0df8e9fa2a3
\.


--
-- Data for Name: user_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_session (id, auth_method, ip_address, last_session_refresh, login_username, realm_id, remember_me, started, user_id, user_session_state, broker_session_id, broker_user_id) FROM stdin;
\.


--
-- Data for Name: user_session_note; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_session_note (user_session, name, value) FROM stdin;
\.


--
-- Data for Name: username_login_failure; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.username_login_failure (realm_id, username, failed_login_not_before, last_failure, last_ip_failure, num_failures) FROM stdin;
\.


--
-- Data for Name: web_origins; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.web_origins (client_id, value) FROM stdin;
cc4cf8f3-57b9-47c6-a489-0cd5e817d1fd	+
17139c9f-1d33-46cc-9402-7a4efd1761a3	*
e2247467-ce88-4f66-ad26-2f941c57481a	*
\.


--
-- Name: username_login_failure CONSTRAINT_17-2; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.username_login_failure
    ADD CONSTRAINT "CONSTRAINT_17-2" PRIMARY KEY (realm_id, username);


--
-- Name: keycloak_role UK_J3RWUVD56ONTGSUHOGM184WW2-2; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.keycloak_role
    ADD CONSTRAINT "UK_J3RWUVD56ONTGSUHOGM184WW2-2" UNIQUE (name, client_realm_constraint);


--
-- Name: client_auth_flow_bindings c_cli_flow_bind; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_auth_flow_bindings
    ADD CONSTRAINT c_cli_flow_bind PRIMARY KEY (client_id, binding_name);


--
-- Name: client_scope_client c_cli_scope_bind; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_scope_client
    ADD CONSTRAINT c_cli_scope_bind PRIMARY KEY (client_id, scope_id);


--
-- Name: client_initial_access cnstr_client_init_acc_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_initial_access
    ADD CONSTRAINT cnstr_client_init_acc_pk PRIMARY KEY (id);


--
-- Name: realm_default_groups con_group_id_def_groups; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.realm_default_groups
    ADD CONSTRAINT con_group_id_def_groups UNIQUE (group_id);


--
-- Name: broker_link constr_broker_link_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.broker_link
    ADD CONSTRAINT constr_broker_link_pk PRIMARY KEY (identity_provider, user_id);


--
-- Name: client_user_session_note constr_cl_usr_ses_note; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_user_session_note
    ADD CONSTRAINT constr_cl_usr_ses_note PRIMARY KEY (client_session, name);


--
-- Name: component_config constr_component_config_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.component_config
    ADD CONSTRAINT constr_component_config_pk PRIMARY KEY (id);


--
-- Name: component constr_component_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.component
    ADD CONSTRAINT constr_component_pk PRIMARY KEY (id);


--
-- Name: fed_user_required_action constr_fed_required_action; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fed_user_required_action
    ADD CONSTRAINT constr_fed_required_action PRIMARY KEY (required_action, user_id);


--
-- Name: fed_user_attribute constr_fed_user_attr_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fed_user_attribute
    ADD CONSTRAINT constr_fed_user_attr_pk PRIMARY KEY (id);


--
-- Name: fed_user_consent constr_fed_user_consent_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fed_user_consent
    ADD CONSTRAINT constr_fed_user_consent_pk PRIMARY KEY (id);


--
-- Name: fed_user_credential constr_fed_user_cred_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fed_user_credential
    ADD CONSTRAINT constr_fed_user_cred_pk PRIMARY KEY (id);


--
-- Name: fed_user_group_membership constr_fed_user_group; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fed_user_group_membership
    ADD CONSTRAINT constr_fed_user_group PRIMARY KEY (group_id, user_id);


--
-- Name: fed_user_role_mapping constr_fed_user_role; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fed_user_role_mapping
    ADD CONSTRAINT constr_fed_user_role PRIMARY KEY (role_id, user_id);


--
-- Name: federated_user constr_federated_user; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.federated_user
    ADD CONSTRAINT constr_federated_user PRIMARY KEY (id);


--
-- Name: realm_default_groups constr_realm_default_groups; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.realm_default_groups
    ADD CONSTRAINT constr_realm_default_groups PRIMARY KEY (realm_id, group_id);


--
-- Name: realm_enabled_event_types constr_realm_enabl_event_types; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.realm_enabled_event_types
    ADD CONSTRAINT constr_realm_enabl_event_types PRIMARY KEY (realm_id, value);


--
-- Name: realm_events_listeners constr_realm_events_listeners; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.realm_events_listeners
    ADD CONSTRAINT constr_realm_events_listeners PRIMARY KEY (realm_id, value);


--
-- Name: realm_supported_locales constr_realm_supported_locales; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.realm_supported_locales
    ADD CONSTRAINT constr_realm_supported_locales PRIMARY KEY (realm_id, value);


--
-- Name: identity_provider constraint_2b; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.identity_provider
    ADD CONSTRAINT constraint_2b PRIMARY KEY (internal_id);


--
-- Name: client_attributes constraint_3c; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_attributes
    ADD CONSTRAINT constraint_3c PRIMARY KEY (client_id, name);


--
-- Name: event_entity constraint_4; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event_entity
    ADD CONSTRAINT constraint_4 PRIMARY KEY (id);


--
-- Name: federated_identity constraint_40; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.federated_identity
    ADD CONSTRAINT constraint_40 PRIMARY KEY (identity_provider, user_id);


--
-- Name: realm constraint_4a; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.realm
    ADD CONSTRAINT constraint_4a PRIMARY KEY (id);


--
-- Name: client_session_role constraint_5; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_session_role
    ADD CONSTRAINT constraint_5 PRIMARY KEY (client_session, role_id);


--
-- Name: user_session constraint_57; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_session
    ADD CONSTRAINT constraint_57 PRIMARY KEY (id);


--
-- Name: user_federation_provider constraint_5c; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_federation_provider
    ADD CONSTRAINT constraint_5c PRIMARY KEY (id);


--
-- Name: client_session_note constraint_5e; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_session_note
    ADD CONSTRAINT constraint_5e PRIMARY KEY (client_session, name);


--
-- Name: client constraint_7; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT constraint_7 PRIMARY KEY (id);


--
-- Name: client_session constraint_8; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_session
    ADD CONSTRAINT constraint_8 PRIMARY KEY (id);


--
-- Name: scope_mapping constraint_81; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scope_mapping
    ADD CONSTRAINT constraint_81 PRIMARY KEY (client_id, role_id);


--
-- Name: client_node_registrations constraint_84; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_node_registrations
    ADD CONSTRAINT constraint_84 PRIMARY KEY (client_id, name);


--
-- Name: realm_attribute constraint_9; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.realm_attribute
    ADD CONSTRAINT constraint_9 PRIMARY KEY (name, realm_id);


--
-- Name: realm_required_credential constraint_92; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.realm_required_credential
    ADD CONSTRAINT constraint_92 PRIMARY KEY (realm_id, type);


--
-- Name: keycloak_role constraint_a; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.keycloak_role
    ADD CONSTRAINT constraint_a PRIMARY KEY (id);


--
-- Name: admin_event_entity constraint_admin_event_entity; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.admin_event_entity
    ADD CONSTRAINT constraint_admin_event_entity PRIMARY KEY (id);


--
-- Name: authenticator_config_entry constraint_auth_cfg_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.authenticator_config_entry
    ADD CONSTRAINT constraint_auth_cfg_pk PRIMARY KEY (authenticator_id, name);


--
-- Name: authentication_execution constraint_auth_exec_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.authentication_execution
    ADD CONSTRAINT constraint_auth_exec_pk PRIMARY KEY (id);


--
-- Name: authentication_flow constraint_auth_flow_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.authentication_flow
    ADD CONSTRAINT constraint_auth_flow_pk PRIMARY KEY (id);


--
-- Name: authenticator_config constraint_auth_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.authenticator_config
    ADD CONSTRAINT constraint_auth_pk PRIMARY KEY (id);


--
-- Name: client_session_auth_status constraint_auth_status_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_session_auth_status
    ADD CONSTRAINT constraint_auth_status_pk PRIMARY KEY (client_session, authenticator);


--
-- Name: user_role_mapping constraint_c; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_role_mapping
    ADD CONSTRAINT constraint_c PRIMARY KEY (role_id, user_id);


--
-- Name: composite_role constraint_composite_role; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composite_role
    ADD CONSTRAINT constraint_composite_role PRIMARY KEY (composite, child_role);


--
-- Name: client_session_prot_mapper constraint_cs_pmp_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_session_prot_mapper
    ADD CONSTRAINT constraint_cs_pmp_pk PRIMARY KEY (client_session, protocol_mapper_id);


--
-- Name: identity_provider_config constraint_d; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.identity_provider_config
    ADD CONSTRAINT constraint_d PRIMARY KEY (identity_provider_id, name);


--
-- Name: policy_config constraint_dpc; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.policy_config
    ADD CONSTRAINT constraint_dpc PRIMARY KEY (policy_id, name);


--
-- Name: realm_smtp_config constraint_e; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.realm_smtp_config
    ADD CONSTRAINT constraint_e PRIMARY KEY (realm_id, name);


--
-- Name: credential constraint_f; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.credential
    ADD CONSTRAINT constraint_f PRIMARY KEY (id);


--
-- Name: user_federation_config constraint_f9; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_federation_config
    ADD CONSTRAINT constraint_f9 PRIMARY KEY (user_federation_provider_id, name);


--
-- Name: resource_server_perm_ticket constraint_fapmt; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT constraint_fapmt PRIMARY KEY (id);


--
-- Name: resource_server_resource constraint_farsr; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_server_resource
    ADD CONSTRAINT constraint_farsr PRIMARY KEY (id);


--
-- Name: resource_server_policy constraint_farsrp; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_server_policy
    ADD CONSTRAINT constraint_farsrp PRIMARY KEY (id);


--
-- Name: associated_policy constraint_farsrpap; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.associated_policy
    ADD CONSTRAINT constraint_farsrpap PRIMARY KEY (policy_id, associated_policy_id);


--
-- Name: resource_policy constraint_farsrpp; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_policy
    ADD CONSTRAINT constraint_farsrpp PRIMARY KEY (resource_id, policy_id);


--
-- Name: resource_server_scope constraint_farsrs; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_server_scope
    ADD CONSTRAINT constraint_farsrs PRIMARY KEY (id);


--
-- Name: resource_scope constraint_farsrsp; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_scope
    ADD CONSTRAINT constraint_farsrsp PRIMARY KEY (resource_id, scope_id);


--
-- Name: scope_policy constraint_farsrsps; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scope_policy
    ADD CONSTRAINT constraint_farsrsps PRIMARY KEY (scope_id, policy_id);


--
-- Name: user_entity constraint_fb; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT constraint_fb PRIMARY KEY (id);


--
-- Name: user_federation_mapper_config constraint_fedmapper_cfg_pm; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_federation_mapper_config
    ADD CONSTRAINT constraint_fedmapper_cfg_pm PRIMARY KEY (user_federation_mapper_id, name);


--
-- Name: user_federation_mapper constraint_fedmapperpm; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_federation_mapper
    ADD CONSTRAINT constraint_fedmapperpm PRIMARY KEY (id);


--
-- Name: fed_user_consent_cl_scope constraint_fgrntcsnt_clsc_pm; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fed_user_consent_cl_scope
    ADD CONSTRAINT constraint_fgrntcsnt_clsc_pm PRIMARY KEY (user_consent_id, scope_id);


--
-- Name: user_consent_client_scope constraint_grntcsnt_clsc_pm; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_consent_client_scope
    ADD CONSTRAINT constraint_grntcsnt_clsc_pm PRIMARY KEY (user_consent_id, scope_id);


--
-- Name: user_consent constraint_grntcsnt_pm; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_consent
    ADD CONSTRAINT constraint_grntcsnt_pm PRIMARY KEY (id);


--
-- Name: keycloak_group constraint_group; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.keycloak_group
    ADD CONSTRAINT constraint_group PRIMARY KEY (id);


--
-- Name: group_attribute constraint_group_attribute_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_attribute
    ADD CONSTRAINT constraint_group_attribute_pk PRIMARY KEY (id);


--
-- Name: group_role_mapping constraint_group_role; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_role_mapping
    ADD CONSTRAINT constraint_group_role PRIMARY KEY (role_id, group_id);


--
-- Name: identity_provider_mapper constraint_idpm; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.identity_provider_mapper
    ADD CONSTRAINT constraint_idpm PRIMARY KEY (id);


--
-- Name: idp_mapper_config constraint_idpmconfig; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.idp_mapper_config
    ADD CONSTRAINT constraint_idpmconfig PRIMARY KEY (idp_mapper_id, name);


--
-- Name: migration_model constraint_migmod; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migration_model
    ADD CONSTRAINT constraint_migmod PRIMARY KEY (id);


--
-- Name: offline_client_session constraint_offl_cl_ses_pk3; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.offline_client_session
    ADD CONSTRAINT constraint_offl_cl_ses_pk3 PRIMARY KEY (user_session_id, client_id, client_storage_provider, external_client_id, offline_flag);


--
-- Name: offline_user_session constraint_offl_us_ses_pk2; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.offline_user_session
    ADD CONSTRAINT constraint_offl_us_ses_pk2 PRIMARY KEY (user_session_id, offline_flag);


--
-- Name: protocol_mapper constraint_pcm; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.protocol_mapper
    ADD CONSTRAINT constraint_pcm PRIMARY KEY (id);


--
-- Name: protocol_mapper_config constraint_pmconfig; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.protocol_mapper_config
    ADD CONSTRAINT constraint_pmconfig PRIMARY KEY (protocol_mapper_id, name);


--
-- Name: redirect_uris constraint_redirect_uris; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.redirect_uris
    ADD CONSTRAINT constraint_redirect_uris PRIMARY KEY (client_id, value);


--
-- Name: required_action_config constraint_req_act_cfg_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.required_action_config
    ADD CONSTRAINT constraint_req_act_cfg_pk PRIMARY KEY (required_action_id, name);


--
-- Name: required_action_provider constraint_req_act_prv_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.required_action_provider
    ADD CONSTRAINT constraint_req_act_prv_pk PRIMARY KEY (id);


--
-- Name: user_required_action constraint_required_action; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_required_action
    ADD CONSTRAINT constraint_required_action PRIMARY KEY (required_action, user_id);


--
-- Name: resource_uris constraint_resour_uris_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_uris
    ADD CONSTRAINT constraint_resour_uris_pk PRIMARY KEY (resource_id, value);


--
-- Name: role_attribute constraint_role_attribute_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_attribute
    ADD CONSTRAINT constraint_role_attribute_pk PRIMARY KEY (id);


--
-- Name: user_attribute constraint_user_attribute_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_attribute
    ADD CONSTRAINT constraint_user_attribute_pk PRIMARY KEY (id);


--
-- Name: user_group_membership constraint_user_group; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_group_membership
    ADD CONSTRAINT constraint_user_group PRIMARY KEY (group_id, user_id);


--
-- Name: user_session_note constraint_usn_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_session_note
    ADD CONSTRAINT constraint_usn_pk PRIMARY KEY (user_session, name);


--
-- Name: web_origins constraint_web_origins; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.web_origins
    ADD CONSTRAINT constraint_web_origins PRIMARY KEY (client_id, value);


--
-- Name: databasechangeloglock databasechangeloglock_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.databasechangeloglock
    ADD CONSTRAINT databasechangeloglock_pkey PRIMARY KEY (id);


--
-- Name: client_scope_attributes pk_cl_tmpl_attr; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_scope_attributes
    ADD CONSTRAINT pk_cl_tmpl_attr PRIMARY KEY (scope_id, name);


--
-- Name: client_scope pk_cli_template; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_scope
    ADD CONSTRAINT pk_cli_template PRIMARY KEY (id);


--
-- Name: resource_server pk_resource_server; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_server
    ADD CONSTRAINT pk_resource_server PRIMARY KEY (id);


--
-- Name: client_scope_role_mapping pk_template_scope; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_scope_role_mapping
    ADD CONSTRAINT pk_template_scope PRIMARY KEY (scope_id, role_id);


--
-- Name: default_client_scope r_def_cli_scope_bind; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.default_client_scope
    ADD CONSTRAINT r_def_cli_scope_bind PRIMARY KEY (realm_id, scope_id);


--
-- Name: realm_localizations realm_localizations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.realm_localizations
    ADD CONSTRAINT realm_localizations_pkey PRIMARY KEY (realm_id, locale);


--
-- Name: resource_attribute res_attr_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_attribute
    ADD CONSTRAINT res_attr_pk PRIMARY KEY (id);


--
-- Name: keycloak_group sibling_names; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.keycloak_group
    ADD CONSTRAINT sibling_names UNIQUE (realm_id, parent_group, name);


--
-- Name: identity_provider uk_2daelwnibji49avxsrtuf6xj33; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.identity_provider
    ADD CONSTRAINT uk_2daelwnibji49avxsrtuf6xj33 UNIQUE (provider_alias, realm_id);


--
-- Name: client uk_b71cjlbenv945rb6gcon438at; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT uk_b71cjlbenv945rb6gcon438at UNIQUE (realm_id, client_id);


--
-- Name: client_scope uk_cli_scope; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_scope
    ADD CONSTRAINT uk_cli_scope UNIQUE (realm_id, name);


--
-- Name: user_entity uk_dykn684sl8up1crfei6eckhd7; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT uk_dykn684sl8up1crfei6eckhd7 UNIQUE (realm_id, email_constraint);


--
-- Name: resource_server_resource uk_frsr6t700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_server_resource
    ADD CONSTRAINT uk_frsr6t700s9v50bu18ws5ha6 UNIQUE (name, owner, resource_server_id);


--
-- Name: resource_server_perm_ticket uk_frsr6t700s9v50bu18ws5pmt; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT uk_frsr6t700s9v50bu18ws5pmt UNIQUE (owner, requester, resource_server_id, resource_id, scope_id);


--
-- Name: resource_server_policy uk_frsrpt700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_server_policy
    ADD CONSTRAINT uk_frsrpt700s9v50bu18ws5ha6 UNIQUE (name, resource_server_id);


--
-- Name: resource_server_scope uk_frsrst700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_server_scope
    ADD CONSTRAINT uk_frsrst700s9v50bu18ws5ha6 UNIQUE (name, resource_server_id);


--
-- Name: user_consent uk_jkuwuvd56ontgsuhogm8uewrt; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_consent
    ADD CONSTRAINT uk_jkuwuvd56ontgsuhogm8uewrt UNIQUE (client_id, client_storage_provider, external_client_id, user_id);


--
-- Name: realm uk_orvsdmla56612eaefiq6wl5oi; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.realm
    ADD CONSTRAINT uk_orvsdmla56612eaefiq6wl5oi UNIQUE (name);


--
-- Name: user_entity uk_ru8tt6t700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT uk_ru8tt6t700s9v50bu18ws5ha6 UNIQUE (realm_id, username);


--
-- Name: idx_admin_event_time; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_admin_event_time ON public.admin_event_entity USING btree (realm_id, admin_event_time);


--
-- Name: idx_assoc_pol_assoc_pol_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_assoc_pol_assoc_pol_id ON public.associated_policy USING btree (associated_policy_id);


--
-- Name: idx_auth_config_realm; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_auth_config_realm ON public.authenticator_config USING btree (realm_id);


--
-- Name: idx_auth_exec_flow; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_auth_exec_flow ON public.authentication_execution USING btree (flow_id);


--
-- Name: idx_auth_exec_realm_flow; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_auth_exec_realm_flow ON public.authentication_execution USING btree (realm_id, flow_id);


--
-- Name: idx_auth_flow_realm; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_auth_flow_realm ON public.authentication_flow USING btree (realm_id);


--
-- Name: idx_cl_clscope; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_cl_clscope ON public.client_scope_client USING btree (scope_id);


--
-- Name: idx_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_client_id ON public.client USING btree (client_id);


--
-- Name: idx_client_init_acc_realm; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_client_init_acc_realm ON public.client_initial_access USING btree (realm_id);


--
-- Name: idx_client_session_session; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_client_session_session ON public.client_session USING btree (session_id);


--
-- Name: idx_clscope_attrs; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_clscope_attrs ON public.client_scope_attributes USING btree (scope_id);


--
-- Name: idx_clscope_cl; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_clscope_cl ON public.client_scope_client USING btree (client_id);


--
-- Name: idx_clscope_protmap; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_clscope_protmap ON public.protocol_mapper USING btree (client_scope_id);


--
-- Name: idx_clscope_role; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_clscope_role ON public.client_scope_role_mapping USING btree (scope_id);


--
-- Name: idx_compo_config_compo; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_compo_config_compo ON public.component_config USING btree (component_id);


--
-- Name: idx_component_provider_type; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_component_provider_type ON public.component USING btree (provider_type);


--
-- Name: idx_component_realm; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_component_realm ON public.component USING btree (realm_id);


--
-- Name: idx_composite; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_composite ON public.composite_role USING btree (composite);


--
-- Name: idx_composite_child; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_composite_child ON public.composite_role USING btree (child_role);


--
-- Name: idx_defcls_realm; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_defcls_realm ON public.default_client_scope USING btree (realm_id);


--
-- Name: idx_defcls_scope; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_defcls_scope ON public.default_client_scope USING btree (scope_id);


--
-- Name: idx_event_time; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_event_time ON public.event_entity USING btree (realm_id, event_time);


--
-- Name: idx_fedidentity_feduser; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_fedidentity_feduser ON public.federated_identity USING btree (federated_user_id);


--
-- Name: idx_fedidentity_user; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_fedidentity_user ON public.federated_identity USING btree (user_id);


--
-- Name: idx_fu_attribute; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_fu_attribute ON public.fed_user_attribute USING btree (user_id, realm_id, name);


--
-- Name: idx_fu_cnsnt_ext; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_fu_cnsnt_ext ON public.fed_user_consent USING btree (user_id, client_storage_provider, external_client_id);


--
-- Name: idx_fu_consent; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_fu_consent ON public.fed_user_consent USING btree (user_id, client_id);


--
-- Name: idx_fu_consent_ru; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_fu_consent_ru ON public.fed_user_consent USING btree (realm_id, user_id);


--
-- Name: idx_fu_credential; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_fu_credential ON public.fed_user_credential USING btree (user_id, type);


--
-- Name: idx_fu_credential_ru; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_fu_credential_ru ON public.fed_user_credential USING btree (realm_id, user_id);


--
-- Name: idx_fu_group_membership; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_fu_group_membership ON public.fed_user_group_membership USING btree (user_id, group_id);


--
-- Name: idx_fu_group_membership_ru; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_fu_group_membership_ru ON public.fed_user_group_membership USING btree (realm_id, user_id);


--
-- Name: idx_fu_required_action; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_fu_required_action ON public.fed_user_required_action USING btree (user_id, required_action);


--
-- Name: idx_fu_required_action_ru; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_fu_required_action_ru ON public.fed_user_required_action USING btree (realm_id, user_id);


--
-- Name: idx_fu_role_mapping; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_fu_role_mapping ON public.fed_user_role_mapping USING btree (user_id, role_id);


--
-- Name: idx_fu_role_mapping_ru; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_fu_role_mapping_ru ON public.fed_user_role_mapping USING btree (realm_id, user_id);


--
-- Name: idx_group_att_by_name_value; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_group_att_by_name_value ON public.group_attribute USING btree (name, ((value)::character varying(250)));


--
-- Name: idx_group_attr_group; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_group_attr_group ON public.group_attribute USING btree (group_id);


--
-- Name: idx_group_role_mapp_group; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_group_role_mapp_group ON public.group_role_mapping USING btree (group_id);


--
-- Name: idx_id_prov_mapp_realm; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_id_prov_mapp_realm ON public.identity_provider_mapper USING btree (realm_id);


--
-- Name: idx_ident_prov_realm; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_ident_prov_realm ON public.identity_provider USING btree (realm_id);


--
-- Name: idx_keycloak_role_client; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_keycloak_role_client ON public.keycloak_role USING btree (client);


--
-- Name: idx_keycloak_role_realm; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_keycloak_role_realm ON public.keycloak_role USING btree (realm);


--
-- Name: idx_offline_css_preload; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_offline_css_preload ON public.offline_client_session USING btree (client_id, offline_flag);


--
-- Name: idx_offline_uss_by_user; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_offline_uss_by_user ON public.offline_user_session USING btree (user_id, realm_id, offline_flag);


--
-- Name: idx_offline_uss_by_usersess; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_offline_uss_by_usersess ON public.offline_user_session USING btree (realm_id, offline_flag, user_session_id);


--
-- Name: idx_offline_uss_createon; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_offline_uss_createon ON public.offline_user_session USING btree (created_on);


--
-- Name: idx_offline_uss_preload; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_offline_uss_preload ON public.offline_user_session USING btree (offline_flag, created_on, user_session_id);


--
-- Name: idx_protocol_mapper_client; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_protocol_mapper_client ON public.protocol_mapper USING btree (client_id);


--
-- Name: idx_realm_attr_realm; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_realm_attr_realm ON public.realm_attribute USING btree (realm_id);


--
-- Name: idx_realm_clscope; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_realm_clscope ON public.client_scope USING btree (realm_id);


--
-- Name: idx_realm_def_grp_realm; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_realm_def_grp_realm ON public.realm_default_groups USING btree (realm_id);


--
-- Name: idx_realm_evt_list_realm; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_realm_evt_list_realm ON public.realm_events_listeners USING btree (realm_id);


--
-- Name: idx_realm_evt_types_realm; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_realm_evt_types_realm ON public.realm_enabled_event_types USING btree (realm_id);


--
-- Name: idx_realm_master_adm_cli; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_realm_master_adm_cli ON public.realm USING btree (master_admin_client);


--
-- Name: idx_realm_supp_local_realm; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_realm_supp_local_realm ON public.realm_supported_locales USING btree (realm_id);


--
-- Name: idx_redir_uri_client; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_redir_uri_client ON public.redirect_uris USING btree (client_id);


--
-- Name: idx_req_act_prov_realm; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_req_act_prov_realm ON public.required_action_provider USING btree (realm_id);


--
-- Name: idx_res_policy_policy; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_res_policy_policy ON public.resource_policy USING btree (policy_id);


--
-- Name: idx_res_scope_scope; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_res_scope_scope ON public.resource_scope USING btree (scope_id);


--
-- Name: idx_res_serv_pol_res_serv; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_res_serv_pol_res_serv ON public.resource_server_policy USING btree (resource_server_id);


--
-- Name: idx_res_srv_res_res_srv; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_res_srv_res_res_srv ON public.resource_server_resource USING btree (resource_server_id);


--
-- Name: idx_res_srv_scope_res_srv; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_res_srv_scope_res_srv ON public.resource_server_scope USING btree (resource_server_id);


--
-- Name: idx_role_attribute; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_role_attribute ON public.role_attribute USING btree (role_id);


--
-- Name: idx_role_clscope; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_role_clscope ON public.client_scope_role_mapping USING btree (role_id);


--
-- Name: idx_scope_mapping_role; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_scope_mapping_role ON public.scope_mapping USING btree (role_id);


--
-- Name: idx_scope_policy_policy; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_scope_policy_policy ON public.scope_policy USING btree (policy_id);


--
-- Name: idx_update_time; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_update_time ON public.migration_model USING btree (update_time);


--
-- Name: idx_us_sess_id_on_cl_sess; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_us_sess_id_on_cl_sess ON public.offline_client_session USING btree (user_session_id);


--
-- Name: idx_usconsent_clscope; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_usconsent_clscope ON public.user_consent_client_scope USING btree (user_consent_id);


--
-- Name: idx_user_attribute; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_user_attribute ON public.user_attribute USING btree (user_id);


--
-- Name: idx_user_attribute_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_user_attribute_name ON public.user_attribute USING btree (name, value);


--
-- Name: idx_user_consent; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_user_consent ON public.user_consent USING btree (user_id);


--
-- Name: idx_user_credential; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_user_credential ON public.credential USING btree (user_id);


--
-- Name: idx_user_email; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_user_email ON public.user_entity USING btree (email);


--
-- Name: idx_user_group_mapping; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_user_group_mapping ON public.user_group_membership USING btree (user_id);


--
-- Name: idx_user_reqactions; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_user_reqactions ON public.user_required_action USING btree (user_id);


--
-- Name: idx_user_role_mapping; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_user_role_mapping ON public.user_role_mapping USING btree (user_id);


--
-- Name: idx_user_service_account; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_user_service_account ON public.user_entity USING btree (realm_id, service_account_client_link);


--
-- Name: idx_usr_fed_map_fed_prv; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_usr_fed_map_fed_prv ON public.user_federation_mapper USING btree (federation_provider_id);


--
-- Name: idx_usr_fed_map_realm; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_usr_fed_map_realm ON public.user_federation_mapper USING btree (realm_id);


--
-- Name: idx_usr_fed_prv_realm; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_usr_fed_prv_realm ON public.user_federation_provider USING btree (realm_id);


--
-- Name: idx_web_orig_client; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_web_orig_client ON public.web_origins USING btree (client_id);


--
-- Name: client_session_auth_status auth_status_constraint; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_session_auth_status
    ADD CONSTRAINT auth_status_constraint FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: identity_provider fk2b4ebc52ae5c3b34; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.identity_provider
    ADD CONSTRAINT fk2b4ebc52ae5c3b34 FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: client_attributes fk3c47c64beacca966; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_attributes
    ADD CONSTRAINT fk3c47c64beacca966 FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: federated_identity fk404288b92ef007a6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.federated_identity
    ADD CONSTRAINT fk404288b92ef007a6 FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: client_node_registrations fk4129723ba992f594; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_node_registrations
    ADD CONSTRAINT fk4129723ba992f594 FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: client_session_note fk5edfb00ff51c2736; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_session_note
    ADD CONSTRAINT fk5edfb00ff51c2736 FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: user_session_note fk5edfb00ff51d3472; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_session_note
    ADD CONSTRAINT fk5edfb00ff51d3472 FOREIGN KEY (user_session) REFERENCES public.user_session(id);


--
-- Name: client_session_role fk_11b7sgqw18i532811v7o2dv76; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_session_role
    ADD CONSTRAINT fk_11b7sgqw18i532811v7o2dv76 FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: redirect_uris fk_1burs8pb4ouj97h5wuppahv9f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.redirect_uris
    ADD CONSTRAINT fk_1burs8pb4ouj97h5wuppahv9f FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: user_federation_provider fk_1fj32f6ptolw2qy60cd8n01e8; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_federation_provider
    ADD CONSTRAINT fk_1fj32f6ptolw2qy60cd8n01e8 FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: client_session_prot_mapper fk_33a8sgqw18i532811v7o2dk89; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_session_prot_mapper
    ADD CONSTRAINT fk_33a8sgqw18i532811v7o2dk89 FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: realm_required_credential fk_5hg65lybevavkqfki3kponh9v; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.realm_required_credential
    ADD CONSTRAINT fk_5hg65lybevavkqfki3kponh9v FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: resource_attribute fk_5hrm2vlf9ql5fu022kqepovbr; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_attribute
    ADD CONSTRAINT fk_5hrm2vlf9ql5fu022kqepovbr FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: user_attribute fk_5hrm2vlf9ql5fu043kqepovbr; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_attribute
    ADD CONSTRAINT fk_5hrm2vlf9ql5fu043kqepovbr FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: user_required_action fk_6qj3w1jw9cvafhe19bwsiuvmd; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_required_action
    ADD CONSTRAINT fk_6qj3w1jw9cvafhe19bwsiuvmd FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: keycloak_role fk_6vyqfe4cn4wlq8r6kt5vdsj5c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.keycloak_role
    ADD CONSTRAINT fk_6vyqfe4cn4wlq8r6kt5vdsj5c FOREIGN KEY (realm) REFERENCES public.realm(id);


--
-- Name: realm_smtp_config fk_70ej8xdxgxd0b9hh6180irr0o; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.realm_smtp_config
    ADD CONSTRAINT fk_70ej8xdxgxd0b9hh6180irr0o FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: realm_attribute fk_8shxd6l3e9atqukacxgpffptw; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.realm_attribute
    ADD CONSTRAINT fk_8shxd6l3e9atqukacxgpffptw FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: composite_role fk_a63wvekftu8jo1pnj81e7mce2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composite_role
    ADD CONSTRAINT fk_a63wvekftu8jo1pnj81e7mce2 FOREIGN KEY (composite) REFERENCES public.keycloak_role(id);


--
-- Name: authentication_execution fk_auth_exec_flow; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.authentication_execution
    ADD CONSTRAINT fk_auth_exec_flow FOREIGN KEY (flow_id) REFERENCES public.authentication_flow(id);


--
-- Name: authentication_execution fk_auth_exec_realm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.authentication_execution
    ADD CONSTRAINT fk_auth_exec_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: authentication_flow fk_auth_flow_realm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.authentication_flow
    ADD CONSTRAINT fk_auth_flow_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: authenticator_config fk_auth_realm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.authenticator_config
    ADD CONSTRAINT fk_auth_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: client_session fk_b4ao2vcvat6ukau74wbwtfqo1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_session
    ADD CONSTRAINT fk_b4ao2vcvat6ukau74wbwtfqo1 FOREIGN KEY (session_id) REFERENCES public.user_session(id);


--
-- Name: user_role_mapping fk_c4fqv34p1mbylloxang7b1q3l; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_role_mapping
    ADD CONSTRAINT fk_c4fqv34p1mbylloxang7b1q3l FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: client_scope_attributes fk_cl_scope_attr_scope; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_scope_attributes
    ADD CONSTRAINT fk_cl_scope_attr_scope FOREIGN KEY (scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_scope_role_mapping fk_cl_scope_rm_scope; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_scope_role_mapping
    ADD CONSTRAINT fk_cl_scope_rm_scope FOREIGN KEY (scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_user_session_note fk_cl_usr_ses_note; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_user_session_note
    ADD CONSTRAINT fk_cl_usr_ses_note FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: protocol_mapper fk_cli_scope_mapper; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.protocol_mapper
    ADD CONSTRAINT fk_cli_scope_mapper FOREIGN KEY (client_scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_initial_access fk_client_init_acc_realm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_initial_access
    ADD CONSTRAINT fk_client_init_acc_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: component_config fk_component_config; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.component_config
    ADD CONSTRAINT fk_component_config FOREIGN KEY (component_id) REFERENCES public.component(id);


--
-- Name: component fk_component_realm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.component
    ADD CONSTRAINT fk_component_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: realm_default_groups fk_def_groups_realm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.realm_default_groups
    ADD CONSTRAINT fk_def_groups_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: user_federation_mapper_config fk_fedmapper_cfg; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_federation_mapper_config
    ADD CONSTRAINT fk_fedmapper_cfg FOREIGN KEY (user_federation_mapper_id) REFERENCES public.user_federation_mapper(id);


--
-- Name: user_federation_mapper fk_fedmapperpm_fedprv; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_federation_mapper
    ADD CONSTRAINT fk_fedmapperpm_fedprv FOREIGN KEY (federation_provider_id) REFERENCES public.user_federation_provider(id);


--
-- Name: user_federation_mapper fk_fedmapperpm_realm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_federation_mapper
    ADD CONSTRAINT fk_fedmapperpm_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: associated_policy fk_frsr5s213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.associated_policy
    ADD CONSTRAINT fk_frsr5s213xcx4wnkog82ssrfy FOREIGN KEY (associated_policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: scope_policy fk_frsrasp13xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scope_policy
    ADD CONSTRAINT fk_frsrasp13xcx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: resource_server_perm_ticket fk_frsrho213xcx4wnkog82sspmt; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrho213xcx4wnkog82sspmt FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: resource_server_resource fk_frsrho213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_server_resource
    ADD CONSTRAINT fk_frsrho213xcx4wnkog82ssrfy FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: resource_server_perm_ticket fk_frsrho213xcx4wnkog83sspmt; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrho213xcx4wnkog83sspmt FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: resource_server_perm_ticket fk_frsrho213xcx4wnkog84sspmt; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrho213xcx4wnkog84sspmt FOREIGN KEY (scope_id) REFERENCES public.resource_server_scope(id);


--
-- Name: associated_policy fk_frsrpas14xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.associated_policy
    ADD CONSTRAINT fk_frsrpas14xcx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: scope_policy fk_frsrpass3xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scope_policy
    ADD CONSTRAINT fk_frsrpass3xcx4wnkog82ssrfy FOREIGN KEY (scope_id) REFERENCES public.resource_server_scope(id);


--
-- Name: resource_server_perm_ticket fk_frsrpo2128cx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrpo2128cx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: resource_server_policy fk_frsrpo213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_server_policy
    ADD CONSTRAINT fk_frsrpo213xcx4wnkog82ssrfy FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: resource_scope fk_frsrpos13xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_scope
    ADD CONSTRAINT fk_frsrpos13xcx4wnkog82ssrfy FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: resource_policy fk_frsrpos53xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_policy
    ADD CONSTRAINT fk_frsrpos53xcx4wnkog82ssrfy FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: resource_policy fk_frsrpp213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_policy
    ADD CONSTRAINT fk_frsrpp213xcx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: resource_scope fk_frsrps213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_scope
    ADD CONSTRAINT fk_frsrps213xcx4wnkog82ssrfy FOREIGN KEY (scope_id) REFERENCES public.resource_server_scope(id);


--
-- Name: resource_server_scope fk_frsrso213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_server_scope
    ADD CONSTRAINT fk_frsrso213xcx4wnkog82ssrfy FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: composite_role fk_gr7thllb9lu8q4vqa4524jjy8; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composite_role
    ADD CONSTRAINT fk_gr7thllb9lu8q4vqa4524jjy8 FOREIGN KEY (child_role) REFERENCES public.keycloak_role(id);


--
-- Name: user_consent_client_scope fk_grntcsnt_clsc_usc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_consent_client_scope
    ADD CONSTRAINT fk_grntcsnt_clsc_usc FOREIGN KEY (user_consent_id) REFERENCES public.user_consent(id);


--
-- Name: user_consent fk_grntcsnt_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_consent
    ADD CONSTRAINT fk_grntcsnt_user FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: group_attribute fk_group_attribute_group; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_attribute
    ADD CONSTRAINT fk_group_attribute_group FOREIGN KEY (group_id) REFERENCES public.keycloak_group(id);


--
-- Name: group_role_mapping fk_group_role_group; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_role_mapping
    ADD CONSTRAINT fk_group_role_group FOREIGN KEY (group_id) REFERENCES public.keycloak_group(id);


--
-- Name: realm_enabled_event_types fk_h846o4h0w8epx5nwedrf5y69j; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.realm_enabled_event_types
    ADD CONSTRAINT fk_h846o4h0w8epx5nwedrf5y69j FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: realm_events_listeners fk_h846o4h0w8epx5nxev9f5y69j; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.realm_events_listeners
    ADD CONSTRAINT fk_h846o4h0w8epx5nxev9f5y69j FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: identity_provider_mapper fk_idpm_realm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.identity_provider_mapper
    ADD CONSTRAINT fk_idpm_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: idp_mapper_config fk_idpmconfig; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.idp_mapper_config
    ADD CONSTRAINT fk_idpmconfig FOREIGN KEY (idp_mapper_id) REFERENCES public.identity_provider_mapper(id);


--
-- Name: web_origins fk_lojpho213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.web_origins
    ADD CONSTRAINT fk_lojpho213xcx4wnkog82ssrfy FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: scope_mapping fk_ouse064plmlr732lxjcn1q5f1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scope_mapping
    ADD CONSTRAINT fk_ouse064plmlr732lxjcn1q5f1 FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: protocol_mapper fk_pcm_realm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.protocol_mapper
    ADD CONSTRAINT fk_pcm_realm FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: credential fk_pfyr0glasqyl0dei3kl69r6v0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.credential
    ADD CONSTRAINT fk_pfyr0glasqyl0dei3kl69r6v0 FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: protocol_mapper_config fk_pmconfig; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.protocol_mapper_config
    ADD CONSTRAINT fk_pmconfig FOREIGN KEY (protocol_mapper_id) REFERENCES public.protocol_mapper(id);


--
-- Name: default_client_scope fk_r_def_cli_scope_realm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.default_client_scope
    ADD CONSTRAINT fk_r_def_cli_scope_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: required_action_provider fk_req_act_realm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.required_action_provider
    ADD CONSTRAINT fk_req_act_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: resource_uris fk_resource_server_uris; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_uris
    ADD CONSTRAINT fk_resource_server_uris FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: role_attribute fk_role_attribute_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_attribute
    ADD CONSTRAINT fk_role_attribute_id FOREIGN KEY (role_id) REFERENCES public.keycloak_role(id);


--
-- Name: realm_supported_locales fk_supported_locales_realm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.realm_supported_locales
    ADD CONSTRAINT fk_supported_locales_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: user_federation_config fk_t13hpu1j94r2ebpekr39x5eu5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_federation_config
    ADD CONSTRAINT fk_t13hpu1j94r2ebpekr39x5eu5 FOREIGN KEY (user_federation_provider_id) REFERENCES public.user_federation_provider(id);


--
-- Name: user_group_membership fk_user_group_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_group_membership
    ADD CONSTRAINT fk_user_group_user FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: policy_config fkdc34197cf864c4e43; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.policy_config
    ADD CONSTRAINT fkdc34197cf864c4e43 FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: identity_provider_config fkdc4897cf864c4e43; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.identity_provider_config
    ADD CONSTRAINT fkdc4897cf864c4e43 FOREIGN KEY (identity_provider_id) REFERENCES public.identity_provider(internal_id);


--
-- PostgreSQL database dump complete
--

