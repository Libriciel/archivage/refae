import $ from 'jquery';
import { AsalaeGlobal } from '../vendor/libriciel/asalae-assets/src/js/asalae.global-top'

export default class RefaeModal {
    static ajax(title, url, table = null, params = {})
    {
        var modal = $(RefaeModal.modalId).data('table', table);
        RefaeModal.load(title, true, RefaeModal.loading, params);
        let ajaxParams = {
            url: url,
            success: function(content, textStatus, jqXHR) {
                $(RefaeModal.modalBodyId).html(content);
                let form = $(RefaeModal.modalBodyId).find('form');
                let modal = $(RefaeModal.modalId);
                if (form.length > 0) {
                    window.onbeforeunload = function(e) {
                        return e.returnValue = "blocked by form";
                    };
                    modal.one('hide.bs.modal', function () {
                        window.onbeforeunload = null;
                        $(RefaeModal.modalId).data('table', null);
                    });
                    RefaeModal.setClosable(false);
                    $(RefaeModal.modalFooterId).find('.accept')
                        .off('.modal-btn')
                        .on('click.modal-btn', function(event) {
                            event.preventDefault();
                            let form = $(RefaeModal.modalBodyId).find('form');
                            form.trigger('beforeSubmit.modalForm', event);
                            setTimeout(
                                function() {
                                    form.trigger('submit.modalForm')
                                        .trigger('afterSubmit.modalForm', event);
                                },
                                0
                            );
                        });
                    setTimeout(() => form.find('input, select, textarea, button').filter(':visible').first().focus(), 400);
                } else {
                    window.onbeforeunload = null;
                    RefaeModal.setClosable(true);
                }
                $(document).trigger('modal-loaded', [modal]);

                if (jqXHR.getResponseHeader('X-Asalae-Redirect')) {
                    window.onbeforeunload = '';
                    window.location.href = jqXHR.getResponseHeader('X-Asalae-Redirect');;
                }
            },
            error: RefaeModal.ajaxError,
            complete: function() {
                $(RefaeModal.modalBodyId).find('.loading-div').remove();
            }
        };
        RefaeModal.pushNewRequest(ajaxParams);
        return $.ajax(ajaxParams);
    }

    static pushNewRequest(ajaxParams)
    {
        RefaeModal.requests.push(ajaxParams);
        if (RefaeModal.requests.length > 3) {
            RefaeModal.requests.shift();
        }
    }

    static getLastRequest()
    {
        if (RefaeModal.requests.length >= 1) {
            return RefaeModal.requests[RefaeModal.requests.length - 1];
        }
        return null;
    }

    static getPreviousRequest()
    {
        if (RefaeModal.requests !== undefined && RefaeModal.requests.length >= 2) {
            return RefaeModal.requests[RefaeModal.requests.length - 2];
        }
        return null;
    }

    static setClosable(closable)
    {
        var opts = $(RefaeModal.modalId).data('bs.modal')?._config || {};
        if (closable) {
            opts.backdrop = 'fixed';
            opts.keyboard = true;
            $(RefaeModal.modalHeaderId).find('button.close').show();
            $(RefaeModal.modalFooterId).find('.btn-form').hide();
            $(RefaeModal.modalFooterId).find('.btn-view').show();
        } else {
            opts.backdrop = 'static';
            opts.keyboard = false;
            $(RefaeModal.modalHeaderId).find('button.close').hide();
            $(RefaeModal.modalFooterId).find('.btn-form').show();
            $(RefaeModal.modalFooterId).find('.btn-view').hide();
        }

    }

    static load(title, closable, content, params = {})
    {
        const modal = $(RefaeModal.modalId);
        const footer = $(RefaeModal.modalFooterId);
        const btnAccept = footer.find('button.accept');
        const btnCancel = footer.find('button.cancel');
        RefaeModal.setClosable(closable);
        $(RefaeModal.modalTitleId).html(title);
        $(RefaeModal.modalBodyId).html(content);
        btnAccept.off('.modal-btn');
        btnCancel.off('.modal-btn');
        if (params.btnAcceptContent) {
            btnAccept.html(params.btnAcceptContent)
        } else {
            btnAccept.empty()
                .append('<i class="fa fa-floppy-o" aria-hidden="true"></i>')
                .append('<span>Enregistrer</span>');
        }
        if (params.btnCancelContent) {
            btnCancel.html(params.btnCancelContent)
        } else {
            btnCancel.empty()
                .append('<i class="fa fa-times-circle-o" aria-hidden="true"></i>')
                .append('<span>Annuler</span>');
        }
        if (params.btnCancelUrl) {
            btnCancel.on('click.modal-btn', function(event) {
                event.preventDefault();
                AsalaeGlobal.addWaitingAjaxResponse(modal);
                modal.find('.modal-footer button').disable();
                $.ajax({
                    url: params.btnCancelUrl,
                    method: 'delete',
                    data: $(RefaeModal.modalBodyId).find('form').serialize(),
                    error: function() {
                        alert("Erreur lors de l'annulation");
                    },
                    complete: function() {
                        RefaeModal.hide();
                        AsalaeGlobal.removeWaitingAjaxResponse(modal);
                        modal.find('.modal-footer button').enable();
                    }
                })
            });
        }
        RefaeModal.show(closable);
    }

    static show(closable = true)
    {
        if (closable) {
            $(RefaeModal.modalId).modal('show');
        } else {
            $(RefaeModal.modalId).modal({backdrop: 'static', keyboard: false}, 'show');
        }
        $(RefaeModal.modalId).one(
            'hidden.bs.modal',
            () => setTimeout(() => $(RefaeModal.modalId).off('.bs.modal').removeData('bs.modal'), 0)
        );
    }

    static hide()
    {
        $(RefaeModal.modalId).modal('hide');
    }

    static appendRetryButtons()
    {
        let modalBody = $(RefaeModal.modalId).find('.modal-body');
        modalBody.find('.refae-modal-retry').remove();
        let btnDiv = $('<div class="modal-padding refae-modal-retry">');
        if (RefaeModal.requests.length > 1) {
            let buttonPrevious = $('<button class="btn btn-default">')
                .append('<i class="fa fa-arrow-left fa-space" aria-hidden="true">')
                .append("Revenir à la requête précédente")
                .on('click', () => {
                    btnDiv.remove();
                    $.ajax(RefaeModal.getPreviousRequest());
                    RefaeModal.requests.pop();
                })
            ;
            btnDiv.append(buttonPrevious);
        }
        let buttonRetry = $('<button class="btn btn-default">')
            .append('<i class="fa fa-refresh fa-space" aria-hidden="true">')
            .append("Renvoyer la dernière requête")
            .on('click', () => {
                btnDiv.remove();
                $.ajax(RefaeModal.getLastRequest());
            })
        ;
        btnDiv.append(buttonRetry);
        modalBody.append(btnDiv);
    }

    static ajaxError(jqXHR, textStatus, errorThrown)
    {
        var modalBody = $(RefaeModal.modalId).find('.modal-body');
        if (jqXHR.status === 401) {
            modalBody.text("401 - Vous devez être connecté pour accéder à ce contenu");
        } else if (jqXHR.status === 403) {
            modalBody.text("403 - Vous n'avez pas accès à ce contenu");
        } else if (/<html.*>([\s\S]+)<\/html>/.test(jqXHR.responseText)) {
            let body = /<body.*>([\s\S]+)<\/body>/.exec(jqXHR.responseText);
            let errorDiv = $('<div class="modal-fatal-error">').html(body[1]);
            modalBody.html(errorDiv);
        } else if (
            jqXHR.responseText?.indexOf('<script> Sfdump = window.Sfdump') !== -1
            || jqXHR.getResponseHeader('Rendered-Exception')
        ) {
            let errorDiv = $('<div class="modal-debug">').html(jqXHR.responseText);
            modalBody.html(errorDiv);
        } else {
            modalBody.html(errorThrown);
        }
        RefaeModal.appendRetryButtons();
    }
}
RefaeModal.modalId = '#main-modal';
RefaeModal.modalHeaderId = '#main-modal-header';
RefaeModal.modalTitleId = '#main-modal-title';
RefaeModal.modalBodyId = '#main-modal-body';
RefaeModal.modalFooterId = '#main-modal-footer';
RefaeModal.templateConfigureTableModal = '#template-configure-table';
RefaeModal.loading = '<div class="text-center loading-div">'
    + '<i aria-hidden="true" class="fa fa-4x fa-spinner fa-spin animated"></i>'
    + '</div>';
RefaeModal.requests = [];

$(document).on('click.configure-table', '[data-toggle="configure-table"]', function() {
    var table = $($(this).attr('data-target'));
    var generator = table.data('tableGenerator');
    var content = $(RefaeModal.templateConfigureTableModal).html();
    RefaeModal.load("Configuration du tableau de résultats", false, content);
    generator.max_per_page = generator._memory.max_per_page;
    generator.insertConfiguration(RefaeModal.modalId);
    $(RefaeModal.modalBodyId).find('button.delete-cookies').on('click.configure-table', function() {
        if (confirm("Cette action va supprimer vos préférences de façon permanente. Voulez-vous continuer ?")) {
            generator.reset();
            generator.fillDropZone();
        }
    });
    $(RefaeModal.modalFooterId).find('button.accept')
        .off('.configure-table')
        .one('click.configure-table', function() {
            RefaeModal.hide();
            $(this).off('.configure-table');
        });
});

$(document).on('click.btn-ajax-modal', '[data-modal="ajax"][data-url][data-title]', function() {
    var title = $(this).attr('data-title');
    var url = $(this).attr('data-url');
    var table = $(this).attr('data-table');
    var params = {};
    if ($(this).attr('data-btn-accept-content')) {
        params.btnAcceptContent = $(this).attr('data-btn-accept-content');
    }
    if ($(this).attr('data-btn-cancel-content')) {
        params.btnCancelContent = $(this).attr('data-btn-cancel-content');
    }
    if ($(this).attr('data-btn-cancel-url')) {
        params.btnCancelUrl = $(this).attr('data-btn-cancel-url');
    }
    var m;
    do {
        m = url.match(/%7B([0-9]+)%7D/);
        if (m) {
            url = url.replaceAll('%7B' + m[1] + '%7D', '{' + m[1] + '}');
        }
    } while (m);

    RefaeModal.requests = [];
    RefaeModal.ajax(title, url, table, params);
});

$(document).on('click.btn-ajax-delete', '[data-delete="ajax"][data-url]', function() {
    var tr = $(this).closest('tr');
    var url = $(this).attr('data-url');
    var table = $($(this).attr('data-table'));
    var generator = table.data('tableGenerator');
    var confirmText = $(this).attr('data-confirm');
    if (confirmText && !confirm(confirmText)) {
        return;
    }
    $.ajax(
        {
            url: url,
            method: 'DELETE',
            contentType: 'application/json',
            success: function (content, textStatus, jqXHR) {
                if (generator) {
                    generator.removeDataId(tr.attr('data-id'));
                    tr.fadeOut(
                        400,
                        function () {
                            $(this).remove();
                            generator.decrementPaginator(table.closest('section').find('.pagination-counters'));
                        }
                    );
                    if (jqXHR.getResponseHeader('X-Asalae-PreviousVersionEntity')) {
                        generator.data.push(content);
                        generator.generateAll();
                    }
                    generator.table.trigger('modal_form_delete_success', [content, jqXHR]);
                }
                if (jqXHR.getResponseHeader('X-Asalae-Redirect')) {
                    window.onbeforeunload = '';
                    window.location.href = jqXHR.getResponseHeader('X-Asalae-Redirect');;
                }
            }
        }
    );
});

$(document).on('click.btn-ajax-update', '[data-update="ajax"][data-url]', function() {
    var tr = $(this).closest('tr');
    var url = $(this).attr('data-url');
    var table = $($(this).attr('data-table'));
    var generator = table.data('tableGenerator');
    var confirmText = $(this).attr('data-confirm');
    if (confirmText && !confirm(confirmText)) {
        return;
    }
    $.ajax(
        {
            url: url,
            method: 'UPDATE',
            contentType: 'application/json',
            success: function(content, textStatus, jqXHR) {
                if (generator) {
                    generator.replaceDataId(tr.attr('data-id'), content);
                    generator.generateAll();
                }
                if (jqXHR.getResponseHeader('X-Asalae-Redirect')) {
                    window.onbeforeunload = '';
                    window.location.href = jqXHR.getResponseHeader('X-Asalae-Redirect');;
                }
            }
        }
    );
});

$(document).on('configuration_changed', 'table', function() {
    const table = $(this);
    const generator = table.data('tableGenerator');
    generator._memory.max_per_page = generator.max_per_page;
    const exported = generator.exportMemory();
    $.ajax(
        {
            url: table.attr('data-save-url'),
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                id: table.attr('id'),
                activeLine: exported.activeLine,
                columns: exported.columns,
                favorites: exported.favorites,
                max_per_page: exported.max_per_page
            })
        }
    );
});

$(document).on('submit.modalForm', '.modal form', function(event) {
    var form = $(this);
    if (form.length && !this.checkValidity()) {
        event.preventDefault();
        this.reportValidity();
        return;
    }

    if (event.isDefaultPrevented()) {
        return false;
    }
    event.preventDefault();

    var modal = form.closest('.modal');
    AsalaeGlobal.addWaitingAjaxResponse(modal);
    modal.find('.modal-footer button').disable();

    var table = $(modal.data('table'));
    var generator = table.data('tableGenerator');

    let ajaxParams = {
        url: form.attr('action'),
        method: 'POST',
        data: form.serialize(),
        success: function (content, textStatus, jqXHR) {
            if (jqXHR.getResponseHeader('X-Asalae-Redirect')) {
                window.onbeforeunload = '';
                window.location.href = jqXHR.getResponseHeader('X-Asalae-Redirect');
                return;
            }
            if (
                !jqXHR.getResponseHeader('X-Asalae-Success')
                || jqXHR.getResponseHeader('X-Asalae-Success') !== 'true'
            ) {
                modal.find('.modal-body').html(content);
                if (modal.find('form').length === 0) {
                    window.onbeforeunload = null;
                    RefaeModal.setClosable(true);
                }
            } else {
                if (content && content.id && generator) {
                    let exists = generator.getDataId(content.id);
                    if (exists) {
                        generator.replaceDataId(content.id, content);
                    } else {
                        generator.data.push(content);
                        generator.incrementPaginator(table.closest('section').find('.pagination-counters'));
                    }
                    generator._prevMemory = JSON.stringify(generator._memory);
                    generator._memory.activeLine = content.id;
                    generator.generateAll();
                    if (JSON.stringify(generator._memory) !== generator._prevMemory) {
                        generator.table.trigger('configuration_changed');
                    }
                    $('.main-content').get(0).scrollTop = $('tr[data-id="' + content.id +'"]').get(0).offsetTop - 200;
                    generator.table.trigger('modal_form_success', [content, jqXHR]);
                }
                modal.modal('hide');
            }
            if (jqXHR.getResponseHeader('X-Asalae-Step')) {
                let params = jqXHR.getResponseHeader('X-Asalae-Modal-Params');
                RefaeModal.hide();
                modal.one('hidden.bs.modal.submitForm', function () {
                    setTimeout(() => RefaeModal.ajax(
                        JSON.parse(jqXHR.getResponseHeader('X-Asalae-Step-Title')),
                        jqXHR.getResponseHeader('X-Asalae-Step'),
                        table,
                        params ? JSON.parse(params) : {}
                    ), 10);
                });
            } else if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                modal.one('hidden.bs.modal.submitForm', () => RefaeModal.requests = []);
            }
            $(document).trigger('modal-loaded', [modal]);
        },
        error: RefaeModal.ajaxError,
        complete: function() {
            AsalaeGlobal.removeWaitingAjaxResponse(modal);
            modal.find('.modal-footer button').enable();
        }
    };
    RefaeModal.pushNewRequest(ajaxParams);
    var xhr = $.ajax(ajaxParams);
    modal.off('.submitForm')
        .one('hide.bs.modal.submitForm', function () {
            xhr.abort();
        });
});

$(document).on('modal-loaded', () => {
    if (RefaeModal.debug) {
        RefaeModal.appendRetryButtons();
    }
});
