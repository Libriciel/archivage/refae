/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import $ from 'jquery';
const jQuery = $;
window.$ = window.jQuery = $;

import 'popper.js';
import 'bootstrap';
import './styles/app.css';

import 'select2';
import 'jquery-ui/ui/widget';
import 'jquery-ui/ui/widgets/draggable';
import 'jquery-ui/ui/widgets/datepicker';
import 'jquery-ui/ui/widgets/sortable';
import 'jquery-ui/ui/widgets/droppable';
import 'jquery-ui/ui/widgets/resizable';
import 'jquery-ui/ui/widgets/tabs';
// deprecated jquery-ui function
$.fn.extend({
    disableSelection: ( function() {
        var eventType = "onselectstart" in document.createElement( "div" ) ?
            "selectstart" :
            "mousedown";

        return function() {
            return this.on( eventType + ".ui-disableSelection", function( event ) {
                event.preventDefault();
            } );
        };
    } )(),

    enableSelection: function() {
        return this.off( ".ui-disableSelection" );
    }
});
import '../vendor/libriciel/asalae-assets/src/js/jquery-serialize-object.min';

import 'tinymce';
import {v4 as uuidv4} from 'uuid';
window.uuid = {v4: uuidv4};
import Cookies from 'js-cookie';
window.Cookies = Cookies;
import '../vendor/libriciel/asalae-assets/src/js/js.cookie.min';
import '../vendor/libriciel/asalae-assets/src/js/asalae.extended_jquery.js';
import '../vendor/libriciel/asalae-assets/src/js/jquery.orgchart';
import '../vendor/libriciel/asalae-assets/src/js/sprintf';
import moment from 'moment';
window.moment = moment;
// import '../vendor/libriciel/asalae-assets/src/js/Chart'; // missing "moment.js"
import { AsalaeGlobal, AsalaeLoading } from '../vendor/libriciel/asalae-assets/src/js/asalae.global-top'
window.AsalaeGlobal = AsalaeGlobal;
window.AsalaeLoading = AsalaeLoading;
import { TableGenerator, TableHelper, TableGenericAction } from '../vendor/libriciel/asalae-assets/src/js/asalae.table-generator'
window.TableGenerator = TableGenerator;
window.TableGenericAction = TableGenericAction;
window.TableHelper = TableHelper;

$(document).on('click.toggle-side', '.ls-sidebar-toggle a', function() {
    $('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed');
    $('#body-row').toggleClass('sidebar-is-collapsed');
});
$(document).on(
    'click.follow-anchor',
    'a.with-anchor[data-original-href][data-toggle-href]',
    function (event) {
        event.preventDefault(); // pour être sur, est déjà annulé par dropdown
        let a = $(this);
        if (a.data('active-event')) {
            return;
        }
        if (!$(event.target).is('span.anchor')) {
            event.preventDefault();
            a.attr('href', a.attr('data-toggle-href')).data('active-event', true);
            setTimeout(() => a.trigger('click'), 0); // bien plus rapide en détaché
            setTimeout(() => a.attr('href', a.attr('data-original-href')).data('active-event', false), 1);
        } else {
            window.location.href = a.attr('href');
        }
    }
);
import './refae.sidebar';
var observeDOM = (function(){
    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

    return function( obj, callback ){
        if( !obj || obj.nodeType !== 1 ) return;

        if( MutationObserver ){
            // define a new observer
            var mutationObserver = new MutationObserver(callback)

            // have the observer observe for changes in children
            mutationObserver.observe( obj, { childList:true, subtree:true })
            return mutationObserver
        }

        // browser support fallback
        else if( window.addEventListener ){
            obj.addEventListener('DOMNodeInserted', callback, false)
            obj.addEventListener('DOMNodeRemoved', callback, false)
        }
    }
})();

var cooldown;
observeDOM(document.documentElement, function(){
    if (cooldown) {
        return;
    }
    cooldown = true;
    setTimeout(function() {
        cooldown = false;
    }, 500);
    $('[data-toggle="tooltip"]').tooltip();
});
$(function() {
    $('[data-toggle="tooltip"]').tooltip();
});

import RefaeModal from './refae.modal';
window.RefaeModal = RefaeModal;

import './refae.filter';

// Corrige un bug lors du history back (les scripts ne se lance pas)
window.onunload = () => null;

import './refae.global';

$(document).on('modal-loaded', function(event, modal) {
    const STRLEN_BETWEEN_ELLIPSIS = 10;
    modal.find('ul[data-activity-outcome] > li').each(function() {
        var text = $(this).text();
        // extrait les expressions sous la forme: '{author:test} blabla {action:...}'
        var regex = new RegExp(
            '^\\{' + ['author', 'action', 'subject', 'date', 'time'].join(':([\\s\\S]*)}([\\s\\S]*)\\{')
                + ':([\\s\\S]*)}([\\s\\S]*)$'
        );
        var match = text.match(regex);
        var formatter = function(classname, text) {
            var span = $('<span>').addClass(classname);
            if (text.length > STRLEN_BETWEEN_ELLIPSIS * 2 + 3) {
                span.text(text.substr(0, STRLEN_BETWEEN_ELLIPSIS) + '...' + text.substr(text.length - STRLEN_BETWEEN_ELLIPSIS))
                    .attr('title', text)
                    .tooltip();
            } else {
                span.text(text);
            }
            return span;
        };
        if (match && match.length) {
            let author = $('<span class="author">').text(match[1]);
            let action = $('<span class="action">').text(match[3]);
            let subject = $('<span class="subject">');
            let subjectMatch1 = match[5].match(/^"(.*)" \("(.*)" -> "(.*)"\)$/);
            let subjectMatch2 = match[5].match(/^"(.*)" \("(.*)"\)$/);
            if (subjectMatch2 && subjectMatch2.length) {
                subject.append($('<span class="field">').text(subjectMatch2[1]));
                subject.append(' (');
                if (subjectMatch1 && subjectMatch1.length) {
                    subject.append(formatter('content-before', subjectMatch1[2]));
                    subject.append(' -> ');
                    subject.append(formatter('content-after', subjectMatch1[3]));
                } else {
                    subject.append(formatter('content-before', subjectMatch2[2]));
                }
                subject.append(')');
            } else {
                subject.append($('<span class="field">').text(match[5]));
            }
            let momentdate = moment(
                match[7] + ' ' + match[9].replace('h', ':'),
                'DD/MM/YYYY HH:mm'
            );
            let datetime = $('<span class="date">')
                .text(momentdate.fromNow())
                .prepend(' ')
                .attr('title', match[6] + match[7] + match[8] + match[9])
                .tooltip();
            $(this).empty();
            $(this).append(author);
            $(this).append(match[2]);
            $(this).append(action);
            $(this).append(match[4]);
            $(this).append(subject);
            $(this).append(datetime);
        }
    });
});

$(function() {
    $('[data-link-download-csv]').each(function() {
        var timestamp = moment().format('YYYY-MM-DD_HHmmss');
        var download = $(this).attr('data-link-download-csv').format(timestamp);
        $(this).attr('download', download);
    });
});

window.base64_encode = function(str)
{
    return btoa(String.fromCodePoint(...new TextEncoder().encode(str)));
}
window.base64_decode = function(base64)
{
    return new TextDecoder().decode(Uint8Array.from(atob(base64), (m) => m.codePointAt(0)));
}

import { bstr, buf, str } from "crc-32";
window.crc32 = {bstr: bstr, buf: buf, str: str};

import ChunkyUploader from './chunkyUploader';
window.ChunkyUploader = ChunkyUploader;

import 'jsrender';
import { Form } from '../vendor/libriciel/asalae-assets/src/js/asalae.formhelper';
window.Form = Form;

let applySelect2 = function(scope) {
    $(scope).find('[data-s2]').each(function() {
        let addClassAndTitle = function (data, container)
        {
            if (data.element) {
                var element = $(data.element);
                $(container).addClass(element.attr('data-s2-class'));
                if (element.attr('data-s2-title')) {
                    $(container).prop('title', element.attr('data-s2-title'));
                }
                if ($(data.element).attr('locked')) {
                    $(container).addClass('locked-tag');
                }
            }
            return data.text;
        };
        let options = $(this).attr('data-s2-options');
        let defaultOptions = {
            templateResult: addClassAndTitle,
            templateSelection: addClassAndTitle
        }
        if (options) {
            options = JSON.parse(options);
            for (let key in options) {
                if (options[key].indexOf('.') === -1) {
                    continue;
                }
                let [classname, attribute] = options[key].split('.');
                if (window[classname] && window[classname][attribute]) {
                    options[key] = window[classname][attribute];
                }
            }
        }

        AsalaeGlobal.select2($(this), Object.assign(defaultOptions, options));
    });
};
$(function() {
    applySelect2(document);
});
$(document).on('modal-loaded', function (event, modal) {
    applySelect2(modal);
});
$(document).on('change', 'table[data-table-uid]', function (event, modal) {
    $('.btn-filter').off('click.filter.table');
});
$(document).on('hide.bs.dropdown', 'th[data-fieldname]', function (event) {
    if (
        event.clickEvent?.target
        && $(event.clickEvent?.target).closest(event.target).length === 1
    ) {
        // évite la fermeture si l'évenement à lieu à l'interieur du formulaire de filtres
        event.preventDefault()
    }
});

$(document).on('click.btn-delete-filter', '[data-delete="filter"][data-url]', function() {
    var tr = $(this).closest('tr');
    var url = $(this).attr('data-url');
    var confirmText = $(this).attr('data-confirm');
    if (confirmText && !confirm(confirmText)) {
        return;
    }
    $.ajax(
        {
            url: url,
            method: 'DELETE',
            contentType: 'application/json',
            success: function(content) {
                tr.fadeOut(
                    400,
                    function () {
                        $(this).remove();
                    }
                );
            }
        }
    );
});

$(document).on('click.btn-action-download', '[data-ajax-download][data-url]', function() {
    var tr = $(this).closest('tr');
    var url = $(this).attr('data-url');
    var confirmText = $(this).attr('data-confirm');
    if (confirmText && !confirm(confirmText)) {
        return;
    }
    window.location = url;
});

$(document).on('click.btn-ajax-action', '[data-ajax-method][data-url]', function() {
    $(this).tooltip('dispose');
    var tr = $(this).closest('tr');
    var url = $(this).attr('data-url');
    var confirmText = $(this).attr('data-confirm');
    if (confirmText && !confirm(confirmText)) {
        return;
    }
    var tableGenerator = tr.closest('table').data('tableGenerator');
    var buttons = tr.find('button:not(:disabled)');
    buttons.disable();

    let success = $(this).attr('data-ajax-success');
    success = success ? window[success] : TableGenericAction.afterEdit(tableGenerator);

    $.ajax(
        {
            url: url,
            method: $(this).attr('data-ajax-method'),
            contentType: 'application/json',
            success: success,
            complete: () => buttons.enable()
        }
    );
});

import './refae.tabs';
import RefaeLabel from './refae.label';
window.RefaeLabel = RefaeLabel;
import RefaeSearch from './refae.search';
window.RefaeSearch = RefaeSearch;
import RefaeMce from './refae.tinymce';
window.RefaeMce = RefaeMce;

import Chart from 'chart.js/auto';
window.Chart = Chart;

import RefaeSession from './refae.session';
window.RefaeSession = RefaeSession;

import RefaeNotify from './refae.notify';
window.RefaeNotify = RefaeNotify;

$(document).on('click.btn-slugify', '[data-slugify]', function() {
    let target = $($(this).attr('data-slugify'));
    let value = target.val();
    value = AsalaeGlobal.removeDiacritics(value);
    value = value.trim();
    value = value.replaceAll(' ', '_');
    value = value.toUpperCase();
    target.val(value);
});

/**
 * Améliore l'apparence d'un tableau en cas d'éclatement
 */
AsalaeGlobal.resizeTimeout = null;
$(window).on('resize.smartTdSize',
    function () {
        clearTimeout(AsalaeGlobal.resizeTimeout);
        // Limite grandement les ressources nécéssaire
        AsalaeGlobal.resizeTimeout = setTimeout(
            function () {
                AsalaeGlobal.smartTdSize();
            },
            100
        );
    }
).trigger('resize.smartTdSize');

window.RefaeTable = {
    insertAnchor: function (value) {
        let m = value?.match(/\{url:([^}]+)}/);
        if (m) {
            let anchor = '<a href="' + m[1] + '" target="_blank">' + m[1] + '</a>';
            value = value.replace(m[0], anchor);
        }
        m = value?.match(/\{user:([^}]+)}/);
        if (m) {
            let user = ' - <b>' + m[1] + '</b>';
            value = value.replace(m[0], user);
        }
        return value;
    }
};
