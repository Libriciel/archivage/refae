import $ from "jquery";

export default class RefaeSearch
{
    constructor(form) {
        this.form = form;
        this.dropdown = form.siblings('.dropdown-menu');
        let url = form.attr('action');
        let input = form.find('[data-search-engine]');
        let keyupTimeout = null;

        form.on('submit.refae-search', (event) => {
            event.preventDefault();
            if (keyupTimeout) {
                clearTimeout(keyupTimeout);
            }
            keyupTimeout = null;
            if (input.val().length < 3) {
                return;
            }
            this.search(url, input);
        });
        $(document).on('click.refae-search', (event) => {
            if ($(event.target).closest(form.parent()).length !== 1) {
                this.dropdown.dropdown('hide');
            }
        });
        $(document).on('hide.bs.dropdown', this.form, () => {
            this.form.removeClass('open');
        });
        $(document).on('show.bs.dropdown', this.form, () => {
            setTimeout(() => {
                this.form.addClass('open');
                this.dropdown.removeAttr('style');
            }, 0);
        });
        form.on('keyup.refae-search', (event) => {
            if (keyupTimeout) {
                clearTimeout(keyupTimeout);
            }
            keyupTimeout = setTimeout(() => form.trigger('submit.refae-search'), 600);
        });
    }

    search(url, input)
    {
        $.ajax(
            {
                url: url,
                method: 'POST',
                data: this.form.serialize(),
                success: (content) => {
                    this.dropdown.empty();
                    if (content?.hits?.length) {
                        this.dropdown.append(this.resultToList(content));
                    } else {
                        this.dropdown.append('<div class="search-bar-results">Aucun résultat</div>');
                    }
                    this.form.dropdown('show');
                },
                complete: () => {

                }
            }
        );
    }

    resultToList(content)
    {
        let ul = $('<ul class="search-bar-results nav navbar"></ul>');
        for (let i = 0; i < content?.hits.length; i++) {
            let li = $('<li></li>');
            let table = $('<table>');
            let tbody = $('<tbody>');
            let first = true;

            for (let key in content.hits[i]._matchesPosition) {
                let tr = this.contentLineToTr(content.hits[i], i, key, content.translation);
                if (first) {
                    first = false;
                    tr.prepend(
                        $('<th>').attr('rowspan', Object.keys(content.hits[i]._matchesPosition).length)
                            .text(content.hits[i].identifier)
                    );
                }
                tbody.append(tr);
                tr.attr('onclick', 'RefaeSearch.redirectToIdentifier("' + content.hits[i].identifier + '")');
            }
            table.append(tbody);
            li.append(table);
            ul.append(li);
        }
        return ul;
    }

    contentLineToTr(contentLine, i, matchingField, translation)
    {
        let tr = $('<tr tabindex="0" role="button">')
            .attr('data-id', contentLine.id)
            .attr('data-index', i);
        let fieldname = translation && translation[matchingField] ? translation[matchingField] : matchingField;
        tr.append($('<th>').text(fieldname));
        let current = contentLine[matchingField];
        let parts = [];
        let pointer = 0;
        let td = $('<td>');
        for (let j = 0; j < contentLine._matchesPosition[matchingField].length; j++) {
            if (Array.isArray(current)) {
                td.append($('<span>').text(current.join(', ')).html());
                break;
            }
            let params = contentLine._matchesPosition[matchingField][j];
            if (pointer < params.start) {
                let text = current.substr(pointer, params.start - pointer);
                td.append($('<span>').text(text).html());
                pointer = params.start;
            }
            let text = current.substr(params.start, params.length);
            td.append('<b>' + $('<span>').text(text).html() + '</b>');
            pointer = params.start + params.length;
        }
        if (!Array.isArray(current)) {
            let text = current.substr(pointer);
            td.append($('<span>').text(text).html());
        }
        tr.append(td);
        return tr;
    }

    static redirectToIdentifier(identifier)
    {
        var currentUrl = '' + window.location;
        let m = currentUrl.match(/([?&])identifier\[0]=([^&]+)(&)?/);
        if (m) {
            currentUrl = m[3] === undefined ? currentUrl.replace(m[0], '') : currentUrl.replace(m[0], m[1]);
        }
        currentUrl = currentUrl.replace(/#[^&?]*/, '');
        currentUrl += currentUrl.indexOf('?') !== -1 ? '&' : '?';
        window.location = currentUrl + 'identifier[0]=' + encodeURI(identifier);
    }
}

$(function() {
    $('[data-search-engine]').each(
        function () {
            var url = $(this).attr('data-search-engine');
            var submit = $('<button type="submit" class="submit-search-engine">')
                .append($('<span class="sr-only">').text("Rechercher"))
                .append($('<i class="fa fa-search" aria-hidden="true">'))
            ;
            submit.insertAfter($(this));
            var search = new RefaeSearch($(this).closest('form'));
        }
    );
});
