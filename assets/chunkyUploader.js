import $ from 'jquery';
import {v4 as uuidv4} from 'uuid';
import { bstr as crc32bstr } from "crc-32";
import moment from 'moment';

class ChunkyUploader {
    static MAX_ERRORS_ON_THE_SAME_CHUNK = 3;
    static MAX_SIMULTANEOUS_REQUESTS = 3;
    static CHUNK_SIZE = 2097152; // 2Mo
    static translations = {
        action: 'Action',
        confirm_delete: "Êtes-vous sûr de vouloir supprimer ce fichier ?",
        delete: "supprimer",
        dropbox_text: "Glissez-déposez vos fichiers ici",
        file: 'Fichier',
        pause: "pause",
        resume: "reprendre",
        select: "Sélectionner",
        select_all: "Sélectionner toutes les lignes",
        select_file: "Sélectionner ",
        size: ['octets', 'ko', 'Mo', 'Go', 'To', 'Po'],
        uploading_validation: "Un upload est en cours"
    };

    constructor(input, sizeLimit = null) {
        this.input = input;
        this.url = input.attr('data-uploader');
        this.queue = [];
        this.sizeLimit = sizeLimit;
        this.uploading = false;
    }

    enqueue(chunkyFile)
    {
        this.queue.push(chunkyFile);
    }

    unqueue(chunkyFile)
    {
        for (let i = 0; i < this.queue.length; i++) {
            if (this.queue.uuid === chunkyFile.uuid) {
                this.queue.splice(i, 1);
                break;
            }
        }
    }

    init()
    {
        this.currentRequestCount = 0;
        this.stopped = false;
        this.interval = 0;
        this.currentFile = null;
        this.uploading = true;
    }

    run()
    {
        this.init();
        this.currentFile = this.queue.shift();
        this.input.trigger('upload-started.chunky');
        if (this.currentFile?.paused) {
            let pauseDuration = performance.now() - this.currentFile.pauseTime;
            this.currentFile.beginTime = this.currentFile.beginTime + pauseDuration;
            this.currentFile.progresses = this.currentFile.progresses.map((v) => v + pauseDuration);
        } else {
            this.currentFile.beginTime = performance.now();
        }

        this.interval = setInterval(() => this.updateProgresBar(), 500);
        this.sendNextChunk();
    }

    stop()
    {
        this.stopped = true;
        clearInterval(this.interval);
    }

    sendNextChunk()
    {
        let chunkyFile = this.currentFile;
        if (this.stopped) {
            return;
        }
        if (chunkyFile.succededChunks.length === chunkyFile.totalChunk) {
            this.stop();
            if (this.queue.length) {
                this.run();
            } else {
                this.uploading = false;
                this.input.trigger('upload-finished.chunky');
            }
            return;
        }
        let currentChunk = chunkyFile.remainingChunks.shift();
        if (currentChunk === undefined) {
            if (chunkyFile.failedChunks.length) {
                chunkyFile.remainingChunks = chunkyFile.failedChunks;
                currentChunk = chunkyFile.remainingChunks.shift();
            } else {
                return; // il reste des requetes en cours
            }
        }

        let chunkContent = chunkyFile.file.slice(
            currentChunk * ChunkyUploader.CHUNK_SIZE,
            (currentChunk + 1) * ChunkyUploader.CHUNK_SIZE
        );
        let reader = new FileReader();
        reader.onload = () => {
            let formData = new FormData();
            formData.append('filename', chunkyFile.file.name);
            formData.append('uuid', chunkyFile.uuid);
            formData.append('size', chunkyFile.file.size);
            formData.append('chunk[i]', currentChunk + 1);
            formData.append('chunk[total]', chunkyFile.totalChunk);
            formData.append('chunk[size]', reader.result.length);
            formData.append('chunk[hash]', (crc32bstr(reader.result)>>>0).toString(16));
            formData.append('file', chunkContent, 'chunk-'+currentChunk);
            let len = ('' + chunkyFile.totalChunk).length;

            this.currentRequestCount++;
            $.ajax(
                {
                    url: ("%s?f=%s&c=%0"+len+"d..%d").format(
                        this.url,
                        encodeURIComponent(chunkyFile.file.name),
                        currentChunk + 1,
                        chunkyFile.totalChunk
                    ),
                    method: 'POST',
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: (content) => {
                        if (content.indexOf('partial') !== -1 || content.indexOf('done') !== -1) {
                            this.chunkSuccess(currentChunk);
                        } else {
                            this.chunkError(currentChunk);
                        }
                    },
                    error: () => {
                        this.chunkError(currentChunk);
                    },
                    complete: () => {
                        this.currentRequestCount--;
                        this.sendNextChunk(chunkyFile);
                    }
                }
            );
            if (this.currentRequestCount < ChunkyUploader.MAX_SIMULTANEOUS_REQUESTS) {
                this.sendNextChunk(chunkyFile);
            }
        };
        reader.readAsBinaryString(chunkContent);
    }

    chunkSuccess(currentChunk)
    {
        let chunkyFile = this.currentFile;
        chunkyFile.succededChunks.push(currentChunk);
        chunkyFile.progresses.push(performance.now());
        let percent = Math.ceil(chunkyFile.progresses.length / chunkyFile.totalChunk * 100);
        chunkyFile.tr.find('.progress-bar').css('width', percent + '%');
        if (percent === 100) {
            chunkyFile.tr.find('.progress-bar').addClass('progress-bar-success');
            chunkyFile.tr.find('.uploading-message').remove();
            chunkyFile.tr.find('.action-pause').remove();
            chunkyFile.tr.find('.action-resume').remove();
        }
    }

    chunkError(currentChunk)
    {
        let chunkyFile = this.currentFile;
        chunkyFile.failedChunks.push(currentChunk);
        let chunkErrorCount = chunkyFile.failedChunks.filter((v) => v === currentChunk).length;
        if (chunkErrorCount >= ChunkyUploader.MAX_ERRORS_ON_THE_SAME_CHUNK) {
            this.stop();
            chunkyFile.tr.find('.progress-bar').addClass('progress-bar-danger');
        }
    }

    updateProgresBar()
    {
        let chunkyFile = this.currentFile;
        if (chunkyFile.progresses.length === 0) {
            return;
        }
        let statusMessage = chunkyFile.tr.find('.uploading-message').css('opacity', 1);
        let newTime = performance.now();
        let difftime = newTime - chunkyFile.beginTime;
        let seconds = difftime / 1000;
        chunkyFile.progresses.filter((v) => v + 10000 > newTime);
        let secs = chunkyFile.progresses.length ? (newTime - chunkyFile.progresses[0]) / 1000 : 0;
        var rate = chunkyFile.progresses.length / secs * ChunkyUploader.CHUNK_SIZE;
        statusMessage.find('.duration').text(moment().startOf('day').seconds(seconds).format('HH:mm:ss'));
        let transferred = chunkyFile.succededChunks.length * ChunkyUploader.CHUNK_SIZE;
        let toUpload = chunkyFile.file.size - transferred;
        var estimation = toUpload / (transferred / seconds);
        var date = moment().startOf('day').seconds(Math.round(estimation));
        var s = ChunkyUploader.translations.size;
        var e = Math.floor(Math.log(rate) / Math.log(1024));

        statusMessage.find('.speed').text(
            Number.isFinite(e)
                ? (rate / Math.pow(1024, e)).toFixed(2) + " " + s[e] + "/s"
                : '0/s'
        );
        if (toUpload >= 0) {
            if (date.hour()) {
                statusMessage.find('.estimated').text(date.hour()+'h '+date.minute()+'m');
            } else if (date.minute()) {
                statusMessage.find('.estimated').text(date.minute()+'m '+date.second()+'s');
            } else if (date.second()) {
                statusMessage.find('.estimated').text(date.second()+'s');
            } else {
                statusMessage.remove();
            }
        } else {
            statusMessage.remove();
        }
    }

    generateDropbox(input)
    {
        var dropbox = $('<div class="dropbox dropbox-small">').attr('id', input.attr('id') + '-dropbox');
        dropbox.append($('<i class="fa fa-upload fa-3x text-primary">'));
        dropbox.append($('<p>').text(ChunkyUploader.translations.dropbox_text));
        var button = $('<button type="button" class="btn btn-success">');
        button.append($('<i class="fa fa-folder-open fa-space" aria-hidden="true">'));
        button.append('Parcourir...');
        button.on('click.chunky', () => input.trigger('click.chunky'));
        dropbox.append(button);
        input.after(dropbox);
        input.css({position: 'absolute', height: 0}).prop('tabindex', -1);

        dropbox
            .on('dragover.chunky', (event) => {
                event.preventDefault();
                dropbox.addClass('dragover')
            })
            .on('dragleave.chunky', () => dropbox.removeClass('dragover'))
            .on('drop.chunky', (event) => {
                event.preventDefault();
                event.stopPropagation();
                dropbox.removeClass('dragover');
                input.get(0).files = event.originalEvent.dataTransfer.files;
                input.trigger('change.chunky');
            })
        ;
        return dropbox;
    }

    generateTableUploads(multiple)
    {
        var table = $('<table class="table table-striped table-hover hide">')
        var tr = $('<tr>');
        var selector;
        if (multiple) {
            selector = $('<th class="table-checkbox">');
            let label = $('<label>');
            let checkAll = $('<input type="checkbox" class="checkall">');
            checkAll.on('change.chunky', () => {
                var checked = checkAll.prop('checked');
                table.find('.selection-checkbox').prop('checked', checked);
                this.input.trigger('check-file.chunky');
            });
            label.append($('<span class="sr-only">').text(ChunkyUploader.translations.select_all));
            label.append(checkAll);
            selector.append(label);
        } else {
            selector = $('<th>').text(ChunkyUploader.translations.select);
        }
        tr.append(selector);
        tr.append($('<th>').text(ChunkyUploader.translations.file));
        tr.append($('<th>').text(ChunkyUploader.translations.action));
        var thead = $('<thead>').append(tr);
        table.append(thead);
        var tbody = $('<tbody>');
        table.append(tbody);
        return table;
    }

    handleCheckEvent(input, table)
    {
        return () => {
            if (table.find('tbody input:checked').length === 0) {
                input.prop('required', input.get(0).hasAttribute('data-required'));
            } else {
                input.prop('required', false);
            }
            if (table.find('tbody tr').length === 0) {
                table.addClass('hide');
                input.trigger('upload-finished.chunky');
            }
        };
    }

    handleInputChange(input, table)
    {
        return () => {
            var files = input.get(0).files;
            if (!files.length) {
                return;
            }
            if (table.hasClass('hide')) {
                table.removeClass('hide');
            }
            input.val('');
            if (input.prop('required')) {
                input.prop('required', false);
                input.attr('data-required', '1');
            }
            for (let i = 0; i < files.length; i++) {
                let chunkyFile = new ChunkyFile(this, files[i]);
                if (!this.sizeLimit || this.sizeLimit > files[i].size) {
                    this.enqueue(chunkyFile);
                    table.find('tbody').append(chunkyFile.generateTr());
                } else {
                    table.find('tbody').append(chunkyFile.generateExceededSizeTr());
                }
            }
            if (this.uploading === false) {
                this.run();
            }
        };
    }

    createTableFromPreviousValues(input, table, previousValue)
    {
        if (input.prop('required')) {
            input.prop('required', false);
        }
        if (!Array.isArray(previousValue)) {
            previousValue = [previousValue];
        }
        for (let i = 0; i < previousValue.length; i++) {
            let chunkyFile = new ChunkyFile(this, previousValue[i]);
            chunkyFile.uuid = previousValue[i].uuid;
            table.find('tbody').append(chunkyFile.generateTr());
            table.removeClass('hide');
            chunkyFile.tr.find('.progress-bar').css('width', '100%');
            chunkyFile.tr.find('.progress-bar').addClass('progress-bar-success');
            chunkyFile.tr.find('.uploading-message').remove();
            chunkyFile.tr.find('.action-pause').remove();
            chunkyFile.tr.find('.action-resume').remove();
        }
    }

    wrapInput()
    {
        var uploader = this;
        var input = this.input;
        input.closest('.custom-file').find('.custom-file-label').remove();
        input.removeClass('custom-file-input');
        input.data('uploader', this);

        var dropbox = this.generateDropbox(input);
        var multiple = input.get(0).hasAttribute('multiple');
        var table = this.generateTableUploads(multiple);
        dropbox.after(table);
        if (input.prop('required')) {
            input.attr('data-required', '1');
            input.on('check-file.chunky', this.handleCheckEvent(input, table));
        }

        var s = ChunkyUploader.translations.size;
        var name = input.attr('name');
        input.on('change.chunky', this.handleInputChange(input, table));
        input.on('upload-started.chunky', () => {
            input.get(0).setCustomValidity(ChunkyUploader.translations.uploading_validation);
        });
        input.on('upload-finished.chunky', () => {
            input.get(0).setCustomValidity("");
        });

        let previousValue = JSON.parse(input.attr('data-value') || '""');
        if (previousValue) {
            this.createTableFromPreviousValues(input, table, previousValue);
        }
    }

    pauseAction(chunkyFile)
    {
        let uuid = chunkyFile.uuid;
        chunkyFile.paused = true;
        chunkyFile.pauseTime = performance.now();
        if (this.currentFile?.uuid === uuid) {
            this.stop();
            if (this.queue.length) {
                this.run();
            }
        } else {
            this.unqueue(chunkyFile);
        }
        chunkyFile.tr.find('.action-pause').hide();
        chunkyFile.tr.find('.action-resume').show();
    }

    resumeAction(chunkyFile)
    {
        this.queue.unshift(chunkyFile);
        if (this.stopped) {
            this.run();
        }
        chunkyFile.tr.find('.action-pause').show();
        chunkyFile.tr.find('.action-resume').hide();
    }

    deleteAction(chunkyFile)
    {
        if (!confirm(ChunkyUploader.translations.confirm_delete)) {
            return;
        }
        chunkyFile.tr.find('button').removeAttr('data-toggle').tooltip('hide').tooltip('disable').disable();
        this.pauseAction(chunkyFile);
        this.unqueue(chunkyFile);
        if (!this.queue.length) {
            this.uploading = false;
            this.input.trigger('upload-finished.chunky');
        }
        let url = chunkyFile.file.deleteUrl
            ? chunkyFile.file.deleteUrl
            : ("%s?f=%s").format(
                this.url,
                encodeURIComponent(chunkyFile.file.name)
            );
        $.ajax(
            {
                url: url,
                method: 'DELETE',
                data: {uuid: chunkyFile.uuid},
                success: (content) => {
                    if (content.indexOf('deleted') === -1 ) {
                        return;
                    }
                    chunkyFile.tr.fadeOut(
                        400,
                        () => {
                            let tbody = chunkyFile.tr.closest('tbody');
                            chunkyFile.tr.remove();
                            this.input.trigger('check-file.chunky');
                        }
                    );
                }
            }
        );
    }
}

class ChunkyFile {
    constructor(uploader, file) {
        this.uploader = uploader;
        this.file = file;
        this.uuid = uuidv4();
        this.succededChunks = [];
        this.remainingChunks = [];
        this.failedChunks = [];
        this.beginTime = null;
        this.pauseTime = null;
        this.progresses = [];
        this.paused = false;
        this.totalChunk = Math.ceil(file.size / ChunkyUploader.CHUNK_SIZE);
        for (let i = 0; i < this.totalChunk; i++) {
            this.remainingChunks.push(i);
        }
    }

    generateFileTd()
    {
        let fileTd = $('<td>');
        var s = ChunkyUploader.translations.size;
        let e = Math.floor(Math.log(this.file.size) / Math.log(1024));
        let size = Number.isFinite(e)
            ? (this.file.size / Math.pow(1024, e)).toFixed(2) + " " + s[e]
            : '0'
        let chunkCount = Math.ceil(this.file.size / ChunkyUploader.CHUNK_SIZE);
        let progressBar = $('<div class="progress-bar" role="progressbar">')
            .attr('aria-valuenow', 0)
            .attr('aria-valuemin', 0)
            .attr('aria-valuemax', chunkCount)
            .css('width', 0)
        ;
        let progress = $('<div class="progress">').append(progressBar);
        fileTd.append(progress);
        fileTd.append(
            $('<div class="uploader-filername-container">')
                .append($('<span class="uploader-filername">').text(this.file.name))
                .append($('<span class="uploader-size float-right">').text(size))
        );
        let statusMessage = $('<div class="uploading-message">')
            .append($('<span class="duration">'))
            .append($('<span class="speed">'))
            .append($('<span class="estimated">'))
            .css('opacity', 0)
        ;
        fileTd.append(statusMessage);
        fileTd.css('width', '100%');
        return fileTd;
    }

    generateTr()
    {
        let name = this.uploader.input.attr('name');
        var multiple = this.uploader.input.get(0).hasAttribute('multiple');
        var required = this.uploader.input.get(0).hasAttribute('data-required');
        let tr = $('<tr>').attr('data-id', this.uuid);

        let selectionTd = $('<td class="table-checkbox">');
        let label = $('<label>');
        label.append($('<span class="sr-only">').text(ChunkyUploader.translations.select_file + this.file.name));
        if (multiple) {
            let checkbox = $('<input type="checkbox" class="selection-checkbox">')
                .val(this.uuid)
                .attr('name', name + '[]')
                .prop('checked', true)
                .on('change.chunky', () => this.uploader.input.trigger('check-file.chunky'));
            label.append(checkbox);
        } else {
            label.append(
                $('<input type="radio" class="selection-radio">')
                    .val(this.uuid)
                    .attr('name', name)
                    .prop('checked', true)
                    .on('change.chunky', () => this.uploader.input.trigger('check-file.chunky'))
            );
        }
        selectionTd.append(label);
        tr.append(selectionTd);

        let fileTd = this.generateFileTd();
        tr.append(fileTd);

        let actionTd = $('<td class="action">');
        let pauseBtn = $('<button type="button" class="btn btn-link action-pause" data-toggle="tooltip">')
            .attr('title', "Mettre le transfert de fichier en pause")
            .append($('<i class="fa fa-pause">'))
            .append($('<span class="sr-only">').text(ChunkyUploader.translations.pause))
            .on('click.chunky', () => this.uploader.pauseAction(this))
        ;
        actionTd.append(pauseBtn);
        let resumeBtn = $('<button type="button" class="btn btn-link action-resume" data-toggle="tooltip">')
            .hide()
            .attr('title', "Continuer le transfert de fichier")
            .append($('<i class="fa fa-play">'))
            .append($('<span class="sr-only">').text(ChunkyUploader.translations.resume))
            .on('click.chunky', () => this.uploader.resumeAction(this))
        ;
        actionTd.append(resumeBtn);
        let deleteBtn = $('<button type="button" class="btn btn-link action-delete" data-toggle="tooltip">')
            .attr('title', "Supprimer le transfert de fichier")
            .append($('<i class="fa fa-trash text-danger">'))
            .append($('<span class="sr-only">').text(ChunkyUploader.translations.delete))
            .on('click.chunky', () => this.uploader.deleteAction(this))
        ;
        actionTd.append(deleteBtn);
        tr.append(actionTd);
        this.tr = tr;
        return tr;
    }

    generateExceededSizeTr()
    {
        let name = this.uploader.input.attr('name');
        var multiple = this.uploader.input.get(0).hasAttribute('multiple');
        var required = this.uploader.input.get(0).hasAttribute('data-required');
        let tr = $('<tr>').attr('data-id', this.uuid);

        let selectionTd = $('<td class="table-checkbox">');
        tr.append(selectionTd);

        let fileTd = this.generateFileTd();
        fileTd.find('.uploading-message').empty().css('opacity', 1).text(
            "Ce fichier dépasse la taille limite autorisée"
        );
        tr.append(fileTd);

        let actionTd = $('<td class="action">');
        let deleteBtn = $('<button type="button" class="btn btn-link action-delete" data-toggle="tooltip">')
            .attr('title', "Supprimer le transfert de fichier")
            .append($('<i class="fa fa-times-circle-o">'))
            .append($('<span class="sr-only">').text(ChunkyUploader.translations.delete))
        ;
        deleteBtn.on('click.chunky', () => tr.fadeOut(
            400,
            () => {
                deleteBtn.tooltip('dispose');
                let tbody = tr.closest('tbody');
                tr.remove();
                if (tbody.find('tr input:checked').length === 0) {
                    this.uploader.input.prop(
                        'required',
                        this.uploader.input.get(0).hasAttribute('data-required')
                    );
                }
                if (tbody.find('tr').length === 0) {
                    tr.closest('table').addClass('hide');
                    this.uploader.input.trigger('upload-finished.chunky');
                }
            }
        ));
        actionTd.append(deleteBtn);
        tr.append(actionTd);
        this.tr = tr;
        return tr;
    }
}

$(function() {
    $('[data-uploader]').each(function() {
        var sizeLimit = $(this).attr('data-uploader-filesize-limit');
        var uploader = new ChunkyUploader($(this), sizeLimit);
        uploader.wrapInput();
    });
});
$(document).on('modal-loaded', function (event, modal) {
    modal.find('[data-uploader]').each(function() {
        var sizeLimit = $(this).attr('data-uploader-filesize-limit');
        var uploader = new ChunkyUploader($(this), sizeLimit);
        uploader.wrapInput();
    });
});

export default ChunkyUploader;
