import $ from 'jquery';
import { AsalaeGlobal } from '../vendor/libriciel/asalae-assets/src/js/asalae.global-top'
import { AsalaeFilter } from '../vendor/libriciel/asalae-assets/src/js/asalae.filter';
window.AsalaeFilter = AsalaeFilter;

AsalaeGlobal.interceptedLinkToAjax = function(href) {
    window.location.href = href;
};

$(function() {
    var form = $('[data-filter-form]');
    var filterTemplates = {};
    form.find('.filter-templates > [data-name]').each(function() {
        filterTemplates[$(this).attr('data-name')] = $(this).prop('outerHTML');
    });
    var searchForm = new AsalaeFilter(form, filterTemplates);
    form.on('new-filter.filter', function (event, container) {
        $(document).trigger('modal-loaded', [container]);
    });
    $(document).on('submit.filter', 'form[data-filter-form]', function(event) {
        event.preventDefault();

        if ($(event.target).get(0).checkValidity()) {
            let clone = $(event.target).clone();
            clone.find('[data-prepend-value-to]').each(function() {
                var target = clone.find('[id="' + $(this).attr('data-prepend-value-to') + '"]');
                if (target.is(':disabled')) {
                    target.enable().val('');
                }
                var newInput = $('<input>')
                    .attr('name', $(target).attr('name'))
                    .val($(this).val() + '~' + target.val());
                target.replaceWith(newInput);
            });
            clone.find('input, select, textarea').filter(function() {
                return !$(this).val();
            }).remove();
            var data = clone.serialize();
            var action = $(this).attr('action');
            if (data) {
                action += (action.indexOf('?') >= 0 ? '&' : '?') + data;
            }
            window.location.href = action;
        } else {
            $(form).get(0).reportValidity();
        }
    });

    var nameCounts = {};
    form.find('.filter-container [data-field]').each(function () {
        var name = $(this).attr('data-field');
        var basename = name;
        if (name.indexOf('[]') !== -1) {
            basename = name.substr(0, name.indexOf('[]'));
        }
        var value = JSON.parse($(this).attr('data-value'));
        if (!nameCounts[basename]) {
            nameCounts[basename] = 0;
        }

        var filter = $(filterTemplates[basename]);
        filter.find('[id]').each(
            function() {
                $(this).attr('id', $(this).attr('id') + '-' + nameCounts[basename]);
            }
        );
        filter.find('[for]').each(
            function() {
                $(this).attr('for', $(this).attr('for') + '-' + nameCounts[basename]);
            }
        );
        filter.find('[name]').each(
            function() {
                let name = $(this).attr('name');
                let m = name.match(/^(.*)\[]$/);
                if (m) {
                    name = m[1] + '[' + nameCounts[basename] + '][]';
                } else {
                    name += '[' + nameCounts[basename] + ']';
                }
                $(this).attr('name', name);
            }
        );
        var splittedValue = filter.find('[data-prepend-value-to]');
        splittedValue.each(
            function() {
                var splittedValue = value.split('~');
                $(this).attr('data-prepend-value-to', $(this).attr('data-prepend-value-to') + '-' + nameCounts[basename]);
                $(this).val(splittedValue[0]);
                filter.find('[id="' + $(this).attr('data-prepend-value-to') + '"]').val(splittedValue[1]);
            }
        );
        if (splittedValue.length === 0) {
            filter.find('input, select, textarea').val(value);
        }

        var container = AsalaeFilter.getContainerWithCloseBtn();
        container.append(filter);
        $(this).replaceWith(container);

        nameCounts[basename]++;
    });

    var formSaveFilters = $('form.filter-popup');
    formSaveFilters.off('.filter').on('submit.filter', function(event) {
        $(event.target).find('[name="table_filters[filters]"]').val(form.serialize());
    });
});

$(document).on('change.disable_linked_date', 'select[data-disable-linked-date]', function() {
    var target = $('#' + $(this).attr('data-prepend-value-to'));
    var option = $(this).find('option[value="' + $(this).val() + '"]');
    if (option.is('[data-option-disable]')) {
        target.disable();
    } else {
        target.enable();
    }
});
$(function() {
    $('[data-disable-linked-date]').trigger('change');
});
