import $ from 'jquery';
import { AsalaeGlobal } from '../vendor/libriciel/asalae-assets/src/js/asalae.global-top'

/**
 * jquery.actual.js
 *
 * .width() ne donne pas le bon résultat sur élément caché.
 * Toujours pas de solution fonctionnelle sur stackoverflow (2023)
 * Projet git non maintenu mais code toujours fonctionnel
 * @see https://github.com/dreamerslab/jquery.actual
 */
(function(a){if(typeof define==="function"&&define.amd){define(["jquery"],a);
}else{a(jQuery);}}(function(a){a.fn.addBack=a.fn.addBack||a.fn.andSelf;a.fn.extend({actual:function(b,l){if(!this[b]){throw'$.actual => The jQuery method "'+b+'" you called does not exist';
    }var f={absolute:false,clone:false,includeMargin:false,display:"block"};var i=a.extend(f,l);var e=this.eq(0);var h,j;if(i.clone===true){h=function(){var m="position: absolute !important; top: -1000 !important; ";
        e=e.clone().attr("style",m).appendTo("body");};j=function(){e.remove();};}else{var g=[];var d="";var c;h=function(){c=e.parents().addBack().filter(":hidden");
        d+="visibility: hidden !important; display: "+i.display+" !important; ";if(i.absolute===true){d+="position: absolute !important; ";}c.each(function(){var m=a(this);
            var n=m.attr("style");g.push(n);m.attr("style",n?n+";"+d:d);});};j=function(){c.each(function(m){var o=a(this);var n=g[m];if(n===undefined){o.removeAttr("style");
    }else{o.attr("style",n);}});};}h();var k=/(outer)/.test(b)?e[b](i.includeMargin):e[b]();j();return k;}});}));


$.fn.draggableCustom = function (options = {}) {
    if (typeof options !== 'object') {
        options = {};
    }
    options.handle = typeof options.handle !== 'string'
        ? '.handle'
        : options.handle
    ;
    this.each(
        function () {
            var container = $(this);
            $(this).find(options.handle).each(
                function () {
                    var handle = $(this);
                    handle.css('cursor', 'move');
                    handle.on(
                        'mousedown.draggableCustom',
                        function (event) {
                            container.addClass('grabbing')
                                .data('pageX', event.pageX)
                                .data('pageY', event.pageY);
                        }
                    );
                    $(window).on(
                        'mousemove.draggableCustom',
                        function (event) {
                            if (container.hasClass('grabbing')) {
                                var diffX = container.data('pageX') - event.pageX,
                                    diffY = container.data('pageY') - event.pageY,
                                    left = parseInt(container.css('left'), 10) - diffX,
                                    top = parseInt(container.css('top'), 10) - diffY
                                ;
                                container.css(
                                    {
                                        left: parseInt(container.css('left'), 10) - diffX,
                                        top: parseInt(container.css('top'), 10) - diffY,
                                        right: (parseInt(container.css('left'), 10) - diffX) * -1,
                                        bottom: (parseInt(container.css('top'), 10) - diffY) * -1
                                    }
                                )
                                    .data('pageX', event.pageX)
                                    .data('pageY', event.pageY);
                                // Assure le non dépassement du cadre du body
                                let position = container.offset(),
                                    parentPosition = container.parent().offset(),
                                    x1 = position.left - parentPosition.left,
                                    x2 = x1 + container.outerWidth(),
                                    y1 = position.top - parentPosition.top,
                                    y2 = y1 + container.outerHeight(),
                                    body = $('body'),
                                    bodyX = body.outerWidth(),
                                    bodyY = body.outerHeight();
                                if (x1 > bodyX) {
                                    container.css(
                                        {
                                            left: '-='+(x2 - bodyX),
                                            right: '+='+(x2 - bodyX)
                                        }
                                    );
                                }
                                if (x2 < 0) {
                                    container.css(
                                        {
                                            left: '-='+x1,
                                            right: '+='+x1
                                        }
                                    );
                                }
                                if (y1 > bodyY) {
                                    container.css(
                                        {
                                            top: '-='+(y2 - bodyY),
                                            bottom: '+='+(y2 - bodyY)
                                        }
                                    );
                                }
                                if (y2 < 0) {
                                    container.css(
                                        {
                                            top: '-='+y1,
                                            bottom: '+='+y1
                                        }
                                    );
                                }

                                // Retire les sélections potentiellement occasionnées par
                                // le déplacement de la modale
                                document.getSelection().removeAllRanges();
                            }
                        }
                    );
                    $(window).on('mouseup.draggableCustom', AsalaeGlobal.grabHandlers.mouseup);
                }
            );
        }
    );
    return this;
};

var uniqueId = $.fn.extend( {
    uniqueId: ( function() {
        var uuid = 0;

        return function() {
            return this.each( function() {
                if ( !this.id ) {
                    this.id = "ui-id-" + ( ++uuid );
                }
            } );
        };
    } )(),

    removeUniqueId: function() {
        return this.each( function() {
            if ( /^ui-id-\d+$/.test( this.id ) ) {
                $( this ).removeAttr( "id" );
            }
        } );
    }
} );


// noinspection OverlyComplexFunctionJS
$.widget( "ui.tabs", {
    version: "1.12.1",
    delay: 300,
    options: {
        active: null,
        classes: {
            "ui-tabs": "ui-corner-all",
            "ui-tabs-nav": "ui-corner-all",
            "ui-tabs-panel": "ui-corner-bottom",
            "ui-tabs-tab": "ui-corner-top"
        },
        collapsible: false,
        event: "click",
        heightStyle: "content",
        hide: null,
        show: null,

        // Callbacks
        activate: null,
        beforeActivate: null,
        beforeLoad: null,
        load: null
    },

    _isLocal: ( function() {
        var rhash = /#.*$/;

        return function( anchor ) {
            var anchorUrl, locationUrl;

            anchorUrl = anchor.href.replace( rhash, "" );
            locationUrl = location.href.replace( rhash, "" );

            // Decoding may throw an error if the URL isn't UTF-8 (#9518)
            try {
                anchorUrl = decodeURIComponent( anchorUrl );
            } catch ( error ) {}
            try {
                locationUrl = decodeURIComponent( locationUrl );
            } catch ( error ) {}

            return anchor.hash.length > 1 && anchorUrl === locationUrl;
        };
    } )(),

    _create: function() {
        var that = this,
            options = this.options;

        this.running = false;

        this._addClass( "ui-tabs", "ui-widget ui-widget-content" );
        this._toggleClass( "ui-tabs-collapsible", null, options.collapsible );

        this._processTabs();
        options.active = this._initialActive();

        // Take disabling tabs via class attribute from HTML
        // into account and update option properly.
        if ( $.isArray( options.disabled ) ) {
            options.disabled = $.unique( options.disabled.concat(
                $.map( this.tabs.filter( ".ui-state-disabled" ), function( li ) {
                    return that.tabs.index( li );
                } )
            ) ).sort();
        }

        // Check for length avoids error when initializing empty list
        if ( this.options.active !== false && this.anchors.length ) {
            this.active = this._findActive( options.active );
        } else {
            this.active = $();
        }

        this._refresh();

        if ( this.active.length ) {
            this.load( options.active );
        }
    },

    _initialActive: function() {
        var active = this.options.active,
            collapsible = this.options.collapsible,
            locationHash = location.hash.substring( 1 );

        if ( active === null ) {

            // check the fragment identifier in the URL
            if ( locationHash ) {
                this.tabs.each( function( i, tab ) {
                    if ( $( tab ).attr( "aria-controls" ) === locationHash ) {
                        active = i;
                        return false;
                    }
                } );
            }

            // Check for a tab marked active via a class
            if ( active === null ) {
                active = this.tabs.index( this.tabs.filter( ".ui-tabs-active" ) );
            }

            // No active tab, set to false
            if ( active === null || active === -1 ) {
                active = this.tabs.length ? 0 : false;
            }
        }

        // Handle numbers: negative, out of range
        if ( active !== false ) {
            active = this.tabs.index( this.tabs.eq( active ) );
            if ( active === -1 ) {
                active = collapsible ? false : 0;
            }
        }

        // Don't allow collapsible: false and active: false
        if ( !collapsible && active === false && this.anchors.length ) {
            active = 0;
        }

        return active;
    },

    _getCreateEventData: function() {
        return {
            tab: this.active,
            panel: !this.active.length ? $() : this._getPanelForTab( this.active )
        };
    },

    _tabKeydown: function( event ) {
        var focusedTab = $( $.ui.safeActiveElement( this.document[ 0 ] ) ).closest( "li" ),
            selectedIndex = this.tabs.index( focusedTab ),
            goingForward = true;

        if ( this._handlePageNav( event ) ) {
            return;
        }

        switch ( event.keyCode ) {
            case $.ui.keyCode.RIGHT:
            case $.ui.keyCode.DOWN:
                selectedIndex++;
                break;
            case $.ui.keyCode.UP:
            case $.ui.keyCode.LEFT:
                goingForward = false;
                selectedIndex--;
                break;
            case $.ui.keyCode.END:
                selectedIndex = this.anchors.length - 1;
                break;
            case $.ui.keyCode.HOME:
                selectedIndex = 0;
                break;
            case $.ui.keyCode.SPACE:

                // Activate only, no collapsing
                event.preventDefault();
                clearTimeout( this.activating );
                this._activate( selectedIndex );
                return;
            case $.ui.keyCode.ENTER:

                // Toggle (cancel delayed activation, allow collapsing)
                event.preventDefault();
                clearTimeout( this.activating );

                // Determine if we should collapse or activate
                this._activate( selectedIndex === this.options.active ? false : selectedIndex );
                return;
            default:
                return;
        }

        // Focus the appropriate tab, based on which key was pressed
        event.preventDefault();
        clearTimeout( this.activating );
        selectedIndex = this._focusNextTab( selectedIndex, goingForward );

        // Navigating with control/command key will prevent automatic activation
        if ( !event.ctrlKey && !event.metaKey ) {

            // Update aria-selected immediately so that AT think the tab is already selected.
            // Otherwise AT may confuse the user by stating that they need to activate the tab,
            // but the tab will already be activated by the time the announcement finishes.
            focusedTab.attr( "aria-selected", "false" );
            this.tabs.eq( selectedIndex ).attr( "aria-selected", "true" );

            this.activating = this._delay( function() {
                this.option( "active", selectedIndex );
            }, this.delay );
        }
    },

    _panelKeydown: function( event ) {
        if ( this._handlePageNav( event ) ) {
            return;
        }

        // Ctrl+up moves focus to the current tab
        if ( event.ctrlKey && event.keyCode === $.ui.keyCode.UP ) {
            event.preventDefault();
            this.active.trigger( "focus" );
        }
    },

    // Alt+page up/down moves focus to the previous/next tab (and activates)
    _handlePageNav: function( event ) {
        if ( event.altKey && event.keyCode === $.ui.keyCode.PAGE_UP ) {
            this._activate( this._focusNextTab( this.options.active - 1, false ) );
            return true;
        }
        if ( event.altKey && event.keyCode === $.ui.keyCode.PAGE_DOWN ) {
            this._activate( this._focusNextTab( this.options.active + 1, true ) );
            return true;
        }
    },

    _findNextTab: function( index, goingForward ) {
        var lastTabIndex = this.tabs.length - 1;

        function constrain() {
            if ( index > lastTabIndex ) {
                index = 0;
            }
            if ( index < 0 ) {
                index = lastTabIndex;
            }
            return index;
        }

        while ( $.inArray( constrain(), this.options.disabled ) !== -1 ) {
            index = goingForward ? index + 1 : index - 1;
        }

        return index;
    },

    _focusNextTab: function( index, goingForward ) {
        index = this._findNextTab( index, goingForward );
        this.tabs.eq( index ).trigger( "focus" );
        return index;
    },

    _setOption: function( key, value ) {
        if ( key === "active" ) {

            // _activate() will handle invalid values and update this.options
            this._activate( value );
            return;
        }

        this._super( key, value );

        if ( key === "collapsible" ) {
            this._toggleClass( "ui-tabs-collapsible", null, value );

            // Setting collapsible: false while collapsed; open first panel
            if ( !value && this.options.active === false ) {
                this._activate( 0 );
            }
        }

        if ( key === "event" ) {
            this._setupEvents( value );
        }

        if ( key === "heightStyle" ) {
            this._setupHeightStyle( value );
        }
    },

    _sanitizeSelector: function( hash ) {
        return hash ? hash.replace( /[!"$%&'()*+,.\/:;<=>?@\[\]^`{|}~]/g, "\\$&" ) : "";
    },

    refresh: function() {
        var options = this.options,
            lis = this.tablist.children( ":has(a[href])" );

        // Get disabled tabs from class attribute from HTML
        // this will get converted to a boolean if needed in _refresh()
        options.disabled = $.map( lis.filter( ".ui-state-disabled" ), function( tab ) {
            return lis.index( tab );
        } );

        this._processTabs();

        // Was collapsed or no tabs
        if ( options.active === false || !this.anchors.length ) {
            options.active = false;
            this.active = $();

            // was active, but active tab is gone
        } else if ( this.active.length && !$.contains( this.tablist[ 0 ], this.active[ 0 ] ) ) {

            // all remaining tabs are disabled
            if ( this.tabs.length === options.disabled.length ) {
                options.active = false;
                this.active = $();

                // activate previous tab
            } else {
                this._activate( this._findNextTab( Math.max( 0, options.active - 1 ), false ) );
            }

            // was active, active tab still exists
        } else {

            // make sure active index is correct
            options.active = this.tabs.index( this.active );
        }

        this._refresh();
    },

    _refresh: function() {
        this._setOptionDisabled( this.options.disabled );
        this._setupEvents( this.options.event );
        this._setupHeightStyle( this.options.heightStyle );

        this.tabs.not( this.active ).attr( {
            "aria-selected": "false",
            "aria-expanded": "false",
            tabIndex: -1
        } );
        this.panels.not( this._getPanelForTab( this.active ) )
            .hide()
            .attr( {
                "aria-hidden": "true"
            } );

        // Make sure one tab is in the tab order
        if ( !this.active.length ) {
            this.tabs.eq( 0 ).attr( "tabIndex", 0 );
        } else {
            this.active
                .attr( {
                    "aria-selected": "true",
                    "aria-expanded": "true",
                    tabIndex: 0
                } );
            this._addClass( this.active, "ui-tabs-active", "ui-state-active" );
            this._getPanelForTab( this.active )
                .show()
                .attr( {
                    "aria-hidden": "false"
                } );
        }
    },

    _processTabs: function() {
        var that = this,
            prevTabs = this.tabs,
            prevAnchors = this.anchors,
            prevPanels = this.panels;

        this.tablist = this._getList().attr( "role", "tablist" );
        this._addClass( this.tablist, "ui-tabs-nav",
            "ui-helper-reset ui-helper-clearfix ui-widget-header" );

        // Prevent users from focusing disabled tabs via click
        this.tablist
            .on( "mousedown" + this.eventNamespace, "> li", function( event ) {
                if ( $( this ).is( ".ui-state-disabled" ) ) {
                    event.preventDefault();
                }
            } )

            // Support: IE <9
            // Preventing the default action in mousedown doesn't prevent IE
            // from focusing the element, so if the anchor gets focused, blur.
            // We don't have to worry about focusing the previously focused
            // element since clicking on a non-focusable element should focus
            // the body anyway.
            .on( "focus" + this.eventNamespace, ".ui-tabs-anchor", function() {
                if ( $( this ).closest( "li" ).is( ".ui-state-disabled" ) ) {
                    this.blur();
                }
            } );

        this.tabs = this.tablist.find( "> li:has(a[href])" )
            .attr( {
                role: "tab",
                tabIndex: -1
            } );
        this._addClass( this.tabs, "ui-tabs-tab", "ui-state-default" );

        this.anchors = this.tabs.map( function() {
            return $( "a", this )[ 0 ];
        } )
            .attr( {
                role: "presentation",
                tabIndex: -1
            } );
        this._addClass( this.anchors, "ui-tabs-anchor" );

        this.panels = $();

        this.anchors.each( function( i, anchor ) {
            var selector, panel, panelId,
                anchorId = $( anchor ).uniqueId().attr( "id" ),
                tab = $( anchor ).closest( "li" ),
                originalAriaControls = tab.attr( "aria-controls" );

            // Inline tab
            if ( that._isLocal( anchor ) ) {
                selector = anchor.hash;
                panelId = selector.substring( 1 );
                panel = that.element.find( that._sanitizeSelector( selector ) );

                // remote tab
            } else {

                // If the tab doesn't already have aria-controls,
                // generate an id by using a throw-away element
                panelId = tab.attr( "aria-controls" ) || $( {} ).uniqueId()[ 0 ].id;
                selector = "#" + panelId;
                panel = that.element.find( selector );
                if ( !panel.length ) {
                    panel = that._createPanel( panelId );
                    panel.insertAfter( that.panels[ i - 1 ] || that.tablist );
                }
                panel.attr( "aria-live", "polite" );
            }

            if ( panel.length ) {
                that.panels = that.panels.add( panel );
            }
            if ( originalAriaControls ) {
                tab.data( "ui-tabs-aria-controls", originalAriaControls );
            }
            tab.attr( {
                "aria-controls": panelId,
                "aria-labelledby": anchorId
            } );
            panel.attr( "aria-labelledby", anchorId );
        } );

        this.panels.attr( "role", "tabpanel" );
        this._addClass( this.panels, "ui-tabs-panel", "ui-widget-content" );

        // Avoid memory leaks (#10056)
        if ( prevTabs ) {
            this._off( prevTabs.not( this.tabs ) );
            this._off( prevAnchors.not( this.anchors ) );
            this._off( prevPanels.not( this.panels ) );
        }
    },

    // Allow overriding how to find the list for rare usage scenarios (#7715)
    _getList: function() {
        return this.tablist || this.element.find( "ol, ul" ).eq( 0 );
    },

    _createPanel: function( id ) {
        return $( "<div>" )
            .attr( "id", id )
            .data( "ui-tabs-destroy", true );
    },

    _setOptionDisabled: function( disabled ) {
        var currentItem, li, i;

        if ( $.isArray( disabled ) ) {
            if ( !disabled.length ) {
                disabled = false;
            } else if ( disabled.length === this.anchors.length ) {
                disabled = true;
            }
        }

        // Disable tabs
        for ( i = 0; ( li = this.tabs[ i ] ); i++ ) {
            currentItem = $( li );
            if ( disabled === true || $.inArray( i, disabled ) !== -1 ) {
                currentItem.attr( "aria-disabled", "true" );
                this._addClass( currentItem, null, "ui-state-disabled" );
            } else {
                currentItem.removeAttr( "aria-disabled" );
                this._removeClass( currentItem, null, "ui-state-disabled" );
            }
        }

        this.options.disabled = disabled;

        this._toggleClass( this.widget(), this.widgetFullName + "-disabled", null,
            disabled === true );
    },

    _setupEvents: function( event ) {
        var events = {};
        if ( event ) {
            $.each( event.split( " " ), function( index, eventName ) {
                events[ eventName ] = "_eventHandler";
            } );
        }

        this._off( this.anchors.add( this.tabs ).add( this.panels ) );

        // Always prevent the default action, even when disabled
        this._on( true, this.anchors, {
            click: function( event ) {
                event.preventDefault();
            }
        } );
        this._on( this.anchors, events );
        this._on( this.tabs, { keydown: "_tabKeydown" } );
        this._on( this.panels, { keydown: "_panelKeydown" } );

        this._focusable( this.tabs );
        this._hoverable( this.tabs );
    },

    _setupHeightStyle: function( heightStyle ) {
        var maxHeight,
            parent = this.element.parent();

        if ( heightStyle === "fill" ) {
            maxHeight = parent.height();
            maxHeight -= this.element.outerHeight() - this.element.height();

            this.element.siblings( ":visible" ).each( function() {
                var elem = $( this ),
                    position = elem.css( "position" );

                if ( position === "absolute" || position === "fixed" ) {
                    return;
                }
                maxHeight -= elem.outerHeight( true );
            } );

            this.element.children().not( this.panels ).each( function() {
                maxHeight -= $( this ).outerHeight( true );
            } );

            this.panels.each( function() {
                $( this ).height( Math.max( 0, maxHeight -
                    $( this ).innerHeight() + $( this ).height() ) );
            } )
                .css( "overflow", "auto" );
        } else if ( heightStyle === "auto" ) {
            maxHeight = 0;
            this.panels.each( function() {
                maxHeight = Math.max( maxHeight, $( this ).height( "" ).height() );
            } ).height( maxHeight );
        }
    },

    _eventHandler: function( event ) {
        var options = this.options,
            active = this.active,
            anchor = $( event.currentTarget ),
            tab = anchor.closest( "li" ),
            clickedIsActive = tab[ 0 ] === active[ 0 ],
            collapsing = clickedIsActive && options.collapsible,
            toShow = collapsing ? $() : this._getPanelForTab( tab ),
            toHide = !active.length ? $() : this._getPanelForTab( active ),
            eventData = {
                oldTab: active,
                oldPanel: toHide,
                newTab: collapsing ? $() : tab,
                newPanel: toShow
            };

        event.preventDefault();

        // noinspection OverlyComplexBooleanExpressionJS
        if ( tab.hasClass( "ui-state-disabled" ) ||

            // tab is already loading
            tab.hasClass( "ui-tabs-loading" ) ||

            // can't switch durning an animation
            this.running ||

            // click on active header, but not collapsible
            ( clickedIsActive && !options.collapsible ) ||

            // allow canceling activation
            ( this._trigger( "beforeActivate", event, eventData ) === false ) ) {
            return;
        }

        options.active = collapsing ? false : this.tabs.index( tab );

        this.active = clickedIsActive ? $() : tab;
        if ( this.xhr ) {
            this.xhr.abort();
        }

        if ( !toHide.length && !toShow.length ) {
            $.error( "jQuery UI Tabs: Mismatching fragment identifier." );
        }

        if ( toShow.length ) {
            this.load( this.tabs.index( tab ), event );
        }
        this._toggle( event, eventData );
    },

    // Handles show/hide for selecting tabs
    _toggle: function( event, eventData ) {
        var that = this,
            toShow = eventData.newPanel,
            toHide = eventData.oldPanel;

        this.running = true;

        function complete() {
            that.running = false;
            that._trigger( "activate", event, eventData );
        }

        function show() {
            that._addClass( eventData.newTab.closest( "li" ), "ui-tabs-active", "ui-state-active" );

            if ( toShow.length && that.options.show ) {
                that._show( toShow, that.options.show, complete );
            } else {
                toShow.show();
                complete();
            }
        }

        // Start out by hiding, then showing, then completing
        if ( toHide.length && this.options.hide ) {
            this._hide( toHide, this.options.hide, function() {
                that._removeClass( eventData.oldTab.closest( "li" ),
                    "ui-tabs-active", "ui-state-active" );
                show();
            } );
        } else {
            this._removeClass( eventData.oldTab.closest( "li" ),
                "ui-tabs-active", "ui-state-active" );
            toHide.hide();
            show();
        }

        toHide.attr( "aria-hidden", "true" );
        eventData.oldTab.attr( {
            "aria-selected": "false",
            "aria-expanded": "false"
        } );

        // If we're switching tabs, remove the old tab from the tab order.
        // If we're opening from collapsed state, remove the previous tab from the tab order.
        // If we're collapsing, then keep the collapsing tab in the tab order.
        if ( toShow.length && toHide.length ) {
            eventData.oldTab.attr( "tabIndex", -1 );
        } else if ( toShow.length ) {
            this.tabs.filter( function() {
                return $( this ).attr( "tabIndex" ) === 0;
            } )
                .attr( "tabIndex", -1 );
        }

        toShow.attr( "aria-hidden", "false" );
        eventData.newTab.attr( {
            "aria-selected": "true",
            "aria-expanded": "true",
            tabIndex: 0
        } );
    },

    _activate: function( index ) {
        var anchor,
            active = this._findActive( index );

        // Trying to activate the already active panel
        if ( active[ 0 ] === this.active[ 0 ] ) {
            return;
        }

        // Trying to collapse, simulate a click on the current active header
        if ( !active.length ) {
            active = this.active;
        }

        anchor = active.find( ".ui-tabs-anchor" )[ 0 ];
        this._eventHandler( {
            target: anchor,
            currentTarget: anchor,
            preventDefault: $.noop
        } );
    },

    _findActive: function( index ) {
        return index === false ? $() : this.tabs.eq( index );
    },

    _getIndex: function( index ) {

        // meta-function to give users option to provide a href string instead of a numerical index.
        if ( typeof index === "string" ) {
            index = this.anchors.index( this.anchors.filter( "[href$='" +
                $.ui.escapeSelector( index ) + "']" ) );
        }

        return index;
    },

    _destroy: function() {
        if ( this.xhr ) {
            this.xhr.abort();
        }

        this.tablist
            .removeAttr( "role" )
            .off( this.eventNamespace );

        this.anchors
            .removeAttr( "role tabIndex" )
            .removeUniqueId();

        this.tabs.add( this.panels ).each( function() {
            if ( $.data( this, "ui-tabs-destroy" ) ) {
                $( this ).remove();
            } else {
                $( this ).removeAttr( "role tabIndex " +
                    "aria-live aria-busy aria-selected aria-labelledby aria-hidden aria-expanded" );
            }
        } );

        this.tabs.each( function() {
            var li = $( this ),
                prev = li.data( "ui-tabs-aria-controls" );
            if ( prev ) {
                li
                    .attr( "aria-controls", prev )
                    .removeData( "ui-tabs-aria-controls" );
            } else {
                li.removeAttr( "aria-controls" );
            }
        } );

        this.panels.show();

        if ( this.options.heightStyle !== "content" ) {
            this.panels.css( "height", "" );
        }
    },

    enable: function( index ) {
        var disabled = this.options.disabled;
        if ( disabled === false ) {
            return;
        }

        if ( index === undefined ) {
            disabled = false;
        } else {
            index = this._getIndex( index );
            if ( $.isArray( disabled ) ) {
                disabled = $.map( disabled, function( num ) {
                    return num !== index ? num : null;
                } );
            } else {
                disabled = $.map( this.tabs, function( li, num ) {
                    return num !== index ? num : null;
                } );
            }
        }
        this._setOptionDisabled( disabled );
    },

    disable: function( index ) {
        var disabled = this.options.disabled;
        if ( disabled === true ) {
            return;
        }

        if ( index === undefined ) {
            disabled = true;
        } else {
            index = this._getIndex( index );
            if ( $.inArray( index, disabled ) !== -1 ) {
                return;
            }
            if ( $.isArray( disabled ) ) {
                disabled = $.merge( [ index ], disabled ).sort();
            } else {
                disabled = [ index ];
            }
        }
        this._setOptionDisabled( disabled );
    },

    load: function( index, event ) {
        index = this._getIndex( index );
        var that = this,
            tab = this.tabs.eq( index ),
            anchor = tab.find( ".ui-tabs-anchor" ),
            panel = this._getPanelForTab( tab ),
            eventData = {
                tab: tab,
                panel: panel
            },
            complete = function( jqXHR, status ) {
                if ( status === "abort" ) {
                    that.panels.stop( false, true );
                }

                that._removeClass( tab, "ui-tabs-loading" );
                panel.removeAttr( "aria-busy" );

                if ( jqXHR === that.xhr ) {
                    delete that.xhr;
                }
            };

        // Not remote
        if ( this._isLocal( anchor[ 0 ] ) ) {
            return;
        }

        this.xhr = $.ajax( this._ajaxSettings( anchor, event, eventData ) );

        // Support: jQuery <1.8
        // jQuery <1.8 returns false if the request is canceled in beforeSend,
        // but as of 1.8, $.ajax() always returns a jqXHR object.
        if ( this.xhr && this.xhr.statusText !== "canceled" ) {
            this._addClass( tab, "ui-tabs-loading" );
            panel.attr( "aria-busy", "true" );

            this.xhr
                .done( function( response, status, jqXHR ) {

                    // support: jQuery <1.8
                    // http://bugs.jquery.com/ticket/11778
                    setTimeout( function() {
                        panel.html( response );
                        that._trigger( "load", event, eventData );

                        complete( jqXHR, status );
                    }, 1 );
                } )
                .fail( function( jqXHR, status ) {

                    // support: jQuery <1.8
                    // http://bugs.jquery.com/ticket/11778
                    setTimeout( function() {
                        complete( jqXHR, status );
                    }, 1 );
                } );
        }
    },

    _ajaxSettings: function( anchor, event, eventData ) {
        var that = this;
        return {

            // Support: IE <11 only
            // Strip any hash that exists to prevent errors with the Ajax request
            url: anchor.attr( "href" ).replace( /#.*$/, "" ),
            beforeSend: function( jqXHR, settings ) {
                return that._trigger( "beforeLoad", event,
                    $.extend( { jqXHR: jqXHR, ajaxSettings: settings }, eventData ) );
            }
        };
    },

    _getPanelForTab: function( tab ) {
        var id = $( tab ).attr( "aria-controls" );
        return this.element.find( this._sanitizeSelector( "#" + id ) );
    }
} );


AsalaeGlobal.initContentScope = function () {
    /**
     * Permet de réduire une section (bouton)
     */
    $('section.minifiable .r-actions').find('i.fa-compress').remove().end().prepend(
        $('<i></i>')
            .addClass('fa')
            .addClass('fa-compress')
            .attr('role', 'button')
            .attr('title', "Réduire")
            .addClass('slide-icon')
            .attr('tabindex', 0)
            .clickEnter(
                function () {
                    $(this).closest('section').find('.minifiable-target')
                        .slideToggle(
                            400,
                            function () {
                                $(window).trigger('resize');
                            }
                        );
                    $(this).toggleClass('fa-compress')
                        .toggleClass('fa-expand');
                    if ($(this).hasClass('fa-compress')) {
                        $(this).attr('title', "Réduire");
                    } else {
                        $(this).attr('title', "Agrandir");
                    }
                }
            )
    );

    $('.smart-td-size').off('change', AsalaeGlobal.smartTdSize).change(AsalaeGlobal.smartTdSize);

    /**
     * Dropbox
     */
    $('div.dropbox').off('.draggable-dropbox').on(
        'dragover.draggable-dropbox',
        function () {
            $(this).addClass('dragover');
        }
    ).on(
        'dragleave.draggable-dropbox dragend.draggable-dropbox drop.draggable-dropbox',
        function () {
            $(this).removeClass('dragover');
        }
    );

    /**
     * Cache le bouton filtre si il n'y a pas de filtre défini
     */
    $(
        function () {
            if ($('#modal-filters').find('select').length > 0) {
                $('#menu-li-filter').show();
            }
        }
    );

    /**
     * Change la couleur du bouton filtre si il y a des filtres selectionnés
     * Ajoute un bouton filtre aux endroits voulu
     */
    $(
        function () {
            var permanents = $('.permanent-filters')
                .find('select, input, textarea')
                .filter(function() {
                    return $(this).val();
                });
            if ($('.filter-container > div > button.close').length > 0
                || permanents.length
            ) {
                $('#menu-li-filter').addClass('active-dot');
                $('.display-active-filter').show().off('.click.filter')
                    .on('click.filter', function () {
                            $('form.ModalFilters').empty().submit(); // deprecated
                            $('form.filters-section-form').empty().submit();
                        }
                    );
            } else {
                $('#menu-li-filter').removeClass('active-dot');
                $('.display-active-filter').hide();
            }
        }
    );

    /**
     * Déplacement de la fenetre modal
     */
    $(
        function () {
            $('div.modal-dialog').draggableCustom({handle: 'div.modal-header, .bottom-handle'})
                .find('div.modal-header, .bottom-handle').css('cursor', 'move');
        }
    );

    /**
     * Redirige vers la page indiqué dans l'input crée lors du clic sur les "..."
     */
    $(AsalaeGlobal.paginationSelectInput);

    /**
     * Permet la réduction des modals
     */
    $(
        function () {
            $('div.modal.minifiable').each(AsalaeGlobal.minifiableModal);
        }
    );

    /**
     * Rend le contenu des modals redimensionnable (garde la taille actuelle comme minimale)
     */
    $(
        function () {
            $('.modal-content').each(
                function () {
                    $(this).attr('data-width', $(this).actual('width'))
                        .attr('data-height', $(this).actual('height'));
                    $(this).resizable({alsoResize: $(this).find('.modal-body')}).on(
                        "resize",
                        function () {
                            var height = parseInt($(this).css('height'), 10),
                                width = parseInt($(this).css('width'), 10),
                                modalBody = $(this).find('.modal-body'),
                                iframe = $(this).find('iframe');
                            if ($(this).width() <= 0 || $(this).height() <= 0) {
                                return;
                            }
                            if (modalBody.attr('style')) {
                                modalBody.css({maxHeight: 'none', width: '100%'});
                            }
                            if (height < $(this).attr('data-height')) {
                                $(this).css({height: ''});
                                if ($(this).height() > height) {
                                    height = $(this).height();
                                }
                            }
                            if (width < $(this).attr('data-width')) {
                                $(this).css({width: ''});
                                if ($(this).width() > width) {
                                    width = $(this).width();
                                }
                            }
                            if (iframe) {
                                iframe.attr('height', height -85);
                            }
                            $(this).attr('data-width', width)
                                .attr('data-height', height);
                        }
                    );
                }
            );
        }
    );

    /**
     * Si un tri est effectué sur un tableau, fait apparaitre une icone (tri)
     */
    $(
        function () {
            $('table > thead > tr > th')
                .find('a.sort-asc, a.sort-desc')
                .closest('th').addClass('active')
                .closest('section')
                .find('> header .l-actions .display-active-sort')
                .show()
                .clickEnter(
                    function () {
                        var url = location.href,
                            match = url.match(/([&?]sort=[^&]+)(?:&|$)/);
                        if (match) {
                            url = url.substr(0, url.indexOf(match[1]))
                                + url.substr(url.indexOf(match[1])+ match[1].length);
                        } else {
                            return;
                        }
                        match = url.match(/([&?]direction=[^&]+)(?:&|$)/);
                        if (match) {
                            url = url.substr(0, url.indexOf(match[1]))
                                + url.substr(url.indexOf(match[1])+ match[1].length);
                        }
                        AsalaeGlobal.interceptedLinkToAjax(url);
                    }
                );
        }
    );

    $(window).trigger('resize');
}
AsalaeGlobal.initContentScope();
