import $ from "jquery";

export default class RefaeLabel
{
    static span(text, backgroundColor, group = null)
    {
        if (!backgroundColor) {
            backgroundColor = '#ffffff';
        }
        let fontColor = '#000000';
        if (RefaeLabel.calcBrightness(backgroundColor) < 160) {
            fontColor = '#ffffff';
        }
        let span = $('<span class="label-span">');
        span.css({
            outline: '2px solid ' + backgroundColor
        });
        if (group) {
            span.addClass('with-group');
            span.append(
                $('<span class="label-bg label-item-group">')
                    .css({
                        color: fontColor,
                        'background-color': backgroundColor
                    })
                    .text(group)
            );
            span.append(
                $('<span class="label-item-text">').css({
                    color: '#333333'
                }).text(text)
            );
        } else {
            span.addClass('without-group');
            span.append(
                $('<span class="label-bg label-item-text">').text(text)
            );
            span.css({
                'background-color': backgroundColor,
                color: fontColor
            });
        }
        return span;
    }

    static calcBrightness(color)
    {
        let [m, r, g, b] = color.match(/^#([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i);
        r = parseInt(r, 16) * 299;
        g = parseInt(g, 16) * 587;
        b = parseInt(b, 16) * 114;
        return (r + g + b) / 1000;
    }

    static colorDiv(color)
    {
        return $('<div>').css({
            display: 'inline-block',
            width: '20px',
            height: '20px',
            'background-color': color
        });
    }

    static callbackTableData(value, row)
    {
        return $('<div class="label-group">')
            .append(RefaeLabel.span(row.name, row.color, row.labelGroup?.name));
    }

    static callbackMultipleTableData(labels)
    {
        var div = $('<div class="label-group">');
        for (let i = 0; i < labels.length; i++) {
            let label = labels[i];
            div.append(RefaeLabel.span(label.name, label.color, label.labelGroup?.name));
        }
        return div;
    }

    static templateResult(option)
    {
        if ($(option.element).is('option')) {
            let label = JSON.parse($(option.element).attr('data-s2-label'));
            return $('<div class="label-group">')
                .append(RefaeLabel.span(label.name, label.color, label.labelGroup?.name));
        }
        return option.text;
    }

    static templateSelection(option, container)
    {
        if ($(option.element).is('option')) {
            container = $(container);
            let label = JSON.parse($(option.element).attr('data-s2-label'));
            let span = RefaeLabel.span(label.name, label.color, label.labelGroup?.name);
            let cssClass = span.attr('class');
            span.removeAttr('class');
            container.find('> span').replaceWith(span);
            container.attr('style', span.attr('style'));
            span.removeAttr('style')
            container
                .addClass(cssClass)
                .addClass('label-option-container')
                .append(span);

            return ;
        }
        return option.text;
    }
}

$(function() {
    $('[data-render-labels]').each(function() {
        var labels = JSON.parse($(this).attr('data-render-labels'));
        $(this).replaceWith(RefaeLabel.callbackMultipleTableData(labels));
    });
});
$(document).on('modal-loaded', function (event, modal) {
    modal.find('[data-render-labels]').each(function() {
        var labels = JSON.parse($(this).attr('data-render-labels'));
        $(this).replaceWith(RefaeLabel.callbackMultipleTableData(labels));
    });
});
