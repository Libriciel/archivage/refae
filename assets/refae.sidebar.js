import $ from "jquery";

$(function() {
    var navContainer = $('#sidebar-container');
    navContainer.find('.wrapper > a').on(
        'mouseover focus',
        function () {
            // Si le menu est en mode réduit
            if (navContainer.hasClass('sidebar-collapsed')
                || window.matchMedia('(max-width: 767px)').matches
            ) {
                var target = $($(this).attr('data-toggle-href'));
                navContainer.find('.hover-show').not(target).removeClass('hover-show');
                target.addClass('hover-show');
                target.find('ul > a').addClass('hover-show');
                target.css('top', $(this).offset().top)
            }
        }
    );
    navContainer.on(
        'mouseleave focusout',
        function (event) {
            setTimeout(
                function () {
                    var isChildOfMenu = $(':focus').closest(navContainer).length === 1;
                    if ((event.type === 'blur' || event.type === 'focusout') && isChildOfMenu) {
                        return;
                    }
                    navContainer.find('.hover-show').removeClass('hover-show');
                },
                10
            );
        }
    );
    navContainer.on('click', '.hover-show li a[data-toggle].with-anchor', function (event) {
        if ($(event.target).is('span.anchor[data-href]')) {
            return;
        }
        let element = $($(this).attr('data-toggle-href'));
        if (element.hasClass('show')) {
            element.removeClass('show');
            $(this).addClass('collapsed').attr('aria-expanded', 'false');
        } else {
            element.addClass('show');
            $(this).removeClass('collapsed').attr('aria-expanded', 'true');
        }
        return false;
    });
    navContainer.on('keypress', 'a[data-original-href] .anchor', function (event) {
        let charCode = event.originalEvent.charCode;
        if ([13, 32].indexOf(charCode) !== -1) {
            event.preventDefault();
            $(this).trigger('click');
        }
    });
});
