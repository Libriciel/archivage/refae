import $ from "jquery";
import RefaeModal from './refae.modal';
import { AsalaeGlobal } from '../vendor/libriciel/asalae-assets/src/js/asalae.global-top'

export default class RefaeSession
{
    static init = function (remainingTime, checkSessionUrl)
    {
        if (!/username=/.test(checkSessionUrl)) {
            return;
        }
        RefaeSession.checkSessionUrl = checkSessionUrl;
        RefaeSession.checkSessionTimeout = function () {
            $.ajax(
                {
                    url: RefaeSession.checkSessionUrl,
                    headers: {
                        "Accept": "application/json",
                        "No-Session-Renewal": 'true'
                    },
                    success: function (response) {
                        if (response.connected) {
                            RefaeSession.setTimeout(response.time_remaining);
                        } else {
                            RefaeSession.displaySessionLost();
                        }
                    }
                }
            );
        };
        RefaeSession.setTimeout(remainingTime);
    }

    static setTimeout(remainingTime)
    {
        if (RefaeSession.timeout) {
            clearTimeout(RefaeSession.timeout);
        }
        RefaeSession.setRemaining(remainingTime);
        RefaeSession.timeout = setTimeout(RefaeSession.checkSessionTimeout, (remainingTime + 1) * 1000);
    }

    static displaySessionLost()
    {
        let modal = $(RefaeModal.modalId).first();
        RefaeSession.setRemaining(0);
        if (typeof RefaeSession.login_url === 'undefined') {
            RefaeSession.login_url = '/public/login';
        }
        var visible = true;
        if (!modal.is(':visible')) {
            $(RefaeModal.modalTitleId).text("Votre session a expiré");
            $(RefaeModal.modalBodyId).empty();
            visible = false;
        }
        // bloque la modale tant qu'on ne s'est pas reconnecté
        $(RefaeModal.modalHeaderId)
            .find('.close')
            .attr('data-toggle', 'tooltip')
            .attr('title', "Reconnexion nécessaire pour fermer la modale")
            .disable();
        $(RefaeModal.modalFooterId).hide();
        $('.modal-backdrop').remove();
        modal
            .off('.bs.modal')
            .removeData('bs.modal')
            .removeClass('fade')
            .modal({backdrop: 'static', keyboard: false}, 'show')
        ;
        modal.addClass('fade');
        modal.find('.lost-session-div').remove();
        RefaeSession.createSessionLostDiv(modal);
        if (!visible) {
            modal.addClass('small-modal');
            $(RefaeModal.modalTitleId).html("Reconnexion");
        } else {
            let lastChild = $(RefaeModal.modalBodyId).find('> *:last-child');
            setTimeout(() => AsalaeGlobal.scrollBottom(lastChild), 400);
        }
    }

    static createSessionLostDiv(modal) {
        let lostSessionContainer = $('<div class="lost-session-div alert alert-info">');
        let lostSessionDivText = $('<div class="alert-with-icon m-1">').text(
            "Votre session a expiré, vous pouvez vous reconnecter depuis un " +
            "nouvel onglet en cliquant sur le bouton suivant."
        );
        lostSessionDivText.prepend('<i class="fa fa-lg fa-warning text-warning" aria-hidden="true"></i>');
        let lostSessionDivButtons = $('<div class="lost-session-button-wrapper alert-with-buttons m-1">');
        lostSessionContainer.append(lostSessionDivText);
        lostSessionContainer.append(lostSessionDivButtons);
        let linkAnchor = $('<button type="button" class="btn btn-default">')
            .text("Se\xA0reconnecter");

        lostSessionDivButtons.append(linkAnchor);

        let reconnectDiv = $('<div class="lost-session-button-wrapper alert-with-buttons m-1">');
        let reconnectText = $('<div>').text("Suite à votre reconnexion dans un autre onglet, " +
            "vous pourrez reprendre vos travaux en cours en cliquant sur le bouton suivant.");
        reconnectDiv.append(reconnectText);
        lostSessionContainer.append(reconnectDiv);
        let reconnectDivButton = $('<div class="lost-session-button-wrapper alert-with-buttons mt-1">');
        lostSessionContainer.append(reconnectDivButton);
        let button = $('<button type="button" class="btn btn-default">')
            .text("Reprendre votre session de travail");
        reconnectDivButton.append(button);
        $(RefaeModal.modalBodyId).append(lostSessionContainer);
        button.on('click.sessionCheck', RefaeSession.callbackButtonBackToWork(linkAnchor, button));
        linkAnchor.on('click.openPopup', RefaeSession.callbackButtonPopup(linkAnchor, button));
    }

    static callbackButtonPopup(linkAnchor, button)
    {
        var popupWindow = null;
        return function (event) {
            var mouseX = event.screenX;
            var mouseY = event.screenY;
            if (popupWindow === null || popupWindow.closed) {
                var options = "width=1100,height=800,scrollbars=yes," +
                    "left=" + (mouseX - 400) + ",top=" + (mouseY - 100);
                popupWindow = window.open(RefaeSession.login_url, "_blank", options);
                let started = false;
                popupWindow.onload = () => {
                    started = true;
                };
                // pour les autres domains (openid)
                setTimeout(() => started = true, 2000);

                let interval;
                interval = setInterval(
                    () => {
                        if (!popupWindow) {
                            clearInterval(interval);
                            return;
                        }
                        if (!started) {
                            return;
                        }
                        try {
                            String(popupWindow);
                        } catch (e) {
                            return; // autre domain (openid)
                        }
                        if (popupWindow?.RefaeSession?.remaining) {
                            clearInterval(interval);
                            popupWindow.close();
                            button.trigger('click');
                        }
                        if (popupWindow?.closed) {
                            clearInterval(interval);
                        }
                    },
                    200
                );
            } else {
                popupWindow.focus();
            }
        };
    }

    static callbackButtonBackToWork(linkAnchor, button)
    {
        return function (event) {
            event.preventDefault();
            $.ajax(
                {
                    url: RefaeSession.checkSessionUrl,
                    headers: {
                        "Accept": "application/json",
                        "No-Session-Renewal": 'true'
                    },
                    success: function (response) {
                        if (response.connected) {
                            RefaeSession.setTimeout(response.time_remaining);
                            $(RefaeModal.modalHeaderId)
                                .find('.close')
                                .removeAttr('data-toggle')
                                .removeAttr('title')
                                .enable();
                            $(RefaeModal.modalBodyId).find('.lost-session-div').remove();
                            $(RefaeModal.modalFooterId).show();
                            if ($(RefaeModal.modalBodyId).is(':empty')) {
                                $(RefaeModal.modalId).one('hide.bs.modal.sessionCheck', function () {
                                    $('.modal-backdrop').remove();
                                    $(RefaeModal.modalId)
                                        .off('.bs.modal')
                                        .removeData('bs.modal')
                                        .trigger('hide.bs.modal')
                                    ;
                                    $(RefaeModal.modalId).one(
                                        'hidden.bs.modal',
                                        () => $(RefaeModal.modalId).removeClass('small-modal'),
                                    );
                                });
                                RefaeModal.hide();
                            }
                        } else {
                            RefaeSession.setRemaining(0);
                            button
                                .attr('title', "Vous n'êtes pas connecté")
                                .tooltip('show')
                            ;
                            linkAnchor.off('.sessionCheck').on('click.sessionCheck', function() {
                                button.tooltip('dispose');
                            });
                        }
                    }
                }
            );
        };
    }

    static setRemaining(remaining)
    {
        RefaeSession.timestamp = Date.now();
        RefaeSession.remaining = remaining;
    }

    static getRemaining()
    {
        return Math.max(0, (RefaeSession.timestamp - Date.now()) / 1000 + RefaeSession.remaining);
    }
}
