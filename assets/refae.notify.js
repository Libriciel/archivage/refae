import $ from 'jquery';
import Favico from './favico';

export default class RefaeNotify {
    static init = function(deleteUrl) {
        let config = {
            element: '#notifications-container',
            elementUl: '#notifications-ul-container',
            token: 'admin',
            ajax: {
                deleteNotification: deleteUrl
            }
        };
        RefaeNotify.instance = new RefaeNotify();
        RefaeNotify.instance.session = null;
        RefaeNotify.instance.data = null;
        RefaeNotify.instance.count = 0;
        RefaeNotify.instance.user_id = 0;
        RefaeNotify.instance.token = config.token;
        RefaeNotify.instance.element = config.element;
        RefaeNotify.instance.elementUl = config.elementUl;
        RefaeNotify.instance.favicon = new Favico(
            {
                type : 'rectangle',
                animation:'none',
                bgColor : '#f05922',
                fontFamily : 'Arial'
            }
        );
        RefaeNotify.instance.ajax = {
            deleteNotification: config.ajax.deleteNotification
        };
        // Arrete l'animation de la cloche
        $('#container-number-notification').hover(
            function () {
                $(this).find('i.fa-bell').removeClass('fa-shake');
            }
        ).click(
            function (event) {
                $(config.element).children().each(
                    function () {
                        $(this).stop(true, true).slideUp(
                            400,
                            function () {
                                $(this).remove();
                            }
                        );
                    }
                )
            }
        );
    }

    static mercureCallback = function (data) {
        RefaeNotify.instance.updateCount(1);
        if (RefaeNotify.instance.count <= 20) {
            RefaeNotify.instance.append(data);
        }
        if (RefaeNotify.instance.count === 20) {
            alert(
                "L'affichage des notifications a été désactivé afin de préserver " +
                "les performances de votre navigateur. Afin de voir les " +
                "notifications suivantes, il vous faudra lire les notifications" +
                " actuelles et recharger la page."
            )
        }
    }

    updateCount(amount)
    {
        var containerNumberNotification = $('#container-number-notification'),
            notificationBell = containerNumberNotification.find('i.fa-bell'),
            numberNotification = $('#number-notification');
        if (amount === 0 || (RefaeNotify.instance.count === 0 && amount < 0)) {
            notificationBell.removeClass("fa-shake");
            return;
        } else if (amount > 0 && !containerNumberNotification.hasClass('active')) {
            notificationBell.addClass("fa-shake");
        }
        RefaeNotify.instance.count += amount;
        RefaeNotify.instance.favicon.badge(RefaeNotify.instance.count);
        numberNotification.text(RefaeNotify.instance.count).addClass("fa-horizontal");
        setTimeout(
            function () {
                numberNotification.removeClass('faa-horizontal');
            },
            1000
        );

        return RefaeNotify.instance;
    }

    insert(data, deleteByAjax = true)
    {
        $(RefaeNotify.instance.element).empty();

        RefaeNotify.instance.updateCount(data.length);

        for (var key in data) {
            RefaeNotify.instance.append(data[key], deleteByAjax);
        }

        return RefaeNotify.instance;
    }

    static filterExecutable(element)
    {
        $(element).each(
            function () {
                if ($(this).prop("tagName") === 'SCRIPT') {
                    $(this).remove();
                    return;
                }
                var attrs = $(this).attr();
                for (var key in attrs) {
                    if (key.substr(0, 2) === 'on' || attrs[key].indexOf('javascript:') !== -1) {
                        $(this).removeAttr(key);
                    }
                }
                var children = $(this).children();
                for (var i = 0; i < children.length; i++) {
                    RefaeNotify.filterExecutable(children[i]);
                }
            }
        );
        return element;
    }

    append(data, deleteByAjax = true)
    {
        var div, button, that = RefaeNotify.instance, action, li, timer, timeMessage, elapsed;
        var message = RefaeNotify.filterExecutable($('<div></div>').append(data.text));
        var created = typeof data.created === 'object' ? data.created.date : data.created;
        elapsed = Math.round(Math.abs(new Date(created) - new Date()) / 1000);
        if (elapsed < 60) {
            timeMessage = "à l'instant";
        } else if (elapsed < 3600) {
            timeMessage = "depuis " + Math.round(elapsed / 60) + " minute(s)";
        } else if (elapsed < 86400) {
            timeMessage = "depuis " + Math.floor(elapsed / 3600) + " heure(s)";
        } else if (elapsed >= 86400) {
            timeMessage = "depuis " + Math.floor(elapsed / 86400) + " jour(s)";
        }

        div = $('<div></div>')
            .addClass('alert')
            .addClass('alert-dismissible')
            .addClass('alert-notification')
            .addClass(data.css_class)
            .attr('id', 'notification-'+data.id)
            .attr('role', 'alert');

        button = $('<button></button>')
            .addClass('close')
            .attr('type', 'button')
            .attr('aria-label', 'Close')
            .append($('<span></span>').append('&times;'));

        timer = $('<span>')
            .addClass('notification-timer')
            .attr('data-created', created)
            .text(timeMessage);

        div.append(button)
            .append(message.html())
            .append(timer.clone());

        message.append(timer);

        li = $('<li></li>')
            .addClass('navbar-link')
            .addClass('alert')
            .addClass('notification')
            .addClass('alert-notification')
            .addClass(data.css_class)
            .attr('id', 'notification-li-'+data.id)
            .append(message);

        action = function (event) {
            if (event.target.tagName === 'A') {
                return;
            }
            event.stopPropagation();
            // Evite les clics succéssifs
            div.unbind('click');
            li.unbind('click');
            // On envoi l'evenement de cloture aux autres fenètres
            if (that.session !== null) {
                that.session.publish(
                    'close_user_'+that.user_id+'_'+that.token,
                    {
                        target: '#notification-'+data.id,
                        targetLi: '#notification-li-'+data.id
                    }
                );
            } else {
                $(div).stop(true, true).slideToggle(
                    400,
                    function () {
                        $(div).remove();
                    }
                );
                $(li).stop(true, true).slideToggle(
                    400,
                    function () {
                        $(li).remove();
                    }
                );
                that.updateCount(-1);
            }
            // Ajax pour suppression en base
            if (deleteByAjax) {
                $.ajax(
                    {
                        method: 'DELETE',
                        url: that.ajax.deleteNotification+'/'+data.id,
                        error: function () {
                            console.error("Notification delete failed");
                        }
                    }
                );
            }
        };
        div.click(action);
        li.click(action);

        setTimeout(
            function () {
                div.stop(true, true).slideUp(
                    400,
                    function () {
                        $(this).remove();
                    }
                );
            },
            5000
        );

        $(RefaeNotify.instance.element).append(div);
        $(RefaeNotify.instance.elementUl).append(li);

        return RefaeNotify.instance;
    }
}

setInterval(
    function() {
        $('.notification-timer[data-created]').each(
            function() {
                var timeMessage = '';
                var elapsed = Math.round(Math.abs(new Date($(this).attr('data-created')) - new Date()) / 1000);
                if (elapsed < 60) {
                    timeMessage = "à l'instant";
                } else if (elapsed < 3600) {
                    timeMessage = "depuis " + Math.round(elapsed / 60) + " minute(s)";
                } else if (elapsed < 86400) {
                    timeMessage = "depuis " + Math.floor(elapsed / 3600) + " heure(s)";
                } else if (elapsed >= 86400) {
                    timeMessage = "depuis " + Math.floor(elapsed / 86400) + " jour(s)";
                }
                $(this).text(timeMessage);
            }
        )
    },
    30000
);
