import $ from "jquery";
import tinymce from 'tinymce';

import 'tinymce/icons/default';
import 'tinymce/themes/silver';
import 'tinymce/skins/ui/oxide/skin.css';
import 'tinymce/models/dom/model';
import 'tinymce/plugins/lists';
import 'tinymce/plugins/advlist';
import 'tinymce/plugins/emoticons';
import 'tinymce/plugins/link';
import 'tinymce/plugins/image';

window.tinymce = tinymce;

/**
 * Editeur HTML
 */
export default class RefaeMce
{
    constructor(options = {}, colorChanger = null)
    {
        var that = this;
        this.tiny = {};
        options = $.extend(
            true,
            {
                selector: 'textarea.mce-small',
                plugins: 'link lists advlist image',
                toolbar1: 'styles | forecolor fontsizeinput | bold italic underline | removeformat',
                toolbar2: 'undo redo | bullist alignleft aligncenter alignright alignjustify | outdent indent | link image',
                menubar: false,
                branding: false,
                content_css: false,
                skin: false,
                language: 'fr_FR',
                setup: function (editor) {
                    editor.on(
                        'keyup',
                        function (e) {
                            editor.save();
                        }
                    );
                    editor.on(
                        'change',
                        function (e) {
                            editor.save();
                            $(editor.getElement()).trigger('change');
                        }
                    );
                }
            },
            options
        );

        $(options.selector).each(
            function () {
                var noptions = Object.assign({}, options);
                var id = $(this).attr('id');
                if (!id) {
                    id = 'as-mce-'+RefaeMce.id_given;
                    $(this).attr('id', id);
                    RefaeMce.id_given++;
                }
                noptions.selector = '#'+id;
                that.tinymce(noptions, colorChanger);
            }
        );

        $(document).off('.tinymodal').on(
            'focusin.tinymodal',
            function (e) {
                var dialog = $(e.target).closest(".tox-dialog");
                var modal = $(".modal:visible").last();
                if (dialog.length && modal.length && modal.find(dialog).length === 0) {
                    var wrapper = $('.tox-tinymce-aux');
                    modal.append(wrapper);
                }
            }
        );
    }

    tinymce(options, colorChanger)
    {
        var that = this;
        var id = options.selector;

        tinymce.init(options).then(
            function (result) {
                that.tiny[id] = result[0];
                if (colorChanger) {
                    $(colorChanger).change();
                }
            }
        );
        if (colorChanger) {
            $(colorChanger).change(
                function () {
                    var css = that.applyStyle($(colorChanger).val());
                    if (that.tiny[id]) {
                        that.tiny[id].getBody().setAttribute('style', css);
                    }
                }
            ).change();
        }
        var modal = $(id).closest('.modal');
        if (modal.length) {
            modal.one(
                'hidden.bs.modal',
                function () {
                    tinymce.remove(options.selector);
                    that.tiny[id] = null;
                }
            );
        }
    }

    applyStyle(option)
    {
        var css = {
            "color": "#333"
        };
        switch (option) {
            case 'alert-info':
                css["background-color"] = '#c8e5f3';
                break;
            case 'alert-warning':
                css["background-color"] = '#faf5d7';
                break;
            case 'alert-danger':
                css["background-color"] = '#eed5d5';
                break;
            case 'alert-success':
                css["background-color"] = '#d4eacb';
                break;
        }
        var output = [];
        for (var key in css) {
            output.push(key+': '+css[key]);
        }
        return output.join('; ');
    }

    static createInstance(textarea)
    {
        let colorChanger = null;
        if ($(textarea).attr('data-colorchanger')) {
            colorChanger = $(textarea).attr('data-colorchanger');
        }
        let instance = new RefaeMce({selector: '#' + $(textarea).attr('id')}, colorChanger);
        let modal = $(textarea).closest('.modal');
        return instance;
    }
}
RefaeMce.id_given = 0;

$(function() {
    $('[data-tinymce][id]').each(function () {
        var textarea = RefaeMce.createInstance($(this));
    });
});
$(document).on('modal-loaded', function (event, modal) {
    modal.find('[data-tinymce][id]').each(function () {
        var textarea = RefaeMce.createInstance($(this));
    });
});
