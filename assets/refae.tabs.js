import $ from "jquery";

class RefaeTabs
{
    constructor(anchorId) {
        this.anchorId = anchorId;
    }

    tabs()
    {
        var anchorId = this.anchorId;
        var anchor = $('#' + this.anchorId);
        var ul = $('<ul>');
        anchor.append(ul);
        var i = 0;
        $('[data-tabs="' + this.anchorId + '"]').each(function () {
            i++;
            var id = $(this).attr('id');
            if (!id) {
                id = anchorId + '-tab-' + i;
                $(this).attr('id', id);
            }
            var li = $('<li>');
            var a = $('<a>').attr('href', '#' + id);
            var icon = '';
            var tabIcon = $(this).data('tab-icon');
            if (tabIcon) {
                icon = $('<i class="fa fa-space" aria-hidden="true">').addClass(tabIcon);
            }
            var span = $('<span>').text($(this).data('tab-text'));
            a.append(icon).append(span);
            li.append(a);
            ul.append(li);
            anchor.append($(this));
        });
        anchor.tabs();
    }
}


$(function() {
    $('[data-tabs-anchor]').each(function() {
        var anchor = new RefaeTabs($(this).attr('id'));
        anchor.tabs();
    });
});
$(document).on('modal-loaded', function (event, modal) {
    modal.find('[data-tabs-anchor]').each(function() {
        var anchor = new RefaeTabs($(this).attr('id'));
        anchor.tabs();
    });
});
